<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title><s:text name="system.index.title"/></title>
	<%@ include file="/common/meta-css.jsp" %>
	<link href="${r"${ctx}"}/js/easyui/${r"${themeColor}"}/panel.css" rel="stylesheet" type="text/css"/>
<#import "common/field.ftl" as fieldftl>   
<#assign entityName=root.entityName /> 
<#assign formList=root.dataSource.formList/>
<#assign titleName=root.moduleProperties.caption/>
<#assign i18nKey=root.moduleProperties.i18nKey/>
<#assign moduleKey=root.moduleProperties.key/>

<#-- 关联表表 -->
<#assign realEntityName = entityName />
<#if root.moduleProperties.relevanceModule??>
<#assign relevanceModule=root.moduleProperties.relevanceModule/>
<#list formList as form>
<#if form.isMaster>
	<#assign realEntityName = form.entityName />
	<#break/>
</#if>
</#list>
<#else>
<#assign relevanceModule=""/>
</#if>

<#assign isShowInQueryList=false/><#--标识是否存在列表页面显示的tabs-->
<#assign flowList= root.flowList />
<#if formList??>
        <#list formList as form>
            <#if form.isMaster>
            <style type="text/css"><#-- 初始化样式 -->
			<@fieldftl.initQueryPageFormCssOrJavascript formList=formList eventKey="addCssStyle"/>
			</style>
</head>            
         <body class="easyui-layout" fit="true" id="layout_body">
         <#--如果存在树形查询，显示左边的查询框
         <#assign encounterFormTree><@fieldftl.encounterFormTree form=form/></#assign>
         <#if encounterFormTree="true">
         --> 
         <#if form.queryStyle="leftshow">
          <div region="west" title="<s:text name='system.search.title'/>" border="false"
                 split="true" style="width:215px;padding:2px;" 
                 iconCls="icon-search" tools="#pl_tt" >
                		<@fieldftl.initFormTree form=form/>	
                		<#-- 关联表 -->
			            <form action="${r"${ctx}"}/gs/gs-mng!queryList.action?entityName=${entityName}&relevanceModule=${relevanceModule}"
	                      name="queryForm" id="queryForm">
	                       <div class="easyui-accordion" data-options="multiple:true" style="width:98%;" id="queryFormId">
		                     	<@fieldftl.getQueryView form=form/>
		                     	<#list formList as subForm> <#-- 子表查询条件初始化 -->
						     		<#if !subForm.isMaster && !((subForm.isVirtual)!false)>
						     			<@fieldftl.getQueryView form=subForm/>
						     		</#if>
						     	</#list>  	
	                       </div>
	                       <div style="text-align:center;padding:8px 8px;">
						             <button type="button" id="bt_query" class="button_small">
									     <s:text name="system.search.button.title"/>
									 </button>&nbsp;&nbsp;
									 <button type="button" id="bt_reset" class="button_small">
									     <s:text name="system.search.reset.title"/>
									 </button>
						   </div> 
	                </form>
            </div>
            </#if>
            <div region="center" title="" border="false">
                <div class="easyui-layout" fit="true">
            		<#-- <div region="north" border="false" style="text-align:right;height:28px;overflow:hidden;">
            			<ul class="tabs">
            				<li class="">
          						<a href="javascript:void(0)" class="tabs-inner"><span class="tabs-title">总控制台</span><span class="tabs-icon"></span></a>
          					</li>
          					<li class="tabs-selected">
          						<a href="javascript:void(0)" class="tabs-inner"><span class="tabs-title">销售订单</span><span class="tabs-icon"></span></a>
          					</li>
          				</ul>
            		</div> -->
            		<div id="mainsrp-nav" region="north" border="false">
            			<#-- 查询面板 -->
            			<#if form.queryStyle="topshow">
	                    <div class="m-nav">
            				<div class="bread-crumbs row J_breadcrumbs">
            					<#-- 查询面板显隐按钮 -->
            					<div class="counts">
								    <a class="nav-toggle-btn J_navSwitchBtn icon-tag" href="#" title="关闭查询">
								      <span class="icon-btn-arrow-up-3 J_navSwitchBtnSpan"></span>
								    </a>
							    </div>
							    <#-- 查询条件展示栏 -->
							    <div class="crumbs-cont">
							        <span class="cat-name">查询条件</span>
							        <span class="cat-divider"><span class="icon-btn-vbarrow"></span></span>
							    </div>
            				</div>
            				<#-- 查询条件表单 -->
            				<#-- 关联表 -->
            				<form action="${r"${ctx}"}/gs/gs-mng!queryList.action?entityName=${entityName}&relevanceModule=${relevanceModule}" name="queryForm" id="queryForm">
	            				<div class="nav-panel J_navPanel">
	            					<@fieldftl.getMainQueryView form=form/>
	            				</div>
            				</form>
	                    </div>
	                    </#if>
	                    <#-- 查询面板 end-->
	                    <#-- 主要信息，就是显示在列表页面的主表tab start-->
	                    <#list root.dataSource.tabsList as tabs >
                             <#list tabs.tabList as tab>
                                    <#if form.key==tab.form>
                                         <#if form.isMaster && tab.isShowInQueryList?? && tab.isShowInQueryList><#--主表分tab页面-->
                                         	<#assign isShowInQueryList=true>
							                <div id="showInQueryList" style="padding:0;overflow:auto;">
							                <#if tab.layout == "absolute"><#-- 绝对布局 -->
                                         	  <div class="module_div" style="height:${(tab.height)!200}px">
                                         	  	<#assign fields=form.fieldList>
                                         	  	<#list fields as field>
												   <#if field.editProperties.visible && !(field.isCaption?? && field.isCaption)>
												   	<#if field.tabRows==tab.rows && field.tabCols==tab.cols>
													   	<#if field.key?? && field.key!="" >
													   		<@fieldftl.initCheckFieldAuth field=field form=form isEnd=false/>  
													   		<div style="position:absolute;left:${((field.editProperties.left)!10)?c}px;top:${((field.editProperties.top)!10)?c}px;">
														   		<span class="module_th">
														   		<#if field.caption?? && field.caption!="">	
											                        <label for="${field.key}"><#if (field.dataProperties.notNull)!false ><span style="color:red;">*</span></#if><s:text name="${field.i18nKey!field.caption}"/>:</label>
											                    <#else>
											                        &nbsp;
											                    </#if>
											                    </span>
											                    <div class="module_td">
										                    	<@fieldftl.getFiledView field=field form=form/>
										                    	</div>
										                    </div>
										                    <@fieldftl.initCheckFieldAuth field=field form=form isEnd=true/>  
										                </#if>
										             </#if>
												   </#if>
												</#list>
                                         	  </div>
                                         	<#elseif tab.layout == "excel"><#-- excel布局 -->
                                         	  <div style="padding-top:<#if tab.content.paddingTop??>${tab.content.paddingTop}<#else>0</#if>px;padding-bottom:<#if tab.content.paddingBottom??>${tab.content.paddingBottom}<#else>0</#if>px;padding-left:<#if tab.content.paddingLeft??>${tab.content.paddingLeft}<#else>0</#if>px;padding-right:<#if tab.content.paddingRight??>${tab.content.paddingRight}<#else>0</#if>px">
                                         	  	<#assign fields=form.fieldList>
                                         	  	<#assign html=tab.content.html>
                                         	  	<#list fields as field>
                                         	  		<#if field.key?? && field.key!="" && !(field.isCaption?? && field.isCaption)>
                                         	  			<#assign control><@fieldftl.getFiledView field=field form=form/></#assign>
                                         	  			<#assign editType = field.editProperties.editType/>
                                         	  			<#assign myfieldkey = field.key/>
                                         	  			<#assign html = freeMarkerExcel(html,control,editType,myfieldkey)/>
                                         	  		</#if>
                                         	  	</#list>
                                         	  	${html}
                                         	  </div>
                                         	<#else><#-- 表格布局 -->
							                  <table border="0" cellpadding="0" cellspacing="0"  class="module_form">
										                       	<#assign colCount=1>
										                       	<#assign rowCount=0>
																<#assign fields=form.fieldList>
																<#assign columns=tab.tableCols!(form.cols!3)>
																<#assign column=columns>
																<#assign rows=newWritableNumberArray(fields?size)><#--每一行被占的列数-->
												               <#list fields as field>
												                <#if field.editProperties.visible && !(field.isCaption?? && field.isCaption)>														                 
									                                 <#if field.tabRows==tab.rows && field.tabCols==tab.cols>
																	    <#-- 一行首列 -->
																	    <#if colCount == 1>
																	    	<tr>
																	    	<#assign column=column-rows[rowCount]>
																	    </#if>
																	    <#assign oldColumn=column>
																	    <#assign column=column-field.editProperties.cols>
									                                    <#if column gte 0>
									                                    	 <#assign rowspan=field.editProperties.rows>
									                                    	 <#assign colspan=field.editProperties.cols*2-1>
									                                         <#-- 判断行是不是满了-->
									                                       <#if field.key?? && field.key!="">
									                                         <#if field.editProperties.editType != 'Label' && field.editProperties.editType != 'BrowserBox'>
									                                         <th rowspan="${rowspan}">
									                                         	<@fieldftl.initCheckFieldAuth field=field form=form isEnd=false/>
									                                         		<#if field.caption?? && field.caption!="">	
									                                         			<label for="${field.key}"><#if (field.dataProperties.notNull)!false ><span style="color:red;">*</span></#if><s:text name="${field.i18nKey!field.caption}"/>:</label>
									                                                <#else>
									                                                	&nbsp;
									                                                </#if>
												          						 <@fieldftl.initCheckFieldAuth field=field form=form isEnd=true/>  
									                                         </th>
									                                         <td rowspan="${rowspan}" colspan="${colspan}">
									                                            <@fieldftl.initCheckFieldAuth field=field form=form isEnd=false/>   
									                                                   <@fieldftl.getFiledView field=field form=form/>
									                                     		<@fieldftl.initCheckFieldAuth field=field form=form isEnd=true/>      
									                                         </td>
									                                         <#else>
									                                         <td rowspan="${rowspan}" colspan="${colspan+1}">
									                                         	<@fieldftl.initCheckFieldAuth field=field form=form isEnd=false/>   
									                                                   <@fieldftl.getFiledView field=field form=form/>
									                                     		<@fieldftl.initCheckFieldAuth field=field form=form isEnd=true/>   
									                                         </td>
									                                         </#if>
									                                       <#else>
									                                       	 <th rowspan="${rowspan}">&nbsp;</th>
									                                         <td rowspan="${rowspan}" colspan="${colspan}">&nbsp;</td>
									                                       </#if>
										                                         <#if rowspan gt 1>
											                                         <#assign rowStart = rowCount+1>
											                                         <#assign rowEnd = rowCount+rowspan-1>
											                                         <#list rowStart..rowEnd as i>
											                                         	<@modifyArrayDirective seq=rows index=i value=rows[i]+colspan />
											                                         </#list>
											                                      </#if>
									                                      	 <#if column==0>
									                                      	 	<#-- 一行中最后一列 -->
									                                            </tr>
									                                            <#assign rowCount=rowCount+1>
									                                            <#assign colCount=1>
									                                            <#assign column=columns>
									                                         <#else>
									                                         	<#assign colCount=colCount + 1>
									                                         </#if>
									                                     <#else>
										                                     	<th>&nbsp;</th>
												                                <td colspan="${oldColumn*2-1}">&nbsp;</td>
												                                </tr>
												                                <#assign rowCount=rowCount+1>
									                                            <#assign column=columns>
									                                            <#assign column=column-field.editProperties.cols-rows[rowCount]>
												                                <tr>
												                                <#if column gte 0>
												                                 <#assign rowspan=field.editProperties.rows>
										                                    	 <#assign colspan=field.editProperties.cols*2-1>
										                                         <#-- 判断行是不是满了-->
										                                       <#if field.key?? && field.key!="">
										                                         <#if field.editProperties.editType != 'Label' && field.editProperties.editType != 'BrowserBox'>
										                                         <th rowspan="${rowspan}">
										                                       		<@fieldftl.initCheckFieldAuth field=field form=form isEnd=false/>    
										                                         	<#if field.caption?? && field.caption!="">	
									                                         			<label for="${field.key}"><#if (field.dataProperties.notNull)!false ><span style="color:red;">*</span></#if><s:text name="${field.i18nKey!field.caption}"/>:</label>
									                                                <#else>
									                                                	&nbsp;
									                                                </#if> 
									                                        		<@fieldftl.initCheckFieldAuth field=field form=form isEnd=true/>    
										                                         </th>
										                                         <td rowspan="${rowspan}" colspan="${colspan}">
										                                         	<@fieldftl.initCheckFieldAuth field=field form=form isEnd=false/>    
										                                                   <@fieldftl.getFiledView field=field form=form/>
										                                      		<@fieldftl.initCheckFieldAuth field=field form=form isEnd=true/>  
										                                         </td>
										                                         <#else>
										                                         <td rowspan="${rowspan}" colspan="${colspan+1}">
										                                         	<@fieldftl.initCheckFieldAuth field=field form=form isEnd=false/>   
										                                                   <@fieldftl.getFiledView field=field form=form/>
										                                     		<@fieldftl.initCheckFieldAuth field=field form=form isEnd=true/>   
										                                         </td>
										                                         </#if>
										                                        <#else>
										                                        	<th rowspan="${rowspan}">&nbsp;</th>
										                                         	<td rowspan="${rowspan}" colspan="${colspan}">&nbsp;</td>
										                                        </#if>
										                                         <#if rowspan gt 1>
											                                         <#assign rowStart = rowCount+1>
											                                         <#assign rowEnd = rowCount+rowspan-1>
											                                         <#list rowStart..rowEnd as i>
											                                         	<@modifyArrayDirective seq=rows index=i value=rows[i]+colspan />
											                                         </#list>
											                                     </#if>
												                                 <#if column==0>
										                                      	 	<#-- 一行中最后一列 -->
										                                            </tr>
										                                               <#assign rowCount=rowCount+1>
											                                            <#assign colCount=1>
											                                            <#assign column=columns>
										                                         </#if>
										                                      </#if>
   																	 </#if>
   																  </#if>
   																
   																 </#if>
								                                </#list>
								                                <#if column != columns>
								                                	<th>&nbsp;</th>
									                                <td colspan="${column*2-1}">&nbsp;</td>
									                                </tr>
								                                </#if>
                                               </table>
                                               </#if>
                                             </div>
                                        </#if>
                                   </#if>
                              </#list>
                        </#list>
	                    <#-- 主要信息 end-->
	            		<@fieldftl.initFormTabQuery form=form isInitTab=true/>
            		</div>
            		<div region="center" border="false">
            			<table id="queryList" border="false" fit="true"></table>
            		</div>
            		<#if form.enablePageSelect ><#-- 启用分页选择模式 -->
            		<div region="south" border="false" style="height:200px;" split="true" >
            			<table id="pageSelectList" border="false" fit="true"></table>
            		</div>
            		</#if>
            	</div>
            </div>
             <div id="mm" class="easyui-menu" style="width:120px;">
				<security:authorize
					url="/gs/gs-mng!add.action?entityName=${entityName}">
					<div onclick="doAdd()" iconCls="icon-add"><s:text name="system.button.add.title"/></div>
				</security:authorize>
				<security:authorize
					<#-- 关联表 -->
					url="/gs/gs-mng!delete.action?entityName=${entityName}&relevanceModule=${relevanceModule}">
					<div onclick="doDelete()" iconCls="icon-remove"><s:text name="system.button.delete.title"/></div>
				</security:authorize>
				<security:authorize
					url="/gs/gs-mng!view.action?entityName=${entityName}">
					<div onclick="doCmView()" iconCls="icon-search"><s:text name="system.button.view.title"/></div>
				</security:authorize>
				<security:authorize
					url="/gs/gs-mng!edit.action?entityName=${entityName}">
					<div onclick="doCopy()" iconCls="icon-copy"><s:text name="system.button.copy.title"/></div>
				</security:authorize>
				<#-- <security:authorize
					url="/sys/report/report.action?entityName=${entityName}">
						<div onclick="doViewReport('${moduleKey}',$('#mm').data('id'))" iconCls = "icon-report"><s:text name="system.button.report.title"/></div>
				</security:authorize> --> 
				<security:authorize
					url="/sys/attach/attach.action?entityName=${entityName}">
						<div onclick="doAccessory($('#mm').data('id'),$('#mm').data('attachId'),$('#mm').data('status'))" iconCls = "icon-attach"><s:text name="system.button.accessory.title"/></div>
					 </security:authorize>
				<s:if test='#session.USER.isSuperAdmin=="1"'>
					<div onclick="doOpenModifyCreateUserWin()" iconCls="icon-user"><s:text name="修改所有者"/></div>
				</s:if>
			</div>
			<div id="psl_mm" class="easyui-menu" style="width:120px;">
				<div onclick="doRemoveDataOnPageSelectList()" iconCls="icon-remove"><s:text name="移除所选数据"/></div>
			</div>
			<div id="pl_tt">
				<a href="#" id="a_switch_query" class="icon-menu" title="<s:text name="system.button.switchQueryType.title"/>" style="display:none;"></a>
				<a href="#" id="a_exp_clp" class="accordion-expand" title="<s:text name="system.button.expand.title"/>"></a>
			</div>
			<#-- 新版easyui在初始化layout时，将会查询children中的form元素，如存在则初始化对form兄弟元素无效 -->
			<div style="display:none;">
			<form method="post" name="hidFrm" id="hidFrm" >
				<textarea style="display:none;" name="hid_queryParams" id="hid_queryParams">${r"${param.queryParams}"}</textarea><#-- 查询参数 -->
				<textarea style="display:none;" name="hid_addParams" id="hid_addParams">${r"${param.addParams}"}</textarea><#-- 新增参数 -->
				<input type="hidden" name="hid_relevanceModule" id="hid_relevanceModule" value="${entityName}"/><#-- 关联模块 -->
			</form>
			</div>
			<%@ include file="/common/meta-js.jsp" %>
			<script type="text/javascript" src="${r"${ctx}"}/js/easyui-1.4/easyui.xpPanel.js"></script>
			<script type="text/javascript" src="${r"${ctx}"}/gs/gs-mng!loadJs.action?entityName=${entityName}&jf=listJs&isHideQc=${r"${param.isHideQc}"}"></script>
			<@fieldftl.initQueryPageFormCssOrJavascript formList=formList eventKey="referJsOrCss"/>
            </body>
            </#if>
      </#list>
</#if>
</html>