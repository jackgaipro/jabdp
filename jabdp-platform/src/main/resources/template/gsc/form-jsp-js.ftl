<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<#import "common/field.ftl" as fieldftl> 
<#assign entityName=root.entityName />  
<#assign formList=root.dataSource.formList/>
<#assign tabsList=root.dataSource.tabsList/>
<#assign titleName=root.moduleProperties.caption/>
<#assign i18nKey=root.moduleProperties.i18nKey/>
<#assign moduleKey=root.moduleProperties.key/>