//侧边工具栏
myLib.NS("desktop.lrBar");
myLib.desktop.lrBar={
	upLrBar:function(){
		var myData = myLib.desktop.getMydata();
		var $lrBar = myData.panel.lrBar['_this'];
		var wh = myData.winWh;
		var $default_app_li = $("#default_app li");
		$("#default_app").css({'height':$default_app_li.length*60});
		var top = Math.floor((wh['h']-$lrBar.height())/2)-60;
		$lrBar.css({'top':top});
		$default_app_li.each(function(i){
			$(this).css({'position':'absolute','left':'6px','top':i*60+'px'});
			var absolute = {"left":6,"top":top+i*60+5};
			$(this).data('iconData',$.extend($(this).data('iconData'),{'absoluteSite':absolute}));
		});
	},
	newShortcutHtml:function(data){
		var str = "";
		if(data.childsLength) {
			str = "<li iconSkin='imgIcon " + data.iconSkin + "-auto' onclick=\"" + "$('#menu" + data.id 
			+ "').menu('show',{left:$(this).data('iconData').absoluteSite.left+60,top:$(this).data('iconData').absoluteSite.top+5});\" id='lrBardeskIcon"
			+ data.id + "'><span><img class = 'imgIcon " + data.iconSkin
			+ "-auto' src='" + data.root + "/js/desktop/themes/default/images/null.png' title='"
			+ data.name + "'/></span><div class='text'>" + data.name + "<s></s></div></li>";
		}
		else {
			str = "<li iconSkin='imgIcon " + data.iconSkin + "-auto' onclick=\"myLib.desktop.win.newWin({"
			+ "WindowTitle:'"+data.name+"',"
			+ "iframSrc:'"+data.resourceUrl+"',"
			+ "WindowsId:'"+data.id+"',"
			+ "WindowAnimation:'none',"
			+ "imgclass:$(this).attr('iconSkin')"
			+ "});\""
			+" id='lrBardeskIcon" + data.id + "'><span><img class = 'imgIcon "
			+ data.iconSkin +"-auto' src='"+data.root+"/js/desktop/themes/default/images/null.png' title='"
			+ data.name +"'/></span><div class='text'>" + data.name + "<s></s></div></li>";
		}
		return str;
	},
	init:function(){
		//读取元素对象数据
		var myData=myLib.desktop.getMydata()
	        ,$default_tools=myData.panel.lrBar['default_tools']
		    ,$def_tools_Btn=$default_tools.find('span')
			,$start_btn=myData.panel.lrBar['start_btn']
			,$start_block=myData.panel.lrBar.start_block
			,$start_item=myData.panel.lrBar['start_item']
			,$default_app=myData.panel.lrBar['default_app']
			,$lrBar=myData.panel.lrBar['_this']
			,wh=myData.winWh
			,_this=this;
		//拖动距离
		var dyStart=0,dyEnd=0,dxStart=0,dxEnd=0;
		//初始化侧栏位置
		_this.upLrBar();
		//如果窗口大小改变，则更新侧边栏位置
		$(window).wresize(function(){
			myLib.desktop.winWH();//更新窗口大小数据
 			_this.upLrBar();
		});
		//任务栏右边默认组件区域交互效果
		$def_tools_Btn.hover(
			function(){$(this).css("background-color","#999");}
			,function(){$(this).css("background-color","transparent");}
		);
		//默认应用程序区
		$default_app
		.find('li')
		.droppable({
			//只允许用桌面上的模块
			accept:'.desktop_icon',
			onDrop:function(event,ui){
				var title = $(ui).find(".text").text();
				var str = "";
				if($("#" + $(ui)[0].id.replace(/deskIcon/,"menu")).length){
					str = "<li iconSkin='"+ $(ui).find('img').attr('class') +"'onclick=\""
					+"$('#"+$(ui)[0].id.replace(/deskIcon/,"menu")+"').menu('show',{left:80,top:250});\" id='lrBar" + $(ui)[0].id
					+ "'><span><img class = '"+$(ui).find('img').attr('class')+"' src='"+ $(ui).find('img').attr('src') +"' title='"+title
					+"'/></span>" + "<div class='text'>" + title + "<s></s></div></li>";
				}
				else{
					str = "<li iconSkin='"+ $(ui).find('img').attr('class') +"' onclick=\"myLib.desktop.win.newWin({"
					+"WindowTitle:'"+title+"',"
					+"iframSrc:'"+$(ui).attr('_url')+"',"
					+"WindowsId:'"+$(ui)[0].id+"',"
					+"WindowAnimation:'none',"
					+"imgclass:$(this).attr('iconSkin')"
					+ "});\""
					+" id='lrBar" + $(ui)[0].id + "'><span><img class = '"+$(ui).find('img').attr('class')+"' src='"+ $(ui).find('img').attr('src') +"' title='"+title
					+"'/></span>" + "<div class='text'>" + title + "<s></s></div></li>";
					//console.log($('#default_app').find('#'+ui.draggable[0].id));
				}
				//console.log(ui.draggable[0].id);
				if(!$('#default_app').find('#lrBar'+$(ui)[0].id).length){
					if(confirm("确认添加<"+$(this).find("img").attr("title")+">快捷方式？")){
						$(this).before(str);
						var shortcutid = [];
						$('#default_app li[id!=mydesk]').each(function(i){
							//console.log($(this)[0].id);
							shortcutid.push($(this)[0].id.replace(/lrBardeskIcon_/,""));
						});
						shortcutSave(shortcutid);
				    }
				}
				else{
					alert("已经添加该快捷方式");
				}
				myLib.desktop.lrBar.init();
            }
		})
		.hover(
			function(){$(this).addClass('btnOver');}
			,function(){$(this).removeClass('btnOver');
		});
		$default_app
		.find('li:not("#lrBardeskIconmydesk")')
		.draggable({
			proxy:'clone',
			cursor:'pointer',
			onBeforeDrag:function(event){
				$('#desktopInnerPanel').draggable("disable");
			},
			onStartDrag:function(event){
				dxStart=event.pageX;
				dyStart=event.pageY;
				//为了点击事件能够被触发，隐藏drag句柄
				var proxy = $(this).draggable('proxy');
                proxy.hide();
			},
			onDrag:function(){
            	$(this).draggable('proxy').show();
            },
			onStopDrag:function(event){
				$('#desktopInnerPanel').draggable("enable");
				dxEnd=event.pageX;
				dyEnd=event.pageY;
				var dx = dxEnd - dxStart;
				var dy = dyEnd - dyStart;
				if((dx*dx)+(dy*dy)>10000){
					if(confirm("确认删除<"+$(this).find("img").attr("title")+">快捷方式？")){
						$(this).remove();
						var shortcutid = [];
						$("#default_app li:not('#lrBardeskIconmydesk')").each(function(i){
							shortcutid.push($(this)[0].id.replace(/lrBardeskIcon_/,""));
						});
						shortcutSave(shortcutid);
				    }
				}
				myLib.desktop.lrBar.upLrBar();
			}
		});
		//开始按钮、菜单交互效果
		$start_btn.click(function(event){
		  event.preventDefault();
		  event.stopPropagation();
		  if($start_item.is(":hidden"))
		  $start_item.show();
		  else
		  $start_item.hide();
		  });
		
		$("body").click(function(event){
								 event.preventDefault();  
								 $start_item.hide();
									  });
		//全屏
		$("#bt_showZm").toggle(function(){
			myLib.fullscreenIE();	
			myLib.fullscreen();
		},function(){
			myLib.fullscreenIE();
			myLib.exitFullscreen();
		});
	}
};