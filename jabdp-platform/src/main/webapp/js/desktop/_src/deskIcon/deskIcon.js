//桌面图标区域
myLib.NS("desktop.deskIcon");
myLib.desktop.deskIcon = {
	//单个添加模块
	addIcon:function(data) {
		var _this=this;
		if(!$("#"+data.id).size()){
			var iconHtml = "<li class='desktop_icon' id='" + data.id+"'><span class='icon'><img src='" + data.iconSrc
			+ "'/></span><div class='text'>" + data.title + "<s></s></div></li>"; 
		$(iconHtml).insertBefore("ul.currDesktop .add_icon");
		_this.init();
		$("#"+data.id).data("iconData",{
			'title':data.title,
	        'url':data.url,
	        'winWidth':data.winWidth,
	        'winHeight':data.winHeight
			});
		}
	},
 	//桌面模块排列
	arrangeIcons:function(desktop) {
		var myData = myLib.desktop.getMydata(),
			winWh = myData.winWh,
			$navBar = myData.panel.navBar,
			navBarHeight = $navBar.outerHeight(),
			$li = desktop.find("li");
		//计算一共有多少图标
		//var iconNum = $li.size();
		var gH = 120;//一个图标总高度，包括上下margin
		var gW = 120;//图标总宽度,包括左右margin
		var rows = Math.floor((winWh['h'] - navBarHeight - 200) / gH);
		//var cols = Math.ceil(iconNum / rows);
		var curcol = 0;
		var currow = 0;
		//存储当前总共有多少桌面图标
		//desktop.data('deskIconNum',iconNum);
		$li.css({
			"position":"absolute",
			"margin":0,
			"left":function(index,value) {
				var v = curcol * gW + 30;
				if((index + 1) % rows === 0){
					curcol = curcol + 1;
				}
				return v;
 			},
			"top":function(index,value) {
 				var v = (index - rows * currow) * gH + 20;
				if((index + 1) % rows === 0){
					currow = currow + 1;
				}
				return v;
			}
 		})
 		.each(function(i) {
 			if($(this).find('.text').text().replace(/[^\x00-\xff]/g, '__').length <= 10) {
 				$(this).find('.text').css({
 					'margin-right':'10px',
 					'padding-left':'10px',
 		 			'height':'22px',
 		 			'line-height':'22px'//文字的间距
 		 		});
 				$(this).find('s').css({
 					'width':'10px',
 		 			'height':'22px',
 		 			'right':'-10px'
 		 		});
 			}
 			else {
 				$(this).find('.text').css({
 					'margin-right':'14px',
 					'padding-left':'12px'
 		 		});
 				$(this).find('s').css({
 					'width':'13px',
 		 			'height':'28px',
 		 			'right':'-13px'
 		 		});
 			}
 		});
	},
	//刷新位置
	upDesktop:function($deskIcon,$deskIconBlock,$innerPanel,$deskIconNum,navBarHeight,lBarWidth) {
		var myData=myLib.desktop.getMydata()
		    ,winWh=myData.winWh
		    ,w=winWh['w']-lBarWidth
		    ,h=winWh['h']-navBarHeight-125
		    ,_this=this;
		//获取当前是第几页
		var pageindex = $('body').data("pageIndex");
		//是否有第几页被记录，否则当前为第0页
		if(!pageindex) {
			pageindex = 0;
		}
		var left = - (w + lBarWidth) * pageindex;
		//设置桌面图标容器元素区域大小
		$innerPanel.css({"width":((w+lBarWidth)*$deskIconNum)+"px","height":h+"px",'left':left});
		//模块大小
		$deskIcon.css({"width":w+"px","height":h-50+"px",'margin-right':lBarWidth});
		//$("desktopPanel")
		$deskIconBlock.css({"width":w+"px","height":h+"px","margin-top":navBarHeight+50,'margin-left':lBarWidth+'px','margin-bottom':75+"px"});
		//排列模块
		$deskIcon.each(function(i){
			_this.arrangeIcons($(this));
		});
	},
	//桌面滚动，换到公用方法中
	desktopMove:function($innerPanel,$deskIcon,$navTab,dates,moveDx,nextIndex) {
		$("#applemenu img.desktop_icon_over_b").addClass("desktop_icon_over_c").removeClass("desktop_icon_over_b");
		$("#applemenu img:eq("+nextIndex+")").removeClass("desktop_icon_over_c").addClass("desktop_icon_over_b");
		$innerPanel.stop().animate({
			left:-(nextIndex)*moveDx
		},dates,function(){
			$deskIcon.removeClass("currDesktop").eq(nextIndex).addClass("currDesktop");
			$navTab.removeClass("currTab").eq(nextIndex).addClass("currTab");
 		});
		myLib.desktop.pageIndex(nextIndex);
	},
	//自动生成二级模块的字符串
	autodeskIconHtml:function(data) {
		var ctx = $("body").data("ctx");
		var str = "<div id='desktopInnerPanel'>";
		//需要注册事件的deskIconID
		for(var i = 0; i < data.length; i++) {
			var adata = data[i].childs;
			if(i===0) {
				str = str + "<ul _page='" + i + "' class='deskIcon currDesktop'>";
			}
			else {
				str = str + "<ul _page='" + i + "' class='deskIcon'>";
			}
			for(var j = 0; j < adata.length; j++) {
				//截掉长度超过10个中文的字
				var text = adata[j].name;
				var length = text.replace(/[^\x00-\xff]/g, '__').length;
				if(length > 20) {
					text = text.substring(0,9) + '...';
				}
				//如果有下拉菜单添加
				if(adata[j].childs.length) {
					str = str + "<li _id='" + adata[j].id
					+ "' class='desktop_icon' id='deskIcon_" + adata[j].id + "' title='" + adata[j].name + "'>"
					+ "<span class='icon'>"
					+ "<img src='" + ctx + "/js/desktop/themes/default/images/null.png' class = 'imgIcon " + adata[j].iconSkin + "-auto' />"
					+ "</span><div class='text'>" + text + "<s></s></div></li>";
				}
				else {
					str = str + "<li _id='" + adata[j].id + "' _url='" + ctx + adata[j].resourceUrl + "' _page='" + i + "' _name='" + adata[j].name + "' _iconCls='imgIcon " + adata[j].iconSkin
					+ "' class='desktop_icon' id='deskIcon_" + adata[j].id + "' title='" + adata[j].name + "'>"
					+ "<span class='icon'>"
					+ "<img src='" + ctx + "/js/desktop/themes/default/images/null.png' class = 'imgIcon " + adata[j].iconSkin + "-auto' />"
					+ "</span><div class='text'>" + text + "<s></s></div></li>";
				}
			}
			str = str + "</ul>";
		}
		str = str + "<ul class='deskIcon'><iframe src='" + ctx + "/personal-index-qq.action' style='height:"
		+ ($(window).height()-52)+"px;width:" + ($(window).width()-75) + "px;border-width:0px;position:relative;left:-30px;top:-20px;'></iframe></ul></div>";
		return str;
		$("#desktopPanel").append(str);
	},
	init:function(data) {
		var _this = this;
		//将html元素插入desktopPanel元素中
		$("#desktopPanel").append(_this.autodeskIconHtml(data));
		//将新加入的元素添加到存储桌面布局元素的jquery对象
		myLib.desktop.desktopPanel();
 		var myData = myLib.desktop.getMydata()
		    //,winWh = myData.winWh
			,$deskIconBlock = myData.panel.desktopPanel['_this']
			,$innerPanel = myData.panel.desktopPanel.innerPanel
			,$deskIcon = myData.panel.desktopPanel['deskIcon']
			,$deskIconNum = $deskIcon.size()
			,$navBar = myData.panel.navBar
			,navBarHeight = $navBar.outerHeight()
			,$navTab = $navBar.find("a")
			,lBarWidth = myData.panel.lrBar["_this"].outerWidth();
 		//排列二级模块图标
  		_this.upDesktop($deskIcon,$deskIconBlock,$innerPanel,$deskIconNum,navBarHeight,lBarWidth);
		//如果窗口大小改变，则重新排列图标
		$(window).wresize(function(){
			myLib.desktop.winWH();//更新窗口大小数据
			_this.upDesktop($deskIcon,$deskIconBlock,$innerPanel,$deskIconNum,navBarHeight,lBarWidth);
   		});
		//桌面可使用鼠标拖动切换
		var dxStart=0,dxEnd=0;//标记鼠标滑动的距离
		$innerPanel
		.draggable({
			cursor:"pointer",
			axis:'h',
			onStartDrag:function(event){
				dxStart = event.pageX;
			},
			onStopDrag:function(event){
				dxEnd = event.pageX;
				var dxCha=dxEnd-dxStart
					,currDesktop=$(this).find("ul.currDesktop")
					,deskIndex=$deskIcon.index(currDesktop)
					,moveDx=$deskIcon.width()+lBarWidth
					,dates = 500;//滚动屏幕动画的时间
				//左移	
				if(dxCha < -150 && deskIndex<$("#applemenu a").length-1){
 					_this.desktopMove($(this),$deskIcon,$navTab,dates,moveDx,deskIndex+1);
				//右移	 
 				}else if(dxCha > 150 && deskIndex>0){
 					_this.desktopMove($(this),$deskIcon,$navTab,dates,moveDx,deskIndex-1);
 				}else{
					$(this).animate({
						left:-(deskIndex)*moveDx
					},500);
				}
 			}
		});
		//键盘控制左右滚动
		$(window).keydown(function(event) {
			switch(event.keyCode) {
				case 37:
					if(deskIndex>0)
					_this.desktopMove($innerPanel,$deskIcon,$navTab,500,moveDx,deskIndex-1);
					break;
				case 39:
					if(deskIndex<$("#applemenu a").length-1)
					_this.desktopMove($innerPanel,$deskIcon,$navTab,500,moveDx,deskIndex+1);
					break;
			}
		});
		//图标鼠标经过效果
		$deskIcon.find("li").hover(function(){
			$(this).addClass("desktop_icon_over");
		}, function(){
			$(this).removeClass("desktop_icon_over");
		})
		.dblclick(function(event) {
			//双击图标打开窗口
			if($(this).attr("_url")) {
				var option = {
					"WindowTitle":$(this).attr("_name"),
					"iframSrc":$(this).attr("_url"),
					"WindowsId":$(this).attr("_id"),
					"WindowAnimation":'none',
					"imgclass":$(this).attr("_iconCls"),
					"pageindex":$(this).attr('_page')
				};
				myLib.desktop.win.newWin(option);
			}
			//双击打开菜单
			else {
				$("#menu_" + $(this).attr("_id")).menu("show",{left:$(this).offset().left+$(this).width(),top:$(this).offset().top+10});
			}
 		 })
 		 .draggable({
 			 cursor:"pointer",
 			 edge:10,//可以在其中拖动的容器的宽度
	 		 revert:true,
	 		 proxy:"clone",
			 onBeforeDrag:function(event) {
				 $innerPanel.draggable("disable");
			 },
			 onStartDrag:function(event) {
                 var proxy = $(this).draggable('proxy').css('z-index', 10);
                 proxy.hide();
			 },
			 onDrag:function(){
            	 $(this).draggable('proxy').show();
             },
			 onStopDrag:function(event) {
				 $innerPanel.draggable("enable");
			 }
		 });
		 //初始化桌面右键菜单
//		 var data=[
//					[{
//					text:"显示桌面",
//					func:function(){}
//						}]
//					,[{
//					text:"系统设置",
//					func:function(){}
//					  },{
//					text:"主题设置",
//					func:function(){}
//						  }]
//					,[{
//					  text:"退出系统",
//					  func:function(){} 
//					  }]
//					,[{
//					  text:"关于fleiCms",
//					  func:function(){} 
//					  }]
//					];
//		 myLib.desktop.contextMenu($(document.body),data,"body",10);
	}
};
