define("pages/index", ["jquery", "common", "protocol", "widgets/widgets", "dialog"],
function(a) {
    function b(a, b, c) {
        var e = parseInt(a, 0),
        f = 0,
        g = 0,
        h = 0;
        e > 60 && (f = parseInt(e / 60, 0), e = parseInt(e % 60, 0), f > 60 && (g = parseInt(f / 60, 0), f = parseInt(f % 60, 0)), g > 24 && (h = parseInt(g / 24, 0), g = parseInt(g % 24, 0)));
        var i = "";
        i = h > 0 ? "" + parseInt(h, 0) + "天" + parseInt(g, 0) + "时": g > 0 ? "" + parseInt(g, 0) + "时" + parseInt(f, 0) + "分": parseInt(f, 0) + "分" + parseInt(e, 0) + "秒",
        "0" === c && (i += "后预定"),
        "3" === c && (i += "后加入"),
        d(b).html(i)
    }
    function c(a) {
        if (d(a).length <= 0 || "3" !== d(a).attr("data2") && "0" !== d(a).attr("data2") || "-1" === d(a).attr("data1")) return ! 1;
        var c = d(a).attr("data1"),
        e = setInterval(function() {
            c -= 1,
            b(c, a, d(a).attr("data2")),
            0 >= c && (clearInterval(e), location.reload())
        },
        1e3)
    }
    var d = a("jquery"),
    e = a("common"),
    f = a("protocol"),
    g = a("widgets/widgets"),
    h = a("dialog");
    window.parent != window && (window.top.location.href = location.href); !! (navigator.mimeTypes["application/x-shockwave-flash"] || window.ActiveXObject && new ActiveXObject("ShockwaveFlash.ShockwaveFlash"));
    d(function() {
        var a = d("#plan-status-open");
        new g.Slider,
        d("#openweixin").length && d("#weixin").length && new h({
            trigger: "#openweixin",
            width: "350px",
            content: d("#weixin")
        }),
        a.is(":visible") && d(".ui-plan-latest ").on("mouseenter",
        function() {
            a.find(".plan-progress").hide(),
            a.find("a").show()
        }).on("mouseleave",
        function() {
            a.find(".plan-progress").show(),
            a.find("a").hide()
        })
    }),
    d("#notice-title").length > 0 && d("#notice-title").click(function(a) {
        var b = d("#notice-content");
        b.is(":visible") ? (d(this).removeClass("rrdcolor-blue-text"), b.slideUp(200)) : (d(this).addClass("rrdcolor-blue-text"), b.slideDown(200)),
        a.preventDefault()
    })
});