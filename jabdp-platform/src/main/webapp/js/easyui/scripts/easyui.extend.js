/**
 * 回收iframe内存
 */
$.fn.panel.defaults.onBeforeDestroy = function() {
    var frame = $('iframe', this);  
    if (frame.length > 0) {  
        frame[0].contentWindow.document.write('');  
        frame[0].contentWindow.close();  
        frame.remove();  
        if ($.browser.msie) {  
            CollectGarbage();  
        }  
    }  
};


/**
 * 扩展datagrid编辑器，增加my97datepicker支持
 */
$.extend($.fn.datagrid.defaults.editors, {
	my97 : {
		init : function(container, options) {
			var dateFmt = (options && options.dateFmt) || "yyyy-MM-dd";
			var input = null;
			if(options && options.readonly) {
				input = $('<input />').appendTo(container);
				input.attr("readonly", "readonly");
			} else {
				input = $('<input class="Idate Wdate" onfocus="WdatePicker({dateFmt:\'' + dateFmt + '\'' 
					+ (options.minDate && options.minDate != '' ? ',minDate:\'' + options.minDate + '\'' : '')
					+ (options.maxDate && options.maxDate != '' ? ',maxDate:\'' + options.maxDate + '\'' : '')
					+ '});" />').appendTo(container);
			}
			if(options && options["onkeydown"]) { //加入回车事件支持
				input.bind("keydown", options["onkeydown"]);
			}
			input.data({"dateFmt":dateFmt});
			//var option = $.extend({}, options);
			//input.validatebox(option);
			return input;
		},
		getValue : function(target) {
			return $(target).val();
		},
		setValue : function(target, value) {
			if(value) {
				var filter = "[0-9]{4}-[0-9]{2}-[0-9]{2}\\s[0-9]{2}:[0-9]{2}:[0-9]{2}";
				if((new RegExp(filter)).test(value)) {//若数据类型为datetime第一次编辑，数据库标准时间格式，再次编辑时时间格式可能发生改变
					var dateFmt = target.data("dateFmt");
					var xd = new XDate(value);
					$(target).val(xd.toString(dateFmt));
				} else {
					$(target).val(value);
				}
			} else {
				$(target).val(value);
			}
		},
		resize : function(target, width) {
			var input = $(target);
			if(!$.boxModel&&$.browser.msie){
				input.width(width);
			}else{
				input.width(width-(input.outerWidth()-input.width()));
			}
		},destroy: function(target){
			if($dp && $dp.hide) {
				$dp.hide();
			}
			$(target).remove();
		}
	}
});





/**扩展datagrid编辑器，增加combo单选支持*/

$.extend($.fn.datagrid.defaults.editors,{
	comboradio:{
		  init: function(container, options){
			  var sel=$('<input/>').appendTo(container);		  
			  /*sel.comboradio({
					data:options.data,
					valueField:options.valueField,
					textField:options.textField
				});*/
			  sel.comboradio(options);
			  return sel;
		  	},		 
			  /* var divs = $('<div></div>').appendTo(container);
			   sel.combo({
				    //required:true,
					//editable:false
				});
			   var data = options.data;
			   var valueField = options.valueField;
			   var textField = options.textField;
			   
			   if(data && data.length){
				   var str="";
					$.each(data,function(k,v){
						  str +="<div class='combobox-item' ><input name='rad' type='radio' value='"+v[valueField]+"'/>"+"<span>"+v[textField]+"</span>"+"</div>"; 						 
					});
					 divs.append(str);
					   divs.appendTo(sel.combo('panel'));
					   divs.find("input[type=radio]").click(function(){
						   var vv=sel.combo('getValue');
						   var panel = sel.combo('panel');
							var input = panel.find("input[type=radio][value="+ vv+ "]");
							if(input){
							var ss = input.next('span').text();
							var div = input.parent();
							var v = $(this).val();
							var s = $(this).next('span').text();
							var div2 = $(this).parent();
							if(vv==v){
								div.addClass("combobox-item-selected");
								sel.combo('setValue', vv).combo('setText', ss);
							}else{
								div2.addClass("combobox-item-selected");
								div.removeClass("combobox-item-selected");
								sel.combo('setValue', v).combo('setText', s);	
							}
					   }
						});
			   }
			   sel.combo({"keyHandler":{
				 up:function(){
					var panel = $(this).combo('panel');
					var div=  panel.find("div.combobox-item-selected");
					if(div && div.length>0){
						var divPrev = div.prev("div.combobox-item"); 
						if(divPrev && divPrev.length>0){
							var input = divPrev.find("input[type=radio]");	
							input.trigger("click");  	
						}					
						}else{
							var divs = panel.find("div.combobox-item");
							var input = $(divs[0]).find("input[type=radio]");
							input.trigger("click");  	
						}
				},down:function(){
					var panel = $(this).combo('panel');
					var div=  panel.find("div.combobox-item-selected");
					if(div && div.length>0){
						var divNext= div.next("div.combobox-item"); 
						if(divNext && divNext.length>0){
							var input = divNext.find("input[type=radio]");	
							input.trigger("click");  	
						}					
						}else{
							var divs = panel.find("div.combobox-item");
							var input = $(divs[0]).find("input[type=radio]");
							input.trigger("click");  	
						}
			 	},enter:function(){			 		
				}
		  		}
	 		});
			   return sel;
			   
		  },*/
			  destroy: function(target){
				  $(target).comboradio('destroy');
			  },
			  getValue: function(target){
				return $(target).comboradio('getValue');			  
			  },
			  setValue: function(target, value){	
				  $(target).comboradio('setValue',value);
			  },			  
			  resize: function(target, width){
				  $(target).comboradio('resize', width);
		  }
	}
});


/**洛湛datagrid编辑器，增加combo多选支持*/

$.extend($.fn.datagrid.defaults.editors,{
	combocheck:{
		 init: function(container, options){
		  var sel=$('<input/>').appendTo(container);		  
			  /*sel.combocheck({
					data:options.data,
					valueField:options.valueField,
					textField:options.textField
				});*/
			  sel.combocheck(options);
			  return sel;
		  },
			/*  var sel=$('<select/>').appendTo(container);
			   var divs = $('<div></div>').appendTo(container);
			   var data = options.data;
			   var valueField = options.valueField;
			   var textField = options.textField;
			   if(data && data.length){
				   var str="";
					$.each(data,function(k,v){
						  str +="<div class='combobox-item' ><input  type='checkbox' value='"+v[valueField]+"'/>"+"<span>"+v[textField]+"</span>"+"</div>"; 					  
					});
					divs.append(str);
					 sel.combo({
						    //required:true,						
						});
					 divs.appendTo((sel).combo('panel'));
					 
					 divs.find("input[type=checkbox]").click(function(){	
						 var ss=[];
						var panel = sel.combo('panel');
						var vv =sel.combo("getValues");//得到选中的值
						vv=$.map(vv,function(n){
							return n=="" ? null : n;
						});
					$.each(vv,function(k,v){
							var input = panel.find("input[type=checkbox][value="+ v+ "]");  
							if(input && input.length>0){
							var s = input.next('span').text();
							ss.push(s);//得到选中的文本值
							}					
					});	
					var v= $(this).val();	
					var input = panel.find("input[type=checkbox][value="+ v+ "]");  
					var s = input.next('span').text();
					var div = $(this).parent();
					if($.inArray(v, vv)>=0){
						vv=$.map(vv,function(n){
							return n==v ? null : n;
						});
						ss=$.map(ss,function(n){
							return n==s ? null : n;
						});		
						div.removeClass("combobox-item-selected");
					}else{
						ss.push(s);
						vv.push(v);	
						div.addClass("combobox-item-selected");
					}
					sel.combo('setValues',vv ).combo("setText",ss.join(","));						 											 
						});
			 }
			   sel.combo({"keyHandler":{
					 up:function(){
						var panel = $(this).combo('panel');
						var div=  panel.find("div.combobox-item-selected");
						if(div && div.length>0 ){					
							var divPrev = $(div[0]).prev("div.combobox-item"); 
							if(divPrev && divPrev.length>0){
								div.each(function(){
									var input = $(this).find("input[type=checkbox]");	
									input.trigger("click");
								});
								var input = divPrev.find("input[type=checkbox]");	
								input.trigger("click");  	
								}					
							}
						else{
							  var divs = panel.find("div.combobox-item");
								var input = $(divs[0]).find("input[type=checkbox]");
								input.trigger("click");   
						  }
					},down:function(){
						var panel = $(this).combo('panel');
						var div=  panel.find("div.combobox-item-selected");
						if(div && div.length>0){
							var divNext= $(div[div.length-1]).next("div.combobox-item"); 
							if(divNext && divNext.length>0){
								div.each(function(){
									var input = $(this).find("input[type=checkbox]");	
									input.trigger("click");
								});
								var input = divNext.find("input[type=checkbox]");	
								input.trigger("click");  	
							}					
							}
						else{
							  var divs = panel.find("div.combobox-item");
								var input = $(divs[0]).find("input[type=checkbox]");
								input.trigger("click");   
						  }
				 	},enter:function(){			 		
					}
			  		}
		 		});
			   return sel;
			  },*/
			  destroy: function(target){
				 $(target).combocheck('destroy');
			  },
			/*  getValues: function(target){
				  var values=$(target).combo('getValues');
				  if($.isArray(values)){
					  return values.join(",");
				  }else{
					  return values;
				  }		
			  },*/
			 getValue: function(target){
				var values =  $(target).combocheck('getValues');
				  if($.isArray(values)){
					  return values.join(",");
				  }else{
					  return values;
				  }		
				},
			 /* setValues: function(target, value){				  
				  $(target).combocheck('setValues',value);
				  var panel = $(target).combo('panel');
				  if(value!=undefined && value!=null && value!="") {	
					  var values = value.split(",");
					  if($.isArray(values)){
						 $.each(values,function(k,v){
						var input = panel.find("input[type=checkbox][value="+ v+ "]"); 
						input.trigger("click");
						 });						 
					  }else{
						  var input = panel.find("input[type=checkbox][value="+ value+ "]"); 
						  input.trigger("click");  						 
					  }				
				  }
			  },*/
			  setValue: function(target, value){
				  if(value){
					 var values = value.split(",");
					 $(target).combocheck('setValues',values);
				  } else {
					 $(target).combocheck('setValues',[]); 
				  }
			  },
			  resize: function(target, width){
				  $(target).combocheck('resize', width);
		  }
	}
});

/** 扩展datagrid编辑器，增加combogrid支持
 * 
 */
 
$.extend($.fn.datagrid.defaults.editors, {
	combogrid: {
	  init: function(container, options){
	   var inp = $('<input type="text" />').appendTo(container);
	   $.extend(options, {
		   onClickRow:function(rowIndex, rowData){
		    	var selRows=inp.combogrid("grid").datagrid("getSelections"); 
		    	if($.inArray(rowData, selRows)>=0){
		    		inp.combogrid("grid").datagrid("unselectRow", rowIndex); 
		        }else{
		        	inp.combogrid("grid").datagrid("selectRow", rowIndex); 
		    	}
		    }
		   
	   });
	   var cg = inp.combogrid(options);
	   cg.combogrid("grid").datagrid("loadData", options.data); 
	   return cg;
	  },
	  destroy: function(target){
	   $(target).combogrid('destroy');
	  },
	  getValue: function(target){
		 var values=$(target).combogrid('getValues');
		  if($.isArray(values)){
			  return values.join(",");
		  }else{
			  return values;
		  }
	  },
	  setValue: function(target, value){
		  if(value!=undefined && value!=null) {
			  var values = value.split(",");
			  if($.isArray(values)){
					$(target).combogrid('setValues',values);
			  }else{
				    $(target).combogrid('setValue',value);
			  }
		  }
	  },
	  resize: function(target, width){
	   $(target).combogrid('resize',width);
	  }
	}
});


$.extend($.fn.datagrid.defaults.editors, {
	button: {
	  init: function(container, options){
	   var inp = $('<input type="text" />').appendTo(container);
	   
	   $.extend(options, {
		   //多选也支持
		   multiple:true,
		   onClickRow:function(rowIndex, rowData){
		    	var selRows=inp.combogrid("grid").datagrid("getSelections"); 
		    	if($.inArray(rowData, selRows)>=0){
		    		inp.combogrid("grid").datagrid("unselectRow", rowIndex); 
		        }else{
		        	inp.combogrid("grid").datagrid("selectRow", rowIndex); 
		    	}
		    }
		   
	   });
	   var cg = inp.combogrid(options);
	   cg.combogrid("grid").datagrid("loadData", options.data); 
	   return cg;
	  },
	  destroy: function(target){
	   $(target).combogrid('destroy');
	  },
	  getValue: function(target){
		 var values=$(target).combogrid('getValues');
		  if($.isArray(values)){
			  return values.join(",");
		  }else{
			  return values;
		  }
	  },
	  setValue: function(target, value){
		  if(value!=undefined && value!=null) {
			  var values = value.split(",");
			  if($.isArray(values)){
					$(target).combogrid('setValues',values);
			  }else{
				    $(target).combogrid('setValue',value);
			  }
		  }
	  },
	  resize: function(target, width){
	   $(target).combogrid('resize',width);
	  }
	}
});

/**
 * 扩展EasyUI的validatebox验证方法
 * 
 */

$.extend($.fn.validatebox.defaults.rules, { 
    equalTo: {
        validator:function(value,param){
            return $(param[0]).val() == value;
        },
        message:'两次输入的密码不一致'
    }
           
});

/**
 * 扩展EasyUI的验证方法，验证手机号
 * 
 */
$.extend($.fn.validatebox.defaults.rules,{
        mobile:{
            validator:function(value,param){
                return /^([1-9][0-9][0-9]|15[0|1|2|3|6|7|8|9]|18[6|8|9])\d{8}$/.test(value);
            },
            message:'请输入正确的11位手机号码.格式:13120002221'
        },
        postcode:{
	        validator:function(value,param){
	            return /^\d{6}$/.test(value);
	        },
	        message:'请输入正确的6位邮政编码'
       }
 });

/**
 * 扩展EasyUI的验证方法，校验唯一性
 * 
 */
$.extend($.fn.validatebox.defaults.rules, {
    unique: {
        validator:function(value,param){
        	var flag = true;
        	/*if(!value) {
        		return flag;
        	}*/
        	var url = param[0];
        	var key = param[1];
        	var id = $("#id").val();
        	var data = {};
        	data["id"] = id;
        	if(param.length >=3) {
        		var field = param[2];
        		data[key] = jwpf.getFormVal(field);
        	} else {
        		data[key] = value;
        	}
        	if(data[key]!==undefined && data[key]!==null && data[key]!=="") {
        		var options = {
            			url : url,
                        data : data,
        				async: false,
        				success : function(data) {
        					if(data.msg) {
        						flag = true;
        					} else {
        						flag = false;
        					}
        				}
        		};
        		fnFormAjaxWithJson(options, true);
        	}
            return flag;
        },
        message:'该值已存在，请确保唯一性'
    }      
});

/**
 * 扩展EasyUI的验证方法，校验唯一性
 * 
 */
$.extend($.fn.validatebox.defaults.rules, {
    uniqueSub: {
        validator:function(value,param, obj){
        	var flag = true;
        	if(!value) {
        		return flag;
        	}
        	var url = param[0];
        	var key = param[1];
        	var tableKey = param[2];
        	var idAttr = param[3];
        	var rowIndex = $(obj).closest("td[field]").parent().attr("datagrid-row-index");
        	rowIndex = parseInt(rowIndex);
        	var rows = $("#"+tableKey).datagrid('getRows');
        	var row = rows[rowIndex];
        	var data = {};
        	data["id"] = row[idAttr];
        	data[key] = value;
        	if(param.length >= 5) {
        		data["masterId"] = $("#id").val();
        	}
        	var options = {
        			url : url,
                    data : data,
    				async: false,
    				success : function(data) {
    					if(data.msg) {
    						flag = true;
    					} else {
    						flag = false;
    					}
    				}
    		};
    		fnFormAjaxWithJson(options, true);
            return flag;
        },
        message:'该值已存在，请确保唯一性'
    }      
});

/**
 * 扩展EasyUI的验证方法，验证传真
 * 
 */

$.extend($.fn.validatebox.defaults.rules,{
	faxno : {// 验证传真 
	    validator : function(value) { 
	        return /^((\(\d{2,3}\))|(\d{3}\-))?(\(0\d{2,3}\)|0\d{2,3}-)?[1-9]\d{6,7}(\-\d{1,4})?$/i.test(value); 
	    }, 
	    message : '传真号码不正确' 
	}
});

/**
 * 多重校验规则
 */
$.extend($.fn.validatebox.defaults.rules, {
    multiple: {
        validator: function (value, vtypes) {
            var returnFlag = true;
            var opts = $.fn.validatebox.defaults;
            for (var i = 0; i < vtypes.length; i++) {
                var methodinfo = /([a-zA-Z_]+)(.*)/.exec(vtypes[i]);
                var rule = opts.rules[methodinfo[1]];
                if (value && rule) {
                    var parame = eval(methodinfo[2]);
                    if (!rule["validator"](value, parame)) {
                        returnFlag = false;
                        this.message = rule.message;
                        break;
                    }
                }
            }
            return returnFlag;
        },
	    message: ''
    }
});

/** 
 * 扩展datagrid编辑器，增加combotree的keyHandler支持
 */

$.fn.getComboTreeData = function(root,dataArr){
	for (var k = 0; k< root.length; k++) {			
		dataArr.push(root[k]);
		var child =$(this).tree('getChildren', root[k].target);
		if(child && child.length>0){				
			$(this).getTreeData(child,dataArr);
			}
		}
};
$.fn._combotree = function (arr,treeData){
	var panel=$(this).combo("panel");
	panel.find("div.tree-node-selected").removeClass("tree-node-selected");
	var vv=[],ss=[];
	for(var i=0;i<arr.length;i++){
	var v=arr[i];
	var s=v;
	for(var j=0;j<treeData.length;j++){
	if(treeData[j].text==v){
	s=treeData[j].id;
	break;
	}
	}
	vv.push(v);
	ss.push(s);
	}
	$(this).combo("setValue",vv);	
};
$.extend($.fn.combotree.defaults, {
	keyHandler : {
		up : function() {
			var _f = $.data(this, "combotree").options;
			var _10 = $.data(this, "combotree").tree;
			var panel = $(this).combo('panel');
			var divs = panel.find("div.tree-node");
			var vv = [], ss = [];
			var _12 = _10.tree("getSelected");
			if (_12) {
				var v = _12.id;
				v = new String(v);
				for ( var i = 0; i < divs.length; i++) {
					var dv = $(divs[i]).attr("node-id");
					if (dv == v) {
						if (divs[i - 1]) {
							v = $(divs[i - 1]).attr("node-id");
							var _18 = _10.tree("find", v);
							if (_18) {
								var s = _18.text;
								_10.tree("select", _18.target);
								vv.push(v);
								ss.push(s);
								$(this).combo("setValue", vv).combo("setText",
										ss.join(_f.separator));
							}
						}
						break;
					}
				}
			} else {
				var _12 = $(divs[0]);
				var v = _12.attr("node-id");
				var _18 = _10.tree("find", v);
				if (_18) {
					var s = _18.text;
					_10.tree("select", _18.target);
					vv.push(v);
					ss.push(s);
					$(this).combo("setValue", vv).combo("setText",
							ss.join(_f.separator));
				}
			}
		},
		down : function() {
			var _f = $.data(this, "combotree").options;
			var _10 = $.data(this, "combotree").tree;
			var panel = $(this).combo('panel');
			var divs = panel.find("div.tree-node");
			var vv = [], ss = [];
			var _12 = _10.tree("getSelected");
			if (_12) {
				var v = _12.id;
				v = new String(v);
				for ( var i = 0; i < divs.length; i++) {
					var dv = $(divs[i]).attr("node-id");
					if (dv == v) {
						if (divs[i + 1]) {
							v = $(divs[i + 1]).attr("node-id");
							var _18 = _10.tree("find", v);
							if (_18) {
								var s = _18.text;
								_10.tree("select", _18.target);
								vv.push(v);
								ss.push(s);
								$(this).combo("setValue", vv).combo("setText",
										ss.join(_f.separator));
							}
						}
						break;
					}
				}
			} else {
				var _12 = $(divs[0]);
				var v = _12.attr("node-id");
				var _18 = _10.tree("find", v);
				if (_18) {
					var s = _18.text;
					_10.tree("select", _18.target);
					vv.push(v);
					ss.push(s);
					$(this).combo("setValue", vv).combo("setText",
							ss.join(_f.separator));
				}
			}  
		},enter:function(){
		},
		query : function(q) {
			var opts = $.data(this,"combotree").options;
			var panel = $(this).combo("panel");
			var treeObj = $(this).combotree("tree");
			var treeData = [];
			var roots = treeObj.tree("getRoots");
			$(this).getComboTreeData(roots,treeData);

			$(this)._combotree([ q ],treeData);

			panel.find("div.tree-node").hide();
			for ( var i = 0; i < treeData.length; i++) {
				var bl = treeData[i].text.indexOf(q) >= 0;
				if (bl) {
					var v = treeData[i].id;
					var s = treeData[i].text;
					var div = panel.find("div.tree-node[node-id=" + v + "]");
					div.show();
					if (s == q) {
						$(this)._combotree([q],treeData);
						div.addClass("tree-node-selected");
					}
				}
			}
		}
	}
});


/**
 * 扩展combo编辑器，增加ComboRadio支持
 */
(function($){
		
	$.fn.comboradio = function(options, param){//初始化方法
		if (typeof options == 'string'){
			var opt=$.fn.comboradio.methods[options];
			if(opt){
			return opt(this,param);
			}else{
			return this.combo(options,param);
			}
		}		
		options = options || {};		
		options.multiple= false;
		return this.each(function(){
			var state=$.data(this,"comboradio");
			if(state){
				$.extend(state.options,options);			
			}else{
				state = {options:$.extend({},$.fn.comboradio.defaults,
						$.fn.comboradio.parseOptions(this),options)};
				$.data(this,"comboradio",state);	
			}			
			var tag= $(this);
			tag.addClass("comboradio-f");
			tag.combo(state.options);//??? 初始化继承combo

			var panel = $(this).combo("panel");
			var divs = $('<div></div>').appendTo(panel);
			var data =  state.options.data;
			var valueField =  state.options.valueField;
			var textField =  state.options.textField;		
			var id = $(this).attr("id");
			if(data && data.length){
				var str="";
				$.each(data,function(k,v){
				 str +="<div class='comboradio-item' ><input   name ='"+id+"'  type='radio' value='"+v[valueField]+"'/>"+"<span>"+v[textField]+"</span>"+"</div>"; 							 
					});
					divs.append(str);
					 divs.appendTo(panel);
					 
					 $(".comboradio-item",panel).hover(function(){
						 $(this).addClass("combobox-item-hover");
					 },function(){
						 $(this).removeClass("combobox-item-hover");
					}).find("input[type=radio]").click(function(){
							   var vv=tag.combo('getValue');
								var input = panel.find("input[type=radio][value="+ vv+ "]");
								if(input){
								var ss = input.next('span').text();
								var div = input.parent();
								
								var v = $(this).val();
								var s = $(this).next('span').text();
								var div2 = $(this).parent();
								if(vv==v){
									div.addClass("combobox-item-selected");
									tag.combo('setValue', vv).combo('setText', ss);
								}else{
									div2.addClass("combobox-item-selected");
									div.removeClass("combobox-item-selected");
									tag.combo('setValue', v).combo('setText', s);	
								}
							}
											
						});
				   }									  
			});
	
	};
	
$.fn.comboradio.parseOptions = function(target){
		var t=$(target);
		return $.extend({},$.fn.combo.parseOptions(target),{valueField:t.attr("valueField"),textField:t.attr("textField"),mode:t.attr("mode"),method:(t.attr("method")?t.attr("method"):undefined),url:t.attr("url")});
	};
	
function _radio(obj,arr){
	var panel=$(obj).combo("panel");
	var opts=$.data(obj,"comboradio").options;
	var allData=opts.data;
	panel.find("div.combobox-item-selected").removeClass("combobox-item-selected");
	var vv=[],ss=[];
	for(var i=0;i<arr.length;i++){
	var v=arr[i];
	var s=v;
	for(var j=0;j<allData.length;j++){
	if(allData[j][opts.valueField]==v){
	s=allData[j][opts.textField];
	break;
	}
	}
	vv.push(v);
	ss.push(s);
	panel.find("input[type=radio][value="+ v+ "]").parent().addClass("combobox-item-selected");
	}
	$(this).combo("setValue",vv);
}	
$.fn.comboradio.defaults = $.extend({},$.fn.combo.defaults,{//属性和事件
	valueField:"value",
	textField:"text",
	mode:"local",
	method:"post",
	url:null,
	data:null,
	keyHandler:{
		 up:function(){
				var panel = $(this).combo('panel');
				var div=  panel.find("div.combobox-item-selected");
				if(div && div.length>0){
					var divPrev = div.prev("div.comboradio-item"); 
					if(divPrev && divPrev.length>0){
						var input = divPrev.find("input[type=radio]");	
						input.trigger("click");  	
					}					
					}else{
						var divs = panel.find("div.comboradio-item");
						var input = $(divs[0]).find("input[type=radio]");
						input.trigger("click");  	
					}

			},
			down:function(){
				var panel = $(this).combo('panel');
				var div=  panel.find("div.combobox-item-selected");
				if(div && div.length>0){
					var divNext= div.next("div.comboradio-item"); 
					if(divNext && divNext.length>0){
						var input = divNext.find("input[type=radio]");	
						input.trigger("click");  	
					}					
					}else{
						var divs = panel.find("div.comboradio-item");
						var input = $(divs[0]).find("input[type=radio]");
						input.trigger("click");  	
					}
		 	},
		 	enter:function(){	
		 		var val=$(this).comboradio("getValue");
		 		$(this).comboradio("setValue",val);
		 		$(this).combo("hidePanel");
			},
			query:function(val){
				var opts=$.data(this,"comboradio").options;
				var panel=$(this).combo("panel");
				var allData=opts.data;
				_radio(this,[val]);
				
				panel.find("div.comboradio-item").hide();
				for(var i=0;i<allData.length;i++){
				var bl = allData[i][opts.textField].indexOf(val)>=0;
				if(bl){
				var v=allData[i][opts.valueField];
				var s=allData[i][opts.textField];
				var input=panel.find("input[type=radio][value="+ v+ "]");
				var div = input.parent();
				div.show();
				if(s==val){
				_radio(this,[val]);
				div.addClass("combobox-item-selected");
				input.trigger("click");  	
				}			
				}
				}
			}
			
		}
});

$.fn.comboradio.methods={
	options:function(jq){
		return $.data(jq[0],"comboradio").options;
	},
	getData:function(jq){
		return $.data(jq[0],"comboradio").data;
	},
	setValue: function(jq, value){	
		return jq.each(function() {
		 	  var panel = $(this).combo('panel');
			  if(value!=undefined && value!=null) {									
				  var input = panel.find("input[type=radio][value="+ value+ "]"); 
				  input.trigger("click");  							  
			  }
		});
	  },
	getValue: function(target){		
		  var value=$(target).combo('getValue');
			  return value;	  
	 },
	 clear:function(jq){
		return jq.each(function(){
				$(this).combo("clear");
			var p=$(this).combo("panel");
				p.find("div.combobox-item-selected").removeClass("combobox-item-selected");
		});
	},
	  destroy: function(jq){
		  return jq.each(function() {
			  $(this).combo("hidePanel");
			  $(this).remove(); 
		  });
	  },
	  resize: function(jq, width){
		  return jq.each(function() {
			  $(this).combo("resize", width); 
		  });
	  }
	};

})(jQuery);

/**
 * 扩展combo编辑器，增加ComboCheck支持
 */
(function($){		
	$.fn.combocheck = function(options, param){//初始化方法
		if (typeof options == 'string'){
			var opt=$.fn.combocheck.methods[options];
			if(opt){
			return opt(this,param);
			}else{
			return this.combocheck(options,param);
			}
		}		
		options = options || {};
		options.multiple= true;
		return this.each(function(){			
			var state=$.data(this,"combocheck");
			if(state){
				$.extend(state.options,options);			
			}else{
				state = {options:$.extend({},$.fn.combocheck.defaults,
						$.fn.combocheck.parseOptions(this),options)};
				$.data(this,"combocheck",state);		
			}		
			var tag= $(this);
			tag.addClass("combocheck-f");
			tag.combo(state.options);
						
			var panel = $(this).combo("panel");
			var divs = $('<div></div>').appendTo(panel);
			var data = state.options.data;
			var valueField = state.options.valueField;
			var textField =state.options.textField;		
			var id = $(this).attr("id");
			if(data && data.length){
				var str="";
				$.each(data,function(k,v){
				 str +="<div class='combocheck-item' ><input name ='"+id+"'  type='checkbox' value='"+v[valueField]+"'/>"+"<span>"+v[textField]+"</span>"+"</div>"; 							 
					});
					divs.append(str);
					divs.appendTo(panel);				 
					 $(".combocheck-item",panel).hover(function(){
						 $(this).addClass("combobox-item-hover");
					 },function(){
						 $(this).removeClass("combobox-item-hover");
					 }).find("input[type=checkbox]").click(function(){
						var ss=[];					
						var vv =tag.combo("getValues");//得到选中的值
							vv=$.map(vv,function(n){
								return n=="" ? null : n;
							});
//						$.each(vv,function(k,v){
//							var input = panel.find("input[type=checkbox][value="+ v+ "]");  
//							if(input && input.length>0){
//								var s = input.next('span').text();
//								ss.push(s);//得到选中的文本值
//							}
//						});	
						
						for(var l=0; l<vv.length;) {
							var input = panel.find("input[type=checkbox][value='" + vv[l] + "']");
							if(input && input.length>0){
								var s = input.next('span').text();
								ss.push(s);//得到选中的文本值
								l++;
							} else {
								vv.splice(l,1);
							}
						}
						
						var v= $(this).val();	
						var input = panel.find("input[type=checkbox][value='"+ v+ "']");  
						var s = input.next('span').text();
						if($.inArray(v, vv)>=0){
							vv=$.map(vv,function(n){
								return n==v ? null : n;
							});
							ss=$.map(ss,function(n){
								return n==s ? null : n;
							});		
						}else{
							ss.push(s);
							vv.push(v);	
						}
						if(vv.length == 0) {
							vv = [""];
						}
						tag.combo('setValues',vv);
						tag.combo("setText",ss.join(","));
					});
			
					 $(this).next().find("input.combo-text").bind('keydown','space',function (e) {	 
						var div= panel.find("div.combobox-item-selected");
							if(div && div.length>0 ){		 
							var input =$(div[0]).find("input[type=checkbox]"); 
								$(input[0]).trigger("click");						
							}
							return false;							 
				 });
			}	
		});	
	};
	
$.fn.combocheck.parseOptions = function(target){
		var t=$(target);
		return $.extend({},$.fn.combo.parseOptions(target),{valueField:t.attr("valueField"),textField:t.attr("textField"),mode:t.attr("mode"),method:(t.attr("method")?t.attr("method"):undefined),url:t.attr("url")});
	};
function _check(obj,arr){
		var panel=$(obj).combo("panel");
		var opts=$.data(obj,"combocheck").options;
		var allData=opts.data;
		panel.find("div.combobox-item-selected").removeClass("combobox-item-selected");
		var vv=[],ss=[];
		for(var i=0;i<arr.length;i++){
		var v=arr[i];
		var s=v;
		for(var j=0;j<allData.length;j++){
		if(allData[j][opts.valueField]==v){
		s=allData[j][opts.textField];
		break;
		}
		}
		vv.push(v);
		ss.push(s);
		panel.find("input[type=checkbox][value='"+ v+ "']").parent().addClass("combobox-item-selected");
		}
		$(obj).combo("setValues",vv);
}
$.fn.combocheck.defaults = $.extend({},$.fn.combo.defaults,{//属性和事件
	valueField:"value",
	textField:"text",
	mode:"local",
	method:"post",
	url:null,
	data:null,
	keyHandler:{
		up:function(){
			var panel = $(this).combo('panel');
			var div= panel.find("div.combobox-item-selected");
			if(div && div.length>0 ){	
				var divPrev = $(div[0]).prev("div.combocheck-item"); 
				if(divPrev && divPrev.length>0){
					divPrev.addClass("combobox-item-selected");	
					$(div[0]).removeClass("combobox-item-selected");
					}					
				}else{
				  var divs = panel.find("div.combocheck-item");
				  $(divs[0]).addClass("combobox-item-selected");	
			  }
		},down:function(){
			var panel = $(this).combo('panel');
			var div=  panel.find("div.combobox-item-selected");
			if(div && div.length>0){
				var divNext= $(div[div.length-1]).next("div.combocheck-item"); 
				if(divNext && divNext.length>0){

					divNext.addClass("combobox-item-selected");
					$(div[div.length-1]).removeClass("combobox-item-selected");
					}					
				}
			else{
				  var divs = panel.find("div.combocheck-item");
				  $(divs[0]).addClass("combobox-item-selected");
			  }
	 	},enter:function(){		 		
		},
		query:function(val){
			var opts=$.data(this,"combocheck").options;
			var panel=$(this).combo("panel");
			var allData=opts.data;
			if(opts.multiple&&!val){
				_check(this,[]);
			}else{
				_check(this,[val]);
			}
			panel.find("div.combocheck-item").hide();
			for(var i=0;i<allData.length;i++){
			var bl = allData[i][opts.textField].indexOf(val)>=0;
			if(bl){
			var v=allData[i][opts.valueField];
			var s=allData[i][opts.textField];
			var input=panel.find("input[type=checkbox][value='"+ v+ "']");
			var div = input.parent();
			div.show();
			if(s==val){
			_check(this,[val]);
			div.addClass("combobox-item-selected");
			input.trigger("click");  	
			}			
			}
			}
		}
		}
});

$.fn.combocheck.methods={
	options:function(jq){
		return $.data(jq[0],"combocheck").options;
	},
	getData:function(jq){
		return $.data(jq[0],"combocheck").data;
	},
	 destroy: function(jq){
		return jq.each(function() {
			$(this).combo("hidePanel");
			$(this).remove();
		});
	  },
	 getValues: function(target){
		  var values=$(target).combo('getValues');			  
	      return values; 
	  },
	  getValue: function(target){
		  var values=$(target).combo('getValue');	 
	      return values;	  	 
	  },
	  setValues: function(jq, value){	
		/*  var panel = $(target).combo('panel');
		  if(value!=undefined && value!=null && value!="") {	
			  var values = value.split(",");
			  if($.isArray(values)){
				 $.each(values,function(k,v){
				var input = panel.find("input[type=checkbox][value="+ v+ "]"); 
				input.trigger("click");
				 });						 
			  }else{
				  var input = panel.find("input[type=checkbox][value="+ value+ "]"); 
				  input.trigger("click");  						 
			  }				
		  }*/
		  	// var options=$.data(target,"combocheck").options;
	  return jq.each(function() {  
		  if(value){
		  	var options=$(this).combocheck("options");
			//var data=$(target).combocheck("getData");
		  	var data=options.data;
			var panel=$(this).combo("panel");
			//panel.find("div.combobox-item-selected").removeClass("combobox-item-selected");
			var vv=[],ss=[];
			for(var i=0;i<value.length;i++){
			var v=value[i];
			var s=v;
			for(var j=0;j<data.length;j++){
			if(data[j][options.valueField]==v){
			s=data[j][options.textField];
			break;
				}
			}
			var input =	panel.find("input[type=checkbox][value='"+ v+ "']");
			//lnk.parentNode.appendChild(input); 
			input.attr("checked","checked"); 
			vv.push(v);
			ss.push(s);	
			}
			$(this).combo("setValues",vv);			
			$(this).combo("setText",ss.join(options.separator));	
		  } else {
			$(this).combo("setValues",[]);			
			$(this).combo("setText","");
		  }
	   });	  
	  },
	  setValue: function(jq, value){	
		  return jq.each(function() {
			  if($.isArray(value)){
				  $(this).combocheck("setValues",value);
			  }else{
				  $(this).combocheck("setValues",[value]);
			  }	 
		  });	 	
	  },
	  resize: function(jq, width){
		  return jq.each(function() {
			  $(this).combo("resize", width); 
		  });
	  }
	
	};

})(jQuery);

/**
 * 扩展ImageBox控件，
 * 子表图片控件没用直接调用此控件，但实现方式大致类似
 * param
 */
/*(function($){
	$.fn.imagebox = function(options, param) {
		if (typeof options == 'string') {
			var opt = $.fn.imagebox.methods[options];
			if (opt) {
				return opt(this, param);
			} else {
				return this.imagebox(options, param);
			}
		}
		options = options || {};
		return this
				.each(function() {
					options = $.extend($.fn.imagebox.defaults, options);
					var imageName = $(this).attr("id");
					var w = $(this).width();
					var h = $(this).height();
					if (w && w >= 20) {
						options.width = w;
					}
					if (h && h >= 20) {
						options.height = h;
					}
					$.data(this, "imagebox", {"options":options});
					var imgs = imageName + "Imgs";
					var src = imageName + "Src";
					var alter = imageName + "Alter";
					var upload = imageName + "Upload";
					var del = imageName + "Del";
//					var but = "<div id='" + alter + "' style='position:relative;top:-"+20+"px;display:none;' >"
//							+ "<div style='position:absolute;top:0;left:0;'>"
//							+ "<input title='上传图片' type='file' name='uploadify_gs' id='"
//							+ upload
//							+ "' /></div>"
//							+ "<div style='position:absolute;top:0;left:30px;'>"
//							+ "<input title='删除图片' style='width:20px;height:20px;border:0px' type='button' class='icon-remove' id='"
//							+ del
//							+ "' /></div>"
//							+ "</div>";
					//$(this).after(but);
					var img = "<div id='" + imgs + "' style='position:relative'>"
							+ "<img src='"
							+ options.src
							//+ "http://127.0.0.1/jwpf/upload/gs-attach/image/20150107/20150107163608349.jpg"
							+ "' id='"+src+"' style='width:"+options.width+"px;height:"+options.height+"px;'/>"
							+ "<div id='" + alter + "' style='width:"+options.width+"px;height:16px;position:absolute;bottom:0px;visibility:hidden;background-color:#000;"
							+ "filter:alpha(opacity=50);-moz-opacity:0.5;-khtml-opacity:0.5;opacity:0.5' >"
							+ "<div title='上传图片' style='width:16px;height:16px;position:absolute;left:"+(options.width/2-22)+"px;'>"
							+ "<input type='file' name='uploadify_gs' id='" + upload + "' /></div>"
							+ "<div title='删除图片' style='width:16px;height:16px;position:absolute;left:"+(options.width/2+6)+"px;'>"
							+ "<input style='background-color:transparent;width:20px;height:20px;border:0px;outline:none;cursor:pointer' "
							+ "type='button' class='icon-remove' id='"+del+"' /></div>"
							+ "</div>"
							+ "</div>";
					$(this).after(img);
					存放上传的图片的图片名字
					var input = "<input type='hidden' name='" + imageName
							+ "' />";
					$(this).after(input);
					用loadmask插件初始化图片上传控件
					uploadImage(upload, imageName, src, options.vPath);
//					$("#" + imgs).gzoom({
//						sW : options.width,
//						sH : options.height,
//						lightbox : true
//					});
					为图片控件添加按钮显示事件双击展示事件
					$("#" + imgs).parents("td").mouseout(function(){
							jQuery("#"+alter).css({visibility:"hidden"});
					}).mouseover(function(){
							if("view"!=operMethod) jQuery("#"+alter).css({visibility:"visible"});
					}).dblclick(function(){
						   jQuery("#"+src).gzoomExpand($(this).find("img").attr("src"));
					});
					$("#" + del).click(function() {
						$("#" + src).attr("src", "#");
						$("input[name=" + imageName + "]").val("");
					});
					$(this).hide();
					$(this).removeAttr("name");
				});
	};
	$.fn.imagebox.methods = {
		options:function(jq){
			return $.data(jq[0],"imagebox").options;
		},
		getValue: function(target){
			var imgSrc = $(target).next().val();
			return imgSrc;
		},
		setValue: function(jq, val){
			return jq.each(function() {
				var value = val || "";
				将图片路径value值加入<input type='hidden' name='" + imageName+ "' />
				 $(this).next().val(value);
				 将img元素的src属性设置为图片路径value值
				 var opt = $(this).imagebox("options");
				 var divs = $(this).nextAll("div");
				 for(var i = 0;i<divs.length;i++){
						var img_img = $(divs[i]).find("img");
						if(img_img.length>0){
							if(value=="") {
								img_img.attr("src", "#");
							} else {
								//$($(img_div[0]).find("img")[0]).attr("src", opt.vPath + value);
								img_img.attr("src", "/jwpf/upload/gs-attach/" + value);
							}
						}
					}
			});
		},
		enable : function(jq){
			return jq.each(function() {
				var but_div = $(this).nextAll("div.file_upload_button");
				if(but_div.length > 0){
					 for(var i = 0;i<but_div.length;i++){
							$(but_div[i]).show();
						}	
				}
			});
		},
		disable : function(jq){
			return jq.each(function() {
				var but_div = $(this).nextAll("div.file_upload_button");
				if(but_div.length > 0){
					 for(var i = 0;i<but_div.length;i++){
							$(but_div[i]).hide();
						}	
				}
			});
		},
		resize: function(jq, width){
			return jq.each(function() {
				var input = $(this);
				if(!$.boxModel&&$.browser.msie){
					input.width(width);
				}else{
					input.width(width-(input.outerWidth()-input.width()));
				}
			});
		}
	};
	$.fn.imagebox.defaults = {
			src:null,
			width:100,
			height:100,
			vPath:null
	};
	
})(jQuery);*/

(function($) {
	$.fn.imagebox = function(options, param) {
		if(typeof options == 'string') {
			var opt = $.fn.imagebox.methods[options];
			if(opt) {
				return opt(this, param);
			} else {
				return this.imagebox(options, param);
			}
		}
		options = options || {};
		return this
			.each(function() {
				options = $.extend($.fn.imagebox.defaults, options);
				var $this = $(this);
				var w = $this.width();
				var h = $this.height();
				if (w && w >= 20) {
					options.width = w;
				}
				if (h && h >= 20) {
					options.height = h;
				}
				$.data(this, "imagebox", {"options":options});
				var imageName = $this.attr("id");
				var imgs = imageName + "Imgs";
				var src = imageName + "Src";
				var alter = imageName + "Alter";
				var upload = imageName + "Upload";
				var del = imageName + "Del";
				/*构造图片的显示html*/
				var imageHtml = "<div id='" + imgs + "' style='position:relative'>"
					+ "<img src='" + options.src
					//+ "http://127.0.0.1/jwpf/upload/gs-attach/image/20150107/20150107163608349.jpg"
					+ "' id='" + src + "' style='width:" + options.width + "px;height:" + options.height + "px;'/>"
					+ "<div id='" + alter + "' style='width:" + options.width + "px;height:16px;position:absolute;bottom:0px;"
					+ "visibility:hidden;background-color:#000;filter:alpha(opacity=50);-moz-opacity:0.5;-khtml-opacity:0.5;opacity:0.5'>"
					+ "<div id='" + upload + "' title='上传图片'"
					+ " style='width:16px;height:16px;position:absolute;left:" + (options.width/2-22) + "px;'>"
					+ "<input class='icon-add' style='width:16px;height:16px;background-color:transparent;border:0px;'>"
					+ "</div>"
					+ "<div title='删除图片' style='width:16px;height:16px;position:absolute;left:" + (options.width/2+6) + "px;'>"
					+ "<input style='background-color:transparent;width:20px;height:20px;border:0px;outline:none;cursor:pointer'"
					+ " type='button' class='icon-remove' id='" + del + "' />"
					+ "</div></div></div>";
				$this.after(imageHtml);
				/*存放上传的图片的图片名字*/
				var inputHtml = "<input type='hidden' name='" + imageName + "'/>";
				$this.after(inputHtml);
				/*用webuploader插件初始化图片上传控件*/
				// 初始化Web Uploader
				var webuploader = WebUploader.create({
				    // 选完文件后，是否自动上传。
				    auto : true,
				    // swf文件路径
				    swf : __ctx + '/js/webuploader/Uploader.swf',
				    // 文件接收服务端。
				    server : __ctx + '/sys/attach/upload-file!input.action',
				    // 选择文件的按钮。可选。
				    // 内部根据当前运行是创建，可能是input元素，也可能是flash.
				    pick : '#' + upload,
				    // 只允许选择图片文件。
				    accept : {
				        title : 'Images',
				        extensions : 'gif,jpg,jpeg,bmp,png',
				        mimeTypes : 'image/*'
				    },
				    //fileVal : "filedata",
				    sendAsBinary : true
				});
				/*webuploader.on('fileQueued', function(file) {
					console.log(file);
					webuploader.makeThumb(file, function(error, _src) {
				        if(error) {
				            $img.replaceWith('<span>不能预览</span>');
				            return;
				        }
				        $("#" + src).attr('src', _src);
				    });
				});*/
				webuploader.on('uploadSuccess', function(file, path) {
					$("#" + src).attr('src', options.vPath+path.msg);
					$("input[name="+imageName+"]").val(path.msg);
				});
				/*为图片控件添加按钮显示事件*/
				var navigator_platform = navigator.platform;
				if(navigator_platform.indexOf('Win') == 0 || navigator_platform.indexOf('Mac') == 0 || navigator_platform == 'X11') {
					/*桌面端效果*/
					$("#" + imgs)
					.parents("td")
					.mouseout(function() {
						if("view" != operMethod) $("#" + alter).css({visibility : "hidden"});
					}).mouseover(function() {
						if("view" != operMethod) $("#" + alter).css({visibility : "visible"});
					}).dblclick(function() {
						$("#" + src).gzoomExpand($(this).find("img").attr("src"));
					});
				} else {
					/*移动端效果*/
					if("view" != operMethod) {
						$("#" + alter)
						.css({visibility : "visible"})
						.children("div");
					} else {
						$("#tedit").click(function(){
							$("#" + alter)
							.css({visibility : "visible"})
							.children("div");
						});
					}
				}
				$("#" + del).click(function() {
					$("#" + src).removeAttr("src");
					$("input[name=" + imageName + "]").val("");
				});
				/*禁止$this提交字段内容，由hidden元素提交图片文件名*/
				$this.hide();
			});
	};
	$.fn.imagebox.methods = {
		options:function(jq){
			return $.data(jq[0],"imagebox").options;
		},
		getValue: function(target){
			var imgSrc = $(target).next().val();
			return imgSrc;
		},
		setValue: function(jq, val){
			return jq.each(function() {
				var value = val || "";
				/*将图片路径value值加入<input type='hidden' name='" + imageName+ "' />*/
				 $(this).next().val(value);
				 /*将img元素的src属性设置为图片路径value值*/
				 var opt = $(this).imagebox("options");
				 var divs = $(this).nextAll("div");
				 for(var i = 0;i<divs.length;i++){
						var img_img = $(divs[i]).find("img");
						if(img_img.length>0){
							if(value=="") {
								img_img.attr("src", "#");
							} else {
								img_img.attr("src", opt.vPath + value);
								//img_img.attr("src", "/jwpf/upload/gs-attach/" + value);
							}
						}
					}
			});
		},
		enable : function(jq){
			return jq.each(function() {
				var but_div = $(this).nextAll("div.file_upload_button");
				if(but_div.length > 0){
					 for(var i = 0;i<but_div.length;i++){
							$(but_div[i]).show();
						}	
				}
			});
		},
		disable : function(jq){
			return jq.each(function() {
				var but_div = $(this).nextAll("div.file_upload_button");
				if(but_div.length > 0){
					 for(var i = 0;i<but_div.length;i++){
							$(but_div[i]).hide();
						}	
				}
			});
		},
		resize: function(jq, width){
			return jq.each(function() {
				var input = $(this);
				if(!$.boxModel&&$.browser.msie){
					input.width(width);
				}else{
					input.width(width-(input.outerWidth()-input.width()));
				}
			});
		}
	};
	$.fn.imagebox.defaults = {
			src:null,
			width:100,
			height:100,
			vPath:null
	};
	
})(jQuery);

/**
 * 扩展DialogueWindow控件，
 * param
 */
(function($){
	$.fn.dialoguewindow = function(options, param) {
		if(typeof options == 'string') {
			var opt = $.fn.dialoguewindow.methods[options];
			if(opt) {
				return opt(this, param);
			} else {
				return this;
			}
		}
		options = options || {};
		return this.each(function() {
			options = $.extend({}, $.fn.dialoguewindow.defaults, options);
			var __parent = $(this).parent();
			var __key = $(this).context.id;
			$(this).remove();
			var str = '<span class="dialoguewindow" style="width:'
				+ options.width
				+ 'px;">'
				+ '<input id="'
				+ __key
				+ '" name="'
				+ __key
				+ '" type="text" class="dialoguewindow-text" style="width:'
				+ (options.width - 18)
				+ 'px;height:'
				+ options.height
				+ 'px;" />'//readonly="readonly"
				+ '<span class="dialogue-window"></span>'
				+ '</span>';
			__parent.append(str);
			__parent.find("#" + __key).data({id:options.id,options:options});
			var _url = "/sys/dataFilter/dialogue-window.action?entityName=" + options.entityName + "&style=" + options.style;
			__parent.find("#" + __key).keydown(function(event) {
				if(event.keyCode == 13) {
					var __url = _url + "&fieldValue=" + $("#" + __key).val();
					openFormWin({
						title : "查询" + options.title,
						url : __url,
						onAfterSure : function(win) {
							var _data = win.getValue();
							if(_data) {
								var _id = "#" + __key + "key";
								var x = __parent.find("#" + __key);
								var x1 = $(_id);
								if(x.val() != _data.caption || x1.val() != _data.key) {
									x.dialoguewindow("setValue", _data);
									var o = x.data("options");
									if(o.onChange) {
										o.onChange.call(x, _data, {key : x1.val(), caption : x.val()});
									}
								}
								__parent.find("input").val(_data.caption);
								$(_id).val(_data.key);
							}
							return true;
						}
					});
					return false;
				}
			});
			__parent.find(".dialogue-window").click(function() {
				if(__parent.find("#" + __key).attr("disabled") !== "disabled") {
					var keyval = $("#" + __key + "key").val();
					var __url;
					if(keyval) {
						__url = _url + "&key=" + keyval;
					} else {
						__url = _url;
					}
					openFormWin({
						title : "查询" + options.title,
						url : __url,
						onAfterSure : function(win) {
							var _data = win.getValue();
							if(_data) {
								var _id = "#" + __key + "key";
								var x = __parent.find("#" + __key);
								var x1 = $(_id);
								if(x.val() != _data.caption || x1.val() != _data.key) {
									x.dialoguewindow("setValue", _data);
									var o = x.data("options");
									if(o.onChange) {
										o.onChange.call(x, _data, {key : x1.val(), caption : x.val()});
									}
								}
								__parent.find("input").val(_data.caption);
								$(_id).val(_data.key);
							}
							return true;
						}
					});
				}
			});
		});
	};
	$.fn.dialoguewindow.methods = {
		options:function(target){
			return target.data("options");
		},
		getValue : function(target) {
			var _id = "#" + target.attr("id") + "key";
			var x1 = $(_id);
			return {key : x1.val(), caption : target.val()};
		},
		setValue : function(target, val) {
			if(val) {
				var _id = "#" + target.attr("id") + "key";
				var x1 = $(_id);
				$(target).val(val.caption);
				x1.val(val.key);
			}
			return target;
		},
		enable : function(target) {
			target.next().show();
			return target;
		},
		disable : function(target) {
			target.next().hide();
			//object.width(object.width()+18);
			return target;
		}
	};
	$.fn.dialoguewindow.defaults = $.extend({}, $.fn.combo.defaults, {
		width:150,
		height:20
	});
})(jQuery);

/**
 * 扩展ProcessBar和NumberSpinner控件
 * param
 */
(function($){
	$.fn.processbarSpinner = function(options, param) {
		if (typeof options == 'string') {
			var opt = $.fn.processbarSpinner.methods[options];
			if (opt) {
				return opt(this, param);
			} else {
				return this.processbarSpinner(options, param);
			}
		}
		options = options || {};
		return this
				.each(function() {
					options = $.extend($.fn.processbarSpinner.defaults, options);
					var id = $(this).attr("id");
					var pbId = "pb_" + id;
					var w = $(this).width();
					var h = $(this).height();
					if (w && w >= 100) {
						options.width = w;
					}
					if (h && h >= 100) {
						options.height = h;
					}
					$.data(this, "processbarSpinner", {"options":options});
					$(this).wrap("<div/>");
					$(this).before("<div style='float:left;padding:8px 0px 0px 2px;'><div id='" + pbId + "'/></div>");
					$(this).after("<div style='clear:both;'></div>");
					$("#"+pbId).progressbar({
						width:options.width
					});
					$(this).css("height",18);
					$(this).wrap("<div style='float:left;padding:5px;'/>");
					$(this).numberspinner({
						min:0,
						max:100,
						value:0,
						width:50,
						onSpinUp:function() {
							var value = $(this).numberspinner("getValue");
							$("#"+pbId).progressbar("setValue", value);
						},
						onSpinDown:function() {
							var value = $(this).numberspinner("getValue");
							$("#"+pbId).progressbar("setValue", value);
						}
					});
				});
	};
	$.fn.processbarSpinner.methods = {
		options:function(jq){
			return $.data(jq[0],"processbarSpinner").options;
		},
		getValue: function(target){
			var val = $(target).numberspinner("getValue");
			return val;
		},
		setValue: function(jq, value){
			return jq.each(function() {
				$(this).numberspinner("setValue", value);
				var id = $(this).attr("id");
				$("#pb_"+id).progressbar("setValue", value);
			});
		},
		enable : function(jq){
			return jq.each(function() {
				$(this).numberspinner("enable");
			});
		},
		disable : function(jq){
			return jq.each(function() {
				$(this).numberspinner("disable");
			});
		},
		resize: function(jq, width){
			return jq.each(function() {
				var input = $(this);
				if(!$.boxModel&&$.browser.msie){
					input.width(width);
				}else{
					input.width(width-(input.outerWidth()-input.width()));
				}
			});
		}
	};
	$.fn.processbarSpinner.defaults = {
			width:100,
			height:100
	};
})(jQuery);

/**
 * 自动适应内容高度
 * @param $
 */
(function($) {

	$.fn.autoTextarea = function(options) {

		var defaults = {

			maxHeight : null,// 文本框是否自动撑高，默认：null，不自动撑高；如果自动撑高必须输入数值，该值作为文本框自动撑高的最大高度

			minHeight : $(this).height()
		// 默认最小高度，也就是文本框最初的高度，当内容高度小于这个高度的时候，文本以这个高度显示

		};

		var opts = $.extend({}, defaults, options);

		return $(this)
				.each(
						function() {

							$(this)
									.bind(
											"paste cut keydown keyup focus blur",
											function() {

												var height, style = this.style;

												this.style.height = opts.minHeight
														+ 'px';

												if (this.scrollHeight > opts.minHeight) {

													if (opts.maxHeight
															&& this.scrollHeight > opts.maxHeight) {

														height = opts.maxHeight;

														style.overflowY = 'scroll';

													} else {

														height = this.scrollHeight;

														style.overflowY = 'hidden';

													}

													style.height = height
															+ 'px';
													// 触发datagrid重新设置高度
													var tr = $(this)
															.closest(
																	"tr[datagrid-row-index]");
													if (tr.length) {
														var dg = tr
																.closest(
																		"div[class='datagrid-view']")
																.find(
																		"table[id]");
														if (dg.length) {
															var dgId = dg
																	.attr("id");
															var rowIndex = tr
																	.attr("datagrid-row-index");
															$("#" + dgId)
																	.edatagrid(
																			"fixRowHeight",
																			parseInt(rowIndex));
														}
													}

												}

											});

						});

	};

})(jQuery);

$.fn.datagrid.defaults.editors.text.init = function(container,options){
	var _4fb=$("<input type=\"text\" class=\"datagrid-editable-input\">").appendTo(container);
	if(options && options.readonly) {
		_4fb.attr("readonly", "readonly");
	}
	if(options && options["onkeydown"]) { //加入回车事件支持
		_4fb.bind("keydown", options["onkeydown"]);
	}
	return _4fb;
};

$.fn.datagrid.defaults.editors.textarea.init = function(container,options){
	var _4fb=$("<textarea class=\"datagrid-editable-input\"></textarea>").appendTo(container);
	if(options && options.readonly) {
		_4fb.attr("readonly", "readonly");
	}
	_4fb.autoTextarea();
	_4fb.bind('keydown',function(e) {
		if(e.ctrlKey && e.keyCode==13){
			/*var content = $(e.target).val();
			content = content+'\n';
			$(e.target).val(content);*/
			$(e.target).insert({"text":"\n"});
			return false;
		}
	});
	return _4fb;
};

$.fn.datagrid.defaults.editors.validatebox.init = function(container,options){
	var _4fb=$("<input type=\"text\" class=\"datagrid-editable-input\">").appendTo(container);
	if(options && options.readonly) {
		_4fb.attr("readonly", "readonly");
	}
	_4fb.validatebox(options);
	if(options && options["onkeydown"]) { //加入回车事件支持
		_4fb.bind("keydown", options["onkeydown"]);
	}
	return _4fb;
};

$.fn.datagrid.defaults.editors.numberbox.init = function(container,options){
	var _512=$("<input type=\"text\" class=\"datagrid-editable-input\">").appendTo(container);
	_512.numberbox(options);
	if(options && options["onkeydown"]) { //加入回车事件支持
		_512.bind("keydown", options["onkeydown"]);
	}
	return _512;
};

/**
 * 扩展combo编辑器，增加newradiobox支持
 */
(function($){
		
	$.fn.newradiobox = function(options, param){//初始化方法
		if (typeof options == 'string'){
			var opt=$.fn.newradiobox.methods[options];
			if(opt){
			return opt(this,param);
			}else{
			return this.combo(options,param);
			}
		}		
		options = options || {};		
		options.multiple= false;
		return this.each(function(){
			var state=$.data(this,"newradiobox");
			if(state){
				$.extend(state.options,options);			
			}else{
				state = {options:$.extend({},$.fn.newradiobox.defaults,options)};
				$.data(this,"newradiobox",state);	
			}		
			var tag= $(this);
			tag.addClass("newradiobox-f");
			tag.combo(state.options);
			tag.combo("panel").hide();
			var divs =$('<div class="easyui-newradiobox" style="width:100%;"></div>');
			tag.after(divs);
			tag.removeAttr("name");
			tag.hide();
			divs.next("span.combo").hide();
			var data =  state.options.data;
			var valueField =  state.options.valueField;
			var textField =  state.options.textField;		
			var id = $(this).attr("id");
			if(data && data.length){
				var str="";
				$.each(data,function(k,v){
					 str +="<input name='"+id+"1' type='radio' value='"+v[valueField]+"'/>"+v[textField] + "&nbsp;&nbsp;"; 				 						 
					});
					divs.append(str);
					
					divs.find("input[type=radio]").click(function(){	
						
						   	var vv=tag.combo('getValue');
						   	
							var input = divs.find("input[type=radio][value="+ vv+ "]");
							if(input){			
							var v = $(this).val();
							if(vv==v){
								tag.combo('setValue', vv);
							}else{
								tag.combo('setValue', v);	
							}
						}
										
					});
				   }	
			
			});
	
	};
	
$.fn.newradiobox.defaults = $.extend({},$.fn.combo.defaults,{
	valueField:"value",
	textField:"text",
	mode:"local",
	method:"post",
	url:null,
	data:null,
	keyHandler:{
	}
});

$.fn.newradiobox.methods={
	options:function(jq){
		return $.data(jq[0],"newradiobox").options;
	},
	getData:function(jq){
		return $.data(jq[0],"newradiobox").data;
	},
	setValue: function(jq, value){	
		return jq.each(function() {
		 	  var divs = $(this).next("div.easyui-newradiobox");
			  if(value!=undefined && value!=null) {									
				  var input = divs.find("input[type=radio][value="+ value+ "]"); 
				  input.trigger("click");  							  
			  }
		});
	  },
	getValue: function(target){		
		  var value=$(target).combo('getValue');
			  return value;	  
	 },
	 destroy: function(jq){
		  return jq.each(function() {
			  $(this).combo("hidePanel");
			  $(this).remove(); 
		  });
	  }
	};

})(jQuery);
/**
 * 扩展combo编辑器，增加newcheckbox支持
 */
(function($){		
	$.fn.newcheckbox = function(options, param){//初始化方法
		if (typeof options == 'string'){
			var opt=$.fn.newcheckbox.methods[options];
			if(opt){
			return opt(this,param);
			}else{
			return this.newcheckbox(options,param);
			}
		}		
		options = options || {};
		options.multiple= true;
		return this.each(function(){			
			var state=$.data(this,"newcheckbox");
			if(state){
				$.extend(state.options,options);			
			}else{
				state = {options:$.extend({},$.fn.newcheckbox.defaults,
						options)};
				$.data(this,"newcheckbox",state);		
			}		
			
			var tag= $(this);
			tag.addClass("newcheckbox-f");
			tag.combo(state.options);
			tag.combo("panel").hide();
			var divs =$("<div class='easyui-newcheckbox' style='width:100%;'></div>");
			tag.after(divs);
			tag.removeAttr("name");
			tag.hide();	
			divs.next("span.combo").hide();
			var data = state.options.data;
			var valueField = state.options.valueField;
			var textField =state.options.textField;		
			var id = $(this).attr("id");
			if(data && data.length){
				var str="";
				$.each(data,function(k,v){
				 str +="<input name ='"+id+"1'  type='checkbox' value='"+v[valueField]+"'/>"+"<span>"+v[textField]+"</span>"; 							 
					});
				divs.append(str);			 
					divs.find("input[type=checkbox]").click(function(){				
						var vv =tag.combo("getValues");//得到选中的值
							vv=$.map(vv,function(n){
								return n=="" ? null : n;
							});
						/*$.each(vv,function(k,v){
							var input = divs.find("input[type=checkbox][value="+ v+ "]");  
						});*/	
						var v= $(this).val();	
						//var input = divs.find("input[type=checkbox][value="+ v+ "]");  

						if($.inArray(v, vv)>=0){
							vv=$.map(vv,function(n){
								return n==v ? null : n;
							});

						}else{
							vv.push(v);	
						}
						if(vv.length==0){
							vv=[""];
							tag.combo('setValues',vv);
						}else{
						tag.combo('setValues',vv);
						}
					});
			
				/*	 $(this).next().find("input.combo-text").bind('keydown','space',function (e) {	 
						var div= panel.find("div.combobox-item-selected");
							if(div && div.length>0 ){		 
							var input =$(div[0]).find("input[type=checkbox]"); 
								$(input[0]).trigger("click");						
							}
							return false;							 
				 });*/
			}	
		});	
	};
	
$.fn.newcheckbox.defaults = $.extend({},$.fn.combo.defaults,{
	valueField:"value",
	textField:"text",
	mode:"local",
	method:"post",
	url:null,
	data:null,
	keyHandler:{

		}
});

$.fn.newcheckbox.methods={
	options:function(jq){
		return $.data(jq[0],"newcheckbox").options;
	},
	getData:function(jq){
		return $.data(jq[0],"newcheckbox").data;
	},
	getValues: function(target){
		  var values=$(target).combo('getValues');			  
	      return values; 
	  },
	getValue: function(target){
		  var values=$(target).combo('getValue');	 
	      return values;	  	 
	  },
	setValues : function(jq, value) {
			return jq.each(function() {
				if (value) {
					var options = $(this).newcheckbox("options");
					var divs = $(this).next("div.easyui-newcheckbox");
					var data = options.data;
					var vv = [];
					for ( var i = 0; i < value.length; i++) {
						var v = value[i];
						for ( var j = 0; j < data.length; j++) {
							if (data[j][options.valueField] == v) {
								var input = divs.find("input[type=checkbox][value="+ v+ "]"); 
								input.attr("checked","checked"); 
								break;
							}
						}
						vv.push(v);
					}
					$(this).combo("setValues", vv);

				} else {
					$(this).combo("setValues", []);
				}
			});
		},
	setValue: function(jq, value){	
		  return jq.each(function() {
			  if($.isArray(value)){
				  $(this).newcheckbox("setValues",value);
			  }else{
				  $(this).newcheckbox("setValues",[value]);
			  }	 
		  });	 	
	  }
	
	};
})(jQuery);

/**扩展datagrid编辑器，增加newradiobox、newcheckbox*/
$.extend($.fn.datagrid.defaults.editors, {
	newradiobox : {
		init : function(container, options) {
			var sel = $('<input/>').appendTo(container);
			sel.newradiobox(options);
			return sel;
		},
		destroy : function(target) {
			$(target).newradiobox('destroy');
		},
		getValue : function(target) {
			var value = $(target).newradiobox('getValue');
			return value;
		},

		setValue : function(target, value) {
			if (value) {
				$(target).newradiobox('setValue', value);
			}
		}
	}
});
/** 扩展datagrid编辑器，增加combo多选支持 */

$.extend($.fn.datagrid.defaults.editors,{
	newcheckbox:{
		 init: function(container, options){
		  var sel=$('<input/>').appendTo(container);		  

			  sel.newcheckbox(options);
			  return sel;
		  },
		getValue: function(target){
				var values =  $(target).newcheckbox('getValues');
				  if($.isArray(values)){
					  return values.join(",");
				  }else{
					  return values;
				  }		
				},

		setValue: function(target, value){
				  if(value){
					 var values = value.split(",");
					 $(target).newcheckbox('setValues',values);
				  } else {
					 $(target).newcheckbox('setValues',[]); 
				  }
			  }
	}
});

/** 扩展datagrid编辑器，增加imagebox支持 */
$.extend($.fn.datagrid.defaults.editors, {
	ImageBox : {
		init : function(container, options) {
			var imageName = "__edite";
			var imgs = imageName + "Imgs";
			var src = imageName + "Src";
			var alter = imageName +"Alter";
			var upload = imageName + "Upload";
			var del = imageName + "Del";
			var img = "<div id='" + imgs + "' style='position:relative;padding-left:4px'>"
			+ "<img src='#' id='"+src+"' style='width:"+options.width+"px;height:"+options.height+"px;'/>"
			+ "<div id='" + alter + "' style='width:"+options.width+"px;height:16px;position:absolute;bottom:0px;background-color:#000;"
			+ "filter:alpha(opacity=50);-moz-opacity:0.5;-khtml-opacity:0.5;opacity:0.5' >"
			+ "<div title='上传图片' style='width:16px;height:16px;position:absolute;left:"+(options.width/2-22)+"px;'>"
			+ "<input type='file' name='uploadify_gs' id='" + upload + "' /></div>"
			+ "<div title='删除图片' style='width:16px;height:16px;position:absolute;left:"+(options.width/2+6)+"px;'>"
			+ "<input style='background-color:transparent;width:20px;height:20px;border:0px;outline:none;cursor:pointer' "
			+ "type='button' class='icon-remove' id='"+del+"' /></div>"
			+ "</div>"
			+ "<input __edite type='hidden' name='" + imageName + "' />";
			+ "</div>";
			container.append(img);
			uploadImage(upload, imageName, src, options.vPath);
			$("#" + imgs).dblclick(function(){
				   jQuery("#"+src).gzoomExpand($(this).find("img").attr("src"));
			});
			$("#" + del).click(function() {
				$("#"+src).attr("src", "#");
				$("#"+imgs+" input[__edite]").val("");
			});
			var target = container.children().data("vPath",options.vPath);
			return target;
		},
		destroy : function(target) {
			
		},
		getValue : function(target) {
			return target.find("input[__edite]").val();
		},
		setValue : function(target, value) {
			if (value) {
				$(target).find("input[__edite]").val(value);
				var src = $(target).data("vPath") + value;
				target.find("img").attr({src:src});
			}
		},
		resize : function(target, width) {
			
		}
	}
});

/** 扩展datagrid编辑器，增加dialoguewindow支持 */
$.extend($.fn.datagrid.defaults.editors, {
	dialoguewindow : {
		init : function(container, options) {
			options = $.extend({width:80,height:'auto'}, options);
			var str = '<input id="' 
				+ options.key
				+ '" name="'
				+ options.key
				+ '"/>';
			container.append(str);
			container.find("input").dialoguewindow(options);
			var str2 = '<input id="' 
				+ options.key
				+ 'key" name="'
				+ options.key
				+ 'key" type="hidden"/>';
			container.append(str2);
			return container.find("#"+options.key+"");
		},
		getValue : function(target) {
			var val = target.dialoguewindow("getValue");
			var id = target.data("id");
			if(id) {
				var index = target.parents("tr.datagrid-row").attr("datagrid-row-index");
				var rows = $("#" + id).datagrid("getRows");
				rows[index][target.attr("id")+'key'] = val.key;
				return val.caption;
			}
		},
		setValue : function(target, value) {
			var id = target.data("id");
			var index = target.parents("tr.datagrid-row").attr("datagrid-row-index");
			var rows = $("#" + id).datagrid("getRows");
			var key = rows[index][target.attr("id")+'key'];
			target.dialoguewindow("setValue", {key:key, caption:value});
		},
		resize : function(target, width) {
			target.parent().width(width);
			target.width(width-18);
		}
	}
});
//使textarea不出现滚动条
/*$(function() {
	$(".module_form textarea").live("keyup",function(e) {
		var height = $(this).height();
		var innerHeight = $(this).innerHeight();
		$(this).height(this.scrollHeight - innerHeight + height);
	});
});*/