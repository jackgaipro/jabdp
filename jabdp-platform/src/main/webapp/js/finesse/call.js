/**
 * 呼叫中心接口
 */
var

//Private finesse object.
_finesse,

//Store agent information
_username, _password, _extension, _domain,

//Private reference to JabberWerx eventing object.
_jwClient;

/**
 * Reset the sample page back to a signed out state.
 */
function _reset() {
    //Clear console.
    $("#console-area").val("");

    //Show sign in boxes.
    $("#div-signin").show();

    //Hide all API actions DIV until successful sign in.
    $("#actions").hide();

    //Hide agent info DIV until successful sign in.
    $("#div-signout").hide();

    //Reset agent info data.
    $("#span-agent-info").html("");

    //Clear the dial number field.
    $("#field-call-control-make-dialnum").val("");

    //Clear the callid field.
    $("#field-call-control-callid").val("");
}

function replaceAll(str, stringToReplace, replaceWith)
{
	var result = str, index = 1;
	while (index > 0)
	{
	  result = result.replace(stringToReplace, replaceWith);
          index = result.indexOf(stringToReplace);
        }
 	return result;
}

/**
 * Print text to console output.
 */
function print2Console(type, data) {
    /*var date = new Date(),
    xml = null;
    consoleArea = $("#console-area");

    if (type === "EVENT")
    {
      xml = data.data._DOM.xml;
      xml = replaceAll(xml, "&lt;", "<");
      xml = replaceAll(xml, "&gt;", ">");
   
      consoleArea.val(consoleArea.val() + "[" + date.getTime() + "] [" +
            type + "] " + xml + "\n\n");
    }
    else 
    {
      //Convert data object to string.
    //  if (typeof data === "object") {
    //     data = JSON.stringify(data);
    //  }
      //Convert data object to string and print to console.
      consoleArea.val(consoleArea.val() + "[" + date.getTime() + "] [" +
              type + "] " + data + "\n\n");
    }

    //Scroll to bottom to see latest.
    consoleArea.scrollTop(consoleArea[0].scrollHeight);*/
    if(window.console) {
      console.log(type + "\n" + data);
    }
}

function onClientError() {

Print2Console("ERROR ");

}

/**
 * Event handler that prints events to console.
 */
function _eventHandler(data) {
	print2Console("EVENT", data);

	data = data.selected.firstChild.data;
	print2Console("MYEVENT", data);
	// try to get the callid
	data = parseXml(data);
	var $callid = $(data).find("id");
	var $fromAddress = $(data).find("fromAddress");
	//var $toAddress = $(data).find("toAddress");
	//var callType = $callType.text();
	var fromAddress = $fromAddress.text();
	var callId = $callid.text();
	if(callId !== "" && fromAddress !== "") { 
		//来电，来电号码不等于坐席号
		if(fromAddress != _extension) {
			//console.log("来电" + fromAddress);
			//var $phoneNum = $(data).find("fromAddress");
			//var phoneNum = $phoneNum.text();
			var oldCallId = $("#field-call-control-callid").val();
			if(callId!=oldCallId) {
				$("#field-call-control-callid").val(callId);
				if(window.doAnswerPhone) {
					window.doAnswerPhone(callId, fromAddress);
				}
			}
		} else {//去电
			//console.log(fromAddress+"去电");
			var callId = $callid.text();
			var oldCallId = $("#field-call-control-callid").val();
			var $callType = $(data).find("callType");
			var callType = $callType.text();
			if(callType == "CONSULT") {
				var $state = $(data).find("dialog>state");
				var state = $state.text();
				if(state == "ACTIVE") { //咨询呼叫已应答
					if(window.doAfterAnswerConsultPhone) {
						window.doAfterAnswerConsultPhone(callId);
					}
				}
			} else {
				if(callId!=oldCallId) {
					//$(data).find("Dialog>state");
					$("#field-call-control-callid").val(callId);
					if(window.doAfterCallPhone) {
						window.doAfterCallPhone(callId);
					}
				}
			}
		}
	}
	//判断状态改变
	var $stateChange = $(data).find("stateChangeTime");
	if($stateChange.text() !== "") {
		var state = $(data).find("state");
		if(window.doAfterGetState) {
			window.doAfterGetState(state.text());
		}
	}
	// React to call events. Populate the callId field.
    
}

function parseXml(xml) {
     if (jQuery.browser.msie) {
        var xmlDoc = new ActiveXObject("Microsoft.XMLDOM"); 
        xmlDoc.loadXML(xml);
        xml = xmlDoc;
    }   
    return xml;
}

/**
 * Connects to the BOSH connection. Any XMPP library or implementation can be
 * used to connect, as long as it conforms to BOSH over XMPP specifications. In
 * this case, we are using Cisco's Ajax XMPP library (aka JabberWerx). In order
 * to make a cross-domain request to the XMPP server, a proxy should be
 * configured to forward requests to the correct server.
 */
function _eventConnect() {
    if (window.jabberwerx) {
        var
        //Construct JID with username and domain.
        jid = _username + "@" + _domain,

        //GREG
        //Create JabbwerWerx object.
        _jwClient = new jabberwerx.Client("cisco");

        //Arguments to feed into the JabberWerx client on creation.
        jwArgs = {
            //Defines the BOSH path. Should match the path pattern in the proxy
            //so that it knows where to forward the BOSH request to.
             httpBindingURL: "/http-bind",
            //Calls this function callback on successful BOSH connection by the
            //JabberWerx library.
			errorCallback: onClientError,
            successCallback: function () {
                //Get the server generated resource ID to be used for subscriptions.
                _finesse.setResource(_jwClient.resourceName);
            }
        };

        
        jabberwerx._config.unsecureAllowed = true;
        //Bind invoker function to any events that is received. Only invoke
        //handler if XMPP message is in the specified structure.
        _jwClient.event("messageReceived").bindWhen(
                "event[xmlns='http://jabber.org/protocol/pubsub#event'] items item notification",
                _eventHandler);

        //Connect to BOSH connection.
        _jwClient.connect(jid, _password, jwArgs);
    }
    else {
        alert("CAXL library not found. Please download from http://developer.cisco.com/web/xmpp/resources");
    }
}

/**
 * Generic handler that prints response to console.
 */
function _handler(data) {
    print2Console("RESPONSE", data);
}

/**
 * GetState handler that prints response to console.
 */
function _getStateHandler(data) {
    print2Console("RESPONSE", data.xml);
    if(window.doAfterGetState) {
    	var dataXml = parseXml(data.xml);
    	var state = $(dataXml).find("state");
    	window.doAfterGetState(state.text());
    }
}

/**
 * Handler for the make call that will validate the response and automatically
 * store the call id retrieve from the response data.
 */
function _makeCallHandler(data) {
    print2Console("RESPONSE", data);

    //Validate success.
    /*if (data.response === "success") {
        $("#field-call-control-callid").val(data.callId);
        if(window.doAfterCallPhone) {
        	window.doAfterCallPhone(data.callId);
        }
    }*/
}

/**
 * Sign in handler. If successful, hide sign in forms, display actions, and
 * connect to BOSH channel to receive events.
 */
function _signInHandler(data) {
    print2Console("RESPONSE", data);

    //Ensure success.
	if (data === 202) {
          //Connect to XMPP server to receive events.
        _eventConnect();
        if(window.doAfterSignIn) {
        	window.doAfterSignIn();
        }
        //Hide signin forms and show actions.
        /*$("#div-signin").hide();
        $("#actions").show();
        $("#div-signout").show();
        $("#span-agent-info").html("Logged in as <b>" + _username + "</b> with extension <b>" + _extension + "</b>");*/
    } else {
    	if(window.doAfterResponseError) {
    		window.doAfterResponseError(data);
    	}
    }
}

/**
 * Sign out handler. If successful, hide actions, display sign in forms, and
 * disconnect from BOSH channel.
 */
function _signOutHandler(data) {
    print2Console("RESPONSE", data);

    //Ensure success.
	if (data === 202) {
        //Reload the page after successful sign out.
        window.location.reload();
    }
}

/**
 * Init function. Wait until document is ready before binding actions to buttons
 * on the page.
 */
$(document).ready(function () {

    //Reset UI to sign out state.
    //_reset();

    //Binds the button to clear the console output box.
    $("#button-clear-console").click(function () {
        $("#console-area").val("");
    });

    /** Bind all buttons to actions **/

	// SYSINFO button
	$("#form-sysinfo").submit(function() {
	  _finesse.sysInfo(_handler, _handler);
	});

    //SIGNIN button
    $("#form-agent-signin").submit(function () {

        //Grabs the credentials from the input fields.
        _username = $("#field-agentid").val();
        _password = $("#field-password").val();
        _extension = $("#field-extension").val();
        _domain = $("#field-domain").val();

        //Check non-empty fields
        if (!_username || !_password || !_extension || !_domain) {
            alert("Please enter valid domain and credentials.");
        }
        else {
            //Create Finesse object and sign in user. On successful sign in, a
            //handler will be invoked to present more API options in UI.
            _finesse = new Finesse(_username, _password);

            //Sign in agent.
           // _finesse.signIn(_username, _extension, true, _signInHandler, _handler);
		   _finesse.signIn(_username, _extension, true, _signInHandler, _signInHandler);
        }

        return false;
    });

    //SIGNOUT button
    $("#button-signout").click(function () {
        _finesse.signOut(_username, _extension, null, _signOutHandler, _signOutHandler);
    });

    //GET AGENT STATE button
    $("#button-get-agent-state").click(function () {
        _finesse.getState(_username, _getStateHandler, _handler);
    });

   ;

    //CHANGE AGENT STATE READY button
    $("#button-change-agent-state-ready").click(function () {
        var newState = "READY";
        if (newState) {
            _finesse.changeState(_username, newState, null, _handler, _handler);
            if(window.doAfterReady) {
            	 window.doAfterReady();
            }
        }
    });
	
	//CHANGE AGENT STATE NOT READY button
    $("#button-change-agent-state-notready").click(function () {
        var newState = "NOT_READY";
        if (newState) {
            _finesse.changeState(_username, newState, null, _handler, _handler);
            if(window.doAfterNotReady) {
            	 window.doAfterNotReady();
            }
        }
    });

 
    //MAKE CALL button
    $("#form-call-control-make").submit(function () {
        var dialNum = $("#field-call-control-make-dialnum").val();
        _finesse.makeCall(dialNum, _extension, _makeCallHandler, _handler);
        return false;
    });

    //ANSWER CALL button
    $("#button-call-control-answer").click(function () {
        var callId = $("#field-call-control-callid").val();
        _finesse.answerCall(callId, _extension, _handler, _handler);
        if(window.doAfterAnswerPhone) {
        	window.doAfterAnswerPhone(callId);
        }
    });
    
    //Consult CALL button
    $("#button-call-control-consult").click(function () {
        var phoneNum = $("#field-call-control-consult-dialnum").val();
        if(phoneNum) {
        	var callId = $("#field-call-control-callid").val();
        	_finesse.consultCall(callId, phoneNum, _extension, _handler, _handler);
        	if(window.doAfterConsultPhone) {
        		window.doAfterConsultPhone(callId);
        	}
        } else {
        	alert("请输入正确的咨询转接号码");
        }
    });
    
    //Transfer CALL button
    $("#button-call-control-transfer").click(function () {
        var callId = $("#field-call-control-callid").val();
        var phoneNum = $("#field-call-control-consult-dialnum").val();
        _finesse.transferCall(callId, phoneNum, _extension, _handler, _handler);
    	if(window.doAfterTransferPhone) {
    		window.doAfterTransferPhone(callId);
    	}
    });

    //HOLD CALL button
    $("#button-call-control-hold").click(function () {
        var callId = $("#field-call-control-callid").val();
        _finesse.holdCall(callId, _extension, _handler, _handler);
        if(window.doAfterHoldCall) {
           window.doAfterHoldCall(callId);
        }
    });

    //RETRIEVE CALL button
    $("#button-call-control-retrieve").click(function () {
        var callId = $("#field-call-control-callid").val();
        _finesse.retrieveCall(callId, _extension, _handler, _handler);
        if(window.doAfterRetrieveCall) {
           window.doAfterRetrieveCall(callId);
        }
    });

    //DROP CALL button
    $("#button-call-control-drop").click(function () {
        var callId = $("#field-call-control-callid").val();
        _finesse.dropCall(callId, _extension, _handler, _handler);
        if(window.doAfterDropCall) {
           window.doAfterDropCall(callId);
        }
    });
});