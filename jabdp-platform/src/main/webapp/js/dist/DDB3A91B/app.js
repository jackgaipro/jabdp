/*!
 * jQuery Form Plugin
 * version: 3.13 (29-JUL-2012)
 * @requires jQuery v1.3.2 or later
 *
 * Examples and documentation at: http://malsup.com/jquery/form/
 * Project repository: https://github.com/malsup/form
 * Dual licensed under the MIT and GPL licenses:
 *    http://malsup.github.com/mit-license.txt
 *    http://malsup.github.com/gpl-license-v2.txt
 */
/*global ActiveXObject alert */
;(function($) {
"use strict";

/*
    Usage Note:
    -----------
    Do not use both ajaxSubmit and ajaxForm on the same form.  These
    functions are mutually exclusive.  Use ajaxSubmit if you want
    to bind your own submit handler to the form.  For example,

    $(document).ready(function() {
        $('#myForm').on('submit', function(e) {
            e.preventDefault(); // <-- important
            $(this).ajaxSubmit({
                target: '#output'
            });
        });
    });

    Use ajaxForm when you want the plugin to manage all the event binding
    for you.  For example,

    $(document).ready(function() {
        $('#myForm').ajaxForm({
            target: '#output'
        });
    });
    
    You can also use ajaxForm with delegation (requires jQuery v1.7+), so the
    form does not have to exist when you invoke ajaxForm:

    $('#myForm').ajaxForm({
        delegation: true,
        target: '#output'
    });
    
    When using ajaxForm, the ajaxSubmit function will be invoked for you
    at the appropriate time.
*/

/**
 * Feature detection
 */
var feature = {};
feature.fileapi = $("<input type='file'/>").get(0).files !== undefined;
feature.formdata = window.FormData !== undefined;

/**
 * ajaxSubmit() provides a mechanism for immediately submitting
 * an HTML form using AJAX.
 */
$.fn.ajaxSubmit = function(options) {
    /*jshint scripturl:true */

    // fast fail if nothing selected (http://dev.jquery.com/ticket/2752)
    if (!this.length) {
        log('ajaxSubmit: skipping submit process - no element selected');
        return this;
    }
    
    var method, action, url, $form = this;

    if (typeof options == 'function') {
        options = { success: options };
    }

    method = this.attr('method');
    action = this.attr('action');
    url = (typeof action === 'string') ? $.trim(action) : '';
    url = url || window.location.href || '';
    if (url) {
        // clean url (don't include hash vaue)
        url = (url.match(/^([^#]+)/)||[])[1];
    }

    options = $.extend(true, {
        url:  url,
        success: $.ajaxSettings.success,
        type: method || 'GET',
        iframeSrc: /^https/i.test(window.location.href || '') ? 'javascript:false' : 'about:blank'
    }, options);

    // hook for manipulating the form data before it is extracted;
    // convenient for use with rich editors like tinyMCE or FCKEditor
    var veto = {};
    this.trigger('form-pre-serialize', [this, options, veto]);
    if (veto.veto) {
        log('ajaxSubmit: submit vetoed via form-pre-serialize trigger');
        return this;
    }

    // provide opportunity to alter form data before it is serialized
    if (options.beforeSerialize && options.beforeSerialize(this, options) === false) {
        log('ajaxSubmit: submit aborted via beforeSerialize callback');
        return this;
    }

    var traditional = options.traditional;
    if ( traditional === undefined ) {
        traditional = $.ajaxSettings.traditional;
    }
    
    var elements = [];
    var qx, a = this.formToArray(options.semantic, elements);
    if (options.data) {
        options.extraData = options.data;
        qx = $.param(options.data, traditional);
    }

    // give pre-submit callback an opportunity to abort the submit
    if (options.beforeSubmit && options.beforeSubmit(a, this, options) === false) {
        log('ajaxSubmit: submit aborted via beforeSubmit callback');
        return this;
    }

    // fire vetoable 'validate' event
    this.trigger('form-submit-validate', [a, this, options, veto]);
    if (veto.veto) {
        log('ajaxSubmit: submit vetoed via form-submit-validate trigger');
        return this;
    }

    var q = $.param(a, traditional);
    if (qx) {
        q = ( q ? (q + '&' + qx) : qx );
    }    
    if (options.type.toUpperCase() == 'GET') {
        options.url += (options.url.indexOf('?') >= 0 ? '&' : '?') + q;
        options.data = null;  // data is null for 'get'
    }
    else {
        options.data = q; // data is the query string for 'post'
    }

    var callbacks = [];
    if (options.resetForm) {
        callbacks.push(function() { $form.resetForm(); });
    }
    if (options.clearForm) {
        callbacks.push(function() { $form.clearForm(options.includeHidden); });
    }

    // perform a load on the target only if dataType is not provided
    if (!options.dataType && options.target) {
        var oldSuccess = options.success || function(){};
        callbacks.push(function(data) {
            var fn = options.replaceTarget ? 'replaceWith' : 'html';
            $(options.target)[fn](data).each(oldSuccess, arguments);
        });
    }
    else if (options.success) {
        callbacks.push(options.success);
    }

    options.success = function(data, status, xhr) { // jQuery 1.4+ passes xhr as 3rd arg
        var context = options.context || this ;    // jQuery 1.4+ supports scope context 
        for (var i=0, max=callbacks.length; i < max; i++) {
            callbacks[i].apply(context, [data, status, xhr || $form, $form]);
        }
    };

    // are there files to upload?
    var fileInputs = $('input:file:enabled[value]', this); // [value] (issue #113)
    var hasFileInputs = fileInputs.length > 0;
    var mp = 'multipart/form-data';
    var multipart = ($form.attr('enctype') == mp || $form.attr('encoding') == mp);

    var fileAPI = feature.fileapi && feature.formdata;
    log("fileAPI :" + fileAPI);
    var shouldUseFrame = (hasFileInputs || multipart) && !fileAPI;

    // options.iframe allows user to force iframe mode
    // 06-NOV-09: now defaulting to iframe mode if file input is detected
    if (options.iframe !== false && (options.iframe || shouldUseFrame)) {
        // hack to fix Safari hang (thanks to Tim Molendijk for this)
        // see:  http://groups.google.com/group/jquery-dev/browse_thread/thread/36395b7ab510dd5d
        if (options.closeKeepAlive) {
            $.get(options.closeKeepAlive, function() {
                fileUploadIframe(a);
            });
        }
          else {
            fileUploadIframe(a);
          }
    }
    else if ((hasFileInputs || multipart) && fileAPI) {
        fileUploadXhr(a);
    }
    else {
        $.ajax(options);
    }

    // clear element array
    for (var k=0; k < elements.length; k++)
        elements[k] = null;

    // fire 'notify' event
    this.trigger('form-submit-notify', [this, options]);
    return this;

     // XMLHttpRequest Level 2 file uploads (big hat tip to francois2metz)
    function fileUploadXhr(a) {
        var formdata = new FormData();

        for (var i=0; i < a.length; i++) {
            formdata.append(a[i].name, a[i].value);
        }

        if (options.extraData) {
            for (var p in options.extraData)
                if (options.extraData.hasOwnProperty(p))
                    formdata.append(p, options.extraData[p]);
        }

        options.data = null;

        var s = $.extend(true, {}, $.ajaxSettings, options, {
            contentType: false,
            processData: false,
            cache: false,
            type: 'POST'
        });
        
        if (options.uploadProgress) {
            // workaround because jqXHR does not expose upload property
            s.xhr = function() {
                var xhr = jQuery.ajaxSettings.xhr();
                if (xhr.upload) {
                    xhr.upload.onprogress = function(event) {
                        var percent = 0;
                        var position = event.loaded || event.position; /*event.position is deprecated*/
                        var total = event.total;
                        if (event.lengthComputable) {
                            percent = Math.ceil(position / total * 100);
                        }
                        options.uploadProgress(event, position, total, percent);
                    };
                }
                return xhr;
            };
        }

        s.data = null;
            var beforeSend = s.beforeSend;
            s.beforeSend = function(xhr, o) {
                o.data = formdata;
                if(beforeSend)
                    beforeSend.call(this, xhr, o);
        };
        $.ajax(s);
    }

    // private function for handling file uploads (hat tip to YAHOO!)
    function fileUploadIframe(a) {
        var form = $form[0], el, i, s, g, id, $io, io, xhr, sub, n, timedOut, timeoutHandle;
        var useProp = !!$.fn.prop;

        if ($(':input[name=submit],:input[id=submit]', form).length) {
            // if there is an input with a name or id of 'submit' then we won't be
            // able to invoke the submit fn on the form (at least not x-browser)
            alert('Error: Form elements must not have name or id of "submit".');
            return;
        }
        
        if (a) {
            // ensure that every serialized input is still enabled
            for (i=0; i < elements.length; i++) {
                el = $(elements[i]);
                if ( useProp )
                    el.prop('disabled', false);
                else
                    el.removeAttr('disabled');
            }
        }

        s = $.extend(true, {}, $.ajaxSettings, options);
        s.context = s.context || s;
        id = 'jqFormIO' + (new Date().getTime());
        if (s.iframeTarget) {
            $io = $(s.iframeTarget);
            n = $io.attr('name');
            if (!n)
                 $io.attr('name', id);
            else
                id = n;
        }
        else {
            $io = $('<iframe name="' + id + '" src="'+ s.iframeSrc +'" />');
            $io.css({ position: 'absolute', top: '-1000px', left: '-1000px' });
        }
        io = $io[0];


        xhr = { // mock object
            aborted: 0,
            responseText: null,
            responseXML: null,
            status: 0,
            statusText: 'n/a',
            getAllResponseHeaders: function() {},
            getResponseHeader: function() {},
            setRequestHeader: function() {},
            abort: function(status) {
                var e = (status === 'timeout' ? 'timeout' : 'aborted');
                log('aborting upload... ' + e);
                this.aborted = 1;
                // #214
                if (io.contentWindow.document.execCommand) { // IE browsers
                    io.contentWindow.document.execCommand('Stop');
                }
                else if (io.contentWindow.stop) { // other browsers
                    io.contentWindow.stop();
                }
                $io.attr('src', s.iframeSrc); // abort op in progress
                xhr.error = e;
                if (s.error)
                    s.error.call(s.context, xhr, e, status);
                if (g)
                    $.event.trigger("ajaxError", [xhr, s, e]);
                if (s.complete)
                    s.complete.call(s.context, xhr, e);
            }
        };

        g = s.global;
        // trigger ajax global events so that activity/block indicators work like normal
        if (g && 0 === $.active++) {
            $.event.trigger("ajaxStart");
        }
        if (g) {
            $.event.trigger("ajaxSend", [xhr, s]);
        }

        if (s.beforeSend && s.beforeSend.call(s.context, xhr, s) === false) {
            if (s.global) {
                $.active--;
            }
            return;
        }
        if (xhr.aborted) {
            return;
        }

        // add submitting element to data if we know it
        sub = form.clk;
        if (sub) {
            n = sub.name;
            if (n && !sub.disabled) {
                s.extraData = s.extraData || {};
                s.extraData[n] = sub.value;
                if (sub.type == "image") {
                    s.extraData[n+'.x'] = form.clk_x;
                    s.extraData[n+'.y'] = form.clk_y;
                }
            }
        }
        
        var CLIENT_TIMEOUT_ABORT = 1;
        var SERVER_ABORT = 2;

        function getDoc(frame) {
            var doc = frame.contentWindow ? frame.contentWindow.document : frame.contentDocument ? frame.contentDocument : frame.document;
            return doc;
        }
        
        // Rails CSRF hack (thanks to Yvan Barthelemy)
        var csrf_token = $('meta[name=csrf-token]').attr('content');
        var csrf_param = $('meta[name=csrf-param]').attr('content');
        if (csrf_param && csrf_token) {
            s.extraData = s.extraData || {};
            s.extraData[csrf_param] = csrf_token;
        }

        // take a breath so that pending repaints get some cpu time before the upload starts
        function doSubmit() {
            // make sure form attrs are set
            var t = $form.attr('target'), a = $form.attr('action');

            // update form attrs in IE friendly way
            form.setAttribute('target',id);
            if (!method) {
                form.setAttribute('method', 'POST');
            }
            if (a != s.url) {
                form.setAttribute('action', s.url);
            }

            // ie borks in some cases when setting encoding
            if (! s.skipEncodingOverride && (!method || /post/i.test(method))) {
                $form.attr({
                    encoding: 'multipart/form-data',
                    enctype:  'multipart/form-data'
                });
            }

            // support timout
            if (s.timeout) {
                timeoutHandle = setTimeout(function() { timedOut = true; cb(CLIENT_TIMEOUT_ABORT); }, s.timeout);
            }
            
            // look for server aborts
            function checkState() {
                try {
                    var state = getDoc(io).readyState;
                    log('state = ' + state);
                    if (state && state.toLowerCase() == 'uninitialized')
                        setTimeout(checkState,50);
                }
                catch(e) {
                    log('Server abort: ' , e, ' (', e.name, ')');
                    cb(SERVER_ABORT);
                    if (timeoutHandle)
                        clearTimeout(timeoutHandle);
                    timeoutHandle = undefined;
                }
            }

            // add "extra" data to form if provided in options
            var extraInputs = [];
            try {
                if (s.extraData) {
                    for (var n in s.extraData) {
                        if (s.extraData.hasOwnProperty(n)) {
                           // if using the $.param format that allows for multiple values with the same name
                           if($.isPlainObject(s.extraData[n]) && s.extraData[n].hasOwnProperty('name') && s.extraData[n].hasOwnProperty('value')) {
                               extraInputs.push(
                               $('<input type="hidden" name="'+s.extraData[n].name+'">').attr('value',s.extraData[n].value)
                                   .appendTo(form)[0]);
                           } else {
                               extraInputs.push(
                               $('<input type="hidden" name="'+n+'">').attr('value',s.extraData[n])
                                   .appendTo(form)[0]);
                           }
                        }
                    }
                }

                if (!s.iframeTarget) {
                    // add iframe to doc and submit the form
                    $io.appendTo('body');
                    if (io.attachEvent)
                        io.attachEvent('onload', cb);
                    else
                        io.addEventListener('load', cb, false);
                }
                setTimeout(checkState,15);
                form.submit();
            }
            finally {
                // reset attrs and remove "extra" input elements
                form.setAttribute('action',a);
                if(t) {
                    form.setAttribute('target', t);
                } else {
                    $form.removeAttr('target');
                }
                $(extraInputs).remove();
            }
        }

        if (s.forceSync) {
            doSubmit();
        }
        else {
            setTimeout(doSubmit, 10); // this lets dom updates render
        }

        var data, doc, domCheckCount = 50, callbackProcessed;

        function cb(e) {
            if (xhr.aborted || callbackProcessed) {
                return;
            }
            try {
                doc = getDoc(io);
            }
            catch(ex) {
                log('cannot access response document: ', ex);
                e = SERVER_ABORT;
            }
            if (e === CLIENT_TIMEOUT_ABORT && xhr) {
                xhr.abort('timeout');
                return;
            }
            else if (e == SERVER_ABORT && xhr) {
                xhr.abort('server abort');
                return;
            }

            if (!doc || doc.location.href == s.iframeSrc) {
                // response not received yet
                if (!timedOut)
                    return;
            }
            if (io.detachEvent)
                io.detachEvent('onload', cb);
            else    
                io.removeEventListener('load', cb, false);

            var status = 'success', errMsg;
            try {
                if (timedOut) {
                    throw 'timeout';
                }

                var isXml = s.dataType == 'xml' || doc.XMLDocument || $.isXMLDoc(doc);
                log('isXml='+isXml);
                if (!isXml && window.opera && (doc.body === null || !doc.body.innerHTML)) {
                    if (--domCheckCount) {
                        // in some browsers (Opera) the iframe DOM is not always traversable when
                        // the onload callback fires, so we loop a bit to accommodate
                        log('requeing onLoad callback, DOM not available');
                        setTimeout(cb, 250);
                        return;
                    }
                    // let this fall through because server response could be an empty document
                    //log('Could not access iframe DOM after mutiple tries.');
                    //throw 'DOMException: not available';
                }

                //log('response detected');
                var docRoot = doc.body ? doc.body : doc.documentElement;
                xhr.responseText = docRoot ? docRoot.innerHTML : null;
                xhr.responseXML = doc.XMLDocument ? doc.XMLDocument : doc;
                if (isXml)
                    s.dataType = 'xml';
                xhr.getResponseHeader = function(header){
                    var headers = {'content-type': s.dataType};
                    return headers[header];
                };
                // support for XHR 'status' & 'statusText' emulation :
                if (docRoot) {
                    xhr.status = Number( docRoot.getAttribute('status') ) || xhr.status;
                    xhr.statusText = docRoot.getAttribute('statusText') || xhr.statusText;
                }

                var dt = (s.dataType || '').toLowerCase();
                var scr = /(json|script|text)/.test(dt);
                if (scr || s.textarea) {
                    // see if user embedded response in textarea
                    var ta = doc.getElementsByTagName('textarea')[0];
                    if (ta) {
                        xhr.responseText = ta.value;
                        // support for XHR 'status' & 'statusText' emulation :
                        xhr.status = Number( ta.getAttribute('status') ) || xhr.status;
                        xhr.statusText = ta.getAttribute('statusText') || xhr.statusText;
                    }
                    else if (scr) {
                        // account for browsers injecting pre around json response
                        var pre = doc.getElementsByTagName('pre')[0];
                        var b = doc.getElementsByTagName('body')[0];
                        if (pre) {
                            xhr.responseText = pre.textContent ? pre.textContent : pre.innerText;
                        }
                        else if (b) {
                            xhr.responseText = b.textContent ? b.textContent : b.innerText;
                        }
                    }
                }
                else if (dt == 'xml' && !xhr.responseXML && xhr.responseText) {
                    xhr.responseXML = toXml(xhr.responseText);
                }

                try {
                    data = httpData(xhr, dt, s);
                }
                catch (e) {
                    status = 'parsererror';
                    xhr.error = errMsg = (e || status);
                }
            }
            catch (e) {
                log('error caught: ',e);
                status = 'error';
                xhr.error = errMsg = (e || status);
            }

            if (xhr.aborted) {
                log('upload aborted');
                status = null;
            }

            if (xhr.status) { // we've set xhr.status
                status = (xhr.status >= 200 && xhr.status < 300 || xhr.status === 304) ? 'success' : 'error';
            }

            // ordering of these callbacks/triggers is odd, but that's how $.ajax does it
            if (status === 'success') {
                if (s.success)
                    s.success.call(s.context, data, 'success', xhr);
                if (g)
                    $.event.trigger("ajaxSuccess", [xhr, s]);
            }
            else if (status) {
                if (errMsg === undefined)
                    errMsg = xhr.statusText;
                if (s.error)
                    s.error.call(s.context, xhr, status, errMsg);
                if (g)
                    $.event.trigger("ajaxError", [xhr, s, errMsg]);
            }

            if (g)
                $.event.trigger("ajaxComplete", [xhr, s]);

            if (g && ! --$.active) {
                $.event.trigger("ajaxStop");
            }

            if (s.complete)
                s.complete.call(s.context, xhr, status);

            callbackProcessed = true;
            if (s.timeout)
                clearTimeout(timeoutHandle);

            // clean up
            setTimeout(function() {
                if (!s.iframeTarget)
                    $io.remove();
                xhr.responseXML = null;
            }, 100);
        }

        var toXml = $.parseXML || function(s, doc) { // use parseXML if available (jQuery 1.5+)
            if (window.ActiveXObject) {
                doc = new ActiveXObject('Microsoft.XMLDOM');
                doc.async = 'false';
                doc.loadXML(s);
            }
            else {
                doc = (new DOMParser()).parseFromString(s, 'text/xml');
            }
            return (doc && doc.documentElement && doc.documentElement.nodeName != 'parsererror') ? doc : null;
        };
        var parseJSON = $.parseJSON || function(s) {
            /*jslint evil:true */
            return window['eval']('(' + s + ')');
        };

        var httpData = function( xhr, type, s ) { // mostly lifted from jq1.4.4

            var ct = xhr.getResponseHeader('content-type') || '',
                xml = type === 'xml' || !type && ct.indexOf('xml') >= 0,
                data = xml ? xhr.responseXML : xhr.responseText;

            if (xml && data.documentElement.nodeName === 'parsererror') {
                if ($.error)
                    $.error('parsererror');
            }
            if (s && s.dataFilter) {
                data = s.dataFilter(data, type);
            }
            if (typeof data === 'string') {
                if (type === 'json' || !type && ct.indexOf('json') >= 0) {
                    data = parseJSON(data);
                } else if (type === "script" || !type && ct.indexOf("javascript") >= 0) {
                    $.globalEval(data);
                }
            }
            return data;
        };
    }
};

/**
 * ajaxForm() provides a mechanism for fully automating form submission.
 *
 * The advantages of using this method instead of ajaxSubmit() are:
 *
 * 1: This method will include coordinates for <input type="image" /> elements (if the element
 *    is used to submit the form).
 * 2. This method will include the submit element's name/value data (for the element that was
 *    used to submit the form).
 * 3. This method binds the submit() method to the form for you.
 *
 * The options argument for ajaxForm works exactly as it does for ajaxSubmit.  ajaxForm merely
 * passes the options argument along after properly binding events for submit elements and
 * the form itself.
 */
$.fn.ajaxForm = function(options) {
    options = options || {};
    options.delegation = options.delegation && $.isFunction($.fn.on);
    
    // in jQuery 1.3+ we can fix mistakes with the ready state
    if (!options.delegation && this.length === 0) {
        var o = { s: this.selector, c: this.context };
        if (!$.isReady && o.s) {
            log('DOM not ready, queuing ajaxForm');
            $(function() {
                $(o.s,o.c).ajaxForm(options);
            });
            return this;
        }
        // is your DOM ready?  http://docs.jquery.com/Tutorials:Introducing_$(document).ready()
        log('terminating; zero elements found by selector' + ($.isReady ? '' : ' (DOM not ready)'));
        return this;
    }

    if ( options.delegation ) {
        $(document)
            .off('submit.form-plugin', this.selector, doAjaxSubmit)
            .off('click.form-plugin', this.selector, captureSubmittingElement)
            .on('submit.form-plugin', this.selector, options, doAjaxSubmit)
            .on('click.form-plugin', this.selector, options, captureSubmittingElement);
        return this;
    }

    return this.ajaxFormUnbind()
        .bind('submit.form-plugin', options, doAjaxSubmit)
        .bind('click.form-plugin', options, captureSubmittingElement);
};

// private event handlers    
function doAjaxSubmit(e) {
    /*jshint validthis:true */
    var options = e.data;
    if (!e.isDefaultPrevented()) { // if event has been canceled, don't proceed
        e.preventDefault();
        $(this).ajaxSubmit(options);
    }
}
    
function captureSubmittingElement(e) {
    /*jshint validthis:true */
    var target = e.target;
    var $el = $(target);
    if (!($el.is(":submit,input:image"))) {
        // is this a child element of the submit el?  (ex: a span within a button)
        var t = $el.closest(':submit');
        if (t.length === 0) {
            return;
        }
        target = t[0];
    }
    var form = this;
    form.clk = target;
    if (target.type == 'image') {
        if (e.offsetX !== undefined) {
            form.clk_x = e.offsetX;
            form.clk_y = e.offsetY;
        } else if (typeof $.fn.offset == 'function') {
            var offset = $el.offset();
            form.clk_x = e.pageX - offset.left;
            form.clk_y = e.pageY - offset.top;
        } else {
            form.clk_x = e.pageX - target.offsetLeft;
            form.clk_y = e.pageY - target.offsetTop;
        }
    }
    // clear form vars
    setTimeout(function() { form.clk = form.clk_x = form.clk_y = null; }, 100);
}


// ajaxFormUnbind unbinds the event handlers that were bound by ajaxForm
$.fn.ajaxFormUnbind = function() {
    return this.unbind('submit.form-plugin click.form-plugin');
};

/**
 * formToArray() gathers form element data into an array of objects that can
 * be passed to any of the following ajax functions: $.get, $.post, or load.
 * Each object in the array has both a 'name' and 'value' property.  An example of
 * an array for a simple login form might be:
 *
 * [ { name: 'username', value: 'jresig' }, { name: 'password', value: 'secret' } ]
 *
 * It is this array that is passed to pre-submit callback functions provided to the
 * ajaxSubmit() and ajaxForm() methods.
 */
$.fn.formToArray = function(semantic, elements) {
    var a = [];
    if (this.length === 0) {
        return a;
    }

    var form = this[0];
    var els = semantic ? form.getElementsByTagName('*') : form.elements;
    if (!els) {
        return a;
    }

    var i,j,n,v,el,max,jmax;
    for(i=0, max=els.length; i < max; i++) {
        el = els[i];
        n = el.name;
        if (!n) {
            continue;
        }

        if (semantic && form.clk && el.type == "image") {
            // handle image inputs on the fly when semantic == true
            if(!el.disabled && form.clk == el) {
                a.push({name: n, value: $(el).val(), type: el.type });
                a.push({name: n+'.x', value: form.clk_x}, {name: n+'.y', value: form.clk_y});
            }
            continue;
        }

        v = $.fieldValue(el, true);
        if (v && v.constructor == Array) {
            if (elements) 
                elements.push(el);
            for(j=0, jmax=v.length; j < jmax; j++) {
                a.push({name: n, value: v[j]});
            }
        }
        else if (feature.fileapi && el.type == 'file' && !el.disabled) {
            if (elements) 
                elements.push(el);
            var files = el.files;
            if (files.length) {
                for (j=0; j < files.length; j++) {
                    a.push({name: n, value: files[j], type: el.type});
                }
            }
            else {
                // #180
                a.push({ name: n, value: '', type: el.type });
            }
        }
        else if (v !== null && typeof v != 'undefined') {
            if (elements) 
                elements.push(el);
            a.push({name: n, value: v, type: el.type, required: el.required});
        }
    }

    if (!semantic && form.clk) {
        // input type=='image' are not found in elements array! handle it here
        var $input = $(form.clk), input = $input[0];
        n = input.name;
        if (n && !input.disabled && input.type == 'image') {
            a.push({name: n, value: $input.val()});
            a.push({name: n+'.x', value: form.clk_x}, {name: n+'.y', value: form.clk_y});
        }
    }
    return a;
};

/**
 * Serializes form data into a 'submittable' string. This method will return a string
 * in the format: name1=value1&amp;name2=value2
 */
$.fn.formSerialize = function(semantic) {
    //hand off to jQuery.param for proper encoding
    return $.param(this.formToArray(semantic));
};

/**
 * Serializes all field elements in the jQuery object into a query string.
 * This method will return a string in the format: name1=value1&amp;name2=value2
 */
$.fn.fieldSerialize = function(successful) {
    var a = [];
    this.each(function() {
        var n = this.name;
        if (!n) {
            return;
        }
        var v = $.fieldValue(this, successful);
        if (v && v.constructor == Array) {
            for (var i=0,max=v.length; i < max; i++) {
                a.push({name: n, value: v[i]});
            }
        }
        else if (v !== null && typeof v != 'undefined') {
            a.push({name: this.name, value: v});
        }
    });
    //hand off to jQuery.param for proper encoding
    return $.param(a);
};

/**
 * Returns the value(s) of the element in the matched set.  For example, consider the following form:
 *
 *  <form><fieldset>
 *      <input name="A" type="text" />
 *      <input name="A" type="text" />
 *      <input name="B" type="checkbox" value="B1" />
 *      <input name="B" type="checkbox" value="B2"/>
 *      <input name="C" type="radio" value="C1" />
 *      <input name="C" type="radio" value="C2" />
 *  </fieldset></form>
 *
 *  var v = $(':text').fieldValue();
 *  // if no values are entered into the text inputs
 *  v == ['','']
 *  // if values entered into the text inputs are 'foo' and 'bar'
 *  v == ['foo','bar']
 *
 *  var v = $(':checkbox').fieldValue();
 *  // if neither checkbox is checked
 *  v === undefined
 *  // if both checkboxes are checked
 *  v == ['B1', 'B2']
 *
 *  var v = $(':radio').fieldValue();
 *  // if neither radio is checked
 *  v === undefined
 *  // if first radio is checked
 *  v == ['C1']
 *
 * The successful argument controls whether or not the field element must be 'successful'
 * (per http://www.w3.org/TR/html4/interact/forms.html#successful-controls).
 * The default value of the successful argument is true.  If this value is false the value(s)
 * for each element is returned.
 *
 * Note: This method *always* returns an array.  If no valid value can be determined the
 *    array will be empty, otherwise it will contain one or more values.
 */
$.fn.fieldValue = function(successful) {
    for (var val=[], i=0, max=this.length; i < max; i++) {
        var el = this[i];
        var v = $.fieldValue(el, successful);
        if (v === null || typeof v == 'undefined' || (v.constructor == Array && !v.length)) {
            continue;
        }
        if (v.constructor == Array)
            $.merge(val, v);
        else
            val.push(v);
    }
    return val;
};

/**
 * Returns the value of the field element.
 */
$.fieldValue = function(el, successful) {
    var n = el.name, t = el.type, tag = el.tagName.toLowerCase();
    if (successful === undefined) {
        successful = true;
    }

    if (successful && (!n || el.disabled || t == 'reset' || t == 'button' ||
        (t == 'checkbox' || t == 'radio') && !el.checked ||
        (t == 'submit' || t == 'image') && el.form && el.form.clk != el ||
        tag == 'select' && el.selectedIndex == -1)) {
            return null;
    }

    if (tag == 'select') {
        var index = el.selectedIndex;
        if (index < 0) {
            return null;
        }
        var a = [], ops = el.options;
        var one = (t == 'select-one');
        var max = (one ? index+1 : ops.length);
        for(var i=(one ? index : 0); i < max; i++) {
            var op = ops[i];
            if (op.selected) {
                var v = op.value;
                if (!v) { // extra pain for IE...
                    v = (op.attributes && op.attributes['value'] && !(op.attributes['value'].specified)) ? op.text : op.value;
                }
                if (one) {
                    return v;
                }
                a.push(v);
            }
        }
        return a;
    }
    return $(el).val();
};

/**
 * Clears the form data.  Takes the following actions on the form's input fields:
 *  - input text fields will have their 'value' property set to the empty string
 *  - select elements will have their 'selectedIndex' property set to -1
 *  - checkbox and radio inputs will have their 'checked' property set to false
 *  - inputs of type submit, button, reset, and hidden will *not* be effected
 *  - button elements will *not* be effected
 */
$.fn.clearForm = function(includeHidden) {
    return this.each(function() {
        $('input,select,textarea', this).clearFields(includeHidden);
    });
};

/**
 * Clears the selected form elements.
 */
$.fn.clearFields = $.fn.clearInputs = function(includeHidden) {
    var re = /^(?:color|date|datetime|email|month|number|password|range|search|tel|text|time|url|week)$/i; // 'hidden' is not in this list
    return this.each(function() {
        var t = this.type, tag = this.tagName.toLowerCase();
        if (re.test(t) || tag == 'textarea') {
            this.value = '';
        }
        else if (t == 'checkbox' || t == 'radio') {
            this.checked = false;
        }
        else if (tag == 'select') {
            this.selectedIndex = -1;
        }
        else if (includeHidden) {
            // includeHidden can be the value true, or it can be a selector string
            // indicating a special test; for example:
            //  $('#myForm').clearForm('.special:hidden')
            // the above would clean hidden inputs that have the class of 'special'
            if ( (includeHidden === true && /hidden/.test(t)) ||
                 (typeof includeHidden == 'string' && $(this).is(includeHidden)) )
                this.value = '';
        }
    });
};

/**
 * Resets the form data.  Causes all form elements to be reset to their original value.
 */
$.fn.resetForm = function() {
    return this.each(function() {
        // guard against an input with the name of 'reset'
        // note that IE reports the reset function as an 'object'
        if (typeof this.reset == 'function' || (typeof this.reset == 'object' && !this.reset.nodeType)) {
            this.reset();
        }
    });
};

/**
 * Enables or disables any matching elements.
 */
$.fn.enable = function(b) {
    if (b === undefined) {
        b = true;
    }
    return this.each(function() {
        this.disabled = !b;
    });
};

/**
 * Checks/unchecks any matching checkboxes or radio buttons and
 * selects/deselects and matching option elements.
 */
$.fn.selected = function(select) {
    if (select === undefined) {
        select = true;
    }
    return this.each(function() {
        var t = this.type;
        if (t == 'checkbox' || t == 'radio') {
            this.checked = select;
        }
        else if (this.tagName.toLowerCase() == 'option') {
            var $sel = $(this).parent('select');
            if (select && $sel[0] && $sel[0].type == 'select-one') {
                // deselect all other options
                $sel.find('option').selected(false);
            }
            this.selected = select;
        }
    });
};

// expose debug var
$.fn.ajaxSubmit.debug = false;

// helper fn for console logging
function log() {
    if (!$.fn.ajaxSubmit.debug) 
        return;
    var msg = '[jquery.form] ' + Array.prototype.join.call(arguments,'');
    if (window.console && window.console.log) {
        window.console.log(msg);
    }
    else if (window.opera && window.opera.postError) {
        window.opera.postError(msg);
    }
}

})(jQuery);

/*	jQuery json2form Plugin 
 *	version: 1.0 (2011-03-01)
 *
 * 	Copyright (c) 2011, Crystal, shimingxy@163.com
 * 	Dual licensed under the MIT and GPL licenses:
 * 	http://www.opensource.org/licenses/mit-license.php
 * 	http://www.gnu.org/licenses/gpl.html
 * 	Date: 2011-03-01 rev 1
 */
 ;(function ($) {
$.json2form = $.json2form||{};
$.fn.json2form = function(config ) { 
	var config=$.extend({
			url		:null,
			elem	:this.attr("id"),
			type	:'POST'
		}, config || {});

	if(config.url){
		$.ajax({type: config.type,url: config.url,data:$.extend({json2form:config.elem},config.data||{}),dataType: "json",async: false,
			success: function(data){
				config.data=data;
			}
		});
	}
	
	if(!$("#"+config.elem).attr("loadedInit")){//init checkbox radio and select element ,label
		if(config.data.init){
			for (var elem in config.data.init){
				var arrayData=config.data.init[elem];
				if($("#"+config.elem+" input[name='"+elem+"']")){
					var elemType=$("#"+config.elem+" input[name='"+elem+"']").attr("type");
					var elemName=$("#"+config.elem+" input[name='"+elem+"']").attr("name");
					var initElem=$("#"+config.elem+" input[name='"+elem+"']");
					switch(elemType){
						case "checkbox":
						case "radio":
							for (var initelem in arrayData){
								initElem.after('<input type="'+elemType+'"  name="'+elemName+'" value="'+arrayData[initelem].value+'" />'+arrayData[initelem].display);
							}
							initElem.remove();
							break;
					}
				} 
				if($("#"+config.elem+" select[name='"+elem+"']")){
					for (var initelem in arrayData){
						$("#"+config.elem+" select[name='"+elem+"']").append("<option value='"+arrayData[initelem].value+"'>"+arrayData[initelem].display+"</option>");
					}
				}
			}
		}
		if(config.data.label){//label
			$("#"+config.elem+" label").each(function(){
				var labelFor=$(this).attr("for");
				if(config.data.label[labelFor]){
					$(this).html(config.data.label[labelFor]);
				}
			});
		}
	}
	
	if(config.data){//input text password hidden button reset submit checkbox radio and select textarea
		$("#"+config.elem+" input,select,textarea").each(function(){
			var elemType=$(this).attr("type");
			var elemName=$(this).attr("name");
			var elemData=config.data[elemName];
			if(!$("#"+config.elem).attr("loadedInit")&&$(this).attr("url")){
				switch(elemType){
					case "checkbox":
					case "radio":
					case "select":
					case "select-one":
					case "select-multiple":{
						var _this =this;
						$.ajax({type: config.type,url: $(this).attr("url"),dataType: "json",async: false,success: function(data){
								for (var elem in data){
									if(elemType=="select"||elemType=="select-one"||elemType=="select-multiple"){
										$(_this).append("<option value='"+data[elem].value+"'>"+data[elem].display+"</option>");
									}else{
										$(_this).after('<input type="'+elemType+'"  name="'+elemName+'" value="'+data[elem].value+'" />'+data[elem].display);
									}
								}
								if(elemType=="checkbox"||elemType=="radio")$(_this).remove();
							}
						});
						break;
					}
				}
			}
			
			if(elemData){
				switch(elemType){
					case "text":
					case "password":
					case "hidden":
					case "button":
					case "reset":
					case "textarea":
					case "submit":{
						$(this).val(elemData);
						break;
					}
					case "checkbox":
					case "radio":{
						$(this).attr("checked","");
						if(elemData.constructor==Array){//checkbox multiple value is Array
							for (var elem in elemData){
								if(elemData[elem]==$(this).val()){
									$(this).attr("checked",true);
								}
							}
						}else{//radio or checkbox is a string single value
							if(elemData==$(this).val()){
								$(this).attr("checked",true);
							}
						}
						break;
					}
					case "select":
					case "select-one":
					case "select-multiple":{
						$(this).find("option:selected").attr("selected",false);
						if(elemData.constructor==Array){
							for (var elem in elemData){
								$(this).find("option[value='"+elemData[elem]+"']").attr("selected",true);
							}
						}else{
							$(this).find("option[value='"+elemData+"']").attr("selected",true);
						}
						break;
					}
				}
			}
		});
	}

	$("#"+config.elem).attr("loadedInit","true");//loadedInit is true,next invoke not need init checkbox radio and select element ,label
};
})(jQuery);
 $.fn.serializeObject = function() { 
	 var o = {}; 
	 var a = this.serializeArray(); 
	 $.each(a, function() { 
	 if (o[this.name]) { 
	 if (!o[this.name].push) { 
	 o[this.name] = [ o[this.name] ]; 
	 } 
	 o[this.name].push(this.value || ''); 
	 } else { 
	 o[this.name] = this.value || ''; 
	 } 
	 }); 
	 return o; 
	 };

 function paramString2obj (serializedParams) { 

	 var obj={}; 
	 function evalThem (str) { 
	 var attributeName = str.split("=")[0]; 
	 var attributeValue = str.split("=")[1]; 
	 if(!attributeValue){ 
	 return ; 
	 } 

	 var array = attributeName.split("."); 
	 for (var i = 1; i < array.length; i++) { 
	 var tmpArray = Array(); 
	 tmpArray.push("obj"); 
	 for (var j = 0; j < i; j++) { 
	 tmpArray.push(array[j]); 
	 }; 
	 var evalString = tmpArray.join("."); 
	 // alert(evalString); 
	 if(!eval(evalString)){ eval(evalString+"={};"); 
	 } 
	 }; 
	 eval("obj."+attributeName+"='"+attributeValue+"';"); 

	 }; 
	 var properties = serializedParams.split("&"); 
	 for (var i = 0; i < properties.length; i++) { 
	 evalThem(properties[i]); 
	 }; 
	 return obj; 
	 } 
	 $.fn.form2json = function(){ 
		 var serializedParams = this.serialize(); 
		 var obj = paramString2obj(serializedParams); 
		 return JSON.stringify(obj); 
	 };
	 function formToJSON(formid) {
      	var fields = $("#"+formid).serializeArray();
      	var jsonObj = {};
      	/*for(var i = 0; i < fields.length; i++) {
      		var tmpVal = fields[i].value;
      		if($.isArray(tmpVal)) {
      			jsonObj[fields[i].name] = tmpVal.join(",");
      		} else {
      			jsonObj[fields[i].name] = tmpVal;
      		}
      	}*/
      	$.each(fields, function(k, v) {
      		 if (jsonObj[v.name]) {
	      		 /*if (!jsonObj[v.name].push) {
	      		 	jsonObj[v.name] = [jsonObj[v.name]];
	      		 }
	      		 jsonObj[v.name].push(v.value || ''); */
      			 if(v.value) {
      				jsonObj[v.name] = jsonObj[v.name] + "," + v.value;
      			 }
      		 } else { 
      			 jsonObj[v.name] = v.value || ''; 
      		 } 
      	}); 
      	return jsonObj;
      }

/**
 * Copyright (c) 2009 Sergiy Kovalchuk (serg472@gmail.com)
 * 
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 *  
 * Following code is based on Element.mask() implementation from ExtJS framework (http://extjs.com/)
 *
 */
(function(a){a.fn.mask=function(c,b){a(this).each(function(){if(b!==undefined&&b>0){var d=a(this);d.data("_mask_timeout",setTimeout(function(){a.maskElement(d,c)},b))}else{a.maskElement(a(this),c)}})};a.fn.unmask=function(){a(this).each(function(){a.unmaskElement(a(this))})};a.fn.isMasked=function(){return this.hasClass("masked")};a.maskElement=function(d,c){if(d.data("_mask_timeout")!==undefined){clearTimeout(d.data("_mask_timeout"));d.removeData("_mask_timeout")}if(d.isMasked()){a.unmaskElement(d)}if(d.css("position")=="static"){d.addClass("masked-relative")}d.addClass("masked");var e=a('<div class="loadmask"></div>');if(navigator.userAgent.toLowerCase().indexOf("msie")>-1){e.height(d.height()+parseInt(d.css("padding-top"))+parseInt(d.css("padding-bottom")));e.width(d.width()+parseInt(d.css("padding-left"))+parseInt(d.css("padding-right")))}if(navigator.userAgent.toLowerCase().indexOf("msie 6")>-1){d.find("select").addClass("masked-hidden")}d.append(e);if(c!==undefined){var b=a('<div class="loadmask-msg" style="display:none;"></div>');b.append("<div>"+c+"</div>");d.append(b);b.css("top",Math.round(d.height()/2-(b.height()-parseInt(b.css("padding-top"))-parseInt(b.css("padding-bottom")))/2)+"px");b.css("left",Math.round(d.width()/2-(b.width()-parseInt(b.css("padding-left"))-parseInt(b.css("padding-right")))/2)+"px");b.show()}};a.unmaskElement=function(b){if(b.data("_mask_timeout")!==undefined){clearTimeout(b.data("_mask_timeout"));b.removeData("_mask_timeout")}b.find(".loadmask-msg,.loadmask").remove();b.removeClass("masked");b.removeClass("masked-relative");b.find("select").removeClass("masked-hidden")}})(jQuery);
/**
 * 将form表单序列化成key:value形式
 */
jQuery.fn.extend({
	serializeArrayToParam: function() {
		var v = {};
		var o = this.serializeArray();
		for (var i in o) {
			if(o[i].value) {
				if (typeof (v[o[i].name]) == 'undefined') {
					v[o[i].name] = o[i].value;
				} else if(v[o[i].name] instanceof Array) {
					v[o[i].name].push(o[i].value);
				} else {
					var t = v[o[i].name];
					v[o[i].name] = [];
					v[o[i].name].push(t);
					v[o[i].name].push(o[i].value);
				}
			}
		}
		for(var i in v) {
			var ss = v[i];
			if(ss instanceof Array) {
				v[i] = ss.join(",");
			}
		}
		return v;
  },serializeArrayToParamStr: function() {
	  //去除空值参数
	  var v = [];
	  var o = this.serializeArray();
	  for (var i in o) {
		  var val = o[i].value;	
		  if(val) {
			v.push(o[i].name);
			v.push("=");
			v.push(val);
		  }
	  }	
	  return v.join("&");
  },insert:function(value) {
	    value=$.extend({ "text":"[]" },value); 
		var dthis = $(this)[0]; //将jQuery对象转换为DOM元素 
		//IE下 
		if(document.selection){ 
			$(dthis).focus(); //输入元素textara获取焦点 
			var fus = document.selection.createRange();//获取光标位置 
			fus.text = value.text; //在光标位置插入值 
			$(dthis).focus(); ///输入元素textara获取焦点 
		} //火狐，google下标准 
		else if(dthis.selectionStart || dthis.selectionStart == '0'){ 
			var start = dthis.selectionStart; //获取焦点前坐标 
			var end =dthis.selectionEnd; //获取焦点后坐标 
			//以下这句，应该是在焦点之前，和焦点之后的位置，中间插入我们传入的值 .然后把这个得到的新值，赋给文本框 
			dthis.value = dthis.value.substring(0, start) + value.text + dthis.value.substring(end, dthis.value.length); 
			$(dthis).focus();
		} 
		//在输入元素textara没有定位光标的情况 
		else{ 
			this.val(this.val() + value.text); this.focus();
		}
		return $(this); 
  }
});

function fnDoReLogin() {
	setTimeout(function() {
		if(window.top.doLogin) {
			window.top.doLogin();
		}
	} , 2000);
}

/**
 * 封装ajax方法，作统一异常错误处理
 * @param options
 */
function fnFormAjaxWithJson(options, notShowProgress){
	//默认显示进度条，当notShowProgress为true时，不显示
	notShowProgress = notShowProgress || false;
	var tmpFun = options.success;
	var callBackFun = function(data){
		if(!notShowProgress) {
			//$.messager.progress('close');
			$(document.body).unmask();
		}
		if(typeof tmpFun==='function'&&tmpFun){
			if(data) {
				if(data.flag=="1" || data.rows || $.isArray(data)) {
					tmpFun(data);
				} else if(data.flag=="3") {
					$.messager.alert($.jwpf.system.alertinfo.errorTitle, data.msg ,'error');
					fnDoReLogin();
				} else {
					$.messager.alert($.jwpf.system.alertinfo.errorTitle, data.msg ,'error');
				}
			}
		}
	};
	options.success = undefined;
	options = $.extend({
		url: false,
		type : 'post',
		dataType : 'json',
		success:callBackFun,
		beforeSend:function(xhr) {
			if(!notShowProgress) {
				//$.messager.progress({text:$.jwpf.system.alertinfo.attachList,interval:20});
				$(document.body).mask("Please Waiting...");
			}
		},
		error:function(xhr, ts, errorThrown) {
			if(!notShowProgress) {
				//$.messager.progress('close');
				$(document.body).unmask();
			}
			$.messager.alert($.jwpf.system.alertinfo.errorTitle,$.jwpf.system.alertinfo.errorInfo,'error');
		},
		complete: function (XMLHttpRequest, textStatus) {
			//$.messager.progress('close');
		}
		},options);
	$.ajax(options);
}

/**
 * EasyUI form提交方法
 * @param formId
 * @param options
 */
function fnEasyUIFormWithJson(formId, options){
	var tmpFun = options.success;
	var callBackFun = function(result){
		if(typeof tmpFun==='function'&&tmpFun){
			if(result) {
				var data = $.parseJSON(result);
				if(data.flag=="1") {
					tmpFun(data);
				} else if(data.flag=="3") {
					$.messager.alert($.jwpf.system.alertinfo.errorTitle, data.msg ,'error');
					fnDoReLogin();
				} else {
					$.messager.alert($.jwpf.system.alertinfo.errorTitle, data.msg ,'error');
				}
			}
		}
	};
	options.success = undefined;
	options = $.extend({
		onSubmit: function(){  
			return $(this).form('validate');
	    },
		success:callBackFun
		},options);
	$("#" + formId).form("submit", options);
}

/**
 * 初始化表单，预提交处理，form提交必须有submit类型按钮触发
 * @param formId 表单ID
 * @param options 可选项，参考$.ajax之options
 */
function fnAjaxFormWithJson(formId, options){
	var tmpFun = options.success;
	var callBackFun = function(data){
		if(typeof tmpFun==='function'&&tmpFun){
			if(data) {
				if(data.flag=="1") {
					tmpFun(data);
				} else {
					$.messager.alert($.jwpf.system.alertinfo.errorTitle, data.msg ,'error');
				}
			}
		}
	};
	options.success = undefined;
	options = $.extend({
		type : 'post',
		dataType : 'json',
		success:callBackFun
		},options);
	$("#" + formId).ajaxForm(options);
}

/**
 * 立即触发ajax表单提交事件，可以加到按钮点击事件中触发
 * @param formId 表单ID
 * @param options 可选项，参考$.ajax之options
 */
function fnAjaxSubmitWithJson(formId, options){
	var tmpFun = options.success;
	var callBackFun = function(data){
		//$.messager.progress('close');
		$(document.body).unmask();
		if(typeof tmpFun==='function'&&tmpFun){
			if(data) {
				if(data.flag=="1") {
					tmpFun(data);
				} else if(data.flag=="3") {
					$.messager.alert($.jwpf.system.alertinfo.errorTitle, data.msg ,'error');
					fnDoReLogin();
				} else {
					$.messager.alert($.jwpf.system.alertinfo.errorTitle, data.msg ,'error');
				}
			}
		}
	};
	options.success = undefined;
	options = $.extend({
		type : 'post',
		dataType : 'json',
		beforeSubmit: function(formData, jqForm, options) {
			return $("#" + formId).form('validate');
		},
		beforeSend:function(xhr) {
			//$.messager.progress({text:'$.jwpf.system.alertinfo.attachList',interval:20});
			$(document.body).mask("Please Waiting...");
		},
		error:function(xhr, ts, errorThrown) {
			//$.messager.progress('close');
			$(document.body).unmask();
			$.messager.alert($.jwpf.system.alertinfo.errorTitle, $.jwpf.system.alertinfo.errorInfo,'error');
		},
		success:callBackFun
		},options);
	$("#" + formId).ajaxSubmit(options);
}

/**
 * 格式化日期
 * @param dateStr yyyy-MM-dd HH:mm:ss格式的字符串
 * @param pattern 日期格式
 */
function fnFormatDate(dateStr, pattern) {
	if(dateStr) {
		try {
			var d = new Date(dateStr.replace(/-/g, "\/"));
			if(fnIsValidDate(d)) {
				return d.format(pattern);
			} else {
				return dateStr;
			}
		} catch(e) {
			return dateStr;
		}
	} else {
		return "";
	}
}

function fnIsValidDate(d) {
	if (Object.prototype.toString.call(d) !== "[object Date]" )
	    return false;
	return !isNaN(d.getTime());  
}

/**
 * 将回车换行符替换为<br/>符号
 * @param str
 */
function fnFormatText(str) {
	if(str) {
		return str.replace(/\r\n/gi,"<br/>").replace(/\n/gi,"<br/>");
	} else {
		return "";
	}
}

//时间格式化
Date.prototype.format = function(format){
   /*
  * eg:format="yyyy-MM-dd hh:mm:ss";
    */
   if(!format){
      format = "yyyy-MM-dd";
    }

  var o = {
           "M+": this.getMonth() + 1, // month
           "d+": this.getDate(), // day
           "h+": this.getHours(), // hour
           "H+": this.getHours(), // hour
           "m+": this.getMinutes(), // minute
           "s+": this.getSeconds(), // second
           "q+": Math.floor((this.getMonth() + 3) / 3), // quarter
            "S": this.getMilliseconds()
            // millisecond
   };

    if (/(y+)/.test(format)) {
       format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
   }

    for (var k in o) {
        if (new RegExp("(" + k + ")").test(format)) { 
            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" +o[k]).length));
        }
    }
    return format;
};	

/*
函数：计算两个日期之间的差值
参数：date是日期对象
	flag：ms-毫秒，s-秒，m-分，h-小时，d-天，M-月，y-年
返回：当前日期和date两个日期相差的毫秒/秒/分/小时/天
*/
Date.prototype.dateDiff = function (date, flag) 
{
	var msCount;
	var diff = this.getTime() - date.getTime();
	switch (flag) 
	{
		case "ms":
			msCount = 1;
			break;
		case "s":
			msCount = 1000;
			break;
		case "m":
			msCount = 60 * 1000;
			break;
		case "h":
			msCount = 60 * 60 * 1000;
			break;
		case "d":
			msCount = 24 * 60 * 60 * 1000;
			break;
	}
	return Math.floor(diff / msCount);
};

/*
函数：判断一个年份是否为闰年
返回：是否为闰年
*/
Date.prototype.isLeapYear = function () {
	var year = this.getFullYear();
	return (year % 4 == 0) && (year % 100 != 0 || year % 400 == 0);
};

//附件功能
function doAttach(opts) {
	if(!opts.attachId) {opts.attachId="";}
	opts.readOnly = opts.readOnly || false;
	var attUrl = __ctx+'/sys/attach/attach.action?1=1&entityName='+opts.entityName+"&bid=" +opts.id+"&key="+opts.key
				+ "&attachId=" + opts.attachId + "&dgId=" + opts.dgId + "&flag=" + opts.flag+"&readOnly="+opts.readOnly;
	if($("#attachWin").length == 0) {
		var p =	$("<div/>").appendTo("body");
		p.window({
			id:'attachWin',
			title:$.jwpf.system.attach.attachList,
			href:attUrl,
			width:600,
			height:400,
			closable:true,
			collapsible:true,
			maximizable:true,
			minimizable:false,
			modal: true,
			shadow: false,
			cache:false,
			closed:true,
			onClose:function() {
				$("#attachWin").window("destroy");
			}
		});
	} else {
		$("#attachWin").window("refresh", attUrl);
	}
	$("#attachWin").window("open");
}	

/**
 * 启动流程
 * @param id
 * @param tName
 * @param callFunc
 */
function  doStartProcess(id,tName,callFunc,approveDesp){
	var options = {
			url :__ctx+'/gs/process!startPrepare.action',
			data : {
				"bname":tName,
				"bid":id,
				"approveDesp":approveDesp
			},
			success : function(data) {
				if (data.msg) {
					var funName = callFunc.toString().match(/function\s+([^\(]+)/)[1];//传递一个js函数的名字
						var attUrl = __ctx+"/gs/process!init.action?roles="+encodeURIComponent(data.msg) +"&bname="+tName+"&bid="+id+"&fname="+funName;
						if($("#processWin").length == 0) {
							var p =	$("<div/>").appendTo("body");
							p.window({
								id:'processWin',
								title:$.jwpf.system.process.processStartWin,
								href: attUrl,
								width:500,
								height:400,
								closable:true,
								collapsible:true,
								maximizable:true,
								minimizable:false,
								modal: true,
								shadow: false,
								cache:false,
								closed:true,
								onClose:function() {
									$("#processWin").window("destroy");
								},
								onLoad:function() {
									$("#_start_flow_approve_desp_").val(approveDesp||"");
								}
							});
						} else {
							$("#processWin").window("refresh", attUrl);
						}
						$("#processWin").window("open");
				}else{
					 $.messager.alert($.jwpf.system.alertinfo.titlet, $.jwpf.system.process.startSuccess,'info');
					 if(callFunc) { setTimeout(callFunc, 2000);}
				}				
			}
		};
	fnFormAjaxWithJson(options,true);
}

//自定义发送消息通知给选择的用户
function doSendMessageToUser(type,callFunc){
		var attUrl = __ctx+'/gs/gs-mng!selUserWin.action?type='+type;
		var p =	$("<div style='overflow: auto;'/>").appendTo("body");
		p.dialog({
			id:'sel_usersWin',
			title:'选择提醒用户',
			href:attUrl,
			width:450,
			height:350,
			closable:true,
			collapsible:true,
			maximizable:true,
			minimizable:false,
			resizable:true,
			modal: true,
			shadow: false,
			cache:false,
			closed:true,
			onClose:function() {
				$("#sel_usersWin").dialog("destroy");
			},
			buttons:[{
				text:'确定',
				iconCls:'icon-ok',
				handler:function(){
					var zTree = $.fn.zTree.getZTreeObj("m_users_");
					if(zTree!=null){
					var nodes = zTree.getCheckedNodes(true);
					var nodeIds = [];
					$.each(nodes, function(k,v) {
						nodeIds.push(v.idNum);
					});
					var userIds = nodeIds.join(",");
					if(callFunc){
						callFunc(userIds);
						$('#sel_usersWin').dialog('close');
						}
					}				
				}
			},{
				text:'取消',
				iconCls:'icon-cancel',
				handler:function(){
					$('#sel_usersWin').dialog('close');
				}
			}]
		});
	$("#sel_usersWin").dialog("open");
}
/**
 * 打开字典树窗口，进行分类选择，暂时不用
 * @param opt
 */
function doOpenDictTreeWin(opt){
		var opts = $.extend({"title":"分类选择","width":450,"height":350} ,opt);
		var attUrl = __ctx+'/sys/commmon/dict-tree.action?type='+opts.type;
		var p =	$("<div style='overflow: auto;'/>").appendTo("body");
		p.dialog({
			id:'_sel_dictTree_dialog_',
			title:opts.title,
			href:attUrl,
			width:opts.width,
			height:opts.height,
			closable:true,
			collapsible:true,
			maximizable:true,
			minimizable:false,
			resizable:true,
			modal: true,
			shadow: false,
			cache:false,
			closed:true,
			onClose:function() {
				$("#_sel_dictTree_dialog_").dialog("destroy");
			},
			buttons:[{
				text:'确定',
				iconCls:'icon-ok',
				handler:function(){
					if(callFunc){
						var node = _getCommonTypeNode_();
						var obj = {"key":node.id,"caption":node.name};
						callFunc(obj);
						$('#_sel_dictTree_dialog_').dialog('close');
					}
				}
			},{
				text:'取消',
				iconCls:'icon-cancel',
				handler:function(){
					$('#_sel_dictTree_dialog_').dialog('close');
				}
			}]
		});
		$("#_sel_dictTree_dialog_").dialog("open");
}
/*
url: 转向网页的地址
name: 网页名称，可为空
iWidth: 弹出窗口的宽度
iHeight: 弹出窗口的高度
*/
function fnOpenWindow(url,name,iWidth,iHeight){
	//获得窗口的垂直位置
	var iTop = (window.screen.availHeight-30-iHeight)/2;
	//获得窗口的水平位置
	var iLeft = (window.screen.availWidth-10-iWidth)/2;
	window.open(url,name,'height='+iHeight+',,innerHeight='+iHeight+',width='+iWidth+',innerWidth='+iWidth+',top='+iTop+',left='+iLeft+',status=yes,toolbar=no,menubar=no,location=no,resizable=yes,scrollbars=yes,titlebar=no');
}
function fnOpenWindow2(url,name){
	window.open(url,name,'status=yes,toolbar=no,menubar=no,location=no,resizable=yes,scrollbars=yes,titlebar=no,fullscreen=3');
}

/***传入相应参数(sql)，返回json结果 */
function getJsonObjByUrl(param, url) {
	var jsonData=null;
	var opt = {
			data:param,
			async:false,
            url:url,
			success:function(data){
				jsonData=data.msg;
			}
	};
	fnFormAjaxWithJson(opt, true);
	return jsonData;
}
/**
 * 批量修改拥有者
 * @param rows 选中记录数据
 * @param winId 窗口Id
 * @param entityName
 * @param callFunc
 */
function doUpdateCreateUser(rows,entityName,callFunc) {
	 var ids = [];
	 if(rows && rows.length>0){
		 for(var i =0;i<rows.length;i++){
			ids.push(rows[i].id);
		 }        		
	 }
	var edittabs = $('#_user_sel_win').find("iframe");
	var editwin = edittabs[0].contentWindow.window;
	var userId = editwin.getUserId();
	var options = {
			url : __ctx+'/gs/gs-mng!updateCreateUser.action',
			data : {
				"entityName":entityName,
				"entityIds":ids,
				"field":"createUser",
				"value":userId
			},
			success : function(data) {
				if (data.msg) {
					if(callFunc){
						callFunc();
					}
					$('#_user_sel_win').window("close");
				}
			},
			traditional:true
		};
	fnFormAjaxWithJson(options, true);
}
/**
 * 重新恢复已审批记录的流程
 * @param entityId 
 * @param entityName
 */
function doRecoverProcess(id,entityName,callFunc) {
	var options = {
			url : __ctx+'/gs/process!recoverProcess.action',
			data : {
				"entityName":entityName,
				"id":id
			},
			success : function(data) {
				if(callFunc){
					callFunc();
				}
			}
		};
	fnFormAjaxWithJson(options, true);
}

/**
 * 打开自定义选择用户窗口
 * @param url 
 * @param title
 */
function doOpenUserWin(url,title) {
	if($("#_user_sel_win").length == 0){
		var p =	$("<div/>").appendTo("body");
		var ifr = [];
		var strUrl = url;
		var imgPath = __ctx+"/themes/default/blue/images/loading.gif";
		ifr.push('<iframe src="');
		ifr.push(imgPath);
		ifr.push('" frameborder="0" style="width:100%;height:100%" ></iframe>');
		var c = ifr.join("");
		p.window({
			id:'_user_sel_win',
			title:title,
			width:400,
			height:180,
			closable:true,
			content:c,
			modal: true,
			shadow: false,
			minimizable:false,
			cache:false,
			doSize :true,
			onOpen:function(){
				var ifWin = $('#_user_sel_win').find("iframe");
				ifWin[0].contentWindow.window.location.replace(strUrl);
			},
			onClose: function(){
				var ifWin = $('#_user_sel_win').find("iframe");
				ifWin[0].contentWindow.window.location.replace(imgPath);
			}
		});
	}
	$("#_user_sel_win").window("open");
}

/**
 * 打开Excel导入窗口
 * @param url 
 * @param title
 */
function doOpenExcelImportWin(entityName,title) {
	var winTitle = title || "Excel导入";
	var strUrl = __ctx+"/gs/gs-mng!importFile.action?entityName=" + entityName;
	doOpenModuleOperWin(strUrl, winTitle, 500, 300);
}

/**
 * 获取导出参数
 * @param formKey
 * @returns
 */
function getExportParams(formKey) {
	var opt = $("#"+formKey).datagrid("options");
	var param = opt["queryParams"];
	var sort = opt["sortName"];
	var order = opt["sortOrder"];
	var queryParam = $.extend(true, {"sort":sort,"order":order}, param);
	var strParam = JSON.toUrlParam(queryParam);
	return strParam;
}

/**
 * 导出Excel格式数据文件
 * @param entityName
 * @param ids
 */
function doExportExcel(entityName, queryParams, ids, notNeedConfirm) {
	var strUrl = __ctx+"/gs/gs-mng!exportModuleExcel.action?entityName=" + entityName + "&" + queryParams + "&value=" + ids;
	if(!ids) {
		if(notNeedConfirm) {
			window.open(strUrl);
		} else {
			$.messager.confirm('提示', '您没有选择数据，确认要导出全部数据吗？', function(r) {
	            if(r) {
	            	window.open(strUrl);
	            }
			});
		}
	} else {
		window.open(strUrl);
	}
}

/**
 * 打开模块操作窗口
 * @param url
 * @param title
 * @param width
 * @param height
 * @param isMaxWin
 */
function doOpenModuleOperWin(url, title, width, height, isMaxWin) {
	var winTitle = title || "操作窗口";
	var winWid = width || 800;
	var winHei = height || 600;
	var winMax = isMaxWin || false;
	var p = $("<div/>").appendTo("body");
	var ifr = [];
	var strUrl = url;
	var imgPath = __ctx+"/themes/default/blue/images/loading.gif";
	ifr.push('<iframe src="');
	ifr.push(imgPath);
	ifr.push('" frameborder="0" style="width:100%;height:100%" ></iframe>');
	var c = ifr.join("");
	p.window({
		id:'_module_oper_win',
		title:winTitle,
		width:winWid,
		height:winHei,
		closable:true,
		content:c,
		modal: true,
		shadow: false,
		minimizable:false,
		maximized:winMax,
		cache:false,
		doSize :true,
		onOpen:function(){
			var ifWin = $('#_module_oper_win').find("iframe");
			ifWin[0].contentWindow.window.location.replace(strUrl);
		},
		onClose: function(){
			$("#_module_oper_win").window("destroy");
		}
	});		
	$("#_module_oper_win").window("open");
}
/**
 * 关闭模块操作窗口
 */
function doCloseModuleOperWin() {
	$("#_module_oper_win").window("close");
}

/**
 * 打开模块操作对话框
 * @param url
 * @param title
 * @param width
 * @param height
 * @param callFunc
 * @param isMaxWin
 */
function doOpenModuleOperDialog(url, title, width, height, callFunc, isMaxWin) {
	var winTitle = title || "操作对话框";
	var winWid = width || 800;
	var winHei = height || 600;
	var winMax = isMaxWin || false;
	var p = $("<div style='overflow: auto;'/>").appendTo("body");
	p.dialog({
		id:'_module_oper_dialog',
		title:winTitle,
		href:url,
		width:winWid,
		height:winHei,
		closable:true,
		collapsible:true,
		maximizable:true,
		minimizable:false,
		maximized:winMax,
		resizable:true,
		modal: true,
		shadow: false,
		cache:false,
		closed:true,
		onClose:function() {
			$("#_module_oper_dialog").dialog("destroy");
		},
		buttons:[{
			text:'确定',
			iconCls:'icon-ok',
			handler:function(){
				if(callFunc) {
					callFunc();
				}
			}
		},{
			text:'取消',
			iconCls:'icon-cancel',
			handler:function(){
				doCloseModuleOperDialog();
			}
		}]
	});
	$("#_module_oper_dialog").dialog("open");
}

/**
 * 关闭模块操作对话框
 */
function doCloseModuleOperDialog() {
	$("#_module_oper_dialog").dialog("close");
}

/**
 * 打开产品选型窗口
 * @param callFunc
 */
function doOpenProductSelWin(param, callFunc){
	var fp = JSON.stringify(param);
	var attUrl = "/sys/project/kbt/dataselect.action?filterParam=" + encodeURIComponent(fp);
	window.top.openFormWin({
		title:"产品选型",
		url:attUrl,
		width:1000,
		height:600,
		onAfterSure:function(win) {
			var rowData= win._getSelectProductInfo_();
			if(rowData) {
				if(callFunc){
					callFunc(rowData);
				}
				return true;
			}
		}
	});
	/*var func = function() {
		var rowData= window.top._getSelectProductInfo_();
		if(rowData) {
			if(callFunc){
				callFunc(rowData);
			}
			window.top.doCloseModuleOperDialog();
		}
	};
	window.top.doOpenModuleOperDialog(attUrl, "产品选型", 1000, 650, func);*/
}

/**
 * 打开产品信息生成窗口
 * @param callFunc
 */
function doOpenProductGenWin(xhid, callFunc){
	var attUrl = "/sys/project/kbt/product-gen.action?xhid=" + xhid;
	window.top.openFormWin({
		title:"产品信息生成向导",
		url:attUrl,
		width:850,
		height:600,
		onAfterSure:function(win) {
			var result= win._saveProductInfo_();
			if(result) {
				if(callFunc){
					callFunc();
				}
				return true;
			}
		}
	});
	/*var func = function() {
		var result = window.top._saveProductInfo_();
		if(result) {
			if(callFunc) {
				callFunc();
			}
			window.top.doCloseModuleOperDialog();
		}
	};
	window.top.doOpenModuleOperDialog(attUrl, "产品信息生成向导", 850, 600, func);*/
}

//打开组织树
function doOpenOrganizationSelWin(callFunc){
		var orgUrl = __ctx+'/sys/common/organization-ztree.action';		
		var p =	$("<div style='overflow: auto;'/>").appendTo("body");
		p.dialog({
			id:'sel_orgWin',
			title:'选择组织',
			href:orgUrl,
			width:450,
			height:350,
			closable:true,
			collapsible:true,
			maximizable:true,
			minimizable:false,
			resizable:true,
			modal: true,
			shadow: false,
			cache:false,
			closed:true,
			onClose:function() {
				$("#sel_orgWin").dialog("destroy");
			},
			buttons:[{
				text:'确定',
				iconCls:'icon-ok',
				handler:function(){
					var zTree = $.fn.zTree.getZTreeObj("m_org_");
					if(zTree!=null){
					var nodes = zTree.getCheckedNodes(true);
					var nodeIds = [];
					$.each(nodes, function(k,v) {
						nodeIds.push(v.id);
					});
					var orgIds = nodeIds.join(",");
					if(callFunc){
						callFunc(orgIds);
						$('#sel_orgWin').dialog('close');
						}
					}				
				}
			},{
				text:'取消',
				iconCls:'icon-cancel',
				handler:function(){
					$('#sel_orgWin').dialog('close');
				}
			}]
		});
	$("#sel_orgWin").dialog("open");
}
/**
 * 打开自定义表单窗口
 * @param fromParam
 * 包含以下参数
 * formName
 * formKey
 * onAfterSure
 * width
 * height
 * isMaxWin
 * filterParam
 * id
 */
function openFormWin(formParam){
	var defaultParam = {
			"title":"操作对话框",
			"width":800,
			"height":600,
			"filterParam":{},
			"addParam":{},//新增参数
			"urlParam":null,//附加参数
			"isMaxWin":false,
			"isModal":true,//模式窗口
			"showTools":true,//显示按钮栏
			"onAfterSure":null};
	var param = $.extend(defaultParam, formParam);
	var fp = JSON.stringify(param.filterParam);
	var formKey = (param.formKey)?param.formKey.toLowerCase():"";
	var url = __ctx+"/gs/forms/"+formKey+".action?queryParams="+(encodeURIComponent(fp)||"").replace(/\'/,"%27");
	if(param.url) {
		url = __ctx+param.url;
	}
	if(param.id) {
		url += "&id=" + param.id;
	}
	if(param.addParam) {
		url += "&addParams=" + encodeURIComponent(JSON.stringify(param.addParam));
	}
	if(param.urlParam) {
		url += param.urlParam;
	}
	var win = "<iframe id='_form_win_ifr'  src='"+ url +"' frameborder='0' style='width:100%;height:100%' scrolling='auto'></iframe>";
	var winTitle = param.title;
	var winWid = param.width;
	var winHei = param.height;
	var winMax = param.isMaxWin;
	var isModal = param.isModal;
	var callFunc = param.onAfterSure;
	var p = $("<div style='overflow: hidden;'/>").appendTo("body");
	var opt = {
			id:'_form_win_',
			title:winTitle,
			content: win,
			width:winWid,
			height:winHei,
			closable:true,
			collapsible:true,
			maximizable:true,
			minimizable:false,
			maximized:winMax,
			resizable:true,
			modal: isModal,
			shadow: false,
			cache:false,
			closed:true,
			onClose:function() {
				$("#_form_win_").dialog("destroy");
			}
	};
	if(param.showTools) {
		opt["buttons"] = [{
			text:'确定',
			iconCls:'icon-ok',
			handler:function(){
				if(callFunc) {
					var wins = $('#_form_win_').find("iframe");
					var win = wins[0].contentWindow.window;
					var result = callFunc(win);
					if(result) $("#_form_win_").dialog("close");
				}
			}
		},{
			text:'取消',
			iconCls:'icon-cancel',
			handler:function(){
				$("#_form_win_").dialog("close");
			}
		}];
	}
	if($('#_form_win_').length) {
		alert("操作对话框已经打开，请先关闭打开的对话框再操作！");
		return;
	}
	p.dialog(opt);
	$("#_form_win_").dialog("open");
}

/**
 * 打开自定义表单窗口 layui方式打开弹窗
 * @param fromParam
 * 包含以下参数
 * formName
 * formKey
 * onAfterSure
 * width
 * height
 * isMaxWin
 * filterParam
 * id
 */
function openFormWinV2(formParam){
	var defaultParam = {
			"winId":"",//窗口ID
			"title":"操作",
			"width":800,
			"height":400,
			"url":null,
			"size":"large",//尺寸大小
			"message":"",//窗口内容
			"isIframe":true,
			"isExtUrl":false,//外部链接
			"isTop":false,
			"filterParam":{},
			"addParam":{},//新增参数
			"urlParam":null,//附加参数
			"isMaxWin":false,
			"zIndex":1024,//层叠顺序
			"isModal":true,//模式窗口
			//"isUseLayui":false,//是否启用layui--layer
			"showTools":true,//显示按钮栏
			"onAfterSure":null//确认按钮
			,"onAfterCancel":null
			,"okTitle":"确认"
			,"cancelTitle":"取消"
			,"onInit":null//初始化事件
			,"onHide":null//窗口关闭后事件
	};
	var param = $.extend(defaultParam, formParam);
	var fp = JSON.stringify(param.filterParam);
	var formKey = (param.formKey)?param.formKey.toLowerCase():"";
	var winId = "_form_win_ifr_" + (param.winId || "");
	var url = __ctx+"/gs/forms/"+formKey+".action?queryParams="+(encodeURIComponent(fp)||"").replace(/\'/g,"%27");
	if(param.url) {
		url = (param.isExtUrl ? "": __ctx)+param.url;
	}
	if(url.indexOf("?") < 0) {
		url += "?1=1";
	}
	if(param.id) {
		url += "&id=" + param.id;
	}
	if(param.addParam) {
		url += "&addParams=" + encodeURIComponent(JSON.stringify(param.addParam));
	}
	if(param.urlParam) {
		url += param.urlParam;
	}
	var winHei = param.height || "100%";
	if($.isNumeric(winHei)) {
		winHei = winHei + "px";
	}
	var winWid = param.width || "100%";
	if($.isNumeric(winWid)) {
		winWid = winWid + "px";
	}
	var win = "";
	var isIframe = param.isIframe;
	if(isIframe) {
	   win = "<iframe id='" + winId + "'  src='"+ url +"' frameborder='0' style='width:100%;height:"+winHei+"'></iframe>";
	} else {
	   win = (typeof param.message=='string')?"<div id='"+winId+"' style='width:100%;height:" + winHei + "'>"+param.message+"</div>":param.message;
	}
	var winTitle = param.title;
	var winMax = param.isMaxWin;
	var isModal = param.isModal;
	var callFunc = param.onAfterSure;
	var cancelFunc = param.onAfterCancel;
	var thisWin = top;
	if (!param.isTop) {
		thisWin = self;
	}
		var opt = {
			type: isIframe?2:1,
			title: winTitle,
			shadeClose:!isModal,
			content: isIframe?url:win,
			area:[winWid, winHei],
			maxmin:true,
			zIndex:param.zIndex || 1024,
			success: function(layero, index){
				if(!isIframe && !param.message) {
					$(layero).find("#"+winId).load(url);
				}
				if(param.onInit) {
					param.onInit.call(layero, index);
				}
			},
			end: function() {
				if(param.onHide) {
					param.onHide.call(this);
				}
			}
		};
		if(param.showTools) {
			var btOpt = {
				btn: [param.okTitle, param.cancelTitle]
				,yes: function(index, layero){
					var result = null;
					if (callFunc) {
						var win = thisWin;
						if (isIframe) {
							var wins = $(layero).find("iframe");
							win = wins[0].contentWindow.window;
						}
						result = callFunc(win);
					}
					result !== false && layer.close(index);
				}
				,btn2: function(index, layero){
					var result = null;
					if (cancelFunc) {
						var win = thisWin;
						if (isIframe) {
							var wins = $(layero).find("iframe");
							win = wins[0].contentWindow.window;
						}
						result = cancelFunc(win);
					}
					result !== false && layer.close(index);
				}
			};
			$.extend(true, opt, btOpt);
		}
		layer.open(opt);
}

$(document).keydown(function(e){ 
    var keyEvent; 
    if(e.keyCode==8){ 
        var d=e.srcElement||e.target; 
        if(d.tagName.toUpperCase()=='INPUT'||d.tagName.toUpperCase()=='TEXTAREA'){ 
            keyEvent=d.readOnly||d.disabled; 
        }else{ 
            keyEvent=true; 
        } 
    }else{ 
        keyEvent=false; 
    } 
    if(keyEvent){ 
        e.preventDefault(); 
    } 
}); 


var $MsgUtil = {
		alert:function(opt, method) {
			var op = method || "warning";
			top.toastr.options = {
				"closeButton":true,
				"positionClass": "toast-top-center"
			}
			top.toastr[op](opt);
		},
		info:function(opt) {
			$MsgUtil.alert(opt, "info");
		}
};

window.alert = function(msg) {
	$MsgUtil.alert(msg);
};

var pageStorage = {
		name: 'pageStorage',
		storage: {},
		read: function(key) { 
			return this.storage[key];
		},
		write: function(key, value) {
			this.storage[key] = value;
		},
		each: function(callback) {
			var memoryStorage = this.storage;
			for (var key in memoryStorage) {
				if (memoryStorage.hasOwnProperty(key)) {
					callback(memoryStorage[key], key);
				}
			}
		},
		remove: function(key) {
			delete this.storage[key];
		},
		clearAll: function() {
			this.storage = {};
		}
};
	
window.PageStorage = pageStorage;
/*
    http://www.JSON.org/json2.js
    2011-02-23

    Public Domain.

    NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.

    See http://www.JSON.org/js.html


    This code should be minified before deployment.
    See http://javascript.crockford.com/jsmin.html

    USE YOUR OWN COPY. IT IS EXTREMELY UNWISE TO LOAD CODE FROM SERVERS YOU DO
    NOT CONTROL.


    This file creates a global JSON object containing two methods: stringify
    and parse.

        JSON.stringify(value, replacer, space)
            value       any JavaScript value, usually an object or array.

            replacer    an optional parameter that determines how object
                        values are stringified for objects. It can be a
                        function or an array of strings.

            space       an optional parameter that specifies the indentation
                        of nested structures. If it is omitted, the text will
                        be packed without extra whitespace. If it is a number,
                        it will specify the number of spaces to indent at each
                        level. If it is a string (such as '\t' or '&nbsp;'),
                        it contains the characters used to indent at each level.

            This method produces a JSON text from a JavaScript value.

            When an object value is found, if the object contains a toJSON
            method, its toJSON method will be called and the result will be
            stringified. A toJSON method does not serialize: it returns the
            value represented by the name/value pair that should be serialized,
            or undefined if nothing should be serialized. The toJSON method
            will be passed the key associated with the value, and this will be
            bound to the value

            For example, this would serialize Dates as ISO strings.

                Date.prototype.toJSON = function (key) {
                    function f(n) {
                        // Format integers to have at least two digits.
                        return n < 10 ? '0' + n : n;
                    }

                    return this.getUTCFullYear()   + '-' +
                         f(this.getUTCMonth() + 1) + '-' +
                         f(this.getUTCDate())      + 'T' +
                         f(this.getUTCHours())     + ':' +
                         f(this.getUTCMinutes())   + ':' +
                         f(this.getUTCSeconds())   + 'Z';
                };

            You can provide an optional replacer method. It will be passed the
            key and value of each member, with this bound to the containing
            object. The value that is returned from your method will be
            serialized. If your method returns undefined, then the member will
            be excluded from the serialization.

            If the replacer parameter is an array of strings, then it will be
            used to select the members to be serialized. It filters the results
            such that only members with keys listed in the replacer array are
            stringified.

            Values that do not have JSON representations, such as undefined or
            functions, will not be serialized. Such values in objects will be
            dropped; in arrays they will be replaced with null. You can use
            a replacer function to replace those with JSON values.
            JSON.stringify(undefined) returns undefined.

            The optional space parameter produces a stringification of the
            value that is filled with line breaks and indentation to make it
            easier to read.

            If the space parameter is a non-empty string, then that string will
            be used for indentation. If the space parameter is a number, then
            the indentation will be that many spaces.

            Example:

            text = JSON.stringify(['e', {pluribus: 'unum'}]);
            // text is '["e",{"pluribus":"unum"}]'


            text = JSON.stringify(['e', {pluribus: 'unum'}], null, '\t');
            // text is '[\n\t"e",\n\t{\n\t\t"pluribus": "unum"\n\t}\n]'

            text = JSON.stringify([new Date()], function (key, value) {
                return this[key] instanceof Date ?
                    'Date(' + this[key] + ')' : value;
            });
            // text is '["Date(---current time---)"]'


        JSON.parse(text, reviver)
            This method parses a JSON text to produce an object or array.
            It can throw a SyntaxError exception.

            The optional reviver parameter is a function that can filter and
            transform the results. It receives each of the keys and values,
            and its return value is used instead of the original value.
            If it returns what it received, then the structure is not modified.
            If it returns undefined then the member is deleted.

            Example:

            // Parse the text. Values that look like ISO date strings will
            // be converted to Date objects.

            myData = JSON.parse(text, function (key, value) {
                var a;
                if (typeof value === 'string') {
                    a =
/^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/.exec(value);
                    if (a) {
                        return new Date(Date.UTC(+a[1], +a[2] - 1, +a[3], +a[4],
                            +a[5], +a[6]));
                    }
                }
                return value;
            });

            myData = JSON.parse('["Date(09/09/2001)"]', function (key, value) {
                var d;
                if (typeof value === 'string' &&
                        value.slice(0, 5) === 'Date(' &&
                        value.slice(-1) === ')') {
                    d = new Date(value.slice(5, -1));
                    if (d) {
                        return d;
                    }
                }
                return value;
            });


    This is a reference implementation. You are free to copy, modify, or
    redistribute.
*/

/*jslint evil: true, strict: false, regexp: false */

/*members "", "\b", "\t", "\n", "\f", "\r", "\"", JSON, "\\", apply,
    call, charCodeAt, getUTCDate, getUTCFullYear, getUTCHours,
    getUTCMinutes, getUTCMonth, getUTCSeconds, hasOwnProperty, join,
    lastIndex, length, parse, prototype, push, replace, slice, stringify,
    test, toJSON, toString, valueOf
*/


// Create a JSON object only if one does not already exist. We create the
// methods in a closure to avoid creating global variables.

var JSON;
if (!JSON) {
    JSON = {};
}

(function () {
    "use strict";

    function f(n) {
        // Format integers to have at least two digits.
        return n < 10 ? '0' + n : n;
    }

    if (typeof Date.prototype.toJSON !== 'function') {

        Date.prototype.toJSON = function (key) {

            return isFinite(this.valueOf()) ?
                this.getUTCFullYear()     + '-' +
                f(this.getUTCMonth() + 1) + '-' +
                f(this.getUTCDate())      + 'T' +
                f(this.getUTCHours())     + ':' +
                f(this.getUTCMinutes())   + ':' +
                f(this.getUTCSeconds())   + 'Z' : null;
        };

        String.prototype.toJSON      =
            Number.prototype.toJSON  =
            Boolean.prototype.toJSON = function (key) {
                return this.valueOf();
            };
    }

    var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        gap,
        indent,
        meta = {    // table of character substitutions
            '\b': '\\b',
            '\t': '\\t',
            '\n': '\\n',
            '\f': '\\f',
            '\r': '\\r',
            '"' : '\\"',
            '\\': '\\\\'
        },
        rep;


    function quote(string) {

// If the string contains no control characters, no quote characters, and no
// backslash characters, then we can safely slap some quotes around it.
// Otherwise we must also replace the offending characters with safe escape
// sequences.

        escapable.lastIndex = 0;
        return escapable.test(string) ? '"' + string.replace(escapable, function (a) {
            var c = meta[a];
            return typeof c === 'string' ? c :
                '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
        }) + '"' : '"' + string + '"';
    }


    function str(key, holder) {

// Produce a string from holder[key].

        var i,          // The loop counter.
            k,          // The member key.
            v,          // The member value.
            length,
            mind = gap,
            partial,
            value = holder[key];

// If the value has a toJSON method, call it to obtain a replacement value.

        if (value && typeof value === 'object' &&
                typeof value.toJSON === 'function') {
            value = value.toJSON(key);
        }

// If we were called with a replacer function, then call the replacer to
// obtain a replacement value.

        if (typeof rep === 'function') {
            value = rep.call(holder, key, value);
        }

// What happens next depends on the value's type.

        switch (typeof value) {
        case 'string':
            return quote(value);

        case 'number':

// JSON numbers must be finite. Encode non-finite numbers as null.

            return isFinite(value) ? String(value) : 'null';

        case 'boolean':
        case 'null':

// If the value is a boolean or null, convert it to a string. Note:
// typeof null does not produce 'null'. The case is included here in
// the remote chance that this gets fixed someday.

            return String(value);

// If the type is 'object', we might be dealing with an object or an array or
// null.

        case 'object':

// Due to a specification blunder in ECMAScript, typeof null is 'object',
// so watch out for that case.

            if (!value) {
                return 'null';
            }

// Make an array to hold the partial results of stringifying this object value.

            gap += indent;
            partial = [];

// Is the value an array?

            if (Object.prototype.toString.apply(value) === '[object Array]') {

// The value is an array. Stringify every element. Use null as a placeholder
// for non-JSON values.

                length = value.length;
                for (i = 0; i < length; i += 1) {
                    partial[i] = str(i, value) || 'null';
                }

// Join all of the elements together, separated with commas, and wrap them in
// brackets.

                v = partial.length === 0 ? '[]' : gap ?
                    '[\n' + gap + partial.join(',\n' + gap) + '\n' + mind + ']' :
                    '[' + partial.join(',') + ']';
                gap = mind;
                return v;
            }

// If the replacer is an array, use it to select the members to be stringified.

            if (rep && typeof rep === 'object') {
                length = rep.length;
                for (i = 0; i < length; i += 1) {
                    if (typeof rep[i] === 'string') {
                        k = rep[i];
                        v = str(k, value);
                        if (v) {
                            partial.push(quote(k) + (gap ? ': ' : ':') + v);
                        }
                    }
                }
            } else {

// Otherwise, iterate through all of the keys in the object.

                for (k in value) {
                    if (Object.prototype.hasOwnProperty.call(value, k)) {
                        v = str(k, value);
                        if (v) {
                            partial.push(quote(k) + (gap ? ': ' : ':') + v);
                        }
                    }
                }
            }

// Join all of the member texts together, separated with commas,
// and wrap them in braces.

            v = partial.length === 0 ? '{}' : gap ?
                '{\n' + gap + partial.join(',\n' + gap) + '\n' + mind + '}' :
                '{' + partial.join(',') + '}';
            gap = mind;
            return v;
        }
    }

// If the JSON object does not yet have a stringify method, give it one.

    if (typeof JSON.stringify !== 'function') {
        JSON.stringify = function (value, replacer, space) {

// The stringify method takes a value and an optional replacer, and an optional
// space parameter, and returns a JSON text. The replacer can be a function
// that can replace values, or an array of strings that will select the keys.
// A default replacer method can be provided. Use of the space parameter can
// produce text that is more easily readable.

            var i;
            gap = '';
            indent = '';

// If the space parameter is a number, make an indent string containing that
// many spaces.

            if (typeof space === 'number') {
                for (i = 0; i < space; i += 1) {
                    indent += ' ';
                }

// If the space parameter is a string, it will be used as the indent string.

            } else if (typeof space === 'string') {
                indent = space;
            }

// If there is a replacer, it must be a function or an array.
// Otherwise, throw an error.

            rep = replacer;
            if (replacer && typeof replacer !== 'function' &&
                    (typeof replacer !== 'object' ||
                    typeof replacer.length !== 'number')) {
                throw new Error('JSON.stringify');
            }

// Make a fake root object containing our value under the key of ''.
// Return the result of stringifying the value.

            return str('', {'': value});
        };
    }


// If the JSON object does not yet have a parse method, give it one.

    if (typeof JSON.parse !== 'function') {
        JSON.parse = function (text, reviver) {

// The parse method takes a text and an optional reviver function, and returns
// a JavaScript value if the text is a valid JSON text.

            var j;

            function walk(holder, key) {

// The walk method is used to recursively walk the resulting structure so
// that modifications can be made.

                var k, v, value = holder[key];
                if (value && typeof value === 'object') {
                    for (k in value) {
                        if (Object.prototype.hasOwnProperty.call(value, k)) {
                            v = walk(value, k);
                            if (v !== undefined) {
                                value[k] = v;
                            } else {
                                delete value[k];
                            }
                        }
                    }
                }
                return reviver.call(holder, key, value);
            }


// Parsing happens in four stages. In the first stage, we replace certain
// Unicode characters with escape sequences. JavaScript handles many characters
// incorrectly, either silently deleting them, or treating them as line endings.

            text = String(text);
            cx.lastIndex = 0;
            if (cx.test(text)) {
                text = text.replace(cx, function (a) {
                    return '\\u' +
                        ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
                });
            }

// In the second stage, we run the text against regular expressions that look
// for non-JSON patterns. We are especially concerned with '()' and 'new'
// because they can cause invocation, and '=' because it can cause mutation.
// But just to be safe, we want to reject all unexpected forms.

// We split the second stage into 4 regexp operations in order to work around
// crippling inefficiencies in IE's and Safari's regexp engines. First we
// replace the JSON backslash pairs with '@' (a non-JSON character). Second, we
// replace all simple value tokens with ']' characters. Third, we delete all
// open brackets that follow a colon or comma or that begin the text. Finally,
// we look to see that the remaining characters are only whitespace or ']' or
// ',' or ':' or '{' or '}'. If that is so, then the text is safe for eval.

            if (/^[\],:{}\s]*$/
                    .test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@')
                        .replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']')
                        .replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

// In the third stage we use the eval function to compile the text into a
// JavaScript structure. The '{' operator is subject to a syntactic ambiguity
// in JavaScript: it can begin a block or an object literal. We wrap the text
// in parens to eliminate the ambiguity.

                j = eval('(' + text + ')');

// In the optional fourth stage, we recursively walk the new structure, passing
// each name/value pair to a reviver function for possible transformation.

                return typeof reviver === 'function' ?
                    walk({'': j}, '') : j;
            }

// If the text is not JSON parseable, then a SyntaxError is thrown.

            throw new SyntaxError('JSON.parse');
        };
    }
}());

//比较数组是否相同
JSON.compArray = function(array1, array2) {
	if ((array1 && typeof array1 === "object" && array1.constructor === Array)
			&& (array2 && typeof array2 === "object" && array2.constructor === Array)) {
		if (array1.length == array2.length) {
			for ( var i = 0; i < array1.length; i++) {
				var ggg = JSON.compObj(array1[i], array2[i]);
				if (!ggg) {
					return false;
				}
			}
		} else {
			return false;
		}
	} else {
		throw new Error("argument is  error ;");
	}
	return true;
};
JSON.compObj = function(obj1, obj2)//比较两个对象是否相等，不包含原形上的属性计较
{
	if ((obj1 && typeof obj1 === "object")
			&& ((obj2 && typeof obj2 === "object"))) {
		var count1 = JSON.propertyLength(obj1);
		var count2 = JSON.propertyLength(obj2);
		if (count1 == count2) {
			for ( var ob in obj1) {
				if (obj1.hasOwnProperty(ob) && obj2.hasOwnProperty(ob)) {
					if (obj1[ob] && obj1[ob].constructor == Array
							&& obj2[ob] && obj2[ob].constructor == Array)//如果属性是数组
					{
						if (!JSON.compArray(obj1[ob], obj2[ob])) {
							return false;
						}
						;
					} else if (typeof obj1[ob] === "object"
							&& typeof obj2[ob] === "object")//属性是对象
					{
						if (!JSON.compObj(obj1[ob], obj2[ob])) {
							return false;
						}
						;
					} else {
						if (obj1[ob] !== obj2[ob]) {
							return false;
						}
					}
				} else {
					return false;
				}
			}
		} else {
			return false;
		}
	}
	return true;
};
JSON.propertyLength = function(obj)//获得对象上的属性个数，不包含对象原形上的属性
{
	var count = 0;
	if (obj && typeof obj === "object") {
		for ( var ooo in obj) {
			if (obj.hasOwnProperty(ooo)) {
				count++;
			}
		}
		return count;
	} else {
		throw new Error("argument can not be null;");
	}

};
JSON.toUrlParam = function(jsonObj) {
	var params = [];
	for(var key in jsonObj) {
		var o = jsonObj[key];
		if(o!==undefined && o!==null) {
			if(o instanceof Array) {
				for(var k in o) {
					var so = o[k];
					if(so!==undefined && so!==null) {
						params.push(key+"="+encodeURIComponent(so));
					}
				}
			} else {
				params.push(key+"="+encodeURIComponent(o));
			}
		}
	}
	return params.join("&");
};

jwpf={
		messageStyle :{
			"COMMON" : "01",//普通通知
			"WARNING" : "02",//预警通知
			"TODO" : "03",//待办事宜通知
			"BUSINESS" : "04",//业务确认通知
			"REMIND" : "05",//提醒通知
			"BUSINESS_DEAL" : "06"//业务办理通知
		},
		getFormVal:function(fieldKey){
			var values;
			var object = $("#" + fieldKey);
			var className = object.attr("class");
			if(className && className.indexOf("easyui") >=0){// 判断字符中是否包含"easyui"
				if(className.indexOf("combo") >=0){
					if(object.combo("options").multiple){// 判断是否为多选
						values = object.combo("getValues");
					} else {
						values = object.combo("getValue");
					}
				} else if(className.indexOf("numberbox") >=0) {
					values = object.numberbox("getValue");
				} else if(className.indexOf("processbarSpinner") >= 0) {
					values = object.processbarSpinner("getValue");
			    } else if(className.indexOf("dialoguewindow") >=0) {
			    	values = object.dialoguewindow("getValue");
			    } else if(className.indexOf("textbox") >=0) { 
			    	values = object.textbox("getValue");
			    } else {
					values = object.val();
				}
			} else if(className && className.indexOf("combobox") >=0) {
				values = $ES.getComboBoxVal(object);
			} else if(className && className.indexOf("checkbox-radio") >=0) {
				values = $ES.getCheckBoxVal(object);
			} else if(className && className.indexOf("combotree") >=0) {
				values = $ES.getSelectTreeVal(object);
			} else {// 非easyui选框取值
				values = object.val();
			}
			return values;
		},//isTriggerSelectEvent--是否触发选中事件
		setFormVal:function(fieldKey,value,isTriggerSelectEvent){
			var object = $("#" + fieldKey);
			var className = object.attr("class");
			if(className && className.indexOf("easyui") >=0){// 判断字符中是否包含"easyui"
				if(className.indexOf("combo") >=0){
					if(className.indexOf("combobox") >=0){
						var opt = object.combobox("options");
						if(opt.multiple){// 判断是否为多选
							object.combobox("setValues",value);					
						} else {
							object.combobox("setValue",value);
						}
						var selectFlag = true;
						if(opt.url) {
							var val = (opt.multiple)?object.combobox("getValues"):[object.combobox("getValue")];
							var txt = object.combobox("getText");
							if(val.join(",") == txt) {
								if(isTriggerSelectEvent) {
									opt.onLoadSuccess = function() {
										if(opt.onSelect) {
											var target = object[0];
											opt.onSelect.call(target, opt.finder.getRow(target, value));
										}
										opt.onLoadSuccess = null;
									};
								}
								selectFlag = false;//不需要重新触发onselect事件
								object.combobox("reload");
							}
						}
						if(selectFlag && isTriggerSelectEvent && opt.onSelect) {
							var target = object[0];
							opt.onSelect.call(target, opt.finder.getRow(target, value));
						}
					} else if(className.indexOf("combotree") >=0){
						if(object.combotree("options").multiple){// 判断是否为多选
							object.combotree("setValues",value);					
						} else {
							object.combotree("setValue",value);
						}
					} else if(className.indexOf("combogrid") >=0){
						if(object.combogrid("options").multiple){// 判断是否为多选
							object.combogrid("setValues",value);					
						} else {
							object.combogrid("setValue",value);
						}
					} else if(className.indexOf("comboradio") >=0) {
						object.comboradio("setValue",value);
					} else if(className.indexOf("combocheck") >=0) {
						object.combocheck("setValue",value);
					} else if(className.indexOf("newradiobox") >=0) {
				    	object.newradiobox("setValue", value);
					} else if(className.indexOf("newcheckbox") >=0) {
				    	if(value!=undefined && value!=null) {
				    		object.newcheckbox("setValues", value.split(","));
				    	}
					} else{
						if(object.combo("options").multiple){// 判断是否为多选
							object.combo("setValues",value);					
						} else {
							object.combo("setValue",value);
						}
					}				
				} else if(className.indexOf("numberbox") >=0) {
					object.numberbox("setValue",value);
				} else if(className.indexOf("processbarSpinner") >= 0) {
					object.processbarSpinner("setValue", value);
			    } else if(className.indexOf("dialoguewindow") >=0) {
			    	object.dialoguewindow("setValue", value);
			    } else if(className.indexOf("textbox") >=0) { 
			    	object.textbox("setValue", value);
			    } else {
					object.val(value);
				}
			} else if(className && className.indexOf("hyperlink") >=0) {//超链接
				object.val(value);
				if(value) $("#hl_"+fieldKey).text(value);
		    } else if(className && className.indexOf("combobox") >=0) {
		    	$ES.setComboBoxVal(object,value);
			} else if(className && className.indexOf("checkbox-radio") >=0) {
				$ES.setCheckBoxVal(object, value);
			} else if(className && className.indexOf("combotree") >=0) {
				$ES.setSelectTreeVal(object, value);
			}  else if(className && className.indexOf("imagebox") >=0) {
				object.imagebox("setValue", value);
			} else {// 非easyui选框
				object.val(value);
			}
		},//获取表单combo类字段显示名称
		getFormComboFieldCaption:function(fieldKey){
			var values = null;
			var object = $("#" + fieldKey);
			var className = object.attr("class");
			if(className && className.indexOf("easyui") >=0){// 判断字符中是否包含"easyui"
				if(className.indexOf("combo") >=0){
					if(object.combo("options").multiple){// 判断是否为多选
						values = object.combo("getText");
					} else {
						values = object.combo("getText");
					}
				}
			} else if(className && className.indexOf("combobox") >=0) {
				values = $ES.getComboBoxVal(object,"text");
			} else if(className && className.indexOf("checkbox-radio") >=0) {
				values = $ES.getCheckBoxVal(object,"text");
			} else if(className && className.indexOf("combotree") >=0) {
				values = $ES.getSelectTreeVal(object,"text");
			}
			return values;
		},
		//设置form字段编辑与否
		setFormFieldReadonly:function(fieldKey, isReadonly){
			var object = $("#" + fieldKey);
			var className = object.attr("class");
			if(className) {
				if(className.indexOf("easyui") >=0){// 判断字符中是否包含"easyui"
					if(className.indexOf("newradiobox") >=0) {
				    	if(isReadonly) {
							object.newradiobox("disable");
						} else {
							object.newradiobox('enable');
						}
					} else if(className.indexOf("newcheckbox") >=0) {
				    	if(isReadonly) {
							object.newcheckbox("disable");
						} else {
							object.newcheckbox('enable');
						}
					} else if(className.indexOf("combo") >=0){
						if(isReadonly) {
							object.combo('readonly', true);
						} else {
							object.combo('readonly', false);
						}
					} else if(className.indexOf("numberbox") >=0) {
						if(isReadonly) {
							object.numberbox('readonly', true);
						} else {
							object.numberbox('readonly', false);
						}
					} else if(className.indexOf("processbarSpinner") >= 0) {
						if(isReadonly) {
							object.processbarSpinner("disable");
						} else {
							object.processbarSpinner("enable");
						}
				    } else if(className.indexOf("textbox") >=0) { 
				    	if(isReadonly) {
							object.textbox('readonly', true);
						} else {
							object.textbox('readonly', false);
						}
				    } else {
				    	if(isReadonly) {
							object.attr("readonly", "readonly");
						} else {
							object.removeAttr("readonly");
						}
					}
				} else if(className.indexOf("Idate") >=0) {//日期控件
					if(isReadonly) {
						object.attr("readonly", "readonly");
						var onfocusStr = object.attr("onfocus");
						if(onfocusStr) {
							object.data("focusEvent",onfocusStr);
							object.removeAttr("onfocus");
						}
					} else {
						object.removeAttr("readonly");
						object.removeAttr("disabled");
						var onfocusStr = object.data("focusEvent");
						if(onfocusStr) {
							object.attr("onfocus", onfocusStr);
							object.removeData("focusEvent");
						}
					}
				} else if(className.indexOf("jquery_ckeditor") >=0) {//富文本框控件
					var ro = isReadonly || false;
					try {
						object.ckeditorGet().setReadOnly(ro);
					} catch(e) {
						object.ckeditorGet().setReadOnly(ro);
					}
				} else if(className.indexOf("dialoguewindow") >=0) {//对话框控件
					if(isReadonly) {
						object.dialoguewindow("disable");
					} else {
						object.dialoguewindow("enable");
					}
				} else if(className.indexOf("combobox") >=0) {
					if(isReadonly) {
						object.prop("disabled", true);
					} else {
						object.removeProp("disabled");
					}
				} else if(className.indexOf("checkbox-radio") >=0) {
					if(isReadonly) {
						$ES.disableCheckBox(object);
					} else {
						$ES.enableCheckBox(object);
					}
				} else if(className.indexOf("combotree") >=0) {
					if(isReadonly) {
						object.selectTree("disable");
					} else {
						object.selectTree("enable");
					}
				} else {
					if(isReadonly) {
						object.attr("readonly", "readonly");
					} else {
						object.removeAttr("readonly");
					}
				}
			} else {
				if(isReadonly) {
					object.attr("readonly", "readonly");
				} else {
					object.removeAttr("readonly");
				}
			}
		},
		isEmptyRow:function(row) {
			   var flag=true;
			   $.each(row,function(k,v){
				   if(k!="isNewRecord" && k!="autoParentCode" && k!="autoCode" &&k!="_parentId"&& k!="state"){
					   //为空v=false
					   //不为空v=true
				      flag=flag&&!v;
				   }
			   });
			   return flag;
		},
		getTableVal:function(tableKey,rowIndex,fieldKey){
			var child = $("#"+tableKey);
			if(!child.length || !child.data("datagrid")) return null;
			if($.isNumeric(rowIndex)){// rowIndex为数字，返回未选的记录
				var allrows = child.datagrid("getRows");
				if(fieldKey){// 返回未选的记录对象指定字段的数值
				  if(allrows[rowIndex])
					return allrows[rowIndex][fieldKey];
				}else{// 返回未选的记录对象
					return allrows[rowIndex];
				}
			} else if(rowIndex === true){// 非数字,返回已选的记录
				if(fieldKey){// 已选的记录，指定字段，返回字段数值
					var row = child.datagrid('getChecked');
					var rows = [];
					$.each(row, function(k,v) {
						if(!jwpf.isEmptyRow(v)) {
							rows.push(v[fieldKey]);
						}
					});			
					return rows;
				}else{// 已选的记录，未指定字段，返回记录对象
					var row = child.datagrid('getChecked');	
					var rows = [];
					$.each(row, function(k,v) {
						if(!jwpf.isEmptyRow(v)) {
							rows.push(v);
						}
					});
					return rows;
				}
			} else {// 其他情况，为false或者null、undefined值，返回所有的记录
				if(fieldKey){// 返回所有记录对象指定字段的数值
					var row = child.datagrid('getRows');
					var rows = [];
					$.each(row, function(k,v) {
						if(!jwpf.isEmptyRow(v)) {
							rows.push(v[fieldKey]);
						}
					});			
					return rows;
				}else{// 返回所有记录对象
					var row = child.datagrid('getRows');
					var rows = [];
					$.each(row, function(k,v) {
						if(!jwpf.isEmptyRow(v)) {
							rows.push(v);
						}
					});
					return rows;
				}			
			}
		},
		/**
		 * 设置列表值
		 * @param tableKey
		 * @param rowIndex
		 * @param rowData 包含部分列字段及值，如：{"fieldKey":"fieldVal"}
		 * @param isAddRow 是否添加新行
		 * @param isNotAutoExp 不自动计算
		 */
		setTableVal:function(tableKey,rowIndex,rowData,isAddRow,isNotAutoExp){
			rowIndex = parseInt(rowIndex);
			if(rowData) {
				var tb = $("#"+tableKey);
				//先结束可编辑控件
				var opts = tb.edatagrid('options');
				var index = opts.editIndex;
				var field = opts.editCol;
				if((index ||index==0) && field) {
					var editOpt={"index":index,"field":field};
					tb.edatagrid('endCellEdit', editOpt);
				}
				//再更新行数据
				tb.edatagrid("updateRow", {index:rowIndex,row:rowData});
				if(!isNotAutoExp) {
					setTimeout(function() {
						  jwpf.doSubExpression(tableKey,rowIndex);
						  jwpf.doFormExpression(tableKey);
					},0);
				}
				if(isAddRow) {
					tb.edatagrid("addRow");
				}
			}
		},
		/**
		 * 公式设置子表值
		 */
		setTableValExp:function(tableKey,rowIndex,rowData,isAddRow){
			if(rowData) {
				var tb = $("#"+tableKey);
				//updateRow方法会导致tr.html（）被刷新。正在编辑的编辑器会被动销毁。而子表两个字段需要运算，
				//故简单的对datagrid的panel附加了两次相同的keyup执行函数。(此处反复执行相同事件函数，还有优化余地)
				//当第二次事件执行完毕，input其实已受到updateRow的影响销毁了，
				//e.targer之中取不到data，从而产生报错
				//因updateRow在datagrid内部大量调用，不方便修改。
				//tb.edatagrid("updateRow", {index:rowIndex,row:rowData});
				var row = tb.edatagrid("getRows")[rowIndex];
				tb.edatagrid("getPanel").panel("panel").find("tr[datagrid-row-index="+rowIndex+"] td").each(function(){
					var _this = $(this);
					$.each(rowData, function(k, v) {
						if(_this.attr("field")==k){
							if(_this.hasClass("datagrid-cell-editing")) {//编辑条件下
								//_this.find("input.numberbox-f").numberbox("setValue", v);
							} else {
								_this.children("div.datagrid-cell").html(v);//显示下
							}
							row[k] = v;//没有对计算后的值回调edit的验证器进行验证，不过观察datagrid的updatarow也不进行验证的
							//自动计算关联属性
							setTimeout(function() {
								  jwpf.doSubExpression(tableKey,rowIndex, k);
								  jwpf.doFormExpression(tableKey, k);
							},0);
							return false;
						}
					});
				});
				if(isAddRow) {
					tb.edatagrid("addRow");
				}//重新计算footer值
				jwpf.reloadDataGridFooter(tb);
			}
		},	
		getTreeTableVal:function(tableKey,rowCode,fieldKey){
			var child = $("#"+tableKey);
			var allRows=child.edatagrid("getData");
			if($.type(rowCode) === "string"){// rowCode为字符串，返回指定行记录对象的指定字段的数值			
				var row = child.treegrid("find",rowCode);
				if(fieldKey){
					/*var rows = [];
					if(!jwpf.isEmptyRow(row)) {
						rows.push(row[fieldKey]);
					}*/	
					return row[fieldKey];
				}else{// 返回指定行记录
					return row;
				}
			} else if($.type(rowCode) === "boolean" && rowCode === true){// 非数字,返回已选的记录
				if(fieldKey){// 已选的记录，指定字段，返回字段数值
					var row = child.edatagrid('getChecked');
					var rows = [];
					$.each(row, function(k,v) {
						if(!jwpf.isEmptyRow(v)) {
							rows.push(v[fieldKey]);
						}
					});			
					return rows;
				}else{// 已选的记录，未指定字段，返回记录对象
					var row = child.edatagrid('getChecked');	
					var rows = [];
					$.each(row, function(k,v) {
						if(!jwpf.isEmptyRow(v)) {
							rows.push(v);
						}
					});
					return rows;
				}
			} else {// 其他情况，为false或者null、undefined值，返回所有的记录
				if(fieldKey){// 返回所有记录对象指定字段的数值
					var row = allRows;
					var rows = [];
					$.each(row, function(k,v) {
						if(!jwpf.isEmptyRow(v)) {
							rows.push(v[fieldKey]);
						}
					});			
					return rows;
				}else{// 返回所有记录对象
					var row = allRows;
					var rows = [];
					$.each(row, function(k,v) {
						if(!jwpf.isEmptyRow(v)) {
							rows.push(v);
						}
					});
					return rows;
				}			
			}
		},
		setTreeTableVal:function(tableKey,rowCode,rowData,isAddRow){
			if(rowData) {
				var tb = $("#"+tableKey);
				//先结束可编辑控件
				var opts = tb.edatagrid('options');
				var index = opts.editIndex;
				var field = opts.editCol;
				if((index ||index==0) && field) {
					var editOpt={"index":index,"field":field};
					tb.datagrid('endCellEdit', editOpt);
				}
				//再更新行数据
				tb.edatagrid("update",  {
					id: rowCode,
					update:true,
					row : rowData
				});
				if(isAddRow) {
					tb.edatagrid("addRow");
				}
			}
		},	
		/**
		 * key-统计字段，caption-显示名称，displayKey-显示字段，dataType-数据类型
		 * type-统计类别：total、average、other，value-统计规则
		 * @param tableId
		 * @param rules
		 * @param isInit--初始化
		 */
		setTableTotalVal : function(tableId, rules, isInit) {
			 if(rules && rules.length > 0) {
				 var grid = $("#"+tableId);
				 var map = {};
				 $.each(rules, function(k, v) {
					 var name = v.caption + "===" + v.displayKey;
					 if(map[name]) {
						 map[name].push(v);
					 } else {
						 map[name] = [v];
					 }
				 });
				 grid.data("ruleMap", map);//缓存统计规则
				 if(isInit) {
					 jwpf.reloadDataGridFooter(grid);
				 } /*else {
					 grid.datagrid('getPanel').panel('panel').bind('keyup', function (e) {
				        	 try {
				        		if(jwpf.isNumber(e)) {
				        			var input =  $(e.target);
						        	var td = input.closest("td[field]");
						        	var field = td.attr("field");
						        	if(field && map[field]) {
						        		var tr=td.parent();
										var index = tr.attr("datagrid-row-index");
										index = parseInt(index);
						        		var allrows = grid.datagrid("getRows");
						        		allrows[index][field] = input.val();
						        		var fl = map[field];
						        		for(var i=0,len=fl.length;i<len;i++) {
						        			var val = 0;
						        			if(fl[i].type=="total") {
						        				val = jwpf.getTotalVal(allrows, field, fl[i].dataType);
						        			} else if(fl[i].type=="average") {
						        				val = jwpf.getAvgVal(allrows, field, fl[i].dataType);
						        			} else if(fl[i].type=="max") {
												val = jwpf.getMaxVal(allrows, field, fl[i].dataType);
											} else if(fl[i].type=="min") {
												val = jwpf.getMinVal(allrows, field, fl[i].dataType);
											}
						        			var footAllrows =  grid.datagrid("getFooterRows");
											if(footAllrows && footAllrows.length > 0) {
												for(var j=0,jlen=footAllrows.length;j<jlen;j++) {
													if(fl[i].caption == footAllrows[j][fl[i].displayKey]) {
														footAllrows[j][field] = val;
														break;
													}
												}
											}
						        		}
										grid.datagrid('reloadFooter');
						        	}
				        		}	
				         } catch(e1) {alert("执行汇总统计时出现异常：" + e1);}	
					  }); 
				 } */
			 }
		},//重新加载footer，用于自动计算值变化，isCheckedFirst-- 选中优先计算
		reloadDataGridFooter:function(grid, isCheckedFirst) {
			var ruleMap = grid.data("ruleMap");
			if(ruleMap) {
				 var map = ruleMap;
				 var list = [];
				 var allrows = [];
				 if(isCheckedFirst) {
					 allrows = grid.datagrid("getChecked");//选中数据
					 if(allrows && allrows.length == 0) {
						 allrows = grid.datagrid("getRows");
					 }
				 } else {
					 allrows = grid.datagrid("getRows");
				 }
				 $.each(map, function(k,v) {
					 var frObj = {};
					 var names = k.split("===");
					 var caption = names[0];
					 var disKey = names[1];
					 frObj[disKey] = caption;
					 frObj["_isFooter_"] = true;
					 if(v.length > 0) {
						 $.each(v, function(i,o) {
							 var field = o.key;
							 var val = "";
							 if(o.type=="total") {
								 val = jwpf.getTotalVal(allrows, field, o.dataType);
							 } else if(o.type=="average") {
								 val = jwpf.getAvgVal(allrows, field, o.dataType);
							 } else if(o.type=="max") {
								 val = jwpf.getMaxVal(allrows, field, o.dataType);
							 } else if(o.type=="min") {
								 val = jwpf.getMinVal(allrows, field, o.dataType);
							 }
							 frObj[field] = val;
						 });
					 }
					 list.push(frObj);
				 });
				 grid.datagrid('reloadFooter', list);
			}
		},
		getTotalVal:function(allrows, field, dataType) {
			var totalVal = "";
			var num = allrows.length;
			if(num == 0) {
				return totalVal;
			}
			totalVal = 0;
			for(var i =0;i<num;i++){
				var flVal = null;
				if(field) {
					flVal = allrows[i][field];
				} else {
					flVal = allrows[i];
				}
				if(!flVal) {
					flVal = 0;
				}
				if(dataType=="dtLong"){
					var val = parseInt(flVal);					
					//totalVal = val+totalVal ;
					totalVal = Calc.add(val, totalVal);
				}else if(dataType=="dtDouble"){
					var val = parseFloat(flVal);				
					//totalVal = val+totalVal ;
					totalVal = Calc.add(val, totalVal);
				}else {
					var val = parseFloat(flVal);				
					//totalVal = val+totalVal ;
					totalVal = Calc.add(val, totalVal);
				}
			}
			return totalVal;
		},//求和函数，对于对象数组与数字数组求和
		getSumVal:function(data, field, dataType) {
			var totalVal = 0;
			if(data && data.length) {
				var num = data.length;
				for(var i =0;i<num;i++){
					var flVal = null;
					if(field) {
						flVal = data[i][field];
					} else {
						flVal = data[i];
					}
					if(!flVal) {
						flVal = 0;
					}
					if(dataType=="dtLong"){
						var val = parseInt(flVal);					
						//totalVal = val+totalVal ;
						totalVal = Calc.add(val, totalVal);
					} else if(dataType=="dtDouble"){
						var val = parseFloat(flVal);				
						//totalVal = val+totalVal ;
						totalVal = Calc.add(val, totalVal);
					} else {
						var val = parseInt(flVal);					
						//totalVal = val+totalVal;
						totalVal = Calc.add(val, totalVal);
					}
				}
			}
			return totalVal;
		},
		getAvgVal:function(allrows, field, dataType, fixNum) {
			var num = allrows.length;
			if(num == 0) {
				return "";
			}
			var totalVal = jwpf.getTotalVal(allrows, field, dataType);
			//var averageVal = totalVal/num;
			//return averageVal.toFixed(2);
			var ps = ((fixNum === 0)?0:(fixNum || 2));
			return Calc.div(totalVal, num, ps);
		},
		getMaxVal:function(allrows, field, dataType) {
			var num = allrows.length;
			if(num == 0) {
				return "";
			}
			var numArr = [];
			for(var i =0;i<num;i++){
				var flVal = allrows[i][field];
				if(flVal) {
					if(dataType=="dtLong"){
						var val = parseInt(flVal);		
						numArr.push(val);
					}else if(dataType=="dtDouble"){
						var val = parseFloat(flVal);			
						numArr.push(val);
					}
				}
			}
			var maxVal = "";
			if(numArr.length > 0) {
				var maxVal = Math.max.apply(null, numArr);
			}
			return maxVal;
		},
		getMinVal:function(allrows, field, dataType) {
			var num = allrows.length;
			if(num == 0) {
				return "";
			}
			var numArr = [];
			for(var i =0;i<num;i++){
				var flVal = allrows[i][field];
				if(flVal) {
					if(dataType=="dtLong"){
						var val = parseInt(flVal);					
						numArr.push(val);
					}else if(dataType=="dtDouble"){
						var val = parseFloat(flVal);				
						numArr.push(val);
					}
				}
			}
			var minVal = "";
			if(numArr.length > 0) {
				minVal = Math.min.apply(null, numArr);
			}
			return minVal;
		},
		setFormTotalVal : function(tableId,formKey,type) {
			 var grid = $("#"+tableId);
			    grid.datagrid('getPanel').panel('panel').bind('keyup', function (e) {
		        	 try {
		        		if(jwpf.isNumber(e)){
		        			var input =  $(e.target);
				        	var td = input.closest("td[field]");
				        	var field = td.attr("field");
							var allrows = grid.datagrid("getRows");
							var totalVal = 0;
							var num =allrows.length;
							for(var i =0;i<num;i++){
								if(type=="dtLong"){
									var val = parseInt(allrows[i][field]);							
									//totalVal = val+totalVal ;
									totalVal = Calc.add(val, totalVal);
								}else if(type=="dtDouble"){
									var val = parseFloat(allrows[i][field]);							
									//totalVal = val+totalVal ;
									totalVal = Calc.add(val, totalVal);
								}
							}
							$("#"+formKey).val(totalVal);	        
		        		}	
		         } catch(e1) {alert("执行setFormTotalVal求和计算时出现异常："+e1);}	
			  });  
		},
		updateField:function(moduleTableKey,fieldKey,id,val, callFunc){
			var ids = [];
			if($.isArray(id)) {
				ids = id;
			} else {
				ids.push(id);
			}
		   	var options = {
		             url : __ctx+'/gs/gs-mng!updateTableField.action',
		             async : false,
                     traditional:true,
		             data : {
		            	 "ids" : ids,
		            	 "entityName":moduleTableKey,
		            	 "field":fieldKey,
		            	 "value":val
		             },
		             success : function(data) {
		            	 if(data.msg) {
		            		 if(callFunc) { 
		            			 callFunc();
		            	     }
						 }
		             }
		        };
		        fnFormAjaxWithJson(options, true);
		   },
		getId:function(){
			var id = $("#id").val();
			return id;
		},

	uniqueArr : function(arr) {//去除数组中的重复数据
			var i = 0;
			while (i < arr.length) {
				var current = arr[i];
				for (k = arr.length; k > i; k--) {
					if (arr[k] === current) {
						arr.splice(k, 1);
					}
				}
				i++;
			}
			 return arr.sort();
		},
	unbindFormKey : function(expression) {//去除公式中字段的监听(主要用于主表字段)
		var regex = /([^\[]+?)(?=\])/gi;
		var fieldKeys = expression.match(regex);
		fieldKeys = fieldKeys?fieldKeys:[];
		fieldKeys = jwpf.uniqueArr(fieldKeys);
		for(var i =0;i<fieldKeys.length;i++){
			$("#"+fieldKeys[i]).unbind('keyup');
		}
	},
	
 	//页面加载时添加公式监听方法
	  changeTableFormValue:function(key,expression){
		 try {
			 var exp =  expression;
			var regex = /([^\[]+?)(?=\])/gi;
			var fieldKeys = expression.match(regex);
			fieldKeys = fieldKeys?fieldKeys:[];
			fieldKeys = jwpf.uniqueArr(fieldKeys);
			var keys = key.split(".");
			if(keys.length > 1) { // 目标字段为---子表
				
				for(var i =0;i<fieldKeys.length;i++){
					if(fieldKeys[i] && fieldKeys[i]!=""){
					   var fks = fieldKeys[i].split(".");
					   if(fks.length > 1) { //公式可能 ：子+子...=子  或 子+主...=子
						   //1.子表的字段添加监听 //2.计算剩余对象的值
						   
						   //保存子表公式到内存数据中
						  	var expAry = $("#"+fks[0]).data("subExpArray");						  	
						  	var expAll = key + "=" +exp;
						   	if(expAry && expAry.length){						   		
						   		if($.inArray(expAll, expAry)<0){
						   			expAry.push(expAll);
						   			$("#"+fks[0]).data("subExpArray",expAry);
						   		} 
						   	}else{
						   		var ary = [];
						   		ary.push(expAll);
						   		$("#"+fks[0]).data("subExpArray",ary);
						   	}
						   
						   	var newArray = [];
						   	for(var j =0;j<fieldKeys.length;j++){
							   	if(fieldKeys[j]!=fieldKeys[i]){
								   	newArray.push(fieldKeys[j]);
							   		}
						   		}	  
						   	jwpf.changeTableValueOther(fks[0],fks[1],newArray,exp,keys);
						
					   } else { //公式 ：主+...=子 
						 //1.公式中主表的字段添加监听 //2.计算剩余对象的值
						   if(fieldKeys[i].split("_").length>1){//不對其監聽
								 
						   }else{
							   jwpf.changeTableValue(fieldKeys[i],fieldKeys,exp,keys);
						   }
					   }
					}
				}
			} else { // 目标字段为---主表
				for(var i =0;i<fieldKeys.length;i++){
					if(fieldKeys[i] && fieldKeys[i]!=""){
					   var fks = fieldKeys[i].split(".");
					   if(fks.length > 1) { //公式可能 ：子+主... =主  或 子+子...=主 			 					   												  
						   //1.子表字段添加监听 //2.计算剩余对象的值
						   
						   //保存公式到内存数据中
						  	var expAry = $("#"+fks[0]).data("expArray");						  	
						  	var expAll = key + "=" +expression;
						   	if(expAry && expAry.length){						   		
						   		if($.inArray(expAll, expAry)<0){
						   			expAry.push(expAll);
						   			$("#"+fks[0]).data("expArray",expAry);
						   		} 
						   	}else{
						   		var ary = [];
						   		ary.push(expAll);
						   		$("#"+fks[0]).data("expArray",ary);
						   	}
						   	
						   if(fks.length==2){
							jwpf.changeFormValueOther(fks[0],fks[1],fieldKeys,exp,key);
						   }else if(fks.length==3){//这种公式为了计算子表的合计
							 	var newArray = [];
							   	for(var j =0;j<fieldKeys.length;j++){
								   	if(fieldKeys[j]!=fieldKeys[i]){
									   	newArray.push(fieldKeys[j]);
								   		}
							   		}
							 jwpf.changeFormValueOnce(fks[0],fks[1],fks[2],newArray,exp,key);
						   }						 						 
					   } else { //公式 ：主+主....=主 或主+子....=主 
						 //1.主表字段添加监听 //2.计算剩余对象的值					
						 jwpf.changeFormValue(fieldKeys[i],fieldKeys,exp,key);
					   }
					}
				}
			} 
		}catch(e) {alert("为" + key + "添加自动计算公式时出现异常：" + e);} 
	  },
	//事件中动态添加公式监听
	  newChangeTableFormValue : function(key, expression, rowIndex,flag) {
		var exp = expression;
		var regex = /([^\[]+?)(?=\])/gi;
		var fieldKeys = expression.match(regex);
		fieldKeys = fieldKeys ? fieldKeys : [];
		fieldKeys = jwpf.uniqueArr(fieldKeys);
		var keys = key.split(".");
		if (keys.length > 1) { // 子表字段
			if(flag){
				var grid = $("#" + keys[0]);//去除当前行的监听
				var pl = grid.edatagrid('getPanel').panel('panel');
				var tr = pl.find("div.datagrid-body").find(
					"tr[datagrid-row-index=" + rowIndex + "]");
				tr.unbind('keyup');
				}
			for ( var i = 0; i < fieldKeys.length; i++) {
				if (fieldKeys[i] && fieldKeys[i] != "") {
					var fks = fieldKeys[i].split(".");
					if (fks.length > 1) { // 公式可能 ：子+子...=子 或 子+主...=子
						// 1.子表字段添加监听 //2.计算剩余对象的值
						var newArray = [];
						for ( var j = 0; j < fieldKeys.length; j++) {
							if (fieldKeys[j] != fieldKeys[i]) {
								newArray.push(fieldKeys[j]);
							}
						}
						jwpf.newChangeTableValueOther(fks[0], fks[1], newArray,
								exp, keys, rowIndex);

					} else { // 公式 ：主+...=子
						// 1.主表字段添加监听 //2.计算剩余对象的值
						// jwpf.changeTableValue(fieldKeys[i],fieldKeys,exp,keys);
					}
				}
			}
		} else { //主表字段
			//保存公式到内存数据中
			   var expressionData = $("#"+key).data("expressionData");
			   if(expressionData){
				   jwpf.unbindFormKey(expressionData);
			   }
			  $("#"+key).data("expressionData",exp);
			for(var i =0;i<fieldKeys.length;i++){
				if(fieldKeys[i] && fieldKeys[i]!=""){
				   var fks = fieldKeys[i].split(".");
				   if(fks.length > 1) { //公式可能 ：子+主... =主  或 子+子...=主 			 					   												  
					   //1.子表字段添加监听 //2.计算剩余对象的值
						if(flag){//去除这行当前的监听事件
							var grid = $("#" + keys[0]);
							var pl = grid.edatagrid('getPanel').panel('panel');
							var tr = pl.find("div.datagrid-body").find(
								"tr[datagrid-row-index=" + rowIndex + "]");
							tr.unbind('keyup');
							}
						
						  //保存动态公式到内存数据中
					  	var expAry = $("#"+fks[0]).data("expArray");						  	
					  	var expAll = key + "=" +expression;
					   	if(expAry && expAry.length){						   		
					   		if($.inArray(expAll, expAry)<0){
					   			expAry.push(expAll);
					   			$("#"+fks[0]).data("expArray",expAry);
					   		} 
					   	}else{
					   		var ary = [];
					   		ary.push(expAll);
					   		$("#"+fks[0]).data("expArray",ary);
					   	}
					   if(fks.length==2){					
						jwpf.newChangeFormValueOther(fks[0],fks[1],fieldKeys,exp,key,rowIndex);
					   }else if(fks.length==3){
						 	var newArray = [];
						   	for(var j =0;j<fieldKeys.length;j++){
							   	if(fieldKeys[j]!=fieldKeys[i]){
								   	newArray.push(fieldKeys[j]);
							   		}
						   		}
						 //jwpf.newchangeFormValueTotal(fks[0],fks[1],fks[2],newArray,exp,key,rowIndex);
					   }				 						 
				   } else { //公式 ：主+主....=主 或主+子....=主 
					 //1.主表字段添加监听 //2.计算剩余对象的值						   
					 jwpf.changeFormValue(fieldKeys[i],fieldKeys,exp,key);
				   }
				}
			}
		} 
	},
	//自动处理子表数据
	autoHandleTableFieldValue:function(dgObj, fieldKey, val) {
		var opt = dgObj.datagrid("getColumnOption",fieldKey);
		if(opt && opt.editor ){
			var precision = opt.editor.options.precision;
			var newVal = jwpf.dataHandling(val,precision);
			return newVal;
		} else {
			return val;
		}
	},
	  changeTableValueOther:function(tableKey,targetField,newArray,expression,key){
			 var grid = $("#" + tableKey);
			 if(!grid.length || !grid.data("datagrid")) return null;
			 var exp =  expression;
			 /*var eventName = "input";
			 if(jwpf.getIEVersion() != -1) {
				 eventName = "keyup";
			 }*/
			 var eventName = "keyup";
			 //console.log(eventName);
			 grid.edatagrid('getPanel').panel('panel').on(eventName, function(e) {
				if (jwpf.isNumber(e)) {
					//console.log("1:" + e.timeStamp);
					var input = $(e.target);
					var td = input.closest("td[field]");
					var currentField = td.attr("field");				
					if (currentField == targetField) {
						var tr = td.parent();
						var allrows = grid.datagrid("getRows");
						var index = tr.attr("datagrid-row-index");
						index = parseInt(index);
						var value = input.val();
						if(!value){
							value=0;
						}else{
							value = jwpf.autoHandleTableFieldValue(grid, targetField, value);
							//console.log(input.data('events').keyup);
							//input.val(value);
							//console.log("2:" + e.timeStamp);
						}
						exp = expression.replace(eval("/\\["+tableKey+"."+targetField+"\\]/g"),value);
						allrows[index][targetField] = value;
						jwpf.commonChangeTableValue(tableKey,index,newArray,exp,key);

					}
				}
				exp = expression;
			});
		  },  
		  
		  commonChangeTableValue :function(tableKey,rowIndex,newArray,exp,key){
			  var grid = $("#" + tableKey);
				for ( var h = 0; h < newArray.length; h++) {
					var fields = newArray[h].split(".");
					if (fields.length > 1) { // 子+子...=子
						var allrows = $("#" + fields[0]).edatagrid("getRows");
						var value = allrows[rowIndex][fields[1]];
						if(!value){//处理空值
							value=0;
						}else if(value<0){//处理负数
							value = "("+value+")";
						}
						exp = exp.replace(eval("/\\[" + newArray[h] + "\\]/g"), value);
					} else {// 子+主...=子
						var master = newArray[h].split("_");
						var value = 0;
						if(master.length>1){
							value = $("#" + master[0]).val();
						}else{
							value = $("#" + newArray[h]).val();
						}
						exp = exp.replace(eval("/\\[" + newArray[h] + "\\]/g"), value);
					}
				}
				
				try {
					var newVal = eval(exp);
					var opt = grid.datagrid("getColumnOption",key[1]);
					if(opt && opt.editor ){
						var precision = opt.editor.options.precision;
						newVal = jwpf.dataHandling(newVal,precision);
						var obj = {};
						obj[key[1]] = newVal;
						jwpf.setTableValExp(tableKey,rowIndex,obj);
					}
				} catch(e) {}
	},

	newChangeTableValueOther : function(tableKey, targetField, newArray,
			expression, key, rowIndex) {
		var grid = $("#" + tableKey);
		if(!grid.length || !grid.data("datagrid")) return null;
		var exp = expression;
		var pl = grid.edatagrid('getPanel').panel('panel');
		var tr = pl.find("div.datagrid-body").find(
				"tr[datagrid-row-index=" + rowIndex + "]");
		tr.bind('keyup', function(e) {
			if (jwpf.isNumber(e)) {

				var input = $(e.target);
				var td = input.closest("td[field]");
				var currentField = td.attr("field");
				if (currentField == targetField) {
					var tr = td.parent();
					var allrows = grid.datagrid("getRows");
					var index = tr.attr("datagrid-row-index");
					index = parseInt(index);
					var value = input.val();
					if(!value){
						value=0;
					}else{
						value = jwpf.autoHandleTableFieldValue(grid, targetField, value);
						input.val(value);
					}
					exp = expression.replace(eval("/\\["+tableKey+"."+targetField+"\\]/g"),value);
					for ( var h = 0; h < newArray.length; h++) {
						var fields = newArray[h].split(".");
						if (fields.length > 1) { // 子+子...=子
							var allrows = $("#" + fields[0]).edatagrid(
									"getRows");
							var value = allrows[index][fields[1]];
							if (!value) {
								value = 0;
							}
							exp = exp.replace(eval("/\\[" + newArray[h] + "\\]/g"), value);
						} else {// 子+主...=子
							var value = $("#" + newArray[h]).val();
							if (!value) {
								value = 0;
							}
							exp = exp.replace(eval("/\\[" + newArray[h] + "\\]/g"), value);
						}
					}
					var newVal = eval(exp);
					var opt = grid.datagrid("getColumnOption",key[1]);
					var precision = opt.editor.options.precision;
					newVal = jwpf.dataHandling(newVal,precision);
					var obj = {};
					obj[key[1]] = newVal;
					jwpf.setTableValExp(tableKey, index, obj);
		
				}
			}
			exp = expression;
			//return false;
		});
	},  
		  changeTableValue:function(formKey,fieldList,expression,key){
			  //$("#"+formKey).bind('keyup', function(e) {
			  /*$("#"+formKey).numberbox*/
			  jwpf.bindTextBoxEvent($("#"+formKey), {onChange:function(newValue,oldValue){
			        var exp =  expression;
					//var value = $("#"+formKey).val();
					var value = newValue;
			        if(!value){
						value=0;
					}
					var f ={};
					f[formKey]=value;
					var newArray = [];
					var allrows= [];
					   for(var j =0;j<fieldList.length;j++){
						   if(fieldList[j]!=formKey){
							   newArray.push(fieldList[j]);
						   }
					   }
					   for(var h =0;h<newArray.length;h++){
							  var fields = newArray[h].split(".");
							   if(fields.length>1){	//主+子....=子								
								   allrows =$("#"+fields[0]).edatagrid("getRows");
								   f[newArray[h]] = [];
								   for(var g =0;g<allrows.length;g++){
									   	var value = allrows[g][fields[1]];
									   	if(!value){
											value=0;
										}
									   	f[newArray[h]].push(value);												 											   	
								   }
							   }else{//主+主....=子
								  var value = $("#"+newArray[h]).val();
								  if(!value){
										value=0;
									}
								  f[newArray[h]]=value; 
							   }
						   }
					   
					var m =[];
					for(var t =0;t<fieldList.length;t++){
						if($.isArray(f[fieldList[t]])){
							if(m.length==0){
								m= new Array(f[fieldList[t]].length);
							}
							var values = f[fieldList[t]];
							for(var v =0;v<values.length;v++){
								if(m[v]){
									m[v] = m[v].replace(eval("/\\["+fieldList[t]+"\\]/g"),values[v]);
								}else{
									m[v] = exp.replace(eval("/\\["+fieldList[t]+"\\]/g"),values[v]);
								}								
							}																				
						}else{
							if(m.length==0) {//只有主...的情况
								exp = exp.replace(eval("/\\["+fieldList[t]+"\\]/g"),f[fieldList[t]]);
							} else {
								for(var mi=0;mi<m.length;mi++) {
									if(m[mi]) {
										m[mi] = m[mi].replace(eval("/\\["+fieldList[t]+"\\]/g"),f[fieldList[t]]);
										
									}
								}
							}
						}									
					}
				
					if(m.length==0){
						var newVal = eval(exp);
						var opt = $("#"+key[0]).datagrid("getColumnOption",key[1]);
						var precision = opt.editor.options.precision;
						newVal = jwpf.dataHandling(newVal,precision);
						allrows =$("#"+key[0]).edatagrid("getRows");
						for(var e=0;e<allrows.length;e++){
							var obj = {};
							obj[key[1]] = newVal;
							jwpf.setTableValExp(key[0],e, obj);
							//$("#"+key[0]).edatagrid("refreshRow",e);
						}
					}else{
						var opt =$("#"+key[0]).edatagrid("getColumnOption",key[1]);
						var precision = opt.editor.options.precision;
						allrows =$("#"+key[0]).edatagrid("getRows");
						for(var e=0;e<m.length;e++){							
							var newVal = eval(m[e]);
							newVal = jwpf.dataHandling(newVal,precision);
							var obj = {};
							obj[key[1]] = newVal;
							jwpf.setTableValExp(key[0],e, obj);
							//$("#"+key[0]).edatagrid("refreshRow",e);
						}
					}
					
					setTimeout(function() {
						  jwpf.doSubExpression(key[0]);
						  jwpf.doFormExpression(key[0]);
					},0);
					
					exp = expression;
			}}, "dtDouble");  
			//});	
		  },
	  changeFormValueOther : function(tableKey, targetField, fieldList,
			expression, key) {
		var exp = expression;
		$("#" + tableKey).edatagrid('getPanel').panel('panel').bind(
				'keyup',
				function(e) {
					var newArray = [];
					var fd = tableKey + "." + targetField;
					for ( var j = 0; j < fieldList.length; j++) {
						if (fieldList[j] != fd) {
							newArray.push(fieldList[j]);
						}
					}
					if (jwpf.isNumber(e)) {
						var input = $(e.target);
						var td = input.closest("td[field]");
						var currentField = td.attr("field");
						if (currentField == targetField) {
							var tr=td.parent();	
							var index = tr.attr("datagrid-row-index");
							index=parseInt(index);	
							var allrows = $("#" + tableKey)
									.edatagrid("getRows");
							var opt = $("#"+tableKey).datagrid("getColumnOption",currentField);
							var precision = opt.editor.options.precision;
							var f = {};
							f[fd] = [];
							for ( var k = 0; k < allrows.length; k++) {
								var value = 0;
								if(k==index){
									value = jwpf.dataHandling(input.val(),precision);
								}else{
									value = allrows[k][currentField];
								}
								if (!value) {
									value = 0;
								}
								f[fd].push(value);
								
							}
							jwpf.commonChangeFormVal(key,newArray,fieldList,exp,f);							
						}
					}
					exp = expression;
				});
	},
	commonChangeFormVal : function (formKey,expFields,allFileds,expression,dataObj){//计算的子表每一行
		var exp = expression;
		var f = dataObj?dataObj:{};
		for ( var h = 0; h < expFields.length; h++) {
			var fields = expFields[h].split(".");
			if (fields.length > 1) { //子+子....=主
				allrows = $("#" + fields[0]).edatagrid(
						"getRows");
				f[expFields[h]] = [];
				for ( var g = 0; g < allrows.length; g++) {
					var value = allrows[g][fields[1]];
					if (!value) {
						value = 0;
					}
					f[expFields[h]].push(value);
				}

			} else {// 子+主... =主
				var value = $("#" + expFields[h]).val();
				if (!value) {
					value = 0;
				}
				f[expFields[h]] = value;
			}
		}
		var m = [];
		for ( var t = 0; t < allFileds.length; t++) {
			if ($.isArray(f[allFileds[t]])) {
				if (m.length == 0) {
					m = new Array(f[allFileds[t]].length);
				}
				var values = f[allFileds[t]];
				for ( var v = 0; v < values.length; v++) {
					if (m[v]) {
						m[v] = m[v].replace(eval("/\\["+allFileds[t]+"\\]/g"),values[v]);
					} else {
						m[v] =exp.replace(eval("/\\["+allFileds[t]+"\\]/g"),values[v]);
					}
				}
			} else {
				if (m.length == 0) {// 主...的情况
					exp = exp.replace(eval("/\\["+allFileds[t]+"\\]/g"),f[allFileds[t]]);
				} else {
					for ( var mi = 0; mi < m.length; mi++) {
						if (m[mi]) {
					m[mi] = m[mi].replace(eval("/\\["+allFileds[t]+"\\]/g"),f[allFileds[t]]);	
						}
					}
				}
			}
		}
		var total = 0;
		
		for ( var e = 0; e < m.length; e++) {
			var newVal = eval(m[e]);
			total = total + newVal;
		}
		/*var opt =  $('#'+formKey).numberbox("options");
		var precision = opt.precision;*/
		total = jwpf.dataHandling(total,jwpf.getPrecision(formKey));
		jwpf.setFormVal(formKey,total);
		jwpf.triggerInputEvent(formKey);
	},
	triggerInputEvent:function(fieldKey) {
		$("#"+fieldKey).trigger("input");
	},
	 newChangeFormValueOther : function(tableKey, targetField, fieldList,
				expression, key, rowIndex) {
			var grid = $("#" + tableKey);
			var exp = expression;
			var pl = grid.edatagrid('getPanel').panel('panel');
			var tr = pl.find("div.datagrid-body").find(
					"tr[datagrid-row-index=" + rowIndex + "]");
			tr.bind('keyup', function(e) {
						var newArray = [];
						var fd = tableKey + "." + targetField;
						for ( var j = 0; j < fieldList.length; j++) {
							if (fieldList[j] != fd) {
								newArray.push(fieldList[j]);
							}
						}
						if (jwpf.isNumber(e)) {
							var input = $(e.target);
							var td = input.closest("td[field]");
							var currentField = td.attr("field");
							if (currentField == targetField) {
								var tr=td.parent();	
								var index = tr.attr("datagrid-row-index");
								index=parseInt(index);	
								var allrows = $("#" + tableKey)
										.edatagrid("getRows");
								var f = {};
								f[fd] = [];
								for ( var k = 0; k < allrows.length; k++) {
									var value = 0;
									if(k==index){
										value = input.val();	
									}else{
										value = allrows[k][currentField];
									}
									if (!value) {
										value = 0;
									}
									f[fd].push(value);
									
								}
								jwpf.commonChangeFormVal(key,newArray,fieldList,exp,f);
								
							}
						}
						exp = expression;
					});
		},
		 newChangeFormValueTotal : function(tableKey, targetField, dataType,newArray,
					expression, key, rowIndex) {
				var grid = $("#" + tableKey);
				var exp = expression;
				var pl = grid.edatagrid('getPanel').panel('panel');
				var tr = pl.find("div.datagrid-body").find(
						"tr[datagrid-row-index=" + rowIndex + "]");
				tr.bind('keyup', function(e) {																							
							if (jwpf.isNumber(e)) {
									var index =	0;											
									var input = $(e.target);
									var td = input.closest("td[field]");
									var currentField = td.attr("field");
									if (currentField == targetField) {
										var tr = td.parent();
										var allrows = grid.datagrid("getRows");
										index =  parseInt(tr.attr("datagrid-row-index"));					
										var value = input.val();
										if(!value){
											value=0;
										}else{
											value = jwpf.autoHandleTableFieldValue(grid, targetField, value);
											input.val(value);
										}										
									
									exp = expression.replace(eval("/\\["+tableKey+"."+targetField+"."+dataType+"\\]/g"),value);
									
									for ( var h = 0; h < newArray.length; h++) {
										var fields = newArray[h].split(".");
										if (fields.length > 1 && fields.length==3) { // 子+子....=主
											var allrows = grid.datagrid("getRows");
											var value = allrows[index][fields[1]];
												if (!value) {
													value = 0;
												}
											exp = expression.replace(eval("/\\["+newArray[h]+"\\]/g"),value);											
										} else {// 子+主... =主
											var value = $("#" + newArray[h]).val();
											if (!value) {
												value = 0;
											}
											exp = expression.replace(eval("/\\["+newArray[h]+"\\]/g"),value);
										}
									}
						
									var newVal = eval(exp);
									newVal = jwpf.dataHandling(newVal);
									var obj = {};
									obj[dataType] = newVal;
									jwpf.setTableValExp(tableKey, index, obj);
									var allrows = grid.datagrid("getRows");
									var total = jwpf.getTotalVal(allrows,dataType,dtDouble);
									jwpf.setFormVal(key,total);
								}
							}
							exp = expression;
						});
			},
	changeFormValueOnce : function(tableKey, targetField,dataType,fieldList,
			expression, key) {
		var exp = expression;
		var grid = $("#" + tableKey);
		grid.edatagrid('getPanel').panel('panel').bind(
				'keyup',
				function(e) {
					var newArray = [];
					var fd = tableKey + "." + targetField;
					for ( var j = 0; j < fieldList.length; j++) {
						if (fieldList[j] != fd) {
							newArray.push(fieldList[j]);
						}
					}
					if (jwpf.isNumber(e)) {
						var input = $(e.target);
						var td = input.closest("td[field]");
						var currentField = td.attr("field");
						if (currentField == targetField) {
							var tr=td.parent();	
							var index = tr.attr("datagrid-row-index");
							index=parseInt(index);	
							var allrows = $("#" + tableKey)
									.edatagrid("getRows");
							var total = 0;
							for ( var k = 0; k < allrows.length; k++) {
								var value = 0;							
								if(k==index){
									value = input.val();	
									value = jwpf.autoHandleTableFieldValue(grid, targetField, value);
									input.val(value);
								}else{
									value = allrows[k][currentField];
								}
								if (!value) {
									value = 0;
								}
								if(dataType=="dtLong"){
									value = parseInt(value);					
								}else if(dataType=="dtDouble"){
									value = parseFloat(value);				
								}
								total = total + value;
							}
							exp = expression.replace(eval("/\\["+tableKey+"."+targetField+"."+dataType+"\\]/g"),total);
							
							jwpf.commonChangeFormValOther(key,newArray,exp);
																
						}
					}
					//exp = expression;
				});
	},
	
	commonChangeFormValOther: function(formKey,expFields,expression){//计算的子表一列的总和
		var exp = expression;
		for ( var h = 0; h < expFields.length; h++) {
			var fields =expFields[h].split(".");
			if (fields.length > 1) { // 主+子....=子
				allrows = $("#" + fields[0]).edatagrid(
						"getRows");
				var total = 0;
				for ( var k = 0; k < allrows.length; k++) {
					var value = allrows[k][fields[1]];
					if (!value) {
						value = 0;
					}
					if(fields[2]=="dtLong"){
						value = parseInt(value);					
					}else if(fields[2]=="dtDouble"){
						value = parseFloat(value);				
					}
					total = total + value;									
					}
				exp = exp.replace(eval("/\\[" + expFields[h] + "\\]/g"), total);
			} else {// 子+主... =主
				var value = $("#" + expFields[h]).val();
				if (!value) {
					value = 0;
				}
				exp = exp.replace(eval("/\\[" + expFields[h] + "\\]/g"), value);
			}
		}
		var newVal = eval(exp);	
		/*var opt =  $('#'+formKey).numberbox("options");
		var precision = opt.precision;*/
		newVal = jwpf.dataHandling(newVal,jwpf.getPrecision(formKey));
		jwpf.setFormVal(formKey,newVal);
	},
	  changeFormValue : function(formKey,fieldList,expression,key){
		  var newArray = [];																
			   for(var j =0;j<fieldList.length;j++){
				   if(fieldList[j]!=formKey){
					   newArray.push(fieldList[j]);
				   }
			   }
		  for(var h =0;h<newArray.length;h++){
			  var fields = newArray[h].split(".");
			   if(fields.length==1){//主+主....=主
					/*$("#"+newArray[h]).numberbox({
							onChange :function(newValue,oldValue){
								 jwpf.formToformValue(formKey,fieldList,newArray,expression,key);
							}
					});*/
				   jwpf.bindTextBoxEvent($("#"+newArray[h]), {
						onChange :function(newValue,oldValue){
							 jwpf.formToformValue(formKey,fieldList,newArray,expression,key);
						}
				   }, "dtDouble");
				   /*$("#"+newArray[h]).numberbox("textbox").bind("keyup", function(e) {
					    console.log(e);
						jwpf.formToformValue(formKey,fieldList,newArray,expression,key);
				   });*/
			   }
		   }
		  $("#"+formKey).bind('keyup', function(e) {
			  jwpf.formToformValue(formKey,fieldList,newArray,expression,key);
		  });	
	  },
	  formToformValue :function(formKey,fieldList,newArray,expression,key){
		  var exp =  expression;
			//var value = $("#"+formKey).val();// 2.主表的
		    var value = jwpf.getFormVal(formKey);
			  if(!value){
					value=0;
				}
			var f ={};
			f[formKey]=value;
			var allrows = [];
			   for(var h =0;h<newArray.length;h++){
					  var fields = newArray[h].split(".");
					   if(fields.length==2){	//主+子(当前该列的每一行计算)....=主								
						   allrows =$("#"+fields[0]).edatagrid("getRows");							   
						   f[newArray[h]] = [];
						   for(var g =0;g<allrows.length;g++){
							   	var value = allrows[g][fields[1]];
							   	if(!value){
									value=0;
								}
							   	f[newArray[h]].push(value);												 											   	
						   }
					   }if(fields.length==3){//主+子(当前这一列的总和)....=主
						   allrows =$("#"+fields[0]).edatagrid("getRows");
						   var totalVal = jwpf.getTotalVal(allrows,fields[1],fields[2]);
						   f[newArray[h]] = [];
						   f[newArray[h]].push(totalVal );						   
					   }else{//主+主....=主
						   //var value = $("#"+newArray[h]).val();
						   var value = jwpf.getFormVal(newArray[h]);
						   if(!value){
									value=0;
								}
							  f[newArray[h]]=value;						
					   }
				   }
				var m = [];
				for ( var t = 0; t < fieldList.length; t++) {
					if ($.isArray(f[fieldList[t]])) {
						if (m.length == 0) {
							m = new Array(f[fieldList[t]].length);
						}
						var values = f[fieldList[t]];
						for ( var v = 0; v < values.length; v++) {
							if (m[v]) {
								m[v] = m[v].replace(eval("/\\["+fieldList[t]+"\\]/g"),values[v]);
							} else {
								m[v] =exp.replace(eval("/\\["+fieldList[t]+"\\]/g"),values[v]);
							}
						}
					} else {
						if (m.length == 0) {// 主...的情况
							exp = exp.replace(eval("/\\["+fieldList[t]+"\\]/g"),f[fieldList[t]]);
						} else {
							for ( var mi = 0; mi < m.length; mi++) {
								if (m[mi]) {
									m[mi] = m[mi].replace(eval("/\\["+fieldList[t]+"\\]/g"),f[fieldList[t]]);
								}
							}
						}
					}
				}
				
				if(m.length==0){
					var newVal = eval(exp);
					/*var opt =  $('#'+key).numberbox("options");
					var precision = opt.precision;*/
					newVal = jwpf.dataHandling(newVal,jwpf.getPrecision(key));
					jwpf.setFormVal(key,newVal);
					jwpf.triggerInputEvent(key);
				}else{
					var total = 0;
					for ( var e = 0; e < m.length; e++) {
						var newVal = eval(m[e]);
						total = total + newVal;
					}
					/*var opt =  $('#'+key).numberbox("options");
					var precision = opt.precision;*/
					total = jwpf.dataHandling(total,jwpf.getPrecision(key));
					jwpf.setFormVal(key,total);	
					jwpf.triggerInputEvent(key);
				}	
				//$("#"+key).trigger("input");//手动触发input事件
				exp = expression;	  
	  },
	  getPrecision:function(key) {
		  var jqObj = $('#'+key);
		  if(jqObj.hasClass("numberbox-f")) {
			  var opt =  jqObj.numberbox("options");
			  return opt.precision;
		  } else {
			  return jqObj.attr("options");
		  }
	  },//子表公式自动计算
	  doSubExpression: function (tableKey,rowIndex, fieldKey){
			//执行该子表中相关的公式（子+....=子）
			var expAry = $("#"+tableKey).data("subExpArray");
		  	if(expAry && expAry.length){	
		  		for ( var j = 0; j <expAry.length; j++) {
		  			var  expression = [];				
					expression.push(expAry[j].slice(0,expAry[j].indexOf("=")));
					expression.push(expAry[j].slice(expAry[j].indexOf("=")+1));		  			
					var regex = /([^\[]+?)(?=\])/gi;
					var allfieldKeys =(expression[1]).match(regex);
					allfieldKeys = allfieldKeys?allfieldKeys:[];
					allfieldKeys = jwpf.uniqueArr(allfieldKeys);
					//只自动计算fieldKey相关的字段
					if(!fieldKey || (fieldKey && allfieldKeys.indexOf(tableKey+"."+fieldKey) >= 0)) {
						var keys = expression[0].split(".");
						if(rowIndex){
							jwpf.commonChangeTableValue(tableKey,rowIndex,allfieldKeys,expression[1],keys);	
						}else{
							var allrows = $("#" + tableKey).edatagrid("getRows");
							for(var i = 0; i <allrows.length; i++){
								jwpf.commonChangeTableValue(tableKey,i,allfieldKeys,expression[1],keys);	
							}
						}
					}
				}
		  	}
	  },//主表公式自动计算
	  doFormExpression: function (tableKey, fieldKey){
		  //执行该子表中相关的公式（子+....=主）
		var expAry = $("#"+tableKey).data("expArray");
	  	if(expAry && expAry.length){		
	  		for ( var j = 0; j <expAry.length; j++) {
	  			var  expression = [];				
				expression.push(expAry[j].slice(0,expAry[j].indexOf("=")));
				expression.push(expAry[j].slice(expAry[j].indexOf("=")+1));				  			
				var regex = /([^\[]+?)(?=\])/gi;
				var allfieldKeys =(expression[1]).match(regex);
				allfieldKeys = allfieldKeys?allfieldKeys:[];
				allfieldKeys = jwpf.uniqueArr(allfieldKeys);
				//只自动计算fieldKey相关的字段
				if(!fieldKey || (fieldKey && allfieldKeys.indexOf(tableKey+"."+fieldKey) >= 0)) {
					var flag = false;
					for ( var k = 0; k <allfieldKeys.length; k++) {
						var fields = allfieldKeys[k].split(".");
						if (fields.length > 1 && fields.length==3){
							flag = true;
							break;
						}
					}				
					if(flag){
						//计算子表字段的总和的赋值给主表字段
						jwpf.commonChangeFormValOther(expression[0],allfieldKeys,expression[1]);
					}else{
						jwpf.commonChangeFormVal(expression[0],allfieldKeys,allfieldKeys,expression[1]);
					}															
				}
			}
	  	}
	  },
	  //判断输入的是否为数字
	  isNumber :function(e){		
		  if(e.which==45 || e.which==46){
			  return true;
		  }else if(e.which>=96 && e.which<=105){
			  return true;
		  }
		  if(e.which==46){
			  return true;
		  }else{
			  if((e.which>=48&&e.which<=57&&e.ctrlKey==false&&e.shiftKey==false)||e.which==0||e.which==8){
				  return true;;
			  }else{
				  if(e.ctrlKey==true&&(e.which==99||e.which==118)){
					  return true;
				  }else{
					  return false;
				  }
			  }
		  }
	  },
	  bindEvent:function(id, eventObj, editType, dataType) {
		try {
		  var obj = $("#" + id);
		  switch(editType) {
			  case "TextBox":
				  jwpf.bindTextBoxEvent(obj, eventObj, dataType);
				  break;
			  case "ComboBox":
				  if(obj.hasClass("combobox-f")) {
					  obj.combobox(eventObj);
				  } else if(obj.hasClass("autocomplete")) {
					  $ES.bindAutoCompleteEvent(obj, eventObj);
				  } else {
					  $ES.bindComboBoxEvent(obj, eventObj);
				  }
				  break;
			  case "ComboBoxSearch":
				  if(obj.hasClass("combobox-f")) {
					  obj.comboboxsearch(eventObj);
				  } else if(obj.hasClass("autocomplete")) {
					  $ES.bindAutoCompleteEvent(obj, eventObj);
				  } else {
					  $ES.bindComboBoxEvent(obj, eventObj);
				  }
				  break;	  
			  case "ComboTree":
				  obj.combotree(eventObj);
				  break;
			  case "RadioBox":
			  case "CheckBox":	
				  if(obj.hasClass("combo-f")) {
					  $.extend(obj.combo("options"),eventObj);
				  } else {
					  $ES.bindCheckBoxEvent(obj, eventObj);
				  }
				  break;
			  case "DataGrid":
				  obj.datagrid(eventObj);
				  break;
			  case "TreeGrid":
				  obj.treegrid(eventObj);
				  break;
			  case "Tree":
				  obj.tree(eventObj);
				  break;
			  case "ComboGrid":
				  obj.combogrid(eventObj);
				  break;
			  case "Button":	
				  obj = $("#bt_" + id);
				  jwpf.bindDefaultEvent(obj, eventObj);
				  break;
			  case "HyperLink":
				  obj = $("#hl_" + id);
				  jwpf.bindDefaultEvent(obj, eventObj);
				  break;
			  case "zTree":
				  var treeObj = $.fn.zTree.getZTreeObj(id);
				  var set = treeObj.setting;
				  set.callback = eventObj;
				  break;	  
			  case "DialogueWindow":
				  $.extend(obj.dialoguewindow("options"),eventObj);
				  break;	
			  case "ImageBox":
				  $.extend(obj.imagebox("options"),eventObj);
				  break;	  
			  default:
				  jwpf.bindDefaultEvent(obj, eventObj);
		  }
		} catch(e) {alert("为" + id + "绑定事件时出现异常：" + e);}
	  },
	  bindDefaultEvent:function(obj, eventObj) {
		  $.each(eventObj, function(k, v) {
			  if(k) {
				  var eventName = k;
				  if(eventName.indexOf("on") >= 0) {
					  eventName = eventName.substr(2);
				  }
				  //console.debug(k);
				  //obj.unbind(eventName, v);
				  obj.bind(eventName, v);
			  }
		  });
	  },
	  bindTextBoxEvent:function(obj, eventObj, dataType) {
		  if(dataType=="dtLong" || dataType=="dtInteger" || dataType=="dtDouble") {
			  if(obj.hasClass("numberbox-f")) {
				  obj.numberbox(eventObj); 
				  if(eventObj["onfocus"] || eventObj["onblur"]) {
					  jwpf.bindDefaultEvent(obj, eventObj);
				  }
			  } else {
				  if(eventObj["onChange"]) {
					  eventObj["oninput"] = function(event) {
						  var newVal = event.delegateTarget.value, oldVal = null;
						  eventObj["onChange"].call(obj, newVal, oldVal);
					  };
				  }
				  jwpf.bindDefaultEvent(obj, eventObj);
			  }
		  } else {
			  if(obj.hasClass("textbox-f")) {
				  if(eventObj["onChange"]) {
					  obj.textbox(eventObj);
				  }
				  jwpf.bindDefaultEvent(obj.textbox("textbox"), eventObj);
			  } else {
				  if(eventObj["onChange"]) {
					  eventObj["oninput"] = function(event) {
						  var newVal = event.delegateTarget.value, oldVal = null;
						  eventObj["onChange"].call(obj, newVal, oldVal);
					  };
				  }
				  jwpf.bindDefaultEvent(obj, eventObj);
			  }
		  }
	  },
	  findPreOrNextEditTd : function(td, isPre) {
				var nextTd = null;
				if(isPre) {
					nextTd = td.prev();
				} else {
					nextTd = td.next();
				}
				if(nextTd.length) {
					return jwpf.findEditInput(nextTd, isPre);
				} else {
					//找下一行
					var tr = td.closest("tr");
					var nextTr = null;
					if(isPre) {
						nextTr = tr.prev();
					} else {
						nextTr = tr.next();
					}
					if(nextTr.length) {
						if(isPre) {
							nextTd = nextTr.find("td:last");
						} else {
							nextTd = nextTr.find("td:first");
						}
						return jwpf.findEditInput(nextTd, isPre);
					} else {
						return null;
					}
				}
	  },		
	  findEditInput:function(nextTd, isPre) {
		    var nextInputs = nextTd.find("input,select,textarea");
			if(nextInputs.length) {
				var nextInput = nextInputs[0];
				if($(nextInput).hasClass("combo-f")) {
					return nextInput;
				} else {
					if($(nextInput).is(":visible")) {
						if($(nextInput).attr("readonly") || $(nextInput).attr("disabled")
								|| $(nextInput).attr("type") == "button") {
							return jwpf.findPreOrNextEditTd(nextTd, isPre);
						} else {
							return nextInput;
						}
					} else {
						return jwpf.findPreOrNextEditTd(nextTd, isPre);
					}
				}
			} else {
				return jwpf.findPreOrNextEditTd(nextTd, isPre);
			}
	  },
	  switchEditInput:function(input, isPre) {
		  var td = input.closest("td");
		  var nextInput = jwpf.findPreOrNextEditTd(td, isPre);
		  if(nextInput){
			  /*现在combo已经在显示input上取消了独立样式，继承了text-box*/
			    /*if (input.hasClass("combo-text")) {
					$(td.find("input,select")[0])
						.combo('hidePanel');
				}*/
			  td.find("input.combo-f,select.combo-f").combo('hidePanel');
				if ($(nextInput).hasClass("combo-f")) {
					$(nextInput).combo('showPanel');
				}
				$(nextInput).focusEnd();
		  }
	  },//向上或向下查找可编辑控件
	  findUpDownEditInput:function(nextTd, isUp) {
		    var curTr = nextTd.parent();
		    var tb = nextTd.parent().parent();
		    var trIndex = $("tr",tb).index(curTr);
		    //var tdIndex = $("td", curTr).index(nextTd);
		    var left = nextTd.offset().left;
		    var trs = $("tr", tb);
		    if(isUp) {
		    	trIndex--;
				var nextInput = null;
		    	for(var i=trIndex ;i>=0 ; i--) {
		    		$("td", trs[i]).each(function(j){
			    		if($(this).offset().left + $(this).width() > left) {
			    			var nextInputs = $(this).find("input,select,textarea");
			    			if(nextInputs.length) {
			    				nextInput = nextInputs[0];
			    				if(!$(nextInput).hasClass("combo-f")) {
			    					if($(nextInput).is(":visible")) {
			    						if($(nextInput).attr("readonly") || $(nextInput).attr("disabled")
			    								|| $(nextInput).attr("type") == "button") {
			    							nextInput = null;
			    						}
			    					} else {
			    						nextInput = null;
			    					}
			    				}
			    			} else {
			    				nextInput = null;
			    			}
			    			return false;
			    		}
			    	});
			    	if(nextInput !== null) return nextInput;
			    }
		    } else {
		    	trIndex++;
		    	var nextInput = null;
		    	for(var i=trIndex ;i<trs.length ; i++) {
			    	$("td", trs[i]).each(function(j){
			    		if($(this).offset().left + $(this).width() > left) {
			    			var nextInputs = $(this).find("input,select,textarea");
			    			if(nextInputs.length) {
			    				nextInput = nextInputs[0];
			    				if(!$(nextInput).hasClass("combo-f")) {
			    					if($(nextInput).is(":visible")) {
			    						if($(nextInput).attr("readonly") || $(nextInput).attr("disabled")
			    								|| $(nextInput).attr("type") == "button") {
			    							nextInput = null;
			    						}
			    					} else {
			    						nextInput = null;
			    					}
			    				}
			    			} else {
			    				nextInput = null;
			    			}
			    			return false;
			    		}
			    	});
			    	if(nextInput !== null) return nextInput;
			    }
		    }
		    /*var nTd = $("td",$("tr:eq("+trIndex+")", tb)).eq(tdIndex);
		    if(nTd.length == 0) return null;
		    var nextInputs = nTd.find("input,select,textarea");
			if(nextInputs.length) {
				var nextInput = nextInputs[0];
				if($(nextInput).hasClass("combo-f")) {
					return nextInput;
				} else {
					if($(nextInput).is(":visible")) {
						if($(nextInput).attr("readonly") || $(nextInput).attr("disabled")
								|| $(nextInput).attr("type") == "button") {
							return jwpf.findUpDownEditInput(nTd, isUp);
						} else {
							return nextInput;
						}
					} else {
						return jwpf.findUpDownEditInput(nTd, isUp);
					}
				}
			} else {
				return jwpf.findUpDownEditInput(nTd, isUp);
			}*/
	  },
	  switchUpDownEditInput:function(input, isUp) {
		  var td = input.closest("td");
		  var nextInput = jwpf.findUpDownEditInput(td, isUp);
		  if(nextInput){
			    if (input.hasClass("combo-text")) {
					$(td.find("input,select")[0])
						.combo('hidePanel');
				}
				if ($(nextInput).hasClass("combo-f")) {
					$(nextInput).combo('showPanel');
				}
				$(nextInput).focusEnd();
		  }
	  },
	  bindKeydownOnForm : function(id) {
		$("#" + id)
				.bind(
						'keydown',
						function(e) {
							var input = $(e.target);
							switch (e.keyCode) {
							case 38: // up
								jwpf.switchUpDownEditInput(input, true);
								break;
							case 40: // down	
								jwpf.switchUpDownEditInput(input);
							    break;
							case 37: // left
								if(input.getSelectionStart() == 0) {
									jwpf.switchEditInput(input, true);
								}
								break;
							case 39: // right
								var valLength = input.val().length;
								if(input.getSelectionStart() == valLength) {
									jwpf.switchEditInput(input);
								}
								break;
							case 13: // 回车
								if(e.ctrlKey && input[0].type=="textarea"){
									/*var content = input.val();
									content = content+'\n';
									input.val(content);*/
									input.insert({"text":"\n"});
								}else{
									jwpf.switchEditInput(input);
								}
								break;
							}
						});
	},
	focusFirstInputOnFormTabs : function(id) {
		var tab = $("#" + id).tabs('getSelected');
		/*避免选到主表控件*/
		var tbl = tab.find("table[id]:not([grid_widget])");
		if(tbl && tbl.length>0){
 			var childId = $(tbl[0]).attr("id");
			var opts = $("#"+childId).edatagrid('options');    
			var editIndex = opts.editIndex;
			var editCol =opts.editCol;
			if(editIndex && editCol){
				$("#"+childId).edatagrid('editCol',{"index":editIndex,"field":editCol});
				$("#"+childId).focusEditor(editCol);
			}else{
				$("#"+childId).edatagrid('addRow');
			}
		} else {
			var body = tab.panel('body');
			var input = body.find("input:visible:not([readonly]),select:visible:not([readonly]),textarea:not([readonly])");
			if (input && input.length > 0) {
				if ($(input[0]).hasClass("combo-f")) {
					$(input[0]).combo('showPanel');
				} else if($(input[0]).hasClass("jquery_ckeditor")) {
					var ckId = $(input[0]).attr("id");
					CKEDITOR.instances[ckId].focus();
					return;
				}
				$(input[0]).focusEnd();
			}
		}
	},
	focusFirstInputOnForm : function(id) {
		var input = $("#" + id).find("input:visible:not([readonly]),select:visible:not([readonly]),textarea:not([readonly])");
		if (input && input.length > 0) {
			if ($(input[0]).hasClass("combo-f")) {
				$(input[0]).combo('showPanel');
			} else if($(input[0]).hasClass("jquery_ckeditor")) {
				var ckId = $(input[0]).attr("id");
				CKEDITOR.instances[ckId].focus();
				return;
			}
			$(input[0]).focusEnd();
		}
	},//发送业务通知
	doSendBusinessMessage : function(options){
		//		var options = {
		//				"userId" : ,
		//				"content" :,
		//				"title" :,
		//				"moduleDataId" :,
		//				"moduleKey" :,
		//				"fields" : []
		//		}
		jwpf.sendMessage(options,jwpf.messageStyle.BUSINESS);
	},
	//发送业务预警通知
	doSendWarningMessage : function(options){
		jwpf.sendMessage(options,jwpf.messageStyle.WARNING);
	},
	//发送待办事宜通知
	doSendTodoMessage : function(options){
		jwpf.sendMessage(options,jwpf.messageStyle.TODO);
	},
	//发送普通通知
	doSendCommonMessage : function(options){
		jwpf.sendMessage(options,jwpf.messageStyle.COMMON);
	},//发送业务办理通知
	doSendBusinessDealMessage : function(options){		
		jwpf.sendMessage(options,jwpf.messageStyle.BUSINESS_DEAL);
	},
	sendMessage : function(opt,style){
		var sendJsondata = JSON.stringify(opt);
	   	var options = {
	             url : __ctx+'/gs/gs-mng!sendMessage.action',
	             data : {
	            	 "jsonSenddata" : sendJsondata,
	            	 "style":style
	             },
	             success : function(data) {
	            	 
	             }
	        };
	        fnFormAjaxWithJson(options);
	},
	bindHotKeysOnListPage : function(){
        jQuery(document).bind('keydown', 'Ctrl+E',function (evt){evt.preventDefault();if($("#bt_view").is(":visible")){$("#bt_view").trigger('click');} return false; });//查看
        jQuery(document).bind('keydown', 'Ctrl+G',function (evt){evt.preventDefault();if($("#bt_copy").is(":visible")){$("#bt_copy").trigger('click');} return false; });//复制
        jQuery(document).bind('keydown', 'Ctrl+I',function (evt){evt.preventDefault();if($("#bt_add").is(":visible")){$("#bt_add").trigger('click');} return false; });//新增
        jQuery(document).bind('keydown', 'Ctrl+Y',function (evt){evt.preventDefault();if($("#bt_accessory").is(":visible")){$("#bt_accessory").trigger('click');} return false; });//附件
        jQuery(document).bind('keydown', 'Ctrl+R',function (evt){evt.preventDefault();if($("#bt_report").is(":visible")){$("#bt_report").trigger('click');} return false; });//报表
        jQuery(document).bind('keydown', 'Ctrl+D',function (evt){evt.preventDefault();if($("#bt_del").is(":visible")){$("#bt_del").trigger('click');} return false; });//删除
  
	},
	bindHotKeysOnViewPage : function(){
		jQuery("input,textarea").live('keydown','Ctrl+Q',function (evt){
        	evt.preventDefault();
        	if($("#tclose").is(":visible")){
        		$(this).blur();
        		$("#tclose").trigger('click');
        		} 
        	evt.stopPropagation();
        	return false; 
        	});//关闭
        jQuery(document).bind('keydown', 'Ctrl+Q',function (evt){evt.preventDefault();if($("#tclose").is(":visible")){$("#tclose").trigger('click');} return false; });//关闭
        jQuery("input,textarea").live('keydown','Ctrl+U',function (evt){
        	evt.preventDefault();
        	if($("#tcancel").is(":visible")){
        		$(this).blur();
        		$("#tcancel").trigger('click');
        		}
        	evt.stopPropagation();
        	return false;
        	});//取消
        jQuery(document).bind('keydown', 'Ctrl+U',function (evt){evt.preventDefault();if($("#tcancel").is(":visible")){$("#tcancel").trigger('click');} return false; });//取消
        jQuery("input,textarea").live('keydown', 'Ctrl+S',function (evt){       	     	
        	evt.preventDefault();
        	if($("#tsave").is(":visible")){
        		$(this).blur();
        		doSaveObj();
        	}
        	evt.stopPropagation();
        	return false; 
        });//保存
        jQuery(document).bind('keydown', 'Ctrl+S',function (evt){
        	evt.preventDefault();
        	if($("#tsave").is(":visible")){
        		doSaveObj();
        	} 
        	return false; 
        });//保存
        jQuery(document).bind('keydown', 'Ctrl+M',function (evt){evt.preventDefault();if($("#tedit").is(":visible")){$("#tedit").trigger('click');} return false; });//修改
        jQuery(document).bind('keydown', 'Ctrl+G',function (evt){evt.preventDefault();if($("#tcopy").is(":visible")){$("#tcopy").trigger('click');} return false; });//复制
        jQuery(document).bind('keydown', 'Ctrl+I',function (evt){evt.preventDefault();if($("#tadd").is(":visible")){$("#tadd").trigger('click');} return false; });//新增
        jQuery(document).bind('keydown', 'Ctrl+Y',function (evt){evt.preventDefault();if($("#tattach").is(":visible")){$("#tattach").trigger('click');} return false; });//附件
        jQuery(document).bind('keydown', 'Ctrl+R',function (evt){evt.preventDefault();if($("#bt_report").is(":visible")){$("#treport").trigger('click');} return false; });//报表
        jQuery(document).bind('keydown', 'Ctrl+F',function (evt){evt.preventDefault();if($("#tfirst").is(":visible")){$("#tfirst").trigger('click');} return false; });//第一条
        jQuery(document).bind('keydown', 'Ctrl+right',function (evt){evt.preventDefault();if($("#tnext").is(":visible")){$("#tnext").trigger('click');} return false; });//下一条
        jQuery(document).bind('keydown', 'Ctrl+left',function (evt){evt.preventDefault();if($("#tprev").is(":visible")){$("#tprev").trigger('click');} return false; });//上一条
        jQuery(document).bind('keydown', 'Ctrl+L',function (evt){evt.preventDefault();if($("#tlast").is(":visible")){$("#tlast").trigger('click');}return false; });//最后一条
        jQuery(document).bind('keydown', 'Ctrl+P',function (evt){evt.preventDefault();if($("#tprocess").is(":visible")){$("#tprocess").trigger('click');}return false; });//启动流程
        jQuery(document).bind('keydown', 'Ctrl+E',function (evt){evt.preventDefault();if($("#tviewprocess").is(":visible")){$("#tviewprocess").trigger('click');} return false; });//查看流程
        jQuery(document).bind('keydown', 'Ctrl+J',function (evt){evt.preventDefault();if($("#tcancelprocess").is(":visible")){$("#tcancelprocess").trigger('click');}return false; });//取消审核
        jQuery(document).bind('keydown', 'Ctrl+D',function (evt){evt.preventDefault();if($("#tdel").is(":visible")){$("#tdel").trigger('click');} return false; });//删除
      /*  jQuery(document).bind('keydown', 'Ctrl+J',function (evt){evt.preventDefault();if($("#stInsert").is(":visible")){$("#stInsert").trigger('click');} return false; });//插入 insert
        jQuery(document).bind('keydown', 'Ctrl+H',function (evt){evt.preventDefault();if($("#stCopy").is(":visible")){$("#stCopy").trigger('click');} return false; });//复制  copy
        jQuery(document).bind('keydown', 'Ctrl+Y',function (evt){evt.preventDefault();if($("#stMoveUp").is(":visible")){$("#stMoveUp").trigger('click');} return false; });//上移 moveUp
        jQuery(document).bind('keydown', 'Ctrl+X',function (evt){evt.preventDefault();if($("#stMoveDown").is(":visible")){$("#stMoveDown").trigger('click');} return false; });//下移 moveDown
        jQuery(document).bind('keydown', 'Ctrl+D',function (evt){evt.preventDefault();if($("#stDelete").is(":visible")){$("#stDelete").trigger('click');} return false; });//删除  delete
       */	
        //绑定双击复制事件    
        $("body").append('<button id="_copy_text_bt_" type="button" class="validInView" style="display:none;"></button>');
        var clipboard = new ClipboardJS("#_copy_text_bt_", {
		    text: function(trigger) {
		    	return $("#_copy_text_bt_").data("text");
		    }
		});

		clipboard.on('success', function(e) {
			//console.log(e);
			alert("已复制到剪切板！");
		});

		clipboard.on('error', function(e) {
		    console.log(e);
		    alert("复制失败，请尝试重新复制！");
		});
        
	    $("input,textarea,span.select2-selection").on("dblclick", function(event) {
	        	var obj = this;
	        	if($(obj).is("span")) {
	        		var txt = $(obj).text() || "";
	        		txt = txt.slice(1).replace(/×/g," ");
	        		$("#_copy_text_bt_").data("text", txt);
		    	} else {
		    		$("#_copy_text_bt_").data("text", $(obj).val());
		    	}
	        	$("#_copy_text_bt_").trigger("click");
	    });
        },
        getFilterData : function(sqlKey,filterParam){				 
      		 	var  param = {
      		 			"entityName" :	sqlKey
      		 	};
      		 	$.extend(param,filterParam);
      		  	var jsonData=null;
      			var opt = {
      					data:param,
      					async:false,
      					url:__ctx+"/gs/gs-mng!dataFilter.action",
      					success:function(data){
      						jsonData=data.msg;
      					}
      			};
      		  	fnFormAjaxWithJson(opt, true);
      			return jsonData;
      	},
      	getFilterDataAsync : function(sqlKey,filterParam,callFunc){				 
  		 	var  param = {
  		 			"entityName" :	sqlKey
  		 	};
  		 	$.extend(param,filterParam);
  		  	var jsonData=null;
  			var opt = {
  					data:param,
  					async:false,
  					url:__ctx+"/gs/gs-mng!dataFilter.action",
  					success:function(data){
  						if(callFunc) callFunc(data.msg);
  					}
  			};
  		  	fnFormAjaxWithJson(opt, true);
      	},
      	//根据sqlKey查找查询字段列表
      	getFieldsByKey : function(sqlKey){				 
  		 	var  param = {
  		 			"entityName" :	sqlKey
  		 	};
  		  	var jsonData=null;
  			var opt = {
  					data:param,
  					async:false,
  					url:__ctx+"/gs/gs-mng!getFieldsBySqlKey.action",
  					success:function(data){
  						jsonData=data.msg;
  					}
  			};
  		  	fnFormAjaxWithJson(opt, true);
  			return jsonData;
      	},
      	getFieldValBySql : function(sqlKey,filterParam, fieldKey) {
      		var data = jwpf.getFilterData(sqlKey,filterParam);
      		var fk = fieldKey;
      		if(data && data.list && data.list.length) {
      			return fk ? data.list[0][fk] : data.list[0];
      		} else {
      			return "";
      		}
      	},
      	getFieldValBySqlAsync : function(sqlKey,filterParam, fieldKey, callFunc) {
      		var fk = fieldKey, cf = callFunc;
      		jwpf.getFilterDataAsync(sqlKey,filterParam, function(data) {
          		var result = "";
      			if(data && data.list && data.list.length) {
      				result = (fk ? data.list[0][fk] : data.list[0]);
          		}
      			if(cf) {
      				cf(result);
      			}
      		});
      	},
        getQueryData : function(sqlKey,filterParam,event){				 
  		 	var  param = {
  		 			"entityName" :	sqlKey
  		 	};
  		 	$.extend(param,filterParam);
  		  	var jsonData=null;
  			var opt = {
  					data:param,
  					async:false,
  					url:__ctx+"/gs/gs-mng!dataFilter.action",
  					success:function(data){
  						jsonData=data.msg;
  						var list = jsonData['list'];
  						var opt = {
  								"top" : 0,
  								"left" : 0,
  								"width" : 0,
  								"height" : 0
  						};
  						opt.top = $(event.target).position().top+$(event.target).height()+5;
  						opt.left = $(event.target).position().left;
  						opt.width = $(event.target).width();
  						opt.height = 150;
  						opt.object = $(event.target);
  						jwpf.createPanel(list,opt);
  					}
  			};
  		  	fnFormAjaxWithJson(opt, true);
  			return jsonData;
        },
  		createPanel : function (list,opts){		
				var len=list.length;
				if(len>0){
				var arr=[];
				for(var i=0;i<len;i++){//拼装内容
					var dataObject=list[i];
					 var str =
							"<li><span>"
							+dataObject
							+"</span></li>";
					 arr.push(str); 
				}
				var content=arr.join("");
				var color = opts.color || "red";
				var dp = $('#_query_pl_').length;
				var divs = "<div id='_query_content_' style='color:" + color + "'>"+content+"</div>";
				if(!dp){	
					var options = {
							"top" : 0,
							"left" : 0,
							"width" : 150,
							"height" : 150
					};
					$.extend(options,opts);
					var top = options.top;
					var left = options.left;
					var width = options.width;
					var height = options.height;
					var pl = "<div id='_query_pl_' style='position: absolute; top:"+top+"px;left:"+left+"px;z-index: 9000;width:"+width+"px;height:"+height+"px;padding:10px;background:#fafafa;'> ";
					var obj = options.object;
					if(obj){
						obj.after(pl);
					}else{
						$("body").append(pl);
					}
					
					$('#_query_pl_').panel({
						"content" : divs
					});
				}else{
					$('#_query_pl_').show();							
					$('#_query_content_').html(content);
				}
				window.onmousedown = function(){
					$('#_query_pl_').hide();
				};
			}
  	},
      	doOpenDataFilterWin : function(sqlKey,filterParam,childTableKey,winTitle,onAfterSure, width, height){
      		var title = winTitle || "数据筛选";
      		var param = JSON.stringify(filterParam);
      		var wid = width || 600;
      		var hei = height || 400;
      		var url = __ctx+"/gs/gs-mng!dataFilterWin.action?sqlKey="+sqlKey+
      													"&filterParam="+encodeURI(param);
      		var callFunc = function() {
      			var flag = saveFilterData__(childTableKey);
      			if(flag) {
      				if(onAfterSure) onAfterSure();
      				doCloseModuleOperDialog();
      			}
      		};
      		doOpenModuleOperDialog(url, title, wid, hei, callFunc);
      	},
      	createProc : function(content){
    	   	var options = {
    	             url : __ctx+'/gs/gs-mng!createProcedure.action',
    	             data : {
    	            	 "procContent":content
    	             },
    	             success : function(data) {
    	            	 if(data.msg){
    	            		 alert("创建成功");
    	            	 }
    	             }
    	        };
    	        fnFormAjaxWithJson(options,true);
    	},
    	callProc : function(key,filterParam, callFunc, isShowProgress){
    		var isSP = isShowProgress || false;
    		var  param = {
  		 			"procKey" :	key
  		 	};
  		 	$.extend(param,filterParam);
    	   	var options = {
    	   			data:param,
  					async:isSP,
    	            url : __ctx+'/gs/gs-mng!callProcedure.action',
    	            success : function(data) {
    	            	 if(data.msg){
    	            		if(callFunc) {
								callFunc(data.msg);
							}
    	            	 }
    	             }
    	        };
    	    fnFormAjaxWithJson(options,!isSP);
    	},//更新子表combo控件类型编辑属性，不支持combogrid、combotree
    	updateTableComboFieldOption : function(tableKey, fieldKey, data, isNeedReEdit) {
    		var dgId = "#" + tableKey;
    		var opt = $(dgId).data("edatagrid");
    		var paramObj = {"key":"key","caption":"caption"};
	     	if(opt) {
		     	var fieldOpt = $(dgId).edatagrid("getColumnOption", fieldKey);
		     	fieldOpt.editor.options.data = data;
		     	fieldOpt.editor.options.valueField = paramObj.key;
		     	fieldOpt.editor.options.textField = paramObj.caption;
	     	}
	     	var obj = {"data":data,"obj":paramObj};
	     	eval("sub_" + tableKey + "_" + fieldKey + "Json = obj;");
	    	if(isNeedReEdit && opt) {
	    		var opts = opt.options;
				if (opts.editIndex >= 0 && opts.editCol==fieldKey) {
					var eIndex = opts.editIndex;
					var editOpt = {
							"index" : opts.editIndex,
							"field" : fieldKey
						};
					$(dgId).edatagrid('endCellEdit', editOpt);
					$(dgId).edatagrid('beginCellEdit',editOpt);
					opts.editIndex = eIndex;
					opts.editCol = fieldKey;
				}
	    	}
    	},//更新子表combo控件类型数据属性，不支持combogrid、combotree
    	updateTableComboFieldDataOption : function(tableKey, fieldKey, data) {
    		if(tableKey) {
    			var dgId = "#" + tableKey;
        		var opt = $(dgId).data("edatagrid");
    	     	if(opt) {
    		     	var fieldOpt = $(dgId).edatagrid("getColumnOption", fieldKey);
    		     	fieldOpt.editor.options.data = data;
    	     	}
    		} else {
    			var object = $("#" + fieldKey);
    			var className = object.attr("class");
    			if(className){
    				if(className.indexOf("easyui") >=0 && className.indexOf("combobox") >=0) {//老控件
    					object.combobox({
    						data:data,
    						valueField:"id",
    						textField:"text"
    					});
    				} else if(className.indexOf("combobox") >=0) {//新控件
    					object.empty();
						for(var i in data) {
							var row = data[i];
							var $opt = $('<option value="'+ row.id +'" >'+ (row.text) +'</option>');
							$opt.data("data", row);
							jqObj.append($opt);
						}
						object.trigger("change");
    				}
    			}
    		}
    	},
    	//打开数据项列表，横向展示
    	doOpenItemListWin : function(winParam){
    		var defaultParam = {"title":"数据筛选",
    							"width":600,
    							"height":400,
    							"sqlKey":"",
    							"filterParam":{},
    							"onAfterSure":null};
      		var param = $.extend(defaultParam, winParam);
      		var filterParam = JSON.stringify(param.filterParam);
      		var url = __ctx+"/sys/common/item-list.action?sqlKey="+param.sqlKey+
      													"&filterParam="+encodeURI(filterParam);
      		var callFunc = function() {
      			var items = _getItemList_();
      			if(items) {
	      			var flag = false;
	      			if(param.onAfterSure) flag = param.onAfterSure(items);
	      			if(flag) {
	      				doCloseModuleOperDialog();
	      			}
      			}
      		};
      		doOpenModuleOperDialog(url, param.title, param.width, param.height, callFunc);
      	},
      	//设置子表数据，isAppend表示是否追加
      	setTableData : function(tableKey, rowsData, isAppend, isAutoAddEmptyRow, isCopyCol) {
			var isAddRow = !(isAutoAddEmptyRow === false);//默认自动加行 
      		if(rowsData) {
      			var dg = $('#'+tableKey);
      			var oldRows = dg.edatagrid("getRows");
      			if(!isAppend) {
          			//覆盖更新，先删除原有历史数据
      				if(oldRows && oldRows.length) {
      					for(var i=oldRows.length-1;i>=0;i--) {
      						dg.edatagrid("deleteRow", i);
      					}
      				}
          		} else {
          			//删除最后一个空行或默认行
      				if(oldRows && oldRows.length) {
      					var rowIndex = oldRows.length - 1;
      					if(dg.edatagrid("validEmptyOrDefaultRow", oldRows[rowIndex])) {
      						dg.edatagrid("deleteRow", rowIndex);
      					}
      				}
          		}
      			//追加记录
      			if(isCopyCol) {
	  				for(var i=0,len=rowsData.length;i<len;i++) {
						rowsData[i]["_isAutoFocus_"] = false;//不自动聚焦
						if(oldRows && oldRows.length > i) {
							dg.edatagrid("updateRow",{index : i,row : rowsData[i]});
						} else {
							dg.edatagrid("addRow",rowsData[i]);
						}
	  				}
      			} else {
      				for(var i=0,len=rowsData.length;i<len;i++) {
						rowsData[i]["_isAutoFocus_"] = false;//不自动聚焦
						dg.edatagrid("addRow",rowsData[i]);
	  				}
      			}
		    setTimeout(function() {
  			  jwpf.doSubExpression(tableKey);
  			  jwpf.doFormExpression(tableKey);
			 }, 0);
  				//自动加入空行或默认行
  			  if(isAddRow) dg.edatagrid("addRow");
      		}
      	},
        //下推选中数据
      	pushDownTableData : function(fromTableKey, toTableKey, duplicateKeys, isPushDownFunc, noticeFunc) {
			var noticeArr = [];
			var dpKeys = duplicateKeys || "";
			var dpKeyArr = dpKeys.split(",");
			var fromDg = $('#'+fromTableKey);
			var toDg = $('#'+toTableKey);
			var rowsData = jwpf.getTableVal(fromTableKey, true);
			var toData = jwpf.getTableVal(toTableKey, false);
      		if(rowsData && rowsData.length) {
  				for(var i=0,len=rowsData.length;i<len;i++) {
  					var row = rowsData[i];
  					var flag = true;
					if(isPushDownFunc && !isPushDownFunc(row, toData)) {
						noticeArr.push(row);
						flag = false;
					}
					if(flag) {
						if(!jwpf.checkRowInRows(row, toData, dpKeyArr)) {//检查重复项
							row["_isAutoFocus_"] = false;//不自动聚焦
		  					toDg.edatagrid("addRow",row);
						}
					}
  				}
      		}//不需要下推的数据进行提醒
      		if(noticeFunc) {
      			noticeFunc(noticeArr);
      		}
      		fromDg.edatagrid("clearSelections");
      	}, //根据重复检测属性值，检测row是否在row中
      	checkRowInRows:function(row, rows, duplicateKeyArr) {
      		if(rows && rows.length 
      				&& duplicateKeyArr && duplicateKeyArr.length) {
      			for(var i=0,len=rows.length; i<len; i++) {
      				var r = rows[i];
      				var flag = true;
      				for(var j=0,jLen=duplicateKeyArr.length;j<jLen;j++) {
      					var key = duplicateKeyArr[j];
      					flag = flag && (row[key] == r[key]); 
      				}
      				if(flag) {
      					return true;
      				}
      			}
      		}
      		return false;
      	},
		//设置树形子表数据
      	setTreeTableData : function(tableKey, rowsData, isAppend, isAutoAddEmptyRow) {
      		var isAddRow = !(isAutoAddEmptyRow === false);//默认自动加行 
      		if(rowsData) {
      			var dg = $('#'+tableKey);
      			if(!isAppend) { //覆盖数据
      				dg.edatagrid("removeAllNodes");//删除数据
      			}
      			//追加记录
  				for(var i=0,len=rowsData.length;i<len;i++) {
					rowsData[i]["_isAutoFocus_"] = false;//不自动聚焦
  					dg.edatagrid("addRow",rowsData[i]);
  				}
				setTimeout(function() {
  			      jwpf.doSubExpression(tableKey);
  			      jwpf.doFormExpression(tableKey);
				},0);
  			    //自动加入空行或默认行
  			    if(isAddRow) dg.edatagrid("addRow");
      		}
      	},
        //打开Datagrid数据列表，对于数据进行处理
    	doOpenDataGridWin : function(winParam){
    		var defaultParam = {"title":"数据列表",
    							"width":600,
    							"height":400,
    							"sqlKey":"",
    							"filterParam":{},
    							"onAfterSure":null};
      		var param = $.extend(defaultParam, winParam);
      		var filterParam = JSON.stringify(param.filterParam);
      		var url = "/gs/gs-mng!dataFilterWin.action?sqlKey="+param.sqlKey+
      													"&filterParam="+encodeURIComponent(filterParam);
      		param["url"] = url;
      		openFormWin(param);
      		/*var callFunc = function() {
      			var flag = false;
	      		if(param.onAfterSure) flag = param.onAfterSure();
	      		if(flag) {
	      			doCloseModuleOperDialog();
	      		}
      		};
      		doOpenModuleOperDialog(url, param.title, param.width, param.height, callFunc);*/
      	},
		//更新主表combo类控件数据
		updateFormComboFieldOption : function(fieldKey, data, paramObj, subCall, initFunc) {
    		if(initFunc) {
				initFunc(data, fieldKey, paramObj, subCall);
			} else {
				selectinit(data, fieldKey, paramObj, subCall);
			}
    	},//执行sql语句
    	execSql : function(sqlKey, filterParam){				 
  		 	var  param = {
  		 		"entityName" :	sqlKey
  		 	};
  		 	$.extend(param,filterParam);
  		 	var result = 0;
  			var opt = {
  					data:param,
  					async:false,
  					url:__ctx+"/gs/gs-mng!execSql.action",
  					success:function(data){
  						result=data.msg;
  					}
  			};
  		  	fnFormAjaxWithJson(opt, true);
  			return result;
    	},//异步执行sql语句
    	execSqlAsync : function(sqlKey, filterParam, callFunc){				 
  		 	var  param = {
  		 		"entityName" :	sqlKey
  		 	};
  		 	$.extend(param,filterParam);
  		 	var result = 0;
  			var opt = {
  					data:param,
  					url:__ctx+"/gs/gs-mng!execSql.action",
  					success:function(data){
  						if(callFunc) {
  							callFunc(data.msg);
  						}
  					}
  			};
  		  	fnFormAjaxWithJson(opt, true);
    	},//批量执行sql，params格式为[{"sqlKey":{param}}, {"sqlKey2":{param2}}]
    	execBatchSql : function(params){				 
  		 	var result = 0;
  			var opt = {
  					data:{"jsonSenddata":JSON.stringify(params)},
  					async:false,
  					url:__ctx+"/gs/gs-mng!execBatchSql.action",
  					success:function(data){
  						result=data.msg;
  					}
  			};
  		  	fnFormAjaxWithJson(opt, true);
  			return result;
    	},//显示表格行
    	showTableRow : function(rowIndex, tableIndex) {
    		if($.isNumeric(rowIndex)) {
    			var tableObj = null;
    			if($.isNumeric(tableIndex)) {
    				tableObj = $("table.module_form").eq(tableIndex);
    			} else {
    				tableObj = $("table.module_form").first();
    			}
    			tableObj.find("tr").eq(rowIndex).show();
    		} else {
    			alert("rowIndex必须为数字");
    		}
    	},//隐藏表格行
    	hideTableRow : function(rowIndex, tableIndex) {
    		if($.isNumeric(rowIndex)) {
    			var tableObj = null;
    			if($.isNumeric(tableIndex)) {
    				tableObj = $("table.module_form").eq(tableIndex);
    			} else {
    				tableObj = $("table.module_form").first();
    			}
    			tableObj.find("tr").eq(rowIndex).hide();
    		} else {
    			alert("rowIndex必须为数字");
    		}
    	},//根据json对象获取主表字段映射值
    	getMappedVal:function(fieldKey, jsonMap) {
    		var fieldVal = jwpf.getFormVal(fieldKey);
    		if($.isNumeric(fieldVal)) {
    			return jsonMap[fieldVal.toString()];
    		} else {
    			if(fieldVal) {
    				return jsonMap[fieldVal];
    			} else {
    				return "";
    			}
    		}
    	},//当前日期时间
    	now:function(dp) {
    		var p = dp || "yyyy-MM-dd";
    		return new XDate().toString(p);
    	},
    	showTab : function(tabIndex,title){
        	var tb = $('#form_tabs_'+tabIndex).tabs('getTab',title);
    		tb.panel("options").tab.show();
    		$('#form_tabs_'+tabIndex).tabs('select',title);
    	},
    	hideTab : function(tabIndex,title){
    		var tb = $('#form_tabs_'+tabIndex).tabs('getTab',title);
    		tb.panel("options").tab.hide();
    	},
    	dataHandling : function(newVal,precision){
	    	if(isNaN(newVal)){
				newVal=0;
			}else{
				if(parseInt(newVal)!=newVal){
					if(precision){
						precision = parseInt(precision);
						//newVal = newVal.toFixed(precision);
						newVal = accounting.toFixed(newVal, precision);
					}else{
						//newVal = newVal.toFixed(2);
						newVal = accounting.toFixed(newVal, 2);
					}
				}
			}
	    	return newVal;
    	},//设置子表字段只读
    	setTableFieldReadonly:function(tableKey, fieldKey, isReadonly){
			var object = $("#" + tableKey);
			if(object.length) {
				var fieldOpt = object.edatagrid("getColumnOption", fieldKey);
				if(fieldOpt && fieldOpt.editor) {
					var editor = fieldOpt.editor;
					var readOnlyAttr = "readonly";
					switch(editor.type) {
						case "newcheckbox":
						case "newradiobox":
							readOnlyAttr = "disabled";
							break;
						default:
							readOnlyAttr = "readonly";
							break;
					}
					if(editor.options) {
						if(isReadonly) {
							editor.options[readOnlyAttr] = true;
						} else {
							delete editor.options[readOnlyAttr];
						}
					}
				}
			}
		},//打开URL链接地址
		doOpenUrl:function(title, url) {
			if(top.addTab) {
				top.addTab(title, url);
			} else {
				window.open(url, title);
			}
		},//执行动态函数体
		execFunction:function(fBody, param) {
			var callFunc = new Function("param",fBody);
			return callFunc(param);
		},//保存变更日志
		doSaveChangeLog:function(param) {
			var dp = {
				"moduleNameCn" : "",//模块中文名
				"moduleNameEn" : "",//模块英文名
				"entityName" : "",//实体名
				"entityId" : "",//实体ID
				"subEntityId":"",//子实体ID
				"tableNameCn":"",//表中文名
				"tableNameEn":"",//表英文名
				"fieldNameCn" : "",//变更字段
				"fieldNameEn" : "",//变更字段英文
				"beforeVal" : "",//变更前值
				"afterVal" : "",//变更后值
				"changeDesc" : "",//变更详情
				"onAfterSave":null //保存日志后调用方法
			};
			$.extend(dp, param);
			var opt = {
				data : dp,
				async : false,
				url : __ctx + "/sys/log/change-log!save.action",
				success : function(data) {
					if(data.msg && dp.onAfterSave) {
						dp.onAfterSave(data.msg);
					}
				}
			};
			fnFormAjaxWithJson(opt, true);
		},//获取本地接口返回数据 param-url,param-callFunc
		doGetLocalLinkResult:function(param, callFunc) {
			$(document.body).mask("Please Waiting...正在努力读取数据中");
			var dp = $.extend(true, {}, param);
			var url = dp.url || "http://127.0.0.1:8310";
			var opt = {
					type:"post",
					data : dp,
					dataType:'jsonp',
					async : false,//jsonp模式下，同步异步无效
					url : url,
					success : function(data) {
						$(document.body).unmask();
						if(data.flag == "1") { //成功返回结果
							if(callFunc) callFunc(data.msg);
						} else {
							alert("获取数据出错了："+data.msg);
						}
					},
					error:function(XMLHttpRequest, textStatus, errorThrown) {
						$(document.body).unmask();
						alert("出错了！" + errorThrown);
					}
			};
			$.ajax(opt);
		},//初始化
		doInitDatagrid:function(dgId, jsonData) {
			var data = jsonData["list"];	
			var mapping = jsonData["map"];
		  	var fields = [];
		   $.each(mapping,function(i,n){
			   var field = {width:n.width||100};
			   field["field"] = n.key;
			   field["title"] = n.caption;
			   if(n.formatter) {
				   field["formatter"] = n.formatter;
			   }
			   if(n.order == "hidden" || n.hidden) {
				   field["hidden"] = true;
			   }
			   if(n.align) {
				   field["align"] = n.align;
			   }
			   fields.push(field);
		   });	   
		   var fColumns=[];
		   var field={};
		   field["field"]="ck";
		   field["checkbox"]=true;
		   fColumns.push(field);		   
		   var dataList={
	    		   "total":data.length,                                                      
				   "rows":data
	       };
		   var dgList = $("#"+dgId);
		   dgList.datagrid({  
			    nowrap: false,
				striped: true,	
				rownumbers:true,
				//idField:'key',  
				frozenColumns:[fColumns],
				columns:[fields]					 							
		   });  
		   dgList.datagrid("loadData",dataList);
		},//打开datagrid窗口，展示列表数据
		doOpenDataGridWinNew:function(dgParam) {
			var defaultParam = {
			"title":"结果列表",
			"width":800,
			"height":600,
			"isMaxWin":false,
			"data":{"list":[],"map":[]},
			"onAfterSure":null};
			var param = $.extend(defaultParam, dgParam);
			var win = '<div style="text-align: center;" class="easyui-layout" fit="true"><div region="center" border="false"><table id="__resultList__"></table></div></div>';
			var winTitle = param.title;
			var winWid = param.width;
			var winHei = param.height;
			var winMax = param.isMaxWin;
			var callFunc = param.onAfterSure;
			var p = $("<div style='overflow: auto;'/>").appendTo("body");
			p.dialog({
				id:'_datagrid_win_',
				title:winTitle,
				content: win,
				width:winWid,
				height:winHei,
				closable:true,
				collapsible:true,
				maximizable:true,
				minimizable:false,
				maximized:winMax,
				resizable:true,
				modal: true,
				shadow: false,
				cache:false,
				closed:true,
				onOpen:function() {
					jwpf.doInitDatagrid("__resultList__",param.data);
				},
				onClose:function() {
					$("#_datagrid_win_").dialog("destroy");
				},
				buttons:[{
					text:'确定',
					iconCls:'icon-ok',
					handler:function(){
						if(callFunc) {
							var rows = $("#__resultList__").datagrid('getChecked');
							var result = callFunc(rows);
							if(result) $("#_datagrid_win_").dialog("close");
						}
					}
				},{
					text:'取消',
					iconCls:'icon-cancel',
					handler:function(){
						$("#_datagrid_win_").dialog("close");
					}
				}]
			});
			$("#_datagrid_win_").dialog("open");
		},//触发下移事件
		triggerDatagridDownEvent:function(obj) {
			var e = jQuery.Event("keydown");
			e.keyCode = 40;
			$(obj).trigger(e);
			return false;
		},//保存子表数据，在主表数据存在情况下
		doSaveSubObj: function(tableKey, entityName, type, callFunc, saveIds, masterId) {
		 		var jsons = {"id":masterId || parseInt(jwpf.getId())};
		 		var rows = false;
		 		if(type == "TreeGrid") {
		 			rows = cSaveObjTree(tableKey);
		 		} else {
		 			rows = cSaveObj(tableKey);
		 		}
		        if(rows === false) {return;}
		        if(saveIds && saveIds.length) {
		        	var cr = rows["updated"];
		        	var upRows = [];
		        	if(cr && cr.length) {
		        		for(var i=0,len=cr.length;i<len;i++) {
		        			var obj = cr[i];
		        			if($.inArray(obj["id"], saveIds)>=0) {
		        				upRows.push(obj);
		        			}
		        		}
		        	}
		        	var newRows = {"updated":upRows};
		        	jsons['data'] = newRows;
		        	jsons["saveType"] = "saveModify";//只保存修改数据的模式
		        } else {
		        	jsons['data'] = rows;//全部数据保存
		        }
				var jsonSavedata = JSON.stringify(jsons);
				var options = {
					url : __ctx + '/gs/gs-mng!saveEntity.action?entityName=' + entityName,
					async : false,
					data : {
						"jsonSaveData" : jsonSavedata
					},
					success : function(data) {
						$("#" + tableKey).edatagrid("load");//刷新子表
						if(callFunc) {
							callFunc();
						}
					}
				};
				fnFormAjaxWithJson(options);
		},//开启指定条件行的可编辑性
		enableEditRow:function(tableKey, attrKey, attrVal, isAutoFocus, focusField) {
			var dg = $("#"+tableKey);
			var param = {
				"attrKey":attrKey,
				"attrVal":attrVal,
				"isAutoFocus":isAutoFocus,
				"field":focusField
			};
			dg.edatagrid("enableEditRowByAttr",param);
		},//随机数，minVal为下限，maxVal为上限，scale为需要保留的小数位
		getRandom:function(minVal,maxVal,scale){
		    var t1 = minVal,t2 = maxVal, t3 = scale;
		    if(!$.isNumeric(t1)) {t1=0;}
		    if(!$.isNumeric(t2)) {t2=1;}
		    if(!$.isNumeric(t3)) {t3=0;}
		    t3 = t3>15?15:t3; // 小数位不能大于15位
		    var ra = Math.random() * (t2-t1)+t1,du=Math.pow(10,t3);
		    ra = Math.round(ra * du)/du;
		    return ra;
		},//启用超链接
		enableLinkButton: function(jqObj) {
			var jqElements = jqObj || [];
            if (jqElements.length > 0) {
                jqElements.each(function(){
                    if ($(this).hasClass('link-disabled')) {
                    	var backStore = $(this).data("$backup$");
                    	var itemData = backStore || {};
                        if(itemData.target == this){
                            //恢复超链接
                            if (itemData.href) {
                                $(this).attr("href", itemData.href);
                            }
                            //回复点击事件
                            if (itemData.onclicks) {
                                for (var j = 0; j < itemData.onclicks.length; j++) {
                                    $(this).bind('click', itemData.onclicks[j]);
                                }
                            }
                            //设置target为null，清空存储的事件处理程序
                            itemData.target = null;
                            itemData.onclicks = [];
                            $(this).removeClass('link-disabled');
                        }
                    }
                });
            }
		},//禁用超链接
		disableLinkButton: function(jqObj) {
			var jqElements = jqObj || [];
            if (jqElements.length > 0) {
                jqElements.each(function(){
                    if (!$(this).hasClass('link-disabled')) {
                        var backStore = {};
                        backStore.target = this;
                        backStore.onclicks = [];
                        //处理超链接
                        var strHref = $(this).attr("href");
                        if (strHref) {
                            backStore.href = strHref;
                            $(this).attr("href", "javascript:void(0)");
                        }
                        //处理直接耦合绑定到onclick属性上的事件
                        var onclickStr = $(this).attr("onclick");
                        if (onclickStr && onclickStr != "") {
                            backStore.onclicks[backStore.onclicks.length] = new Function(onclickStr);
                            $(this).attr("onclick", "");
                        }
                        //处理使用jquery绑定的事件
                        var eventDatas = $(this).data("events") || $._data(this, 'events');
                        if (eventDatas && eventDatas["click"]) {
                            var eventData = eventDatas["click"];
                            for (var i = 0; i < eventData.length; i++) {
                                backStore.onclicks[backStore.onclicks.length] = eventData[i]["handler"];
                                $(this).unbind('click', eventData[i]["handler"]);
                                i--;
                            }
                        }
                        $(this).data("$backup$", backStore);
                        $(this).addClass('link-disabled');
                    }
                });
            }
		},//数字转大写
		toDigitUppercase : function(n) {
		    var fraction = ['角', '分'];
		    var digit = [
		        '零', '壹', '贰', '叁', '肆',
		        '伍', '陆', '柒', '捌', '玖'
		    ];
		    var unit = [
		        ['元', '万', '亿'],
		        ['', '拾', '佰', '仟']
		    ];
		    var head = n < 0? '欠': '';
		    n = Math.abs(n);
		    var s = '';
		    for (var i = 0; i < fraction.length; i++) {
		        s += (digit[Math.floor(n * 10 * Math.pow(10, i)) % 10] + fraction[i]).replace(/零./, '');
		    }
		    s = s || '整';
		    n = Math.floor(n);
		    for (var i = 0; i < unit[0].length && n > 0; i++) {
		        var p = '';
		        for (var j = 0; j < unit[1].length && n > 0; j++) {
		            p = digit[n % 10] + unit[1][j] + p;
		            n = Math.floor(n / 10);
		        }
		        s = p.replace(/(零.)*零$/, '')
		             .replace(/^$/, '零')
		          + unit[0][i] + s;
		    }
		    return head + s.replace(/(零.)*零元/, '元')
		                   .replace(/(零.)+/g, '零')
		                   .replace(/^整$/, '零元整');
		},//审核通过或取消审核
		doStartOrCancelApprove:function(entityName, id, approveFlag,callFunc){
			var ids = [];
			if($.isArray(id)) {
				ids = id;
			} else {
				ids.push(id);
			}
		   	var options = {
		             url : __ctx+'/gs/process!'+approveFlag+'.action?entityName='+entityName,
		             async : false,
                     traditional:true,
		             data : {
		            	 "ids" : ids
		             },
		             success : function(data) {
		            	 if(data.msg) {
		            		 if(callFunc) { 
		            			 callFunc();
		            	     }
						 }
		             }
		        };
		        fnFormAjaxWithJson(options, true);
		 },//读通知
		 doReadNotice:function(noticeId, isReadAll, callFunc) {
			 var options = {
						url : __ctx + '/sys/notice/notice!doReadNotice.action',
						async : false,
						data : {
							"id" : noticeId,
							"isReadAll": isReadAll || false
						},
						success : function(data) {
							if(data.msg) {
			            		 if(callFunc) { 
			            			 callFunc();
			            	     }
							}
						}
			 };
			 fnFormAjaxWithJson(options, true); 
		 },//刷新portal页面panel方法
		 doRefreshPortalPanel:function(title) {
			 var ifr = window.top.$("#ifr_portal");
			 if(ifr.length) {
				 var win = ifr[0].contentWindow.window;
				 win.$("div.panel-title:contains('"+title+"')").next().find("a.icon-reload").trigger("click");
			 }
		 },//数字转科学计数法
		 toExponential:function(num, bitNum) {
			 var n = parseFloat(num);
			 var e = n.toExponential(bitNum);
			 return e.toString().toUpperCase();
		 },//返回截取字符串
		 getFormatSpanText:function(content, len, suffix) {
			 if(content) {
				 if(content.length > len) {
					 var str = content.substring(0, len) + (suffix || "...");
					 return "<span title=\"" + content + "\">" + fnFormatText(str) + "</span>";
				 } else {
					 return fnFormatText(content);
				 }
			 } else {
				 return "";
			 }
		 },//查看修订日志
		 doViewReviseLog:function(id, mn, en, fn, sid) {
			 var strUrl = "/sys/log/revise-log.action?mn=";
			 strUrl += mn+"&id=" + id +"&en=" +(en?en:"")+"&fn="+(fn?fn:"")+"&sid="+(sid?sid:"");
			 openFormWin({
				 "title":"修订日志",
				 "width":800,
				 "height":400,
				 "url":strUrl,
				 "showTools":false
			 });
		 },	//将主表控件设置成可修订状态
		 doSetFormToRevise:function(entityName, moduleEn, moduleCn, jqSelector){
			    var jqs = jqSelector || ".jwpf-revise";
		    	$(jqs).each(function(i) {
		    		var cid = $(this).attr("id");
		    		var isTextField = $(this).hasClass("easyui-dialoguewindow") || false;//是否包含text字段
		    		var btn = $('<a href="javascript:void(0);" class="easyui-linkbutton bt-extra validInView" plain="true" iconCls="icon-edit" title="修订" forId="'+ cid +'"></a>');
		    		var obja = $("a[forId="+cid+"]");
		    		if(obja.length) obja.remove();
		    		var parentObj = $(this).parent();
		    		if(parentObj.hasClass("input-group")) {parentObj = parentObj.parent();}
		    		btn.appendTo(parentObj);
		    		btn.linkbutton({});
		    		btn.toggle(function() { //启用修订
		    			var bid = $(this).attr("forId");
		    			//$("#"+bid).removeAttr("disabled");
		    			jwpf.setFormFieldReadonly(bid, false);
		    			var oldVal = jwpf.getFormVal(bid);
		    			var oldCap = "";
		    			if($.isPlainObject(oldVal)) {
		    				oldCap = oldVal["text"];
		    				oldVal = oldVal["id"];
		    			} else {
		    				oldCap = jwpf.getFormComboFieldCaption(bid);
		    			}
		    			$(this).data("oldVal",oldVal);
		    			$(this).data("oldCap",oldCap);
		    			$(this).attr("title","保存");
		    			$(this).linkbutton({iconCls:"icon-save"});
		    		},function() { //保存修订
		    			var _thibtn_ = $(this);
		    			var bid = _thibtn_.attr("forId");
	    			    var oldVal = _thibtn_.data("oldVal");
	    			    var oldCap = _thibtn_.data("oldCap");
	    				var newVal = jwpf.getFormVal(bid);
		    			var newCap = "";
		    			if($.isPlainObject(newVal)) {
		    				newCap = newVal["text"];
		    				newVal = newVal["id"];
		    			} else {
		    				newCap = jwpf.getFormComboFieldCaption(bid);
		    			}
		    			var dataType = $("#"+bid).attr("jwDataType");
		    			var fnc = $("#"+bid).attr("jwTitle");
		    			var field = "EQ"+dataType+"_" + bid;
		    			var jwEn = $("#"+bid).attr("jwEn");
		    			var sid = jwpf.getId();
		    			if(jwEn) {
		    				entityName = jwEn;
		    				var tableKey = jwEn.split(".")[1];
		    				sid = jwpf.getFormVal(tableKey+"_id");
		    			}
		    			var beforeFunc = window["onBeforeReviseFor_form_"+bid];
						if(beforeFunc && beforeFunc(newVal, oldVal)=== false) {//修订前事件
							return false;
						}
		    			jwpf.updateField(entityName, field, sid, newVal, function() {
		    				if(isTextField) {
		    					jwpf.updateField(entityName, "EQS_" + bid + "text", sid, newCap);//更新text字段
		    				}
		    				var bv = oldVal;
		    				var av = newVal;
		    				if(oldCap) bv = oldVal + "-" + oldCap;
			    			if(newCap) av = newVal + "-" + newCap;
			    			//var fnc = $("label[for="+bid+"]").text();
			    			var tnc = _thibtn_.closest("div.easyui-tabs").find("div.tabs-header li.tabs-selected span.tabs-title").text();
		    				var opt = {
									"moduleNameCn" : moduleCn,
									"moduleNameEn":moduleEn, 
									"entityName" : entityName, 
									"entityId" : jwpf.getId(),
									"tableNameCn": tnc,
									"fieldNameCn" : fnc,
									"fieldNameEn" : bid,
									"beforeVal" : bv,
									"afterVal" : av,
									"changeDesc" : "修订操作"
							};
							jwpf.doSaveChangeLog(opt);
							
							jwpf.setFormFieldReadonly(bid, true);
							//$("#"+bid).attr("disabled","disabled");
							_thibtn_.removeData("oldVal");
							_thibtn_.removeData("oldCapt");
							_thibtn_.attr("title","修订");
							_thibtn_.linkbutton({iconCls:"icon-edit"});
							
							var eventKey = "onAfterReviseFor_form_"+bid;
							var func = window[eventKey];
							if(func) {
								func(newVal, oldVal, newCap, oldCap);
							}
		    			});
		    		});
		    	});
		    },//子表字段启用修订
		    doSetTableToRevise:function(tableKey, entityName, moduleEn, moduleCn, reviseFields) {
		    	$('div.datagrid-body td[field]', $("#"+tableKey).parent()).each(function() {
		    		var fn = $(this).attr("field");
		    		var fdOpt = $("#"+tableKey).edatagrid("getColumnOption",fn);
		    		if((!reviseFields && fdOpt.jwRevise) || (reviseFields && $.inArray(fn,reviseFields)>=0)) {
						if(fdOpt.editor && fdOpt.editor.options) {
							var eOpt = fdOpt.editor.options;
							if(eOpt.readonly) {
								eOpt.readonly = false;
							}
							if(eOpt.disabled) {
								eOpt.disabled = false;
							}
						}
		    			var tnc = $(this).closest("div.easyui-tabs").find("div.tabs-header li.tabs-selected span.tabs-title").text();
		    			var that = $(this);
		    			$(this).qtip({
						    content: '<a href="javascript:void(0)" class="validInView l-btn l-btn-plain" plain="true" title="修订" ><span class="l-btn-left l-btn-icon-left"><span class="l-btn-text">修订</span><span class="l-btn-icon icon-edit">&nbsp;</span></span></a>',
						    position: {
						    	my: 'bottom left',
						        at:"top center"
						    },
						    hide: {
						        delay: 1000
						    },
						    events: {
						      	show: function(event, api) {
									var btn = api.elements.content;
									var target = api.elements.target;
									//var target = that;
									var rowIndex = target.closest("tr.datagrid-row").attr("datagrid-row-index");
									var spBtn = $("span>span.l-btn-text", btn);
									if(target.hasClass("datagrid-cell-editing")) {//可编辑，保存操作
									    spBtn.text("保存");
									    //btn.attr("保存");
									    spBtn.next().removeClass("icon-edit").addClass("icon-save");
										btn.unbind("click").click(function() {
											$("#"+tableKey).edatagrid("endCellEdit",{"index":rowIndex,"field":fn});
											var oldCap = btn.data("oldCap");
											var oldVal = btn.data("oldVal");
											//var newCap = target.text();
											var newCap = $("#"+tableKey).parent().find("tr[datagrid-row-index="+rowIndex+"] td[field=" + fn + "]").text();
											var rowData = jwpf.getTableVal(tableKey,rowIndex);
											var newVal = rowData[fn];
											var beforeFunc = window["onBeforeReviseFor_"+tableKey+"_"+fn];
											if(beforeFunc && beforeFunc(newVal, oldVal, rowIndex, tableKey, rowData)=== false) {//修订前事件
												return false;
											}
											var sid = rowData["id"];
											var dataType = fdOpt.jwDataType;
							    			var field = "EQ"+dataType+"_" + fn;
							    			var isTextField = (fdOpt.editor.type=="dialoguewindow") || false;//是否包含text字段
							    			jwpf.updateField(entityName, field, sid, newVal, function() {
							    				if(isTextField){
							    					jwpf.updateField(entityName, "EQS_" + fn + "text", sid, newCap);//更新text字段
							    				}
							    				var bv = oldVal;
							    				var av = newVal;
							    				if(oldCap != oldVal) bv = oldVal + "-" + oldCap;
								    			if(newCap != newVal) av = newVal + "-" + newCap;
								    			var fnc = fdOpt.title;
								    			tnc = tnc + "第" + (parseInt(rowIndex)+1) + "行记录";
												var opt = {
														"moduleNameCn" : moduleCn,
														"moduleNameEn":moduleEn,
														"entityName" : entityName,
														"entityId" : jwpf.getId(),
														"subEntityId": sid,
														"tableNameCn": tnc,
														"fieldNameCn" : fnc,
														"fieldNameEn" : fn,
														"beforeVal" : bv,
														"afterVal" : av,
														"changeDesc" : "修订操作"
												};
												jwpf.doSaveChangeLog(opt);
												
												var eventKey = "onAfterReviseFor_"+tableKey+"_"+fn;
												var func = window[eventKey];
												if(func) {
													func(newVal, oldVal, newCap, oldCap, rowIndex, tableKey, rowData);
												}
												
												jwpf.doSetTableToRevise(tableKey, entityName, moduleEn, moduleCn, reviseFields);
							    			});
										});
									} else{ //修订操作
									    spBtn.text("修订");
									    //btn.attr("修订");
									    spBtn.next().removeClass("icon-save").addClass("icon-edit");
										btn.unbind("click").click(function() {
											var oldCap = target.text();
											btn.data("oldCap", oldCap);
											var oldVal = jwpf.getTableVal(tableKey,rowIndex,fn);
											btn.data("oldVal", oldVal);
											$("#"+tableKey).edatagrid("beginCellEdit",{"index":rowIndex,"field":fn});
										});
									}
						        }
						    }
						});	
		    		}
		    	});
		    },//退出修订模式
		    doCancelSetFormToRevise:function(jqSelector) {
		    	var jqs = jqSelector || ".jwpf-revise";
		    	$(jqs).each(function(i) {
		    		var cid = $(this).attr("id");
		    		var obja = $("a[forId="+cid+"]");
		    		if(obja.length) obja.remove();
		    	});
		    },//初始化修订查看
		    doInitReviseView:function(entityName, moduleEn, jqSelector) {
		    	var jqs = jqSelector || ".jwpf-revise";
		    	$(jqs).each(function(i) {
		    		var cid = $(this).attr("id");
		    		var btn = $('<a href="javascript:void(0);" class="easyui-linkbutton bt-extra validInView" plain="true" iconCls="icon-new-doc-ico" title="修订日志" forViewId="'+ cid +'"></a>');
		    		var obja = $("a[forViewId="+cid+"]");
		    		if(obja.length) obja.remove();
		    		var parentObj = $(this).parent();
		    		if(parentObj.hasClass("input-group")) {parentObj = parentObj.parent();}
		    		btn.appendTo(parentObj);
		    		//btn.appendTo($(this).parent());
		    		btn.linkbutton({});
		    		btn.click(function() {
		    			var bid = $(this).attr("forViewId");
		    			var en = $("#"+bid).attr("jwEn") || entityName;
		    			jwpf.doViewReviseLog(jwpf.getId(),moduleEn,en, bid);
		    		}); 
		    	});
		    },//取消修订查看按钮
		    doCancelReviseView:function(jqSelector) {
		    	var jqs = jqSelector || ".jwpf-revise";
		    	$(jqs).each(function(i) {
		    		var cid = $(this).attr("id");
		    		var obja = $("a[forViewId="+cid+"]");
		    		if(obja.length) obja.remove();
		    	});
		    },//批量启动流程
		    doBatchStartProcess:function(ids, entityName, userIds, callFunc, isForSingleUser) {
		    	var param = isForSingleUser?"ForSingleUser":"";
		    	var options = {
						url : __ctx+'/gs/process!batchStartProcess'+param+'.action',
						traditional:true,
						data : {
							"entityName": entityName,
							"ids" : ids,
							"userIds": (userIds||"")
						},
						async : false,
						success : function(data) {
							if(data.msg) {
								var attUrl = "/sys/workflow/agree-process.action?roles="+encodeURIComponent(data.msg) 
										+ "&taskIds=" + ids.join(",");
								top.openFormWin({
									"title":"选择下个环节用户",
									 "width":500,
									 "height":450,
									 "url":attUrl,
									 "onAfterSure":function(win) {
										 if(win.getIds) {
											 var uids = win.getIds();
											 if(uids) {
												 jwpf.doBatchStartProcess(ids, entityName, uids, callFunc);
												 return true; 
											 }
										 }
									 }
								});
							} else {
								$.messager.alert("提示信息", "批量操作成功！",'info');
								if(callFunc) { setTimeout(callFunc, 2000);}
							}
						}
			    	};
			    	fnFormAjaxWithJson(options, true);
		    },//批量同意
		    doBatchAgreeProcess:function(taskIds, userIds, callFunc, isForSingleUser) {
		    	var param = isForSingleUser?"ForSingleUser":"";
		    	var options = {
					url : __ctx+'/gs/process!batchAgree'+param+'.action',
					traditional:true,
					data : {
						"taskIds" : taskIds,
						"userIds": (userIds||"")
					},
					async : false,
					success : function(data) {
						if(data.msg) {
							var attUrl = "/sys/workflow/agree-process.action?roles="+encodeURIComponent(data.msg) 
									+ "&taskIds=" + taskIds.join(",");
							top.openFormWin({
								"title":"选择下个环节用户",
								 "width":500,
								 "height":450,
								 "url":attUrl,
								 "onAfterSure":function(win) {
									 if(win.getIds) {
										 var uids = win.getIds();
										 if(uids) {
											 jwpf.doBatchAgreeProcess(taskIds, uids, callFunc);
											 return true;
										 }
									 }
								 }
							});
						} else {
							$.messager.alert("提示信息", "批量操作成功！",'info');
							if(callFunc) { setTimeout(callFunc, 2000);}
						}
					}
		    	};
		    	fnFormAjaxWithJson(options, true);
		    },//批量不同意
		    doBatchDisAgreeProcess:function(taskIds, userIds, callFunc, isForSingleUser) {
		    	var param = isForSingleUser?"ForSingleUser":"";
		    	var options = {
					url : __ctx+'/gs/process!batchDisAgree'+param+'.action',
					traditional:true,
					data : {
						"taskIds" : taskIds,
						"userIds": (userIds||"")
					},
					async : false,
					success : function(data) {
						if(data.msg) {
							var attUrl = "/sys/workflow/agree-process.action?roles="+encodeURIComponent(data.msg) 
									+ "&taskIds=" + taskIds.join(",");
							top.openFormWin({
								"title":"选择下个环节用户",
								 "width":500,
								 "height":450,
								 "url":attUrl,
								 "onAfterSure":function(win) {
									 if(win.getIds) {
										 var uids = win.getIds();
										 if(uids) {
											 jwpf.doBatchAgreeProcess(taskIds, uids, callFunc);
											 return true;
										 }
									 }
								 }
							});
						} else {
							$.messager.alert("提示信息", "批量操作成功！",'info');
							if(callFunc) { setTimeout(callFunc, 2000);}
						}
					}
		    	};
		    	fnFormAjaxWithJson(options, true);
		    },//检查是否内网地址，true-是，false-不是，null-获取出错
	    	checkInnerIp : function() {
	  		 	var result = null;
	  			var opt = {
	  					async:false,
	  					url:__ctx+"/sys/account/online-user!checkInnerIp.action",
	  					success:function(data){
	  						result=data.msg;
	  					}
	  			};
	  		  	fnFormAjaxWithJson(opt, true);
	  			return result;
	    	},//存储过程返回值
	    	callProcWithResult : function(key, filterParam, callFunc){
	    		var result = null;
	    		var isSP = false;
	    		var  param = {
	  		 			"procKey" :	key
	  		 	};
	  		 	$.extend(param,filterParam);
	    	   	var options = {
	    	   			data:param,
	  					async:false,
	    	            url : __ctx+'/gs/gs-mng!callProcedureWithResult.action',
	    	            success : function(data) {
	    	            	 result = data.msg;
	    	            	 if(callFunc) {
								callFunc(data.msg);
							 }
	    	             }
	    	    };
	    	    fnFormAjaxWithJson(options,!isSP);
	    	    return result;
	    	},//设置数据共享用户
	    	doSetDataShareUser:function(entityName, ids, onAfterSetSuccess) {
	    		if(ids && ids.length) {
	    			var attUrl = "/sys/authority/data-share-config.action?id="+ids[0]
							   + "&entityName=" + entityName;
					openFormWin({
						 "title":"设置数据共享用户",
						 "width":500,
						 "height":500,
						 "url":attUrl,
						 "onAfterSure":function(win) {
							 var dataObj = {
								 "entityName":entityName,
								 "entityIds":ids
							 };
							 var flag = win.saveDsc(dataObj);
							 if(flag) {
								 if(onAfterSetSuccess) onAfterSetSuccess();
								 if(window.doQuery) doQuery();
								 return true;
							 }
						 }
					});
	    		} else {
	    			alert("请选择要操作的数据！");
	    		}
	    	},//在查看状态下新增行数据及保存
	    	doAddRowInViewAndSave:function(tableKey, entityName, btObj, saveTitle, onAfterSave, onBeforeSave) {
	    		var oldTitle = $("span.l-btn-text", btObj).text();
	    		if(oldTitle == saveTitle) {//保存
	    			if(onBeforeSave && onBeforeSave() === false) {
	    				return false;
					}
	    			jwpf.doSaveSubObj(tableKey, entityName, null, function() {
	    				var inde = $(btObj).data("index");
	    				$("#"+tableKey).edatagrid("disableEditRow", inde);
	    				if(onAfterSave) {
	    					onAfterSave();
	    				}
	    			});
	    			$("span.l-btn-text", btObj).text($(btObj).data("title"));
	    		} else {//新增
	    			$(btObj).data("title",oldTitle);
	    			$("span.l-btn-text", btObj).text(saveTitle);
		    		var rows = jwpf.getTableVal(tableKey,false);
		    		var index = rows.length;
		    		$(btObj).data("index",index);
		    		$("#"+tableKey).edatagrid("addRow");
		    		$("#"+tableKey).edatagrid("enableEditRow",{index:index});
	    		}
	    	},//在查看状态下修改行数据及保存
	    	doModifyRowInViewAndSave:function(tableKey, entityName, btObj, saveTitle, onAfterSave, onBeforeSave) {
	    		var oldTitle = $("span.l-btn-text", btObj).text();
	    		if(oldTitle == saveTitle) {//保存
					if(onBeforeSave && onBeforeSave() === false) {
						return false;
					}
	    			jwpf.doSaveSubObj(tableKey, entityName, null, function() {
	    				var inde = $(btObj).data("index");
	    				$("#"+tableKey).edatagrid("disableEditRow", inde);
	    				if(onAfterSave) {
	    					onAfterSave();
	    				}
	    			});
	    			$("span.l-btn-text", btObj).text($(btObj).data("title"));
	    		} else {//修改
	    			var rows = jwpf.getTableVal(tableKey,true);
	    			if(rows.length != 1) {
	    				alert("请选择1条记录修改！");
	    				return;
	    			}
	    			$(btObj).data("title",oldTitle);
	    			$("span.l-btn-text", btObj).text(saveTitle);
		    		var index = $("#"+tableKey).edatagrid("getRowIndex", rows[0]);
		    		$(btObj).data("index",index);
		    		$("#"+tableKey).edatagrid("enableEditRow",{index:index});
	    		}
	    	},//平行样计算
	    	doCalcPingXingYang:function(tableKey, pxyNum, calcField, setField, calcFunc, fixNum, preFunc, filterFunc) {
	    		var rows = jwpf.getTableVal(tableKey, false);
	    		if(filterFunc) {
	    			rows = $.grep(rows, function(row,i){
	    				return filterFunc(row);
	    			});
	    		}
	    		var calcValues = [];
	    		var startIndex = 0;
	    		if(rows.length) {
	    			startIndex = $("#"+tableKey).edatagrid("getRowIndex", rows[0]);
	    		}
	    		for(var i=0,len=rows.length; i<len; i++) {
	    			var row = rows[i];
	    			calcValues.push(row);
	    			if((i+1)%pxyNum == 0 || i == (len-1)) {//平行样数或最后一个
	    				var result = 0;
	    				if(calcFunc) {
	    					try {
	    						result = calcFunc(calcValues);
	    					} catch(e) {alert("自定义计算公式出现异常，请检查计算函数和数据！");}
	    				} else {//默认平均值计算
	    					result = jwpf.getAvgVal(calcValues,calcField,null,fixNum);
	    				}
	    				var rowData = {};
	    				rowData[setField] = (preFunc ? preFunc(result) : result);
	    				for(var j=startIndex;j<=startIndex+i;j++) {
	    					jwpf.setTableVal(tableKey,j,rowData, false);
	    				}
	    				calcValues = [];
	    				startIndex = i+1;
	    			}
	    		}
	    	}, //更新表格列标题
	    	doUpdateTableFieldTitle: function(tableKey, fieldKey, title) {
	    		$("#"+tableKey).edatagrid("getPanel")
	    			.find("div.datagrid-header td[field=\"" + fieldKey + "\"] span:first")
	    			.text(title);
	    	},
	    	doDisplayUppercaseNum:function(fieldKey) {
	    		var formatter = function(val) {
	    			return jwpf.toDigitUppercase(val);
	    		};
	    		jwpf.doDisplayTipMsg($("#"+fieldKey), formatter);
	    	},
	    	doDisplayTipMsg:function(jqObj, formatter) {
	    		jqObj.each(function() {
	    			if($(this).attr("disabled")) {
	    				if($(this).hasClass("easyui-numberbox")) {
	    					$(this).numberbox("enable");
	    				} else {
	    					$(this).removeAttr("disabled");
	    				}
	    				if(!$(this).attr("readonly")) {
	    					$(this).attr("readonly", true);
	    				}
	    			}
	    			$(this).qtip({
					    content: '<span></span>',
					    position: {
					    	my: 'bottom left',
					        at:"top center"
					    },
					    hide: {
					        delay: 500
					    },
					    events: {
					      	show: function(event, api) {
								var spanContent = api.elements.content;
								var target = api.elements.target;
								var ct = "";
								var val = target.val();
				    			if(formatter) {
				    				ct = formatter(val);
				    			}
				    			spanContent.text(ct);
					      	}
					    }
					});	
	    		});
	    	},//下顺丰订单
	    	sendSfOrder : function(querySqlKey,filterParam,updateSqlKey, updateErrSqlKey) {				 
      		 	var  param = {
      		 		"querySqlKey" :	querySqlKey,
      		 		"updateSqlKey" : updateSqlKey,
      		 		"updateErrSqlKey": updateErrSqlKey
      		 	};
      		 	$.extend(param,filterParam);
      		  	var jsonData=null;
      			var opt = {
      					data:param,
      					async:false,
      					url:__ctx+"/yk/api/yk-wu-liu!input.action",
      					success:function(data){
      						jsonData=data.msg;
      					}
      			};
      		  	fnFormAjaxWithJson(opt, true);
      			return jsonData;
	    	},//日期字符串转日期对象
			dateStrToDate:function(dateStr) {
				return new Date(dateStr.replace(/-/g, "\/"));
			},//计算时间差，返回分钟数,time2-time1
			getTimeDiff:function(time1, time2) {
				return parseInt(time2.slice(0,2))*60+parseInt(time2.slice(3,5))-(parseInt(time1.slice(0,2))*60+parseInt(time1.slice(3,5)));
			},
			//计算两个日期间的工作时间（小时）
			getWorkHours:function(opt){
				var startDate = opt.startDate, endDate = opt.endDate, startTimeAm = opt.startTimeAm,
								endTimeAm = opt.endTimeAm, startTimePm = opt.startTimePm, endTimePm = opt.endTimePm,
								workHours1Day = opt.workHours1Day;
			    var datetime1 = this.dateStrToDate(startDate), datetime2 = this.dateStrToDate(endDate);

			    var date1 = this.dateStrToDate(startDate.slice(0, 10));  //开始日期，毫秒表示
			    var date2 = this.dateStrToDate(endDate.slice(0, 10));  //结束日期，毫秒表示

			    var time1 = startDate.slice(11); //开始时间
			    var time2 = endDate.slice(11); //结束时间
			    
			    var travelHours = 0;    //保存请假分钟数
			    //开始结束时间均在一天
			    if(date1.getTime() == date2.getTime()){
			        //1.开始时间8-12点
			        if(time1 >= startTimeAm && time1 <= endTimeAm){
			            //1.1 结束时间在8-12点
			            if(time2 >= startTimeAm && time2 <= endTimeAm){
			                travelHours += this.getTimeDiff(time1, time2);
			            }
			            //1.2  结束时间在13-17点
			            if(time2 >= startTimePm && time2 <= endTimePm){
			                travelHours += this.getTimeDiff(time1, endTimeAm) + this.getTimeDiff(startTimePm, time2);
			            }

			            //1.3
			            if(time2 > endTimePm){
			                alert("结束时间不能大于" + endTimePm);
			                return 0;
			            }
			        }
			        //如果开始时间小于8点
			        if(time1 < startTimeAm){
			            alert("开始时间不能小于" + startTimeAm);
			            return 0;
			        }
			        //如果开始时间大于17点，
			        if(time1 > endTimePm){
			        	alert("开始时间不能大于" + endTimePm);
			            return 0;
			        }
			        //2.开始时间在13-17点
			        if(time1 >= startTimePm && time1 <= endTimePm){
			            //1.2  结束时间在13-17点
			            if(time2 >= startTimePm && time2 <= endTimePm){
			                travelHours += this.getTimeDiff(time1, time2);
			            }
			            //2.2 如果结束时间在17点之后，则工时累计到17点截止
			            if(time2 > endTimePm){
			                travelHours += this.getTimeDiff(time1, endTimePm);
			            }
			        }

			    } else {
			        //如果开始时间小于8点，则计算工时的时候从8点开始计算
			        if(time1 < startTimeAm){
			        	alert("开始时间不能小于" + startTimeAm);
			            return 0;
			        }
			        //如果开始时间大于17点，
			        if(time1 > endTimePm){
			        	alert("开始时间不能大于" + endTimePm);
			            return 0;
			        }

			        if(time2 < startTimeAm){
			        	alert("结束时间不能小于" + startTimeAm);
			            return 0;
			        }
			        //1.3
			        if(time2 > endTimePm){
			        	alert("结束时间不能大于" + endTimePm);
			            return 0;
			        }
			        //计算开始时间当天工时
			        if(time1>= startTimeAm && time1<= endTimeAm){
			            travelHours+= this.getTimeDiff(time1, endTimePm) - this.getTimeDiff(endTimeAm, startTimePm);
			        }
			        if(time1>= startTimePm && time1<= endTimePm){
			            travelHours+= this.getTimeDiff(time1, endTimePm);
			        }
			        //计算结束时间当天工时
			        if(time2>=startTimeAm && time2<=endTimeAm){
			            travelHours+= this.getTimeDiff(startTimeAm, time2);
			        }
			        if(time2>=startTimePm && time2<=endTimePm){
			            travelHours+= this.getTimeDiff(startTimeAm, time2) - this.getTimeDiff(endTimeAm, startTimePm);
			        }
			        //计算开始时间和结束时间相差天数  开始时间和结束时间不算进去，只计算相差的时间
			        var dd = (date2 - date1) / 1000 / 60 / 60 / 24 - 1;
			        if(dd > 0){
			            travelHours+= dd*workHours1Day*60;
			        }
			        //计算开始时间和结束时间之中有几个周六周末,然后去除这个时间
			        var wkDays = this.getWeekendDays(date1,date2);
			        if(wkDays > 0){
			            travelHours -= wkDays*workHours1Day*60;
			        }
			    }
			    return travelHours;
			},//计算日期间的双休日天数
			getWeekendDays: function(dtStart, dtEnd) {
			    if (typeof dtEnd == 'string' )
			        dtEnd = this.dateStrToDate(dtEnd);
			    if (typeof dtStart == 'string' )
			        dtStart = this.dateStrToDate(dtStart);

			    var delta = (dtEnd - dtStart) / (1000 * 60 * 60 * 24) + 1; 
			    var date1 = dtStart;
			    
			    var weekEnds = 0; 
			    for(var i = 0; i < delta; i++) { 
			        if(date1.getDay() == 0 || date1.getDay() == 6) weekEnds ++; 
			        date1 = date1.valueOf(); 
			        date1 += 1000 * 60 * 60 * 24; 
			        date1 = new Date(date1); 
			    } 
			    return weekEnds;  
			},//自定义表单显示视图{index:1,row:row,container:container,formKey:formKey}
			setFormDetailView:function(param) {
				var id = param.row["id"];
				var vUrl = __ctx + "/gs/forms/" + param.formKey +".action?notShowQuery=1&queryParams=" 
							+ encodeURI(JSON.stringify({"fix_EQL_id":id, "rowIndex":param.index}));
				var ifrKey = "ifr_" + id;
				var str = '<iframe scrolling="auto" id="' + ifrKey + '" frameborder="0" src="' + vUrl + '" style="width:100%;height:200px;overflow:hidden;"></iframe>';
				param.container.html(str);
			},//父iframe自适应高度
			changeFdvParentHeight:function() {
			       var height = $("div.datagrid-body")[0].scrollHeight+$("div.datagrid-header").height() + 10;
			       var params = getFormQueryParams();
			       var id = params["fix_EQL_id"];
			       var rowIndex = params["rowIndex"];
				   var parentIfr = window.parent.$("#ifr_" + id);
			       if(parentIfr.length) {
			       	   parentIfr.css("height", height);
			       	   window.parent.$("#queryList").datagrid('fixDetailRowHeight', rowIndex);

			       }
			}//保存数据，
			,doSaveData: function(entityName, data, referId, callFunc) {
			 		var rows = data;
					var options = {};
					if(referId) {
						var jsons = {"id":parseInt(referId)};
						var newRows = {"updated":rows};
			        	jsons['data'] = newRows;
			        	jsons["saveType"] = "saveModify";//只保存修改数据的模式
						var jsonSavedata = JSON.stringify(jsons);
						options = {
								url : __ctx + '/gs/gs-mng!saveEntity.action?entityName=' + entityName,
								async : false,
								data : {
									"jsonSaveData" : jsonSavedata
								},
								success : function(data) {
									if(callFunc) {
										callFunc(data);
									}
								}
						};
					} else {
						var jsonSavedata = JSON.stringify(data);
						options = {
								url : __ctx + '/gs/gs-mng!saveList.action?entityName=' + entityName,
								async : false,
								data : {
									"jsonSaveData" : jsonSavedata,
									"field":"id",
									"value":"true"
								},
								success : function(data) {
									if(callFunc) {
										callFunc(data);
									}
								}
						};
					}
					fnFormAjaxWithJson(options);
			},//获取表单值，form表单转成json对象
			getFormData:function(formId) {
		      	var fields = $("#"+(formId||"viewForm")).serializeArray();
		      	var jsonObj = {};
		      	$.each(fields, function(k, v) {
		      		 if (jsonObj[v.name]) {
		      			 if(v.value) {
		      				jsonObj[v.name] = jsonObj[v.name] + "," + v.value;
		      			 }
		      		 } else { 
		      			 jsonObj[v.name] = v.value || ''; 
		      		 } 
		      	}); 
		      	return jsonObj;
		    },//设置表单值
		    setFormData:function(formId, data) {
		    	var fid = formId || "viewForm";
		    	$('#' + fid).form('load', data);
		    },
		    setPageValue:function(key, value) {
				this.getOpenerWin().PageStorage.write(key, value);
			},
			getPageValue:function(key) {
				return this.getOpenerWin().PageStorage.read(key);
			},
			removePageValue:function(key) {
				this.getOpenerWin().PageStorage.remove(key);
			},
			getOpenerWin:function() {
				if(window.opener) {
					return window.opener.top;
				} else {
					return window.top;
				}
			},//判断所有函数是否一致，一致返回true
			isAllEqual:function(array){
			    if(array.length>0){
			       return !array.some(function(value,index){
			         return value !== array[0];
			       });   
			    }else{
			        return true;
			    }
			},//打开新增页面
			doOpenAddPage:function(title, entityName, addParam) {
				var ckdUrl = __ctx + "/gs/gs-mng!add.action?entityName=" + entityName;
				if(addParam) ckdUrl += "&addParams=" + encodeURIComponent(JSON.stringify(addParam));
				jwpf.doOpenUrl(title, ckdUrl);
			},//根据属性名查询实体ID
			getEntityIdByAttr:function(entityName, fieldName, fieldValue) {
				var id = "";
				if($ES.isNotBlank(fieldValue)) {
					var url = __ctx+"/gs/gs-mng!queryEntityIdByAttr.action?entityName=" + entityName;
					var opt = {
							data:{
								"field":fieldName,
								"value":fieldValue
							},
							async:false,
							url:url,
							success:function(data){
								id = data.msg;
							}
					};
					fnFormAjaxWithJson(opt);
				}
				return id;
			},//打开查看页面
			doOpenViewPage:function(title, entityName, fieldName, fieldValue) {
				if($ES.isNotBlank(fieldValue)) {
					var id = (!fieldName || fieldName === "id") ? fieldValue : this.getEntityIdByAttr(entityName, fieldName, fieldValue);
					var ckdUrl = __ctx + "/gs/gs-mng!view.action?entityName=" + entityName + "&id=" + id;
					jwpf.doOpenUrl(title + "-" + id, ckdUrl);
				}
			},
			getStatus : function() {
				return jwpf.getFormVal("status");
			},
			/**
			 * 修订数据
			 * @reviseFields-- ["module.bb.name","module.bb.ss.1"]
			 * 
			 * {"aa.bb":{"id":"1", "version":11, "checkVersion":true, "isRevise":"10", "fields": [{"name":{"id":"","text":""}},{"ss":"11"}]}}
			 */
			reviseData : function(reviseFields, callBack, checkVersion) {
				var data = {};
				var checkVer = !(checkVersion === false);
				var isRevise = jwpf.getStatus();//单据状态
				var formData = jwpf.getFormData("viewForm");
				for(var i in reviseFields) {
					var fieldKey = reviseFields[i];
					if(fieldKey) {
						var fs = fieldKey.split(".");
						if(fs.length >= 3) {
							var isMaster = (fs.length == 3)?true:false;
							var tableKey = fs[1];
							var entityName = fs[0]+"."+fs[1];
							var fkey = fs[2];
							var fkeytext = fkey + "text";
							var map = {};
							if(isMaster) {
								map = formData;
							} else {
								var rowIndex = parseInt(fs[3]);
								map = jwpf.getTableVal(tableKey, rowIndex);
							}
							if(!data[entityName]) {
								data[entityName] = {"id":parseInt(map["id"]), "version":map["version"], "checkVersion": checkVer, "isRevise":isRevise, "fields":[]};
							}
							var field = {};
							if(map[fkeytext] === undefined) {
								field[fkey] = map[fkey];
							} else {
								field[fkey] = {"id":map[fkey], "text":map[fkeytext]};
							}
							data[entityName]["fields"].push(field);
						}
					}
				}
				var jsonSavedata = JSON.stringify(data);
				var options = {
						url : __ctx + '/gs/gs-mng!revise.action?1=1&entityName=' + reviseFields[0],
						//async : false,
						data : {
							"jsonSaveData" : jsonSavedata
						},
						success : function(data) {
							var msg = data.msg;
							if(callBack) {
								callBack(data.msg);
							}
						}
				};
				fnFormAjaxWithJson(options);
			},
			getIEVersion: function() {
	            var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串  
	            var isIE = userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1; //判断是否IE<11浏览器  
	            var isEdge = userAgent.indexOf("Edge") > -1 && !isIE; //判断是否IE的Edge浏览器  
	            var isIE11 = userAgent.indexOf('Trident') > -1 && userAgent.indexOf("rv:11.0") > -1;
	            if(isIE) {
	                var reIE = new RegExp("MSIE (\\d+\\.\\d+);");
	                reIE.test(userAgent);
	                var fIEVersion = parseFloat(RegExp["$1"]);
	                if(fIEVersion == 7) {
	                    return 7;
	                } else if(fIEVersion == 8) {
	                    return 8;
	                } else if(fIEVersion == 9) {
	                    return 9;
	                } else if(fIEVersion == 10) {
	                    return 10;
	                } else {
	                    return 6;//IE版本<=7
	                }   
	            } else if(isEdge) {
	                return 'edge';//edge
	            } else if(isIE11) {
	                return 11; //IE11  
	            }else{
	                return -1;//不是ie浏览器
	            }
	        },//根据word模板导出word文档
			exportWordByTemplateAsync: function(dataSqlKey, dataFilters, fileSqlKey, fileFilters,
												updateSqlKey, updateFilters, callBack) {
				var param = {
					"data":{
						"sqlKey":dataSqlKey,
						"filters":dataFilters
					},
					"file": {
						"sqlKey":fileSqlKey,
						"filters":fileFilters
					},
					"update": {
						"sqlKey":updateSqlKey,
						"filters":updateFilters
					}
				};
				var jsonData = JSON.stringify(param);
				var options = {
					url : __ctx + '/gs/gs-mng!exportWordByTemplateAsync.action',
					data : {
						"jsonSenddata" : jsonData
					},
					success : function(data) {
						if(callBack) {
							callBack(data);
						}
					}
				};
				fnFormAjaxWithJson(options);
			},
			exportWordByTemplate: function(dataSqlKey, dataFilters, fileSqlKey, fileFilters) {
				var param = {
					"data":{
						"sqlKey":dataSqlKey,
						"filters":dataFilters
					},
					"file": {
						"sqlKey":fileSqlKey,
						"filters":fileFilters
					}
				};
				var jsonData = JSON.stringify(param);
				var strUrl = __ctx+'/gs/gs-mng!exportWordByTemplate.action?jsonSenddata='
					+ (encodeURIComponent(jsonData)||"").replace(/\'/,"%27");
				window.open(strUrl);
			},
			exportWord: function(fileSqlKey, fileFilters) {
				var param = {
					"file": {
						"sqlKey":fileSqlKey,
						"filters":fileFilters
					}
				};
				var jsonData = JSON.stringify(param);
				var strUrl = __ctx+'/gs/gs-mng!exportWord.action?jsonSenddata='
					+ (encodeURIComponent(jsonData)||"").replace(/\'/,"%27");
				window.open(strUrl);
			},//{onAfterUpload:function(){},
			uploadFile: function(opt) {
				var param = $.extend(true, {}, opt);
				param.winId = "simple_file_box_win";
				param.message = '<div id="_simple_file_box_" style="margin-top:10px;"></div>';
				param.showTools = false;
				param.isIframe = false;
				param.maxFileNum = param.maxFileNum || 50;
				param.maxFileSize = param.maxFileSize || 500 * 1024 * 1024;
				param.maxSingleFileSize = param.maxSingleFileSize || 10 * 1024 * 1024;
				param.chunked = param.chunked || true;
				param.onInit = function(index) {
					$("#_simple_file_box_").filebox({
						baseUrl:__ctx,
						maxFileNum: param.maxFileNum,
						maxFileSize: param.maxFileSize,
						maxSingleFileSize: param.maxSingleFileSize,
						chunked: param.chunked,
						value:null,
						onAfterUploadFinished:function(val, oldVal, data) {
							if(param.onAfterUpload) {
								param.onAfterUpload(val, index, oldVal, data);
							}
						}
					});
				};
				openFormWinV2(param);
			}
};
(function($){
	$.fn.span_tip=function(setting){
			var option=$.extend({text:'请输入...',hide:false},setting);
			return this.each(function(){
				var self=$(this);
				if(self.parent(".tip_out_wrap").length == 0) {
					self.wrap("<span class='tip_out_wrap' style='width:"+self.outerWidth()+"px;height:"+self.outerHeight()+"px'></span>");
				}
				var tips = self.nextAll(".span_tip");
				if(option.hide) {
					tips.hide();
				} else {
					if(tips.length) {
						tips.html(option.text);
						tips.show();
					} else {
						var tip=$("<span class='span_tip'></span>");
						var namecss=self.attr('name')?self.attr('name')+"_tip_style":"custom_tips_style";
						tip.addClass(namecss).html(option.text).appendTo(self.parents('.tip_out_wrap'));
						self.blur(function(){
							if(self.val()=="")
							{
								tip.show();
							}
						});
						self.focus(function(){
							tip.hide();
						});
						self.change(function(){
							tip.hide();
						});
						tip.bind('click',function(){
							$(this).hide();
							self.focus();
						});
					}
				}
			});
		};
})(jQuery);


/*
 * JQuery zTree core v3.5.36
 * http://treejs.cn/
 *
 * Copyright (c) 2010 Hunter.z
 *
 * Licensed same as jquery - MIT License
 * http://www.opensource.org/licenses/mit-license.php
 *
 * email: hunter.z@263.net
 * Date: 2018-06-26
 */
(function(q){var H,I,J,K,L,M,u,s={},v={},w={},N={treeId:"",treeObj:null,view:{addDiyDom:null,autoCancelSelected:!0,dblClickExpand:!0,expandSpeed:"fast",fontCss:{},nameIsHTML:!1,selectedMulti:!0,showIcon:!0,showLine:!0,showTitle:!0,txtSelectedEnable:!1},data:{key:{isParent:"isParent",children:"children",name:"name",title:"",url:"url",icon:"icon"},simpleData:{enable:!1,idKey:"id",pIdKey:"pId",rootPId:null},keep:{parent:!1,leaf:!1}},async:{enable:!1,contentType:"application/x-www-form-urlencoded",type:"post",
dataType:"text",headers:{},xhrFields:{},url:"",autoParam:[],otherParam:[],dataFilter:null},callback:{beforeAsync:null,beforeClick:null,beforeDblClick:null,beforeRightClick:null,beforeMouseDown:null,beforeMouseUp:null,beforeExpand:null,beforeCollapse:null,beforeRemove:null,onAsyncError:null,onAsyncSuccess:null,onNodeCreated:null,onClick:null,onDblClick:null,onRightClick:null,onMouseDown:null,onMouseUp:null,onExpand:null,onCollapse:null,onRemove:null}},x=[function(a){var b=a.treeObj,c=f.event;b.bind(c.NODECREATED,
function(b,c,h){j.apply(a.callback.onNodeCreated,[b,c,h])});b.bind(c.CLICK,function(b,c,h,e,m){j.apply(a.callback.onClick,[c,h,e,m])});b.bind(c.EXPAND,function(b,c,h){j.apply(a.callback.onExpand,[b,c,h])});b.bind(c.COLLAPSE,function(b,c,h){j.apply(a.callback.onCollapse,[b,c,h])});b.bind(c.ASYNC_SUCCESS,function(b,c,h,e){j.apply(a.callback.onAsyncSuccess,[b,c,h,e])});b.bind(c.ASYNC_ERROR,function(b,c,h,e,m,f){j.apply(a.callback.onAsyncError,[b,c,h,e,m,f])});b.bind(c.REMOVE,function(b,c,h){j.apply(a.callback.onRemove,
[b,c,h])});b.bind(c.SELECTED,function(b,c,h){j.apply(a.callback.onSelected,[c,h])});b.bind(c.UNSELECTED,function(b,c,h){j.apply(a.callback.onUnSelected,[c,h])})}],y=[function(a){var b=f.event;a.treeObj.unbind(b.NODECREATED).unbind(b.CLICK).unbind(b.EXPAND).unbind(b.COLLAPSE).unbind(b.ASYNC_SUCCESS).unbind(b.ASYNC_ERROR).unbind(b.REMOVE).unbind(b.SELECTED).unbind(b.UNSELECTED)}],z=[function(a){var b=e.getCache(a);b||(b={},e.setCache(a,b));b.nodes=[];b.doms=[]}],A=[function(a,b,c,d,g,h){if(c){var k=
e.getRoot(a),m=e.nodeChildren(a,c);c.level=b;c.tId=a.treeId+"_"+ ++k.zId;c.parentTId=d?d.tId:null;c.open=typeof c.open=="string"?j.eqs(c.open,"true"):!!c.open;b=e.nodeIsParent(a,c);j.isArray(m)&&!(b===!1||typeof b=="string"&&j.eqs(b,"false"))?(e.nodeIsParent(a,c,!0),c.zAsync=!0):(b=e.nodeIsParent(a,c,b),c.open=b&&!a.async.enable?c.open:!1,c.zAsync=!b);c.isFirstNode=g;c.isLastNode=h;c.getParentNode=function(){return e.getNodeCache(a,c.parentTId)};c.getPreNode=function(){return e.getPreNode(a,c)};c.getNextNode=
function(){return e.getNextNode(a,c)};c.getIndex=function(){return e.getNodeIndex(a,c)};c.getPath=function(){return e.getNodePath(a,c)};c.isAjaxing=!1;e.fixPIdKeyValue(a,c)}}],t=[function(a){var b=a.target,c=e.getSetting(a.data.treeId),d="",g=null,h="",k="",m=null,i=null,o=null;if(j.eqs(a.type,"mousedown"))k="mousedown";else if(j.eqs(a.type,"mouseup"))k="mouseup";else if(j.eqs(a.type,"contextmenu"))k="contextmenu";else if(j.eqs(a.type,"click"))if(j.eqs(b.tagName,"span")&&b.getAttribute("treeNode"+
f.id.SWITCH)!==null)d=j.getNodeMainDom(b).id,h="switchNode";else{if(o=j.getMDom(c,b,[{tagName:"a",attrName:"treeNode"+f.id.A}]))d=j.getNodeMainDom(o).id,h="clickNode"}else if(j.eqs(a.type,"dblclick")&&(k="dblclick",o=j.getMDom(c,b,[{tagName:"a",attrName:"treeNode"+f.id.A}])))d=j.getNodeMainDom(o).id,h="switchNode";if(k.length>0&&d.length==0&&(o=j.getMDom(c,b,[{tagName:"a",attrName:"treeNode"+f.id.A}])))d=j.getNodeMainDom(o).id;if(d.length>0)switch(g=e.getNodeCache(c,d),h){case "switchNode":e.nodeIsParent(c,
g)?j.eqs(a.type,"click")||j.eqs(a.type,"dblclick")&&j.apply(c.view.dblClickExpand,[c.treeId,g],c.view.dblClickExpand)?m=H:h="":h="";break;case "clickNode":m=I}switch(k){case "mousedown":i=J;break;case "mouseup":i=K;break;case "dblclick":i=L;break;case "contextmenu":i=M}return{stop:!1,node:g,nodeEventType:h,nodeEventCallback:m,treeEventType:k,treeEventCallback:i}}],B=[function(a){var b=e.getRoot(a);b||(b={},e.setRoot(a,b));e.nodeChildren(a,b,[]);b.expandTriggerFlag=!1;b.curSelectedList=[];b.noSelection=
!0;b.createdNodes=[];b.zId=0;b._ver=(new Date).getTime()}],C=[],D=[],E=[],F=[],G=[],e={addNodeCache:function(a,b){e.getCache(a).nodes[e.getNodeCacheId(b.tId)]=b},getNodeCacheId:function(a){return a.substring(a.lastIndexOf("_")+1)},addAfterA:function(a){D.push(a)},addBeforeA:function(a){C.push(a)},addInnerAfterA:function(a){F.push(a)},addInnerBeforeA:function(a){E.push(a)},addInitBind:function(a){x.push(a)},addInitUnBind:function(a){y.push(a)},addInitCache:function(a){z.push(a)},addInitNode:function(a){A.push(a)},
addInitProxy:function(a,b){b?t.splice(0,0,a):t.push(a)},addInitRoot:function(a){B.push(a)},addNodesData:function(a,b,c,d){var g=e.nodeChildren(a,b);g?c>=g.length&&(c=-1):(g=e.nodeChildren(a,b,[]),c=-1);if(g.length>0&&c===0)g[0].isFirstNode=!1,i.setNodeLineIcos(a,g[0]);else if(g.length>0&&c<0)g[g.length-1].isLastNode=!1,i.setNodeLineIcos(a,g[g.length-1]);e.nodeIsParent(a,b,!0);c<0?e.nodeChildren(a,b,g.concat(d)):(a=[c,0].concat(d),g.splice.apply(g,a))},addSelectedNode:function(a,b){var c=e.getRoot(a);
e.isSelectedNode(a,b)||c.curSelectedList.push(b)},addCreatedNode:function(a,b){(a.callback.onNodeCreated||a.view.addDiyDom)&&e.getRoot(a).createdNodes.push(b)},addZTreeTools:function(a){G.push(a)},exSetting:function(a){q.extend(!0,N,a)},fixPIdKeyValue:function(a,b){a.data.simpleData.enable&&(b[a.data.simpleData.pIdKey]=b.parentTId?b.getParentNode()[a.data.simpleData.idKey]:a.data.simpleData.rootPId)},getAfterA:function(a,b,c){for(var d=0,e=D.length;d<e;d++)D[d].apply(this,arguments)},getBeforeA:function(a,
b,c){for(var d=0,e=C.length;d<e;d++)C[d].apply(this,arguments)},getInnerAfterA:function(a,b,c){for(var d=0,e=F.length;d<e;d++)F[d].apply(this,arguments)},getInnerBeforeA:function(a,b,c){for(var d=0,e=E.length;d<e;d++)E[d].apply(this,arguments)},getCache:function(a){return w[a.treeId]},getNodeIndex:function(a,b){if(!b)return null;for(var c=b.parentTId?b.getParentNode():e.getRoot(a),c=e.nodeChildren(a,c),d=0,g=c.length-1;d<=g;d++)if(c[d]===b)return d;return-1},getNextNode:function(a,b){if(!b)return null;
for(var c=b.parentTId?b.getParentNode():e.getRoot(a),c=e.nodeChildren(a,c),d=0,g=c.length-1;d<=g;d++)if(c[d]===b)return d==g?null:c[d+1];return null},getNodeByParam:function(a,b,c,d){if(!b||!c)return null;for(var g=0,h=b.length;g<h;g++){var k=b[g];if(k[c]==d)return b[g];k=e.nodeChildren(a,k);if(k=e.getNodeByParam(a,k,c,d))return k}return null},getNodeCache:function(a,b){if(!b)return null;var c=w[a.treeId].nodes[e.getNodeCacheId(b)];return c?c:null},getNodePath:function(a,b){if(!b)return null;var c;
(c=b.parentTId?b.getParentNode().getPath():[])&&c.push(b);return c},getNodes:function(a){return e.nodeChildren(a,e.getRoot(a))},getNodesByParam:function(a,b,c,d){if(!b||!c)return[];for(var g=[],h=0,k=b.length;h<k;h++){var m=b[h];m[c]==d&&g.push(m);m=e.nodeChildren(a,m);g=g.concat(e.getNodesByParam(a,m,c,d))}return g},getNodesByParamFuzzy:function(a,b,c,d){if(!b||!c)return[];for(var g=[],d=d.toLowerCase(),h=0,k=b.length;h<k;h++){var m=b[h];typeof m[c]=="string"&&b[h][c].toLowerCase().indexOf(d)>-1&&
g.push(m);m=e.nodeChildren(a,m);g=g.concat(e.getNodesByParamFuzzy(a,m,c,d))}return g},getNodesByFilter:function(a,b,c,d,g){if(!b)return d?null:[];for(var h=d?null:[],k=0,m=b.length;k<m;k++){var f=b[k];if(j.apply(c,[f,g],!1)){if(d)return f;h.push(f)}f=e.nodeChildren(a,f);f=e.getNodesByFilter(a,f,c,d,g);if(d&&f)return f;h=d?f:h.concat(f)}return h},getPreNode:function(a,b){if(!b)return null;for(var c=b.parentTId?b.getParentNode():e.getRoot(a),c=e.nodeChildren(a,c),d=0,g=c.length;d<g;d++)if(c[d]===b)return d==
0?null:c[d-1];return null},getRoot:function(a){return a?v[a.treeId]:null},getRoots:function(){return v},getSetting:function(a){return s[a]},getSettings:function(){return s},getZTreeTools:function(a){return(a=this.getRoot(this.getSetting(a)))?a.treeTools:null},initCache:function(a){for(var b=0,c=z.length;b<c;b++)z[b].apply(this,arguments)},initNode:function(a,b,c,d,e,h){for(var k=0,f=A.length;k<f;k++)A[k].apply(this,arguments)},initRoot:function(a){for(var b=0,c=B.length;b<c;b++)B[b].apply(this,arguments)},
isSelectedNode:function(a,b){for(var c=e.getRoot(a),d=0,g=c.curSelectedList.length;d<g;d++)if(b===c.curSelectedList[d])return!0;return!1},nodeChildren:function(a,b,c){if(!b)return null;a=a.data.key.children;typeof c!=="undefined"&&(b[a]=c);return b[a]},nodeIsParent:function(a,b,c){if(!b)return!1;a=a.data.key.isParent;typeof c!=="undefined"&&(typeof c==="string"&&(c=j.eqs(c,"true")),b[a]=!!c);return b[a]},nodeName:function(a,b,c){a=a.data.key.name;typeof c!=="undefined"&&(b[a]=c);return""+b[a]},nodeTitle:function(a,
b){return""+b[a.data.key.title===""?a.data.key.name:a.data.key.title]},removeNodeCache:function(a,b){var c=e.nodeChildren(a,b);if(c)for(var d=0,g=c.length;d<g;d++)e.removeNodeCache(a,c[d]);e.getCache(a).nodes[e.getNodeCacheId(b.tId)]=null},removeSelectedNode:function(a,b){for(var c=e.getRoot(a),d=0,g=c.curSelectedList.length;d<g;d++)if(b===c.curSelectedList[d]||!e.getNodeCache(a,c.curSelectedList[d].tId))c.curSelectedList.splice(d,1),a.treeObj.trigger(f.event.UNSELECTED,[a.treeId,b]),d--,g--},setCache:function(a,
b){w[a.treeId]=b},setRoot:function(a,b){v[a.treeId]=b},setZTreeTools:function(a,b){for(var c=0,d=G.length;c<d;c++)G[c].apply(this,arguments)},transformToArrayFormat:function(a,b){function c(b){d.push(b);(b=e.nodeChildren(a,b))&&(d=d.concat(e.transformToArrayFormat(a,b)))}if(!b)return[];var d=[];if(j.isArray(b))for(var g=0,h=b.length;g<h;g++)c(b[g]);else c(b);return d},transformTozTreeFormat:function(a,b){var c,d,g=a.data.simpleData.idKey,h=a.data.simpleData.pIdKey;if(!g||g==""||!b)return[];if(j.isArray(b)){var k=
[],f={};for(c=0,d=b.length;c<d;c++)f[b[c][g]]=b[c];for(c=0,d=b.length;c<d;c++){var i=f[b[c][h]];if(i&&b[c][g]!=b[c][h]){var o=e.nodeChildren(a,i);o||(o=e.nodeChildren(a,i,[]));o.push(b[c])}else k.push(b[c])}return k}else return[b]}},n={bindEvent:function(a){for(var b=0,c=x.length;b<c;b++)x[b].apply(this,arguments)},unbindEvent:function(a){for(var b=0,c=y.length;b<c;b++)y[b].apply(this,arguments)},bindTree:function(a){var b={treeId:a.treeId},c=a.treeObj;a.view.txtSelectedEnable||c.bind("selectstart",
u).css({"-moz-user-select":"-moz-none"});c.bind("click",b,n.proxy);c.bind("dblclick",b,n.proxy);c.bind("mouseover",b,n.proxy);c.bind("mouseout",b,n.proxy);c.bind("mousedown",b,n.proxy);c.bind("mouseup",b,n.proxy);c.bind("contextmenu",b,n.proxy)},unbindTree:function(a){a.treeObj.unbind("selectstart",u).unbind("click",n.proxy).unbind("dblclick",n.proxy).unbind("mouseover",n.proxy).unbind("mouseout",n.proxy).unbind("mousedown",n.proxy).unbind("mouseup",n.proxy).unbind("contextmenu",n.proxy)},doProxy:function(a){for(var b=
[],c=0,d=t.length;c<d;c++){var e=t[c].apply(this,arguments);b.push(e);if(e.stop)break}return b},proxy:function(a){var b=e.getSetting(a.data.treeId);if(!j.uCanDo(b,a))return!0;for(var b=n.doProxy(a),c=!0,d=0,g=b.length;d<g;d++){var h=b[d];h.nodeEventCallback&&(c=h.nodeEventCallback.apply(h,[a,h.node])&&c);h.treeEventCallback&&(c=h.treeEventCallback.apply(h,[a,h.node])&&c)}return c}};H=function(a,b){var c=e.getSetting(a.data.treeId);if(b.open){if(j.apply(c.callback.beforeCollapse,[c.treeId,b],!0)==
!1)return!0}else if(j.apply(c.callback.beforeExpand,[c.treeId,b],!0)==!1)return!0;e.getRoot(c).expandTriggerFlag=!0;i.switchNode(c,b);return!0};I=function(a,b){var c=e.getSetting(a.data.treeId),d=c.view.autoCancelSelected&&(a.ctrlKey||a.metaKey)&&e.isSelectedNode(c,b)?0:c.view.autoCancelSelected&&(a.ctrlKey||a.metaKey)&&c.view.selectedMulti?2:1;if(j.apply(c.callback.beforeClick,[c.treeId,b,d],!0)==!1)return!0;d===0?i.cancelPreSelectedNode(c,b):i.selectNode(c,b,d===2);c.treeObj.trigger(f.event.CLICK,
[a,c.treeId,b,d]);return!0};J=function(a,b){var c=e.getSetting(a.data.treeId);j.apply(c.callback.beforeMouseDown,[c.treeId,b],!0)&&j.apply(c.callback.onMouseDown,[a,c.treeId,b]);return!0};K=function(a,b){var c=e.getSetting(a.data.treeId);j.apply(c.callback.beforeMouseUp,[c.treeId,b],!0)&&j.apply(c.callback.onMouseUp,[a,c.treeId,b]);return!0};L=function(a,b){var c=e.getSetting(a.data.treeId);j.apply(c.callback.beforeDblClick,[c.treeId,b],!0)&&j.apply(c.callback.onDblClick,[a,c.treeId,b]);return!0};
M=function(a,b){var c=e.getSetting(a.data.treeId);j.apply(c.callback.beforeRightClick,[c.treeId,b],!0)&&j.apply(c.callback.onRightClick,[a,c.treeId,b]);return typeof c.callback.onRightClick!="function"};u=function(a){a=a.originalEvent.srcElement.nodeName.toLowerCase();return a==="input"||a==="textarea"};var j={apply:function(a,b,c){return typeof a=="function"?a.apply(O,b?b:[]):c},canAsync:function(a,b){var c=e.nodeChildren(a,b),d=e.nodeIsParent(a,b);return a.async.enable&&b&&d&&!(b.zAsync||c&&c.length>
0)},clone:function(a){if(a===null)return null;var b=j.isArray(a)?[]:{},c;for(c in a)b[c]=a[c]instanceof Date?new Date(a[c].getTime()):typeof a[c]==="object"?j.clone(a[c]):a[c];return b},eqs:function(a,b){return a.toLowerCase()===b.toLowerCase()},isArray:function(a){return Object.prototype.toString.apply(a)==="[object Array]"},isElement:function(a){return typeof HTMLElement==="object"?a instanceof HTMLElement:a&&typeof a==="object"&&a!==null&&a.nodeType===1&&typeof a.nodeName==="string"},$:function(a,
b,c){b&&typeof b!="string"&&(c=b,b="");return typeof a=="string"?q(a,c?c.treeObj.get(0).ownerDocument:null):q("#"+a.tId+b,c?c.treeObj:null)},getMDom:function(a,b,c){if(!b)return null;for(;b&&b.id!==a.treeId;){for(var d=0,e=c.length;b.tagName&&d<e;d++)if(j.eqs(b.tagName,c[d].tagName)&&b.getAttribute(c[d].attrName)!==null)return b;b=b.parentNode}return null},getNodeMainDom:function(a){return q(a).parent("li").get(0)||q(a).parentsUntil("li").parent().get(0)},isChildOrSelf:function(a,b){return q(a).closest("#"+
b).length>0},uCanDo:function(){return!0}},i={addNodes:function(a,b,c,d,g){var h=e.nodeIsParent(a,b);if(!a.data.keep.leaf||!b||h)if(j.isArray(d)||(d=[d]),a.data.simpleData.enable&&(d=e.transformTozTreeFormat(a,d)),b){var h=l(b,f.id.SWITCH,a),k=l(b,f.id.ICON,a),m=l(b,f.id.UL,a);if(!b.open)i.replaceSwitchClass(b,h,f.folder.CLOSE),i.replaceIcoClass(b,k,f.folder.CLOSE),b.open=!1,m.css({display:"none"});e.addNodesData(a,b,c,d);i.createNodes(a,b.level+1,d,b,c);g||i.expandCollapseParentNode(a,b,!0)}else e.addNodesData(a,
e.getRoot(a),c,d),i.createNodes(a,0,d,null,c)},appendNodes:function(a,b,c,d,g,h,k){if(!c)return[];var f=[],j=d?d:e.getRoot(a),j=e.nodeChildren(a,j),o,l;if(!j||g>=j.length-c.length)g=-1;for(var n=0,Q=c.length;n<Q;n++){var p=c[n];h&&(o=(g===0||j.length==c.length)&&n==0,l=g<0&&n==c.length-1,e.initNode(a,b,p,d,o,l,k),e.addNodeCache(a,p));o=e.nodeIsParent(a,p);l=[];var q=e.nodeChildren(a,p);q&&q.length>0&&(l=i.appendNodes(a,b+1,q,p,-1,h,k&&p.open));k&&(i.makeDOMNodeMainBefore(f,a,p),i.makeDOMNodeLine(f,
a,p),e.getBeforeA(a,p,f),i.makeDOMNodeNameBefore(f,a,p),e.getInnerBeforeA(a,p,f),i.makeDOMNodeIcon(f,a,p),e.getInnerAfterA(a,p,f),i.makeDOMNodeNameAfter(f,a,p),e.getAfterA(a,p,f),o&&p.open&&i.makeUlHtml(a,p,f,l.join("")),i.makeDOMNodeMainAfter(f,a,p),e.addCreatedNode(a,p))}return f},appendParentULDom:function(a,b){var c=[],d=l(b,a);!d.get(0)&&b.parentTId&&(i.appendParentULDom(a,b.getParentNode()),d=l(b,a));var g=l(b,f.id.UL,a);g.get(0)&&g.remove();g=e.nodeChildren(a,b);g=i.appendNodes(a,b.level+1,
g,b,-1,!1,!0);i.makeUlHtml(a,b,c,g.join(""));d.append(c.join(""))},asyncNode:function(a,b,c,d){var g,h;g=e.nodeIsParent(a,b);if(b&&!g)return j.apply(d),!1;else if(b&&b.isAjaxing)return!1;else if(j.apply(a.callback.beforeAsync,[a.treeId,b],!0)==!1)return j.apply(d),!1;if(b)b.isAjaxing=!0,l(b,f.id.ICON,a).attr({style:"","class":f.className.BUTTON+" "+f.className.ICO_LOADING});var k={},m=j.apply(a.async.autoParam,[a.treeId,b],a.async.autoParam);for(g=0,h=m.length;b&&g<h;g++){var r=m[g].split("="),o=
r;r.length>1&&(o=r[1],r=r[0]);k[o]=b[r]}m=j.apply(a.async.otherParam,[a.treeId,b],a.async.otherParam);if(j.isArray(m))for(g=0,h=m.length;g<h;g+=2)k[m[g]]=m[g+1];else for(var n in m)k[n]=m[n];var P=e.getRoot(a)._ver;q.ajax({contentType:a.async.contentType,cache:!1,type:a.async.type,url:j.apply(a.async.url,[a.treeId,b],a.async.url),data:a.async.contentType.indexOf("application/json")>-1?JSON.stringify(k):k,dataType:a.async.dataType,headers:a.async.headers,xhrFields:a.async.xhrFields,success:function(h){if(P==
e.getRoot(a)._ver){var k=[];try{k=!h||h.length==0?[]:typeof h=="string"?eval("("+h+")"):h}catch(g){k=h}if(b)b.isAjaxing=null,b.zAsync=!0;i.setNodeLineIcos(a,b);k&&k!==""?(k=j.apply(a.async.dataFilter,[a.treeId,b,k],k),i.addNodes(a,b,-1,k?j.clone(k):[],!!c)):i.addNodes(a,b,-1,[],!!c);a.treeObj.trigger(f.event.ASYNC_SUCCESS,[a.treeId,b,h]);j.apply(d)}},error:function(c,d,h){if(P==e.getRoot(a)._ver){if(b)b.isAjaxing=null;i.setNodeLineIcos(a,b);a.treeObj.trigger(f.event.ASYNC_ERROR,[a.treeId,b,c,d,h])}}});
return!0},cancelPreSelectedNode:function(a,b,c){var d=e.getRoot(a).curSelectedList,g,h;for(g=d.length-1;g>=0;g--)if(h=d[g],b===h||!b&&(!c||c!==h))if(l(h,f.id.A,a).removeClass(f.node.CURSELECTED),b){e.removeSelectedNode(a,b);break}else d.splice(g,1),a.treeObj.trigger(f.event.UNSELECTED,[a.treeId,h])},createNodeCallback:function(a){if(a.callback.onNodeCreated||a.view.addDiyDom)for(var b=e.getRoot(a);b.createdNodes.length>0;){var c=b.createdNodes.shift();j.apply(a.view.addDiyDom,[a.treeId,c]);a.callback.onNodeCreated&&
a.treeObj.trigger(f.event.NODECREATED,[a.treeId,c])}},createNodes:function(a,b,c,d,g){if(c&&c.length!=0){var h=e.getRoot(a),k=!d||d.open||!!l(e.nodeChildren(a,d)[0],a).get(0);h.createdNodes=[];var b=i.appendNodes(a,b,c,d,g,!0,k),m,j;d?(d=l(d,f.id.UL,a),d.get(0)&&(m=d)):m=a.treeObj;m&&(g>=0&&(j=m.children()[g]),g>=0&&j?q(j).before(b.join("")):m.append(b.join("")));i.createNodeCallback(a)}},destroy:function(a){a&&(e.initCache(a),e.initRoot(a),n.unbindTree(a),n.unbindEvent(a),a.treeObj.empty(),delete s[a.treeId])},
expandCollapseNode:function(a,b,c,d,g){var h=e.getRoot(a),k;if(b){var m=e.nodeChildren(a,b),r=e.nodeIsParent(a,b);if(h.expandTriggerFlag)k=g,g=function(){k&&k();b.open?a.treeObj.trigger(f.event.EXPAND,[a.treeId,b]):a.treeObj.trigger(f.event.COLLAPSE,[a.treeId,b])},h.expandTriggerFlag=!1;if(!b.open&&r&&(!l(b,f.id.UL,a).get(0)||m&&m.length>0&&!l(m[0],a).get(0)))i.appendParentULDom(a,b),i.createNodeCallback(a);if(b.open==c)j.apply(g,[]);else{var c=l(b,f.id.UL,a),h=l(b,f.id.SWITCH,a),o=l(b,f.id.ICON,
a);r?(b.open=!b.open,b.iconOpen&&b.iconClose&&o.attr("style",i.makeNodeIcoStyle(a,b)),b.open?(i.replaceSwitchClass(b,h,f.folder.OPEN),i.replaceIcoClass(b,o,f.folder.OPEN),d==!1||a.view.expandSpeed==""?(c.show(),j.apply(g,[])):m&&m.length>0?c.slideDown(a.view.expandSpeed,g):(c.show(),j.apply(g,[]))):(i.replaceSwitchClass(b,h,f.folder.CLOSE),i.replaceIcoClass(b,o,f.folder.CLOSE),d==!1||a.view.expandSpeed==""||!(m&&m.length>0)?(c.hide(),j.apply(g,[])):c.slideUp(a.view.expandSpeed,g))):j.apply(g,[])}}else j.apply(g,
[])},expandCollapseParentNode:function(a,b,c,d,e){b&&(b.parentTId?(i.expandCollapseNode(a,b,c,d),b.parentTId&&i.expandCollapseParentNode(a,b.getParentNode(),c,d,e)):i.expandCollapseNode(a,b,c,d,e))},expandCollapseSonNode:function(a,b,c,d,g){var h=e.getRoot(a),h=b?e.nodeChildren(a,b):e.nodeChildren(a,h),k=b?!1:d,f=e.getRoot(a).expandTriggerFlag;e.getRoot(a).expandTriggerFlag=!1;if(h)for(var j=0,l=h.length;j<l;j++)h[j]&&i.expandCollapseSonNode(a,h[j],c,k);e.getRoot(a).expandTriggerFlag=f;i.expandCollapseNode(a,
b,c,d,g)},isSelectedNode:function(a,b){if(!b)return!1;var c=e.getRoot(a).curSelectedList,d;for(d=c.length-1;d>=0;d--)if(b===c[d])return!0;return!1},makeDOMNodeIcon:function(a,b,c){var d=e.nodeName(b,c),d=b.view.nameIsHTML?d:d.replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/>/g,"&gt;");a.push("<span id='",c.tId,f.id.ICON,"' title='' treeNode",f.id.ICON," class='",i.makeNodeIcoClass(b,c),"' style='",i.makeNodeIcoStyle(b,c),"'></span><span id='",c.tId,f.id.SPAN,"' class='",f.className.NAME,"'>",
d,"</span>")},makeDOMNodeLine:function(a,b,c){a.push("<span id='",c.tId,f.id.SWITCH,"' title='' class='",i.makeNodeLineClass(b,c),"' treeNode",f.id.SWITCH,"></span>")},makeDOMNodeMainAfter:function(a){a.push("</li>")},makeDOMNodeMainBefore:function(a,b,c){a.push("<li id='",c.tId,"' class='",f.className.LEVEL,c.level,"' tabindex='0' hidefocus='true' treenode>")},makeDOMNodeNameAfter:function(a){a.push("</a>")},makeDOMNodeNameBefore:function(a,b,c){var d=e.nodeTitle(b,c),g=i.makeNodeUrl(b,c),h=i.makeNodeFontCss(b,
c),k=[],m;for(m in h)k.push(m,":",h[m],";");a.push("<a id='",c.tId,f.id.A,"' class='",f.className.LEVEL,c.level,"' treeNode",f.id.A,' onclick="',c.click||"",'" ',g!=null&&g.length>0?"href='"+g+"'":""," target='",i.makeNodeTarget(c),"' style='",k.join(""),"'");j.apply(b.view.showTitle,[b.treeId,c],b.view.showTitle)&&d&&a.push("title='",d.replace(/'/g,"&#39;").replace(/</g,"&lt;").replace(/>/g,"&gt;"),"'");a.push(">")},makeNodeFontCss:function(a,b){var c=j.apply(a.view.fontCss,[a.treeId,b],a.view.fontCss);
return c&&typeof c!="function"?c:{}},makeNodeIcoClass:function(a,b){var c=["ico"];if(!b.isAjaxing){var d=e.nodeIsParent(a,b);c[0]=(b.iconSkin?b.iconSkin+"_":"")+c[0];d?c.push(b.open?f.folder.OPEN:f.folder.CLOSE):c.push(f.folder.DOCU)}return f.className.BUTTON+" "+c.join("_")},makeNodeIcoStyle:function(a,b){var c=[];if(!b.isAjaxing){var d=e.nodeIsParent(a,b)&&b.iconOpen&&b.iconClose?b.open?b.iconOpen:b.iconClose:b[a.data.key.icon];d&&c.push("background:url(",d,") 0 0 no-repeat;");(a.view.showIcon==
!1||!j.apply(a.view.showIcon,[a.treeId,b],!0))&&c.push("width:0px;height:0px;")}return c.join("")},makeNodeLineClass:function(a,b){var c=[];a.view.showLine?b.level==0&&b.isFirstNode&&b.isLastNode?c.push(f.line.ROOT):b.level==0&&b.isFirstNode?c.push(f.line.ROOTS):b.isLastNode?c.push(f.line.BOTTOM):c.push(f.line.CENTER):c.push(f.line.NOLINE);e.nodeIsParent(a,b)?c.push(b.open?f.folder.OPEN:f.folder.CLOSE):c.push(f.folder.DOCU);return i.makeNodeLineClassEx(b)+c.join("_")},makeNodeLineClassEx:function(a){return f.className.BUTTON+
" "+f.className.LEVEL+a.level+" "+f.className.SWITCH+" "},makeNodeTarget:function(a){return a.target||"_blank"},makeNodeUrl:function(a,b){var c=a.data.key.url;return b[c]?b[c]:null},makeUlHtml:function(a,b,c,d){c.push("<ul id='",b.tId,f.id.UL,"' class='",f.className.LEVEL,b.level," ",i.makeUlLineClass(a,b),"' style='display:",b.open?"block":"none","'>");c.push(d);c.push("</ul>")},makeUlLineClass:function(a,b){return a.view.showLine&&!b.isLastNode?f.line.LINE:""},removeChildNodes:function(a,b){if(b){var c=
e.nodeChildren(a,b);if(c){for(var d=0,g=c.length;d<g;d++)e.removeNodeCache(a,c[d]);e.removeSelectedNode(a);delete b[a.data.key.children];a.data.keep.parent?l(b,f.id.UL,a).empty():(e.nodeIsParent(a,b,!1),b.open=!1,c=l(b,f.id.SWITCH,a),d=l(b,f.id.ICON,a),i.replaceSwitchClass(b,c,f.folder.DOCU),i.replaceIcoClass(b,d,f.folder.DOCU),l(b,f.id.UL,a).remove())}}},scrollIntoView:function(a,b){if(b)if(typeof Element==="undefined"){var c=a.treeObj.get(0).getBoundingClientRect(),d=b.getBoundingClientRect();(d.top<
c.top||d.bottom>c.bottom||d.right>c.right||d.left<c.left)&&b.scrollIntoView()}else{if(!Element.prototype.scrollIntoViewIfNeeded)Element.prototype.scrollIntoViewIfNeeded=function(a){function b(a,c,d,f){return{left:a,top:c,width:d,height:f,right:a+d,bottom:c+f,translate:function(e,g){return b(e+a,g+c,d,f)},relativeFromTo:function(g,k){var i=a,j=c,g=g.offsetParent,k=k.offsetParent;if(g===k)return e;for(;g;g=g.offsetParent)i+=g.offsetLeft+g.clientLeft,j+=g.offsetTop+g.clientTop;for(;k;k=k.offsetParent)i-=
k.offsetLeft+k.clientLeft,j-=k.offsetTop+k.clientTop;return b(i,j,d,f)}}}for(var c,d=this,e=b(this.offsetLeft,this.offsetTop,this.offsetWidth,this.offsetHeight);j.isElement(c=d.parentNode);){var f=c.offsetLeft+c.clientLeft,i=c.offsetTop+c.clientTop,e=e.relativeFromTo(d,c).translate(-f,-i);c.scrollLeft=!1===a||e.left<=c.scrollLeft+c.clientWidth&&c.scrollLeft<=e.right-c.clientWidth+c.clientWidth?Math.min(e.left,Math.max(e.right-c.clientWidth,c.scrollLeft)):(e.right-c.clientWidth+e.left)/2;c.scrollTop=
!1===a||e.top<=c.scrollTop+c.clientHeight&&c.scrollTop<=e.bottom-c.clientHeight+c.clientHeight?Math.min(e.top,Math.max(e.bottom-c.clientHeight,c.scrollTop)):(e.bottom-c.clientHeight+e.top)/2;e=e.translate(f-c.scrollLeft,i-c.scrollTop);d=c}};b.scrollIntoViewIfNeeded()}},setFirstNode:function(a,b){var c=e.nodeChildren(a,b);if(c.length>0)c[0].isFirstNode=!0},setLastNode:function(a,b){var c=e.nodeChildren(a,b);if(c.length>0)c[c.length-1].isLastNode=!0},removeNode:function(a,b){var c=e.getRoot(a),d=b.parentTId?
b.getParentNode():c;b.isFirstNode=!1;b.isLastNode=!1;b.getPreNode=function(){return null};b.getNextNode=function(){return null};if(e.getNodeCache(a,b.tId)){l(b,a).remove();e.removeNodeCache(a,b);e.removeSelectedNode(a,b);for(var g=e.nodeChildren(a,d),h=0,k=g.length;h<k;h++)if(g[h].tId==b.tId){g.splice(h,1);break}i.setFirstNode(a,d);i.setLastNode(a,d);var j,h=g.length;if(!a.data.keep.parent&&h==0)e.nodeIsParent(a,d,!1),d.open=!1,delete d[a.data.key.children],h=l(d,f.id.UL,a),k=l(d,f.id.SWITCH,a),j=
l(d,f.id.ICON,a),i.replaceSwitchClass(d,k,f.folder.DOCU),i.replaceIcoClass(d,j,f.folder.DOCU),h.css("display","none");else if(a.view.showLine&&h>0){var r=g[h-1],h=l(r,f.id.UL,a),k=l(r,f.id.SWITCH,a);j=l(r,f.id.ICON,a);d==c?g.length==1?i.replaceSwitchClass(r,k,f.line.ROOT):(c=l(g[0],f.id.SWITCH,a),i.replaceSwitchClass(g[0],c,f.line.ROOTS),i.replaceSwitchClass(r,k,f.line.BOTTOM)):i.replaceSwitchClass(r,k,f.line.BOTTOM);h.removeClass(f.line.LINE)}}},replaceIcoClass:function(a,b,c){if(b&&!a.isAjaxing&&
(a=b.attr("class"),a!=void 0)){a=a.split("_");switch(c){case f.folder.OPEN:case f.folder.CLOSE:case f.folder.DOCU:a[a.length-1]=c}b.attr("class",a.join("_"))}},replaceSwitchClass:function(a,b,c){if(b){var d=b.attr("class");if(d!=void 0){d=d.split("_");switch(c){case f.line.ROOT:case f.line.ROOTS:case f.line.CENTER:case f.line.BOTTOM:case f.line.NOLINE:d[0]=i.makeNodeLineClassEx(a)+c;break;case f.folder.OPEN:case f.folder.CLOSE:case f.folder.DOCU:d[1]=c}b.attr("class",d.join("_"));c!==f.folder.DOCU?
b.removeAttr("disabled"):b.attr("disabled","disabled")}}},selectNode:function(a,b,c){c||i.cancelPreSelectedNode(a,null,b);l(b,f.id.A,a).addClass(f.node.CURSELECTED);e.addSelectedNode(a,b);a.treeObj.trigger(f.event.SELECTED,[a.treeId,b])},setNodeFontCss:function(a,b){var c=l(b,f.id.A,a),d=i.makeNodeFontCss(a,b);d&&c.css(d)},setNodeLineIcos:function(a,b){if(b){var c=l(b,f.id.SWITCH,a),d=l(b,f.id.UL,a),g=l(b,f.id.ICON,a),h=i.makeUlLineClass(a,b);h.length==0?d.removeClass(f.line.LINE):d.addClass(h);c.attr("class",
i.makeNodeLineClass(a,b));e.nodeIsParent(a,b)?c.removeAttr("disabled"):c.attr("disabled","disabled");g.removeAttr("style");g.attr("style",i.makeNodeIcoStyle(a,b));g.attr("class",i.makeNodeIcoClass(a,b))}},setNodeName:function(a,b){var c=e.nodeTitle(a,b),d=l(b,f.id.SPAN,a);d.empty();a.view.nameIsHTML?d.html(e.nodeName(a,b)):d.text(e.nodeName(a,b));j.apply(a.view.showTitle,[a.treeId,b],a.view.showTitle)&&l(b,f.id.A,a).attr("title",!c?"":c)},setNodeTarget:function(a,b){l(b,f.id.A,a).attr("target",i.makeNodeTarget(b))},
setNodeUrl:function(a,b){var c=l(b,f.id.A,a),d=i.makeNodeUrl(a,b);d==null||d.length==0?c.removeAttr("href"):c.attr("href",d)},switchNode:function(a,b){b.open||!j.canAsync(a,b)?i.expandCollapseNode(a,b,!b.open):a.async.enable?i.asyncNode(a,b)||i.expandCollapseNode(a,b,!b.open):b&&i.expandCollapseNode(a,b,!b.open)}};q.fn.zTree={consts:{className:{BUTTON:"button",LEVEL:"level",ICO_LOADING:"ico_loading",SWITCH:"switch",NAME:"node_name"},event:{NODECREATED:"ztree_nodeCreated",CLICK:"ztree_click",EXPAND:"ztree_expand",
COLLAPSE:"ztree_collapse",ASYNC_SUCCESS:"ztree_async_success",ASYNC_ERROR:"ztree_async_error",REMOVE:"ztree_remove",SELECTED:"ztree_selected",UNSELECTED:"ztree_unselected"},id:{A:"_a",ICON:"_ico",SPAN:"_span",SWITCH:"_switch",UL:"_ul"},line:{ROOT:"root",ROOTS:"roots",CENTER:"center",BOTTOM:"bottom",NOLINE:"noline",LINE:"line"},folder:{OPEN:"open",CLOSE:"close",DOCU:"docu"},node:{CURSELECTED:"curSelectedNode"}},_z:{tools:j,view:i,event:n,data:e},getZTreeObj:function(a){return(a=e.getZTreeTools(a))?
a:null},destroy:function(a){if(a&&a.length>0)i.destroy(e.getSetting(a));else for(var b in s)i.destroy(s[b])},init:function(a,b,c){var d=j.clone(N);q.extend(!0,d,b);d.treeId=a.attr("id");d.treeObj=a;d.treeObj.empty();s[d.treeId]=d;if(typeof document.body.style.maxHeight==="undefined")d.view.expandSpeed="";e.initRoot(d);a=e.getRoot(d);c=c?j.clone(j.isArray(c)?c:[c]):[];d.data.simpleData.enable?e.nodeChildren(d,a,e.transformTozTreeFormat(d,c)):e.nodeChildren(d,a,c);e.initCache(d);n.unbindTree(d);n.bindTree(d);
n.unbindEvent(d);n.bindEvent(d);var g={setting:d,addNodes:function(a,b,c,g){function f(){i.addNodes(d,a,b,n,g==!0)}a||(a=null);var l=e.nodeIsParent(d,a);if(a&&!l&&d.data.keep.leaf)return null;l=parseInt(b,10);isNaN(l)?(g=!!c,c=b,b=-1):b=l;if(!c)return null;var n=j.clone(j.isArray(c)?c:[c]);j.canAsync(d,a)?i.asyncNode(d,a,g,f):f();return n},cancelSelectedNode:function(a){i.cancelPreSelectedNode(d,a)},destroy:function(){i.destroy(d)},expandAll:function(a){a=!!a;i.expandCollapseSonNode(d,null,a,!0);
return a},expandNode:function(a,b,c,g,f){function n(){var b=l(a,d).get(0);b&&g!==!1&&i.scrollIntoView(d,b)}if(!a||!e.nodeIsParent(d,a))return null;b!==!0&&b!==!1&&(b=!a.open);if((f=!!f)&&b&&j.apply(d.callback.beforeExpand,[d.treeId,a],!0)==!1)return null;else if(f&&!b&&j.apply(d.callback.beforeCollapse,[d.treeId,a],!0)==!1)return null;b&&a.parentTId&&i.expandCollapseParentNode(d,a.getParentNode(),b,!1);if(b===a.open&&!c)return null;e.getRoot(d).expandTriggerFlag=f;!j.canAsync(d,a)&&c?i.expandCollapseSonNode(d,
a,b,!0,n):(a.open=!b,i.switchNode(this.setting,a),n());return b},getNodes:function(){return e.getNodes(d)},getNodeByParam:function(a,b,c){return!a?null:e.getNodeByParam(d,c?e.nodeChildren(d,c):e.getNodes(d),a,b)},getNodeByTId:function(a){return e.getNodeCache(d,a)},getNodesByParam:function(a,b,c){return!a?null:e.getNodesByParam(d,c?e.nodeChildren(d,c):e.getNodes(d),a,b)},getNodesByParamFuzzy:function(a,b,c){return!a?null:e.getNodesByParamFuzzy(d,c?e.nodeChildren(d,c):e.getNodes(d),a,b)},getNodesByFilter:function(a,
b,c,f){b=!!b;return!a||typeof a!="function"?b?null:[]:e.getNodesByFilter(d,c?e.nodeChildren(d,c):e.getNodes(d),a,b,f)},getNodeIndex:function(a){if(!a)return null;for(var b=a.parentTId?a.getParentNode():e.getRoot(d),b=e.nodeChildren(d,b),c=0,f=b.length;c<f;c++)if(b[c]==a)return c;return-1},getSelectedNodes:function(){for(var a=[],b=e.getRoot(d).curSelectedList,c=0,f=b.length;c<f;c++)a.push(b[c]);return a},isSelectedNode:function(a){return e.isSelectedNode(d,a)},reAsyncChildNodesPromise:function(a,
b,c){return new Promise(function(d,e){try{g.reAsyncChildNodes(a,b,c,function(){d(a)})}catch(f){e(f)}})},reAsyncChildNodes:function(a,b,c,g){if(this.setting.async.enable){var j=!a;j&&(a=e.getRoot(d));if(b=="refresh"){for(var b=e.nodeChildren(d,a),n=0,q=b?b.length:0;n<q;n++)e.removeNodeCache(d,b[n]);e.removeSelectedNode(d);e.nodeChildren(d,a,[]);j?this.setting.treeObj.empty():l(a,f.id.UL,d).empty()}i.asyncNode(this.setting,j?null:a,!!c,g)}},refresh:function(){this.setting.treeObj.empty();var a=e.getRoot(d),
b=e.nodeChildren(d,a);e.initRoot(d);e.nodeChildren(d,a,b);e.initCache(d);i.createNodes(d,0,e.nodeChildren(d,a),null,-1)},removeChildNodes:function(a){if(!a)return null;var b=e.nodeChildren(d,a);i.removeChildNodes(d,a);return b?b:null},removeNode:function(a,b){a&&(b=!!b,b&&j.apply(d.callback.beforeRemove,[d.treeId,a],!0)==!1||(i.removeNode(d,a),b&&this.setting.treeObj.trigger(f.event.REMOVE,[d.treeId,a])))},selectNode:function(a,b,c){function e(){if(!c){var b=l(a,d).get(0);i.scrollIntoView(d,b)}}if(a&&
j.uCanDo(d)){b=d.view.selectedMulti&&b;if(a.parentTId)i.expandCollapseParentNode(d,a.getParentNode(),!0,!1,e);else if(!c)try{l(a,d).focus().blur()}catch(f){}i.selectNode(d,a,b)}},transformTozTreeNodes:function(a){return e.transformTozTreeFormat(d,a)},transformToArray:function(a){return e.transformToArrayFormat(d,a)},updateNode:function(a){a&&l(a,d).get(0)&&j.uCanDo(d)&&(i.setNodeName(d,a),i.setNodeTarget(d,a),i.setNodeUrl(d,a),i.setNodeLineIcos(d,a),i.setNodeFontCss(d,a))}};a.treeTools=g;e.setZTreeTools(d,
g);(c=e.nodeChildren(d,a))&&c.length>0?i.createNodes(d,0,c,null,-1):d.async.enable&&d.async.url&&d.async.url!==""&&i.asyncNode(d);return g}};var O=q.fn.zTree,l=j.$,f=O.consts})(jQuery);

/*
 * JQuery zTree excheck v3.5.36
 * http://treejs.cn/
 *
 * Copyright (c) 2010 Hunter.z
 *
 * Licensed same as jquery - MIT License
 * http://www.opensource.org/licenses/mit-license.php
 *
 * email: hunter.z@263.net
 * Date: 2018-06-26
 */
(function(n){var q,r,s,p={event:{CHECK:"ztree_check"},id:{CHECK:"_check"},checkbox:{STYLE:"checkbox",DEFAULT:"chk",DISABLED:"disable",FALSE:"false",TRUE:"true",FULL:"full",PART:"part",FOCUS:"focus"},radio:{STYLE:"radio",TYPE_ALL:"all",TYPE_LEVEL:"level"}},w={check:{enable:!1,autoCheckTrigger:!1,chkStyle:p.checkbox.STYLE,nocheckInherit:!1,chkDisabledInherit:!1,radioType:p.radio.TYPE_LEVEL,chkboxType:{Y:"ps",N:"ps"}},data:{key:{checked:"checked"}},callback:{beforeCheck:null,onCheck:null}};q=function(c,
a){if(a.chkDisabled===!0)return!1;var b=e.getSetting(c.data.treeId);if(i.apply(b.callback.beforeCheck,[b.treeId,a],!0)==!1)return!0;var d=e.nodeChecked(b,a);e.nodeChecked(b,a,!d);f.checkNodeRelation(b,a);d=m(a,h.id.CHECK,b);f.setChkClass(b,d,a);f.repairParentChkClassWithSelf(b,a);b.treeObj.trigger(h.event.CHECK,[c,b.treeId,a]);return!0};r=function(c,a){if(a.chkDisabled===!0)return!1;var b=e.getSetting(c.data.treeId),d=m(a,h.id.CHECK,b);a.check_Focus=!0;f.setChkClass(b,d,a);return!0};s=function(c,
a){if(a.chkDisabled===!0)return!1;var b=e.getSetting(c.data.treeId),d=m(a,h.id.CHECK,b);a.check_Focus=!1;f.setChkClass(b,d,a);return!0};n.extend(!0,n.fn.zTree.consts,p);n.extend(!0,n.fn.zTree._z,{tools:{},view:{checkNodeRelation:function(c,a){var b,d,j;d=h.radio;b=e.nodeChecked(c,a);if(c.check.chkStyle==d.STYLE){var g=e.getRadioCheckedList(c);if(b)if(c.check.radioType==d.TYPE_ALL){for(d=g.length-1;d>=0;d--){b=g[d];var k=e.nodeChecked(c,b);k&&b!=a&&(e.nodeChecked(c,b,!1),g.splice(d,1),f.setChkClass(c,
m(b,h.id.CHECK,c),b),b.parentTId!=a.parentTId&&f.repairParentChkClassWithSelf(c,b))}g.push(a)}else{g=a.parentTId?a.getParentNode():e.getRoot(c);g=e.nodeChildren(c,g);for(d=0,j=g.length;d<j;d++)if(b=g[d],(k=e.nodeChecked(c,b))&&b!=a)e.nodeChecked(c,b,!1),f.setChkClass(c,m(b,h.id.CHECK,c),b)}else if(c.check.radioType==d.TYPE_ALL)for(d=0,j=g.length;d<j;d++)if(a==g[d]){g.splice(d,1);break}}else g=e.nodeChildren(c,a),b&&(!g||g.length==0||c.check.chkboxType.Y.indexOf("s")>-1)&&f.setSonNodeCheckBox(c,a,
!0),!b&&(!g||g.length==0||c.check.chkboxType.N.indexOf("s")>-1)&&f.setSonNodeCheckBox(c,a,!1),b&&c.check.chkboxType.Y.indexOf("p")>-1&&f.setParentNodeCheckBox(c,a,!0),!b&&c.check.chkboxType.N.indexOf("p")>-1&&f.setParentNodeCheckBox(c,a,!1)},makeChkClass:function(c,a){var b=h.checkbox,d=h.radio,j="",g=e.nodeChecked(c,a),j=a.chkDisabled===!0?b.DISABLED:a.halfCheck?b.PART:c.check.chkStyle==d.STYLE?a.check_Child_State<1?b.FULL:b.PART:g?a.check_Child_State===2||a.check_Child_State===-1?b.FULL:b.PART:
a.check_Child_State<1?b.FULL:b.PART,d=c.check.chkStyle+"_"+(g?b.TRUE:b.FALSE)+"_"+j,d=a.check_Focus&&a.chkDisabled!==!0?d+"_"+b.FOCUS:d;return h.className.BUTTON+" "+b.DEFAULT+" "+d},repairAllChk:function(c,a){if(c.check.enable&&c.check.chkStyle===h.checkbox.STYLE)for(var b=e.getRoot(c),b=e.nodeChildren(c,b),d=0,j=b.length;d<j;d++){var g=b[d];g.nocheck!==!0&&g.chkDisabled!==!0&&e.nodeChecked(c,g,a);f.setSonNodeCheckBox(c,g,a)}},repairChkClass:function(c,a){if(a&&(e.makeChkFlag(c,a),a.nocheck!==!0)){var b=
m(a,h.id.CHECK,c);f.setChkClass(c,b,a)}},repairParentChkClass:function(c,a){if(a&&a.parentTId){var b=a.getParentNode();f.repairChkClass(c,b);f.repairParentChkClass(c,b)}},repairParentChkClassWithSelf:function(c,a){if(a){var b=e.nodeChildren(c,a);b&&b.length>0?f.repairParentChkClass(c,b[0]):f.repairParentChkClass(c,a)}},repairSonChkDisabled:function(c,a,b,d){if(a){if(a.chkDisabled!=b)a.chkDisabled=b;f.repairChkClass(c,a);if((a=e.nodeChildren(c,a))&&d)for(var j=0,g=a.length;j<g;j++)f.repairSonChkDisabled(c,
a[j],b,d)}},repairParentChkDisabled:function(c,a,b,d){if(a){if(a.chkDisabled!=b&&d)a.chkDisabled=b;f.repairChkClass(c,a);f.repairParentChkDisabled(c,a.getParentNode(),b,d)}},setChkClass:function(c,a,b){a&&(b.nocheck===!0?a.hide():a.show(),a.attr("class",f.makeChkClass(c,b)))},setParentNodeCheckBox:function(c,a,b,d){var j=m(a,h.id.CHECK,c);d||(d=a);e.makeChkFlag(c,a);a.nocheck!==!0&&a.chkDisabled!==!0&&(e.nodeChecked(c,a,b),f.setChkClass(c,j,a),c.check.autoCheckTrigger&&a!=d&&c.treeObj.trigger(h.event.CHECK,
[null,c.treeId,a]));if(a.parentTId){j=!0;if(!b)for(var g=e.nodeChildren(c,a.getParentNode()),k=0,o=g.length;k<o;k++){var l=g[k],i=e.nodeChecked(c,l);if(l.nocheck!==!0&&l.chkDisabled!==!0&&i||(l.nocheck===!0||l.chkDisabled===!0)&&l.check_Child_State>0){j=!1;break}}j&&f.setParentNodeCheckBox(c,a.getParentNode(),b,d)}},setSonNodeCheckBox:function(c,a,b,d){if(a){var j=m(a,h.id.CHECK,c);d||(d=a);var g=!1,k=e.nodeChildren(c,a);if(k)for(var o=0,l=k.length;o<l;o++){var i=k[o];f.setSonNodeCheckBox(c,i,b,d);
i.chkDisabled===!0&&(g=!0)}if(a!=e.getRoot(c)&&a.chkDisabled!==!0){g&&a.nocheck!==!0&&e.makeChkFlag(c,a);if(a.nocheck!==!0&&a.chkDisabled!==!0){if(e.nodeChecked(c,a,b),!g)a.check_Child_State=k&&k.length>0?b?2:0:-1}else a.check_Child_State=-1;f.setChkClass(c,j,a);c.check.autoCheckTrigger&&a!=d&&a.nocheck!==!0&&a.chkDisabled!==!0&&c.treeObj.trigger(h.event.CHECK,[null,c.treeId,a])}}}},event:{},data:{getRadioCheckedList:function(c){for(var a=e.getRoot(c).radioCheckedList,b=0,d=a.length;b<d;b++)e.getNodeCache(c,
a[b].tId)||(a.splice(b,1),b--,d--);return a},getCheckStatus:function(c,a){if(!c.check.enable||a.nocheck||a.chkDisabled)return null;var b=e.nodeChecked(c,a);return{checked:b,half:a.halfCheck?a.halfCheck:c.check.chkStyle==h.radio.STYLE?a.check_Child_State===2:b?a.check_Child_State>-1&&a.check_Child_State<2:a.check_Child_State>0}},getTreeCheckedNodes:function(c,a,b,d){if(!a)return[];for(var j=b&&c.check.chkStyle==h.radio.STYLE&&c.check.radioType==h.radio.TYPE_ALL,d=!d?[]:d,g=0,f=a.length;g<f;g++){var i=
a[g],l=e.nodeChildren(c,i),m=e.nodeChecked(c,i);if(i.nocheck!==!0&&i.chkDisabled!==!0&&m==b&&(d.push(i),j))break;e.getTreeCheckedNodes(c,l,b,d);if(j&&d.length>0)break}return d},getTreeChangeCheckedNodes:function(c,a,b){if(!a)return[];for(var b=!b?[]:b,d=0,j=a.length;d<j;d++){var g=a[d],f=e.nodeChildren(c,g),h=e.nodeChecked(c,g);g.nocheck!==!0&&g.chkDisabled!==!0&&h!=g.checkedOld&&b.push(g);e.getTreeChangeCheckedNodes(c,f,b)}return b},makeChkFlag:function(c,a){if(a){var b=-1,d=e.nodeChildren(c,a);
if(d)for(var j=0,g=d.length;j<g;j++){var f=d[j],i=e.nodeChecked(c,f),l=-1;if(c.check.chkStyle==h.radio.STYLE)if(l=f.nocheck===!0||f.chkDisabled===!0?f.check_Child_State:f.halfCheck===!0?2:i?2:f.check_Child_State>0?2:0,l==2){b=2;break}else l==0&&(b=0);else if(c.check.chkStyle==h.checkbox.STYLE)if(l=f.nocheck===!0||f.chkDisabled===!0?f.check_Child_State:f.halfCheck===!0?1:i?f.check_Child_State===-1||f.check_Child_State===2?2:1:f.check_Child_State>0?1:0,l===1){b=1;break}else if(l===2&&b>-1&&j>0&&l!==
b){b=1;break}else if(b===2&&l>-1&&l<2){b=1;break}else l>-1&&(b=l)}a.check_Child_State=b}}}});var n=n.fn.zTree,i=n._z.tools,h=n.consts,f=n._z.view,e=n._z.data,m=i.$;e.nodeChecked=function(c,a,b){if(!a)return!1;c=c.data.key.checked;typeof b!=="undefined"&&(typeof b==="string"&&(b=i.eqs(b,"true")),a[c]=!!b);return a[c]};e.exSetting(w);e.addInitBind(function(c){c.treeObj.bind(h.event.CHECK,function(a,b,d,e){a.srcEvent=b;i.apply(c.callback.onCheck,[a,d,e])})});e.addInitUnBind(function(c){c.treeObj.unbind(h.event.CHECK)});
e.addInitCache(function(){});e.addInitNode(function(c,a,b,d){if(b){a=e.nodeChecked(c,b);a=e.nodeChecked(c,b,a);b.checkedOld=a;if(typeof b.nocheck=="string")b.nocheck=i.eqs(b.nocheck,"true");b.nocheck=!!b.nocheck||c.check.nocheckInherit&&d&&!!d.nocheck;if(typeof b.chkDisabled=="string")b.chkDisabled=i.eqs(b.chkDisabled,"true");b.chkDisabled=!!b.chkDisabled||c.check.chkDisabledInherit&&d&&!!d.chkDisabled;if(typeof b.halfCheck=="string")b.halfCheck=i.eqs(b.halfCheck,"true");b.halfCheck=!!b.halfCheck;
b.check_Child_State=-1;b.check_Focus=!1;b.getCheckStatus=function(){return e.getCheckStatus(c,b)};c.check.chkStyle==h.radio.STYLE&&c.check.radioType==h.radio.TYPE_ALL&&a&&e.getRoot(c).radioCheckedList.push(b)}});e.addInitProxy(function(c){var a=c.target,b=e.getSetting(c.data.treeId),d="",f=null,g="",k=null;if(i.eqs(c.type,"mouseover")){if(b.check.enable&&i.eqs(a.tagName,"span")&&a.getAttribute("treeNode"+h.id.CHECK)!==null)d=i.getNodeMainDom(a).id,g="mouseoverCheck"}else if(i.eqs(c.type,"mouseout")){if(b.check.enable&&
i.eqs(a.tagName,"span")&&a.getAttribute("treeNode"+h.id.CHECK)!==null)d=i.getNodeMainDom(a).id,g="mouseoutCheck"}else if(i.eqs(c.type,"click")&&b.check.enable&&i.eqs(a.tagName,"span")&&a.getAttribute("treeNode"+h.id.CHECK)!==null)d=i.getNodeMainDom(a).id,g="checkNode";if(d.length>0)switch(f=e.getNodeCache(b,d),g){case "checkNode":k=q;break;case "mouseoverCheck":k=r;break;case "mouseoutCheck":k=s}return{stop:g==="checkNode",node:f,nodeEventType:g,nodeEventCallback:k,treeEventType:"",treeEventCallback:null}},
!0);e.addInitRoot(function(c){e.getRoot(c).radioCheckedList=[]});e.addBeforeA(function(c,a,b){c.check.enable&&(e.makeChkFlag(c,a),b.push("<span ID='",a.tId,h.id.CHECK,"' class='",f.makeChkClass(c,a),"' treeNode",h.id.CHECK,a.nocheck===!0?" style='display:none;'":"","></span>"))});e.addZTreeTools(function(c,a){a.checkNode=function(a,b,g,k){var o=e.nodeChecked(c,a);if(a.chkDisabled!==!0&&(b!==!0&&b!==!1&&(b=!o),k=!!k,(o!==b||g)&&!(k&&i.apply(this.setting.callback.beforeCheck,[this.setting.treeId,a],
!0)==!1)&&i.uCanDo(this.setting)&&this.setting.check.enable&&a.nocheck!==!0))e.nodeChecked(c,a,b),b=m(a,h.id.CHECK,this.setting),(g||this.setting.check.chkStyle===h.radio.STYLE)&&f.checkNodeRelation(this.setting,a),f.setChkClass(this.setting,b,a),f.repairParentChkClassWithSelf(this.setting,a),k&&this.setting.treeObj.trigger(h.event.CHECK,[null,this.setting.treeId,a])};a.checkAllNodes=function(a){f.repairAllChk(this.setting,!!a)};a.getCheckedNodes=function(a){var a=a!==!1,b=e.nodeChildren(c,e.getRoot(this.setting));
return e.getTreeCheckedNodes(this.setting,b,a)};a.getChangeCheckedNodes=function(){var a=e.nodeChildren(c,e.getRoot(this.setting));return e.getTreeChangeCheckedNodes(this.setting,a)};a.setChkDisabled=function(a,b,c,e){b=!!b;c=!!c;f.repairSonChkDisabled(this.setting,a,b,!!e);f.repairParentChkDisabled(this.setting,a.getParentNode(),b,c)};var b=a.updateNode;a.updateNode=function(c,e){b&&b.apply(a,arguments);if(c&&this.setting.check.enable&&m(c,this.setting).get(0)&&i.uCanDo(this.setting)){var g=m(c,
h.id.CHECK,this.setting);(e==!0||this.setting.check.chkStyle===h.radio.STYLE)&&f.checkNodeRelation(this.setting,c);f.setChkClass(this.setting,g,c);f.repairParentChkClassWithSelf(this.setting,c)}}});var t=f.createNodes;f.createNodes=function(c,a,b,d,e){t&&t.apply(f,arguments);b&&f.repairParentChkClassWithSelf(c,d)};var u=f.removeNode;f.removeNode=function(c,a){var b=a.getParentNode();u&&u.apply(f,arguments);a&&b&&(f.repairChkClass(c,b),f.repairParentChkClass(c,b))};var v=f.appendNodes;f.appendNodes=
function(c,a,b,d,h,g,i){var m="";v&&(m=v.apply(f,arguments));d&&e.makeChkFlag(c,d);return m}})(jQuery);

/*
 * JQuery zTree exedit v3.5.36
 * http://treejs.cn/
 *
 * Copyright (c) 2010 Hunter.z
 *
 * Licensed same as jquery - MIT License
 * http://www.opensource.org/licenses/mit-license.php
 *
 * email: hunter.z@263.net
 * Date: 2018-06-26
 */
(function(B){var I={event:{DRAG:"ztree_drag",DROP:"ztree_drop",RENAME:"ztree_rename",DRAGMOVE:"ztree_dragmove"},id:{EDIT:"_edit",INPUT:"_input",REMOVE:"_remove"},move:{TYPE_INNER:"inner",TYPE_PREV:"prev",TYPE_NEXT:"next"},node:{CURSELECTED_EDIT:"curSelectedNode_Edit",TMPTARGET_TREE:"tmpTargetzTree",TMPTARGET_NODE:"tmpTargetNode"}},v={onHoverOverNode:function(a,b){var c=i.getSetting(a.data.treeId),d=i.getRoot(c);if(d.curHoverNode!=b)v.onHoverOutNode(a);d.curHoverNode=b;e.addHoverDom(c,b)},onHoverOutNode:function(a){var a=
i.getSetting(a.data.treeId),b=i.getRoot(a);if(b.curHoverNode&&!i.isSelectedNode(a,b.curHoverNode))e.removeTreeDom(a,b.curHoverNode),b.curHoverNode=null},onMousedownNode:function(a,b){function c(a){if(m.dragFlag==0&&Math.abs(N-a.clientX)<f.edit.drag.minMoveSize&&Math.abs(O-a.clientY)<f.edit.drag.minMoveSize)return!0;var b,c,g,j;L.css("cursor","pointer");if(m.dragFlag==0){if(k.apply(f.callback.beforeDrag,[f.treeId,n],!0)==!1)return l(a),!0;for(b=0,c=n.length;b<c;b++){if(b==0)m.dragNodeShowBefore=[];
g=n[b];i.nodeIsParent(f,g)&&g.open?(e.expandCollapseNode(f,g,!g.open),m.dragNodeShowBefore[g.tId]=!0):m.dragNodeShowBefore[g.tId]=!1}m.dragFlag=1;y.showHoverDom=!1;k.showIfameMask(f,!0);j=!0;var p=-1;if(n.length>1){var o=n[0].parentTId?i.nodeChildren(f,n[0].getParentNode()):i.getNodes(f);g=[];for(b=0,c=o.length;b<c;b++)if(m.dragNodeShowBefore[o[b].tId]!==void 0&&(j&&p>-1&&p+1!==b&&(j=!1),g.push(o[b]),p=b),n.length===g.length){n=g;break}}j&&(H=n[0].getPreNode(),Q=n[n.length-1].getNextNode());C=q("<ul class='zTreeDragUL'></ul>",
f);for(b=0,c=n.length;b<c;b++)g=n[b],g.editNameFlag=!1,e.selectNode(f,g,b>0),e.removeTreeDom(f,g),b>f.edit.drag.maxShowNodeNum-1||(j=q("<li id='"+g.tId+"_tmp'></li>",f),j.append(q(g,d.id.A,f).clone()),j.css("padding","0"),j.children("#"+g.tId+d.id.A).removeClass(d.node.CURSELECTED),C.append(j),b==f.edit.drag.maxShowNodeNum-1&&(j=q("<li id='"+g.tId+"_moretmp'><a>  ...  </a></li>",f),C.append(j)));C.attr("id",n[0].tId+d.id.UL+"_tmp");C.addClass(f.treeObj.attr("class"));C.appendTo(L);u=q("<span class='tmpzTreeMove_arrow'></span>",
f);u.attr("id","zTreeMove_arrow_tmp");u.appendTo(L);f.treeObj.trigger(d.event.DRAG,[a,f.treeId,n])}if(m.dragFlag==1){t&&u.attr("id")==a.target.id&&w&&a.clientX+G.scrollLeft()+2>B("#"+w+d.id.A,t).offset().left?(g=B("#"+w+d.id.A,t),a.target=g.length>0?g.get(0):a.target):t&&(t.removeClass(d.node.TMPTARGET_TREE),w&&B("#"+w+d.id.A,t).removeClass(d.node.TMPTARGET_NODE+"_"+d.move.TYPE_PREV).removeClass(d.node.TMPTARGET_NODE+"_"+I.move.TYPE_NEXT).removeClass(d.node.TMPTARGET_NODE+"_"+I.move.TYPE_INNER));
w=t=null;J=!1;h=f;g=i.getSettings();for(var z in g)if(g[z].treeId&&g[z].edit.enable&&g[z].treeId!=f.treeId&&(a.target.id==g[z].treeId||B(a.target).parents("#"+g[z].treeId).length>0))J=!0,h=g[z];z=G.scrollTop();j=G.scrollLeft();p=h.treeObj.offset();b=h.treeObj.get(0).scrollHeight;g=h.treeObj.get(0).scrollWidth;c=a.clientY+z-p.top;var E=h.treeObj.height()+p.top-a.clientY-z,r=a.clientX+j-p.left,s=h.treeObj.width()+p.left-a.clientX-j,p=c<f.edit.drag.borderMax&&c>f.edit.drag.borderMin,o=E<f.edit.drag.borderMax&&
E>f.edit.drag.borderMin,F=r<f.edit.drag.borderMax&&r>f.edit.drag.borderMin,v=s<f.edit.drag.borderMax&&s>f.edit.drag.borderMin,E=c>f.edit.drag.borderMin&&E>f.edit.drag.borderMin&&r>f.edit.drag.borderMin&&s>f.edit.drag.borderMin,r=p&&h.treeObj.scrollTop()<=0,s=o&&h.treeObj.scrollTop()+h.treeObj.height()+10>=b,M=F&&h.treeObj.scrollLeft()<=0,P=v&&h.treeObj.scrollLeft()+h.treeObj.width()+10>=g;if(a.target&&k.isChildOrSelf(a.target,h.treeId)){for(var D=a.target;D&&D.tagName&&!k.eqs(D.tagName,"li")&&D.id!=
h.treeId;)D=D.parentNode;var R=!0;for(b=0,c=n.length;b<c;b++)if(g=n[b],D.id===g.tId){R=!1;break}else if(q(g,f).find("#"+D.id).length>0){R=!1;break}if(R&&a.target&&k.isChildOrSelf(a.target,D.id+d.id.A))t=B(D),w=D.id}g=n[0];if(E&&k.isChildOrSelf(a.target,h.treeId)){if(!t&&(a.target.id==h.treeId||r||s||M||P)&&(J||!J&&g.parentTId))t=h.treeObj;p?h.treeObj.scrollTop(h.treeObj.scrollTop()-10):o&&h.treeObj.scrollTop(h.treeObj.scrollTop()+10);F?h.treeObj.scrollLeft(h.treeObj.scrollLeft()-10):v&&h.treeObj.scrollLeft(h.treeObj.scrollLeft()+
10);t&&t!=h.treeObj&&t.offset().left<h.treeObj.offset().left&&h.treeObj.scrollLeft(h.treeObj.scrollLeft()+t.offset().left-h.treeObj.offset().left)}C.css({top:a.clientY+z+3+"px",left:a.clientX+j+3+"px"});b=j=0;if(t&&t.attr("id")!=h.treeId){var A=w==null?null:i.getNodeCache(h,w),p=(a.ctrlKey||a.metaKey)&&f.edit.drag.isMove&&f.edit.drag.isCopy||!f.edit.drag.isMove&&f.edit.drag.isCopy;c=!!(H&&w===H.tId);F=!!(Q&&w===Q.tId);o=g.parentTId&&g.parentTId==w;g=(p||!F)&&k.apply(h.edit.drag.prev,[h.treeId,n,A],
!!h.edit.drag.prev);c=(p||!c)&&k.apply(h.edit.drag.next,[h.treeId,n,A],!!h.edit.drag.next);p=(p||!o)&&!(h.data.keep.leaf&&!i.nodeIsParent(f,A))&&k.apply(h.edit.drag.inner,[h.treeId,n,A],!!h.edit.drag.inner);o=function(){t=null;w="";x=d.move.TYPE_INNER;u.css({display:"none"});if(window.zTreeMoveTimer)clearTimeout(window.zTreeMoveTimer),window.zTreeMoveTargetNodeTId=null};if(!g&&!c&&!p)o();else if(F=B("#"+w+d.id.A,t),v=A.isLastNode?null:B("#"+A.getNextNode().tId+d.id.A,t.next()),E=F.offset().top,r=
F.offset().left,s=g?p?0.25:c?0.5:1:-1,M=c?p?0.75:g?0.5:0:-1,z=(a.clientY+z-E)/F.height(),(s==1||z<=s&&z>=-0.2)&&g?(j=1-u.width(),b=E-u.height()/2,x=d.move.TYPE_PREV):(M==0||z>=M&&z<=1.2)&&c?(j=1-u.width(),b=v==null||i.nodeIsParent(f,A)&&A.open?E+F.height()-u.height()/2:v.offset().top-u.height()/2,x=d.move.TYPE_NEXT):p?(j=5-u.width(),b=E,x=d.move.TYPE_INNER):o(),t){u.css({display:"block",top:b+"px",left:r+j+"px"});F.addClass(d.node.TMPTARGET_NODE+"_"+x);if(S!=w||T!=x)K=(new Date).getTime();if(A&&i.nodeIsParent(f,
A)&&x==d.move.TYPE_INNER&&(z=!0,window.zTreeMoveTimer&&window.zTreeMoveTargetNodeTId!==A.tId?(clearTimeout(window.zTreeMoveTimer),window.zTreeMoveTargetNodeTId=null):window.zTreeMoveTimer&&window.zTreeMoveTargetNodeTId===A.tId&&(z=!1),z))window.zTreeMoveTimer=setTimeout(function(){x==d.move.TYPE_INNER&&A&&i.nodeIsParent(f,A)&&!A.open&&(new Date).getTime()-K>h.edit.drag.autoOpenTime&&k.apply(h.callback.beforeDragOpen,[h.treeId,A],!0)&&(e.switchNode(h,A),h.edit.drag.autoExpandTrigger&&h.treeObj.trigger(d.event.EXPAND,
[h.treeId,A]))},h.edit.drag.autoOpenTime+50),window.zTreeMoveTargetNodeTId=A.tId}}else if(x=d.move.TYPE_INNER,t&&k.apply(h.edit.drag.inner,[h.treeId,n,null],!!h.edit.drag.inner)?t.addClass(d.node.TMPTARGET_TREE):t=null,u.css({display:"none"}),window.zTreeMoveTimer)clearTimeout(window.zTreeMoveTimer),window.zTreeMoveTargetNodeTId=null;S=w;T=x;f.treeObj.trigger(d.event.DRAGMOVE,[a,f.treeId,n])}return!1}function l(a){if(window.zTreeMoveTimer)clearTimeout(window.zTreeMoveTimer),window.zTreeMoveTargetNodeTId=
null;T=S=null;G.unbind("mousemove",c);G.unbind("mouseup",l);G.unbind("selectstart",g);L.css("cursor","");t&&(t.removeClass(d.node.TMPTARGET_TREE),w&&B("#"+w+d.id.A,t).removeClass(d.node.TMPTARGET_NODE+"_"+d.move.TYPE_PREV).removeClass(d.node.TMPTARGET_NODE+"_"+I.move.TYPE_NEXT).removeClass(d.node.TMPTARGET_NODE+"_"+I.move.TYPE_INNER));k.showIfameMask(f,!1);y.showHoverDom=!0;if(m.dragFlag!=0){m.dragFlag=0;var b,j,o;for(b=0,j=n.length;b<j;b++)o=n[b],i.nodeIsParent(f,o)&&m.dragNodeShowBefore[o.tId]&&
!o.open&&(e.expandCollapseNode(f,o,!o.open),delete m.dragNodeShowBefore[o.tId]);C&&C.remove();u&&u.remove();var r=(a.ctrlKey||a.metaKey)&&f.edit.drag.isMove&&f.edit.drag.isCopy||!f.edit.drag.isMove&&f.edit.drag.isCopy;!r&&t&&w&&n[0].parentTId&&w==n[0].parentTId&&x==d.move.TYPE_INNER&&(t=null);if(t){var p=w==null?null:i.getNodeCache(h,w);if(k.apply(f.callback.beforeDrop,[h.treeId,n,p,x,r],!0)==!1)e.selectNodes(v,n);else{var s=r?k.clone(n):n;b=function(){if(J){if(!r)for(var b=0,c=n.length;b<c;b++)e.removeNode(f,
n[b]);x==d.move.TYPE_INNER?e.addNodes(h,p,-1,s):e.addNodes(h,p.getParentNode(),x==d.move.TYPE_PREV?p.getIndex():p.getIndex()+1,s)}else if(r&&x==d.move.TYPE_INNER)e.addNodes(h,p,-1,s);else if(r)e.addNodes(h,p.getParentNode(),x==d.move.TYPE_PREV?p.getIndex():p.getIndex()+1,s);else if(x!=d.move.TYPE_NEXT)for(b=0,c=s.length;b<c;b++)e.moveNode(h,p,s[b],x,!1);else for(b=-1,c=s.length-1;b<c;c--)e.moveNode(h,p,s[c],x,!1);e.selectNodes(h,s);b=q(s[0],f).get(0);e.scrollIntoView(f,b);f.treeObj.trigger(d.event.DROP,
[a,h.treeId,s,p,x,r])};x==d.move.TYPE_INNER&&k.canAsync(h,p)?e.asyncNode(h,p,!1,b):b()}}else e.selectNodes(v,n),f.treeObj.trigger(d.event.DROP,[a,f.treeId,n,null,null,null])}}function g(){return!1}var o,j,f=i.getSetting(a.data.treeId),m=i.getRoot(f),y=i.getRoots();if(a.button==2||!f.edit.enable||!f.edit.drag.isCopy&&!f.edit.drag.isMove)return!0;var r=a.target,s=i.getRoot(f).curSelectedList,n=[];if(i.isSelectedNode(f,b))for(o=0,j=s.length;o<j;o++){if(s[o].editNameFlag&&k.eqs(r.tagName,"input")&&r.getAttribute("treeNode"+
d.id.INPUT)!==null)return!0;n.push(s[o]);if(n[0].parentTId!==s[o].parentTId){n=[b];break}}else n=[b];e.editNodeBlur=!0;e.cancelCurEditNode(f);var G=B(f.treeObj.get(0).ownerDocument),L=B(f.treeObj.get(0).ownerDocument.body),C,u,t,J=!1,h=f,v=f,H,Q,S=null,T=null,w=null,x=d.move.TYPE_INNER,N=a.clientX,O=a.clientY,K=(new Date).getTime();k.uCanDo(f)&&G.bind("mousemove",c);G.bind("mouseup",l);G.bind("selectstart",g);return!0}};B.extend(!0,B.fn.zTree.consts,I);B.extend(!0,B.fn.zTree._z,{tools:{getAbs:function(a){a=
a.getBoundingClientRect();return[a.left+(document.body.scrollLeft+document.documentElement.scrollLeft),a.top+(document.body.scrollTop+document.documentElement.scrollTop)]},inputFocus:function(a){a.get(0)&&(a.focus(),k.setCursorPosition(a.get(0),a.val().length))},inputSelect:function(a){a.get(0)&&(a.focus(),a.select())},setCursorPosition:function(a,b){if(a.setSelectionRange)a.focus(),a.setSelectionRange(b,b);else if(a.createTextRange){var c=a.createTextRange();c.collapse(!0);c.moveEnd("character",
b);c.moveStart("character",b);c.select()}},showIfameMask:function(a,b){for(var c=i.getRoot(a);c.dragMaskList.length>0;)c.dragMaskList[0].remove(),c.dragMaskList.shift();if(b)for(var d=q("iframe",a),g=0,e=d.length;g<e;g++){var j=d.get(g),f=k.getAbs(j),j=q("<div id='zTreeMask_"+g+"' class='zTreeMask' style='top:"+f[1]+"px; left:"+f[0]+"px; width:"+j.offsetWidth+"px; height:"+j.offsetHeight+"px;'></div>",a);j.appendTo(q("body",a));c.dragMaskList.push(j)}}},view:{addEditBtn:function(a,b){if(!(b.editNameFlag||
q(b,d.id.EDIT,a).length>0)&&k.apply(a.edit.showRenameBtn,[a.treeId,b],a.edit.showRenameBtn)){var c=q(b,d.id.A,a),l="<span class='"+d.className.BUTTON+" edit' id='"+b.tId+d.id.EDIT+"' title='"+k.apply(a.edit.renameTitle,[a.treeId,b],a.edit.renameTitle)+"' treeNode"+d.id.EDIT+" style='display:none;'></span>";c.append(l);q(b,d.id.EDIT,a).bind("click",function(){if(!k.uCanDo(a)||k.apply(a.callback.beforeEditName,[a.treeId,b],!0)==!1)return!1;e.editNode(a,b);return!1}).show()}},addRemoveBtn:function(a,
b){if(!(b.editNameFlag||q(b,d.id.REMOVE,a).length>0)&&k.apply(a.edit.showRemoveBtn,[a.treeId,b],a.edit.showRemoveBtn)){var c=q(b,d.id.A,a),l="<span class='"+d.className.BUTTON+" remove' id='"+b.tId+d.id.REMOVE+"' title='"+k.apply(a.edit.removeTitle,[a.treeId,b],a.edit.removeTitle)+"' treeNode"+d.id.REMOVE+" style='display:none;'></span>";c.append(l);q(b,d.id.REMOVE,a).bind("click",function(){if(!k.uCanDo(a)||k.apply(a.callback.beforeRemove,[a.treeId,b],!0)==!1)return!1;e.removeNode(a,b);a.treeObj.trigger(d.event.REMOVE,
[a.treeId,b]);return!1}).bind("mousedown",function(){return!0}).show()}},addHoverDom:function(a,b){if(i.getRoots().showHoverDom)b.isHover=!0,a.edit.enable&&(e.addEditBtn(a,b),e.addRemoveBtn(a,b)),k.apply(a.view.addHoverDom,[a.treeId,b])},cancelCurEditNode:function(a,b,c){var l=i.getRoot(a),g=l.curEditNode;if(g){var o=l.curEditInput,b=b?b:c?i.nodeName(a,g):o.val();if(k.apply(a.callback.beforeRename,[a.treeId,g,b,c],!0)===!1)return!1;i.nodeName(a,g,b);q(g,d.id.A,a).removeClass(d.node.CURSELECTED_EDIT);
o.unbind();e.setNodeName(a,g);g.editNameFlag=!1;l.curEditNode=null;l.curEditInput=null;e.selectNode(a,g,!1);a.treeObj.trigger(d.event.RENAME,[a.treeId,g,c])}return l.noSelection=!0},editNode:function(a,b){var c=i.getRoot(a);e.editNodeBlur=!1;if(i.isSelectedNode(a,b)&&c.curEditNode==b&&b.editNameFlag)setTimeout(function(){k.inputFocus(c.curEditInput)},0);else{b.editNameFlag=!0;e.removeTreeDom(a,b);e.cancelCurEditNode(a);e.selectNode(a,b,!1);q(b,d.id.SPAN,a).html("<input type=text class='rename' id='"+
b.tId+d.id.INPUT+"' treeNode"+d.id.INPUT+" >");var l=q(b,d.id.INPUT,a);l.attr("value",i.nodeName(a,b));a.edit.editNameSelectAll?k.inputSelect(l):k.inputFocus(l);l.bind("blur",function(){e.editNodeBlur||e.cancelCurEditNode(a)}).bind("keydown",function(b){b.keyCode=="13"?(e.editNodeBlur=!0,e.cancelCurEditNode(a)):b.keyCode=="27"&&e.cancelCurEditNode(a,null,!0)}).bind("click",function(){return!1}).bind("dblclick",function(){return!1});q(b,d.id.A,a).addClass(d.node.CURSELECTED_EDIT);c.curEditInput=l;
c.noSelection=!1;c.curEditNode=b}},moveNode:function(a,b,c,l,g,k){var j=i.getRoot(a);if(b!=c&&(!a.data.keep.leaf||!b||i.nodeIsParent(a,b)||l!=d.move.TYPE_INNER)){var f=c.parentTId?c.getParentNode():j,m=b===null||b==j;m&&b===null&&(b=j);if(m)l=d.move.TYPE_INNER;j=b.parentTId?b.getParentNode():j;if(l!=d.move.TYPE_PREV&&l!=d.move.TYPE_NEXT)l=d.move.TYPE_INNER;if(l==d.move.TYPE_INNER)if(m)c.parentTId=null;else{if(!i.nodeIsParent(a,b))i.nodeIsParent(a,b,!0),b.open=!!b.open,e.setNodeLineIcos(a,b);c.parentTId=
b.tId}var y;m?y=m=a.treeObj:(!k&&l==d.move.TYPE_INNER?e.expandCollapseNode(a,b,!0,!1):k||e.expandCollapseNode(a,b.getParentNode(),!0,!1),m=q(b,a),y=q(b,d.id.UL,a),m.get(0)&&!y.get(0)&&(y=[],e.makeUlHtml(a,b,y,""),m.append(y.join(""))),y=q(b,d.id.UL,a));var r=q(c,a);r.get(0)?m.get(0)||r.remove():r=e.appendNodes(a,c.level,[c],null,-1,!1,!0).join("");y.get(0)&&l==d.move.TYPE_INNER?y.append(r):m.get(0)&&l==d.move.TYPE_PREV?m.before(r):m.get(0)&&l==d.move.TYPE_NEXT&&m.after(r);var s;y=-1;var r=0,n=null,
m=null,B=c.level,v=i.nodeChildren(a,f),C=i.nodeChildren(a,j),u=i.nodeChildren(a,b);if(c.isFirstNode){if(y=0,v.length>1)n=v[1],n.isFirstNode=!0}else if(c.isLastNode)y=v.length-1,n=v[y-1],n.isLastNode=!0;else for(j=0,s=v.length;j<s;j++)if(v[j].tId==c.tId){y=j;break}y>=0&&v.splice(y,1);if(l!=d.move.TYPE_INNER)for(j=0,s=C.length;j<s;j++)C[j].tId==b.tId&&(r=j);if(l==d.move.TYPE_INNER){u||(u=i.nodeChildren(a,b,[]));if(u.length>0)m=u[u.length-1],m.isLastNode=!1;u.splice(u.length,0,c);c.isLastNode=!0;c.isFirstNode=
u.length==1}else b.isFirstNode&&l==d.move.TYPE_PREV?(C.splice(r,0,c),m=b,m.isFirstNode=!1,c.parentTId=b.parentTId,c.isFirstNode=!0,c.isLastNode=!1):b.isLastNode&&l==d.move.TYPE_NEXT?(C.splice(r+1,0,c),m=b,m.isLastNode=!1,c.parentTId=b.parentTId,c.isFirstNode=!1,c.isLastNode=!0):(l==d.move.TYPE_PREV?C.splice(r,0,c):C.splice(r+1,0,c),c.parentTId=b.parentTId,c.isFirstNode=!1,c.isLastNode=!1);i.fixPIdKeyValue(a,c);i.setSonNodeLevel(a,c.getParentNode(),c);e.setNodeLineIcos(a,c);e.repairNodeLevelClass(a,
c,B);!a.data.keep.parent&&v.length<1?(i.nodeIsParent(a,f,!1),f.open=!1,b=q(f,d.id.UL,a),l=q(f,d.id.SWITCH,a),j=q(f,d.id.ICON,a),e.replaceSwitchClass(f,l,d.folder.DOCU),e.replaceIcoClass(f,j,d.folder.DOCU),b.css("display","none")):n&&e.setNodeLineIcos(a,n);m&&e.setNodeLineIcos(a,m);a.check&&a.check.enable&&e.repairChkClass&&(e.repairChkClass(a,f),e.repairParentChkClassWithSelf(a,f),f!=c.parent&&e.repairParentChkClassWithSelf(a,c));k||e.expandCollapseParentNode(a,c.getParentNode(),!0,g)}},removeEditBtn:function(a,
b){q(b,d.id.EDIT,a).unbind().remove()},removeRemoveBtn:function(a,b){q(b,d.id.REMOVE,a).unbind().remove()},removeTreeDom:function(a,b){b.isHover=!1;e.removeEditBtn(a,b);e.removeRemoveBtn(a,b);k.apply(a.view.removeHoverDom,[a.treeId,b])},repairNodeLevelClass:function(a,b,c){if(c!==b.level){var e=q(b,a),g=q(b,d.id.A,a),a=q(b,d.id.UL,a),c=d.className.LEVEL+c,b=d.className.LEVEL+b.level;e.removeClass(c);e.addClass(b);g.removeClass(c);g.addClass(b);a.removeClass(c);a.addClass(b)}},selectNodes:function(a,
b){for(var c=0,d=b.length;c<d;c++)e.selectNode(a,b[c],c>0)}},event:{},data:{setSonNodeLevel:function(a,b,c){if(c){var d=i.nodeChildren(a,c);c.level=b?b.level+1:0;if(d)for(var b=0,g=d.length;b<g;b++)d[b]&&i.setSonNodeLevel(a,c,d[b])}}}});var H=B.fn.zTree,k=H._z.tools,d=H.consts,e=H._z.view,i=H._z.data,q=k.$;i.exSetting({edit:{enable:!1,editNameSelectAll:!1,showRemoveBtn:!0,showRenameBtn:!0,removeTitle:"remove",renameTitle:"rename",drag:{autoExpandTrigger:!1,isCopy:!0,isMove:!0,prev:!0,next:!0,inner:!0,
minMoveSize:5,borderMax:10,borderMin:-5,maxShowNodeNum:5,autoOpenTime:500}},view:{addHoverDom:null,removeHoverDom:null},callback:{beforeDrag:null,beforeDragOpen:null,beforeDrop:null,beforeEditName:null,beforeRename:null,onDrag:null,onDragMove:null,onDrop:null,onRename:null}});i.addInitBind(function(a){var b=a.treeObj,c=d.event;b.bind(c.RENAME,function(b,c,d,e){k.apply(a.callback.onRename,[b,c,d,e])});b.bind(c.DRAG,function(b,c,d,e){k.apply(a.callback.onDrag,[c,d,e])});b.bind(c.DRAGMOVE,function(b,
c,d,e){k.apply(a.callback.onDragMove,[c,d,e])});b.bind(c.DROP,function(b,c,d,e,f,i,q){k.apply(a.callback.onDrop,[c,d,e,f,i,q])})});i.addInitUnBind(function(a){var a=a.treeObj,b=d.event;a.unbind(b.RENAME);a.unbind(b.DRAG);a.unbind(b.DRAGMOVE);a.unbind(b.DROP)});i.addInitCache(function(){});i.addInitNode(function(a,b,c){if(c)c.isHover=!1,c.editNameFlag=!1});i.addInitProxy(function(a){var b=a.target,c=i.getSetting(a.data.treeId),e=a.relatedTarget,g="",o=null,j="",f=null,m=null;if(k.eqs(a.type,"mouseover")){if(m=
k.getMDom(c,b,[{tagName:"a",attrName:"treeNode"+d.id.A}]))g=k.getNodeMainDom(m).id,j="hoverOverNode"}else if(k.eqs(a.type,"mouseout"))m=k.getMDom(c,e,[{tagName:"a",attrName:"treeNode"+d.id.A}]),m||(g="remove",j="hoverOutNode");else if(k.eqs(a.type,"mousedown")&&(m=k.getMDom(c,b,[{tagName:"a",attrName:"treeNode"+d.id.A}])))g=k.getNodeMainDom(m).id,j="mousedownNode";if(g.length>0)switch(o=i.getNodeCache(c,g),j){case "mousedownNode":f=v.onMousedownNode;break;case "hoverOverNode":f=v.onHoverOverNode;
break;case "hoverOutNode":f=v.onHoverOutNode}return{stop:!1,node:o,nodeEventType:j,nodeEventCallback:f,treeEventType:"",treeEventCallback:null}});i.addInitRoot(function(a){var a=i.getRoot(a),b=i.getRoots();a.curEditNode=null;a.curEditInput=null;a.curHoverNode=null;a.dragFlag=0;a.dragNodeShowBefore=[];a.dragMaskList=[];b.showHoverDom=!0});i.addZTreeTools(function(a,b){b.cancelEditName=function(a){i.getRoot(this.setting).curEditNode&&e.cancelCurEditNode(this.setting,a?a:null,!0)};b.copyNode=function(b,
l,g,o){if(!l)return null;var j=i.nodeIsParent(a,b);if(b&&!j&&this.setting.data.keep.leaf&&g===d.move.TYPE_INNER)return null;var f=this,m=k.clone(l);if(!b)b=null,g=d.move.TYPE_INNER;g==d.move.TYPE_INNER?(l=function(){e.addNodes(f.setting,b,-1,[m],o)},k.canAsync(this.setting,b)?e.asyncNode(this.setting,b,o,l):l()):(e.addNodes(this.setting,b.parentNode,-1,[m],o),e.moveNode(this.setting,b,m,g,!1,o));return m};b.editName=function(a){a&&a.tId&&a===i.getNodeCache(this.setting,a.tId)&&(a.parentTId&&e.expandCollapseParentNode(this.setting,
a.getParentNode(),!0),e.editNode(this.setting,a))};b.moveNode=function(b,l,g,o){function j(){e.moveNode(m.setting,b,l,g,!1,o)}if(!l)return l;var f=i.nodeIsParent(a,b);if(b&&!f&&this.setting.data.keep.leaf&&g===d.move.TYPE_INNER)return null;else if(b&&(l.parentTId==b.tId&&g==d.move.TYPE_INNER||q(l,this.setting).find("#"+b.tId).length>0))return null;else b||(b=null);var m=this;k.canAsync(this.setting,b)&&g===d.move.TYPE_INNER?e.asyncNode(this.setting,b,o,j):j();return l};b.setEditable=function(a){this.setting.edit.enable=
a;return this.refresh()}});var N=e.cancelPreSelectedNode;e.cancelPreSelectedNode=function(a,b){for(var c=i.getRoot(a).curSelectedList,d=0,g=c.length;d<g;d++)if(!b||b===c[d])if(e.removeTreeDom(a,c[d]),b)break;N&&N.apply(e,arguments)};var O=e.createNodes;e.createNodes=function(a,b,c,d,g){O&&O.apply(e,arguments);c&&e.repairParentChkClassWithSelf&&e.repairParentChkClassWithSelf(a,d)};var V=e.makeNodeUrl;e.makeNodeUrl=function(a,b){return a.edit.enable?null:V.apply(e,arguments)};var K=e.removeNode;e.removeNode=
function(a,b){var c=i.getRoot(a);if(c.curEditNode===b)c.curEditNode=null;K&&K.apply(e,arguments)};var P=e.selectNode;e.selectNode=function(a,b,c){var d=i.getRoot(a);if(i.isSelectedNode(a,b)&&d.curEditNode==b&&b.editNameFlag)return!1;P&&P.apply(e,arguments);e.addHoverDom(a,b);return!0};var U=k.uCanDo;k.uCanDo=function(a,b){var c=i.getRoot(a);if(b&&(k.eqs(b.type,"mouseover")||k.eqs(b.type,"mouseout")||k.eqs(b.type,"mousedown")||k.eqs(b.type,"mouseup")))return!0;if(c.curEditNode)e.editNodeBlur=!1,c.curEditInput.focus();
return!c.curEditNode&&(U?U.apply(e,arguments):!0)}})(jQuery);


function switchStylestyle(styleName) {
	$('link[rel=stylesheet][colorTitle]').each(function(i) {
		this.disabled = true;
		if (this.getAttribute('colorTitle') == styleName)
			this.disabled = false;
	});

	$("iframe").contents().find('link[rel=stylesheet][colorTitle]').each(
			function(i) {
				this.disabled = true;
				if (this.getAttribute('colorTitle') == styleName)
					this.disabled = false;
			});
	
	/*$('link[rel=stylesheet][colorTitle]').each(function(i) {
		var sHref = $(this).attr("href").replace($(this).attr("colorTitle"), styleName);
		$(this).attr("colorTitle", styleName);
		$(this).attr("href", sHref);
	});

	$("iframe").contents().find('link[rel=stylesheet][colorTitle]').each(
			function(i) {
				var sHref = $(this).attr("href").replace($(this).attr("colorTitle"), styleName);
				$(this).attr("colorTitle", styleName);
				$(this).attr("href", sHref);
			});*/
	createCookie('style', styleName, 365);
}
// cookie
function createCookie(name, value, days) {
	/*if (days) {
		var date = new Date();
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
		var expires = "; expires=" + date.toGMTString();
	} else
		var expires = "";
	document.cookie = name + "=" + value + expires + "; path=/";*/
	store.set(name, value);
}

function readCookie(name) {
	/*var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for ( var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ')
			c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) == 0)
			return c.substring(nameEQ.length, c.length);
	}
	return null;*/
	return store.get(name);
}
function eraseCookie(name) {
	//createCookie(name, "", -1);
	store.remove(name);
}
/***传入相应参数(sql)，返回json结果 */
  function getJsonObjByParam(param)
  {
  	var jsonData=null;
  	var url = param.url || (__ctx+"/gs/gs-mng!queryResult.action");
	var opt = {
			data:param,
			async:false,
            url:url,
			success:function(data){
				jsonData=data.msg;
			}
	};
  	fnFormAjaxWithJson(opt, true);
	return jsonData;
  }
  /*** 查询列表字段翻译 */
  function getColumnValue(id,name,proJson,value)
  {
  	  var res = "";
	  if(proJson){
  		if(value!==undefined && value!==null){
  			var tv=String(value).split(",");
  			if(tv){
  				var tvName = [];
  				for(var i=0;i<proJson.length;i++){
  		      		if($.inArray(String(proJson[i][id]),tv) >= 0){
  		      			tvName.push(proJson[i][name]);
  		      		}
  		      	}
  				res = tvName.join(",");
  			}
  		}
  	  }
	  return res;
  }
  
  /* 查询列表字段翻译 */
  function getColumnValueByKeyCaption(paramObj,proJson,value)
  {
  	if(proJson){
  		if(value!==undefined && value!==null){
  			var tv=String(value).split(",");
  			if(tv){
  				var tvName = [];
  				for(var i=0;i<proJson.length;i++){
  		      		if($.inArray(String(proJson[i][paramObj["key"]]),tv) >= 0){
  		      			tvName.push(proJson[i][paramObj["caption"]]);
  		      		}
  		      	}
  				return tvName.join(",");
  			}
  		}
  	}
  }
  
  /* 查询列表字段翻译 */
  function getColumnValueByIdText(paramObj,proJson,value)
  {
  	if(proJson){
  		if(value!==undefined && value!==null){
  			var tv=String(value).split(",");
  			if(tv){
  				var tvName = [];
  				for(var i=0;i<proJson.length;i++){
  		      		if($.inArray(String(proJson[i][paramObj["id"]]),tv) >= 0){
  		      			tvName.push(proJson[i][paramObj["text"]]);
  		      		}
  		      	}
  				return tvName.join(",");
  			}
  		}
  	}
  }
  
  /**
   * 查询用户列表
   * @returns
   */
  function findAllUser(){
		var jsonData=null;
		$.ajax({
			type:"post",
			async:false,
            url:__ctx+"/sys/account/user!findAllUser.action",
			success:function(data){
				jsonData=data.msg;
				},
			dataType:"json"
		});
		return jsonData;
  } 
  /**
   * 查询公用分类列表
   * 用于列表中的类型字段翻译
   * @returns
   */
  function findAllCommonType(typeUrl){
		var jsonData=null;
		$.ajax({
			type:"post",
			async:false,
            url:__ctx+typeUrl,
			success:function(data){
				jsonData=data.msg;
				},
			dataType:"json"
		});
		return jsonData;
	  
  }
  
  
  /**
   * 树形查询和普通查询切换
   * @param queryFormId
   * @param a_exp_clp
   * @param a_switch_query
   * @param tree_type
   */
/*  function treeShowOrNot(queryFormId,a_exp_clp,a_switch_query,tree_type){
	  $("#"+queryFormId).hide();
		 $("#"+a_exp_clp).hide();
			$("#"+a_switch_query).toggle(function() {
				$("#"+queryFormId).show();
				$("#"+a_exp_clp).show();
				$("#"+tree_type).hide();
			}, function() {
				$("#"+queryFormId).hide();
				$("#"+a_exp_clp).hide();
				$("#"+tree_type).show();
			});
		  $("#"+a_switch_query).show();
		  
		   $("#"+a_exp_clp).toggle(expandAll, collapseAll);
  }
 
	   
  function collapseAll(xpstyle_panel,xpstyle) {
		$("."+xpstyle_panel+" ."+xpstyle).panel("collapse");
		$(this).removeClass("icon-collapse");
		$(this).addClass("icon-expand");
		$(this).attr("title", "全部展开");
	}

	function expandAll(xpstyle_panel,xpstyle) {
		$("."+xpstyle_panel+" ."+xpstyle).panel("expand");
		$(this).removeClass("icon-expand");
		$(this).addClass("icon-collapse");
		$(this).attr("title", "全部收缩");
	}*/
	
	
/**
 * 发送查询公用类型请求
 * @param queryUrl
 * @param treeId
 * @param rootName
 *//*
  function typeTree(queryUrl,treeId,rootName) {
     	var options = {
           url : __ctx+queryUrl,
           success : function(data) {
          	 treeInit(data.msg,treeId,{"id":"id","text":"name","pid":"pid"},doQueryByType,{"id":"0","text":rootName,"pid":""});
          	 
           },
           async:false
      };
      fnFormAjaxWithJson(options,true);  
 } 
  
  *//**
   * 公用类型树状查询
   * @param filterType
   * @param id
   * @param tableId
   *//*
  function doQueryByType(filterType,id,tableId){
				var param = {filterType:id};																                
	              $("#"+tableId).datagrid("load", param);
 } 
*/

  /**
   * 焦点事件
   * @param id
   */
  function focusEditor(id) {
		setTimeout(function() {$("#" + id).focus();}, 0);
	} 
  /**
   * 回车事件监听
   */
  function doQuseryAction(id){
	  //焦点移出控件
	  $(document).bind('keydown', 'return',function (evt){doQuery(); return false; });
		//焦点在控件里面
	 $("#"+id+" input").bind('keydown', 'return',function (evt){doQuery(); return false; });
  }
  
  /**
   * 上传图片
   * @param buttonUploadId
   * @param id
   * @param imageId
   * @returns
   */
  function uploadImage(buttonUploadId,id,imageId,path){
	  $("#"+buttonUploadId).uploadify(
				{
					'uploader' : __ctx+'/sys/attach/upload-file!saveImage.action;',
					'swf' : __ctx+'/js/uploadify/uploadify-3.1.swf',
					'auto' : true,
					'multi' : false,
					'width' : 20,
					'height' : 20,
					'buttonClass' : 'uploadify-button-add',
					'fileSizeLimit' : '10MB', //限制上传的文件大小
					'fileTypeExts' : '*.jpg;*.JPG;*.gif;*.GIF;*.png;*.PNG;',
					'queueID'     : true,    //控制进度条是否显示
					'onUploadSuccess' : function(file,
							data, response) {
						 var obj = $.parseJSON(data);
				            if(obj) {
				            	if(obj.flag=='1') {
				            		//$.messager.alert('提示', '上传成功！','info');
				            		$("input[name="+ id +"]").attr("value",obj.msg);
				            		$("#"+imageId).attr("src",path+obj.msg);
				            		//alert(path+obj.msg);
				            		
				                } else {
				                	$.messager.alert($.jwpf.system.alertinfo.titleInfo, $.jwpf.system.alertinfo.uploadFailed + obj.msg + '。'+$.jwpf.system.alertinfo.tryAgain,'warning');
				                }
				            } else {
				            	$.messager.alert($.jwpf.system.alertinfo.titleInfo,$.jwpf.system.alertinfo.serverException,'warning');
				            }
					},
					'onUploadError' : function(file,
							errorCode, errorMsg,
							errorString) {
						$.messager.alert($.jwpf.system.alertinfo.titleInfo,$.jwpf.system.alertinfo.file
								+ file.name
								+ $.jwpf.system.alertinfo.failedInfo
								+ errorString,
								"warning");
					}
									
				}); 
  }
	
 	
  /**
   * 转换成json格式，用于子表生成chexkbox
   * @param data
   * @param obj
   * @returns {___anonymous1809_1831}
   */
  function transCheckboxData(data,obj){
	  if(!obj) {
	  		obj = {};
	  		obj["key"] = "key";
	  		obj["caption"] = "caption";
	  	}
		return {"data":data,"obj":obj};
	   
  }
  
  /**
   * 转换json格式，用于子表生成radiobox
   * @param data
   * @param obj
   * @returns {___anonymous2118_2140}
   */
  function transRadioboxData(data,obj){
	  if(!obj) {
	  		obj = {};
	  		obj["key"] = "key";
	  		obj["caption"] = "caption";
	  	}
	  return {"data":data,"obj":obj};
  }

  /**
   * 转换json格式数据，用于子表生成combotree
   * @param data
   * @param obj
   * @param isAutoCollapse 自动折叠
   * @returns {___anonymous2609_2652}
   */
  function transCombotreeData(data,obj,isAutoCollapse){
	  if(data && data.length) {
		   var treeData = [];
		   $.each(data, function(k, v) {
			   var node = {};
			   $.each(obj, function(m, n) {
				   node[m] = v[n];
			   });
			   if(isAutoCollapse) {
				   node["state"] = "closed";
			   }
			   treeData.push(node);
		   });
		   var data2 = transData(treeData, "id", "pid", "children");
		   return {"data":data2,"obj":obj,"treeData":treeData};
	   }
  }
  
  /**
   * 转换combogrid需要的json格式，用于子表生成combogrid
   * @param data
   * @param obj
   * @returns {___anonymous3530_3579}
   */
  function transCombogridData(data,obj){
	   var data2=[];
	   if(data && data.length){
		  $.each(data,function(k,v){
			  var obj2={};
			  $.each(obj,function(m,n){
				  obj2[m]=v[n["column"]];
			  });
			  data2.push(obj2);
		  });
	   }
	   
	   var fields = [];
	   $.each(obj,function(m,n){
		   var field = {width:80};
		   field["field"] = m;
		   field["title"] = n["caption"];
		   if(m == "key") {
			   field["hidden"] = true;
		   }
		   fields.push(field);
	   });
	   
	   var fColumns=[];
	   var field={};
	   field["field"]="ck";
	   field["checkbox"]=true;
	   fColumns.push(field);
	   
	   var jsonData={
   		   "total":data2.length,                                                      
			   "rows":data2
				        
      };
	   return {"data":jsonData,"obj":fields,"fColumns":fColumns};
  }
  
  /**
   * 转换成json格式,用于子表生成combobox
   * @param data
   * @param obj
   * @returns {___anonymous4063_4088}
   */
  function transComboboxData(data,obj){
	  if(!obj) {
	  		obj = {};
	  		obj["key"] = "key";
	  		obj["caption"] = "caption";
	  	}
	  	var newData = [];
	  	//var selData = {};
	  	//selData[obj.key]="";
	  	//selData[obj.caption]=$.jwpf.system.alertinfo.set;
	  	//newData.push(selData);
		if(data) {
			newData = newData.concat(data);
		}
		return {"data":newData,"obj":obj};
  }
  
  /**
   * 通知类型初始化
   * @param data
   * @param selectid
   * @param obj
   */
  function selectType(data,selectid,obj,callFun){
	  var newDataObj= transTypeData(data,obj);
	 	var $select1=$("#"+selectid);
		$select1.combobox({
			//required: true,
			data:newDataObj.data,
			valueField:newDataObj.obj.key,
			textField:newDataObj.obj.caption,
			//onChange:function(newValue,oldValue){
				onSelect:function(rec){
				if(rec.id=="-1"){
					if(callFun){
						callFun();
					}
				}
			}
		});
		//$select1.combobox("loadData", newDataObj.data);
		//var selVal = $select1.data("selVal");
		/*
		if(selVal) {
			$select1.combobox("setValue", selVal);
			$select1.removeData("selVal");
		}*/
	}
  
  
  /**
   * 通知类型数据转换
   * @param data
   * @param obj
   * @returns {___anonymous5858_5883}
   */
  function transTypeData(data,obj){
	  if(!obj) {
	  		obj = {};
	  		obj["key"] = "key";
	  		obj["caption"] = "caption";
	  	}
	  	var newData = [];
	  	var selData = {};
	  	//selData[obj.key]="";
	  	//selData[obj.caption]="";
	  	//newData.push(selData);
		if(data) {
			newData = newData.concat(data);
		}
	    selData = {};
		selData[obj.key]="-1";
	  	selData[obj.caption]=$.jwpf.system.alertinfo.addType;
		newData.push(selData);
		return {"data":newData,"obj":obj};
  }
  
  
  /**
   * 生成查询checkbox控件
   * @param data
   * @param selectid
   * @param obj
   */
 function queryCheckbox(data,selectid,obj){
	 var newData=transCheckboxData(data,obj);
	   if(data && data.length){
		  $.each(data,function(k,v){
			  var str="<input name=filter_INS_"+selectid+"  type='checkbox' value='"+v[newData.obj["key"]]+"'/>"+v[newData.obj["caption"]];
			  $("#div_"+selectid).append(str);
		  });
	   }
 }
 
 /**
  * 生成查询radiobox控件
  * @param data
  * @param selectid
  * @param obj
  */
 function queryRadiobox(data,selectid,obj){
	  var newData=transRadioboxData(data,obj);
	  if(data && data.length){
		  $.each(data,function(k,v){
			  var str="<input name=filter_INS_"+selectid+" type='radio' value='"+v[newData.obj["key"]]+"'/>"+v[newData.obj["caption"]];
			  $("#div_"+selectid).append(str);
		  });
	   }
 }
  
 /**
  * 主表checkbox初始化
  * @param data
  * @param selectid
  * @param obj
  */
  function addCheckbox(data,selectid,obj){
		var newData=transCheckboxData(data,obj);
	   if(data && data.length){
		  $.each(data,function(k,v){
			  var str="<input name="+selectid+"  type='checkbox' value='"+v[newData.obj["key"]]+"'/>"+v[newData.obj["caption"]];
			  $("#div_"+selectid).append(str);
		  });
	   }
  }
  
  /**
   * 主表radiobox初始化
   * @param data
   * @param selectid
   * @param obj
   */
  function addRadio(data,selectid,obj){
	  var newData=transRadioboxData(data,obj);
	  if(data && data.length){
		  $.each(data,function(k,v){
			  var str="<input name="+selectid+" type='radio' value='"+v[newData.obj["key"]]+"'/>"+v[newData.obj["caption"]];
			  $("#div_"+selectid).append(str);
		  });
	   }
  }
  

    /**主表comboboxsearch初始化*/
    function comboboxsearchinit(data,selectid,obj,callFun){
  	    var newDataObj= transComboboxData(data,obj);
  	 	var $select1=$("#"+selectid);
  		$select1.comboboxsearch({
  			valueField:newDataObj.obj.key,
  			textField:newDataObj.obj.caption
  		});
  		$select1.comboboxsearch("loadData",newDataObj.data);
  		var selVal = $select1.data("selVal");
  		if(selVal) {
  			$select1.comboboxsearch("setValue", selVal);
  			$select1.removeData("selVal");
  		}
  		if(callFun) {
  			callFun();
  		}
  	}

/**
 * 主表combobox初始化
 * @param data
 * @param selectid
 * @param obj
 */
  function selectinit(data,selectid,obj,callFun){
	  var newDataObj= transComboboxData(data,obj);
	 	var $select1=$("#"+selectid);
		$select1.combobox({
			//required: true,
			data:newDataObj.data,
			valueField:newDataObj.obj.key,
			textField:newDataObj.obj.caption
		});
		//$select1.combobox("loadData", newDataObj.data);
		var selVal = $select1.data("selVal");
		if(selVal) {
			$select1.combobox("setValue", selVal);
			$select1.removeData("selVal");
		}
		if(callFun) {
			callFun();
		}
	}
  
  /**初始化自动完成带搜索下拉框，后台过滤数据*/
  function autoCompleteComboBoxSearchInit(sid, param) {
	  var $sel = $("#" + sid);
	  var opt = {
				mode:"remote",
				hasDownArrow:false,
				autoComplete:true,
				delay:400,
				url:__ctx+"/gs/gs-mng!queryAjaxResult.action",
				valueField:"key",
				textField:"caption",
				onBeforeLoad:function(p) {
					//$.extend(p, param);
					/*if(!$(this).data("_afterFirstInit_")) {
						$(this).data("_afterFirstInit_", true);
						return false;
					}*/
					_initSelectParam_(p, this, param);
				}
	  };
	  if(param["formatter"]) {
		  opt["formatter"] = param["formatter"];
		  delete param["formatter"];
	  }
	  $sel.comboboxsearch(opt);
  }
  
  /**
   * 初始化自动完成下拉框，后台过滤数据
   * @param sid
   * @param param
   */
  function autoCompleteSelectInit(sid, param) {
	  var $sel = $("#" + sid);
	  var opt = {
				mode:"remote",
				hasDownArrow:false,
				autoComplete:true,
				delay:400,
				url:__ctx+"/gs/gs-mng!queryAjaxResult.action",
				valueField:"key",
				textField:"caption",
				onBeforeLoad:function(p) {
					//$.extend(p, param);
					/*if(!$(this).data("_afterFirstInit_")) {
						$(this).data("_afterFirstInit_", true);
						return false;
					}*/
					_initSelectParam_(p, this, param);
				}
		  };
	  if(param["formatter"]) {
		  opt["formatter"] = param["formatter"];
		  delete param["formatter"];
	  }
	  $sel.combobox(opt);
  }
  
  function ajaxSelectInit(sid, param) {
	  var $sel = $("#" + sid);
	  var opt = {
				mode:"remote",
				prompt:"请输入关键字查询",
				hasDownArrow:true,
				delay:400,
				url:__ctx+"/gs/gs-mng!queryAjaxResult.action",
				valueField:"key",
				textField:"caption",
				onBeforeLoad:function(p) {
					/*if(!$(this).data("_isFirstInit_")) {
						$(this).data("_isFirstInit_", true);
						return false;
					} else {
						return ajaxSelectParamInit(p, this, param);
					}*/
					return ajaxSelectParamInit(p, this, param);
				}
		  };
	  if(param["formatter"]) {
		  opt["formatter"] = param["formatter"];
		  delete param["formatter"];
	  }
	  if(param.multiple || $sel.prop("multiple")) {
		  $sel.comboboxsearch(opt);
	  } else {
		  $sel.combobox(opt);
	  }
  } 
  
  //初始化请求参数
  function _initSelectParam_(p, obj, param) {
	  $.extend(p, param);
	  var opts = $(obj).combobox('options');
	  if(opts && opts["onSetQueryParam"]) {
		$.extend(true, p, opts["onSetQueryParam"].call(obj, p, opts["rowData"], opts["rowIndex"]));
	  }
  }
  
  function ajaxSelectParamInit(p, obj, param) {
	  _initSelectParam_(p, obj, param);
	  if(p == null || p.q == null){
			var value = $(obj).combobox('getValue');
			if(value){// 修改的时候才会出现q为空而value不为空
				p.kq = "1";
				p.q = value;
				return true;
			}
	  }
	  return true;
  }
  //combo类控件显示值
  function ajaxSelectFormatter(row, fieldKey, param) {
	  var caption = row[fieldKey+"_caption"];
	  if(row["_isFooter_"]) {
		  caption = row[fieldKey] || "";
	  }
	  return (caption !==undefined && caption !== null) ? caption : getAjaxResultByKey(row, fieldKey, param);
  }
  
  function getAjaxResultByKey(row, fieldKey, param) {
	  	var value = row[fieldKey];
	  	if(!value && value !== 0) {
	  		return "";
	  	}
	  	var result=[];
		$.ajax({
			type:"post",
			async:false,
			data:$.extend(true, {}, param, {kq:"1", q:value}),
            url:__ctx+"/gs/gs-mng!queryAjaxResult.action",
			success:function(data){
			   result = $.map(data, function(row) {
				  return row["caption"]; 
			   });
			},
			dataType:"json"
		});
		var res = result.join(",");
	    row[fieldKey+"_caption"] = res;
		return res;
  }
  
  function comboRadioInit(data,selectid,obj,callFun){
	  var newDataObj= transComboboxData(data,obj);
	 	var $select1=$("#"+selectid);
		$select1.comboradio({
			data:newDataObj.data,
			valueField:newDataObj.obj.key,
			textField:newDataObj.obj.caption
		});
		if(callFun) {
			callFun();
		}
  }
  function  comboCheckInit(data,selectid,obj,callFun){
	  var newDataObj= transComboboxData(data,obj);
	 	var $select1=$("#"+selectid);
		$select1.combocheck({
			data:newDataObj.data,
			valueField:newDataObj.obj.key,
			textField:newDataObj.obj.caption
		});	
		if(callFun) {
			callFun();
		}
  }
  function  newRadioBoxInit(data,selectid,obj,callFun){
	  var newDataObj= transComboboxData(data,obj);
	 	var $select1=$("#"+selectid);
		$select1.newradiobox({
			data:newDataObj.data,
			valueField:newDataObj.obj.key,
			textField:newDataObj.obj.caption
		});
		if(callFun) {
			callFun();
		}
  }
  function  newCheckBoxInit(data,selectid,obj,callFun){
	  var newDataObj= transComboboxData(data,obj);
	 	var $select1=$("#"+selectid);
		$select1.newcheckbox({
			data:newDataObj.data,
			valueField:newDataObj.obj.key,
			textField:newDataObj.obj.caption
		});
		if(callFun) {
			callFun();
		}
  }
 /**
   * 转换成json格式,用于生成ztree
   * @param data
   * @param obj
   */
  function  transzTreeData(data,obj){
	  if(data && data.length){
		var treeData = [];
		$.each(data, function(k, v) {
			var node = {};
			$.each(obj, function(m, n) {
					node[m] = v[n];
				});
			treeData.push(node);
		});						
			return treeData;
	  }
	}
  function zTreeInit(data,selectid,obj,callFun){
	   	var _ztree_setting_ = {
				data : {
					key:{
						name :"text"
					},
					simpleData: {
						enable: true,
						idKey: "id",
						pIdKey: "pid"
					}
				}
		};
	  var newDataObj= transzTreeData(data,obj);
	  var zTreeObj = $.fn.zTree.init($("#"+selectid),_ztree_setting_,newDataObj);
	  zTreeObj.expandAll(true);
  }
  //不展开树
  function zTreeInitWithCollapse(data,selectid,obj,callFun){
	   	var _ztree_setting_ = {
				data : {
					key:{
						name :"text"
					},
					simpleData: {
						enable: true,
						idKey: "id",
						pIdKey: "pid"
					}
				}
		};
	  var newDataObj= transzTreeData(data,obj);
	  var zTreeObj = $.fn.zTree.init($("#"+selectid),_ztree_setting_,newDataObj);
   }
  
    function initCommonData(sid,param,obj, callFunc, subCallFunc,isAsync) {
    	if(isAsync!==false){
    		isAsync=true;//异步
    	}
    	var url = param.url || (__ctx+"/gs/gs-mng!queryResult.action");
    	$.ajax({
			type:"post",
			data:param,
			async:isAsync,
			url:url,
			success:function(data){
				if(callFunc) {
					callFunc(data.msg,sid,obj,subCallFunc);
				}
			},
			dataType:"json"
		});
    }
    
    /**
     * 数字控件初始化
     * @param data
     * @param selectid
     * @param obj
     */
    function numberBoxValueInit(data,selectid,obj, callFun) {
    	var newDataObj= transComboboxData(data,obj);
    	var val = "";
    	var data = newDataObj.data;
    	if(data && data.length > 0) {
    		val = data[0][newDataObj.obj.key];
    	}
    	var sid = $("#"+selectid);
    	sid.numberbox('setValue', val);
    	if(callFun) {
			callFun();
		}
    }
    
    /**
     * 文本控件初始化
     * @param data
     * @param selectid
     * @param obj
     */
    function textBoxValueInit(data,selectid,obj, callFun) {
    	var newDataObj= transComboboxData(data,obj);
    	var val = "";
    	var data = newDataObj.data;
    	if(data && data.length > 0) {
    		val = data[0][newDataObj.obj.key];
    	}
    	var sid = $("#"+selectid);
    	sid.val(val);
    	if(callFun) {
			callFun();
		}
    }
	   
	   /** 
	    * json格式转树状结构 
	    * @param   {json}      json数据 
	    * @param   {String}    id的字符串 
	    * @param   {String}    父id的字符串 
	    * @param   {String}    children的字符串 
	    * @return  {Array}     数组 
	    */  
	   function transData(a, idStr, pidStr, chindrenStr){  
	       var r = [], hash = {}, id = idStr, pid = pidStr, children = chindrenStr, i = 0, j = 0, len = a.length;  
	       for(; i < len; i++){  
	           hash[a[i][id]] = a[i];  
	       }  
	       for(; j < len; j++){  
	           var aVal = a[j], hashVP = hash[aVal[pid]];  
	           if(hashVP){  
	               !hashVP[children] && (hashVP[children] = []);  
	               hashVP[children].push(aVal);  
	           }else{  
	               r.push(aVal);  
	           }  
	       }  
	       return r;  
	   }  
	   
	   /**
	    * 根据id查找系统个人桌面
	    * @param id
	    * @param callFun
	    */
	 function getLevelBydesktopId(id,callFun) {
	       	var options = {
	             url : __ctx+'/sys/desktop/desktop!queryLevel.action',
	             data:{
	            	 "id":id
	             },
	             success : function(data) {
	            	 var level = data.msg.level;
	            	 //级别为1隐藏桌面布局和实际布局
	            	//if(level==1){
	            	 if(callFun) {
	            		 callFun(id,level);
	            	 }
	            	//}
	            	 
	             }
	        };
	        fnFormAjaxWithJson(options,true);
	   } 
	  
/**
 * 主表combotree初始化
 * @param data
 * @param selectid
 * @param obj
 */
	   function comnbotreeInit(data,selectid,obj,callFun,pNode){
		 var $select=$("#"+selectid);
		 if(data && data.length) {
			   var treeData = [];
			   $.each(data, function(k, v) {
				   var node = {};
				   $.each(obj, function(m, n) {
					   node[m] = v[n];
				   });
				   treeData.push(node);
			   });
			   var data2 = transData(treeData, "id", "pid", "children");
			   if(pNode){
				   pNode["children"]=data2;
				   data2=[pNode];
			   }
		       //var newData=transCombotreeData(data,obj);
			   $select.combotree({
					  lines:true,
					  editable:true,
					  onClick:function(rec){
						 if(callFun) {
							 callFun(rec);
						 }
						 //getLevelBydesktopId(rec.id,callFun);
					  }
			   });
			   $select.combotree("loadData",data2);
		   }
	   } 
	   
	   /**
	    * 主表combotree初始化，收缩所有节点
	    * @param data
	    * @param selectid
	    * @param obj
	    */
	   	   function comnbotreeInitWithCollapse(data,selectid,obj,callFun,pNode){
	   		   var $select=$("#"+selectid);
	   		 if(data && data.length) {
	   			   var treeData = [];
	   			   $.each(data, function(k, v) {
	   				   var node = {};
	   				   $.each(obj, function(m, n) {
	   					   node[m] = v[n];
	   				   });
	   				   treeData.push(node);
	   			   });
	   			   var data2 = transData(treeData, "id", "pid", "children");
	   			   if(pNode){
	   				   pNode["children"]=data2;
	   				   data2=[pNode];
	   			   }
	   		       //var newData=transCombotreeData(data,obj);
	   			   $select.combotree({
	   					  lines:true,
	   					  editable:true,
	   					  onClick:function(rec){
	   						  if(callFun) {
	   							  callFun(rec);
	   						  }
	   						  //getLevelBydesktopId(rec.id,callFun);
	   					  }
	   				  });
	   			   $select.combotree("loadData",data2);
	   			   $select.combotree("tree").tree("collapseAll"); //折叠所有节点
	   		   }
	   	   } 
	   
	
/**
 * 树状查询初始化
 * @param data
 * @param selectid
 * @param obj
 * @param subCallFunc
 */
	   function treeInit(data,selectid,obj, subCallFunc,pNode,menuId,isCheck){
		   var $select=$("#"+selectid);
		   if(pNode || (data && data.length)){
				   var treeData = [];
				   $.each(data, function(k, v) {
					   var node = {};
					   $.each(obj, function(m, n) {
						   node[m] = v[n];
					   });
					   treeData.push(node);
				   });
				   var data2 = transData(treeData, "id", "pid", "children");
				   if(pNode){
					   pNode["children"]=data2;
					   data2=[pNode];
				   }
				   if(data2 && data2.length>0){
					   $select.tree({
						     lines:true,
							 checkbox:isCheck,
							 onClick:function(node){
									if(subCallFunc) {
										if (node){
											if(node.id ==0) {
												subCallFunc("");
											} else {
												var children = $select.tree('getChildren', node.target);
												var idArray = [node.id];
												$.each(children, function(k,v) {
													idArray.push(v.id);
												});
												subCallFunc(idArray.join(","));
											}
										} 
										
									}
								},	/// /右键单击节点，然后显示上下文菜单
							 onContextMenu: function(e, node){
								 if(menuId){
										e.preventDefault();
										$select.tree('select', node.target);
										$('#'+menuId).menu('show', {
											left: e.pageX,
											top: e.pageY
										});
										var childrens = $select.tree('getChildren', node.target);
										var idArray = [node.id];
										$.each(childrens, function(k,v) {
											idArray.push(v.id);
										});
										$("#"+menuId).data("nodeId",node.id);
										$("#"+menuId).data("childIds",idArray);
								 }
							
							}
						 });
						 $select.tree("loadData",data2);
				   }
		
			   }
		   
	   }
   
	   /**
	    * 树状查询初始化，自动折叠节点
	    * @param data
	    * @param selectid
	    * @param obj
	    * @param subCallFunc
	    */
	   	   function treeInitWithCollapse(data,selectid,obj, subCallFunc,pNode,menuId,isCheck){
	   		   var $select=$("#"+selectid);
	   		   if(pNode || (data && data.length)){
	   				   var treeData = [];
	   				   $.each(data, function(k, v) {
	   					   var node = {};
	   					   $.each(obj, function(m, n) {
	   						   node[m] = v[n];
	   					   });
	   					   treeData.push(node);
	   				   });
	   				   var data2 = transData(treeData, "id", "pid", "children");
	   				   if(pNode){
	   					   pNode["children"]=data2;
	   					   data2=[pNode];
	   				   }
	   				   if(data2 && data2.length>0){
	   					   $select.tree({
	   						     lines:true,
	   							 checkbox:isCheck,
	   							 onClick:function(node){
	   									if(subCallFunc) {
	   										if (node){
	   											if(node.id ==0) {
	   												subCallFunc("");
	   											} else {
	   												var children = $select.tree('getChildren', node.target);
	   												var idArray = [node.id];
	   												$.each(children, function(k,v) {
	   													idArray.push(v.id);
	   												});
	   												subCallFunc(idArray.join(","));
	   											}
	   										} 
	   										
	   									}
	   								},	/// /右键单击节点，然后显示上下文菜单
	   							 onContextMenu: function(e, node){
	   								 if(menuId){
	   										e.preventDefault();
	   										$select.tree('select', node.target);
	   										$('#'+menuId).menu('show', {
	   											left: e.pageX,
	   											top: e.pageY
	   										});
	   										var childrens = $select.tree('getChildren', node.target);
	   										var idArray = [node.id];
	   										$.each(childrens, function(k,v) {
	   											idArray.push(v.id);
	   										});
	   										$("#"+menuId).data("nodeId",node.id);
	   										$("#"+menuId).data("childIds",idArray);
	   								 }
	   							
	   							}
	   						 });
	   						 $select.tree("loadData",data2);
	   						 $select.tree("collapseAll"); //折叠所有节点
	   				   }
	   		
	   			   }
	   		   
	   	   }	   
	  
	   
/**
 * 主表combogrid初始化
 * @param data
 * @param selectid
 * @param obj
 */
	   function combogridInit(data,selectid,obj, callFun){
		   var $sel=$("#"+selectid);
		   var data2=[];
		   if(data && data.length){
			  $.each(data,function(k,v){
				  var obj2={};
				  $.each(obj,function(m,n){
					  obj2[m]=v[n["column"]];
				  });
				  data2.push(obj2);
			  });
		   }
		   
		   var fields = [];
		   $.each(obj,function(m,n){
			   var field = {width:80};
			   field["field"] = m;
			   field["title"] = n["caption"];
			   if(m == "key") {
				   field["hidden"] = true;
			   }
			   fields.push(field);
		   });
		   
		   var fColumns=[];
		   var field={};
		   field["field"]="ck";
		   field["checkbox"]=true;
		   fColumns.push(field);
		   
		   var jsonData={
	    		   "total":data2.length,                                                      
				   "rows":data2
	       };
		   //var newData=transCombogridData(data,obj);
			 $sel.combogrid({
				rownumbers : true,
			    panelWidth:500,
			    fitColumns: true,
			    idField:'key',  
			    textField:'caption',
			    mode:"local",
			    data:jsonData,
			    //frozenColumns:[fColumns],
			    columns:[fields]
			    /**由于datagrid里的选中行做了修改（设置为不选中），在此可以通过点击行来触发事件设置选中值*/
			    /*onClickRow:function(rowIndex, rowData){
			    	var selRows=$sel.combogrid("grid").datagrid("getSelections"); 
			    	if($.inArray(rowData, selRows)>=0){
				        $sel.combogrid("grid").datagrid("unselectRow", rowIndex); 
			        }else{
			    		$sel.combogrid("grid").datagrid("selectRow", rowIndex); 
			    	}
			    }*/
			});  
			//$sel.combogrid("grid").datagrid("loadData", jsonData); 
			var selVal = $sel.data("selVal");
			if(selVal) {
				if($.isArray(selVal)) {
					$sel.combogrid("setValues",selVal);
				} else {
					if(selVal){
						$sel.combogrid("setValues",String(selVal).split(","));
					} else {
						$sel.combogrid("setValues",[]);
					}
				}
				
				$sel.removeData("selVal");
			}
			if(callFun) {
				callFun();
			}
	   }
	   function dataGridInit(data,selectid,obj, callFun){
			var data2=[];
			if(data && data.length){
				$.each(data,function(k,v){
					  var obj2={};
					  $.each(obj,function(m,n){
						  obj2[m]=v[n["column"]];
					  });
					  data2.push(obj2);
				  });
			 }
			
		  	var fields = [];
		   $.each(obj,function(i,n){
			   var field = {width:100};
			   field["field"] = i;
			   field["title"] = n.caption;
			  /* if(i == "key") {
				   field["hidden"] = true;
			   }*/
			   fields.push(field);
		   });	   
		   
		   var fColumns=[];
		   var field={};
		   field["field"]="ck";
		   field["checkbox"]=true;
		   fColumns.push(field);		   
		   var dataList={
	    		   "total":data2.length,                                                      
				   "rows":data2
	       };
		   
		   $('#'+selectid).datagrid({  
			    nowrap: true,
				striped: true,	
				rownumbers:true, 
				frozenColumns:[fColumns],
				columns:[fields]					 							
		   });  
			 $('#'+selectid).datagrid("loadData",dataList);
		}
	   function initDataGridFilterData(dataGridId,jsonData,title){
			var data = jsonData["list"];	
			var mapping = jsonData["map"];
		  	var fields = [];
		  	//加公用属性
		   $.each(mapping,function(i,n){
			   var field = {width:80};
			   field["field"] = n.key;
			   field["title"] = n.caption;
			 /*  if(nameMapping[n.key]){
				   	field["formatter"]= function(value, rowData, rowIndex){
					  var formatArr =  nameMapping[n.key];	   
					   for(var j=0,len=formatArr.length;j<len;j++){
				    		if(formatArr[j].key==value){
				    			return	formatArr[j].caption;
				    		}
					   	} 				    	                                                            
			        };
			   }*/

			   if(n.order == "hidden") {
				  field["hidden"] = true;
			   } else {
				  fields.push(field); 
			   } 
		   });	
		  	//加扩展属性
	/*	  if(nameArry.length){
		   $.each(nameArry, function(i,n) {
			   var field = {width:80};
			   field["field"] = n.key;
			   field["title"] = n.caption;
			   field["formatter"]= function(value, rowData, rowIndex){
				  var formatArr =  n.data;	   
				   for(var j=0,len=formatArr.length;j<len;j++){
			    		if(formatArr[j].key==value){
			    			return formatArr[j].caption;
			    		}
				   	} 				    	                                                            
		       };
			   fields.push(field);
		   		});
	   		}*/
		   var fColumns=[];
		   var field={};
		   field["field"]="ck";
		   field["checkbox"]=true;
		   fColumns.push(field);		   
		   var dataList={
				   "total":data.length,                                                      
				   "rows":data
		   };
		   
		   $('#'+dataGridId).datagrid({  
			   title:	title,
			    nowrap: true,
				striped: true,	
				rownumbers:true,
				frozenColumns:[fColumns],
				columns:[fields]
		   });  
			 $('#'+dataGridId).datagrid("loadData",dataList);
		}
	   
	   var $ES = {
				getCtxPath:function() {
					return __ctx;
				},
				getJqObj:function(obj) {
					if(obj instanceof jQuery) {
						return obj;
					} else {
						return $(obj);
					}
				},
				getJsonObjByParam:function(param) {
					var jsonData=null;
					var url = param.url || (__ctx+"/gs/gs-mng!queryAjaxResult.action");
					var opt = {
							data:param,
							async:false,
							url:url,
							success:function(data){
								if($.isPlainObject(data)) {
									jsonData=data.msg;
								} else {
									jsonData=data;
								}
							}
					};
					fnFormAjaxWithJson(opt, true);
					return jsonData;
				},
				getFilterData : function(sqlKey,filterParam,callback){				 
					 	var param = {
					 		"entityName" :	sqlKey
					 	};
					 	$.extend(param,filterParam);
					  	var jsonData=null;
						var opt = {
								data:param,
								//async:true,
								url:__ctx+"/gs/gs-mng!dataFilter.action",
								success:function(data){
									if(callback) {
										callback(data.msg);
									}
								}
						};
					  	fnFormAjaxWithJson(opt, true);
						return jsonData;
				},
				isNotBlank:function(val) {
					return ($.isNumeric(val) || val);
				},
				getStringIfBlank:function(val ,defaultVal) {
					if($ES.isNotBlank(val)) {
						return String(val);
					} else {
						return defaultVal;
					}
				},
				replaceNLToBr:function(val) {
					if(val) {
						val = val.replace(/\r\n/g,"<br/>").replace(/\n/g,"<br/>");
					}
					return val;
				},
				fnFormatText:function(val) {
					if(val) {
						if(val.length > 50) {
							val = val.slice(0, 50) + "...";
						}
					}
					return $ES.replaceNLToBr(val);
				},
				bindDefaultEvent:function(obj, eventObj) {
					var jqObj = $ES.getJqObj(obj);
					  $.each(eventObj, function(k, v) {
						  if(k) {
							  var eventName = k;
							  if(eventName.indexOf("on") >= 0) {
								  eventName = eventName.substr(2);
							  }
							  eventName = eventName.toLowerCase();
							  jqObj.off(eventName).on(eventName, v);
						  }
					  });
				},
				loadUrl:function(obj, url) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.each(function() {
						$(this).load(url || $(this).attr("url"));
					});
				},
				getTextBoxVal:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					return jqObj.val();
				},
				setTextBoxVal:function(obj, val) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.val(val);
				},
				setText:function(obj, text) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.text(text);
				},
				getText:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					return jqObj.text();
				},
				initTextBox:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					if(opt["jwDataType"] == "L") {//整数
						jqObj.attr("type", "number");
					} else if(opt["jwDataType"] == "C") {//小数
						jqObj.attr("type", "number");
						jqObj.attr("precision", opt["precision"]);
						var step = 1/parseInt("1".rightPad("0", parseInt(opt["precision"])+1));
						jqObj.attr("step", step);
						jqObj.attr("data-sanitize", "numberFormat");
						var fmt = String(step).replaceAll("1","0");
						jqObj.attr("data-sanitize-number-format", fmt);
					} else {
						if(opt["format"]) {
							jqObj.attr("type", opt["format"]);
						}
					}
					//增加校验信息
					if(opt["validate"]) {
						var validateOpt = opt["validate"];
						if(validateOpt["validation"] && validateOpt["validation"].indexOf("ajax") >= 0) {
							jqObj.attr("data-validation", validateOpt["validation"]);
							jqObj.attr("data-validation-url", validateOpt["url"]);
							jqObj.attr("data-validation-param-name", validateOpt["paramName"]);
							jqObj.attr("data-validation-req-params", validateOpt["reqParams"]);
						}
						if(validateOpt["pattern"]) {
							jqObj.attr("pattern", validateOpt["pattern"]);
						}
						if(validateOpt["length"]) {
							jqObj.attr("data-validation", "length");
							jqObj.attr("data-validation-length", validateOpt["length"]);
						}
					}
					if($ES.isNotBlank(opt.value)) {//设置初始值
						$ES.setTextBoxVal(jqObj, opt.value);
					}
					if(opt["onChange"]) {
						jqObj.on("input", function (event) {
							opt["onChange"].call(this, this.value);
						});
					}
					["onclick","ondblclick","onkeydown","onkeyup","onfocus", "onblur"].forEach(function(item, index, arr) {
						if(opt[item]) {
							var eventStr = item.slice(2);
							jqObj.on(eventStr, function (event) {
								opt[item].call(this, event);
							});
						}
					});
					if(opt["esAutoComplete"]) {//自动完成输入
						$ES.initAutoComplete(jqObj, opt);
					}
				},
				initAutoComplete:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.typeahead({
						source:function(query, process) {
							var qp = {};
							var onSetQueryParam = jqObj.data("onSetQueryParam");
							if(onSetQueryParam) {
								qp = onSetQueryParam(query);
							}
							var qd = $.extend(true, {}, opt.queryParams, qp, {q:query});
							fnFormAjaxWithJson({
								url:__ctx+"/gs/gs-mng!queryAjaxResult.action"
								,data:qd
								,success : function(data) {
					            	process(data);
					            }
							}, true);
						}
						,afterSelect:function(item) {
							var onChange = jqObj.data("onChange");
							if(onChange) {
								onChange.call(this, item.text);
							}
						}
						,delay: 300
						,showHintOnFocus:true
					});
				},
				bindAutoCompleteEvent:function(obj, eventObj) {
					var jqObj = $ES.getJqObj(obj);
					if(eventObj["onSetQueryParam"]) {
						jqObj.data("onSetQueryParam", eventObj["onSetQueryParam"]);
					}
					if(eventObj["onChange"]) {
						jqObj.data("onChange", eventObj["onChange"]);
					}
				},
				getResultByVal:function(val, opt, callBack) {
					if(opt["data"]) {
						var valArr = val.split(",");
						var data = opt["data"];
						var result = [];
						for(var i in data) {
							var obj = data[i];
							if(valArr.indexOf(obj.id) >= 0) {
								result.push(obj);
							}
						}
						if(callBack) {
							return callBack(result);
						}
					} else {
						if(val !== "") {
							var qp = $.extend(true,{q:val}, opt["queryParams"]);
							return $.post(__ctx+"/gs/gs-mng!queryAjaxResultByKey.action", qp, function (data) {
								var optArr = [];
								if(data.flag) {
									$MsgUtil.alert($.jwpf.system.alertinfo.loadDataError + data.msg);
								} else {
									if(callBack) {
										callBack(data);
									}
								}
								
							},"json");
						} else {
							if(callBack) {
								return callBack([]);
							}
						}
						
					}
				},
				getDefaultComboBoxOpt:function(opt, jqObj) {
					var initData = [{id: "", text: opt.placeholder || $.jwpf.system.alertinfo.set}];
					if(opt.data) {
						return {
						  width:opt.width || "resolve",
						  allowClear: true,
						  language: opt.locale || "zh-CN",
						  theme: opt.theme || "bootstrap",
						  containerCssClass:opt.containerCssClass || "input-sm",
						  placeholder:opt.placeholder || $.jwpf.system.alertinfo.set,
						  placeholderOption: "first",
						  data:initData.concat(opt.data),
						  multiple:opt["multiple"],
						  onChange:opt["onChange"],
						  onSelect:opt["onSelect"],
						  onUnSelect:opt["onUnSelect"]
						};
					} else {
						return {
						  width:opt.width || "resolve",
						  allowClear: true,
						  language: opt.locale || "zh-CN",
						  theme: opt.theme || "bootstrap",
						  containerCssClass:opt.containerCssClass || "input-sm",
						  placeholder:opt.placeholder || $.jwpf.system.alertinfo.set,
						  placeholderOption: "first",
						  multiple:opt["multiple"],
						  ajax: {
							  url: __ctx+"/gs/gs-mng!queryAjaxResult.action",
							  dataType: 'json',
							  delay: 250,
							  data: function (params) {
								  var qp = {};
								  var onSetQueryParam = jqObj.data("onSetQueryParam");
								  if(onSetQueryParam) {
									  qp = onSetQueryParam(params);
								  }
								  return $.extend(true,{
									q: params.term, 
									page: params.page
								  }, opt.queryParams, qp);
								},
								processResults: function (data, params) {
								  params.page = params.page || 1;
								  /*$.each(data, function(i,n) {
									 n["id"] = n["key"];
									 n["text"] = n["caption"];
								  });*/
								  return {
									results: initData.concat(data),
									pagination: {
									  more: (params.page * 30) < data.total_count
									}
								  };
								},
								cache: true
						  },
						  /*initSelection: function(element, callback) {
								var val = $(element).val();
								var multiple = $(element).prop("multiple");
								if(val) {
									var qp = $.extend(true,{q:val},opt.queryParams);
									return $.post(__ctx+"/gs/gs-mng!queryAjaxResultByKey.action", qp, function (data) {
										callback(multiple?data:data[0]);
									},"json");
								}
						  },*/
						  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
						  minimumInputLength: opt.minInputLen || 0,
						  onChange:opt["onChange"],
						  onSelect:opt["onSelect"],
						  onUnSelect:opt["onUnSelect"],
						  onSetQueryParam:opt["onSetQueryParam"],
						  templateResult: function (data) {
							  return (data.caption2) ? data.caption2 : data.text;
						  },
						  templateSelection: function (data) {
							  return (data.caption2) ? data.caption2 : data.text;
						  }
						};	
					}
				},
				initComboBox:function(obj,opt) {
					var jqObj = $ES.getJqObj(obj);
					if(opt.data) {
						jqObj.data("isFixData", true);
					}
					if(jqObj.attr("multiple")) {
						opt.multiple = true;
					}
					jqObj.data("esFieldCaption", opt["esFieldCaption"]);
					jqObj.data("queryParams", opt.queryParams);
					jqObj.data("onSetQueryParam", opt.onSetQueryParam);
					var jqOpt = $ES.getDefaultComboBoxOpt(opt, jqObj);
					jqObj.select2(jqOpt);
					if($ES.isNotBlank(opt.value)) {
						$ES.setComboBoxVal(jqObj, opt.value, true);
					}
					$ES.bindComboBoxEvent(jqObj, jqOpt);
				},
				bindComboBoxEvent:function(obj, eventObj) {
					var jqObj = obj;
					if(eventObj["onChange"]) {
						jqObj.on("change", function(e) {
							if(!$(this).data("notTriggerChange")) {//不触发change事件
								eventObj["onChange"].call(this, $ES.getComboBoxValText(jqObj)["id"], $ES.getComboBoxResult(jqObj)["id"]);
								return true;
							} else {
								$(this).removeData("notTriggerChange");
								return false;
							}
						});
					}
					if(eventObj["onSelect"]) {
						jqObj.on("select2:select", function(e) {
							eventObj["onSelect"].call(this, e.params.data);
							return true;
						});
					}
					if(eventObj["onUnSelect"]) {
						jqObj.on("select2:unselect", function(e) {
							eventObj["onUnSelect"].call(this, e.params.data);
							return true;
						});
					}
					if(eventObj["onSetQueryParam"]) {
						jqObj.data("onSetQueryParam", eventObj["onSetQueryParam"]);
					}
				},
				setComboBoxVal:function(obj, val, isInit) {
					var jqObj = $ES.getJqObj(obj);
					if($.isPlainObject(val)) {
						$ES.setComboBoxValText(jqObj, val.id, val.text, isInit);
					} else {
						val = $ES.getStringIfBlank(val, "");
						if(jqObj.data("isFixData")) {//固定值
							if(jqObj.prop("multiple")) {
								jqObj.val(val.split(","));
							} else {
								jqObj.val(val);
							}
							jqObj.trigger("change");
							//if(!isInit) jqObj.trigger("change");
						} else {
							$ES.initComboBoxSelection(jqObj, val, isInit);
						}
					}
				},
				setComboBoxSelected:function(jqObj, val, txt) {
					var opt = jqObj.find("option[value='"+ val +"']");
					if(opt.length) {
						opt.prop("selected",true);
					} else {
						jqObj.append('<option value="'+ val +'" selected="true">'+ txt +'</option>');
					}
				},
				initComboBoxSelection:function(jqObj, val, isInit) {
					if($ES.isNotBlank(val)) {
						var onSetQueryParam = jqObj.data("onSetQueryParam");
						var qps = {};
						if(onSetQueryParam) {
							qps = onSetQueryParam({});
						}
						var qp = $.extend(true,{q:val},jqObj.data("queryParams"), qps);
						return $.post(__ctx+"/gs/gs-mng!queryAjaxResultByKey.action", qp, function (data) {
							var optArr = [];
							if(data.flag) {
								$MsgUtil.alert(jqObj.data("esFieldCaption") + $.jwpf.system.alertinfo.loadDataError + data.msg);
							} else {
								jqObj.empty();//先清空数据
								jqObj.append('<option value=""></option>');//默认空选项
								for(var i in data) {
									var row = data[i];
									var $opt = $('<option value="'+ row.id +'" selected="true">'+ (row.caption2 || row.text) +'</option>');
									$opt.data("data", row);
									jqObj.append($opt);
									//optArr.push();
								}
								//jqObj.html(optArr.join(""));
								if(!isInit) jqObj.trigger("change");
							}
							
						},"json");
					} else {
						jqObj.val(null);
						if(!isInit) jqObj.trigger("change");
					}
				},
				setComboBoxValText:function(obj, val, text, isInit) {
					var jqObj = $ES.getJqObj(obj);
					val = $ES.getStringIfBlank(val, "");
					var txt = text;
					if(jqObj.data("isFixData")) {//固定值
							txt = txt || val;
							if(jqObj.prop("multiple")) {
								if(val) {
									var valArr = val.split(",");
									var txtArr = txt.split(",");
									for(var i=0,len=valArr.length;i<len;i++) {
										$ES.setComboBoxSelected(jqObj, valArr[i], txtArr[i]);
									}
								} else {//清空数据
									jqObj.val(null);
								}
							} else {
								$ES.setComboBoxSelected(jqObj, val, txt);
							}
							if(!isInit) jqObj.trigger("change");
					} else {
						if(txt) {
							var optArr = [];
							optArr.push('<option value=""></option>');//默认空选项
							if(jqObj.prop("multiple")) {
								var valArr = val.split(",");
								var txtArr = txt.split(",");
								for(var i=0,len=valArr.length;i<len;i++) {
									optArr.push('<option value="'+ valArr[i] +'" selected="true">'+ txtArr[i] +'</option>');
								}
							} else {
								optArr.push('<option value="'+ val +'" selected="true">'+ txt +'</option>');
							}	
							jqObj.html(optArr.join(""));
							if(!isInit) jqObj.trigger("change");
						} else {//动态值，取查询数据
							$ES.initComboBoxSelection(jqObj, val, isInit);
						}
					}
				},
				getComboBoxVal:function(obj, textKey) {
					var jqObj = $ES.getJqObj(obj);
					var data = jqObj.select2("data");
					var valArr = [];
					var key = textKey || "id";
					for(var i in data) {
						valArr.push(data[i][key]);
					}
					return valArr.join(",");
				},
				getComboBoxValText:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					var data = jqObj.select2("data");
					var valArr = [], textArr = [];
					for(var i in data) {
						valArr.push(data[i]["id"]);
						textArr.push(data[i]["caption2"]||data[i]["text"]);
					}
					return {"id":valArr.join(","),"text":textArr.join(",")};
				},
				getComboBoxResult:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					var data = jqObj.select2("data");
					return data;
				},
				getDefaultDataGridOpt:function() {
					return {
						editable:false,
						classes:"table table-hover table-no-bordered es-table",
				        striped : true,
				        sidePagination:'server',
						method:'post',
						idField:'id',
						contentType:"application/x-www-form-urlencoded",
						search:true,
						searchOnEnterKey:true,
						advancedSearch:true,
						showToggle:true,
						showRefresh:true,
						showColumns:true,
						showExport:false,
						reorderableColumns:true,
						resizable:true,
						mobileResponsive:true,
				        pagination : true,
				        clickToSelect:true,
				        paginationVAlign:"both",
				        paginationHAlign:"left",
				        paginationDetailHAlign:"right",
				        stickyHeader: true,
			            stickyHeaderOffsetY: $ES.getStickyHeaderOffsetY,
			            fixedColumns: false,
			            fixedNumber: 0,
			            iconSize:"sm",
			            storage:true
					}
				},
				initDataGrid:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.bootstrapTable(opt);
					if($ES.isNotBlank(opt.value)) {
						$ES.setDataGridVal(jqObj, opt.value);
					}
				},
				methodDataGrid:function(obj, method, param) {
					var jqObj = $ES.getJqObj(obj);
					return jqObj.bootstrapTable(method, param);
				},
				refreshDataGrid:function(obj) {
					$ES.methodDataGrid(obj, "refresh");
				},
				enableDataGrid:function(obj) {
					var options = $ES.methodDataGrid(obj,"getOptions");
					if(options.editable == false) {
						options.editable = true;
						$ES.methodDataGrid(obj, "refresh");
					}
				},
				disableDataGrid:function(obj) {
					var options = $ES.methodDataGrid(obj,"getOptions");
					if(options.editable == true) {
						options.editable = false;
						$ES.methodDataGrid(obj, "refresh");
					}
				},
				setDataGridVal:function(obj, val) {
					var ele = obj;
					var option = $ES.methodDataGrid(ele, "getOptions");
					var newUrl = option["esUrl"];
					if(newUrl /*&& option["sidePagination"]!="client"*/) {
						newUrl = newUrl + "&id="+val;
						option["esBindId"] = val;
						option["url"] = newUrl;
						option["sidePagination"] = "server";
						$ES.methodDataGrid(ele, "refresh");
					}
				},
				getDataGridData:function(obj, isSel) {
					var method = (!isSel)?"getData":"getSelections";
					var resultData = $ES.methodDataGrid(obj, method);
					return resultData;
				},
				addDataGridRow:function(obj, row) {
					var opt = $ES.methodDataGrid(obj,'getOptions');
					var newRow = $.extend(true, {}, opt.defaultRow, row);
					$ES.methodDataGrid(obj,'append', newRow);
				},
				getDataGridRowIndex:function(obj, row) {
					var data = $ES.methodDataGrid(obj,'getData');
					return data.indexOf(row);
				},
				insertDataGridRow:function(obj, row) {
					var selRows = $ES.methodDataGrid(obj,'getSelections');
					if(selRows && selRows.length) {
						var curRow = selRows[0];
						var index = $ES.getDataGridRowIndex(obj, curRow);
						var opt = $ES.methodDataGrid(obj,'getOptions');
						var newRow = $.extend(true, {}, opt.defaultRow, row);
						$ES.methodDataGrid(obj, "insertRow", {index:index, row:newRow});
						$ES.methodDataGrid(obj, "uncheckAll");
						$ES.methodDataGrid(obj, "check", index);
					} else {
						$MsgUtil.alert($.jwpf.system.confirm.selectInsertRow);
					}
				},
				deleteDataGridRow:function(obj) {
					var selRows = $ES.methodDataGrid(obj,'getSelections');
					if(selRows && selRows.length) {
						var data = $ES.methodDataGrid(obj,'getData');
						for(var len=selRows.length,i=len-1;i>=0;i--) {
							var row = selRows[i];
							data.splice(data.indexOf(row), 1);
						}
						$ES.methodDataGrid(obj,'load', data);
					} else {
						$MsgUtil.alert($.jwpf.system.confirm.selectRow);
					}
				},
				preSaveDataGrid:function(obj, opt) {
					
				},
				saveDataGrid:function(obj, opt) {
					$ES.preSaveDataGrid(obj, opt);
					var resultData = $ES.getDataGridData(obj);
					var rows = {"updated":[]};
					var oper = opt.oper;
					for(var k = 0,len = resultData.length; k < len; k++) {
						if(oper == "edit" || oper == "add") {
							resultData[k].id = null;
						}
						rows.updated.push(resultData[k]);
					}
					return rows;
				},
				initTree:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					opt = $ES.getDefaultSelectTreeOpt(opt, jqObj);
					jqObj.data("queryParams", opt.queryParams);
					jqObj.esTree(opt);
				},
				getTreeVal:function(obj, textKey) {
					var jqObj = $ES.getJqObj(obj);
					var key = textKey || "id";
					var result = jqObj.esTree("getValue");
					return result[key];
				},
				setTreeVal:function(obj, val) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.esTree("setValue", val);
				},
				getDefaultSelectTreeOpt:function(defaultOpt, jqObj) {
					var opt = $.extend(true, {}, defaultOpt);
					var url = __ctx + "/gs/gs-mng!queryAjaxResult.action";
					var filterUrl = __ctx + "/gs/gs-mng!queryAjaxResultByKey.action";
					if(opt["esType"] == "DictTree") {
						url = __ctx + "/sys/common/common-type!queryList.action";
						filterUrl = __ctx + "/sys/common/common-type!queryListByKey.action";
					}
					opt["url"] = opt["url"] || url;
					opt["filterUrl"] = opt["filterUrl"] || filterUrl; 
					opt["queryParams"]["rows"] = opt["queryParams"]["rows"] || 65535;
					if(jqObj.attr("multiple")) {
						opt.multiple = true;
					}
					return opt;
				},
				initSelectTree:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					opt = $ES.getDefaultSelectTreeOpt(opt, jqObj);
					jqObj.data("queryParams", opt.queryParams);
					jqObj.selectTree(opt);
					if($ES.isNotBlank(opt.value)) {//设置初始值
						$ES.setSelectTreeVal(jqObj, opt.value);
					}
				},
				getSelectTreeVal:function(obj, textKey) {
					var jqObj = $ES.getJqObj(obj);
					var key = textKey || "id";
					var result = jqObj.selectTree("getValue");
					return result[key];
				},
				setSelectTreeVal:function(obj, val) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.selectTree("setValue", val);
				},
				setSelectTreeValText:function(obj, val, text) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.selectTree("setValue", {"id":val,"text":text});
				},
				initRichTextBox:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					if(opt["format"] == "html") {
						jqObj.data("format", "html");
						jqObj.summernote(opt);
						if($ES.isNotBlank(opt.value)) {//设置初始值
							$ES.setRichTextBoxVal(jqObj, opt.value);
						}
						if(opt["onChange"]) {
							jqObj.off("summernote.change").on("summernote.change", function(we, contents, $editable) {
								opt["onChange"].call(this, contents);
							});
						}
					} else {
						jqObj.data("format", "text");
						if($ES.isNotBlank(opt.value)) {//设置初始值
							$ES.setRichTextBoxVal(jqObj, opt.value);
						}
						if(opt["onChange"]) {
							jqObj.off("input").on("input", function (event) {
								opt["onChange"].call(this, this.value);
							});
						}
					}
				},
				setRichTextBoxVal:function(obj, val) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.val(val);
					if(jqObj.data("format")=="html") {
						jqObj.summernote("code",val);
					}
				},
				getRichTextBoxVal:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					if(jqObj.data("format")=="html") {
						return jqObj.summernote("code");
					} else {
						return jqObj.val();
					}
				},
				enableRichTextBox:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					if(jqObj.data("format")=="html") {
						jqObj.removeProp("readonly");
					} else {
						jqObj.removeProp("readonly");
					}
				},
				disableRichTextBox:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					if(jqObj.data("format")=="html") {
						jqObj.prop("readonly", true);
					} else {
						jqObj.prop("readonly", true);
					}
				},
				initCheckBox:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					var jsonData = opt.data || $ES.getJsonObjByParam(opt.queryParams);
					var htmlStr = [];
					var checkType = "radio";
					opt.multiple = opt.multiple || (jqObj.attr("multiple")=="true" || jqObj.attr("multiple")=="multiple");
					if(opt.multiple) {
						checkType = "checkbox";
					}
					opt["esFieldKey"] = opt["esFieldKey"] || jqObj.attr("id");
					var className = checkType + "-inline";
					var checkId = opt["esFieldKey"] + (opt["esIndex"]||"");
					var checkName = opt["esFieldKey"] || checkId;
					for(var i in jsonData) {
						var json = jsonData[i];
						htmlStr.push("<label class='");
						htmlStr.push(className);
						htmlStr.push("'>");
						htmlStr.push("<input type='");
						htmlStr.push(checkType);
						htmlStr.push("' name='");
						htmlStr.push(checkName);
						htmlStr.push("' id='");
						htmlStr.push(checkId);
						htmlStr.push(i);
						htmlStr.push("' value='");
						htmlStr.push(json["id"]);
						htmlStr.push("' title='");
						htmlStr.push(json["text"]);
						htmlStr.push("' />");
						htmlStr.push(" ");
						htmlStr.push(json["text"]);
						htmlStr.push("</label>");
					}
					jqObj.html(htmlStr.join(""));
					if($ES.isNotBlank(opt.value)) {//设置初始值
						$ES.setCheckBoxVal(jqObj, opt.value);
					}
					$ES.bindCheckBoxEvent(jqObj, opt);
				},
				bindCheckBoxEvent:function(obj, eventObj) {
					var jqObj = $ES.getJqObj(obj);
					if(eventObj["onChange"]) {
						jqObj.find("input").off("change").on("change", function(event) {
							var val = $ES.getCheckBoxVal(obj);
							//var text = $ES.getCheckBoxVal(obj, "text");
							eventObj["onChange"].call(this, val);
						});
					}
				},
				setCheckBoxVal:function(obj, val) {
					var jqObj = $ES.getJqObj(obj);
					val = $ES.getStringIfBlank(val, "");
					var valArr = val.split(",");
					for(var i in valArr) {
						jqObj.find("input[value='" + valArr[i] + "']").prop("checked", true);
					}
				},
				getCheckBoxVal:function(obj, textKey) {
					var jqObj = $ES.getJqObj(obj);
					var data = [];
					var valArr = [];
					var key = textKey || "id";
					jqObj.find("input:checked").each(function() {
						if(key == "id") {
							valArr.push($(this).val());
						} else {
							valArr.push($(this).attr("title"));
						}
					});
					return valArr.join(",");
				},
				enableCheckBox:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.find("input").removeProp("disabled");
				},
				disableCheckBox:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.find("input").prop("disabled", true);
				},
				initCheckBoxGroup:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.checkboxgroup(opt);
				},
				reloadCheckBoxGroup:function(obj, param) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.checkboxgroup("reload", param);
				},
				getCheckBoxGroupVal:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					return jqObj.checkboxgroup("getValue");
				},
				destroyCheckBoxGroup:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.checkboxgroup("destroy");
				},
				initComboBoxGroup:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.comboboxgroup(opt);
				},
				reloadComboBoxGroup:function(obj, param) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.comboboxgroup("reload", param);
				},
				getComboBoxGroupVal:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					return jqObj.comboboxgroup("getValue");
				},
				destroyComboBoxGroup:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.comboboxgroup("destroy");
				},
				initDateBox:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					var newOpt = {};
					if(opt && opt["format"]) {
						//?replace("m","i"))?replace("M","m"))?replace("H","h")
						var fmt = opt["format"];
						fmt = fmt.replaceAll("m","i").replaceAll("M","m").replaceAll("H","h");
						if(fmt.indexOf("h") == -1 && fmt.indexOf("i") == -1
							&& fmt.indexOf("s") == -1) {
							newOpt["minView"] = 2;
						}
						newOpt["format"] = fmt;
					}
					opt = $.extend(true, {}, opt, {
						//"pickerPosition":"top-right", 
						"todayBtn":true,
						"todayHighlight":true,
						"minuteStep":1,
						"autoclose":true
					}, newOpt);
					jqObj.datetimepicker(opt);
					if($ES.isNotBlank(opt.value)) {//设置初始值
						$ES.setDateBoxVal(jqObj, opt.value);
					}
					if(opt["onChange"]) {
						jqObj.off("changeDate").on("changeDate", function (ev) {
							opt["onChange"].call(this, ev.target.value);
						});
					}
				},
				getDateBoxVal:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					return jqObj.val();
				},
				setDateBoxVal:function(obj, val) {
					var jqObj = $ES.getJqObj(obj);
					return jqObj.val(val);
				},
				initFileUploadBox:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.fileUploader(opt);
				},
				initFileBox:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.attr("name","esFile");
					opt.baseUrl = __ctx;
					jqObj.filebox(opt);
					if($ES.isNotBlank(opt.value)) {//设置初始值
						$ES.setFileBoxVal(jqObj, opt.value);
					}
				},
				setFileBoxVal:function(obj, val) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.filebox("setValue", val);
				},
				getFileBoxVal:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					return jqObj.filebox("getValue");
				},
				initSplitter:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.touchSplit(opt);
				},
				initDialogueWindow:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.dialoguewindow(opt);
					if($ES.isNotBlank(opt.value)) {//设置初始值
						$ES.setDialogueWindowVal(jqObj, opt.value);
					}
				},
				setDialogueWindowVal:function(obj, val) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.dialoguewindow("setValue", val);
				},
				getDialogueWindowVal:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					return jqObj.dialoguewindow("getValue");
				},
				initButton:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					if(opt["onclick"]) {
						jqObj.off("click").on("click", function (event) {
							opt["onclick"].call(this, $(this).attr("esValue"));
						});
					}
				},
				setButtonVal:function(obj, val, text) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.attr("esValue", val);
					if(text) {
						jqObj.html(text);
					}
				},
				getButtonVal:function() {
					var jqObj = $ES.getJqObj(obj);
					return jqObj.attr("esValue");
				},
				initFullCalendar:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					var defaultOpt = {
							header: {
								left: 'prev,next today',
								center: 'title',
								right: 'month,agendaWeek,agendaDay,listMonth'
							},
							defaultDate: $today$,
							//height:"auto",
							weekNumbers: true,
							navLinks: true, 
							editable: true,
							eventLimit: true,
							eventClick: function(event) {
								var el = $(this);
						        el.popover({
						        	title:event.title,
						            content: event.content,
						            html: true,
						            placement: "auto",
						            container: 'body',
						            trigger: 'hover'
						        });
						        el.popover('show');
								return false;
							}
					};
					var fcOpt = $.extend(true, {}, defaultOpt, opt);
					jqObj.fullCalendar(fcOpt);
				},
				refreshFullCalendar:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.fullCalendar("refetchEvents");
				},
				destroyFullCalendar:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.fullCalendar("destroy");
				},
				loadFormData:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					var data = opt["data"]||{};
					$.each(data, function(key, value) {
						jqObj.find("[name='" + key + "']").each(function() {
							var tagName = $(this)[0].tagName;
							var type = $(this).prop('type');
							if(tagName=='INPUT' || tagName=='TEXTAREA'){
								if(type=='radio'){
									$(this).prop('checked',$(this).val()==value);
								} else if(type=='checkbox'){
									value = $ES.getStringIfBlank(value, "");
									var arr = value.split(',');
									for(var i =0;i<arr.length;i++){
										if($(this).val()==arr[i]){
											$(this).prop('checked',true);
											break;
										}
									}
								} else{
									if($(this).hasClass("richtextbox")) {
										$ES.setRichTextBoxVal($(this),value);
									} else if($(this).hasClass("es-combotree")) {
										var textKey = key + "text";
										$ES.setSelectTreeValText($(this),value,data[textKey]);
									} else {
										$(this).val(value);
									}
								}
							} else if(tagName=='SELECT'){
								var textKey = key + "text";
								$ES.setComboBoxValText($(this),value,data[textKey]);
							}
						});
					});
				},
				getFormData:function(formObj) {
					var jqObj = $ES.getJqObj(formObj);
					var fields = jqObj.serializeArray();
			      	var jsonObj = {};
			      	$.each(fields, function(k, v) {
			      		 if (jsonObj[v.name]) {
			      			 if(v.value) {
			      				jsonObj[v.name] = jsonObj[v.name] + "," + v.value;
			      			 }
			      		 } else { 
			      			 jsonObj[v.name] = v.value || ''; 
			      		 } 
			      	}); 
			      	return jsonObj;
				},
				resetForm:function(formObj) {
					var jqObj = $ES.getJqObj(formObj);
					jqObj[0].reset();
				},
				validateForm:function(formObj) {
					var jqObj = $ES.getJqObj(formObj);
					return jqObj.isValid({},{},true);
					//return true;
				},
				initFormValidate:function(opt) {
					var defaultOpt = {
						modules : "html5",
						form: "#viewForm"
					};
					var option = $.extend(true, {}, defaultOpt, opt);
					$.validate(option);
				},
				doDisableForm:function(formObj) {
					var jqObj = $ES.getJqObj(formObj);
					jqObj.find("button,a.bt-extra,select,input.es-date,input:radio,input:checkbox").prop("disabled", true);
					jqObj.find("input,textarea").prop("readonly",true);
					jqObj.find("button.validInView").removeProp("disabled");
					jqObj.find("a.hyperlink").not(".validInView").removeProp("disabled");
					jqObj.find(".richtextbox").summernote("disable");
					var dgObj = jqObj.find("div.bootstrap-table");
					dgObj.find("button,input").removeProp("disabled");
					dgObj.find("input").removeProp("readonly");
			    },
				doEnableForm:function(formObj) {
					var jqObj = $ES.getJqObj(formObj);
					jqObj.find("button,a.bt-extra,select,input.es-date,input:radio,input:checkbox").removeProp("disabled");
					jqObj.find("input,textarea").removeProp("readonly");
					jqObj.find(".richtextbox").summernote("enable");
			    },
				doPrepareSaveForm:function(formObj) {
					var jqObj = $ES.getJqObj(formObj);
					jqObj.find("input,textarea,select").removeProp("disabled");
					jqObj.find("select").each(function() {
						var name = $(this).prop("name");
						name = "#" + name + "text";
						var text = $ES.getComboBoxVal($(this), "text");
						$ES.setTextBoxVal(name, text);
					});
					jqObj.find("div.es-radiobox,div.es-checkbox").each(function() {
						var name = $(this).attr("id");
						name = "#" + name + "text";
						var text = $ES.getCheckBoxVal($(this), "text");
						$ES.setTextBoxVal(name, text);
					});
					jqObj.find("textarea.richtextbox").each(function() {
						var val = $ES.getRichTextBoxVal($(this));
						$ES.setRichTextBoxVal($(this), val);
					});
					jqObj.find("input.es-combotree").each(function() {
						var name = $(this).prop("name");
						name = "#" + name + "text";
						var text = $ES.getSelectTreeVal($(this), "text");
						$ES.setTextBoxVal(name, text);
					});
				},//打开tab页或窗口
				doOpenUrl:function(title, url) {
					if(top.addTab) {
						top.addTab(title, url);
					} else {
						window.open(url, title);
					}
				},//获取状态列表
				getStatusList:function() {
					return $.jwpf.system.module.statusList;
				},
				getStatusDisplayStyleMap:function() {
					var dsMap = {
							"00":"label-info","10":"label-info","20":"label-primary","31":"label-success",
							"35":"label-success","32":"label-warning","40":"label-default"
					};
					return dsMap;
				},
				//获取状态显示
				getStatusDisplayStyle:function(status, rowData) {
					var dsMap = $ES.getStatusDisplayStyleMap();
					return sprintf('<span class="label %s">%s</span>', dsMap[status||""]||"label-info", $ES.getStatusDisplayValue(status, rowData["statusCaption"]));
				},//获取明细状态显示
				getSubStatusDisplayStyle:function(status, rowData) {
					var dsMap = $ES.getStatusDisplayStyleMap();
					return sprintf('<span class="label %s">%s</span>', dsMap[status||""]||"label-info", $.jwpf.system.module.statusSub[status] || rowData["statusCaption"]);
				},
				getStatusDisplayValue:function(status, defaultVal) {
					return $.jwpf.system.module.status[status] || defaultVal;
				},
				getPinYinList:function() {
					return [
						{"id":'a',"text":'A'},
						{"id":'b',"text":'B'},
						{"id":'c',"text":'C'},
						{"id":'d',"text":'D'},
						{"id":'e',"text":'E'},
						{"id":'f',"text":'F'},
						{"id":'g',"text":'G'},
						{"id":'h',"text":'H'},
						{"id":'i',"text":'I'},
						{"id":'j',"text":'J'},
						{"id":'k',"text":'K'},
						{"id":'l',"text":'L'},
						{"id":'m',"text":'M'},
						{"id":'n',"text":'N'},
						{"id":'o',"text":'O'},
						{"id":'p',"text":'P'},
						{"id":'q',"text":'Q'},
						{"id":'r',"text":'R'},
						{"id":'s',"text":'S'},
						{"id":'t',"text":'T'},
						{"id":'u',"text":'U'},
						{"id":'v',"text":'V'},
						{"id":'w',"text":'W'},
						{"id":'x',"text":'X'},
						{"id":'y',"text":'Y'},
						{"id":'z',"text":'Z'}
					];
				},//设置字段只读
				setFieldReadonly:function(obj, isReadonly){
					var object = $ES.getJqObj(obj);
					if(object.hasClass("es-combobox")) {
						if(isReadonly) {
							object.prop("disabled",true);
						} else {
							object.removeProp("disabled");
						}
					} else if(object.hasClass("es-combotree")) {
						if(isReadonly) {
							object.selectTree("disable");
						} else {
							object.selectTree("enable");
						}
					} else if(object.hasClass("es-checkbox")) {
						if(isReadonly) {
							$ES.disableCheckBox(object);
						} else {
							$ES.enableCheckBox(object);
						}
					} else if(object.hasClass("es-datebox")) {
						if(isReadonly) {
							object.prop("readonly", true);
							var onfocusStr = object.attr("onfocus");
							if(onfocusStr) {
								object.data("focusEvent",onfocusStr);
								object.removeAttr("onfocus");
							}
						} else {
							object.removeProp("readonly");
							var onfocusStr = object.data("focusEvent");
							if(onfocusStr) {
								object.attr("onfocus", onfocusStr);
								object.removeData("focusEvent");
							}
						}
					} else if(object.hasClass("es-dialoguewindow")) {
						if(isReadonly) {
							object.dialoguewindow("disable");
						} else {
							object.dialoguewindow("enable");
						}
					} else if(object.hasClass("es-richtextbox")) {
						if(isReadonly) {
							$ES.disableRichTextBox(object);
						} else {
							$ES.enableRichTextBox(object);
						}
					} else {
						if(isReadonly) {
							object.prop("readonly", true);
						} else {
							object.removeProp("readonly");
						}
					}		
				},
				initTabsCollapse:function() {
					$(".collapse-link").click(function() {
						var o = $(this).closest("div.ibox"),
							e = $(this).find("i"),
							i = o.find("div.ibox-content");
						i.slideToggle(200), e.toggleClass("fa-chevron-up").toggleClass("fa-chevron-down"), o.toggleClass("").toggleClass("border-bottom"), setTimeout(function() {
							o.resize(), o.find("[id^=map-]").resize();
						}, 50);
					});
					$(".tabs-collapse-link").click(function() {
						var o = $(this).closest("div.tabs-container"),
							e = $(this).find("i"),
							i = o.find("div.tab-content");
						i.slideToggle(200), e.toggleClass("fa-chevron-up").toggleClass("fa-chevron-down"), o.toggleClass("").toggleClass("border-bottom"), 
						setTimeout(function() {
							o.resize(), o.find("[id^=map-]").resize();
						}, 50);
					});
				},//根据attachId获取文件url
				getOpenerWin:function() {
					if(window.opener) {
						return window.opener.top;
					} else {
						return window.top;
					}
				},
				setPageValue:function(key, value) {
					this.getOpenerWin().PageStorage.write(key, value);
				},
				getPageValue:function(key) {
					return this.getOpenerWin().PageStorage.read(key);
				},
				removePageValue:function(key) {
					this.getOpenerWin().PageStorage.remove(key);
				},
				getQueryOperList:function() {
					return [
							{"id":'EQ',"text":'等于(=)'},
							{"id":'NE',"text":'不等于(!=)'},
							{"id":'LIKE',"text":'包含(CONTAINS)'},
							{"id":'LIKEL',"text":'开始于(START WITH)'},
							{"id":'LIKER',"text":'结束于(END WITH)'}
						];
				},
				initAdvanceQuery:function(opt) {
					if(!opt.isInit) {
						$ES.initComboBox("#queryFieldSel", {data:opt.data, multiple:true, width:'100%'
							,onSelect:function(data) {
								var strHtml = "";
								$(sprintf('#_qf_%(id)s_', data)).remove();
								switch(data.editType) {
									case "TextBox":	
									case "HyperLink":
										if(["dtInteger","dtLong","dtDouble"].indexOf(data.dataType) >= 0) {
											if(data.precision) {
												data.step = Math.pow(10, -1*parseInt(data.precision));
											} else {
												data.step = "1";
											}
											strHtml = sprintf(['<div id="_qf_%(id)s_" class="row form-group form-group-sm">',
											 '<div class="col-sm-2 form-horizontal text-right">',
											 '<label for="query_%(id)s" class="control-label">%(text)s</label>',
											 '</div>',
											 '<div class="col-sm-10 form-inline">',
											 '<div class="form-group">',
											 '<input type="number" class="form-control" placeholder="最小值" name="filter_GE%(dt)s_%(fieldKey)s" id="query_%(id)sStart" step="%(step)s">',
											 '</div>',
											 '<div class="form-group">',
											 ' -- <input type="number" class="form-control" placeholder="最大值" name="filter_LE%(dt)s_%(fieldKey)s" id="query_%(id)sEnd" step="%(step)s">',
											 '</div>',
											 '</div>',
											 '</div>'].join(""), data);
											$("#queryForm").append(strHtml);
											if($ES.isNotBlank(data.start)) {
												var queryKey = sprintf('#query_%(id)sStart',data);
												$(queryKey).val(data.start);
											}
											if($ES.isNotBlank(data.end)) {
												var queryKey = sprintf('#query_%(id)sEnd',data);
												$(queryKey).val(data.end);
											}
										} else {
											var oper = data.oper;
											if(oper!="USER") data.oper = "LIKE";
											strHtml = sprintf(['<div id="_qf_%(id)s_" class="row form-group form-group-sm">',
																 '<div class="col-sm-2 form-horizontal text-right">',
																 '<label for="query_%(id)s" class="control-label">%(text)s</label>',
																 '</div>',
																 '<div class="col-sm-10 form-inline">',
																 '<div class="form-group">',
																 '<select class="form-control" id="query_%(id)sCondSel" forId="query_%(id)s"></select>',
																 '</div>',
																 '<div class="form-group">',
																 '<input type="text" class="form-control" placeholder="%(text)s" name="filter_%(oper)sS_%(fieldKey)s" forName="%(fieldKey)s" id="query_%(id)s" >',
																 '</div>',
																 '</div>',
																 '</div>'].join(""), data);
											$("#queryForm").append(strHtml);
											var fieldKey = sprintf("#query_%(id)sCondSel",data);
											$ES.initComboBox(fieldKey, {"data":$ES.getQueryOperList(),"width":"160px", "onSelect":function(result) {
												var oper = result.id;
												var forId = $(this).attr("forId");
												var fieldName = $("#"+forId).attr("forName");
												//console.log(oper + "," + forId + "," + fieldName);
												$("#"+forId).attr("name", "filter_" + oper + "S_" + fieldName);
											}, "value":data.oper||"EQ"});
											if(oper=="USER") {
												//$ES.setComboBoxVal(fieldKey, "LIKE");
												$(fieldKey).prop("disabled", true);
											}
											if($ES.isNotBlank(data.val)) {
												var queryKey = sprintf('#query_%(id)s',data);
												$(queryKey).val(data.val);
											}
										}
										break;
									case "RichTextBox":
										data.oper = "LIKE";
										strHtml = sprintf(['<div id="_qf_%(id)s_" class="row form-group form-group-sm">',
															 '<div class="col-sm-2 form-horizontal text-right">',
															 '<label for="query_%(id)s" class="control-label">%(text)s</label>',
															 '</div>',
															 '<div class="col-sm-10 form-inline">',
															 '<div class="form-group">',
															 '<select class="form-control" id="query_%(id)sCondSel" forId="query_%(id)s"></select>',
															 '</div>',
															 '<div class="form-group">',
															 '<textarea class="form-control" placeholder="%(text)s" name="filter_%(oper)sS_%(fieldKey)s" forName="%(fieldKey)s" id="query_%(id)s" ></textarea>',
															 '</div>',
															 '</div>',
															 '</div>'].join(""), data);
										$("#queryForm").append(strHtml);
										var fieldKey = sprintf("#query_%(id)sCondSel",data);
										$ES.initComboBox(fieldKey, {"data":$ES.getQueryOperList(),"width":"160px", "onSelect":function(result) {
											var oper = result.id;
											var forId = $(this).attr("forId");
											var fieldName = $("#"+forId).attr("forName");
											$("#"+forId).attr("name", "filter_" + oper + "S_" + fieldName);
										}, "value":data.oper||"EQ"});
										if($ES.isNotBlank(data.val)) {
											var queryKey = sprintf('#query_%(id)s',data);
											$(queryKey).val(data.val);
										}
										break;
									case "DialogueWindow":
										strHtml = sprintf(['<div id="_qf_%(id)s_" class="row form-group form-group-sm">',
															 '<div class="col-sm-2 form-horizontal text-right">',
															 '<label for="query_%(id)stext" class="control-label">%(text)s</label>',
															 '</div>',
															 '<div class="col-sm-10 form-inline">',
															 '<div class="form-group">',
															 '<select class="form-control" id="query_%(id)sCondSel" forId="query_%(id)stext"></select>',
															 '</div>',
															 '<div class="form-group">',
															 '<input type="text" class="form-control" placeholder="%(text)s" name="filter_EQS_%(fieldKey)stext" forName="%(fieldKey)stext" id="query_%(id)stext" >',
															 '</div>',
															 '</div>',
															 '</div>'].join(""), data);
										$("#queryForm").append(strHtml);
										var fieldKey = sprintf("#query_%(id)sCondSel",data);
										$ES.initComboBox(fieldKey, {"data":$ES.getQueryOperList(),"width":"160px", "onSelect":function(result) {
											var oper = result.id;
											var forId = $(this).attr("forId");
											var fieldName = $("#"+forId).attr("forName");
											$("#"+forId).attr("name", "filter_" + oper + "S_" + fieldName);
										}, "value":data.oper||"EQ"});
										if($ES.isNotBlank(data.val)) {
											var queryKey = sprintf('#query_%(id)stext',data)
											$(queryKey).val(data.val);
										}
										break;
									case "DateBox":
										data.fmt = data.fmt || "yyyy-MM-dd";//默认日期格式
										strHtml = sprintf(['<div id="_qf_%(id)s_" class="row form-group form-group-sm">',
										 '<div class="col-sm-2 form-horizontal text-right">',
										 '<label for="query_%(id)s" class="control-label">%(text)s</label>',
										 '</div>',
										 '<div class="col-sm-10 form-inline">',
										 '<div class="form-group">',
										 '<input type="text" class="Wdate form-control input-sm input-sm-date" placeholder="起始值" name="filter_GE%(dt)s_%(fieldKey)s" id="query_%(id)sStart" ',
										 ' onclick="WdatePicker({dateFmt:\'%(fmt)s\',onpicked:function(dp) {if(!$dp.$(\'query_%(id)sEnd\').value)$dp.$(\'query_%(id)sEnd\').value=this.value;}})">',
										 '</div>',
										 '<div class="form-group">',
										 ' -- <input type="text" class="Wdate form-control input-sm input-sm-date" placeholder="结束值" name="filter_LE%(dt)s_%(fieldKey)s" id="query_%(id)sEnd"',
										 ' onclick="WdatePicker({dateFmt:\'%(fmt)s\'})" >',
										 '</div>',
										 '</div>',
										 '</div>'].join(""),data);
										$("#queryForm").append(strHtml);
										if($ES.isNotBlank(data.start)) {
											var queryKey = sprintf('#query_%(id)sStart',data);
											$(queryKey).val(data.start);
										}
										if($ES.isNotBlank(data.end)) {
											var queryKey = sprintf('#query_%(id)sEnd',data);
											$(queryKey).val(data.end);
										}
										break;
									case "ComboBox":
									case "ComboBoxSearch":
									case "CheckBox":
									case "RadioBox":
									case "ComboCheckBox":
									case "ComboRadioBox":
										strHtml = sprintf(['<div id="_qf_%(id)s_" class="row form-group form-group-sm">',
															 '<div class="col-sm-2 form-horizontal text-right">',
															 '<label for="query_%(id)s" class="control-label">%(text)s</label>',
															 '</div>',
															 '<div class="col-sm-10 form-inline">',
															 '<div class="form-group">',
															 '<select class="form-control combobox" name="filter_IN%(dt)s_%(fieldKey)s" id="query_%(id)s" multiple="true"><select>',
															 '</div>',
															 '</div>',
															 '</div>'].join(""), data);
										$("#queryForm").append(strHtml);
										var fieldKey = sprintf("#query_%(id)s", data);
										var selDataKey = sprintf("query_%(id)sJson", data);
										if(window[selDataKey]) {
											$ES.initComboBox(fieldKey, {"data":window[selDataKey],"width":"500px", "value":data.val});
										} else {
											var selDataParam = sprintf("query_%(id)sJsonParam", data);
											if(window[selDataParam]) {
												$ES.initComboBox(fieldKey, {"queryParams":window[selDataParam],"width":"500px", "value":data.val});
											}
										}
										break;
									case "ComboTree":
									case "DictTree":
										//alert("亲，暂不支持该控件查询！");
										strHtml = sprintf(['<div id="_qf_%(id)s_" class="row form-group form-group-sm">',
															 '<div class="col-sm-2 form-horizontal text-right">',
															 '<label for="query_%(id)s" class="control-label">%(text)s</label>',
															 '</div>',
															 '<div class="form-group">',
															 '<input type="text" class="form-control combotree" placeholder="%(text)s" name="filter_IN%(dt)s_%(fieldKey)s" multiple="true" id="query_%(id)s" notAutoWidth="true">',
															 '</div>',
															 '</div>',
															 '</div>'].join(""), data);
										$("#queryForm").append(strHtml);
										var fieldKey = sprintf("#query_%(id)s", data);
										var selDataKey = sprintf("query_%(id)sJson", data);
										if(window[selDataKey]) {
											$ES.initSelectTree(fieldKey, {"data":window[selDataKey],"value":data.val, "esType":(data.editType=="DictTree")?"DictTree":"", "theme":"bootstrap"});
										} else {
											var selDataParam = sprintf("query_%(id)sJsonParam", data);
											if(window[selDataParam]) {
												$ES.initSelectTree(fieldKey, {"queryParams":window[selDataParam],"value":data.val, "esType":(data.editType=="DictTree")?"DictTree":"", "theme":"bootstrap"});
											}
										}
										break;
									default:
										break;
										
								}
							}
							,onUnSelect:function(data) {
								$(sprintf('#_qf_%(id)s_', data)).remove();
							}});
						opt["isInit"] = true;
						$("#queryFieldSel").data("_data_",opt);
						var dk = "_advf_" + opt["entityName"];
						var valMap = store.get(dk);
						$ES.doInitQueryFieldSel(opt.data, valMap);
						$("#queryForm").on('keydown', 'input,textarea', function(e) {if(e.keyCode==13) {doQuery();return false;}});
					}
				},
				doResetAdvanceQuery:function(formKey) {
					var fk = "#" + (formKey || "queryForm");
					$(fk).find("select.combobox").val(null).trigger("change");
					$(fk).find("input,textarea").val("");
					//$("#queryForm")[0] && $("#queryForm")[0].reset();
				},//设置高级查询值，{"fieldKey":{val:"", oper:"", start:"", end:""}}
				doSetAdvanceQueryValue:function(valMap) {
					var opt = $("#queryFieldSel").data("_data_") || window._ad_query_opt_;
					if(opt) {
						$ES.initAdvanceQuery(opt);
						$ES.doInitQueryFieldSel(opt.data, valMap);
					}
				},
				doInitQueryFieldSel:function(data, valMap) {
					if(data && valMap) {
						var selKeyArr = $ES.getComboBoxVal("#queryFieldSel").split(",")||[];
						for(var key in valMap) {
							var mapVal = valMap[key];
							var rs = $ES.findInArray(data, "id", key);
							if(rs && rs.length) {
								var row = $.extend(true, {}, rs[0], mapVal);
								$("#queryFieldSel").trigger({
							        type: 'select2:select',
							        params: {
							            data: row
							        }
							    });
								selKeyArr.push(key);
							}
						}
						$ES.setComboBoxVal("#queryFieldSel", selKeyArr.join(","));
					}
				},
				doAddAQvToFavorite:function(callFunc) {
					var selKeyStr = $ES.getComboBoxVal("#queryFieldSel");
					var opt = $("#queryFieldSel").data("_data_");
					var dk = "_advf_" + opt["entityName"];
					if(selKeyStr) {
						var selKeyArr = selKeyStr.split(",");
						var data = {};
						selKeyArr.forEach(function(value, index) {
							data[value] = {};
						});
						store.set(dk, data);
						if(callFunc) callFunc(dk, data);
					} else {
						store.remove(dk);
						alert("已清空收藏！");
					}
				},
				findInArray:function(arr, key, keyVal) {
					return arr.filter(function(element, index, array) {
						return element[key] == keyVal;
					});
				}
			};	   
/* store.js - Copyright (c) 2010-2017 Marcus Westin */
!function(t){if("object"==typeof exports&&"undefined"!=typeof module)module.exports=t();else if("function"==typeof define&&define.amd)define([],t);else{var e;e="undefined"!=typeof window?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:this,e.store=t()}}(function(){var define,module,exports;return function t(e,n,r){function o(u,s){if(!n[u]){if(!e[u]){var a="function"==typeof require&&require;if(!s&&a)return a(u,!0);if(i)return i(u,!0);var c=new Error("Cannot find module '"+u+"'");throw c.code="MODULE_NOT_FOUND",c}var f=n[u]={exports:{}};e[u][0].call(f.exports,function(t){var n=e[u][1][t];return o(n?n:t)},f,f.exports,t,e,n,r)}return n[u].exports}for(var i="function"==typeof require&&require,u=0;u<r.length;u++)o(r[u]);return o}({1:[function(t,e,n){"use strict";var r=t("../src/store-engine"),o=t("../storages/all"),i=t("../plugins/all");e.exports=r.createStore(o,i)},{"../plugins/all":2,"../src/store-engine":15,"../storages/all":17}],2:[function(t,e,n){"use strict";e.exports=[t("./compression"),t("./defaults"),t("./dump"),t("./events"),t("./observe"),t("./expire"),t("./json2"),t("./operations"),t("./update"),t("./v1-backcompat")]},{"./compression":3,"./defaults":4,"./dump":5,"./events":6,"./expire":7,"./json2":8,"./observe":11,"./operations":12,"./update":13,"./v1-backcompat":14}],3:[function(t,e,n){"use strict";function r(){function t(t,e){var n=t(e);if(!n)return n;var r=o.decompress(n);return null==r?n:this._deserialize(r)}function e(t,e,n){var r=o.compress(this._serialize(n));t(e,r)}return{get:t,set:e}}var o=t("./lib/lz-string");e.exports=r},{"./lib/lz-string":10}],4:[function(t,e,n){"use strict";function r(){function t(t,e){n=e}function e(t,e){var r=t();return void 0!==r?r:n[e]}var n={};return{defaults:t,get:e}}e.exports=r},{}],5:[function(t,e,n){"use strict";function r(){function t(t){var e={};return this.each(function(t,n){e[n]=t}),e}return{dump:t}}e.exports=r},{}],6:[function(t,e,n){"use strict";function r(){function t(t,e,n){return c.on(e,u(this,n))}function e(t,e){c.off(e)}function n(t,e,n){c.once(e,u(this,n))}function r(t,e,n){var r=this.get(e);t(),c.fire(e,n,r)}function i(t,e){var n=this.get(e);t(),c.fire(e,void 0,n)}function a(t){var e={};this.each(function(t,n){e[n]=t}),t(),s(e,function(t,e){c.fire(e,void 0,t)})}var c=o();return{watch:t,unwatch:e,once:n,set:r,remove:i,clearAll:a}}function o(){return a(f,{_id:0,_subSignals:{},_subCallbacks:{}})}var i=t("../src/util"),u=i.bind,s=i.each,a=i.create,c=i.slice;e.exports=r;var f={_id:null,_subCallbacks:null,_subSignals:null,on:function(t,e){return this._subCallbacks[t]||(this._subCallbacks[t]={}),this._id+=1,this._subCallbacks[t][this._id]=e,this._subSignals[this._id]=t,this._id},off:function(t){var e=this._subSignals[t];delete this._subCallbacks[e][t],delete this._subSignals[t]},once:function(t,e){var n=this.on(t,u(this,function(){e.apply(this,arguments),this.off(n)}))},fire:function(t){var e=c(arguments,1);s(this._subCallbacks[t],function(t){t.apply(this,e)})}}},{"../src/util":16}],7:[function(t,e,n){"use strict";function r(){function t(t,e,n,r){return this.hasNamespace(o)||s.set(e,r),t()}function e(t,e){return this.hasNamespace(o)||u.call(this,e),t()}function n(t,e){return this.hasNamespace(o)||s.remove(e),t()}function r(t,e){return s.get(e)}function i(t){var e=[];this.each(function(t,n){e.push(n)});for(var n=0;n<e.length;n++)u.call(this,e[n])}function u(t){var e=s.get(t,Number.MAX_VALUE);e<=(new Date).getTime()&&(this.raw.remove(t),s.remove(t))}var s=this.createStore(this.storage,null,this._namespacePrefix+o);return{set:t,get:e,remove:n,getExpiration:r,removeExpiredKeys:i}}var o="expire_mixin";e.exports=r},{}],8:[function(t,e,n){"use strict";function r(){return t("./lib/json2"),{}}e.exports=r},{"./lib/json2":9}],9:[function(require,module,exports){"use strict";var _typeof="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(t){return typeof t}:function(t){return t&&"function"==typeof Symbol&&t.constructor===Symbol&&t!==Symbol.prototype?"symbol":typeof t};"object"!==("undefined"==typeof JSON?"undefined":_typeof(JSON))&&(JSON={}),function(){function f(t){return t<10?"0"+t:t}function this_value(){return this.valueOf()}function quote(t){return rx_escapable.lastIndex=0,rx_escapable.test(t)?'"'+t.replace(rx_escapable,function(t){var e=meta[t];return"string"==typeof e?e:"\\u"+("0000"+t.charCodeAt(0).toString(16)).slice(-4)})+'"':'"'+t+'"'}function str(t,e){var n,r,o,i,u,s=gap,a=e[t];switch(a&&"object"===("undefined"==typeof a?"undefined":_typeof(a))&&"function"==typeof a.toJSON&&(a=a.toJSON(t)),"function"==typeof rep&&(a=rep.call(e,t,a)),"undefined"==typeof a?"undefined":_typeof(a)){case"string":return quote(a);case"number":return isFinite(a)?String(a):"null";case"boolean":case"null":return String(a);case"object":if(!a)return"null";if(gap+=indent,u=[],"[object Array]"===Object.prototype.toString.apply(a)){for(i=a.length,n=0;n<i;n+=1)u[n]=str(n,a)||"null";return o=0===u.length?"[]":gap?"[\n"+gap+u.join(",\n"+gap)+"\n"+s+"]":"["+u.join(",")+"]",gap=s,o}if(rep&&"object"===("undefined"==typeof rep?"undefined":_typeof(rep)))for(i=rep.length,n=0;n<i;n+=1)"string"==typeof rep[n]&&(r=rep[n],o=str(r,a),o&&u.push(quote(r)+(gap?": ":":")+o));else for(r in a)Object.prototype.hasOwnProperty.call(a,r)&&(o=str(r,a),o&&u.push(quote(r)+(gap?": ":":")+o));return o=0===u.length?"{}":gap?"{\n"+gap+u.join(",\n"+gap)+"\n"+s+"}":"{"+u.join(",")+"}",gap=s,o}}var rx_one=/^[\],:{}\s]*$/,rx_two=/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,rx_three=/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,rx_four=/(?:^|:|,)(?:\s*\[)+/g,rx_escapable=/[\\"\u0000-\u001f\u007f-\u009f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,rx_dangerous=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;"function"!=typeof Date.prototype.toJSON&&(Date.prototype.toJSON=function(){return isFinite(this.valueOf())?this.getUTCFullYear()+"-"+f(this.getUTCMonth()+1)+"-"+f(this.getUTCDate())+"T"+f(this.getUTCHours())+":"+f(this.getUTCMinutes())+":"+f(this.getUTCSeconds())+"Z":null},Boolean.prototype.toJSON=this_value,Number.prototype.toJSON=this_value,String.prototype.toJSON=this_value);var gap,indent,meta,rep;"function"!=typeof JSON.stringify&&(meta={"\b":"\\b","\t":"\\t","\n":"\\n","\f":"\\f","\r":"\\r",'"':'\\"',"\\":"\\\\"},JSON.stringify=function(t,e,n){var r;if(gap="",indent="","number"==typeof n)for(r=0;r<n;r+=1)indent+=" ";else"string"==typeof n&&(indent=n);if(rep=e,e&&"function"!=typeof e&&("object"!==("undefined"==typeof e?"undefined":_typeof(e))||"number"!=typeof e.length))throw new Error("JSON.stringify");return str("",{"":t})}),"function"!=typeof JSON.parse&&(JSON.parse=function(text,reviver){function walk(t,e){var n,r,o=t[e];if(o&&"object"===("undefined"==typeof o?"undefined":_typeof(o)))for(n in o)Object.prototype.hasOwnProperty.call(o,n)&&(r=walk(o,n),void 0!==r?o[n]=r:delete o[n]);return reviver.call(t,e,o)}var j;if(text=String(text),rx_dangerous.lastIndex=0,rx_dangerous.test(text)&&(text=text.replace(rx_dangerous,function(t){return"\\u"+("0000"+t.charCodeAt(0).toString(16)).slice(-4)})),rx_one.test(text.replace(rx_two,"@").replace(rx_three,"]").replace(rx_four,"")))return j=eval("("+text+")"),"function"==typeof reviver?walk({"":j},""):j;throw new SyntaxError("JSON.parse")})}()},{}],10:[function(t,e,n){"use strict";var r=function(){function t(t,e){if(!o[t]){o[t]={};for(var n=0;n<t.length;n++)o[t][t.charAt(n)]=n}return o[t][e]}var e=String.fromCharCode,n="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",r="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-$",o={},i={compressToBase64:function(t){if(null==t)return"";var e=i._compress(t,6,function(t){return n.charAt(t)});switch(e.length%4){default:case 0:return e;case 1:return e+"===";case 2:return e+"==";case 3:return e+"="}},decompressFromBase64:function(e){return null==e?"":""==e?null:i._decompress(e.length,32,function(r){return t(n,e.charAt(r))})},compressToUTF16:function(t){return null==t?"":i._compress(t,15,function(t){return e(t+32)})+" "},decompressFromUTF16:function(t){return null==t?"":""==t?null:i._decompress(t.length,16384,function(e){return t.charCodeAt(e)-32})},compressToUint8Array:function(t){for(var e=i.compress(t),n=new Uint8Array(2*e.length),r=0,o=e.length;r<o;r++){var u=e.charCodeAt(r);n[2*r]=u>>>8,n[2*r+1]=u%256}return n},decompressFromUint8Array:function(t){if(null===t||void 0===t)return i.decompress(t);for(var n=new Array(t.length/2),r=0,o=n.length;r<o;r++)n[r]=256*t[2*r]+t[2*r+1];var u=[];return n.forEach(function(t){u.push(e(t))}),i.decompress(u.join(""))},compressToEncodedURIComponent:function(t){return null==t?"":i._compress(t,6,function(t){return r.charAt(t)})},decompressFromEncodedURIComponent:function(e){return null==e?"":""==e?null:(e=e.replace(/ /g,"+"),i._decompress(e.length,32,function(n){return t(r,e.charAt(n))}))},compress:function(t){return i._compress(t,16,function(t){return e(t)})},_compress:function(t,e,n){if(null==t)return"";var r,o,i,u={},s={},a="",c="",f="",l=2,p=3,h=2,d=[],g=0,v=0;for(i=0;i<t.length;i+=1)if(a=t.charAt(i),Object.prototype.hasOwnProperty.call(u,a)||(u[a]=p++,s[a]=!0),c=f+a,Object.prototype.hasOwnProperty.call(u,c))f=c;else{if(Object.prototype.hasOwnProperty.call(s,f)){if(f.charCodeAt(0)<256){for(r=0;r<h;r++)g<<=1,v==e-1?(v=0,d.push(n(g)),g=0):v++;for(o=f.charCodeAt(0),r=0;r<8;r++)g=g<<1|1&o,v==e-1?(v=0,d.push(n(g)),g=0):v++,o>>=1}else{for(o=1,r=0;r<h;r++)g=g<<1|o,v==e-1?(v=0,d.push(n(g)),g=0):v++,o=0;for(o=f.charCodeAt(0),r=0;r<16;r++)g=g<<1|1&o,v==e-1?(v=0,d.push(n(g)),g=0):v++,o>>=1}l--,0==l&&(l=Math.pow(2,h),h++),delete s[f]}else for(o=u[f],r=0;r<h;r++)g=g<<1|1&o,v==e-1?(v=0,d.push(n(g)),g=0):v++,o>>=1;l--,0==l&&(l=Math.pow(2,h),h++),u[c]=p++,f=String(a)}if(""!==f){if(Object.prototype.hasOwnProperty.call(s,f)){if(f.charCodeAt(0)<256){for(r=0;r<h;r++)g<<=1,v==e-1?(v=0,d.push(n(g)),g=0):v++;for(o=f.charCodeAt(0),r=0;r<8;r++)g=g<<1|1&o,v==e-1?(v=0,d.push(n(g)),g=0):v++,o>>=1}else{for(o=1,r=0;r<h;r++)g=g<<1|o,v==e-1?(v=0,d.push(n(g)),g=0):v++,o=0;for(o=f.charCodeAt(0),r=0;r<16;r++)g=g<<1|1&o,v==e-1?(v=0,d.push(n(g)),g=0):v++,o>>=1}l--,0==l&&(l=Math.pow(2,h),h++),delete s[f]}else for(o=u[f],r=0;r<h;r++)g=g<<1|1&o,v==e-1?(v=0,d.push(n(g)),g=0):v++,o>>=1;l--,0==l&&(l=Math.pow(2,h),h++)}for(o=2,r=0;r<h;r++)g=g<<1|1&o,v==e-1?(v=0,d.push(n(g)),g=0):v++,o>>=1;for(;;){if(g<<=1,v==e-1){d.push(n(g));break}v++}return d.join("")},decompress:function(t){return null==t?"":""==t?null:i._decompress(t.length,32768,function(e){return t.charCodeAt(e)})},_decompress:function(t,n,r){var o,i,u,s,a,c,f,l,p=[],h=4,d=4,g=3,v="",m=[],y={val:r(0),position:n,index:1};for(i=0;i<3;i+=1)p[i]=i;for(s=0,c=Math.pow(2,2),f=1;f!=c;)a=y.val&y.position,y.position>>=1,0==y.position&&(y.position=n,y.val=r(y.index++)),s|=(a>0?1:0)*f,f<<=1;switch(o=s){case 0:for(s=0,c=Math.pow(2,8),f=1;f!=c;)a=y.val&y.position,y.position>>=1,0==y.position&&(y.position=n,y.val=r(y.index++)),s|=(a>0?1:0)*f,f<<=1;l=e(s);break;case 1:for(s=0,c=Math.pow(2,16),f=1;f!=c;)a=y.val&y.position,y.position>>=1,0==y.position&&(y.position=n,y.val=r(y.index++)),s|=(a>0?1:0)*f,f<<=1;l=e(s);break;case 2:return""}for(p[3]=l,u=l,m.push(l);;){if(y.index>t)return"";for(s=0,c=Math.pow(2,g),f=1;f!=c;)a=y.val&y.position,y.position>>=1,0==y.position&&(y.position=n,y.val=r(y.index++)),s|=(a>0?1:0)*f,f<<=1;switch(l=s){case 0:for(s=0,c=Math.pow(2,8),f=1;f!=c;)a=y.val&y.position,y.position>>=1,0==y.position&&(y.position=n,y.val=r(y.index++)),s|=(a>0?1:0)*f,f<<=1;p[d++]=e(s),l=d-1,h--;break;case 1:for(s=0,c=Math.pow(2,16),f=1;f!=c;)a=y.val&y.position,y.position>>=1,0==y.position&&(y.position=n,y.val=r(y.index++)),s|=(a>0?1:0)*f,f<<=1;p[d++]=e(s),l=d-1,h--;break;case 2:return m.join("")}if(0==h&&(h=Math.pow(2,g),g++),p[l])v=p[l];else{if(l!==d)return null;v=u+u.charAt(0)}m.push(v),p[d++]=u+v.charAt(0),h--,u=v,0==h&&(h=Math.pow(2,g),g++)}}};return i}();"function"==typeof define&&define.amd?define(function(){return r}):"undefined"!=typeof e&&null!=e&&(e.exports=r)},{}],11:[function(t,e,n){"use strict";function r(){function t(t,e,n){var r=this.watch(e,n);return n(this.get(e)),r}function e(t,e){this.unwatch(e)}return{observe:t,unobserve:e}}var o=t("./events");e.exports=[o,r]},{"./events":6}],12:[function(t,e,n){"use strict";function r(){function t(t,e,n,r,o,i){return a.call(this,"push",arguments)}function e(t,e){return a.call(this,"pop",arguments)}function n(t,e){return a.call(this,"shift",arguments)}function r(t,e,n,r,o,i){return a.call(this,"unshift",arguments)}function i(t,e,n,r,i,a){var c=u(arguments,2);return this.update(e,{},function(t){if("object"!=("undefined"==typeof t?"undefined":o(t)))throw new Error('store.assign called for non-object value with key "'+e+'"');return c.unshift(t),s.apply(Object,c)})}function a(t,e){var n,r=e[1],o=u(e,2);return this.update(r,[],function(e){n=Array.prototype[t].apply(e,o)}),n}return{push:t,pop:e,shift:n,unshift:r,assign:i}}var o="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(t){return typeof t}:function(t){return t&&"function"==typeof Symbol&&t.constructor===Symbol&&t!==Symbol.prototype?"symbol":typeof t},i=t("../src/util"),u=i.slice,s=i.assign,a=t("./update");e.exports=[a,r]},{"../src/util":16,"./update":13}],13:[function(t,e,n){"use strict";function r(){function t(t,e,n,r){3==arguments.length&&(r=n,n=void 0);var o=this.get(e,n),i=r(o);this.set(e,void 0!=i?i:o)}return{update:t}}e.exports=r},{}],14:[function(t,e,n){"use strict";function r(){return this.disabled=!this.enabled,{has:o,transact:i,clear:u,forEach:s,getAll:a,serialize:c,deserialize:f}}function o(t,e){return void 0!==this.get(e)}function i(t,e,n,r){null==r&&(r=n,n=null),null==n&&(n={});var o=this.get(e,n),i=r(o);this.set(e,void 0===i?o:i)}function u(t){return this.clearAll.call(this)}function s(t,e){return this.each.call(this,function(t,n){e(n,t)})}function a(t){return this.dump.call(this)}function c(t,e){return JSON.stringify(e)}function f(t,e){if("string"==typeof e)try{return JSON.parse(e)}catch(n){return e||void 0}}var l=t("./dump"),p=t("./json2");e.exports=[l,p,r]},{"./dump":5,"./json2":8}],15:[function(t,e,n){"use strict";function r(){var t="undefined"==typeof console?null:console;if(t){var e=t.warn?t.warn:t.log;e.apply(t,arguments)}}function o(t,e,n){n||(n=""),t&&!l(t)&&(t=[t]),e&&!l(e)&&(e=[e]);var o=n?"__storejs_"+n+"_":"",i=n?new RegExp("^"+o):null,g=/^[a-zA-Z0-9_\-]*$/;if(!g.test(n))throw new Error("store.js namespaces can only have alphanumerics + underscores and dashes");var v={_namespacePrefix:o,_namespaceRegexp:i,_testStorage:function(t){try{var e="__storejs__test__";t.write(e,e);var n=t.read(e)===e;return t.remove(e),n}catch(r){return!1}},_assignPluginFnProp:function(t,e){var n=this[e];this[e]=function(){function e(){if(n)return a(arguments,function(t,e){r[e]=t}),n.apply(o,r)}var r=u(arguments,0),o=this,i=[e].concat(r);return t.apply(o,i)}},_serialize:function(t){return JSON.stringify(t)},_deserialize:function(t,e){if(!t)return e;var n="";try{n=JSON.parse(t)}catch(r){n=t}return void 0!==n?n:e},_addStorage:function(t){this.enabled||this._testStorage(t)&&(this.storage=t,this.enabled=!0)},_addPlugin:function(t){var e=this;if(l(t))return void a(t,function(t){e._addPlugin(t)});var n=s(this.plugins,function(e){return t===e});if(!n){if(this.plugins.push(t),!p(t))throw new Error("Plugins must be function values that return objects");var r=t.call(this);if(!h(r))throw new Error("Plugins must return an object of function properties");a(r,function(n,r){if(!p(n))throw new Error("Bad plugin property: "+r+" from plugin "+t.name+". Plugins should only return functions.");e._assignPluginFnProp(n,r)})}},addStorage:function(t){r("store.addStorage(storage) is deprecated. Use createStore([storages])"),this._addStorage(t)}},m=f(v,d,{plugins:[]});return m.raw={},a(m,function(t,e){p(t)&&(m.raw[e]=c(m,t))}),a(t,function(t){m._addStorage(t)}),a(e,function(t){m._addPlugin(t)}),m}var i=t("./util"),u=i.slice,s=i.pluck,a=i.each,c=i.bind,f=i.create,l=i.isList,p=i.isFunction,h=i.isObject;e.exports={createStore:o};var d={version:"2.0.12",enabled:!1,get:function(t,e){var n=this.storage.read(this._namespacePrefix+t);return this._deserialize(n,e)},set:function(t,e){return void 0===e?this.remove(t):(this.storage.write(this._namespacePrefix+t,this._serialize(e)),e)},remove:function(t){this.storage.remove(this._namespacePrefix+t)},each:function(t){var e=this;this.storage.each(function(n,r){t.call(e,e._deserialize(n),(r||"").replace(e._namespaceRegexp,""))})},clearAll:function(){this.storage.clearAll()},hasNamespace:function(t){return this._namespacePrefix=="__storejs_"+t+"_"},createStore:function(){return o.apply(this,arguments)},addPlugin:function(t){this._addPlugin(t)},namespace:function(t){return o(this.storage,this.plugins,t)}}},{"./util":16}],16:[function(t,e,n){(function(t){"use strict";function n(){return Object.assign?Object.assign:function(t,e,n,r){for(var o=1;o<arguments.length;o++)s(Object(arguments[o]),function(e,n){t[n]=e});return t}}function r(){if(Object.create)return function(t,e,n,r){var o=u(arguments,1);return h.apply(this,[Object.create(t)].concat(o))};var t=function(){};return function(e,n,r,o){var i=u(arguments,1);return t.prototype=e,h.apply(this,[new t].concat(i))}}function o(){return String.prototype.trim?function(t){return String.prototype.trim.call(t)}:function(t){return t.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,"")}}function i(t,e){return function(){return e.apply(t,Array.prototype.slice.call(arguments,0))}}function u(t,e){return Array.prototype.slice.call(t,e||0)}function s(t,e){c(t,function(t,n){return e(t,n),!1})}function a(t,e){var n=f(t)?[]:{};return c(t,function(t,r){return n[r]=e(t,r),!1}),n}function c(t,e){if(f(t)){for(var n=0;n<t.length;n++)if(e(t[n],n))return t[n]}else for(var r in t)if(t.hasOwnProperty(r)&&e(t[r],r))return t[r]}function f(t){return null!=t&&"function"!=typeof t&&"number"==typeof t.length}function l(t){return t&&"[object Function]"==={}.toString.call(t)}function p(t){return t&&"[object Object]"==={}.toString.call(t)}var h=n(),d=r(),g=o(),v="undefined"!=typeof window?window:t;e.exports={assign:h,create:d,trim:g,bind:i,slice:u,each:s,map:a,pluck:c,isList:f,isFunction:l,isObject:p,Global:v}}).call(this,"undefined"!=typeof global?global:"undefined"!=typeof self?self:"undefined"!=typeof window?window:{})},{}],17:[function(t,e,n){"use strict";e.exports=[t("./localStorage"),t("./oldFF-globalStorage"),t("./oldIE-userDataStorage"),t("./cookieStorage"),t("./sessionStorage"),t("./memoryStorage")]},{"./cookieStorage":18,"./localStorage":19,"./memoryStorage":20,"./oldFF-globalStorage":21,"./oldIE-userDataStorage":22,"./sessionStorage":23}],18:[function(t,e,n){"use strict";function r(t){if(!t||!a(t))return null;var e="(?:^|.*;\\s*)"+escape(t).replace(/[\-\.\+\*]/g,"\\$&")+"\\s*\\=\\s*((?:[^;](?!;))*[^;]?).*";return unescape(p.cookie.replace(new RegExp(e),"$1"))}function o(t){for(var e=p.cookie.split(/; ?/g),n=e.length-1;n>=0;n--)if(l(e[n])){var r=e[n].split("="),o=unescape(r[0]),i=unescape(r[1]);t(i,o)}}function i(t,e){t&&(p.cookie=escape(t)+"="+escape(e)+"; expires=Tue, 19 Jan 2038 03:14:07 GMT; path=/")}function u(t){t&&a(t)&&(p.cookie=escape(t)+"=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/")}function s(){o(function(t,e){u(e)})}function a(t){return new RegExp("(?:^|;\\s*)"+escape(t).replace(/[\-\.\+\*]/g,"\\$&")+"\\s*\\=").test(p.cookie)}var c=t("../src/util"),f=c.Global,l=c.trim;e.exports={name:"cookieStorage",read:r,write:i,each:o,remove:u,clearAll:s};var p=f.document},{"../src/util":16}],19:[function(t,e,n){"use strict";function r(){return f.localStorage}function o(t){return r().getItem(t)}function i(t,e){return r().setItem(t,e)}function u(t){for(var e=r().length-1;e>=0;e--){var n=r().key(e);t(o(n),n)}}function s(t){return r().removeItem(t)}function a(){return r().clear()}var c=t("../src/util"),f=c.Global;e.exports={name:"localStorage",read:o,write:i,each:u,remove:s,clearAll:a}},{"../src/util":16}],20:[function(t,e,n){"use strict";function r(t){return a[t]}function o(t,e){a[t]=e}function i(t){for(var e in a)a.hasOwnProperty(e)&&t(a[e],e)}function u(t){delete a[t]}function s(t){a={}}e.exports={name:"memoryStorage",read:r,write:o,each:i,remove:u,clearAll:s};var a={}},{}],21:[function(t,e,n){"use strict";function r(t){return f[t]}function o(t,e){f[t]=e}function i(t){for(var e=f.length-1;e>=0;e--){var n=f.key(e);t(f[n],n)}}function u(t){return f.removeItem(t)}function s(){i(function(t,e){delete f[t]})}var a=t("../src/util"),c=a.Global;e.exports={name:"oldFF-globalStorage",read:r,write:o,each:i,remove:u,clearAll:s};var f=c.globalStorage},{"../src/util":16}],22:[function(t,e,n){"use strict";function r(t,e){if(!g){var n=a(t);d(function(t){t.setAttribute(n,e),t.save(p)})}}function o(t){if(!g){var e=a(t),n=null;return d(function(t){n=t.getAttribute(e)}),n}}function i(t){d(function(e){for(var n=e.XMLDocument.documentElement.attributes,r=n.length-1;r>=0;r--){var o=n[r];t(e.getAttribute(o.name),o.name)}})}function u(t){var e=a(t);d(function(t){t.removeAttribute(e),t.save(p)})}function s(){d(function(t){var e=t.XMLDocument.documentElement.attributes;t.load(p);for(var n=e.length-1;n>=0;n--)t.removeAttribute(e[n].name);t.save(p)})}function a(t){return t.replace(/^\d/,"___$&").replace(v,"___")}function c(){if(!h||!h.documentElement||!h.documentElement.addBehavior)return null;var t,e,n,r="script";try{e=new ActiveXObject("htmlfile"),e.open(),e.write("<"+r+">document.w=window</"+r+'><iframe src="/favicon.ico"></iframe>'),e.close(),t=e.w.frames[0].document,n=t.createElement("div")}catch(o){n=h.createElement("div"),t=h.body}return function(e){var r=[].slice.call(arguments,0);r.unshift(n),t.appendChild(n),n.addBehavior("#default#userData"),n.load(p),e.apply(this,r),t.removeChild(n)}}var f=t("../src/util"),l=f.Global;e.exports={name:"oldIE-userDataStorage",write:r,read:o,each:i,remove:u,clearAll:s};var p="storejs",h=l.document,d=c(),g=(l.navigator?l.navigator.userAgent:"").match(/ (MSIE 8|MSIE 9|MSIE 10)\./),v=new RegExp("[!\"#$%&'()*+,/\\\\:;<=>?@[\\]^`{|}~]","g")},{"../src/util":16}],23:[function(t,e,n){"use strict";function r(){return f.sessionStorage}function o(t){return r().getItem(t)}function i(t,e){return r().setItem(t,e)}function u(t){for(var e=r().length-1;e>=0;e--){var n=r().key(e);t(o(n),n)}}function s(t){return r().removeItem(t)}function a(){return r().clear()}var c=t("../src/util"),f=c.Global;e.exports={name:"sessionStorage",read:o,write:i,each:u,remove:s,clearAll:a}},{"../src/util":16}]},{},[1])(1)});
/*
 * jQuery Hotkeys Plugin
 * Copyright 2010, John Resig
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 * Based upon the plugin by Tzury Bar Yochay:
 * http://github.com/tzuryby/hotkeys
 *
 * Original idea by:
 * Binny V A, http://www.openjs.com/scripts/events/keyboard_shortcuts/
*/

(function(jQuery){
	
	jQuery.hotkeys = {
		version: "0.8",

		specialKeys: {
			8: "backspace", 9: "tab", 13: "return", 16: "shift", 17: "ctrl", 18: "alt", 19: "pause",
			20: "capslock", 27: "esc", 32: "space", 33: "pageup", 34: "pagedown", 35: "end", 36: "home",
			37: "left", 38: "up", 39: "right", 40: "down", 45: "insert", 46: "del", 
			96: "0", 97: "1", 98: "2", 99: "3", 100: "4", 101: "5", 102: "6", 103: "7",
			104: "8", 105: "9", 106: "*", 107: "+", 109: "-", 110: ".", 111 : "/", 
			112: "f1", 113: "f2", 114: "f3", 115: "f4", 116: "f5", 117: "f6", 118: "f7", 119: "f8", 
			120: "f9", 121: "f10", 122: "f11", 123: "f12", 144: "numlock", 145: "scroll", 191: "/", 224: "meta"
		},
	
		shiftNums: {
			"`": "~", "1": "!", "2": "@", "3": "#", "4": "$", "5": "%", "6": "^", "7": "&", 
			"8": "*", "9": "(", "0": ")", "-": "_", "=": "+", ";": ": ", "'": "\"", ",": "<", 
			".": ">",  "/": "?",  "\\": "|"
		}
	};

	function keyHandler( handleObj ) {
		// Only care when a possible input has been specified
		if ( typeof handleObj.data !== "string" ) {
			return;
		}
		
		var origHandler = handleObj.handler,
			keys = handleObj.data.toLowerCase().split(" ");
	
		handleObj.handler = function( event ) {
			// Don't fire in text-accepting inputs that we didn't directly bind to
			if ( this !== event.target && (/textarea|select/i.test( event.target.nodeName ) ||
				 event.target.type === "text") ) {
				return;
			}
			
			// Keypress represents characters, not special keys
			var special = event.type !== "keypress" && jQuery.hotkeys.specialKeys[ event.which ],
				character = String.fromCharCode( event.which ).toLowerCase(),
				key, modif = "", possible = {};

			// check combinations (alt|ctrl|shift+anything)
			if ( event.altKey && special !== "alt" ) {
				modif += "alt+";
			}

			if ( event.ctrlKey && special !== "ctrl" ) {
				modif += "ctrl+";
			}
			
			// TODO: Need to make sure this works consistently across platforms
			if ( event.metaKey && !event.ctrlKey && special !== "meta" ) {
				modif += "meta+";
			}

			if ( event.shiftKey && special !== "shift" ) {
				modif += "shift+";
			}

			if ( special ) {
				possible[ modif + special ] = true;

			} else {
				possible[ modif + character ] = true;
				possible[ modif + jQuery.hotkeys.shiftNums[ character ] ] = true;

				// "$" can be triggered as "Shift+4" or "Shift+$" or just "$"
				if ( modif === "shift+" ) {
					possible[ jQuery.hotkeys.shiftNums[ character ] ] = true;
				}
			}

			for ( var i = 0, l = keys.length; i < l; i++ ) {
				if ( possible[ keys[i] ] ) {
					return origHandler.apply( this, arguments );
				}
			}
		};
	}

	jQuery.each([ "keydown", "keyup", "keypress" ], function() {
		jQuery.event.special[ this ] = { add: keyHandler };
	});

})( jQuery );
/*!
 * jQuery gzoom 0.1
 * 2009 Giovanni Battista Lenoci <gianiaz@gianiaz.net>
 * 
 * Based on minizoomPan plugin of Gian Carlo Mingati Version: 1.0 (18-JUNE-2009) http://www.gcmingati.net/wordpress/wp-content/lab/jquery/minizoompan/
 * Inspiration from jquery lightbox plugin http://leandrovieira.com/projects/jquery/lightbox/
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 * Requires:
 * jQuery v1.3.2 or later
 * jQuery ui.core.js v.1.7.1
 * jQuery ui.slider.js v.1.7.1
 * 
 */
jQuery.fn.gzoom = function(settings) {
	settings = jQuery.extend({
  	sW: 10, // small image width
  	sH: 10, // small image height
  	lW: 20, // large image width
  	lH: 20, // large image height
  	step : 5,
  	frameColor: "#cecece",
  	frameWidth: 1,
  	re : /thumbs\//, 
  	replace : '',  
  	debug : false,
  	overlayBgColor : '#000',
    overlayOpacity:0.8,
    containerBorderSize: 10, 
    containerResizeSpeed : 400,
  	loaderContent: "loading...",  // plain text or an image tag eg.: "<img src='yoursite.com/spinner.gif' />"
  	lightbox: false,
  	zoomIcon : "" // icon url, if not empty shown on the right-top corner of the image
	}, settings);

	return this.each(function(){
	  
	  var swapped = false;
	  
		var $div = jQuery(this).css({width: settings.sW, height: settings.sH, border: settings.frameWidth+"px solid "+settings.frameColor, overflow:'hidden'}).addClass("minizoompan");
		$div.wrap('<div class="gzoomwrap"></div>').css({ width:settings.sW });
		var ig = $div.children().css({position: "relative"});
		
		jQuery(window).bind("load", function() {
  		ig.width(settings.sW); 
  		ig.height(settings.sH); 
		});
		
		
		jQuery("<span class='loader'>"+settings.loaderContent+"<\/span>").insertBefore(ig);
		if(settings.zoomIcon != '' && settings.lightbox) {
		  var $zoom = jQuery('<img class="zoomIcon" src="'+settings.zoomIcon+'" alt="view larger" />').insertBefore(ig);
		  $zoom.load(function() {
  		  $zoom.css({'left':(settings.sW - $zoom.width())+'px'}).show();
		  });
		  $zoom.click(function() {
		    drawIface();
		  });
		} else if(settings.lightbox) {
			$div.click(function() {
			  drawIface();
			});
			$div.css({'cursor':'pointer'}); 
		}
		
		var $plus = jQuery('<div class="ui-icon ui-icon-circle-plus gzoombutton">&nbsp;</div>').insertAfter($div);
		var $slider = jQuery('<div class="gzoomSlider"></div>').insertAfter($div).css({ width:(settings.sW-42)+'px'});
		var $minus = jQuery('<div class="ui-icon ui-icon-circle-minus gzoombutton">&nbsp;</div>').insertAfter($div);
		
		$plus.click(function() {
		  valore = parseInt($slider.slider('value')) + settings.step;
		  $slider.slider('value', valore);
		});
		
		$minus.click(function() {
		  valore = parseInt($slider.slider('value')) - settings.step;
		  $slider.slider('value', valore);
		});
		
		$slider.slider({value:0,
		            		min: 0,
		            		max: 100,
		            		step: settings.step, 
                    change: function(event, ui) { 
                    
                      if(!swapped) {
	              				var hiSrc = ig.attr("src").replace(settings.re, settings.replace);
	              				swapImage(ig, hiSrc);
	              				$div.children("span.loader").fadeIn(250);
	                      swapped = true;
	                    }
	                    
                      val = ui.value;

                      newWidth = settings.sW+((settings.lW-settings.sW)*val)/100;
                      newHeight = settings.sH+((settings.lH-settings.sH)*val)/100;
                      ig_pos = ig.position();

                      if(settings.debug && typeof(console) != 'undefined') {
                        console.log('new dimensions:'+newWidth+'x'+newHeight);
                      }
                      
                      deltaWidth = ig.width()-settings.sW;
                      leftPos = Math.abs(ig_pos.left);
                      leftFactor = leftPos/deltaWidth;
                      newDeltaWidth = newWidth-settings.sW;
                      newLeft = (leftFactor*newDeltaWidth)*-1;
                      
                      deltaHeight = ig.height()-settings.sH;
                      topPos = Math.abs(ig_pos.top);
                      topFactor = topPos/deltaHeight;
                      newDeltaHeight = newHeight-settings.sH;
                      newTop = (topFactor*newDeltaHeight)*-1;
              				ig.css({ width: newWidth, height: newHeight, left:newLeft, top:newTop});
                    }
               		});
		$('<br style="clear:both" />').insertAfter($plus);
		
		function swapImage(param, uri){
			param.load(function () {
				$div.children("span.loader").fadeOut(250);
			}).error(function (){
  			alert("Image does not exist or its SRC is not correct.");
			}).attr('src', uri);
		}
		
		$div.mousemove(function(e){
			var divWidth = $div.width();
			var divHeight = $div.height();
			var igW = ig.width();
			var igH = ig.height();
			var dOs = $div.offset();
			var leftPan = (e.pageX - dOs.left) * (divWidth - igW) / (divWidth+settings.frameWidth*2);
			var topPan = (e.pageY - dOs.top) * (divHeight - igH) / (divHeight+settings.frameWidth*2);

      if(settings.debug && typeof(console) != 'undefined') {
			  console.log('left:'+leftPan+'|Top:'+topPan);
			}
			
			ig.css({left: leftPan, top: topPan});			
		});    
		
		if(typeof($.event.special.mousewheel) != 'undefined') {
			ig.mousewheel(function(event, delta) {
	      if (delta > 0) {
	        valore = parseInt($slider.slider('value')) + settings.step;
	  		  $slider.slider('value', valore);
	  		} else if (delta < 0) {
	  		  valore = parseInt($slider.slider('value')) - settings.step;
		  	  $slider.slider('value', valore);
		  	}
	      return false; // prevent default
	    });
	  }
		
		function resize_fx(intImageWidth,intImageHeight) {
		
      if(settings.debug && typeof(console) != 'undefined') {
        console.log('resize_fx('+intImageWidth+','+intImageHeight+')');
      }

			if(intImageWidth > ($(window).width()*80/100)) {
  			imgWidth = $(window).width()*80/100;
  			intImageHeight = (imgWidth/intImageWidth)*intImageHeight;
  			$('#zoomedimage').css({'width': imgWidth+'px', 'height':intImageHeight});
	      if(settings.debug && typeof(console) != 'undefined') {
	        console.log('Img dimensions 80% horizontal:'+imgWidth+'x'+intImageHeight);
	      }
			} else {
  			imgWidth = intImageWidth;
			}
			
			if(intImageHeight > $(window).height()*80/100) {
			  imgHeight = $(window).height()*80/100;
			  imgWidth = (imgHeight/intImageHeight)*imgWidth;
  			$('#zoomedimage').css({'width': imgWidth+'px', 'height':imgHeight});
	      if(settings.debug && typeof(console) != 'undefined') {
	        console.log('Img dimensions 80% vertical:'+imgWidth+'x'+imgHeight);
	      }
			} else {
			  imgHeight = intImageHeight;
			}
			
      if(settings.debug && typeof(console) != 'undefined') {
        console.log('Img dimensions:'+imgWidth+'x'+imgHeight);
      }
			
			// Get current width and height
			var intCurrentWidth = $('#imagebox').width();
			var intCurrentHeight = $('#imagebox').height();
			// Get the width and height of the selected image plus the padding
			var intWidth = (imgWidth + (settings.containerBorderSize * 2)); // Plus the imageŽs width and the left and right padding value
			var intHeight = (imgHeight + (settings.containerBorderSize * 2)); // Plus the imageŽs height and the left and right padding value

    
			
			// Diferences
			var intDiffW = intCurrentWidth - intWidth;
			var intDiffH = intCurrentHeight - intHeight;
			// Perfomance the effect
			$('#imagebox').animate({ width: intWidth, height: intHeight },settings.containerResizeSpeed,function() { _show_image(); });
			if ( ( intDiffW == 0 ) && ( intDiffH == 0 ) ) {
				if ( $.browser.msie ) {
					___pause(250);
				} else {
					___pause(100);	
				}
			} 
			$('#lboximgdatacontainer').css({ width: imgWidth });
		};
		
		function drawIface() {
		
			$('body').append('<div id="gzoomoverlay"></div><div id="gzoomlbox"><div id="imagebox"><div id="gzoom-cont-img"><img id="zoomedimage"><div id="gzoomloading"><a href="#" id="gzoomloadinglink"><img src="images/loading.gif"></a></div></div></div><div id="gzoomlbox-con-imgdata"><div id="lboximgdatacontainer"><div id="gzoomlbox-image-details"><span id="gzoom-image-caption"></span></div></div></div></div>');	
		
			$('#gzoomoverlay').css({
				backgroundColor:	settings.overlayBgColor,
				opacity:			settings.overlayOpacity,
				width:				$(window).width(),
				height:				$(document).height()
			}).fadeIn();
			
			// Calculate top and left offset for the gzoomlbox div object and show it
			$('#gzoomlbox').css({
				top:	$(window).scrollTop() + ($(window).height() / 10),
				left:	$(window).scrollLeft()
			}).show();
			
			$('#gzoomoverlay,#gzoomlbox').click(function() {
				close();									
			});
			// If window was resized, calculate the new overlay dimensions
			$(window).resize(function() {
				$('#gzoomoverlay').css({
					width:		$(window).width(),
					height:		$(window).height()
				});
				// Calculate top and left offset for the jquery-lightbox div object and show it
				$('#gzoomlbox').css({
					top:	$(window).scrollTop() + ($(window).height() / 10),
					left:	$(window).scrollLeft()
				});
			});
			
			_set_image_to_view();
		}

		function _set_image_to_view() { // show the loading
			// Show the loading
			$('#gzoomlbox-con-imgdata').hide();
			$('#zoomedimage').hide();
			$('#gzoomloading').show();
			// Image preload process
			var objImagePreloader = new Image();
      if(!swapped) {
				var hiSrc = ig.attr("src").replace(settings.re, settings.replace);
		  } else {
				var hiSrc = ig.attr("src").replace(settings.re, settings.replace);
		  }
			objImagePreloader.onload = function() {
				$('#zoomedimage').attr('src',hiSrc);
				resize_fx(objImagePreloader.width,objImagePreloader.height);
			};
		  objImagePreloader.src = hiSrc;
		};

		function _show_image() {
 			$('#gzoomloading').hide();
			$('#zoomedimage').fadeIn(function() {
				_show_image_data();
			});
		};

		/**
		 * Show the image information
		 *
		 */
		function _show_image_data() {
			$('#lightbox-loading').hide();
      $('#gzoom-image-caption').html(ig.attr('title'));
			$('#gzoomlbox-con-imgdata').slideDown('fast');
		}
		
		function close() {
			$('#gzoomlbox').remove();
			$('#gzoomoverlay').fadeOut(function() { $('#gzoomoverlay').remove(); });
			$('embed, object, select').css({ 'visibility' : 'visible' });
		}

    function ___pause(ms) {
  		var date = new Date(); 
	  	curDate = null;
		  do { var curDate = new Date(); }
  		while ( curDate - date < ms);
	  };
		
	});	
	
};

jQuery.fn.gzoomExpand = function(path) {
	$('body').append('<div id="gzoomoverlay"></div><div id="gzoomlbox"><div id="imagebox"><div id="gzoom-cont-img"><img id="zoomedimage"><div id="gzoomloading"><a href="#" id="gzoomloadinglink"><img src="images/loading.gif"></a></div></div></div><div id="gzoomlbox-con-imgdata"><div id="lboximgdatacontainer"><div id="gzoomlbox-image-details"><span id="gzoom-image-caption"></span></div></div></div></div>');   						
	$('#gzoomoverlay').css({
		backgroundColor:	'#000',
		opacity:			0.8,
		width:				$(window).width(),
		height:				$(document).height()
	}).fadeIn();   							   							
	$('#gzoomlbox').css({
		top:	$(window).scrollTop() + ($(window).height() / 10),
		left:	$(window).scrollLeft()
	}).show();							
	$('#gzoomoverlay,#gzoomlbox').click(function() {
		$('#gzoomlbox').remove();
		$('#gzoomoverlay').fadeOut(function() { $('#gzoomoverlay').remove(); });
		$('embed, object, select').css({ 'visibility' : 'visible' });									
	});
	$(window).resize(function() {
		$('#gzoomoverlay').css({
			width:		$(window).width(),
			height:		$(window).height()
		});  				
		$('#gzoomlbox').css({
			top:	$(window).scrollTop() + ($(window).height() / 10),
			left:	$(window).scrollLeft()
		});
	});
	
	$('#gzoomlbox-con-imgdata').hide();
	$('#zoomedimage').hide();
	$('#gzoomloading').show();

	var objImagePreloader = new Image();
	var hiSrc = path.replace('/thumbs\//' , '');
	objImagePreloader.onload = function() {
		$('#zoomedimage').attr('src',hiSrc);
		var intImageWidth =objImagePreloader.width;
		var intImageHeight = objImagePreloader.height;
		if(intImageWidth > ($(window).width()*80/100)) {
  			imgWidth = $(window).width()*80/100;
  			intImageHeight = (imgWidth/intImageWidth)*intImageHeight;
  			$('#zoomedimage').css({'width': imgWidth+'px', 'height':intImageHeight});			    
			} else {
  			imgWidth = intImageWidth;
			}
			
			if(intImageHeight > $(window).height()*80/100) {
			  imgHeight = $(window).height()*80/100;
			  imgWidth = (imgHeight/intImageHeight)*imgWidth;
  			$('#zoomedimage').css({'width': imgWidth+'px', 'height':imgHeight});
	     
			} else {
			  imgHeight = intImageHeight;
			}

			var intCurrentWidth = $('#imagebox').width();
			var intCurrentHeight = $('#imagebox').height();
			
			var intWidth = (imgWidth + (10* 2)); // Plus the imageŽs width and the left and right padding value
			var intHeight = (imgHeight + (10 * 2)); // Plus the imageŽs height and the left and right padding value

			var intDiffW = intCurrentWidth - intWidth;
			var intDiffH = intCurrentHeight - intHeight;

			$('#imagebox').animate({ width: intWidth, height: intHeight },400);

			$('#lboximgdatacontainer').css({ width: imgWidth });
			$('#gzoomloading').hide();
		$('#zoomedimage').fadeIn(function() {
	
		});
	};
  objImagePreloader.src = hiSrc; 
};
/*
 * jQuery UI 1.7.1
 *
 * Copyright (c) 2009 AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 * http://docs.jquery.com/UI
 */jQuery.ui||(function(c){var i=c.fn.remove,d=c.browser.mozilla&&(parseFloat(c.browser.version)<1.9);c.ui={version:"1.7.1",plugin:{add:function(k,l,n){var m=c.ui[k].prototype;for(var j in n){m.plugins[j]=m.plugins[j]||[];m.plugins[j].push([l,n[j]])}},call:function(j,l,k){var n=j.plugins[l];if(!n||!j.element[0].parentNode){return}for(var m=0;m<n.length;m++){if(j.options[n[m][0]]){n[m][1].apply(j.element,k)}}}},contains:function(k,j){return document.compareDocumentPosition?k.compareDocumentPosition(j)&16:k!==j&&k.contains(j)},hasScroll:function(m,k){if(c(m).css("overflow")=="hidden"){return false}var j=(k&&k=="left")?"scrollLeft":"scrollTop",l=false;if(m[j]>0){return true}m[j]=1;l=(m[j]>0);m[j]=0;return l},isOverAxis:function(k,j,l){return(k>j)&&(k<(j+l))},isOver:function(o,k,n,m,j,l){return c.ui.isOverAxis(o,n,j)&&c.ui.isOverAxis(k,m,l)},keyCode:{BACKSPACE:8,CAPS_LOCK:20,COMMA:188,CONTROL:17,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,INSERT:45,LEFT:37,NUMPAD_ADD:107,NUMPAD_DECIMAL:110,NUMPAD_DIVIDE:111,NUMPAD_ENTER:108,NUMPAD_MULTIPLY:106,NUMPAD_SUBTRACT:109,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SHIFT:16,SPACE:32,TAB:9,UP:38}};if(d){var f=c.attr,e=c.fn.removeAttr,h="http://www.w3.org/2005/07/aaa",a=/^aria-/,b=/^wairole:/;c.attr=function(k,j,l){var m=l!==undefined;return(j=="role"?(m?f.call(this,k,j,"wairole:"+l):(f.apply(this,arguments)||"").replace(b,"")):(a.test(j)?(m?k.setAttributeNS(h,j.replace(a,"aaa:"),l):f.call(this,k,j.replace(a,"aaa:"))):f.apply(this,arguments)))};c.fn.removeAttr=function(j){return(a.test(j)?this.each(function(){this.removeAttributeNS(h,j.replace(a,""))}):e.call(this,j))}}c.fn.extend({remove:function(){c("*",this).add(this).each(function(){c(this).triggerHandler("remove")});return i.apply(this,arguments)},enableSelection:function(){return this.attr("unselectable","off").css("MozUserSelect","").unbind("selectstart.ui")},disableSelection:function(){return this.attr("unselectable","on").css("MozUserSelect","none").bind("selectstart.ui",function(){return false})},scrollParent:function(){var j;if((c.browser.msie&&(/(static|relative)/).test(this.css("position")))||(/absolute/).test(this.css("position"))){j=this.parents().filter(function(){return(/(relative|absolute|fixed)/).test(c.curCSS(this,"position",1))&&(/(auto|scroll)/).test(c.curCSS(this,"overflow",1)+c.curCSS(this,"overflow-y",1)+c.curCSS(this,"overflow-x",1))}).eq(0)}else{j=this.parents().filter(function(){return(/(auto|scroll)/).test(c.curCSS(this,"overflow",1)+c.curCSS(this,"overflow-y",1)+c.curCSS(this,"overflow-x",1))}).eq(0)}return(/fixed/).test(this.css("position"))||!j.length?c(document):j}});c.extend(c.expr[":"],{data:function(l,k,j){return !!c.data(l,j[3])},focusable:function(k){var l=k.nodeName.toLowerCase(),j=c.attr(k,"tabindex");return(/input|select|textarea|button|object/.test(l)?!k.disabled:"a"==l||"area"==l?k.href||!isNaN(j):!isNaN(j))&&!c(k)["area"==l?"parents":"closest"](":hidden").length},tabbable:function(k){var j=c.attr(k,"tabindex");return(isNaN(j)||j>=0)&&c(k).is(":focusable")}});function g(m,n,o,l){function k(q){var p=c[m][n][q]||[];return(typeof p=="string"?p.split(/,?\s+/):p)}var j=k("getter");if(l.length==1&&typeof l[0]=="string"){j=j.concat(k("getterSetter"))}return(c.inArray(o,j)!=-1)}c.widget=function(k,j){var l=k.split(".")[0];k=k.split(".")[1];c.fn[k]=function(p){var n=(typeof p=="string"),o=Array.prototype.slice.call(arguments,1);if(n&&p.substring(0,1)=="_"){return this}if(n&&g(l,k,p,o)){var m=c.data(this[0],k);return(m?m[p].apply(m,o):undefined)}return this.each(function(){var q=c.data(this,k);(!q&&!n&&c.data(this,k,new c[l][k](this,p))._init());(q&&n&&c.isFunction(q[p])&&q[p].apply(q,o))})};c[l]=c[l]||{};c[l][k]=function(o,n){var m=this;this.namespace=l;this.widgetName=k;this.widgetEventPrefix=c[l][k].eventPrefix||k;this.widgetBaseClass=l+"-"+k;this.options=c.extend({},c.widget.defaults,c[l][k].defaults,c.metadata&&c.metadata.get(o)[k],n);this.element=c(o).bind("setData."+k,function(q,p,r){if(q.target==o){return m._setData(p,r)}}).bind("getData."+k,function(q,p){if(q.target==o){return m._getData(p)}}).bind("remove",function(){return m.destroy()})};c[l][k].prototype=c.extend({},c.widget.prototype,j);c[l][k].getterSetter="option"};c.widget.prototype={_init:function(){},destroy:function(){this.element.removeData(this.widgetName).removeClass(this.widgetBaseClass+"-disabled "+this.namespace+"-state-disabled").removeAttr("aria-disabled")},option:function(l,m){var k=l,j=this;if(typeof l=="string"){if(m===undefined){return this._getData(l)}k={};k[l]=m}c.each(k,function(n,o){j._setData(n,o)})},_getData:function(j){return this.options[j]},_setData:function(j,k){this.options[j]=k;if(j=="disabled"){this.element[k?"addClass":"removeClass"](this.widgetBaseClass+"-disabled "+this.namespace+"-state-disabled").attr("aria-disabled",k)}},enable:function(){this._setData("disabled",false)},disable:function(){this._setData("disabled",true)},_trigger:function(l,m,n){var p=this.options[l],j=(l==this.widgetEventPrefix?l:this.widgetEventPrefix+l);m=c.Event(m);m.type=j;if(m.originalEvent){for(var k=c.event.props.length,o;k;){o=c.event.props[--k];m[o]=m.originalEvent[o]}}this.element.trigger(m,n);return !(c.isFunction(p)&&p.call(this.element[0],m,n)===false||m.isDefaultPrevented())}};c.widget.defaults={disabled:false};c.ui.mouse={_mouseInit:function(){var j=this;this.element.bind("mousedown."+this.widgetName,function(k){return j._mouseDown(k)}).bind("click."+this.widgetName,function(k){if(j._preventClickEvent){j._preventClickEvent=false;k.stopImmediatePropagation();return false}});if(c.browser.msie){this._mouseUnselectable=this.element.attr("unselectable");this.element.attr("unselectable","on")}this.started=false},_mouseDestroy:function(){this.element.unbind("."+this.widgetName);(c.browser.msie&&this.element.attr("unselectable",this._mouseUnselectable))},_mouseDown:function(l){l.originalEvent=l.originalEvent||{};if(l.originalEvent.mouseHandled){return}(this._mouseStarted&&this._mouseUp(l));this._mouseDownEvent=l;var k=this,m=(l.which==1),j=(typeof this.options.cancel=="string"?c(l.target).parents().add(l.target).filter(this.options.cancel).length:false);if(!m||j||!this._mouseCapture(l)){return true}this.mouseDelayMet=!this.options.delay;if(!this.mouseDelayMet){this._mouseDelayTimer=setTimeout(function(){k.mouseDelayMet=true},this.options.delay)}if(this._mouseDistanceMet(l)&&this._mouseDelayMet(l)){this._mouseStarted=(this._mouseStart(l)!==false);if(!this._mouseStarted){l.preventDefault();return true}}this._mouseMoveDelegate=function(n){return k._mouseMove(n)};this._mouseUpDelegate=function(n){return k._mouseUp(n)};c(document).bind("mousemove."+this.widgetName,this._mouseMoveDelegate).bind("mouseup."+this.widgetName,this._mouseUpDelegate);(c.browser.safari||l.preventDefault());l.originalEvent.mouseHandled=true;return true},_mouseMove:function(j){if(c.browser.msie&&!j.button){return this._mouseUp(j)}if(this._mouseStarted){this._mouseDrag(j);return j.preventDefault()}if(this._mouseDistanceMet(j)&&this._mouseDelayMet(j)){this._mouseStarted=(this._mouseStart(this._mouseDownEvent,j)!==false);(this._mouseStarted?this._mouseDrag(j):this._mouseUp(j))}return !this._mouseStarted},_mouseUp:function(j){c(document).unbind("mousemove."+this.widgetName,this._mouseMoveDelegate).unbind("mouseup."+this.widgetName,this._mouseUpDelegate);if(this._mouseStarted){this._mouseStarted=false;this._preventClickEvent=(j.target==this._mouseDownEvent.target);this._mouseStop(j)}return false},_mouseDistanceMet:function(j){return(Math.max(Math.abs(this._mouseDownEvent.pageX-j.pageX),Math.abs(this._mouseDownEvent.pageY-j.pageY))>=this.options.distance)},_mouseDelayMet:function(j){return this.mouseDelayMet},_mouseStart:function(j){},_mouseDrag:function(j){},_mouseStop:function(j){},_mouseCapture:function(j){return true}};c.ui.mouse.defaults={cancel:null,distance:1,delay:0}})(jQuery);
/*
 * jQuery UI Slider 1.7.1
 *
 * Copyright (c) 2009 AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 * http://docs.jquery.com/UI/Slider
 *
 * Depends:
 *	ui.core.js
 */(function(a){a.widget("ui.slider",a.extend({},a.ui.mouse,{_init:function(){var b=this,c=this.options;this._keySliding=false;this._handleIndex=null;this._detectOrientation();this._mouseInit();this.element.addClass("ui-slider ui-slider-"+this.orientation+" ui-widget ui-widget-content ui-corner-all");this.range=a([]);if(c.range){if(c.range===true){this.range=a("<div></div>");if(!c.values){c.values=[this._valueMin(),this._valueMin()]}if(c.values.length&&c.values.length!=2){c.values=[c.values[0],c.values[0]]}}else{this.range=a("<div></div>")}this.range.appendTo(this.element).addClass("ui-slider-range");if(c.range=="min"||c.range=="max"){this.range.addClass("ui-slider-range-"+c.range)}this.range.addClass("ui-widget-header")}if(a(".ui-slider-handle",this.element).length==0){a('<a href="#"></a>').appendTo(this.element).addClass("ui-slider-handle")}if(c.values&&c.values.length){while(a(".ui-slider-handle",this.element).length<c.values.length){a('<a href="#"></a>').appendTo(this.element).addClass("ui-slider-handle")}}this.handles=a(".ui-slider-handle",this.element).addClass("ui-state-default ui-corner-all");this.handle=this.handles.eq(0);this.handles.add(this.range).filter("a").click(function(d){d.preventDefault()}).hover(function(){a(this).addClass("ui-state-hover")},function(){a(this).removeClass("ui-state-hover")}).focus(function(){a(".ui-slider .ui-state-focus").removeClass("ui-state-focus");a(this).addClass("ui-state-focus")}).blur(function(){a(this).removeClass("ui-state-focus")});this.handles.each(function(d){a(this).data("index.ui-slider-handle",d)});this.handles.keydown(function(i){var f=true;var e=a(this).data("index.ui-slider-handle");if(b.options.disabled){return}switch(i.keyCode){case a.ui.keyCode.HOME:case a.ui.keyCode.END:case a.ui.keyCode.UP:case a.ui.keyCode.RIGHT:case a.ui.keyCode.DOWN:case a.ui.keyCode.LEFT:f=false;if(!b._keySliding){b._keySliding=true;a(this).addClass("ui-state-active");b._start(i,e)}break}var g,d,h=b._step();if(b.options.values&&b.options.values.length){g=d=b.values(e)}else{g=d=b.value()}switch(i.keyCode){case a.ui.keyCode.HOME:d=b._valueMin();break;case a.ui.keyCode.END:d=b._valueMax();break;case a.ui.keyCode.UP:case a.ui.keyCode.RIGHT:if(g==b._valueMax()){return}d=g+h;break;case a.ui.keyCode.DOWN:case a.ui.keyCode.LEFT:if(g==b._valueMin()){return}d=g-h;break}b._slide(i,e,d);return f}).keyup(function(e){var d=a(this).data("index.ui-slider-handle");if(b._keySliding){b._stop(e,d);b._change(e,d);b._keySliding=false;a(this).removeClass("ui-state-active")}});this._refreshValue()},destroy:function(){this.handles.remove();this.range.remove();this.element.removeClass("ui-slider ui-slider-horizontal ui-slider-vertical ui-slider-disabled ui-widget ui-widget-content ui-corner-all").removeData("slider").unbind(".slider");this._mouseDestroy()},_mouseCapture:function(d){var e=this.options;if(e.disabled){return false}this.elementSize={width:this.element.outerWidth(),height:this.element.outerHeight()};this.elementOffset=this.element.offset();var h={x:d.pageX,y:d.pageY};var j=this._normValueFromMouse(h);var c=this._valueMax()-this._valueMin()+1,f;var k=this,i;this.handles.each(function(l){var m=Math.abs(j-k.values(l));if(c>m){c=m;f=a(this);i=l}});if(e.range==true&&this.values(1)==e.min){f=a(this.handles[++i])}this._start(d,i);k._handleIndex=i;f.addClass("ui-state-active").focus();var g=f.offset();var b=!a(d.target).parents().andSelf().is(".ui-slider-handle");this._clickOffset=b?{left:0,top:0}:{left:d.pageX-g.left-(f.width()/2),top:d.pageY-g.top-(f.height()/2)-(parseInt(f.css("borderTopWidth"),10)||0)-(parseInt(f.css("borderBottomWidth"),10)||0)+(parseInt(f.css("marginTop"),10)||0)};j=this._normValueFromMouse(h);this._slide(d,i,j);return true},_mouseStart:function(b){return true},_mouseDrag:function(d){var b={x:d.pageX,y:d.pageY};var c=this._normValueFromMouse(b);this._slide(d,this._handleIndex,c);return false},_mouseStop:function(b){this.handles.removeClass("ui-state-active");this._stop(b,this._handleIndex);this._change(b,this._handleIndex);this._handleIndex=null;this._clickOffset=null;return false},_detectOrientation:function(){this.orientation=this.options.orientation=="vertical"?"vertical":"horizontal"},_normValueFromMouse:function(d){var c,h;if("horizontal"==this.orientation){c=this.elementSize.width;h=d.x-this.elementOffset.left-(this._clickOffset?this._clickOffset.left:0)}else{c=this.elementSize.height;h=d.y-this.elementOffset.top-(this._clickOffset?this._clickOffset.top:0)}var f=(h/c);if(f>1){f=1}if(f<0){f=0}if("vertical"==this.orientation){f=1-f}var e=this._valueMax()-this._valueMin(),i=f*e,b=i%this.options.step,g=this._valueMin()+i-b;if(b>(this.options.step/2)){g+=this.options.step}return parseFloat(g.toFixed(5))},_start:function(d,c){var b={handle:this.handles[c],value:this.value()};if(this.options.values&&this.options.values.length){b.value=this.values(c);b.values=this.values()}this._trigger("start",d,b)},_slide:function(f,e,d){var g=this.handles[e];if(this.options.values&&this.options.values.length){var b=this.values(e?0:1);if((e==0&&d>=b)||(e==1&&d<=b)){d=b}if(d!=this.values(e)){var c=this.values();c[e]=d;var h=this._trigger("slide",f,{handle:this.handles[e],value:d,values:c});var b=this.values(e?0:1);if(h!==false){this.values(e,d,(f.type=="mousedown"&&this.options.animate),true)}}}else{if(d!=this.value()){var h=this._trigger("slide",f,{handle:this.handles[e],value:d});if(h!==false){this._setData("value",d,(f.type=="mousedown"&&this.options.animate))}}}},_stop:function(d,c){var b={handle:this.handles[c],value:this.value()};if(this.options.values&&this.options.values.length){b.value=this.values(c);b.values=this.values()}this._trigger("stop",d,b)},_change:function(d,c){var b={handle:this.handles[c],value:this.value()};if(this.options.values&&this.options.values.length){b.value=this.values(c);b.values=this.values()}this._trigger("change",d,b)},value:function(b){if(arguments.length){this._setData("value",b);this._change(null,0)}return this._value()},values:function(b,e,c,d){if(arguments.length>1){this.options.values[b]=e;this._refreshValue(c);if(!d){this._change(null,b)}}if(arguments.length){if(this.options.values&&this.options.values.length){return this._values(b)}else{return this.value()}}else{return this._values()}},_setData:function(b,d,c){a.widget.prototype._setData.apply(this,arguments);switch(b){case"orientation":this._detectOrientation();this.element.removeClass("ui-slider-horizontal ui-slider-vertical").addClass("ui-slider-"+this.orientation);this._refreshValue(c);break;case"value":this._refreshValue(c);break}},_step:function(){var b=this.options.step;return b},_value:function(){var b=this.options.value;if(b<this._valueMin()){b=this._valueMin()}if(b>this._valueMax()){b=this._valueMax()}return b},_values:function(b){if(arguments.length){var c=this.options.values[b];if(c<this._valueMin()){c=this._valueMin()}if(c>this._valueMax()){c=this._valueMax()}return c}else{return this.options.values}},_valueMin:function(){var b=this.options.min;return b},_valueMax:function(){var b=this.options.max;return b},_refreshValue:function(c){var f=this.options.range,d=this.options,l=this;if(this.options.values&&this.options.values.length){var i,h;this.handles.each(function(p,n){var o=(l.values(p)-l._valueMin())/(l._valueMax()-l._valueMin())*100;var m={};m[l.orientation=="horizontal"?"left":"bottom"]=o+"%";a(this).stop(1,1)[c?"animate":"css"](m,d.animate);if(l.options.range===true){if(l.orientation=="horizontal"){(p==0)&&l.range.stop(1,1)[c?"animate":"css"]({left:o+"%"},d.animate);(p==1)&&l.range[c?"animate":"css"]({width:(o-lastValPercent)+"%"},{queue:false,duration:d.animate})}else{(p==0)&&l.range.stop(1,1)[c?"animate":"css"]({bottom:(o)+"%"},d.animate);(p==1)&&l.range[c?"animate":"css"]({height:(o-lastValPercent)+"%"},{queue:false,duration:d.animate})}}lastValPercent=o})}else{var j=this.value(),g=this._valueMin(),k=this._valueMax(),e=k!=g?(j-g)/(k-g)*100:0;var b={};b[l.orientation=="horizontal"?"left":"bottom"]=e+"%";this.handle.stop(1,1)[c?"animate":"css"](b,d.animate);(f=="min")&&(this.orientation=="horizontal")&&this.range.stop(1,1)[c?"animate":"css"]({width:e+"%"},d.animate);(f=="max")&&(this.orientation=="horizontal")&&this.range[c?"animate":"css"]({width:(100-e)+"%"},{queue:false,duration:d.animate});(f=="min")&&(this.orientation=="vertical")&&this.range.stop(1,1)[c?"animate":"css"]({height:e+"%"},d.animate);(f=="max")&&(this.orientation=="vertical")&&this.range[c?"animate":"css"]({height:(100-e)+"%"},{queue:false,duration:d.animate})}}}));a.extend(a.ui.slider,{getter:"value values",version:"1.7.1",eventPrefix:"slide",defaults:{animate:false,delay:0,distance:0,max:100,min:0,orientation:"horizontal",range:false,step:1,value:0,values:null}})})(jQuery);
/*
 XDate v0.8
 Docs & Licensing: http://arshaw.com/xdate/
*/
var XDate=function(g,n,A,p){function f(){var a=this instanceof f?this:new f,c=arguments,b=c.length,d;typeof c[b-1]=="boolean"&&(d=c[--b],c=q(c,0,b));if(b)if(b==1)if(b=c[0],b instanceof g||typeof b=="number")a[0]=new g(+b);else if(b instanceof f){var c=a,h=new g(+b[0]);if(l(b))h.toString=v;c[0]=h}else{if(typeof b=="string"){a[0]=new g(0);a:{for(var c=b,b=d||!1,h=f.parsers,w=0,e;w<h.length;w++)if(e=h[w](c,b,a)){a=e;break a}a[0]=new g(c)}}}else a[0]=new g(m.apply(g,c)),d||(a[0]=r(a[0]));else a[0]=new g;
typeof d=="boolean"&&B(a,d);return a}function l(a){return a[0].toString===v}function B(a,c,b){if(c){if(!l(a))b&&(a[0]=new g(m(a[0].getFullYear(),a[0].getMonth(),a[0].getDate(),a[0].getHours(),a[0].getMinutes(),a[0].getSeconds(),a[0].getMilliseconds()))),a[0].toString=v}else l(a)&&(a[0]=b?r(a[0]):new g(+a[0]));return a}function C(a,c,b,d,h){var e=k(j,a[0],h),a=k(D,a[0],h),h=!1;d.length==2&&typeof d[1]=="boolean"&&(h=d[1],d=[b]);b=c==1?(b%12+12)%12:e(1);a(c,d);h&&e(1)!=b&&(a(1,[e(1)-1]),a(2,[E(e(0),
e(1))]))}function F(a,c,b,d){var b=Number(b),h=n.floor(b);a["set"+o[c]](a["get"+o[c]]()+h,d||!1);h!=b&&c<6&&F(a,c+1,(b-h)*G[c],d)}function H(a,c,b){var a=a.clone().setUTCMode(!0,!0),c=f(c).setUTCMode(!0,!0),d=0;if(b==0||b==1){for(var h=6;h>=b;h--)d/=G[h],d+=j(c,!1,h)-j(a,!1,h);b==1&&(d+=(c.getFullYear()-a.getFullYear())*12)}else b==2?(b=a.toDate().setUTCHours(0,0,0,0),d=c.toDate().setUTCHours(0,0,0,0),d=n.round((d-b)/864E5)+(c-d-(a-b))/864E5):d=(c-a)/[36E5,6E4,1E3,1][b-3];return d}function s(a){var c=
a(0),b=a(1),d=a(2),a=new g(m(c,b,d)),c=t(I(c,b,d));return n.floor(n.round((a-c)/864E5)/7)+1}function I(a,c,b){c=new g(m(a,c,b));if(c<t(a))return a-1;else if(c>=t(a+1))return a+1;return a}function t(a){a=new g(m(a,0,4));a.setUTCDate(a.getUTCDate()-(a.getUTCDay()+6)%7);return a}function J(a,c,b,d){var h=k(j,a,d),e=k(D,a,d);b===p&&(b=I(h(0),h(1),h(2)));b=t(b);d||(b=r(b));a.setTime(+b);e(2,[h(2)+(c-1)*7])}function K(a,c,b,d,h){var e=f.locales,g=e[f.defaultLocale]||{},i=k(j,a,h),b=(typeof b=="string"?
e[b]:b)||{};return x(a,c,function(a){if(d)for(var b=(a==7?2:a)-1;b>=0;b--)d.push(i(b));return i(a)},function(a){return b[a]||g[a]},h)}function x(a,c,b,d,e){for(var f,g,i="";f=c.match(N);){i+=c.substr(0,f.index);if(f[1]){g=i;for(var i=a,j=f[1],l=b,m=d,n=e,k=j.length,o=void 0,q="";k>0;)o=O(i,j.substr(0,k),l,m,n),o!==p?(q+=o,j=j.substr(k),k=j.length):k--;i=g+(q+j)}else f[3]?(g=x(a,f[4],b,d,e),parseInt(g.replace(/\D/g,""),10)&&(i+=g)):i+=f[7]||"'";c=c.substr(f.index+f[0].length)}return i+c}function O(a,
c,b,d,e){var g=f.formatters[c];if(typeof g=="string")return x(a,g,b,d,e);else if(typeof g=="function")return g(a,e||!1,d);switch(c){case "fff":return i(b(6),3);case "s":return b(5);case "ss":return i(b(5));case "m":return b(4);case "mm":return i(b(4));case "h":return b(3)%12||12;case "hh":return i(b(3)%12||12);case "H":return b(3);case "HH":return i(b(3));case "d":return b(2);case "dd":return i(b(2));case "ddd":return d("dayNamesShort")[b(7)]||"";case "dddd":return d("dayNames")[b(7)]||"";case "M":return b(1)+
1;case "MM":return i(b(1)+1);case "MMM":return d("monthNamesShort")[b(1)]||"";case "MMMM":return d("monthNames")[b(1)]||"";case "yy":return(b(0)+"").substring(2);case "yyyy":return b(0);case "t":return u(b,d).substr(0,1).toLowerCase();case "tt":return u(b,d).toLowerCase();case "T":return u(b,d).substr(0,1);case "TT":return u(b,d);case "z":case "zz":case "zzz":return e?c="Z":(d=a.getTimezoneOffset(),a=d<0?"+":"-",b=n.floor(n.abs(d)/60),d=n.abs(d)%60,e=b,c=="zz"?e=i(b):c=="zzz"&&(e=i(b)+":"+i(d)),c=
a+e),c;case "w":return s(b);case "ww":return i(s(b));case "S":return c=b(2),c>10&&c<20?"th":["st","nd","rd"][c%10-1]||"th"}}function u(a,c){return a(3)<12?c("amDesignator"):c("pmDesignator")}function y(a){return!isNaN(+a[0])}function j(a,c,b){return a["get"+(c?"UTC":"")+o[b]]()}function D(a,c,b,d){a["set"+(c?"UTC":"")+o[b]].apply(a,d)}function r(a){return new g(a.getUTCFullYear(),a.getUTCMonth(),a.getUTCDate(),a.getUTCHours(),a.getUTCMinutes(),a.getUTCSeconds(),a.getUTCMilliseconds())}function E(a,
c){return 32-(new g(m(a,c,32))).getUTCDate()}function z(a){return function(){return a.apply(p,[this].concat(q(arguments)))}}function k(a){var c=q(arguments,1);return function(){return a.apply(p,c.concat(q(arguments)))}}function q(a,c,b){return A.prototype.slice.call(a,c||0,b===p?a.length:b)}function L(a,c){for(var b=0;b<a.length;b++)c(a[b],b)}function i(a,c){c=c||2;for(a+="";a.length<c;)a="0"+a;return a}var o="FullYear,Month,Date,Hours,Minutes,Seconds,Milliseconds,Day,Year".split(","),M=["Years",
"Months","Days"],G=[12,31,24,60,60,1E3,1],N=/(([a-zA-Z])\2*)|(\((('.*?'|\(.*?\)|.)*?)\))|('(.*?)')/,m=g.UTC,v=g.prototype.toUTCString,e=f.prototype;e.length=1;e.splice=A.prototype.splice;e.getUTCMode=z(l);e.setUTCMode=z(B);e.getTimezoneOffset=function(){return l(this)?0:this[0].getTimezoneOffset()};L(o,function(a,c){e["get"+a]=function(){return j(this[0],l(this),c)};c!=8&&(e["getUTC"+a]=function(){return j(this[0],!0,c)});c!=7&&(e["set"+a]=function(a){C(this,c,a,arguments,l(this));return this},c!=
8&&(e["setUTC"+a]=function(a){C(this,c,a,arguments,!0);return this},e["add"+(M[c]||a)]=function(a,d){F(this,c,a,d);return this},e["diff"+(M[c]||a)]=function(a){return H(this,a,c)}))});e.getWeek=function(){return s(k(j,this,!1))};e.getUTCWeek=function(){return s(k(j,this,!0))};e.setWeek=function(a,c){J(this,a,c,!1);return this};e.setUTCWeek=function(a,c){J(this,a,c,!0);return this};e.addWeeks=function(a){return this.addDays(Number(a)*7)};e.diffWeeks=function(a){return H(this,a,2)/7};f.parsers=[function(a,
c,b){if(a=a.match(/^(\d{4})(-(\d{2})(-(\d{2})([T ](\d{2}):(\d{2})(:(\d{2})(\.(\d+))?)?(Z|(([-+])(\d{2})(:?(\d{2}))?))?)?)?)?$/)){var d=new g(m(a[1],a[3]?a[3]-1:0,a[5]||1,a[7]||0,a[8]||0,a[10]||0,a[12]?Number("0."+a[12])*1E3:0));a[13]?a[14]&&d.setUTCMinutes(d.getUTCMinutes()+(a[15]=="-"?1:-1)*(Number(a[16])*60+(a[18]?Number(a[18]):0))):c||(d=r(d));return b.setTime(+d)}}];f.parse=function(a){return+f(""+a)};e.toString=function(a,c,b){return a===p||!y(this)?this[0].toString():K(this,a,c,b,l(this))};
e.toUTCString=e.toGMTString=function(a,c,b){return a===p||!y(this)?this[0].toUTCString():K(this,a,c,b,!0)};e.toISOString=function(){return this.toUTCString("yyyy-MM-dd'T'HH:mm:ss(.fff)zzz")};f.defaultLocale="";f.locales={"":{monthNames:"January,February,March,April,May,June,July,August,September,October,November,December".split(","),monthNamesShort:"Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec".split(","),dayNames:"Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday".split(","),dayNamesShort:"Sun,Mon,Tue,Wed,Thu,Fri,Sat".split(","),
amDesignator:"AM",pmDesignator:"PM"}};f.formatters={i:"yyyy-MM-dd'T'HH:mm:ss(.fff)",u:"yyyy-MM-dd'T'HH:mm:ss(.fff)zzz"};L("getTime,valueOf,toDateString,toTimeString,toLocaleString,toLocaleDateString,toLocaleTimeString,toJSON".split(","),function(a){e[a]=function(){return this[0][a]()}});e.setTime=function(a){this[0].setTime(a);return this};e.valid=z(y);e.clone=function(){return new f(this)};e.clearTime=function(){return this.setHours(0,0,0,0)};e.toDate=function(){return new g(+this[0])};f.now=function(){return+new g};
f.today=function(){return(new f).clearTime()};f.UTC=m;f.getDaysInMonth=E;if(typeof module!=="undefined"&&module.exports)module.exports=f;typeof define==="function"&&define.amd&&define([],function(){return f});return f}(Date,Math,Array);

/*!
 * accounting.js v0.4.1
 * Copyright 2014 Open Exchange Rates
 *
 * Freely distributable under the MIT license.
 * Portions of accounting.js are inspired or borrowed from underscore.js
 *
 * Full details and documentation:
 * http://openexchangerates.github.io/accounting.js/
 */
(function(root, undefined) {

	/* --- Setup --- */

	// Create the local library object, to be exported or referenced globally later
	var lib = {};

	// Current version
	lib.version = '0.4.1';


	/* --- Exposed settings --- */

	// The library's settings configuration object. Contains default parameters for
	// currency and number formatting
	lib.settings = {
		currency: {
			symbol : "$",		// default currency symbol is '$'
			format : "%s%v",	// controls output: %s = symbol, %v = value (can be object, see docs)
			decimal : ".",		// decimal point separator
			thousand : ",",		// thousands separator
			precision : 2,		// decimal places
			grouping : 3		// digit grouping (not implemented yet)
		},
		number: {
			precision : 0,		// default precision on numbers is 0
			grouping : 3,		// digit grouping (not implemented yet)
			thousand : ",",
			decimal : "."
		}
	};


	/* --- Internal Helper Methods --- */

	// Store reference to possibly-available ECMAScript 5 methods for later
	var nativeMap = Array.prototype.map,
		nativeIsArray = Array.isArray,
		toString = Object.prototype.toString;

	/**
	 * Tests whether supplied parameter is a string
	 * from underscore.js
	 */
	function isString(obj) {
		return !!(obj === '' || (obj && obj.charCodeAt && obj.substr));
	}

	/**
	 * Tests whether supplied parameter is a string
	 * from underscore.js, delegates to ECMA5's native Array.isArray
	 */
	function isArray(obj) {
		return nativeIsArray ? nativeIsArray(obj) : toString.call(obj) === '[object Array]';
	}

	/**
	 * Tests whether supplied parameter is a true object
	 */
	function isObject(obj) {
		return obj && toString.call(obj) === '[object Object]';
	}

	/**
	 * Extends an object with a defaults object, similar to underscore's _.defaults
	 *
	 * Used for abstracting parameter handling from API methods
	 */
	function defaults(object, defs) {
		var key;
		object = object || {};
		defs = defs || {};
		// Iterate over object non-prototype properties:
		for (key in defs) {
			if (defs.hasOwnProperty(key)) {
				// Replace values with defaults only if undefined (allow empty/zero values):
				if (object[key] == null) object[key] = defs[key];
			}
		}
		return object;
	}

	/**
	 * Implementation of `Array.map()` for iteration loops
	 *
	 * Returns a new Array as a result of calling `iterator` on each array value.
	 * Defers to native Array.map if available
	 */
	function map(obj, iterator, context) {
		var results = [], i, j;

		if (!obj) return results;

		// Use native .map method if it exists:
		if (nativeMap && obj.map === nativeMap) return obj.map(iterator, context);

		// Fallback for native .map:
		for (i = 0, j = obj.length; i < j; i++ ) {
			results[i] = iterator.call(context, obj[i], i, obj);
		}
		return results;
	}

	/**
	 * Check and normalise the value of precision (must be positive integer)
	 */
	function checkPrecision(val, base) {
		val = Math.round(Math.abs(val));
		return isNaN(val)? base : val;
	}


	/**
	 * Parses a format string or object and returns format obj for use in rendering
	 *
	 * `format` is either a string with the default (positive) format, or object
	 * containing `pos` (required), `neg` and `zero` values (or a function returning
	 * either a string or object)
	 *
	 * Either string or format.pos must contain "%v" (value) to be valid
	 */
	function checkCurrencyFormat(format) {
		var defaults = lib.settings.currency.format;

		// Allow function as format parameter (should return string or object):
		if ( typeof format === "function" ) format = format();

		// Format can be a string, in which case `value` ("%v") must be present:
		if ( isString( format ) && format.match("%v") ) {

			// Create and return positive, negative and zero formats:
			return {
				pos : format,
				neg : format.replace("-", "").replace("%v", "-%v"),
				zero : format
			};

		// If no format, or object is missing valid positive value, use defaults:
		} else if ( !format || !format.pos || !format.pos.match("%v") ) {

			// If defaults is a string, casts it to an object for faster checking next time:
			return ( !isString( defaults ) ) ? defaults : lib.settings.currency.format = {
				pos : defaults,
				neg : defaults.replace("%v", "-%v"),
				zero : defaults
			};

		}
		// Otherwise, assume format was fine:
		return format;
	}


	/* --- API Methods --- */

	/**
	 * Takes a string/array of strings, removes all formatting/cruft and returns the raw float value
	 * Alias: `accounting.parse(string)`
	 *
	 * Decimal must be included in the regular expression to match floats (defaults to
	 * accounting.settings.number.decimal), so if the number uses a non-standard decimal 
	 * separator, provide it as the second argument.
	 *
	 * Also matches bracketed negatives (eg. "$ (1.99)" => -1.99)
	 *
	 * Doesn't throw any errors (`NaN`s become 0) but this may change in future
	 */
	var unformat = lib.unformat = lib.parse = function(value, decimal) {
		// Recursively unformat arrays:
		if (isArray(value)) {
			return map(value, function(val) {
				return unformat(val, decimal);
			});
		}

		// Fails silently (need decent errors):
		value = value || 0;

		// Return the value as-is if it's already a number:
		if (typeof value === "number") return value;

		// Default decimal point comes from settings, but could be set to eg. "," in opts:
		decimal = decimal || lib.settings.number.decimal;

		 // Build regex to strip out everything except digits, decimal point and minus sign:
		var regex = new RegExp("[^0-9-" + decimal + "]", ["g"]),
			unformatted = parseFloat(
				("" + value)
				.replace(/\((.*)\)/, "-$1") // replace bracketed values with negatives
				.replace(regex, '')         // strip out any cruft
				.replace(decimal, '.')      // make sure decimal point is standard
			);

		// This will fail silently which may cause trouble, let's wait and see:
		return !isNaN(unformatted) ? unformatted : 0;
	};


	/**
	 * Implementation of toFixed() that treats floats more like decimals
	 *
	 * Fixes binary rounding issues (eg. (0.615).toFixed(2) === "0.61") that present
	 * problems for accounting- and finance-related software.
	 */
	var toFixed = lib.toFixed = function(value, precision) {
		precision = checkPrecision(precision, lib.settings.number.precision);
		var power = Math.pow(10, precision);

		// Multiply up by precision, round accurately, then divide and use native toFixed():
		return (Math.round(Calc.mulFix(lib.unformat(value),power)) / power).toFixed(precision);
		//return Math.ceil(lib.unformat(value) * power) / power;
	};


	/**
	 * Format a number, with comma-separated thousands and custom precision/decimal places
	 * Alias: `accounting.format()`
	 *
	 * Localise by overriding the precision and thousand / decimal separators
	 * 2nd parameter `precision` can be an object matching `settings.number`
	 */
	var formatNumber = lib.formatNumber = lib.format = function(number, precision, thousand, decimal) {
		// Resursively format arrays:
		if (isArray(number)) {
			return map(number, function(val) {
				return formatNumber(val, precision, thousand, decimal);
			});
		}

		// Clean up number:
		number = unformat(number);

		// Build options object from second param (if object) or all params, extending defaults:
		var opts = defaults(
				(isObject(precision) ? precision : {
					precision : precision,
					thousand : thousand,
					decimal : decimal
				}),
				lib.settings.number
			),

			// Clean up precision
			usePrecision = checkPrecision(opts.precision),

			// Do some calc:
			negative = number < 0 ? "-" : "",
			base = parseInt(toFixed(Math.abs(number || 0), usePrecision), 10) + "",
			mod = base.length > 3 ? base.length % 3 : 0;

		// Format the number:
		return negative + (mod ? base.substr(0, mod) + opts.thousand : "") + base.substr(mod).replace(/(\d{3})(?=\d)/g, "$1" + opts.thousand) + (usePrecision ? opts.decimal + toFixed(Math.abs(number), usePrecision).split('.')[1] : "");
	};


	/**
	 * Format a number into currency
	 *
	 * Usage: accounting.formatMoney(number, symbol, precision, thousandsSep, decimalSep, format)
	 * defaults: (0, "$", 2, ",", ".", "%s%v")
	 *
	 * Localise by overriding the symbol, precision, thousand / decimal separators and format
	 * Second param can be an object matching `settings.currency` which is the easiest way.
	 *
	 * To do: tidy up the parameters
	 */
	var formatMoney = lib.formatMoney = function(number, symbol, precision, thousand, decimal, format) {
		// Resursively format arrays:
		if (isArray(number)) {
			return map(number, function(val){
				return formatMoney(val, symbol, precision, thousand, decimal, format);
			});
		}

		// Clean up number:
		number = unformat(number);

		// Build options object from second param (if object) or all params, extending defaults:
		var opts = defaults(
				(isObject(symbol) ? symbol : {
					symbol : symbol,
					precision : precision,
					thousand : thousand,
					decimal : decimal,
					format : format
				}),
				lib.settings.currency
			),

			// Check format (returns object with pos, neg and zero):
			formats = checkCurrencyFormat(opts.format),

			// Choose which format to use for this value:
			useFormat = number > 0 ? formats.pos : number < 0 ? formats.neg : formats.zero;

		// Return with currency symbol added:
		return useFormat.replace('%s', opts.symbol).replace('%v', formatNumber(Math.abs(number), checkPrecision(opts.precision), opts.thousand, opts.decimal));
	};


	/**
	 * Format a list of numbers into an accounting column, padding with whitespace
	 * to line up currency symbols, thousand separators and decimals places
	 *
	 * List should be an array of numbers
	 * Second parameter can be an object containing keys that match the params
	 *
	 * Returns array of accouting-formatted number strings of same length
	 *
	 * NB: `white-space:pre` CSS rule is required on the list container to prevent
	 * browsers from collapsing the whitespace in the output strings.
	 */
	lib.formatColumn = function(list, symbol, precision, thousand, decimal, format) {
		if (!list) return [];

		// Build options object from second param (if object) or all params, extending defaults:
		var opts = defaults(
				(isObject(symbol) ? symbol : {
					symbol : symbol,
					precision : precision,
					thousand : thousand,
					decimal : decimal,
					format : format
				}),
				lib.settings.currency
			),

			// Check format (returns object with pos, neg and zero), only need pos for now:
			formats = checkCurrencyFormat(opts.format),

			// Whether to pad at start of string or after currency symbol:
			padAfterSymbol = formats.pos.indexOf("%s") < formats.pos.indexOf("%v") ? true : false,

			// Store value for the length of the longest string in the column:
			maxLength = 0,

			// Format the list according to options, store the length of the longest string:
			formatted = map(list, function(val, i) {
				if (isArray(val)) {
					// Recursively format columns if list is a multi-dimensional array:
					return lib.formatColumn(val, opts);
				} else {
					// Clean up the value
					val = unformat(val);

					// Choose which format to use for this value (pos, neg or zero):
					var useFormat = val > 0 ? formats.pos : val < 0 ? formats.neg : formats.zero,

						// Format this value, push into formatted list and save the length:
						fVal = useFormat.replace('%s', opts.symbol).replace('%v', formatNumber(Math.abs(val), checkPrecision(opts.precision), opts.thousand, opts.decimal));

					if (fVal.length > maxLength) maxLength = fVal.length;
					return fVal;
				}
			});

		// Pad each number in the list and send back the column of numbers:
		return map(formatted, function(val, i) {
			// Only if this is a string (not a nested array, which would have already been padded):
			if (isString(val) && val.length < maxLength) {
				// Depending on symbol position, pad after symbol or at index 0:
				return padAfterSymbol ? val.replace(opts.symbol, opts.symbol+(new Array(maxLength - val.length + 1).join(" "))) : (new Array(maxLength - val.length + 1).join(" ")) + val;
			}
			return val;
		});
	};


	/* --- Module Definition --- */

	// Export accounting for CommonJS. If being loaded as an AMD module, define it as such.
	// Otherwise, just add `accounting` to the global object
	if (typeof exports !== 'undefined') {
		if (typeof module !== 'undefined' && module.exports) {
			exports = module.exports = lib;
		}
		exports.accounting = lib;
	} else if (typeof define === 'function' && define.amd) {
		// Return the library as an AMD module:
		define([], function() {
			return lib;
		});
	} else {
		// Use accounting.noConflict to restore `accounting` back to its original value.
		// Returns a reference to the library's `accounting` object;
		// e.g. `var numbers = accounting.noConflict();`
		lib.noConflict = (function(oldAccounting) {
			return function() {
				// Reset the value of the root's `accounting` variable:
				root.accounting = oldAccounting;
				// Delete the noConflict method:
				lib.noConflict = undefined;
				// Return reference to the library to re-assign it:
				return lib;
			};
		})(root.accounting);

		// Declare `fx` on the root (global/window) object:
		root['accounting'] = lib;
	}

	// Root will be `window` in browser or `global` on the server:
}(this));

(function () { 
		var calc = {
		/*
		 * 函数，加法函数，用来得到精确的加法结果
		 * 说明：javascript的加法结果会有误差，在两个浮点数相加的时候会比较明显。这个函数返回较为精确的加法结果。
		 * 参数：arg1：第一个加数；arg2第二个加数；d要保留的小数位数（可以不传此参数，如果不传则不处理小数位数）
		 * 调用：Calc.add(arg1,arg2,d) 返回值：两数相加的结果
		 */
		add : function(arg1, arg2, d) {
			arg1 = arg1.toString(), arg2 = arg2.toString();
			var arg1Arr = arg1.split("."), arg2Arr = arg2.split("."), d1 = arg1Arr.length == 2 ? arg1Arr[1]
					: "", d2 = arg2Arr.length == 2 ? arg2Arr[1] : "";
			var maxLen = Math.max(d1.length, d2.length);
			var m = Math.pow(10, maxLen);
			//var result = Number(((arg1 * m + arg2 * m) / m).toFixed(maxLen));
			var result = Number(accounting.toFixed(((arg1 * m + arg2 * m) / m), maxLen));
			var d = arguments[2];
			//return typeof d === "number" ? Number((result).toFixed(d)) : result;
			return typeof d === "number" ? Number(accounting.toFixed(result, d)) : result;
		},
		/*
		 * 函数：减法函数，用来得到精确的减法结果 说明：函数返回较为精确的减法结果。
		 * 参数：arg1：第一个加数；arg2第二个加数；d要保留的小数位数（可以不传此参数，如果不传则不处理小数位数
		 * 调用：Calc.sub(arg1,arg2) 返回值：两数相减的结果
		 */
		sub : function(arg1, arg2) {
			return Calc.add(arg1, -Number(arg2), arguments[2]);
		},
		/*
		 * 函数：乘法函数，用来得到精确的乘法结果 说明：函数返回较为精确的乘法结果。
		 * 参数：arg1：第一个乘数；arg2第二个乘数；d要保留的小数位数（可以不传此参数，如果不传则不处理小数位数)
		 * 调用：Calc.mul(arg1,arg2) 返回值：两数相乘的结果
		 */
		mul : function(arg1, arg2) {
			var r1 = arg1.toString(), r2 = arg2.toString(), m, resultVal, d = arguments[2];
			m = (r1.split(".")[1] ? r1.split(".")[1].length : 0)
					+ (r2.split(".")[1] ? r2.split(".")[1].length : 0);
			resultVal = Number(r1.replace(".", ""))
					* Number(r2.replace(".", "")) / Math.pow(10, m);
			//return typeof d !== "number" ? Number(resultVal) : Number(resultVal
			//		.toFixed(parseInt(d)));
			return typeof d !== "number" ? Number(resultVal) : Number(accounting.toFixed(resultVal,parseInt(d)));
		},//用于tofixed方法中乘法
		mulFix : function(arg1, arg2) {
			var r1 = arg1.toString(), r2 = arg2.toString(), m, resultVal, d = arguments[2];
			m = (r1.split(".")[1] ? r1.split(".")[1].length : 0)
					+ (r2.split(".")[1] ? r2.split(".")[1].length : 0);
			resultVal = Number(r1.replace(".", ""))
					* Number(r2.replace(".", "")) / Math.pow(10, m);
			return Number(resultVal);
		},
		/*
		 * 函数：除法函数，用来得到精确的除法结果 说明：函数返回较为精确的除法结果。
		 * 参数：arg1：除数；arg2被除数；d要保留的小数位数（可以不传此参数，如果不传则不处理小数位数)
		 * 调用：Calc.div(arg1,arg2) 返回值：arg1除于arg2的结果
		 */
		div : function(arg1, arg2) {
			var r1 = arg1.toString(), r2 = arg2.toString(), m, resultVal, d = arguments[2];
			m = (r2.split(".")[1] ? r2.split(".")[1].length : 0)
					- (r1.split(".")[1] ? r1.split(".")[1].length : 0);
			resultVal = Number(r1.replace(".", ""))
					/ Number(r2.replace(".", "")) * Math.pow(10, m);
			//return typeof d !== "number" ? Number(resultVal) : Number(resultVal
			//		.toFixed(parseInt(d)));
			return typeof d !== "number" ? Number(resultVal) : Number(accounting.toFixed(resultVal,parseInt(d)));		
		},
		/*
		 * 将数值四舍五入后格式化. @param num 数值(Number或者String) @param cent
		 * 要保留的小数位(Number) @param isThousand 是否需要千分位 0:不需要,1:需要(数值类型); @return
		 * 格式的字符串,如'1,234,567.45' @type String
		 * 调用：Calc.formatNumber(num,cent,isThousand)
		 */
		formatNumber : function(num, cent, isThousand) {
			num = num.toString().replace(/\$|\,/g, '');
			if (isNaN(num))// 检查传入数值为数值类型.
				num = "0";
			if (isNaN(cent))// 确保传入小数位为数值型数值.
				cent = 0;
			cent = parseInt(cent);
			cent = Math.abs(cent);// 求出小数位数,确保为正整数.
			if (isNaN(isThousand))// 确保传入是否需要千分位为数值类型.
				isThousand = 0;
			isThousand = parseInt(isThousand);
			if (isThousand < 0)
				isThousand = 0;
			if (isThousand >= 1) // 确保传入的数值只为0或1
				isThousand = 1;
			sign = (num == (num = Math.abs(num)));// 获取符号(正/负数)
			// Math.floor:返回小于等于其数值参数的最大整数
			num = Math.floor(num * Math.pow(10, cent) + 0.50000000001);// 把指定的小数位先转换成整数.多余的小数位四舍五入.
			cents = num % Math.pow(10, cent); // 求出小数位数值.
			num = Math.floor(num / Math.pow(10, cent)).toString();// 求出整数位数值.
			cents = cents.toString();// 把小数位转换成字符串,以便求小数位长度.
			while (cents.length < cent) {// 补足小数位到指定的位数.
				cents = "0" + cents;
			}
			if (isThousand == 0) // 不需要千分位符.
				return (((sign) ? '' : '-') + num + '.' + cents);
			// 对整数部分进行千分位格式化.
			for ( var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
				num = num.substring(0, num.length - (4 * i + 3)) + ','
						+ num.substring(num.length - (4 * i + 3));
			return (((sign) ? '' : '-') + num + '.' + cents);
		}
	};
	window.Calc = calc; 
}()); 


/**
 * 修复toFixed的四舍五入bug
 * 在IE 下和FF 下对于小数的进位有点不同。 
 * 例如（ 0.005）在ie 下 toFix(2)=0.00. 在FF 下 toFix(2)=0.01. 
 * @param s
 * @returns
 */
/*
Number.prototype.toFixed = function(s) {
	//return (parseInt(this * Math.pow( 10, s ) + 0.5)/ Math.pow( 10, s )).toString();
	var changenum = (parseInt(this * Math.pow(10, s) + 0.5) / Math.pow(10, s))
			.toString();
	var index = changenum.indexOf(".");
	if (index < 0 && s > 0) {
		changenum = changenum + ".";
		for (i = 0; i < s; i++) {
			changenum = changenum + "0";
		}

	} else {
		index = changenum.length - index;
		for (i = 0; i < (s - index) + 1; i++) {
			changenum = changenum + "0";
		}

	}
	return changenum;
};*/

/* qTip2 v2.2.1 | Plugins: tips modal viewport svg imagemap ie6 | Styles: core basic css3 | qtip2.com | Licensed MIT | Sat Sep 06 2014 23:12:07 */

!function(a,b,c){!function(a){"use strict";"function"==typeof define&&define.amd?define(["jquery"],a):jQuery&&!jQuery.fn.qtip&&a(jQuery)}(function(d){"use strict";function e(a,b,c,e){this.id=c,this.target=a,this.tooltip=F,this.elements={target:a},this._id=S+"-"+c,this.timers={img:{}},this.options=b,this.plugins={},this.cache={event:{},target:d(),disabled:E,attr:e,onTooltip:E,lastClass:""},this.rendered=this.destroyed=this.disabled=this.waiting=this.hiddenDuringWait=this.positioning=this.triggering=E}function f(a){return a===F||"object"!==d.type(a)}function g(a){return!(d.isFunction(a)||a&&a.attr||a.length||"object"===d.type(a)&&(a.jquery||a.then))}function h(a){var b,c,e,h;return f(a)?E:(f(a.metadata)&&(a.metadata={type:a.metadata}),"content"in a&&(b=a.content,f(b)||b.jquery||b.done?b=a.content={text:c=g(b)?E:b}:c=b.text,"ajax"in b&&(e=b.ajax,h=e&&e.once!==E,delete b.ajax,b.text=function(a,b){var f=c||d(this).attr(b.options.content.attr)||"Loading...",g=d.ajax(d.extend({},e,{context:b})).then(e.success,F,e.error).then(function(a){return a&&h&&b.set("content.text",a),a},function(a,c,d){b.destroyed||0===a.status||b.set("content.text",c+": "+d)});return h?f:(b.set("content.text",f),g)}),"title"in b&&(d.isPlainObject(b.title)&&(b.button=b.title.button,b.title=b.title.text),g(b.title||E)&&(b.title=E))),"position"in a&&f(a.position)&&(a.position={my:a.position,at:a.position}),"show"in a&&f(a.show)&&(a.show=a.show.jquery?{target:a.show}:a.show===D?{ready:D}:{event:a.show}),"hide"in a&&f(a.hide)&&(a.hide=a.hide.jquery?{target:a.hide}:{event:a.hide}),"style"in a&&f(a.style)&&(a.style={classes:a.style}),d.each(R,function(){this.sanitize&&this.sanitize(a)}),a)}function i(a,b){for(var c,d=0,e=a,f=b.split(".");e=e[f[d++]];)d<f.length&&(c=e);return[c||a,f.pop()]}function j(a,b){var c,d,e;for(c in this.checks)for(d in this.checks[c])(e=new RegExp(d,"i").exec(a))&&(b.push(e),("builtin"===c||this.plugins[c])&&this.checks[c][d].apply(this.plugins[c]||this,b))}function k(a){return V.concat("").join(a?"-"+a+" ":" ")}function l(a,b){return b>0?setTimeout(d.proxy(a,this),b):void a.call(this)}function m(a){this.tooltip.hasClass(ab)||(clearTimeout(this.timers.show),clearTimeout(this.timers.hide),this.timers.show=l.call(this,function(){this.toggle(D,a)},this.options.show.delay))}function n(a){if(!this.tooltip.hasClass(ab)&&!this.destroyed){var b=d(a.relatedTarget),c=b.closest(W)[0]===this.tooltip[0],e=b[0]===this.options.show.target[0];if(clearTimeout(this.timers.show),clearTimeout(this.timers.hide),this!==b[0]&&"mouse"===this.options.position.target&&c||this.options.hide.fixed&&/mouse(out|leave|move)/.test(a.type)&&(c||e))try{a.preventDefault(),a.stopImmediatePropagation()}catch(f){}else this.timers.hide=l.call(this,function(){this.toggle(E,a)},this.options.hide.delay,this)}}function o(a){!this.tooltip.hasClass(ab)&&this.options.hide.inactive&&(clearTimeout(this.timers.inactive),this.timers.inactive=l.call(this,function(){this.hide(a)},this.options.hide.inactive))}function p(a){this.rendered&&this.tooltip[0].offsetWidth>0&&this.reposition(a)}function q(a,c,e){d(b.body).delegate(a,(c.split?c:c.join("."+S+" "))+"."+S,function(){var a=y.api[d.attr(this,U)];a&&!a.disabled&&e.apply(a,arguments)})}function r(a,c,f){var g,i,j,k,l,m=d(b.body),n=a[0]===b?m:a,o=a.metadata?a.metadata(f.metadata):F,p="html5"===f.metadata.type&&o?o[f.metadata.name]:F,q=a.data(f.metadata.name||"qtipopts");try{q="string"==typeof q?d.parseJSON(q):q}catch(r){}if(k=d.extend(D,{},y.defaults,f,"object"==typeof q?h(q):F,h(p||o)),i=k.position,k.id=c,"boolean"==typeof k.content.text){if(j=a.attr(k.content.attr),k.content.attr===E||!j)return E;k.content.text=j}if(i.container.length||(i.container=m),i.target===E&&(i.target=n),k.show.target===E&&(k.show.target=n),k.show.solo===D&&(k.show.solo=i.container.closest("body")),k.hide.target===E&&(k.hide.target=n),k.position.viewport===D&&(k.position.viewport=i.container),i.container=i.container.eq(0),i.at=new A(i.at,D),i.my=new A(i.my),a.data(S))if(k.overwrite)a.qtip("destroy",!0);else if(k.overwrite===E)return E;return a.attr(T,c),k.suppress&&(l=a.attr("title"))&&a.removeAttr("title").attr(cb,l).attr("title",""),g=new e(a,k,c,!!j),a.data(S,g),g}function s(a){return a.charAt(0).toUpperCase()+a.slice(1)}function t(a,b){var d,e,f=b.charAt(0).toUpperCase()+b.slice(1),g=(b+" "+rb.join(f+" ")+f).split(" "),h=0;if(qb[b])return a.css(qb[b]);for(;d=g[h++];)if((e=a.css(d))!==c)return qb[b]=d,e}function u(a,b){return Math.ceil(parseFloat(t(a,b)))}function v(a,b){this._ns="tip",this.options=b,this.offset=b.offset,this.size=[b.width,b.height],this.init(this.qtip=a)}function w(a,b){this.options=b,this._ns="-modal",this.init(this.qtip=a)}function x(a){this._ns="ie6",this.init(this.qtip=a)}var y,z,A,B,C,D=!0,E=!1,F=null,G="x",H="y",I="width",J="height",K="top",L="left",M="bottom",N="right",O="center",P="flipinvert",Q="shift",R={},S="qtip",T="data-hasqtip",U="data-qtip-id",V=["ui-widget","ui-tooltip"],W="."+S,X="click dblclick mousedown mouseup mousemove mouseleave mouseenter".split(" "),Y=S+"-fixed",Z=S+"-default",$=S+"-focus",_=S+"-hover",ab=S+"-disabled",bb="_replacedByqTip",cb="oldtitle",db={ie:function(){for(var a=4,c=b.createElement("div");(c.innerHTML="<!--[if gt IE "+a+"]><i></i><![endif]-->")&&c.getElementsByTagName("i")[0];a+=1);return a>4?a:0/0}(),iOS:parseFloat((""+(/CPU.*OS ([0-9_]{1,5})|(CPU like).*AppleWebKit.*Mobile/i.exec(navigator.userAgent)||[0,""])[1]).replace("undefined","3_2").replace("_",".").replace("_",""))||E};z=e.prototype,z._when=function(a){return d.when.apply(d,a)},z.render=function(a){if(this.rendered||this.destroyed)return this;var b,c=this,e=this.options,f=this.cache,g=this.elements,h=e.content.text,i=e.content.title,j=e.content.button,k=e.position,l=("."+this._id+" ",[]);return d.attr(this.target[0],"aria-describedby",this._id),f.posClass=this._createPosClass((this.position={my:k.my,at:k.at}).my),this.tooltip=g.tooltip=b=d("<div/>",{id:this._id,"class":[S,Z,e.style.classes,f.posClass].join(" "),width:e.style.width||"",height:e.style.height||"",tracking:"mouse"===k.target&&k.adjust.mouse,role:"alert","aria-live":"polite","aria-atomic":E,"aria-describedby":this._id+"-content","aria-hidden":D}).toggleClass(ab,this.disabled).attr(U,this.id).data(S,this).appendTo(k.container).append(g.content=d("<div />",{"class":S+"-content",id:this._id+"-content","aria-atomic":D})),this.rendered=-1,this.positioning=D,i&&(this._createTitle(),d.isFunction(i)||l.push(this._updateTitle(i,E))),j&&this._createButton(),d.isFunction(h)||l.push(this._updateContent(h,E)),this.rendered=D,this._setWidget(),d.each(R,function(a){var b;"render"===this.initialize&&(b=this(c))&&(c.plugins[a]=b)}),this._unassignEvents(),this._assignEvents(),this._when(l).then(function(){c._trigger("render"),c.positioning=E,c.hiddenDuringWait||!e.show.ready&&!a||c.toggle(D,f.event,E),c.hiddenDuringWait=E}),y.api[this.id]=this,this},z.destroy=function(a){function b(){if(!this.destroyed){this.destroyed=D;var a,b=this.target,c=b.attr(cb);this.rendered&&this.tooltip.stop(1,0).find("*").remove().end().remove(),d.each(this.plugins,function(){this.destroy&&this.destroy()});for(a in this.timers)clearTimeout(this.timers[a]);b.removeData(S).removeAttr(U).removeAttr(T).removeAttr("aria-describedby"),this.options.suppress&&c&&b.attr("title",c).removeAttr(cb),this._unassignEvents(),this.options=this.elements=this.cache=this.timers=this.plugins=this.mouse=F,delete y.api[this.id]}}return this.destroyed?this.target:(a===D&&"hide"!==this.triggering||!this.rendered?b.call(this):(this.tooltip.one("tooltiphidden",d.proxy(b,this)),!this.triggering&&this.hide()),this.target)},B=z.checks={builtin:{"^id$":function(a,b,c,e){var f=c===D?y.nextid:c,g=S+"-"+f;f!==E&&f.length>0&&!d("#"+g).length?(this._id=g,this.rendered&&(this.tooltip[0].id=this._id,this.elements.content[0].id=this._id+"-content",this.elements.title[0].id=this._id+"-title")):a[b]=e},"^prerender":function(a,b,c){c&&!this.rendered&&this.render(this.options.show.ready)},"^content.text$":function(a,b,c){this._updateContent(c)},"^content.attr$":function(a,b,c,d){this.options.content.text===this.target.attr(d)&&this._updateContent(this.target.attr(c))},"^content.title$":function(a,b,c){return c?(c&&!this.elements.title&&this._createTitle(),void this._updateTitle(c)):this._removeTitle()},"^content.button$":function(a,b,c){this._updateButton(c)},"^content.title.(text|button)$":function(a,b,c){this.set("content."+b,c)},"^position.(my|at)$":function(a,b,c){"string"==typeof c&&(this.position[b]=a[b]=new A(c,"at"===b))},"^position.container$":function(a,b,c){this.rendered&&this.tooltip.appendTo(c)},"^show.ready$":function(a,b,c){c&&(!this.rendered&&this.render(D)||this.toggle(D))},"^style.classes$":function(a,b,c,d){this.rendered&&this.tooltip.removeClass(d).addClass(c)},"^style.(width|height)":function(a,b,c){this.rendered&&this.tooltip.css(b,c)},"^style.widget|content.title":function(){this.rendered&&this._setWidget()},"^style.def":function(a,b,c){this.rendered&&this.tooltip.toggleClass(Z,!!c)},"^events.(render|show|move|hide|focus|blur)$":function(a,b,c){this.rendered&&this.tooltip[(d.isFunction(c)?"":"un")+"bind"]("tooltip"+b,c)},"^(show|hide|position).(event|target|fixed|inactive|leave|distance|viewport|adjust)":function(){if(this.rendered){var a=this.options.position;this.tooltip.attr("tracking","mouse"===a.target&&a.adjust.mouse),this._unassignEvents(),this._assignEvents()}}}},z.get=function(a){if(this.destroyed)return this;var b=i(this.options,a.toLowerCase()),c=b[0][b[1]];return c.precedance?c.string():c};var eb=/^position\.(my|at|adjust|target|container|viewport)|style|content|show\.ready/i,fb=/^prerender|show\.ready/i;z.set=function(a,b){if(this.destroyed)return this;{var c,e=this.rendered,f=E,g=this.options;this.checks}return"string"==typeof a?(c=a,a={},a[c]=b):a=d.extend({},a),d.each(a,function(b,c){if(e&&fb.test(b))return void delete a[b];var h,j=i(g,b.toLowerCase());h=j[0][j[1]],j[0][j[1]]=c&&c.nodeType?d(c):c,f=eb.test(b)||f,a[b]=[j[0],j[1],c,h]}),h(g),this.positioning=D,d.each(a,d.proxy(j,this)),this.positioning=E,this.rendered&&this.tooltip[0].offsetWidth>0&&f&&this.reposition("mouse"===g.position.target?F:this.cache.event),this},z._update=function(a,b){var c=this,e=this.cache;return this.rendered&&a?(d.isFunction(a)&&(a=a.call(this.elements.target,e.event,this)||""),d.isFunction(a.then)?(e.waiting=D,a.then(function(a){return e.waiting=E,c._update(a,b)},F,function(a){return c._update(a,b)})):a===E||!a&&""!==a?E:(a.jquery&&a.length>0?b.empty().append(a.css({display:"block",visibility:"visible"})):b.html(a),this._waitForContent(b).then(function(a){c.rendered&&c.tooltip[0].offsetWidth>0&&c.reposition(e.event,!a.length)}))):E},z._waitForContent=function(a){var b=this.cache;return b.waiting=D,(d.fn.imagesLoaded?a.imagesLoaded():d.Deferred().resolve([])).done(function(){b.waiting=E}).promise()},z._updateContent=function(a,b){this._update(a,this.elements.content,b)},z._updateTitle=function(a,b){this._update(a,this.elements.title,b)===E&&this._removeTitle(E)},z._createTitle=function(){var a=this.elements,b=this._id+"-title";a.titlebar&&this._removeTitle(),a.titlebar=d("<div />",{"class":S+"-titlebar "+(this.options.style.widget?k("header"):"")}).append(a.title=d("<div />",{id:b,"class":S+"-title","aria-atomic":D})).insertBefore(a.content).delegate(".qtip-close","mousedown keydown mouseup keyup mouseout",function(a){d(this).toggleClass("ui-state-active ui-state-focus","down"===a.type.substr(-4))}).delegate(".qtip-close","mouseover mouseout",function(a){d(this).toggleClass("ui-state-hover","mouseover"===a.type)}),this.options.content.button&&this._createButton()},z._removeTitle=function(a){var b=this.elements;b.title&&(b.titlebar.remove(),b.titlebar=b.title=b.button=F,a!==E&&this.reposition())},z._createPosClass=function(a){return S+"-pos-"+(a||this.options.position.my).abbrev()},z.reposition=function(c,e){if(!this.rendered||this.positioning||this.destroyed)return this;this.positioning=D;var f,g,h,i,j=this.cache,k=this.tooltip,l=this.options.position,m=l.target,n=l.my,o=l.at,p=l.viewport,q=l.container,r=l.adjust,s=r.method.split(" "),t=k.outerWidth(E),u=k.outerHeight(E),v=0,w=0,x=k.css("position"),y={left:0,top:0},z=k[0].offsetWidth>0,A=c&&"scroll"===c.type,B=d(a),C=q[0].ownerDocument,F=this.mouse;if(d.isArray(m)&&2===m.length)o={x:L,y:K},y={left:m[0],top:m[1]};else if("mouse"===m)o={x:L,y:K},(!r.mouse||this.options.hide.distance)&&j.origin&&j.origin.pageX?c=j.origin:!c||c&&("resize"===c.type||"scroll"===c.type)?c=j.event:F&&F.pageX&&(c=F),"static"!==x&&(y=q.offset()),C.body.offsetWidth!==(a.innerWidth||C.documentElement.clientWidth)&&(g=d(b.body).offset()),y={left:c.pageX-y.left+(g&&g.left||0),top:c.pageY-y.top+(g&&g.top||0)},r.mouse&&A&&F&&(y.left-=(F.scrollX||0)-B.scrollLeft(),y.top-=(F.scrollY||0)-B.scrollTop());else{if("event"===m?c&&c.target&&"scroll"!==c.type&&"resize"!==c.type?j.target=d(c.target):c.target||(j.target=this.elements.target):"event"!==m&&(j.target=d(m.jquery?m:this.elements.target)),m=j.target,m=d(m).eq(0),0===m.length)return this;m[0]===b||m[0]===a?(v=db.iOS?a.innerWidth:m.width(),w=db.iOS?a.innerHeight:m.height(),m[0]===a&&(y={top:(p||m).scrollTop(),left:(p||m).scrollLeft()})):R.imagemap&&m.is("area")?f=R.imagemap(this,m,o,R.viewport?s:E):R.svg&&m&&m[0].ownerSVGElement?f=R.svg(this,m,o,R.viewport?s:E):(v=m.outerWidth(E),w=m.outerHeight(E),y=m.offset()),f&&(v=f.width,w=f.height,g=f.offset,y=f.position),y=this.reposition.offset(m,y,q),(db.iOS>3.1&&db.iOS<4.1||db.iOS>=4.3&&db.iOS<4.33||!db.iOS&&"fixed"===x)&&(y.left-=B.scrollLeft(),y.top-=B.scrollTop()),(!f||f&&f.adjustable!==E)&&(y.left+=o.x===N?v:o.x===O?v/2:0,y.top+=o.y===M?w:o.y===O?w/2:0)}return y.left+=r.x+(n.x===N?-t:n.x===O?-t/2:0),y.top+=r.y+(n.y===M?-u:n.y===O?-u/2:0),R.viewport?(h=y.adjusted=R.viewport(this,y,l,v,w,t,u),g&&h.left&&(y.left+=g.left),g&&h.top&&(y.top+=g.top),h.my&&(this.position.my=h.my)):y.adjusted={left:0,top:0},j.posClass!==(i=this._createPosClass(this.position.my))&&k.removeClass(j.posClass).addClass(j.posClass=i),this._trigger("move",[y,p.elem||p],c)?(delete y.adjusted,e===E||!z||isNaN(y.left)||isNaN(y.top)||"mouse"===m||!d.isFunction(l.effect)?k.css(y):d.isFunction(l.effect)&&(l.effect.call(k,this,d.extend({},y)),k.queue(function(a){d(this).css({opacity:"",height:""}),db.ie&&this.style.removeAttribute("filter"),a()})),this.positioning=E,this):this},z.reposition.offset=function(a,c,e){function f(a,b){c.left+=b*a.scrollLeft(),c.top+=b*a.scrollTop()}if(!e[0])return c;var g,h,i,j,k=d(a[0].ownerDocument),l=!!db.ie&&"CSS1Compat"!==b.compatMode,m=e[0];do"static"!==(h=d.css(m,"position"))&&("fixed"===h?(i=m.getBoundingClientRect(),f(k,-1)):(i=d(m).position(),i.left+=parseFloat(d.css(m,"borderLeftWidth"))||0,i.top+=parseFloat(d.css(m,"borderTopWidth"))||0),c.left-=i.left+(parseFloat(d.css(m,"marginLeft"))||0),c.top-=i.top+(parseFloat(d.css(m,"marginTop"))||0),g||"hidden"===(j=d.css(m,"overflow"))||"visible"===j||(g=d(m)));while(m=m.offsetParent);return g&&(g[0]!==k[0]||l)&&f(g,1),c};var gb=(A=z.reposition.Corner=function(a,b){a=(""+a).replace(/([A-Z])/," $1").replace(/middle/gi,O).toLowerCase(),this.x=(a.match(/left|right/i)||a.match(/center/)||["inherit"])[0].toLowerCase(),this.y=(a.match(/top|bottom|center/i)||["inherit"])[0].toLowerCase(),this.forceY=!!b;var c=a.charAt(0);this.precedance="t"===c||"b"===c?H:G}).prototype;gb.invert=function(a,b){this[a]=this[a]===L?N:this[a]===N?L:b||this[a]},gb.string=function(a){var b=this.x,c=this.y,d=b!==c?"center"===b||"center"!==c&&(this.precedance===H||this.forceY)?[c,b]:[b,c]:[b];return a!==!1?d.join(" "):d},gb.abbrev=function(){var a=this.string(!1);return a[0].charAt(0)+(a[1]&&a[1].charAt(0)||"")},gb.clone=function(){return new A(this.string(),this.forceY)},z.toggle=function(a,c){var e=this.cache,f=this.options,g=this.tooltip;if(c){if(/over|enter/.test(c.type)&&e.event&&/out|leave/.test(e.event.type)&&f.show.target.add(c.target).length===f.show.target.length&&g.has(c.relatedTarget).length)return this;e.event=d.event.fix(c)}if(this.waiting&&!a&&(this.hiddenDuringWait=D),!this.rendered)return a?this.render(1):this;if(this.destroyed||this.disabled)return this;var h,i,j,k=a?"show":"hide",l=this.options[k],m=(this.options[a?"hide":"show"],this.options.position),n=this.options.content,o=this.tooltip.css("width"),p=this.tooltip.is(":visible"),q=a||1===l.target.length,r=!c||l.target.length<2||e.target[0]===c.target;return(typeof a).search("boolean|number")&&(a=!p),h=!g.is(":animated")&&p===a&&r,i=h?F:!!this._trigger(k,[90]),this.destroyed?this:(i!==E&&a&&this.focus(c),!i||h?this:(d.attr(g[0],"aria-hidden",!a),a?(this.mouse&&(e.origin=d.event.fix(this.mouse)),d.isFunction(n.text)&&this._updateContent(n.text,E),d.isFunction(n.title)&&this._updateTitle(n.title,E),!C&&"mouse"===m.target&&m.adjust.mouse&&(d(b).bind("mousemove."+S,this._storeMouse),C=D),o||g.css("width",g.outerWidth(E)),this.reposition(c,arguments[2]),o||g.css("width",""),l.solo&&("string"==typeof l.solo?d(l.solo):d(W,l.solo)).not(g).not(l.target).qtip("hide",d.Event("tooltipsolo"))):(clearTimeout(this.timers.show),delete e.origin,C&&!d(W+'[tracking="true"]:visible',l.solo).not(g).length&&(d(b).unbind("mousemove."+S),C=E),this.blur(c)),j=d.proxy(function(){a?(db.ie&&g[0].style.removeAttribute("filter"),g.css("overflow",""),"string"==typeof l.autofocus&&d(this.options.show.autofocus,g).focus(),this.options.show.target.trigger("qtip-"+this.id+"-inactive")):g.css({display:"",visibility:"",opacity:"",left:"",top:""}),this._trigger(a?"visible":"hidden")},this),l.effect===E||q===E?(g[k](),j()):d.isFunction(l.effect)?(g.stop(1,1),l.effect.call(g,this),g.queue("fx",function(a){j(),a()})):g.fadeTo(90,a?1:0,j),a&&l.target.trigger("qtip-"+this.id+"-inactive"),this))},z.show=function(a){return this.toggle(D,a)},z.hide=function(a){return this.toggle(E,a)},z.focus=function(a){if(!this.rendered||this.destroyed)return this;var b=d(W),c=this.tooltip,e=parseInt(c[0].style.zIndex,10),f=y.zindex+b.length;return c.hasClass($)||this._trigger("focus",[f],a)&&(e!==f&&(b.each(function(){this.style.zIndex>e&&(this.style.zIndex=this.style.zIndex-1)}),b.filter("."+$).qtip("blur",a)),c.addClass($)[0].style.zIndex=f),this},z.blur=function(a){return!this.rendered||this.destroyed?this:(this.tooltip.removeClass($),this._trigger("blur",[this.tooltip.css("zIndex")],a),this)},z.disable=function(a){return this.destroyed?this:("toggle"===a?a=!(this.rendered?this.tooltip.hasClass(ab):this.disabled):"boolean"!=typeof a&&(a=D),this.rendered&&this.tooltip.toggleClass(ab,a).attr("aria-disabled",a),this.disabled=!!a,this)},z.enable=function(){return this.disable(E)},z._createButton=function(){var a=this,b=this.elements,c=b.tooltip,e=this.options.content.button,f="string"==typeof e,g=f?e:"Close tooltip";b.button&&b.button.remove(),b.button=e.jquery?e:d("<a />",{"class":"qtip-close "+(this.options.style.widget?"":S+"-icon"),title:g,"aria-label":g}).prepend(d("<span />",{"class":"ui-icon ui-icon-close",html:"&times;"})),b.button.appendTo(b.titlebar||c).attr("role","button").click(function(b){return c.hasClass(ab)||a.hide(b),E})},z._updateButton=function(a){if(!this.rendered)return E;var b=this.elements.button;a?this._createButton():b.remove()},z._setWidget=function(){var a=this.options.style.widget,b=this.elements,c=b.tooltip,d=c.hasClass(ab);c.removeClass(ab),ab=a?"ui-state-disabled":"qtip-disabled",c.toggleClass(ab,d),c.toggleClass("ui-helper-reset "+k(),a).toggleClass(Z,this.options.style.def&&!a),b.content&&b.content.toggleClass(k("content"),a),b.titlebar&&b.titlebar.toggleClass(k("header"),a),b.button&&b.button.toggleClass(S+"-icon",!a)},z._storeMouse=function(a){return(this.mouse=d.event.fix(a)).type="mousemove",this},z._bind=function(a,b,c,e,f){if(a&&c&&b.length){var g="."+this._id+(e?"-"+e:"");return d(a).bind((b.split?b:b.join(g+" "))+g,d.proxy(c,f||this)),this}},z._unbind=function(a,b){return a&&d(a).unbind("."+this._id+(b?"-"+b:"")),this},z._trigger=function(a,b,c){var e=d.Event("tooltip"+a);return e.originalEvent=c&&d.extend({},c)||this.cache.event||F,this.triggering=a,this.tooltip.trigger(e,[this].concat(b||[])),this.triggering=E,!e.isDefaultPrevented()},z._bindEvents=function(a,b,c,e,f,g){var h=c.filter(e).add(e.filter(c)),i=[];h.length&&(d.each(b,function(b,c){var e=d.inArray(c,a);e>-1&&i.push(a.splice(e,1)[0])}),i.length&&(this._bind(h,i,function(a){var b=this.rendered?this.tooltip[0].offsetWidth>0:!1;(b?g:f).call(this,a)}),c=c.not(h),e=e.not(h))),this._bind(c,a,f),this._bind(e,b,g)},z._assignInitialEvents=function(a){function b(a){return this.disabled||this.destroyed?E:(this.cache.event=a&&d.event.fix(a),this.cache.target=a&&d(a.target),clearTimeout(this.timers.show),void(this.timers.show=l.call(this,function(){this.render("object"==typeof a||c.show.ready)},c.prerender?0:c.show.delay)))}var c=this.options,e=c.show.target,f=c.hide.target,g=c.show.event?d.trim(""+c.show.event).split(" "):[],h=c.hide.event?d.trim(""+c.hide.event).split(" "):[];this._bind(this.elements.target,["remove","removeqtip"],function(){this.destroy(!0)},"destroy"),/mouse(over|enter)/i.test(c.show.event)&&!/mouse(out|leave)/i.test(c.hide.event)&&h.push("mouseleave"),this._bind(e,"mousemove",function(a){this._storeMouse(a),this.cache.onTarget=D}),this._bindEvents(g,h,e,f,b,function(){return this.timers?void clearTimeout(this.timers.show):E}),(c.show.ready||c.prerender)&&b.call(this,a)},z._assignEvents=function(){var c=this,e=this.options,f=e.position,g=this.tooltip,h=e.show.target,i=e.hide.target,j=f.container,k=f.viewport,l=d(b),q=(d(b.body),d(a)),r=e.show.event?d.trim(""+e.show.event).split(" "):[],s=e.hide.event?d.trim(""+e.hide.event).split(" "):[];d.each(e.events,function(a,b){c._bind(g,"toggle"===a?["tooltipshow","tooltiphide"]:["tooltip"+a],b,null,g)}),/mouse(out|leave)/i.test(e.hide.event)&&"window"===e.hide.leave&&this._bind(l,["mouseout","blur"],function(a){/select|option/.test(a.target.nodeName)||a.relatedTarget||this.hide(a)}),e.hide.fixed?i=i.add(g.addClass(Y)):/mouse(over|enter)/i.test(e.show.event)&&this._bind(i,"mouseleave",function(){clearTimeout(this.timers.show)}),(""+e.hide.event).indexOf("unfocus")>-1&&this._bind(j.closest("html"),["mousedown","touchstart"],function(a){var b=d(a.target),c=this.rendered&&!this.tooltip.hasClass(ab)&&this.tooltip[0].offsetWidth>0,e=b.parents(W).filter(this.tooltip[0]).length>0;b[0]===this.target[0]||b[0]===this.tooltip[0]||e||this.target.has(b[0]).length||!c||this.hide(a)}),"number"==typeof e.hide.inactive&&(this._bind(h,"qtip-"+this.id+"-inactive",o,"inactive"),this._bind(i.add(g),y.inactiveEvents,o)),this._bindEvents(r,s,h,i,m,n),this._bind(h.add(g),"mousemove",function(a){if("number"==typeof e.hide.distance){var b=this.cache.origin||{},c=this.options.hide.distance,d=Math.abs;(d(a.pageX-b.pageX)>=c||d(a.pageY-b.pageY)>=c)&&this.hide(a)}this._storeMouse(a)}),"mouse"===f.target&&f.adjust.mouse&&(e.hide.event&&this._bind(h,["mouseenter","mouseleave"],function(a){return this.cache?void(this.cache.onTarget="mouseenter"===a.type):E}),this._bind(l,"mousemove",function(a){this.rendered&&this.cache.onTarget&&!this.tooltip.hasClass(ab)&&this.tooltip[0].offsetWidth>0&&this.reposition(a)})),(f.adjust.resize||k.length)&&this._bind(d.event.special.resize?k:q,"resize",p),f.adjust.scroll&&this._bind(q.add(f.container),"scroll",p)},z._unassignEvents=function(){var c=this.options,e=c.show.target,f=c.hide.target,g=d.grep([this.elements.target[0],this.rendered&&this.tooltip[0],c.position.container[0],c.position.viewport[0],c.position.container.closest("html")[0],a,b],function(a){return"object"==typeof a});e&&e.toArray&&(g=g.concat(e.toArray())),f&&f.toArray&&(g=g.concat(f.toArray())),this._unbind(g)._unbind(g,"destroy")._unbind(g,"inactive")},d(function(){q(W,["mouseenter","mouseleave"],function(a){var b="mouseenter"===a.type,c=d(a.currentTarget),e=d(a.relatedTarget||a.target),f=this.options;b?(this.focus(a),c.hasClass(Y)&&!c.hasClass(ab)&&clearTimeout(this.timers.hide)):"mouse"===f.position.target&&f.position.adjust.mouse&&f.hide.event&&f.show.target&&!e.closest(f.show.target[0]).length&&this.hide(a),c.toggleClass(_,b)}),q("["+U+"]",X,o)}),y=d.fn.qtip=function(a,b,e){var f=(""+a).toLowerCase(),g=F,i=d.makeArray(arguments).slice(1),j=i[i.length-1],k=this[0]?d.data(this[0],S):F;return!arguments.length&&k||"api"===f?k:"string"==typeof a?(this.each(function(){var a=d.data(this,S);if(!a)return D;if(j&&j.timeStamp&&(a.cache.event=j),!b||"option"!==f&&"options"!==f)a[f]&&a[f].apply(a,i);else{if(e===c&&!d.isPlainObject(b))return g=a.get(b),E;a.set(b,e)}}),g!==F?g:this):"object"!=typeof a&&arguments.length?void 0:(k=h(d.extend(D,{},a)),this.each(function(a){var b,c;return c=d.isArray(k.id)?k.id[a]:k.id,c=!c||c===E||c.length<1||y.api[c]?y.nextid++:c,b=r(d(this),c,k),b===E?D:(y.api[c]=b,d.each(R,function(){"initialize"===this.initialize&&this(b)}),void b._assignInitialEvents(j))}))},d.qtip=e,y.api={},d.each({attr:function(a,b){if(this.length){var c=this[0],e="title",f=d.data(c,"qtip");if(a===e&&f&&"object"==typeof f&&f.options.suppress)return arguments.length<2?d.attr(c,cb):(f&&f.options.content.attr===e&&f.cache.attr&&f.set("content.text",b),this.attr(cb,b))}return d.fn["attr"+bb].apply(this,arguments)},clone:function(a){var b=(d([]),d.fn["clone"+bb].apply(this,arguments));return a||b.filter("["+cb+"]").attr("title",function(){return d.attr(this,cb)}).removeAttr(cb),b}},function(a,b){if(!b||d.fn[a+bb])return D;var c=d.fn[a+bb]=d.fn[a];d.fn[a]=function(){return b.apply(this,arguments)||c.apply(this,arguments)}}),d.ui||(d["cleanData"+bb]=d.cleanData,d.cleanData=function(a){for(var b,c=0;(b=d(a[c])).length;c++)if(b.attr(T))try{b.triggerHandler("removeqtip")}catch(e){}d["cleanData"+bb].apply(this,arguments)}),y.version="2.2.1",y.nextid=0,y.inactiveEvents=X,y.zindex=15e3,y.defaults={prerender:E,id:E,overwrite:D,suppress:D,content:{text:D,attr:"title",title:E,button:E},position:{my:"top left",at:"bottom right",target:E,container:E,viewport:E,adjust:{x:0,y:0,mouse:D,scroll:D,resize:D,method:"flipinvert flipinvert"},effect:function(a,b){d(this).animate(b,{duration:200,queue:E})}},show:{target:E,event:"mouseenter",effect:D,delay:90,solo:E,ready:E,autofocus:E},hide:{target:E,event:"mouseleave",effect:D,delay:0,fixed:E,inactive:E,leave:"window",distance:E},style:{classes:"",widget:E,width:E,height:E,def:D},events:{render:F,move:F,show:F,hide:F,toggle:F,visible:F,hidden:F,focus:F,blur:F}};var hb,ib="margin",jb="border",kb="color",lb="background-color",mb="transparent",nb=" !important",ob=!!b.createElement("canvas").getContext,pb=/rgba?\(0, 0, 0(, 0)?\)|transparent|#123456/i,qb={},rb=["Webkit","O","Moz","ms"];if(ob)var sb=a.devicePixelRatio||1,tb=function(){var a=b.createElement("canvas").getContext("2d");return a.backingStorePixelRatio||a.webkitBackingStorePixelRatio||a.mozBackingStorePixelRatio||a.msBackingStorePixelRatio||a.oBackingStorePixelRatio||1}(),ub=sb/tb;else var vb=function(a,b,c){return"<qtipvml:"+a+' xmlns="urn:schemas-microsoft.com:vml" class="qtip-vml" '+(b||"")+' style="behavior: url(#default#VML); '+(c||"")+'" />'};d.extend(v.prototype,{init:function(a){var b,c;c=this.element=a.elements.tip=d("<div />",{"class":S+"-tip"}).prependTo(a.tooltip),ob?(b=d("<canvas />").appendTo(this.element)[0].getContext("2d"),b.lineJoin="miter",b.miterLimit=1e5,b.save()):(b=vb("shape",'coordorigin="0,0"',"position:absolute;"),this.element.html(b+b),a._bind(d("*",c).add(c),["click","mousedown"],function(a){a.stopPropagation()},this._ns)),a._bind(a.tooltip,"tooltipmove",this.reposition,this._ns,this),this.create()},_swapDimensions:function(){this.size[0]=this.options.height,this.size[1]=this.options.width},_resetDimensions:function(){this.size[0]=this.options.width,this.size[1]=this.options.height},_useTitle:function(a){var b=this.qtip.elements.titlebar;return b&&(a.y===K||a.y===O&&this.element.position().top+this.size[1]/2+this.options.offset<b.outerHeight(D))},_parseCorner:function(a){var b=this.qtip.options.position.my;return a===E||b===E?a=E:a===D?a=new A(b.string()):a.string||(a=new A(a),a.fixed=D),a},_parseWidth:function(a,b,c){var d=this.qtip.elements,e=jb+s(b)+"Width";return(c?u(c,e):u(d.content,e)||u(this._useTitle(a)&&d.titlebar||d.content,e)||u(d.tooltip,e))||0},_parseRadius:function(a){var b=this.qtip.elements,c=jb+s(a.y)+s(a.x)+"Radius";return db.ie<9?0:u(this._useTitle(a)&&b.titlebar||b.content,c)||u(b.tooltip,c)||0},_invalidColour:function(a,b,c){var d=a.css(b);return!d||c&&d===a.css(c)||pb.test(d)?E:d},_parseColours:function(a){var b=this.qtip.elements,c=this.element.css("cssText",""),e=jb+s(a[a.precedance])+s(kb),f=this._useTitle(a)&&b.titlebar||b.content,g=this._invalidColour,h=[];return h[0]=g(c,lb)||g(f,lb)||g(b.content,lb)||g(b.tooltip,lb)||c.css(lb),h[1]=g(c,e,kb)||g(f,e,kb)||g(b.content,e,kb)||g(b.tooltip,e,kb)||b.tooltip.css(e),d("*",c).add(c).css("cssText",lb+":"+mb+nb+";"+jb+":0"+nb+";"),h},_calculateSize:function(a){var b,c,d,e=a.precedance===H,f=this.options.width,g=this.options.height,h="c"===a.abbrev(),i=(e?f:g)*(h?.5:1),j=Math.pow,k=Math.round,l=Math.sqrt(j(i,2)+j(g,2)),m=[this.border/i*l,this.border/g*l];return m[2]=Math.sqrt(j(m[0],2)-j(this.border,2)),m[3]=Math.sqrt(j(m[1],2)-j(this.border,2)),b=l+m[2]+m[3]+(h?0:m[0]),c=b/l,d=[k(c*f),k(c*g)],e?d:d.reverse()},_calculateTip:function(a,b,c){c=c||1,b=b||this.size;var d=b[0]*c,e=b[1]*c,f=Math.ceil(d/2),g=Math.ceil(e/2),h={br:[0,0,d,e,d,0],bl:[0,0,d,0,0,e],tr:[0,e,d,0,d,e],tl:[0,0,0,e,d,e],tc:[0,e,f,0,d,e],bc:[0,0,d,0,f,e],rc:[0,0,d,g,0,e],lc:[d,0,d,e,0,g]};return h.lt=h.br,h.rt=h.bl,h.lb=h.tr,h.rb=h.tl,h[a.abbrev()]},_drawCoords:function(a,b){a.beginPath(),a.moveTo(b[0],b[1]),a.lineTo(b[2],b[3]),a.lineTo(b[4],b[5]),a.closePath()},create:function(){var a=this.corner=(ob||db.ie)&&this._parseCorner(this.options.corner);return(this.enabled=!!this.corner&&"c"!==this.corner.abbrev())&&(this.qtip.cache.corner=a.clone(),this.update()),this.element.toggle(this.enabled),this.corner},update:function(b,c){if(!this.enabled)return this;var e,f,g,h,i,j,k,l,m=this.qtip.elements,n=this.element,o=n.children(),p=this.options,q=this.size,r=p.mimic,s=Math.round;b||(b=this.qtip.cache.corner||this.corner),r===E?r=b:(r=new A(r),r.precedance=b.precedance,"inherit"===r.x?r.x=b.x:"inherit"===r.y?r.y=b.y:r.x===r.y&&(r[b.precedance]=b[b.precedance])),f=r.precedance,b.precedance===G?this._swapDimensions():this._resetDimensions(),e=this.color=this._parseColours(b),e[1]!==mb?(l=this.border=this._parseWidth(b,b[b.precedance]),p.border&&1>l&&!pb.test(e[1])&&(e[0]=e[1]),this.border=l=p.border!==D?p.border:l):this.border=l=0,k=this.size=this._calculateSize(b),n.css({width:k[0],height:k[1],lineHeight:k[1]+"px"}),j=b.precedance===H?[s(r.x===L?l:r.x===N?k[0]-q[0]-l:(k[0]-q[0])/2),s(r.y===K?k[1]-q[1]:0)]:[s(r.x===L?k[0]-q[0]:0),s(r.y===K?l:r.y===M?k[1]-q[1]-l:(k[1]-q[1])/2)],ob?(g=o[0].getContext("2d"),g.restore(),g.save(),g.clearRect(0,0,6e3,6e3),h=this._calculateTip(r,q,ub),i=this._calculateTip(r,this.size,ub),o.attr(I,k[0]*ub).attr(J,k[1]*ub),o.css(I,k[0]).css(J,k[1]),this._drawCoords(g,i),g.fillStyle=e[1],g.fill(),g.translate(j[0]*ub,j[1]*ub),this._drawCoords(g,h),g.fillStyle=e[0],g.fill()):(h=this._calculateTip(r),h="m"+h[0]+","+h[1]+" l"+h[2]+","+h[3]+" "+h[4]+","+h[5]+" xe",j[2]=l&&/^(r|b)/i.test(b.string())?8===db.ie?2:1:0,o.css({coordsize:k[0]+l+" "+(k[1]+l),antialias:""+(r.string().indexOf(O)>-1),left:j[0]-j[2]*Number(f===G),top:j[1]-j[2]*Number(f===H),width:k[0]+l,height:k[1]+l}).each(function(a){var b=d(this);b[b.prop?"prop":"attr"]({coordsize:k[0]+l+" "+(k[1]+l),path:h,fillcolor:e[0],filled:!!a,stroked:!a}).toggle(!(!l&&!a)),!a&&b.html(vb("stroke",'weight="'+2*l+'px" color="'+e[1]+'" miterlimit="1000" joinstyle="miter"'))})),a.opera&&setTimeout(function(){m.tip.css({display:"inline-block",visibility:"visible"})},1),c!==E&&this.calculate(b,k)},calculate:function(a,b){if(!this.enabled)return E;var c,e,f=this,g=this.qtip.elements,h=this.element,i=this.options.offset,j=(g.tooltip.hasClass("ui-widget"),{});return a=a||this.corner,c=a.precedance,b=b||this._calculateSize(a),e=[a.x,a.y],c===G&&e.reverse(),d.each(e,function(d,e){var h,k,l;
e===O?(h=c===H?L:K,j[h]="50%",j[ib+"-"+h]=-Math.round(b[c===H?0:1]/2)+i):(h=f._parseWidth(a,e,g.tooltip),k=f._parseWidth(a,e,g.content),l=f._parseRadius(a),j[e]=Math.max(-f.border,d?k:i+(l>h?l:-h)))}),j[a[c]]-=b[c===G?0:1],h.css({margin:"",top:"",bottom:"",left:"",right:""}).css(j),j},reposition:function(a,b,d){function e(a,b,c,d,e){a===Q&&j.precedance===b&&k[d]&&j[c]!==O?j.precedance=j.precedance===G?H:G:a!==Q&&k[d]&&(j[b]=j[b]===O?k[d]>0?d:e:j[b]===d?e:d)}function f(a,b,e){j[a]===O?p[ib+"-"+b]=o[a]=g[ib+"-"+b]-k[b]:(h=g[e]!==c?[k[b],-g[b]]:[-k[b],g[b]],(o[a]=Math.max(h[0],h[1]))>h[0]&&(d[b]-=k[b],o[b]=E),p[g[e]!==c?e:b]=o[a])}if(this.enabled){var g,h,i=b.cache,j=this.corner.clone(),k=d.adjusted,l=b.options.position.adjust.method.split(" "),m=l[0],n=l[1]||l[0],o={left:E,top:E,x:0,y:0},p={};this.corner.fixed!==D&&(e(m,G,H,L,N),e(n,H,G,K,M),(j.string()!==i.corner.string()||i.cornerTop!==k.top||i.cornerLeft!==k.left)&&this.update(j,E)),g=this.calculate(j),g.right!==c&&(g.left=-g.right),g.bottom!==c&&(g.top=-g.bottom),g.user=this.offset,(o.left=m===Q&&!!k.left)&&f(G,L,N),(o.top=n===Q&&!!k.top)&&f(H,K,M),this.element.css(p).toggle(!(o.x&&o.y||j.x===O&&o.y||j.y===O&&o.x)),d.left-=g.left.charAt?g.user:m!==Q||o.top||!o.left&&!o.top?g.left+this.border:0,d.top-=g.top.charAt?g.user:n!==Q||o.left||!o.left&&!o.top?g.top+this.border:0,i.cornerLeft=k.left,i.cornerTop=k.top,i.corner=j.clone()}},destroy:function(){this.qtip._unbind(this.qtip.tooltip,this._ns),this.qtip.elements.tip&&this.qtip.elements.tip.find("*").remove().end().remove()}}),hb=R.tip=function(a){return new v(a,a.options.style.tip)},hb.initialize="render",hb.sanitize=function(a){if(a.style&&"tip"in a.style){var b=a.style.tip;"object"!=typeof b&&(b=a.style.tip={corner:b}),/string|boolean/i.test(typeof b.corner)||(b.corner=D)}},B.tip={"^position.my|style.tip.(corner|mimic|border)$":function(){this.create(),this.qtip.reposition()},"^style.tip.(height|width)$":function(a){this.size=[a.width,a.height],this.update(),this.qtip.reposition()},"^content.title|style.(classes|widget)$":function(){this.update()}},d.extend(D,y.defaults,{style:{tip:{corner:D,mimic:E,width:6,height:6,border:D,offset:0}}});var wb,xb,yb="qtip-modal",zb="."+yb;xb=function(){function a(a){if(d.expr[":"].focusable)return d.expr[":"].focusable;var b,c,e,f=!isNaN(d.attr(a,"tabindex")),g=a.nodeName&&a.nodeName.toLowerCase();return"area"===g?(b=a.parentNode,c=b.name,a.href&&c&&"map"===b.nodeName.toLowerCase()?(e=d("img[usemap=#"+c+"]")[0],!!e&&e.is(":visible")):!1):/input|select|textarea|button|object/.test(g)?!a.disabled:"a"===g?a.href||f:f}function c(a){k.length<1&&a.length?a.not("body").blur():k.first().focus()}function e(a){if(i.is(":visible")){var b,e=d(a.target),h=f.tooltip,j=e.closest(W);b=j.length<1?E:parseInt(j[0].style.zIndex,10)>parseInt(h[0].style.zIndex,10),b||e.closest(W)[0]===h[0]||c(e),g=a.target===k[k.length-1]}}var f,g,h,i,j=this,k={};d.extend(j,{init:function(){return i=j.elem=d("<div />",{id:"qtip-overlay",html:"<div></div>",mousedown:function(){return E}}).hide(),d(b.body).bind("focusin"+zb,e),d(b).bind("keydown"+zb,function(a){f&&f.options.show.modal.escape&&27===a.keyCode&&f.hide(a)}),i.bind("click"+zb,function(a){f&&f.options.show.modal.blur&&f.hide(a)}),j},update:function(b){f=b,k=b.options.show.modal.stealfocus!==E?b.tooltip.find("*").filter(function(){return a(this)}):[]},toggle:function(a,e,g){var k=(d(b.body),a.tooltip),l=a.options.show.modal,m=l.effect,n=e?"show":"hide",o=i.is(":visible"),p=d(zb).filter(":visible:not(:animated)").not(k);return j.update(a),e&&l.stealfocus!==E&&c(d(":focus")),i.toggleClass("blurs",l.blur),e&&i.appendTo(b.body),i.is(":animated")&&o===e&&h!==E||!e&&p.length?j:(i.stop(D,E),d.isFunction(m)?m.call(i,e):m===E?i[n]():i.fadeTo(parseInt(g,10)||90,e?1:0,function(){e||i.hide()}),e||i.queue(function(a){i.css({left:"",top:""}),d(zb).length||i.detach(),a()}),h=e,f.destroyed&&(f=F),j)}}),j.init()},xb=new xb,d.extend(w.prototype,{init:function(a){var b=a.tooltip;return this.options.on?(a.elements.overlay=xb.elem,b.addClass(yb).css("z-index",y.modal_zindex+d(zb).length),a._bind(b,["tooltipshow","tooltiphide"],function(a,c,e){var f=a.originalEvent;if(a.target===b[0])if(f&&"tooltiphide"===a.type&&/mouse(leave|enter)/.test(f.type)&&d(f.relatedTarget).closest(xb.elem[0]).length)try{a.preventDefault()}catch(g){}else(!f||f&&"tooltipsolo"!==f.type)&&this.toggle(a,"tooltipshow"===a.type,e)},this._ns,this),a._bind(b,"tooltipfocus",function(a,c){if(!a.isDefaultPrevented()&&a.target===b[0]){var e=d(zb),f=y.modal_zindex+e.length,g=parseInt(b[0].style.zIndex,10);xb.elem[0].style.zIndex=f-1,e.each(function(){this.style.zIndex>g&&(this.style.zIndex-=1)}),e.filter("."+$).qtip("blur",a.originalEvent),b.addClass($)[0].style.zIndex=f,xb.update(c);try{a.preventDefault()}catch(h){}}},this._ns,this),void a._bind(b,"tooltiphide",function(a){a.target===b[0]&&d(zb).filter(":visible").not(b).last().qtip("focus",a)},this._ns,this)):this},toggle:function(a,b,c){return a&&a.isDefaultPrevented()?this:void xb.toggle(this.qtip,!!b,c)},destroy:function(){this.qtip.tooltip.removeClass(yb),this.qtip._unbind(this.qtip.tooltip,this._ns),xb.toggle(this.qtip,E),delete this.qtip.elements.overlay}}),wb=R.modal=function(a){return new w(a,a.options.show.modal)},wb.sanitize=function(a){a.show&&("object"!=typeof a.show.modal?a.show.modal={on:!!a.show.modal}:"undefined"==typeof a.show.modal.on&&(a.show.modal.on=D))},y.modal_zindex=y.zindex-200,wb.initialize="render",B.modal={"^show.modal.(on|blur)$":function(){this.destroy(),this.init(),this.qtip.elems.overlay.toggle(this.qtip.tooltip[0].offsetWidth>0)}},d.extend(D,y.defaults,{show:{modal:{on:E,effect:D,blur:D,stealfocus:D,escape:D}}}),R.viewport=function(c,d,e,f,g,h,i){function j(a,b,c,e,f,g,h,i,j){var k=d[f],s=u[a],t=v[a],w=c===Q,x=s===f?j:s===g?-j:-j/2,y=t===f?i:t===g?-i:-i/2,z=q[f]+r[f]-(n?0:m[f]),A=z-k,B=k+j-(h===I?o:p)-z,C=x-(u.precedance===a||s===u[b]?y:0)-(t===O?i/2:0);return w?(C=(s===f?1:-1)*x,d[f]+=A>0?A:B>0?-B:0,d[f]=Math.max(-m[f]+r[f],k-C,Math.min(Math.max(-m[f]+r[f]+(h===I?o:p),k+C),d[f],"center"===s?k-x:1e9))):(e*=c===P?2:0,A>0&&(s!==f||B>0)?(d[f]-=C+e,l.invert(a,f)):B>0&&(s!==g||A>0)&&(d[f]-=(s===O?-C:C)+e,l.invert(a,g)),d[f]<q&&-d[f]>B&&(d[f]=k,l=u.clone())),d[f]-k}var k,l,m,n,o,p,q,r,s=e.target,t=c.elements.tooltip,u=e.my,v=e.at,w=e.adjust,x=w.method.split(" "),y=x[0],z=x[1]||x[0],A=e.viewport,B=e.container,C=(c.cache,{left:0,top:0});return A.jquery&&s[0]!==a&&s[0]!==b.body&&"none"!==w.method?(m=B.offset()||C,n="static"===B.css("position"),k="fixed"===t.css("position"),o=A[0]===a?A.width():A.outerWidth(E),p=A[0]===a?A.height():A.outerHeight(E),q={left:k?0:A.scrollLeft(),top:k?0:A.scrollTop()},r=A.offset()||C,("shift"!==y||"shift"!==z)&&(l=u.clone()),C={left:"none"!==y?j(G,H,y,w.x,L,N,I,f,h):0,top:"none"!==z?j(H,G,z,w.y,K,M,J,g,i):0,my:l}):C},R.polys={polygon:function(a,b){var c,d,e,f={width:0,height:0,position:{top:1e10,right:0,bottom:0,left:1e10},adjustable:E},g=0,h=[],i=1,j=1,k=0,l=0;for(g=a.length;g--;)c=[parseInt(a[--g],10),parseInt(a[g+1],10)],c[0]>f.position.right&&(f.position.right=c[0]),c[0]<f.position.left&&(f.position.left=c[0]),c[1]>f.position.bottom&&(f.position.bottom=c[1]),c[1]<f.position.top&&(f.position.top=c[1]),h.push(c);if(d=f.width=Math.abs(f.position.right-f.position.left),e=f.height=Math.abs(f.position.bottom-f.position.top),"c"===b.abbrev())f.position={left:f.position.left+f.width/2,top:f.position.top+f.height/2};else{for(;d>0&&e>0&&i>0&&j>0;)for(d=Math.floor(d/2),e=Math.floor(e/2),b.x===L?i=d:b.x===N?i=f.width-d:i+=Math.floor(d/2),b.y===K?j=e:b.y===M?j=f.height-e:j+=Math.floor(e/2),g=h.length;g--&&!(h.length<2);)k=h[g][0]-f.position.left,l=h[g][1]-f.position.top,(b.x===L&&k>=i||b.x===N&&i>=k||b.x===O&&(i>k||k>f.width-i)||b.y===K&&l>=j||b.y===M&&j>=l||b.y===O&&(j>l||l>f.height-j))&&h.splice(g,1);f.position={left:h[0][0],top:h[0][1]}}return f},rect:function(a,b,c,d){return{width:Math.abs(c-a),height:Math.abs(d-b),position:{left:Math.min(a,c),top:Math.min(b,d)}}},_angles:{tc:1.5,tr:7/4,tl:5/4,bc:.5,br:.25,bl:.75,rc:2,lc:1,c:0},ellipse:function(a,b,c,d,e){var f=R.polys._angles[e.abbrev()],g=0===f?0:c*Math.cos(f*Math.PI),h=d*Math.sin(f*Math.PI);return{width:2*c-Math.abs(g),height:2*d-Math.abs(h),position:{left:a+g,top:b+h},adjustable:E}},circle:function(a,b,c,d){return R.polys.ellipse(a,b,c,c,d)}},R.svg=function(a,c,e){for(var f,g,h,i,j,k,l,m,n,o=(d(b),c[0]),p=d(o.ownerSVGElement),q=o.ownerDocument,r=(parseInt(c.css("stroke-width"),10)||0)/2;!o.getBBox;)o=o.parentNode;if(!o.getBBox||!o.parentNode)return E;switch(o.nodeName){case"ellipse":case"circle":m=R.polys.ellipse(o.cx.baseVal.value,o.cy.baseVal.value,(o.rx||o.r).baseVal.value+r,(o.ry||o.r).baseVal.value+r,e);break;case"line":case"polygon":case"polyline":for(l=o.points||[{x:o.x1.baseVal.value,y:o.y1.baseVal.value},{x:o.x2.baseVal.value,y:o.y2.baseVal.value}],m=[],k=-1,i=l.numberOfItems||l.length;++k<i;)j=l.getItem?l.getItem(k):l[k],m.push.apply(m,[j.x,j.y]);m=R.polys.polygon(m,e);break;default:m=o.getBBox(),m={width:m.width,height:m.height,position:{left:m.x,top:m.y}}}return n=m.position,p=p[0],p.createSVGPoint&&(g=o.getScreenCTM(),l=p.createSVGPoint(),l.x=n.left,l.y=n.top,h=l.matrixTransform(g),n.left=h.x,n.top=h.y),q!==b&&"mouse"!==a.position.target&&(f=d((q.defaultView||q.parentWindow).frameElement).offset(),f&&(n.left+=f.left,n.top+=f.top)),q=d(q),n.left+=q.scrollLeft(),n.top+=q.scrollTop(),m},R.imagemap=function(a,b,c){b.jquery||(b=d(b));var e,f,g,h,i,j=(b.attr("shape")||"rect").toLowerCase().replace("poly","polygon"),k=d('img[usemap="#'+b.parent("map").attr("name")+'"]'),l=d.trim(b.attr("coords")),m=l.replace(/,$/,"").split(",");if(!k.length)return E;if("polygon"===j)h=R.polys.polygon(m,c);else{if(!R.polys[j])return E;for(g=-1,i=m.length,f=[];++g<i;)f.push(parseInt(m[g],10));h=R.polys[j].apply(this,f.concat(c))}return e=k.offset(),e.left+=Math.ceil((k.outerWidth(E)-k.width())/2),e.top+=Math.ceil((k.outerHeight(E)-k.height())/2),h.position.left+=e.left,h.position.top+=e.top,h};var Ab,Bb='<iframe class="qtip-bgiframe" frameborder="0" tabindex="-1" src="javascript:\'\';"  style="display:block; position:absolute; z-index:-1; filter:alpha(opacity=0); -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";"></iframe>';d.extend(x.prototype,{_scroll:function(){var b=this.qtip.elements.overlay;b&&(b[0].style.top=d(a).scrollTop()+"px")},init:function(c){var e=c.tooltip;d("select, object").length<1&&(this.bgiframe=c.elements.bgiframe=d(Bb).appendTo(e),c._bind(e,"tooltipmove",this.adjustBGIFrame,this._ns,this)),this.redrawContainer=d("<div/>",{id:S+"-rcontainer"}).appendTo(b.body),c.elements.overlay&&c.elements.overlay.addClass("qtipmodal-ie6fix")&&(c._bind(a,["scroll","resize"],this._scroll,this._ns,this),c._bind(e,["tooltipshow"],this._scroll,this._ns,this)),this.redraw()},adjustBGIFrame:function(){var a,b,c=this.qtip.tooltip,d={height:c.outerHeight(E),width:c.outerWidth(E)},e=this.qtip.plugins.tip,f=this.qtip.elements.tip;b=parseInt(c.css("borderLeftWidth"),10)||0,b={left:-b,top:-b},e&&f&&(a="x"===e.corner.precedance?[I,L]:[J,K],b[a[1]]-=f[a[0]]()),this.bgiframe.css(b).css(d)},redraw:function(){if(this.qtip.rendered<1||this.drawing)return this;var a,b,c,d,e=this.qtip.tooltip,f=this.qtip.options.style,g=this.qtip.options.position.container;return this.qtip.drawing=1,f.height&&e.css(J,f.height),f.width?e.css(I,f.width):(e.css(I,"").appendTo(this.redrawContainer),b=e.width(),1>b%2&&(b+=1),c=e.css("maxWidth")||"",d=e.css("minWidth")||"",a=(c+d).indexOf("%")>-1?g.width()/100:0,c=(c.indexOf("%")>-1?a:1)*parseInt(c,10)||b,d=(d.indexOf("%")>-1?a:1)*parseInt(d,10)||0,b=c+d?Math.min(Math.max(b,d),c):b,e.css(I,Math.round(b)).appendTo(g)),this.drawing=0,this},destroy:function(){this.bgiframe&&this.bgiframe.remove(),this.qtip._unbind([a,this.qtip.tooltip],this._ns)}}),Ab=R.ie6=function(a){return 6===db.ie?new x(a):E},Ab.initialize="render",B.ie6={"^content|style$":function(){this.redraw()}}})}(window,document);
//# sourceMappingURL=jquery.qtip.min.js.map
/*!
 * imagesLoaded PACKAGED v3.1.8
 * JavaScript is all like "You images are done yet or what?"
 * MIT License
 */

(function(){function e(){}function t(e,t){for(var n=e.length;n--;)if(e[n].listener===t)return n;return-1}function n(e){return function(){return this[e].apply(this,arguments)}}var i=e.prototype,r=this,o=r.EventEmitter;i.getListeners=function(e){var t,n,i=this._getEvents();if("object"==typeof e){t={};for(n in i)i.hasOwnProperty(n)&&e.test(n)&&(t[n]=i[n])}else t=i[e]||(i[e]=[]);return t},i.flattenListeners=function(e){var t,n=[];for(t=0;e.length>t;t+=1)n.push(e[t].listener);return n},i.getListenersAsObject=function(e){var t,n=this.getListeners(e);return n instanceof Array&&(t={},t[e]=n),t||n},i.addListener=function(e,n){var i,r=this.getListenersAsObject(e),o="object"==typeof n;for(i in r)r.hasOwnProperty(i)&&-1===t(r[i],n)&&r[i].push(o?n:{listener:n,once:!1});return this},i.on=n("addListener"),i.addOnceListener=function(e,t){return this.addListener(e,{listener:t,once:!0})},i.once=n("addOnceListener"),i.defineEvent=function(e){return this.getListeners(e),this},i.defineEvents=function(e){for(var t=0;e.length>t;t+=1)this.defineEvent(e[t]);return this},i.removeListener=function(e,n){var i,r,o=this.getListenersAsObject(e);for(r in o)o.hasOwnProperty(r)&&(i=t(o[r],n),-1!==i&&o[r].splice(i,1));return this},i.off=n("removeListener"),i.addListeners=function(e,t){return this.manipulateListeners(!1,e,t)},i.removeListeners=function(e,t){return this.manipulateListeners(!0,e,t)},i.manipulateListeners=function(e,t,n){var i,r,o=e?this.removeListener:this.addListener,s=e?this.removeListeners:this.addListeners;if("object"!=typeof t||t instanceof RegExp)for(i=n.length;i--;)o.call(this,t,n[i]);else for(i in t)t.hasOwnProperty(i)&&(r=t[i])&&("function"==typeof r?o.call(this,i,r):s.call(this,i,r));return this},i.removeEvent=function(e){var t,n=typeof e,i=this._getEvents();if("string"===n)delete i[e];else if("object"===n)for(t in i)i.hasOwnProperty(t)&&e.test(t)&&delete i[t];else delete this._events;return this},i.removeAllListeners=n("removeEvent"),i.emitEvent=function(e,t){var n,i,r,o,s=this.getListenersAsObject(e);for(r in s)if(s.hasOwnProperty(r))for(i=s[r].length;i--;)n=s[r][i],n.once===!0&&this.removeListener(e,n.listener),o=n.listener.apply(this,t||[]),o===this._getOnceReturnValue()&&this.removeListener(e,n.listener);return this},i.trigger=n("emitEvent"),i.emit=function(e){var t=Array.prototype.slice.call(arguments,1);return this.emitEvent(e,t)},i.setOnceReturnValue=function(e){return this._onceReturnValue=e,this},i._getOnceReturnValue=function(){return this.hasOwnProperty("_onceReturnValue")?this._onceReturnValue:!0},i._getEvents=function(){return this._events||(this._events={})},e.noConflict=function(){return r.EventEmitter=o,e},"function"==typeof define&&define.amd?define("eventEmitter/EventEmitter",[],function(){return e}):"object"==typeof module&&module.exports?module.exports=e:this.EventEmitter=e}).call(this),function(e){function t(t){var n=e.event;return n.target=n.target||n.srcElement||t,n}var n=document.documentElement,i=function(){};n.addEventListener?i=function(e,t,n){e.addEventListener(t,n,!1)}:n.attachEvent&&(i=function(e,n,i){e[n+i]=i.handleEvent?function(){var n=t(e);i.handleEvent.call(i,n)}:function(){var n=t(e);i.call(e,n)},e.attachEvent("on"+n,e[n+i])});var r=function(){};n.removeEventListener?r=function(e,t,n){e.removeEventListener(t,n,!1)}:n.detachEvent&&(r=function(e,t,n){e.detachEvent("on"+t,e[t+n]);try{delete e[t+n]}catch(i){e[t+n]=void 0}});var o={bind:i,unbind:r};"function"==typeof define&&define.amd?define("eventie/eventie",o):e.eventie=o}(this),function(e,t){"function"==typeof define&&define.amd?define(["eventEmitter/EventEmitter","eventie/eventie"],function(n,i){return t(e,n,i)}):"object"==typeof exports?module.exports=t(e,require("wolfy87-eventemitter"),require("eventie")):e.imagesLoaded=t(e,e.EventEmitter,e.eventie)}(window,function(e,t,n){function i(e,t){for(var n in t)e[n]=t[n];return e}function r(e){return"[object Array]"===d.call(e)}function o(e){var t=[];if(r(e))t=e;else if("number"==typeof e.length)for(var n=0,i=e.length;i>n;n++)t.push(e[n]);else t.push(e);return t}function s(e,t,n){if(!(this instanceof s))return new s(e,t);"string"==typeof e&&(e=document.querySelectorAll(e)),this.elements=o(e),this.options=i({},this.options),"function"==typeof t?n=t:i(this.options,t),n&&this.on("always",n),this.getImages(),a&&(this.jqDeferred=new a.Deferred);var r=this;setTimeout(function(){r.check()})}function f(e){this.img=e}function c(e){this.src=e,v[e]=this}var a=e.jQuery,u=e.console,h=u!==void 0,d=Object.prototype.toString;s.prototype=new t,s.prototype.options={},s.prototype.getImages=function(){this.images=[];for(var e=0,t=this.elements.length;t>e;e++){var n=this.elements[e];"IMG"===n.nodeName&&this.addImage(n);var i=n.nodeType;if(i&&(1===i||9===i||11===i))for(var r=n.querySelectorAll("img"),o=0,s=r.length;s>o;o++){var f=r[o];this.addImage(f)}}},s.prototype.addImage=function(e){var t=new f(e);this.images.push(t)},s.prototype.check=function(){function e(e,r){return t.options.debug&&h&&u.log("confirm",e,r),t.progress(e),n++,n===i&&t.complete(),!0}var t=this,n=0,i=this.images.length;if(this.hasAnyBroken=!1,!i)return this.complete(),void 0;for(var r=0;i>r;r++){var o=this.images[r];o.on("confirm",e),o.check()}},s.prototype.progress=function(e){this.hasAnyBroken=this.hasAnyBroken||!e.isLoaded;var t=this;setTimeout(function(){t.emit("progress",t,e),t.jqDeferred&&t.jqDeferred.notify&&t.jqDeferred.notify(t,e)})},s.prototype.complete=function(){var e=this.hasAnyBroken?"fail":"done";this.isComplete=!0;var t=this;setTimeout(function(){if(t.emit(e,t),t.emit("always",t),t.jqDeferred){var n=t.hasAnyBroken?"reject":"resolve";t.jqDeferred[n](t)}})},a&&(a.fn.imagesLoaded=function(e,t){var n=new s(this,e,t);return n.jqDeferred.promise(a(this))}),f.prototype=new t,f.prototype.check=function(){var e=v[this.img.src]||new c(this.img.src);if(e.isConfirmed)return this.confirm(e.isLoaded,"cached was confirmed"),void 0;if(this.img.complete&&void 0!==this.img.naturalWidth)return this.confirm(0!==this.img.naturalWidth,"naturalWidth"),void 0;var t=this;e.on("confirm",function(e,n){return t.confirm(e.isLoaded,n),!0}),e.check()},f.prototype.confirm=function(e,t){this.isLoaded=e,this.emit("confirm",this,t)};var v={};return c.prototype=new t,c.prototype.check=function(){if(!this.isChecked){var e=new Image;n.bind(e,"load",this),n.bind(e,"error",this),e.src=this.src,this.isChecked=!0}},c.prototype.handleEvent=function(e){var t="on"+e.type;this[t]&&this[t](e)},c.prototype.onload=function(e){this.confirm(!0,"onload"),this.unbindProxyEvents(e)},c.prototype.onerror=function(e){this.confirm(!1,"onerror"),this.unbindProxyEvents(e)},c.prototype.confirm=function(e,t){this.isConfirmed=!0,this.isLoaded=e,this.emit("confirm",this,t)},c.prototype.unbindProxyEvents=function(e){n.unbind(e.target,"load",this),n.unbind(e.target,"error",this)},s});
/*!
 * clipboard.js v2.0.0
 * https://zenorocha.github.io/clipboard.js
 * 
 * Licensed MIT © Zeno Rocha
 */
!function(t,e){"object"==typeof exports&&"object"==typeof module?module.exports=e():"function"==typeof define&&define.amd?define([],e):"object"==typeof exports?exports.ClipboardJS=e():t.ClipboardJS=e()}(this,function(){return function(t){function e(o){if(n[o])return n[o].exports;var r=n[o]={i:o,l:!1,exports:{}};return t[o].call(r.exports,r,r.exports,e),r.l=!0,r.exports}var n={};return e.m=t,e.c=n,e.i=function(t){return t},e.d=function(t,n,o){e.o(t,n)||Object.defineProperty(t,n,{configurable:!1,enumerable:!0,get:o})},e.n=function(t){var n=t&&t.__esModule?function(){return t.default}:function(){return t};return e.d(n,"a",n),n},e.o=function(t,e){return Object.prototype.hasOwnProperty.call(t,e)},e.p="",e(e.s=3)}([function(t,e,n){var o,r,i;!function(a,c){r=[t,n(7)],o=c,void 0!==(i="function"==typeof o?o.apply(e,r):o)&&(t.exports=i)}(0,function(t,e){"use strict";function n(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}var o=function(t){return t&&t.__esModule?t:{default:t}}(e),r="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(t){return typeof t}:function(t){return t&&"function"==typeof Symbol&&t.constructor===Symbol&&t!==Symbol.prototype?"symbol":typeof t},i=function(){function t(t,e){for(var n=0;n<e.length;n++){var o=e[n];o.enumerable=o.enumerable||!1,o.configurable=!0,"value"in o&&(o.writable=!0),Object.defineProperty(t,o.key,o)}}return function(e,n,o){return n&&t(e.prototype,n),o&&t(e,o),e}}(),a=function(){function t(e){n(this,t),this.resolveOptions(e),this.initSelection()}return i(t,[{key:"resolveOptions",value:function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:{};this.action=t.action,this.container=t.container,this.emitter=t.emitter,this.target=t.target,this.text=t.text,this.trigger=t.trigger,this.selectedText=""}},{key:"initSelection",value:function(){this.text?this.selectFake():this.target&&this.selectTarget()}},{key:"selectFake",value:function(){var t=this,e="rtl"==document.documentElement.getAttribute("dir");this.removeFake(),this.fakeHandlerCallback=function(){return t.removeFake()},this.fakeHandler=this.container.addEventListener("click",this.fakeHandlerCallback)||!0,this.fakeElem=document.createElement("textarea"),this.fakeElem.style.fontSize="12pt",this.fakeElem.style.border="0",this.fakeElem.style.padding="0",this.fakeElem.style.margin="0",this.fakeElem.style.position="absolute",this.fakeElem.style[e?"right":"left"]="-9999px";var n=window.pageYOffset||document.documentElement.scrollTop;this.fakeElem.style.top=n+"px",this.fakeElem.setAttribute("readonly",""),this.fakeElem.value=this.text,this.container.appendChild(this.fakeElem),this.selectedText=(0,o.default)(this.fakeElem),this.copyText()}},{key:"removeFake",value:function(){this.fakeHandler&&(this.container.removeEventListener("click",this.fakeHandlerCallback),this.fakeHandler=null,this.fakeHandlerCallback=null),this.fakeElem&&(this.container.removeChild(this.fakeElem),this.fakeElem=null)}},{key:"selectTarget",value:function(){this.selectedText=(0,o.default)(this.target),this.copyText()}},{key:"copyText",value:function(){var t=void 0;try{t=document.execCommand(this.action)}catch(e){t=!1}this.handleResult(t)}},{key:"handleResult",value:function(t){this.emitter.emit(t?"success":"error",{action:this.action,text:this.selectedText,trigger:this.trigger,clearSelection:this.clearSelection.bind(this)})}},{key:"clearSelection",value:function(){this.trigger&&this.trigger.focus(),window.getSelection().removeAllRanges()}},{key:"destroy",value:function(){this.removeFake()}},{key:"action",set:function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:"copy";if(this._action=t,"copy"!==this._action&&"cut"!==this._action)throw new Error('Invalid "action" value, use either "copy" or "cut"')},get:function(){return this._action}},{key:"target",set:function(t){if(void 0!==t){if(!t||"object"!==(void 0===t?"undefined":r(t))||1!==t.nodeType)throw new Error('Invalid "target" value, use a valid Element');if("copy"===this.action&&t.hasAttribute("disabled"))throw new Error('Invalid "target" attribute. Please use "readonly" instead of "disabled" attribute');if("cut"===this.action&&(t.hasAttribute("readonly")||t.hasAttribute("disabled")))throw new Error('Invalid "target" attribute. You can\'t cut text from elements with "readonly" or "disabled" attributes');this._target=t}},get:function(){return this._target}}]),t}();t.exports=a})},function(t,e,n){function o(t,e,n){if(!t&&!e&&!n)throw new Error("Missing required arguments");if(!c.string(e))throw new TypeError("Second argument must be a String");if(!c.fn(n))throw new TypeError("Third argument must be a Function");if(c.node(t))return r(t,e,n);if(c.nodeList(t))return i(t,e,n);if(c.string(t))return a(t,e,n);throw new TypeError("First argument must be a String, HTMLElement, HTMLCollection, or NodeList")}function r(t,e,n){return t.addEventListener(e,n),{destroy:function(){t.removeEventListener(e,n)}}}function i(t,e,n){return Array.prototype.forEach.call(t,function(t){t.addEventListener(e,n)}),{destroy:function(){Array.prototype.forEach.call(t,function(t){t.removeEventListener(e,n)})}}}function a(t,e,n){return u(document.body,t,e,n)}var c=n(6),u=n(5);t.exports=o},function(t,e){function n(){}n.prototype={on:function(t,e,n){var o=this.e||(this.e={});return(o[t]||(o[t]=[])).push({fn:e,ctx:n}),this},once:function(t,e,n){function o(){r.off(t,o),e.apply(n,arguments)}var r=this;return o._=e,this.on(t,o,n)},emit:function(t){var e=[].slice.call(arguments,1),n=((this.e||(this.e={}))[t]||[]).slice(),o=0,r=n.length;for(o;o<r;o++)n[o].fn.apply(n[o].ctx,e);return this},off:function(t,e){var n=this.e||(this.e={}),o=n[t],r=[];if(o&&e)for(var i=0,a=o.length;i<a;i++)o[i].fn!==e&&o[i].fn._!==e&&r.push(o[i]);return r.length?n[t]=r:delete n[t],this}},t.exports=n},function(t,e,n){var o,r,i;!function(a,c){r=[t,n(0),n(2),n(1)],o=c,void 0!==(i="function"==typeof o?o.apply(e,r):o)&&(t.exports=i)}(0,function(t,e,n,o){"use strict";function r(t){return t&&t.__esModule?t:{default:t}}function i(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}function a(t,e){if(!t)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!e||"object"!=typeof e&&"function"!=typeof e?t:e}function c(t,e){if("function"!=typeof e&&null!==e)throw new TypeError("Super expression must either be null or a function, not "+typeof e);t.prototype=Object.create(e&&e.prototype,{constructor:{value:t,enumerable:!1,writable:!0,configurable:!0}}),e&&(Object.setPrototypeOf?Object.setPrototypeOf(t,e):t.__proto__=e)}function u(t,e){var n="data-clipboard-"+t;if(e.hasAttribute(n))return e.getAttribute(n)}var l=r(e),s=r(n),f=r(o),d="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(t){return typeof t}:function(t){return t&&"function"==typeof Symbol&&t.constructor===Symbol&&t!==Symbol.prototype?"symbol":typeof t},h=function(){function t(t,e){for(var n=0;n<e.length;n++){var o=e[n];o.enumerable=o.enumerable||!1,o.configurable=!0,"value"in o&&(o.writable=!0),Object.defineProperty(t,o.key,o)}}return function(e,n,o){return n&&t(e.prototype,n),o&&t(e,o),e}}(),p=function(t){function e(t,n){i(this,e);var o=a(this,(e.__proto__||Object.getPrototypeOf(e)).call(this));return o.resolveOptions(n),o.listenClick(t),o}return c(e,t),h(e,[{key:"resolveOptions",value:function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:{};this.action="function"==typeof t.action?t.action:this.defaultAction,this.target="function"==typeof t.target?t.target:this.defaultTarget,this.text="function"==typeof t.text?t.text:this.defaultText,this.container="object"===d(t.container)?t.container:document.body}},{key:"listenClick",value:function(t){var e=this;this.listener=(0,f.default)(t,"click",function(t){return e.onClick(t)})}},{key:"onClick",value:function(t){var e=t.delegateTarget||t.currentTarget;this.clipboardAction&&(this.clipboardAction=null),this.clipboardAction=new l.default({action:this.action(e),target:this.target(e),text:this.text(e),container:this.container,trigger:e,emitter:this})}},{key:"defaultAction",value:function(t){return u("action",t)}},{key:"defaultTarget",value:function(t){var e=u("target",t);if(e)return document.querySelector(e)}},{key:"defaultText",value:function(t){return u("text",t)}},{key:"destroy",value:function(){this.listener.destroy(),this.clipboardAction&&(this.clipboardAction.destroy(),this.clipboardAction=null)}}],[{key:"isSupported",value:function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:["copy","cut"],e="string"==typeof t?[t]:t,n=!!document.queryCommandSupported;return e.forEach(function(t){n=n&&!!document.queryCommandSupported(t)}),n}}]),e}(s.default);t.exports=p})},function(t,e){function n(t,e){for(;t&&t.nodeType!==o;){if("function"==typeof t.matches&&t.matches(e))return t;t=t.parentNode}}var o=9;if("undefined"!=typeof Element&&!Element.prototype.matches){var r=Element.prototype;r.matches=r.matchesSelector||r.mozMatchesSelector||r.msMatchesSelector||r.oMatchesSelector||r.webkitMatchesSelector}t.exports=n},function(t,e,n){function o(t,e,n,o,r){var a=i.apply(this,arguments);return t.addEventListener(n,a,r),{destroy:function(){t.removeEventListener(n,a,r)}}}function r(t,e,n,r,i){return"function"==typeof t.addEventListener?o.apply(null,arguments):"function"==typeof n?o.bind(null,document).apply(null,arguments):("string"==typeof t&&(t=document.querySelectorAll(t)),Array.prototype.map.call(t,function(t){return o(t,e,n,r,i)}))}function i(t,e,n,o){return function(n){n.delegateTarget=a(n.target,e),n.delegateTarget&&o.call(t,n)}}var a=n(4);t.exports=r},function(t,e){e.node=function(t){return void 0!==t&&t instanceof HTMLElement&&1===t.nodeType},e.nodeList=function(t){var n=Object.prototype.toString.call(t);return void 0!==t&&("[object NodeList]"===n||"[object HTMLCollection]"===n)&&"length"in t&&(0===t.length||e.node(t[0]))},e.string=function(t){return"string"==typeof t||t instanceof String},e.fn=function(t){return"[object Function]"===Object.prototype.toString.call(t)}},function(t,e){function n(t){var e;if("SELECT"===t.nodeName)t.focus(),e=t.value;else if("INPUT"===t.nodeName||"TEXTAREA"===t.nodeName){var n=t.hasAttribute("readonly");n||t.setAttribute("readonly",""),t.select(),t.setSelectionRange(0,t.value.length),n||t.removeAttribute("readonly"),e=t.value}else{t.hasAttribute("contenteditable")&&t.focus();var o=window.getSelection(),r=document.createRange();r.selectNodeContents(t),o.removeAllRanges(),o.addRange(r),e=o.toString()}return e}t.exports=n}])});
/* WebUploader 0.1.7-alpha */!function(a,b){var c,d={},e=function(a,b){var c,d,e;if("string"==typeof a)return h(a);for(c=[],d=a.length,e=0;d>e;e++)c.push(h(a[e]));return b.apply(null,c)},f=function(a,b,c){2===arguments.length&&(c=b,b=null),e(b||[],function(){g(a,c,arguments)})},g=function(a,b,c){var f,g={exports:b};"function"==typeof b&&(c.length||(c=[e,g.exports,g]),f=b.apply(null,c),void 0!==f&&(g.exports=f)),d[a]=g.exports},h=function(b){var c=d[b]||a[b];if(!c)throw new Error("`"+b+"` is undefined");return c},i=function(a){var b,c,e,f,g,h;h=function(a){return a&&a.charAt(0).toUpperCase()+a.substr(1)};for(b in d)if(c=a,d.hasOwnProperty(b)){for(e=b.split("/"),g=h(e.pop());f=h(e.shift());)c[f]=c[f]||{},c=c[f];c[g]=d[b]}return a},j=function(c){return a.__dollar=c,i(b(a,f,e))};"object"==typeof module&&"object"==typeof module.exports?module.exports=j():"function"==typeof define&&define.amd?define(["jquery"],j):(c=a.WebUploader,a.WebUploader=j(),a.WebUploader.noConflict=function(){a.WebUploader=c})}(window,function(a,b,c){return b("dollar-third",[],function(){var b=a.require,c=a.__dollar||a.jQuery||a.Zepto||b("jquery")||b("zepto");if(!c)throw new Error("jQuery or Zepto not found!");return c}),b("dollar",["dollar-third"],function(a){return a}),b("promise-third",["dollar"],function(a){return{Deferred:a.Deferred,when:a.when,isPromise:function(a){return a&&"function"==typeof a.then}}}),b("promise",["promise-third"],function(a){return a}),b("base",["dollar","promise"],function(b,c){function d(a){return function(){return h.apply(a,arguments)}}function e(a,b){return function(){return a.apply(b,arguments)}}function f(a){var b;return Object.create?Object.create(a):(b=function(){},b.prototype=a,new b)}var g=function(){},h=Function.call;return{version:"0.1.7-alpha",$:b,Deferred:c.Deferred,isPromise:c.isPromise,when:c.when,browser:function(a){var b={},c=a.match(/WebKit\/([\d.]+)/),d=a.match(/Chrome\/([\d.]+)/)||a.match(/CriOS\/([\d.]+)/),e=a.match(/MSIE\s([\d\.]+)/)||a.match(/(?:trident)(?:.*rv:([\w.]+))?/i),f=a.match(/Firefox\/([\d.]+)/),g=a.match(/Safari\/([\d.]+)/),h=a.match(/OPR\/([\d.]+)/);return c&&(b.webkit=parseFloat(c[1])),d&&(b.chrome=parseFloat(d[1])),e&&(b.ie=parseFloat(e[1])),f&&(b.firefox=parseFloat(f[1])),g&&(b.safari=parseFloat(g[1])),h&&(b.opera=parseFloat(h[1])),b}(navigator.userAgent),os:function(a){var b={},c=a.match(/(?:Android);?[\s\/]+([\d.]+)?/),d=a.match(/(?:iPad|iPod|iPhone).*OS\s([\d_]+)/);return c&&(b.android=parseFloat(c[1])),d&&(b.ios=parseFloat(d[1].replace(/_/g,"."))),b}(navigator.userAgent),inherits:function(a,c,d){var e;return"function"==typeof c?(e=c,c=null):e=c&&c.hasOwnProperty("constructor")?c.constructor:function(){return a.apply(this,arguments)},b.extend(!0,e,a,d||{}),e.__super__=a.prototype,e.prototype=f(a.prototype),c&&b.extend(!0,e.prototype,c),e},noop:g,bindFn:e,log:function(){return a.console?e(console.log,console):g}(),nextTick:function(){return function(a){setTimeout(a,1)}}(),slice:d([].slice),guid:function(){var a=0;return function(b){for(var c=(+new Date).toString(32),d=0;5>d;d++)c+=Math.floor(65535*Math.random()).toString(32);return(b||"wu_")+c+(a++).toString(32)}}(),formatSize:function(a,b,c){var d;for(c=c||["B","K","M","G","TB"];(d=c.shift())&&a>1024;)a/=1024;return("B"===d?a:a.toFixed(b||2))+d}}}),b("mediator",["base"],function(a){function b(a,b,c,d){return f.grep(a,function(a){return!(!a||b&&a.e!==b||c&&a.cb!==c&&a.cb._cb!==c||d&&a.ctx!==d)})}function c(a,b,c){f.each((a||"").split(h),function(a,d){c(d,b)})}function d(a,b){for(var c,d=!1,e=-1,f=a.length;++e<f;)if(c=a[e],c.cb.apply(c.ctx2,b)===!1){d=!0;break}return!d}var e,f=a.$,g=[].slice,h=/\s+/;return e={on:function(a,b,d){var e,f=this;return b?(e=this._events||(this._events=[]),c(a,b,function(a,b){var c={e:a};c.cb=b,c.ctx=d,c.ctx2=d||f,c.id=e.length,e.push(c)}),this):this},once:function(a,b,d){var e=this;return b?(c(a,b,function(a,b){var c=function(){return e.off(a,c),b.apply(d||e,arguments)};c._cb=b,e.on(a,c,d)}),e):e},off:function(a,d,e){var g=this._events;return g?a||d||e?(c(a,d,function(a,c){f.each(b(g,a,c,e),function(){delete g[this.id]})}),this):(this._events=[],this):this},trigger:function(a){var c,e,f;return this._events&&a?(c=g.call(arguments,1),e=b(this._events,a),f=b(this._events,"all"),d(e,c)&&d(f,arguments)):this}},f.extend({installTo:function(a){return f.extend(a,e)}},e)}),b("uploader",["base","mediator"],function(a,b){function c(a){this.options=d.extend(!0,{},c.options,a),this._init(this.options)}var d=a.$;return c.options={},b.installTo(c.prototype),d.each({upload:"start-upload",stop:"stop-upload",getFile:"get-file",getFiles:"get-files",addFile:"add-file",addFiles:"add-file",sort:"sort-files",removeFile:"remove-file",cancelFile:"cancel-file",skipFile:"skip-file",retry:"retry",isInProgress:"is-in-progress",makeThumb:"make-thumb",md5File:"md5-file",getDimension:"get-dimension",addButton:"add-btn",predictRuntimeType:"predict-runtime-type",refresh:"refresh",disable:"disable",enable:"enable",reset:"reset"},function(a,b){c.prototype[a]=function(){return this.request(b,arguments)}}),d.extend(c.prototype,{state:"pending",_init:function(a){var b=this;b.request("init",a,function(){b.state="ready",b.trigger("ready")})},option:function(a,b){var c=this.options;return arguments.length>1?(d.isPlainObject(b)&&d.isPlainObject(c[a])?d.extend(c[a],b):c[a]=b,void 0):a?c[a]:c},getStats:function(){var a=this.request("get-stats");return a?{successNum:a.numOfSuccess,progressNum:a.numOfProgress,cancelNum:a.numOfCancel,invalidNum:a.numOfInvalid,uploadFailNum:a.numOfUploadFailed,queueNum:a.numOfQueue,interruptNum:a.numofInterrupt}:{}},trigger:function(a){var c=[].slice.call(arguments,1),e=this.options,f="on"+a.substring(0,1).toUpperCase()+a.substring(1);return b.trigger.apply(this,arguments)===!1||d.isFunction(e[f])&&e[f].apply(this,c)===!1||d.isFunction(this[f])&&this[f].apply(this,c)===!1||b.trigger.apply(b,[this,a].concat(c))===!1?!1:!0},destroy:function(){this.request("destroy",arguments),this.off()},request:a.noop}),a.create=c.create=function(a){return new c(a)},a.Uploader=c,c}),b("runtime/runtime",["base","mediator"],function(a,b){function c(b){this.options=d.extend({container:document.body},b),this.uid=a.guid("rt_")}var d=a.$,e={},f=function(a){for(var b in a)if(a.hasOwnProperty(b))return b;return null};return d.extend(c.prototype,{getContainer:function(){var a,b,c=this.options;return this._container?this._container:(a=d(c.container||document.body),b=d(document.createElement("div")),b.attr("id","rt_"+this.uid),b.css({position:"absolute",top:"0px",left:"0px",width:"1px",height:"1px",overflow:"hidden"}),a.append(b),a.addClass("webuploader-container"),this._container=b,this._parent=a,b)},init:a.noop,exec:a.noop,destroy:function(){this._container&&this._container.remove(),this._parent&&this._parent.removeClass("webuploader-container"),this.off()}}),c.orders="html5,flash",c.addRuntime=function(a,b){e[a]=b},c.hasRuntime=function(a){return!!(a?e[a]:f(e))},c.create=function(a,b){var g,h;if(b=b||c.orders,d.each(b.split(/\s*,\s*/g),function(){return e[this]?(g=this,!1):void 0}),g=g||f(e),!g)throw new Error("Runtime Error");return h=new e[g](a)},b.installTo(c.prototype),c}),b("runtime/client",["base","mediator","runtime/runtime"],function(a,b,c){function d(b,d){var f,g=a.Deferred();this.uid=a.guid("client_"),this.runtimeReady=function(a){return g.done(a)},this.connectRuntime=function(b,h){if(f)throw new Error("already connected!");return g.done(h),"string"==typeof b&&e.get(b)&&(f=e.get(b)),f=f||e.get(null,d),f?(a.$.extend(f.options,b),f.__promise.then(g.resolve),f.__client++):(f=c.create(b,b.runtimeOrder),f.__promise=g.promise(),f.once("ready",g.resolve),f.init(),e.add(f),f.__client=1),d&&(f.__standalone=d),f},this.getRuntime=function(){return f},this.disconnectRuntime=function(){f&&(f.__client--,f.__client<=0&&(e.remove(f),delete f.__promise,f.destroy()),f=null)},this.exec=function(){if(f){var c=a.slice(arguments);return b&&c.unshift(b),f.exec.apply(this,c)}},this.getRuid=function(){return f&&f.uid},this.destroy=function(a){return function(){a&&a.apply(this,arguments),this.trigger("destroy"),this.off(),this.exec("destroy"),this.disconnectRuntime()}}(this.destroy)}var e;return e=function(){var a={};return{add:function(b){a[b.uid]=b},get:function(b,c){var d;if(b)return a[b];for(d in a)if(!c||!a[d].__standalone)return a[d];return null},remove:function(b){delete a[b.uid]}}}(),b.installTo(d.prototype),d}),b("lib/dnd",["base","mediator","runtime/client"],function(a,b,c){function d(a){a=this.options=e.extend({},d.options,a),a.container=e(a.container),a.container.length&&c.call(this,"DragAndDrop")}var e=a.$;return d.options={accept:null,disableGlobalDnd:!1},a.inherits(c,{constructor:d,init:function(){var a=this;a.connectRuntime(a.options,function(){a.exec("init"),a.trigger("ready")})}}),b.installTo(d.prototype),d}),b("widgets/widget",["base","uploader"],function(a,b){function c(a){if(!a)return!1;var b=a.length,c=e.type(a);return 1===a.nodeType&&b?!0:"array"===c||"function"!==c&&"string"!==c&&(0===b||"number"==typeof b&&b>0&&b-1 in a)}function d(a){this.owner=a,this.options=a.options}var e=a.$,f=b.prototype._init,g=b.prototype.destroy,h={},i=[];return e.extend(d.prototype,{init:a.noop,invoke:function(a,b){var c=this.responseMap;return c&&a in c&&c[a]in this&&e.isFunction(this[c[a]])?this[c[a]].apply(this,b):h},request:function(){return this.owner.request.apply(this.owner,arguments)}}),e.extend(b.prototype,{_init:function(){var a=this,b=a._widgets=[],c=a.options.disableWidgets||"";return e.each(i,function(d,e){(!c||!~c.indexOf(e._name))&&b.push(new e(a))}),f.apply(a,arguments)},request:function(b,d,e){var f,g,i,j,k=0,l=this._widgets,m=l&&l.length,n=[],o=[];for(d=c(d)?d:[d];m>k;k++)f=l[k],g=f.invoke(b,d),g!==h&&(a.isPromise(g)?o.push(g):n.push(g));return e||o.length?(i=a.when.apply(a,o),j=i.pipe?"pipe":"then",i[j](function(){var b=a.Deferred(),c=arguments;return 1===c.length&&(c=c[0]),setTimeout(function(){b.resolve(c)},1),b.promise()})[e?j:"done"](e||a.noop)):n[0]},destroy:function(){g.apply(this,arguments),this._widgets=null}}),b.register=d.register=function(b,c){var f,g={init:"init",destroy:"destroy",name:"anonymous"};return 1===arguments.length?(c=b,e.each(c,function(a){return"_"===a[0]||"name"===a?("name"===a&&(g.name=c.name),void 0):(g[a.replace(/[A-Z]/g,"-$&").toLowerCase()]=a,void 0)})):g=e.extend(g,b),c.responseMap=g,f=a.inherits(d,c),f._name=g.name,i.push(f),f},b.unRegister=d.unRegister=function(a){if(a&&"anonymous"!==a)for(var b=i.length;b--;)i[b]._name===a&&i.splice(b,1)},d}),b("widgets/filednd",["base","uploader","lib/dnd","widgets/widget"],function(a,b,c){var d=a.$;return b.options.dnd="",b.register({name:"dnd",init:function(b){if(b.dnd&&"html5"===this.request("predict-runtime-type")){var e,f=this,g=a.Deferred(),h=d.extend({},{disableGlobalDnd:b.disableGlobalDnd,container:b.dnd,accept:b.accept});return this.dnd=e=new c(h),e.once("ready",g.resolve),e.on("drop",function(a){f.request("add-file",[a])}),e.on("accept",function(a){return f.owner.trigger("dndAccept",a)}),e.init(),g.promise()}},destroy:function(){this.dnd&&this.dnd.destroy()}})}),b("lib/filepaste",["base","mediator","runtime/client"],function(a,b,c){function d(a){a=this.options=e.extend({},a),a.container=e(a.container||document.body),c.call(this,"FilePaste")}var e=a.$;return a.inherits(c,{constructor:d,init:function(){var a=this;a.connectRuntime(a.options,function(){a.exec("init"),a.trigger("ready")})}}),b.installTo(d.prototype),d}),b("widgets/filepaste",["base","uploader","lib/filepaste","widgets/widget"],function(a,b,c){var d=a.$;return b.register({name:"paste",init:function(b){if(b.paste&&"html5"===this.request("predict-runtime-type")){var e,f=this,g=a.Deferred(),h=d.extend({},{container:b.paste,accept:b.accept});return this.paste=e=new c(h),e.once("ready",g.resolve),e.on("paste",function(a){f.owner.request("add-file",[a])}),e.init(),g.promise()}},destroy:function(){this.paste&&this.paste.destroy()}})}),b("lib/blob",["base","runtime/client"],function(a,b){function c(a,c){var d=this;d.source=c,d.ruid=a,this.size=c.size||0,this.type=!c.type&&this.ext&&~"jpg,jpeg,png,gif,bmp".indexOf(this.ext)?"image/"+("jpg"===this.ext?"jpeg":this.ext):c.type||"application/octet-stream",b.call(d,"Blob"),this.uid=c.uid||this.uid,a&&d.connectRuntime(a)}return a.inherits(b,{constructor:c,slice:function(a,b){return this.exec("slice",a,b)},getSource:function(){return this.source}}),c}),b("lib/file",["base","lib/blob"],function(a,b){function c(a,c){var f;this.name=c.name||"untitled"+d++,f=e.exec(c.name)?RegExp.$1.toLowerCase():"",!f&&c.type&&(f=/\/(jpg|jpeg|png|gif|bmp)$/i.exec(c.type)?RegExp.$1.toLowerCase():"",this.name+="."+f),this.ext=f,this.lastModifiedDate=c.lastModifiedDate||(new Date).toLocaleString(),b.apply(this,arguments)}var d=1,e=/\.([^.]+)$/;return a.inherits(b,c)}),b("lib/filepicker",["base","runtime/client","lib/file"],function(b,c,d){function e(a){if(a=this.options=f.extend({},e.options,a),a.container=f(a.id),!a.container.length)throw new Error("按钮指定错误");a.innerHTML=a.innerHTML||a.label||a.container.html()||"",a.button=f(a.button||document.createElement("div")),a.button.html(a.innerHTML),a.container.html(a.button),c.call(this,"FilePicker",!0)}var f=b.$;return e.options={button:null,container:null,label:null,innerHTML:null,multiple:!0,accept:null,name:"file",style:"webuploader-pick"},b.inherits(c,{constructor:e,init:function(){var c=this,e=c.options,g=e.button,h=e.style;h&&g.addClass("webuploader-pick"),c.on("all",function(a){var b;switch(a){case"mouseenter":h&&g.addClass("webuploader-pick-hover");break;case"mouseleave":h&&g.removeClass("webuploader-pick-hover");break;case"change":b=c.exec("getFiles"),c.trigger("select",f.map(b,function(a){return a=new d(c.getRuid(),a),a._refer=e.container,a}),e.container)}}),c.connectRuntime(e,function(){c.refresh(),c.exec("init",e),c.trigger("ready")}),this._resizeHandler=b.bindFn(this.refresh,this),f(a).on("resize",this._resizeHandler)},refresh:function(){var a=this.getRuntime().getContainer(),b=this.options.button,c=b.outerWidth?b.outerWidth():b.width(),d=b.outerHeight?b.outerHeight():b.height(),e=b.offset();c&&d&&a.css({bottom:"auto",right:"auto",width:c+"px",height:d+"px"}).offset(e)},enable:function(){var a=this.options.button;a.removeClass("webuploader-pick-disable"),this.refresh()},disable:function(){var a=this.options.button;this.getRuntime().getContainer().css({top:"-99999px"}),a.addClass("webuploader-pick-disable")},destroy:function(){var b=this.options.button;f(a).off("resize",this._resizeHandler),b.removeClass("webuploader-pick-disable webuploader-pick-hover webuploader-pick")}}),e}),b("widgets/filepicker",["base","uploader","lib/filepicker","widgets/widget"],function(a,b,c){var d=a.$;return d.extend(b.options,{pick:null,accept:null}),b.register({name:"picker",init:function(a){return this.pickers=[],a.pick&&this.addBtn(a.pick)},refresh:function(){d.each(this.pickers,function(){this.refresh()})},addBtn:function(b){var e=this,f=e.options,g=f.accept,h=[];if(b)return d.isPlainObject(b)||(b={id:b}),d(b.id).each(function(){var i,j,k;k=a.Deferred(),i=d.extend({},b,{accept:d.isPlainObject(g)?[g]:g,swf:f.swf,runtimeOrder:f.runtimeOrder,id:this}),j=new c(i),j.once("ready",k.resolve),j.on("select",function(a){e.owner.request("add-file",[a])}),j.on("dialogopen",function(){e.owner.trigger("dialogOpen",j.button)}),j.init(),e.pickers.push(j),h.push(k.promise())}),a.when.apply(a,h)},disable:function(){d.each(this.pickers,function(){this.disable()})},enable:function(){d.each(this.pickers,function(){this.enable()})},destroy:function(){d.each(this.pickers,function(){this.destroy()}),this.pickers=null}})}),b("lib/image",["base","runtime/client","lib/blob"],function(a,b,c){function d(a){this.options=e.extend({},d.options,a),b.call(this,"Image"),this.on("load",function(){this._info=this.exec("info"),this._meta=this.exec("meta")})}var e=a.$;return d.options={quality:90,crop:!1,preserveHeaders:!1,allowMagnify:!1},a.inherits(b,{constructor:d,info:function(a){return a?(this._info=a,this):this._info},meta:function(a){return a?(this._meta=a,this):this._meta},loadFromBlob:function(a){var b=this,c=a.getRuid();this.connectRuntime(c,function(){b.exec("init",b.options),b.exec("loadFromBlob",a)})},resize:function(){var b=a.slice(arguments);return this.exec.apply(this,["resize"].concat(b))},crop:function(){var b=a.slice(arguments);return this.exec.apply(this,["crop"].concat(b))},getAsDataUrl:function(a){return this.exec("getAsDataUrl",a)},getAsBlob:function(a){var b=this.exec("getAsBlob",a);return new c(this.getRuid(),b)}}),d}),b("widgets/image",["base","uploader","lib/image","widgets/widget"],function(a,b,c){var d,e=a.$;return d=function(a){var b=0,c=[],d=function(){for(var d;c.length&&a>b;)d=c.shift(),b+=d[0],d[1]()};return function(a,e,f){c.push([e,f]),a.once("destroy",function(){b-=e,setTimeout(d,1)}),setTimeout(d,1)}}(5242880),e.extend(b.options,{thumb:{width:110,height:110,quality:70,allowMagnify:!0,crop:!0,preserveHeaders:!1,type:"image/jpeg"},compress:{width:1600,height:1600,quality:90,allowMagnify:!1,crop:!1,preserveHeaders:!0}}),b.register({name:"image",makeThumb:function(a,b,f,g){var h,i;return a=this.request("get-file",a),a.type.match(/^image/)?(h=e.extend({},this.options.thumb),e.isPlainObject(f)&&(h=e.extend(h,f),f=null),f=f||h.width,g=g||h.height,i=new c(h),i.once("load",function(){a._info=a._info||i.info(),a._meta=a._meta||i.meta(),1>=f&&f>0&&(f=a._info.width*f),1>=g&&g>0&&(g=a._info.height*g),i.resize(f,g)}),i.once("complete",function(){b(!1,i.getAsDataUrl(h.type)),i.destroy()}),i.once("error",function(a){b(a||!0),i.destroy()}),d(i,a.source.size,function(){a._info&&i.info(a._info),a._meta&&i.meta(a._meta),i.loadFromBlob(a.source)}),void 0):(b(!0),void 0)},beforeSendFile:function(b){var d,f,g=this.options.compress||this.options.resize,h=g&&g.compressSize||0,i=g&&g.noCompressIfLarger||!1;return b=this.request("get-file",b),!g||!~"image/jpeg,image/jpg".indexOf(b.type)||b.size<h||b._compressed?void 0:(g=e.extend({},g),f=a.Deferred(),d=new c(g),f.always(function(){d.destroy(),d=null}),d.once("error",f.reject),d.once("load",function(){var a=g.width,c=g.height;b._info=b._info||d.info(),b._meta=b._meta||d.meta(),1>=a&&a>0&&(a=b._info.width*a),1>=c&&c>0&&(c=b._info.height*c),d.resize(a,c)}),d.once("complete",function(){var a,c;try{a=d.getAsBlob(g.type),c=b.size,(!i||a.size<c)&&(b.source=a,b.size=a.size,b.trigger("resize",a.size,c)),b._compressed=!0,f.resolve()}catch(e){f.resolve()}}),b._info&&d.info(b._info),b._meta&&d.meta(b._meta),d.loadFromBlob(b.source),f.promise())}})}),b("file",["base","mediator"],function(a,b){function c(){return f+g++}function d(a){this.name=a.name||"Untitled",this.size=a.size||0,this.type=a.type||"application/octet-stream",this.lastModifiedDate=a.lastModifiedDate||1*new Date,this.id=c(),this.ext=h.exec(this.name)?RegExp.$1:"",this.statusText="",i[this.id]=d.Status.INITED,this.source=a,this.loaded=0,this.on("error",function(a){this.setStatus(d.Status.ERROR,a)})}var e=a.$,f="WU_FILE_",g=0,h=/\.([^.]+)$/,i={};return e.extend(d.prototype,{setStatus:function(a,b){var c=i[this.id];"undefined"!=typeof b&&(this.statusText=b),a!==c&&(i[this.id]=a,this.trigger("statuschange",a,c))},getStatus:function(){return i[this.id]},getSource:function(){return this.source},destroy:function(){this.off(),delete i[this.id]}}),b.installTo(d.prototype),d.Status={INITED:"inited",QUEUED:"queued",PROGRESS:"progress",ERROR:"error",COMPLETE:"complete",CANCELLED:"cancelled",INTERRUPT:"interrupt",INVALID:"invalid"},d}),b("queue",["base","mediator","file"],function(a,b,c){function d(){this.stats={numOfQueue:0,numOfSuccess:0,numOfCancel:0,numOfProgress:0,numOfUploadFailed:0,numOfInvalid:0,numofDeleted:0,numofInterrupt:0},this._queue=[],this._map={}}var e=a.$,f=c.Status;return e.extend(d.prototype,{append:function(a){return this._queue.push(a),this._fileAdded(a),this},prepend:function(a){return this._queue.unshift(a),this._fileAdded(a),this},getFile:function(a){return"string"!=typeof a?a:this._map[a]},fetch:function(a){var b,c,d=this._queue.length;for(a=a||f.QUEUED,b=0;d>b;b++)if(c=this._queue[b],a===c.getStatus())return c;return null},sort:function(a){"function"==typeof a&&this._queue.sort(a)},getFiles:function(){for(var a,b=[].slice.call(arguments,0),c=[],d=0,f=this._queue.length;f>d;d++)a=this._queue[d],(!b.length||~e.inArray(a.getStatus(),b))&&c.push(a);return c},removeFile:function(a){var b=this._map[a.id];b&&(delete this._map[a.id],this._delFile(a),a.destroy(),this.stats.numofDeleted++)},_fileAdded:function(a){var b=this,c=this._map[a.id];c||(this._map[a.id]=a,a.on("statuschange",function(a,c){b._onFileStatusChange(a,c)}))},_delFile:function(a){for(var b=this._queue.length-1;b>=0;b--)if(this._queue[b]==a){this._queue.splice(b,1);break}},_onFileStatusChange:function(a,b){var c=this.stats;switch(b){case f.PROGRESS:c.numOfProgress--;break;case f.QUEUED:c.numOfQueue--;break;case f.ERROR:c.numOfUploadFailed--;break;case f.INVALID:c.numOfInvalid--;break;case f.INTERRUPT:c.numofInterrupt--}switch(a){case f.QUEUED:c.numOfQueue++;break;case f.PROGRESS:c.numOfProgress++;break;case f.ERROR:c.numOfUploadFailed++;break;case f.COMPLETE:c.numOfSuccess++;break;case f.CANCELLED:c.numOfCancel++;break;case f.INVALID:c.numOfInvalid++;break;case f.INTERRUPT:c.numofInterrupt++}}}),b.installTo(d.prototype),d}),b("widgets/queue",["base","uploader","queue","file","lib/file","runtime/client","widgets/widget"],function(a,b,c,d,e,f){var g=a.$,h=/\.\w+$/,i=d.Status;return b.register({name:"queue",init:function(b){var d,e,h,i,j,k,l,m=this;if(g.isPlainObject(b.accept)&&(b.accept=[b.accept]),b.accept){for(j=[],h=0,e=b.accept.length;e>h;h++)i=b.accept[h].extensions,i&&j.push(i);j.length&&(k="\\."+j.join(",").replace(/,/g,"$|\\.").replace(/\*/g,".*")+"$"),m.accept=new RegExp(k,"i")}return m.queue=new c,m.stats=m.queue.stats,"html5"===this.request("predict-runtime-type")?(d=a.Deferred(),this.placeholder=l=new f("Placeholder"),l.connectRuntime({runtimeOrder:"html5"},function(){m._ruid=l.getRuid(),d.resolve()}),d.promise()):void 0},_wrapFile:function(a){if(!(a instanceof d)){if(!(a instanceof e)){if(!this._ruid)throw new Error("Can't add external files.");a=new e(this._ruid,a)}a=new d(a)}return a},acceptFile:function(a){var b=!a||!a.size||this.accept&&h.exec(a.name)&&!this.accept.test(a.name);return!b},_addFile:function(a){var b=this;return a=b._wrapFile(a),b.owner.trigger("beforeFileQueued",a)?b.acceptFile(a)?(b.queue.append(a),b.owner.trigger("fileQueued",a),a):(b.owner.trigger("error","Q_TYPE_DENIED",a),void 0):void 0},getFile:function(a){return this.queue.getFile(a)},addFile:function(a){var b=this;a.length||(a=[a]),a=g.map(a,function(a){return b._addFile(a)}),a.length&&(b.owner.trigger("filesQueued",a),b.options.auto&&setTimeout(function(){b.request("start-upload")},20))},getStats:function(){return this.stats},removeFile:function(a,b){var c=this;a=a.id?a:c.queue.getFile(a),this.request("cancel-file",a),b&&this.queue.removeFile(a)},getFiles:function(){return this.queue.getFiles.apply(this.queue,arguments)},fetchFile:function(){return this.queue.fetch.apply(this.queue,arguments)},retry:function(a,b){var c,d,e,f=this;if(a)return a=a.id?a:f.queue.getFile(a),a.setStatus(i.QUEUED),b||f.request("start-upload"),void 0;for(c=f.queue.getFiles(i.ERROR),d=0,e=c.length;e>d;d++)a=c[d],a.setStatus(i.QUEUED);f.request("start-upload")},sortFiles:function(){return this.queue.sort.apply(this.queue,arguments)},reset:function(){this.owner.trigger("reset"),this.queue=new c,this.stats=this.queue.stats},destroy:function(){this.reset(),this.placeholder&&this.placeholder.destroy()}})}),b("widgets/runtime",["uploader","runtime/runtime","widgets/widget"],function(a,b){return a.support=function(){return b.hasRuntime.apply(b,arguments)},a.register({name:"runtime",init:function(){if(!this.predictRuntimeType())throw Error("Runtime Error")},predictRuntimeType:function(){var a,c,d=this.options.runtimeOrder||b.orders,e=this.type;if(!e)for(d=d.split(/\s*,\s*/g),a=0,c=d.length;c>a;a++)if(b.hasRuntime(d[a])){this.type=e=d[a];break}return e}})}),b("lib/transport",["base","runtime/client","mediator"],function(a,b,c){function d(a){var c=this;a=c.options=e.extend(!0,{},d.options,a||{}),b.call(this,"Transport"),this._blob=null,this._formData=a.formData||{},this._headers=a.headers||{},this.on("progress",this._timeout),this.on("load error",function(){c.trigger("progress",1),clearTimeout(c._timer)})}var e=a.$;return d.options={server:"",method:"POST",withCredentials:!1,fileVal:"file",timeout:12e4,formData:{},headers:{},sendAsBinary:!1},e.extend(d.prototype,{appendBlob:function(a,b,c){var d=this,e=d.options;d.getRuid()&&d.disconnectRuntime(),d.connectRuntime(b.ruid,function(){d.exec("init")}),d._blob=b,e.fileVal=a||e.fileVal,e.filename=c||e.filename},append:function(a,b){"object"==typeof a?e.extend(this._formData,a):this._formData[a]=b},setRequestHeader:function(a,b){"object"==typeof a?e.extend(this._headers,a):this._headers[a]=b},send:function(a){this.exec("send",a),this._timeout()},abort:function(){return clearTimeout(this._timer),this.exec("abort")},destroy:function(){this.trigger("destroy"),this.off(),this.exec("destroy"),this.disconnectRuntime()},getResponse:function(){return this.exec("getResponse")},getResponseAsJson:function(){return this.exec("getResponseAsJson")},getStatus:function(){return this.exec("getStatus")},_timeout:function(){var a=this,b=a.options.timeout;b&&(clearTimeout(a._timer),a._timer=setTimeout(function(){a.abort(),a.trigger("error","timeout")},b))}}),c.installTo(d.prototype),d}),b("widgets/upload",["base","uploader","file","lib/transport","widgets/widget"],function(a,b,c,d){function e(a,b){var c,d,e=[],f=a.source,g=f.size,h=b?Math.ceil(g/b):1,i=0,j=0;for(d={file:a,has:function(){return!!e.length},shift:function(){return e.shift()},unshift:function(a){e.unshift(a)}};h>j;)c=Math.min(b,g-i),e.push({file:a,start:i,end:b?i+c:g,total:g,chunks:h,chunk:j++,cuted:d}),i+=c;return a.blocks=e.concat(),a.remaning=e.length,d}var f=a.$,g=a.isPromise,h=c.Status;f.extend(b.options,{prepareNextFile:!1,chunked:!1,chunkSize:5242880,chunkRetry:2,threads:3,formData:{}}),b.register({name:"upload",init:function(){var b=this.owner,c=this;this.runing=!1,this.progress=!1,b.on("startUpload",function(){c.progress=!0}).on("uploadFinished",function(){c.progress=!1}),this.pool=[],this.stack=[],this.pending=[],this.remaning=0,this.__tick=a.bindFn(this._tick,this),b.on("uploadComplete",function(a){a.blocks&&f.each(a.blocks,function(a,b){b.transport&&(b.transport.abort(),b.transport.destroy()),delete b.transport}),delete a.blocks,delete a.remaning})},reset:function(){this.request("stop-upload",!0),this.runing=!1,this.pool=[],this.stack=[],this.pending=[],this.remaning=0,this._trigged=!1,this._promise=null},startUpload:function(b){var c=this;if(f.each(c.request("get-files",h.INVALID),function(){c.request("remove-file",this)}),b?(b=b.id?b:c.request("get-file",b),b.getStatus()===h.INTERRUPT?(b.setStatus(h.QUEUED),f.each(c.pool,function(a,c){c.file===b&&(c.transport&&c.transport.send(),b.setStatus(h.PROGRESS))})):b.getStatus()!==h.PROGRESS&&b.setStatus(h.QUEUED)):f.each(c.request("get-files",[h.INITED]),function(){this.setStatus(h.QUEUED)}),c.runing)return c.owner.trigger("startUpload",b),a.nextTick(c.__tick);c.runing=!0;var d=[];b||f.each(c.pool,function(a,b){var e=b.file;e.getStatus()===h.INTERRUPT&&(c._trigged=!1,d.push(e),b.transport&&b.transport.send())}),f.each(d,function(){this.setStatus(h.PROGRESS)}),b||f.each(c.request("get-files",h.INTERRUPT),function(){this.setStatus(h.PROGRESS)}),c._trigged=!1,a.nextTick(c.__tick),c.owner.trigger("startUpload")},stopUpload:function(b,c){var d=this;if(b===!0&&(c=b,b=null),d.runing!==!1){if(b){if(b=b.id?b:d.request("get-file",b),b.getStatus()!==h.PROGRESS&&b.getStatus()!==h.QUEUED)return;return b.setStatus(h.INTERRUPT),f.each(d.pool,function(a,e){e.file===b&&(e.transport&&e.transport.abort(),c&&(d._putback(e),d._popBlock(e)))}),d.owner.trigger("stopUpload",b),a.nextTick(d.__tick)}d.runing=!1,this._promise&&this._promise.file&&this._promise.file.setStatus(h.INTERRUPT),c&&f.each(d.pool,function(a,b){b.transport&&b.transport.abort(),b.file.setStatus(h.INTERRUPT)}),d.owner.trigger("stopUpload")}},cancelFile:function(a){a=a.id?a:this.request("get-file",a),a.blocks&&f.each(a.blocks,function(a,b){var c=b.transport;c&&(c.abort(),c.destroy(),delete b.transport)}),a.setStatus(h.CANCELLED),this.owner.trigger("fileDequeued",a)},isInProgress:function(){return!!this.progress},_getStats:function(){return this.request("get-stats")},skipFile:function(a,b){a=a.id?a:this.request("get-file",a),a.setStatus(b||h.COMPLETE),a.skipped=!0,a.blocks&&f.each(a.blocks,function(a,b){var c=b.transport;c&&(c.abort(),c.destroy(),delete b.transport)}),this.owner.trigger("uploadSkip",a)},_tick:function(){var b,c,d=this,e=d.options;return d._promise?d._promise.always(d.__tick):(d.pool.length<e.threads&&(c=d._nextBlock())?(d._trigged=!1,b=function(b){d._promise=null,b&&b.file&&d._startSend(b),a.nextTick(d.__tick)},d._promise=g(c)?c.always(b):b(c)):d.remaning||d._getStats().numOfQueue||d._getStats().numofInterrupt||(d.runing=!1,d._trigged||a.nextTick(function(){d.owner.trigger("uploadFinished")}),d._trigged=!0),void 0)},_putback:function(a){var b;a.cuted.unshift(a),b=this.stack.indexOf(a.cuted),~b||this.stack.unshift(a.cuted)},_getStack:function(){for(var a,b=0;a=this.stack[b++];){if(a.has()&&a.file.getStatus()===h.PROGRESS)return a;(!a.has()||a.file.getStatus()!==h.PROGRESS&&a.file.getStatus()!==h.INTERRUPT)&&this.stack.splice(--b,1)}return null},_nextBlock:function(){var a,b,c,d,f=this,h=f.options;return(a=this._getStack())?(h.prepareNextFile&&!f.pending.length&&f._prepareNextFile(),a.shift()):f.runing?(!f.pending.length&&f._getStats().numOfQueue&&f._prepareNextFile(),b=f.pending.shift(),c=function(b){return b?(a=e(b,h.chunked?h.chunkSize:0),f.stack.push(a),a.shift()):null},g(b)?(d=b.file,b=b[b.pipe?"pipe":"then"](c),b.file=d,b):c(b)):void 0},_prepareNextFile:function(){var a,b=this,c=b.request("fetch-file"),d=b.pending;c&&(a=b.request("before-send-file",c,function(){return c.getStatus()===h.PROGRESS||c.getStatus()===h.INTERRUPT?c:b._finishFile(c)}),b.owner.trigger("uploadStart",c),c.setStatus(h.PROGRESS),a.file=c,a.done(function(){var b=f.inArray(a,d);~b&&d.splice(b,1,c)}),a.fail(function(a){c.setStatus(h.ERROR,a),b.owner.trigger("uploadError",c,a),b.owner.trigger("uploadComplete",c)}),d.push(a))},_popBlock:function(a){var b=f.inArray(a,this.pool);this.pool.splice(b,1),a.file.remaning--,this.remaning--},_startSend:function(b){var c,d=this,e=b.file;return e.getStatus()!==h.PROGRESS?(e.getStatus()===h.INTERRUPT&&d._putback(b),void 0):(d.pool.push(b),d.remaning++,b.blob=1===b.chunks?e.source:e.source.slice(b.start,b.end),c=d.request("before-send",b,function(){e.getStatus()===h.PROGRESS?d._doSend(b):(d._popBlock(b),a.nextTick(d.__tick))}),c.fail(function(){1===e.remaning?d._finishFile(e).always(function(){b.percentage=1,d._popBlock(b),d.owner.trigger("uploadComplete",e),a.nextTick(d.__tick)}):(b.percentage=1,d.updateFileProgress(e),d._popBlock(b),a.nextTick(d.__tick))}),void 0)},_doSend:function(b){var c,e,g=this,i=g.owner,j=g.options,k=b.file,l=new d(j),m=f.extend({},j.formData),n=f.extend({},j.headers);b.transport=l,l.on("destroy",function(){delete b.transport,g._popBlock(b),a.nextTick(g.__tick)}),l.on("progress",function(a){b.percentage=a,g.updateFileProgress(k)}),c=function(a){var c;return e=l.getResponseAsJson()||{},e._raw=l.getResponse(),c=function(b){a=b},i.trigger("uploadAccept",b,e,c)||(a=a||"server"),a},l.on("error",function(a,d){b.retried=b.retried||0,b.chunks>1&&~"http,abort".indexOf(a)&&b.retried<j.chunkRetry?(b.retried++,l.send()):(d||"server"!==a||(a=c(a)),k.setStatus(h.ERROR,a),i.trigger("uploadError",k,a),i.trigger("uploadComplete",k))}),l.on("load",function(){var a;return(a=c())?(l.trigger("error",a,!0),void 0):(1===k.remaning?g._finishFile(k,e):l.destroy(),void 0)}),m=f.extend(m,{id:k.id,name:k.name,type:k.type,lastModifiedDate:k.lastModifiedDate,size:k.size}),b.chunks>1&&f.extend(m,{chunks:b.chunks,chunk:b.chunk}),i.trigger("uploadBeforeSend",b,m,n),l.appendBlob(j.fileVal,b.blob,k.name),l.append(m),l.setRequestHeader(n),l.send()
},_finishFile:function(a,b,c){var d=this.owner;return d.request("after-send-file",arguments,function(){a.setStatus(h.COMPLETE),d.trigger("uploadSuccess",a,b,c)}).fail(function(b){a.getStatus()===h.PROGRESS&&a.setStatus(h.ERROR,b),d.trigger("uploadError",a,b)}).always(function(){d.trigger("uploadComplete",a)})},updateFileProgress:function(a){var b=0,c=0;a.blocks&&(f.each(a.blocks,function(a,b){c+=(b.percentage||0)*(b.end-b.start)}),b=c/a.size,this.owner.trigger("uploadProgress",a,b||0))}})}),b("widgets/validator",["base","uploader","file","widgets/widget"],function(a,b,c){var d,e=a.$,f={};return d={addValidator:function(a,b){f[a]=b},removeValidator:function(a){delete f[a]}},b.register({name:"validator",init:function(){var b=this;a.nextTick(function(){e.each(f,function(){this.call(b.owner)})})}}),d.addValidator("fileNumLimit",function(){var a=this,b=a.options,c=0,d=parseInt(b.fileNumLimit,10),e=!0;d&&(a.on("beforeFileQueued",function(a){return this.trigger("beforeFileQueuedCheckfileNumLimit",a,c)?(c>=d&&e&&(e=!1,this.trigger("error","Q_EXCEED_NUM_LIMIT",d,a),setTimeout(function(){e=!0},1)),c>=d?!1:!0):!1}),a.on("fileQueued",function(){c++}),a.on("fileDequeued",function(){c--}),a.on("reset",function(){c=0}))}),d.addValidator("fileSizeLimit",function(){var a=this,b=a.options,c=0,d=parseInt(b.fileSizeLimit,10),e=!0;d&&(a.on("beforeFileQueued",function(a){var b=c+a.size>d;return b&&e&&(e=!1,this.trigger("error","Q_EXCEED_SIZE_LIMIT",d,a),setTimeout(function(){e=!0},1)),b?!1:!0}),a.on("fileQueued",function(a){c+=a.size}),a.on("fileDequeued",function(a){c-=a.size}),a.on("reset",function(){c=0}))}),d.addValidator("fileSingleSizeLimit",function(){var a=this,b=a.options,d=b.fileSingleSizeLimit;d&&a.on("beforeFileQueued",function(a){return a.size>d?(a.setStatus(c.Status.INVALID,"exceed_size"),this.trigger("error","F_EXCEED_SIZE",d,a),!1):void 0})}),d.addValidator("duplicate",function(){function a(a){for(var b,c=0,d=0,e=a.length;e>d;d++)b=a.charCodeAt(d),c=b+(c<<6)+(c<<16)-c;return c}var b=this,c=b.options,d={};c.duplicate||(b.on("beforeFileQueued",function(b){var c=b.__hash||(b.__hash=a(b.name+b.size+b.lastModifiedDate));return d[c]?(this.trigger("error","F_DUPLICATE",b),!1):void 0}),b.on("fileQueued",function(a){var b=a.__hash;b&&(d[b]=!0)}),b.on("fileDequeued",function(a){var b=a.__hash;b&&delete d[b]}),b.on("reset",function(){d={}}))}),d}),b("lib/md5",["runtime/client","mediator"],function(a,b){function c(){a.call(this,"Md5")}return b.installTo(c.prototype),c.prototype.loadFromBlob=function(a){var b=this;b.getRuid()&&b.disconnectRuntime(),b.connectRuntime(a.ruid,function(){b.exec("init"),b.exec("loadFromBlob",a)})},c.prototype.getResult=function(){return this.exec("getResult")},c}),b("widgets/md5",["base","uploader","lib/md5","lib/blob","widgets/widget"],function(a,b,c,d){return b.register({name:"md5",md5File:function(b,e,f){var g=new c,h=a.Deferred(),i=b instanceof d?b:this.request("get-file",b).source;return g.on("progress load",function(a){a=a||{},h.notify(a.total?a.loaded/a.total:1)}),g.on("complete",function(){h.resolve(g.getResult())}),g.on("error",function(a){h.reject(a)}),arguments.length>1&&(e=e||0,f=f||0,0>e&&(e=i.size+e),0>f&&(f=i.size+f),f=Math.min(f,i.size),i=i.slice(e,f)),g.loadFromBlob(i),h.promise()}})}),b("runtime/compbase",[],function(){function a(a,b){this.owner=a,this.options=a.options,this.getRuntime=function(){return b},this.getRuid=function(){return b.uid},this.trigger=function(){return a.trigger.apply(a,arguments)}}return a}),b("runtime/html5/runtime",["base","runtime/runtime","runtime/compbase"],function(b,c,d){function e(){var a={},d=this,e=this.destroy;c.apply(d,arguments),d.type=f,d.exec=function(c,e){var f,h=this,i=h.uid,j=b.slice(arguments,2);return g[c]&&(f=a[i]=a[i]||new g[c](h,d),f[e])?f[e].apply(f,j):void 0},d.destroy=function(){return e&&e.apply(this,arguments)}}var f="html5",g={};return b.inherits(c,{constructor:e,init:function(){var a=this;setTimeout(function(){a.trigger("ready")},1)}}),e.register=function(a,c){var e=g[a]=b.inherits(d,c);return e},a.Blob&&a.FileReader&&a.DataView&&c.addRuntime(f,e),e}),b("runtime/html5/blob",["runtime/html5/runtime","lib/blob"],function(a,b){return a.register("Blob",{slice:function(a,c){var d=this.owner.source,e=d.slice||d.webkitSlice||d.mozSlice;return d=e.call(d,a,c),new b(this.getRuid(),d)}})}),b("runtime/html5/dnd",["base","runtime/html5/runtime","lib/file"],function(a,b,c){var d=a.$,e="webuploader-dnd-";return b.register("DragAndDrop",{init:function(){var b=this.elem=this.options.container;this.dragEnterHandler=a.bindFn(this._dragEnterHandler,this),this.dragOverHandler=a.bindFn(this._dragOverHandler,this),this.dragLeaveHandler=a.bindFn(this._dragLeaveHandler,this),this.dropHandler=a.bindFn(this._dropHandler,this),this.dndOver=!1,b.on("dragenter",this.dragEnterHandler),b.on("dragover",this.dragOverHandler),b.on("dragleave",this.dragLeaveHandler),b.on("drop",this.dropHandler),this.options.disableGlobalDnd&&(d(document).on("dragover",this.dragOverHandler),d(document).on("drop",this.dropHandler))},_dragEnterHandler:function(a){var b,c=this,d=c._denied||!1;return a=a.originalEvent||a,c.dndOver||(c.dndOver=!0,b=a.dataTransfer.items,b&&b.length&&(c._denied=d=!c.trigger("accept",b)),c.elem.addClass(e+"over"),c.elem[d?"addClass":"removeClass"](e+"denied")),a.dataTransfer.dropEffect=d?"none":"copy",!1},_dragOverHandler:function(a){var b=this.elem.parent().get(0);return b&&!d.contains(b,a.currentTarget)?!1:(clearTimeout(this._leaveTimer),this._dragEnterHandler.call(this,a),!1)},_dragLeaveHandler:function(){var a,b=this;return a=function(){b.dndOver=!1,b.elem.removeClass(e+"over "+e+"denied")},clearTimeout(b._leaveTimer),b._leaveTimer=setTimeout(a,100),!1},_dropHandler:function(a){var b,f,g=this,h=g.getRuid(),i=g.elem.parent().get(0);if(i&&!d.contains(i,a.currentTarget))return!1;a=a.originalEvent||a,b=a.dataTransfer;try{f=b.getData("text/html")}catch(j){}return g.dndOver=!1,g.elem.removeClass(e+"over"),b&&!f?(g._getTansferFiles(b,function(a){g.trigger("drop",d.map(a,function(a){return new c(h,a)}))}),!1):void 0},_getTansferFiles:function(b,c){var d,e,f,g,h,i,j,k=[],l=[];for(d=b.items,e=b.files,j=!(!d||!d[0].webkitGetAsEntry),h=0,i=e.length;i>h;h++)f=e[h],g=d&&d[h],j&&g.webkitGetAsEntry().isDirectory?l.push(this._traverseDirectoryTree(g.webkitGetAsEntry(),k)):k.push(f);a.when.apply(a,l).done(function(){k.length&&c(k)})},_traverseDirectoryTree:function(b,c){var d=a.Deferred(),e=this;return b.isFile?b.file(function(a){c.push(a),d.resolve()}):b.isDirectory&&b.createReader().readEntries(function(b){var f,g=b.length,h=[],i=[];for(f=0;g>f;f++)h.push(e._traverseDirectoryTree(b[f],i));a.when.apply(a,h).then(function(){c.push.apply(c,i),d.resolve()},d.reject)}),d.promise()},destroy:function(){var a=this.elem;a&&(a.off("dragenter",this.dragEnterHandler),a.off("dragover",this.dragOverHandler),a.off("dragleave",this.dragLeaveHandler),a.off("drop",this.dropHandler),this.options.disableGlobalDnd&&(d(document).off("dragover",this.dragOverHandler),d(document).off("drop",this.dropHandler)))}})}),b("runtime/html5/filepaste",["base","runtime/html5/runtime","lib/file"],function(a,b,c){return b.register("FilePaste",{init:function(){var b,c,d,e,f=this.options,g=this.elem=f.container,h=".*";if(f.accept){for(b=[],c=0,d=f.accept.length;d>c;c++)e=f.accept[c].mimeTypes,e&&b.push(e);b.length&&(h=b.join(","),h=h.replace(/,/g,"|").replace(/\*/g,".*"))}this.accept=h=new RegExp(h,"i"),this.hander=a.bindFn(this._pasteHander,this),g.on("paste",this.hander)},_pasteHander:function(a){var b,d,e,f,g,h=[],i=this.getRuid();for(a=a.originalEvent||a,b=a.clipboardData.items,f=0,g=b.length;g>f;f++)d=b[f],"file"===d.kind&&(e=d.getAsFile())&&h.push(new c(i,e));h.length&&(a.preventDefault(),a.stopPropagation(),this.trigger("paste",h))},destroy:function(){this.elem.off("paste",this.hander)}})}),b("runtime/html5/filepicker",["base","runtime/html5/runtime"],function(a,b){var c=a.$;return b.register("FilePicker",{init:function(){var a,b,d,e,f,g=this.getRuntime().getContainer(),h=this,i=h.owner,j=h.options,k=this.label=c(document.createElement("label")),l=this.input=c(document.createElement("input"));if(l.attr("type","file"),l.attr("capture","camera"),l.attr("name",j.name),l.addClass("webuploader-element-invisible"),k.on("click",function(a){l.trigger("click"),a.stopPropagation(),i.trigger("dialogopen")}),k.css({opacity:0,width:"100%",height:"100%",display:"block",cursor:"pointer",background:"#ffffff"}),j.multiple&&l.attr("multiple","multiple"),j.accept&&j.accept.length>0){for(a=[],b=0,d=j.accept.length;d>b;b++)a.push(j.accept[b].mimeTypes);l.attr("accept",a.join(","))}g.append(l),g.append(k),e=function(a){i.trigger(a.type)},f=function(a){var b;return 0===a.target.files.length?!1:(h.files=a.target.files,b=this.cloneNode(!0),b.value=null,this.parentNode.replaceChild(b,this),l.off(),l=c(b).on("change",f).on("mouseenter mouseleave",e),i.trigger("change"),void 0)},l.on("change",f),k.on("mouseenter mouseleave",e)},getFiles:function(){return this.files},destroy:function(){this.input.off(),this.label.off()}})}),b("runtime/html5/util",["base"],function(b){var c=a.createObjectURL&&a||a.URL&&URL.revokeObjectURL&&URL||a.webkitURL,d=b.noop,e=d;return c&&(d=function(){return c.createObjectURL.apply(c,arguments)},e=function(){return c.revokeObjectURL.apply(c,arguments)}),{createObjectURL:d,revokeObjectURL:e,dataURL2Blob:function(a){var b,c,d,e,f,g;for(g=a.split(","),b=~g[0].indexOf("base64")?atob(g[1]):decodeURIComponent(g[1]),d=new ArrayBuffer(b.length),c=new Uint8Array(d),e=0;e<b.length;e++)c[e]=b.charCodeAt(e);return f=g[0].split(":")[1].split(";")[0],this.arrayBufferToBlob(d,f)},dataURL2ArrayBuffer:function(a){var b,c,d,e;for(e=a.split(","),b=~e[0].indexOf("base64")?atob(e[1]):decodeURIComponent(e[1]),c=new Uint8Array(b.length),d=0;d<b.length;d++)c[d]=b.charCodeAt(d);return c.buffer},arrayBufferToBlob:function(b,c){var d,e=a.BlobBuilder||a.WebKitBlobBuilder;return e?(d=new e,d.append(b),d.getBlob(c)):new Blob([b],c?{type:c}:{})},canvasToDataUrl:function(a,b,c){return a.toDataURL(b,c/100)},parseMeta:function(a,b){b(!1,{})},updateImageHead:function(a){return a}}}),b("runtime/html5/imagemeta",["runtime/html5/util"],function(a){var b;return b={parsers:{65505:[]},maxMetaDataSize:262144,parse:function(a,b){var c=this,d=new FileReader;d.onload=function(){b(!1,c._parse(this.result)),d=d.onload=d.onerror=null},d.onerror=function(a){b(a.message),d=d.onload=d.onerror=null},a=a.slice(0,c.maxMetaDataSize),d.readAsArrayBuffer(a.getSource())},_parse:function(a,c){if(!(a.byteLength<6)){var d,e,f,g,h=new DataView(a),i=2,j=h.byteLength-4,k=i,l={};if(65496===h.getUint16(0)){for(;j>i&&(d=h.getUint16(i),d>=65504&&65519>=d||65534===d)&&(e=h.getUint16(i+2)+2,!(i+e>h.byteLength));){if(f=b.parsers[d],!c&&f)for(g=0;g<f.length;g+=1)f[g].call(b,h,i,e,l);i+=e,k=i}k>6&&(l.imageHead=a.slice?a.slice(2,k):new Uint8Array(a).subarray(2,k))}return l}},updateImageHead:function(a,b){var c,d,e,f=this._parse(a,!0);return e=2,f.imageHead&&(e=2+f.imageHead.byteLength),d=a.slice?a.slice(e):new Uint8Array(a).subarray(e),c=new Uint8Array(b.byteLength+2+d.byteLength),c[0]=255,c[1]=216,c.set(new Uint8Array(b),2),c.set(new Uint8Array(d),b.byteLength+2),c.buffer}},a.parseMeta=function(){return b.parse.apply(b,arguments)},a.updateImageHead=function(){return b.updateImageHead.apply(b,arguments)},b}),b("runtime/html5/imagemeta/exif",["base","runtime/html5/imagemeta"],function(a,b){var c={};return c.ExifMap=function(){return this},c.ExifMap.prototype.map={Orientation:274},c.ExifMap.prototype.get=function(a){return this[a]||this[this.map[a]]},c.exifTagTypes={1:{getValue:function(a,b){return a.getUint8(b)},size:1},2:{getValue:function(a,b){return String.fromCharCode(a.getUint8(b))},size:1,ascii:!0},3:{getValue:function(a,b,c){return a.getUint16(b,c)},size:2},4:{getValue:function(a,b,c){return a.getUint32(b,c)},size:4},5:{getValue:function(a,b,c){return a.getUint32(b,c)/a.getUint32(b+4,c)},size:8},9:{getValue:function(a,b,c){return a.getInt32(b,c)},size:4},10:{getValue:function(a,b,c){return a.getInt32(b,c)/a.getInt32(b+4,c)},size:8}},c.exifTagTypes[7]=c.exifTagTypes[1],c.getExifValue=function(b,d,e,f,g,h){var i,j,k,l,m,n,o=c.exifTagTypes[f];if(!o)return a.log("Invalid Exif data: Invalid tag type."),void 0;if(i=o.size*g,j=i>4?d+b.getUint32(e+8,h):e+8,j+i>b.byteLength)return a.log("Invalid Exif data: Invalid data offset."),void 0;if(1===g)return o.getValue(b,j,h);for(k=[],l=0;g>l;l+=1)k[l]=o.getValue(b,j+l*o.size,h);if(o.ascii){for(m="",l=0;l<k.length&&(n=k[l],"\0"!==n);l+=1)m+=n;return m}return k},c.parseExifTag=function(a,b,d,e,f){var g=a.getUint16(d,e);f.exif[g]=c.getExifValue(a,b,d,a.getUint16(d+2,e),a.getUint32(d+4,e),e)},c.parseExifTags=function(b,c,d,e,f){var g,h,i;if(d+6>b.byteLength)return a.log("Invalid Exif data: Invalid directory offset."),void 0;if(g=b.getUint16(d,e),h=d+2+12*g,h+4>b.byteLength)return a.log("Invalid Exif data: Invalid directory size."),void 0;for(i=0;g>i;i+=1)this.parseExifTag(b,c,d+2+12*i,e,f);return b.getUint32(h,e)},c.parseExifData=function(b,d,e,f){var g,h,i=d+10;if(1165519206===b.getUint32(d+4)){if(i+8>b.byteLength)return a.log("Invalid Exif data: Invalid segment size."),void 0;if(0!==b.getUint16(d+8))return a.log("Invalid Exif data: Missing byte alignment offset."),void 0;switch(b.getUint16(i)){case 18761:g=!0;break;case 19789:g=!1;break;default:return a.log("Invalid Exif data: Invalid byte alignment marker."),void 0}if(42!==b.getUint16(i+2,g))return a.log("Invalid Exif data: Missing TIFF marker."),void 0;h=b.getUint32(i+4,g),f.exif=new c.ExifMap,h=c.parseExifTags(b,i,i+h,g,f)}},b.parsers[65505].push(c.parseExifData),c}),b("runtime/html5/jpegencoder",[],function(){function a(a){function b(a){for(var b=[16,11,10,16,24,40,51,61,12,12,14,19,26,58,60,55,14,13,16,24,40,57,69,56,14,17,22,29,51,87,80,62,18,22,37,56,68,109,103,77,24,35,55,64,81,104,113,92,49,64,78,87,103,121,120,101,72,92,95,98,112,100,103,99],c=0;64>c;c++){var d=y((b[c]*a+50)/100);1>d?d=1:d>255&&(d=255),z[P[c]]=d}for(var e=[17,18,24,47,99,99,99,99,18,21,26,66,99,99,99,99,24,26,56,99,99,99,99,99,47,66,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99],f=0;64>f;f++){var g=y((e[f]*a+50)/100);1>g?g=1:g>255&&(g=255),A[P[f]]=g}for(var h=[1,1.387039845,1.306562965,1.175875602,1,.785694958,.5411961,.275899379],i=0,j=0;8>j;j++)for(var k=0;8>k;k++)B[i]=1/(8*z[P[i]]*h[j]*h[k]),C[i]=1/(8*A[P[i]]*h[j]*h[k]),i++}function c(a,b){for(var c=0,d=0,e=new Array,f=1;16>=f;f++){for(var g=1;g<=a[f];g++)e[b[d]]=[],e[b[d]][0]=c,e[b[d]][1]=f,d++,c++;c*=2}return e}function d(){t=c(Q,R),u=c(U,V),v=c(S,T),w=c(W,X)}function e(){for(var a=1,b=2,c=1;15>=c;c++){for(var d=a;b>d;d++)E[32767+d]=c,D[32767+d]=[],D[32767+d][1]=c,D[32767+d][0]=d;for(var e=-(b-1);-a>=e;e++)E[32767+e]=c,D[32767+e]=[],D[32767+e][1]=c,D[32767+e][0]=b-1+e;a<<=1,b<<=1}}function f(){for(var a=0;256>a;a++)O[a]=19595*a,O[a+256>>0]=38470*a,O[a+512>>0]=7471*a+32768,O[a+768>>0]=-11059*a,O[a+1024>>0]=-21709*a,O[a+1280>>0]=32768*a+8421375,O[a+1536>>0]=-27439*a,O[a+1792>>0]=-5329*a}function g(a){for(var b=a[0],c=a[1]-1;c>=0;)b&1<<c&&(I|=1<<J),c--,J--,0>J&&(255==I?(h(255),h(0)):h(I),J=7,I=0)}function h(a){H.push(N[a])}function i(a){h(255&a>>8),h(255&a)}function j(a,b){var c,d,e,f,g,h,i,j,k,l=0,m=8,n=64;for(k=0;m>k;++k){c=a[l],d=a[l+1],e=a[l+2],f=a[l+3],g=a[l+4],h=a[l+5],i=a[l+6],j=a[l+7];var o=c+j,p=c-j,q=d+i,r=d-i,s=e+h,t=e-h,u=f+g,v=f-g,w=o+u,x=o-u,y=q+s,z=q-s;a[l]=w+y,a[l+4]=w-y;var A=.707106781*(z+x);a[l+2]=x+A,a[l+6]=x-A,w=v+t,y=t+r,z=r+p;var B=.382683433*(w-z),C=.5411961*w+B,D=1.306562965*z+B,E=.707106781*y,G=p+E,H=p-E;a[l+5]=H+C,a[l+3]=H-C,a[l+1]=G+D,a[l+7]=G-D,l+=8}for(l=0,k=0;m>k;++k){c=a[l],d=a[l+8],e=a[l+16],f=a[l+24],g=a[l+32],h=a[l+40],i=a[l+48],j=a[l+56];var I=c+j,J=c-j,K=d+i,L=d-i,M=e+h,N=e-h,O=f+g,P=f-g,Q=I+O,R=I-O,S=K+M,T=K-M;a[l]=Q+S,a[l+32]=Q-S;var U=.707106781*(T+R);a[l+16]=R+U,a[l+48]=R-U,Q=P+N,S=N+L,T=L+J;var V=.382683433*(Q-T),W=.5411961*Q+V,X=1.306562965*T+V,Y=.707106781*S,Z=J+Y,$=J-Y;a[l+40]=$+W,a[l+24]=$-W,a[l+8]=Z+X,a[l+56]=Z-X,l++}var _;for(k=0;n>k;++k)_=a[k]*b[k],F[k]=_>0?0|_+.5:0|_-.5;return F}function k(){i(65504),i(16),h(74),h(70),h(73),h(70),h(0),h(1),h(1),h(0),i(1),i(1),h(0),h(0)}function l(a,b){i(65472),i(17),h(8),i(b),i(a),h(3),h(1),h(17),h(0),h(2),h(17),h(1),h(3),h(17),h(1)}function m(){i(65499),i(132),h(0);for(var a=0;64>a;a++)h(z[a]);h(1);for(var b=0;64>b;b++)h(A[b])}function n(){i(65476),i(418),h(0);for(var a=0;16>a;a++)h(Q[a+1]);for(var b=0;11>=b;b++)h(R[b]);h(16);for(var c=0;16>c;c++)h(S[c+1]);for(var d=0;161>=d;d++)h(T[d]);h(1);for(var e=0;16>e;e++)h(U[e+1]);for(var f=0;11>=f;f++)h(V[f]);h(17);for(var g=0;16>g;g++)h(W[g+1]);for(var j=0;161>=j;j++)h(X[j])}function o(){i(65498),i(12),h(3),h(1),h(0),h(2),h(17),h(3),h(17),h(0),h(63),h(0)}function p(a,b,c,d,e){for(var f,h=e[0],i=e[240],k=16,l=63,m=64,n=j(a,b),o=0;m>o;++o)G[P[o]]=n[o];var p=G[0]-c;c=G[0],0==p?g(d[0]):(f=32767+p,g(d[E[f]]),g(D[f]));for(var q=63;q>0&&0==G[q];q--);if(0==q)return g(h),c;for(var r,s=1;q>=s;){for(var t=s;0==G[s]&&q>=s;++s);var u=s-t;if(u>=k){r=u>>4;for(var v=1;r>=v;++v)g(i);u=15&u}f=32767+G[s],g(e[(u<<4)+E[f]]),g(D[f]),s++}return q!=l&&g(h),c}function q(){for(var a=String.fromCharCode,b=0;256>b;b++)N[b]=a(b)}function r(a){if(0>=a&&(a=1),a>100&&(a=100),x!=a){var c=0;c=50>a?Math.floor(5e3/a):Math.floor(200-2*a),b(c),x=a}}function s(){a||(a=50),q(),d(),e(),f(),r(a)}Math.round;var t,u,v,w,x,y=Math.floor,z=new Array(64),A=new Array(64),B=new Array(64),C=new Array(64),D=new Array(65535),E=new Array(65535),F=new Array(64),G=new Array(64),H=[],I=0,J=7,K=new Array(64),L=new Array(64),M=new Array(64),N=new Array(256),O=new Array(2048),P=[0,1,5,6,14,15,27,28,2,4,7,13,16,26,29,42,3,8,12,17,25,30,41,43,9,11,18,24,31,40,44,53,10,19,23,32,39,45,52,54,20,22,33,38,46,51,55,60,21,34,37,47,50,56,59,61,35,36,48,49,57,58,62,63],Q=[0,0,1,5,1,1,1,1,1,1,0,0,0,0,0,0,0],R=[0,1,2,3,4,5,6,7,8,9,10,11],S=[0,0,2,1,3,3,2,4,3,5,5,4,4,0,0,1,125],T=[1,2,3,0,4,17,5,18,33,49,65,6,19,81,97,7,34,113,20,50,129,145,161,8,35,66,177,193,21,82,209,240,36,51,98,114,130,9,10,22,23,24,25,26,37,38,39,40,41,42,52,53,54,55,56,57,58,67,68,69,70,71,72,73,74,83,84,85,86,87,88,89,90,99,100,101,102,103,104,105,106,115,116,117,118,119,120,121,122,131,132,133,134,135,136,137,138,146,147,148,149,150,151,152,153,154,162,163,164,165,166,167,168,169,170,178,179,180,181,182,183,184,185,186,194,195,196,197,198,199,200,201,202,210,211,212,213,214,215,216,217,218,225,226,227,228,229,230,231,232,233,234,241,242,243,244,245,246,247,248,249,250],U=[0,0,3,1,1,1,1,1,1,1,1,1,0,0,0,0,0],V=[0,1,2,3,4,5,6,7,8,9,10,11],W=[0,0,2,1,2,4,4,3,4,7,5,4,4,0,1,2,119],X=[0,1,2,3,17,4,5,33,49,6,18,65,81,7,97,113,19,34,50,129,8,20,66,145,161,177,193,9,35,51,82,240,21,98,114,209,10,22,36,52,225,37,241,23,24,25,26,38,39,40,41,42,53,54,55,56,57,58,67,68,69,70,71,72,73,74,83,84,85,86,87,88,89,90,99,100,101,102,103,104,105,106,115,116,117,118,119,120,121,122,130,131,132,133,134,135,136,137,138,146,147,148,149,150,151,152,153,154,162,163,164,165,166,167,168,169,170,178,179,180,181,182,183,184,185,186,194,195,196,197,198,199,200,201,202,210,211,212,213,214,215,216,217,218,226,227,228,229,230,231,232,233,234,242,243,244,245,246,247,248,249,250];this.encode=function(a,b){b&&r(b),H=new Array,I=0,J=7,i(65496),k(),m(),l(a.width,a.height),n(),o();var c=0,d=0,e=0;I=0,J=7,this.encode.displayName="_encode_";for(var f,h,j,q,s,x,y,z,A,D=a.data,E=a.width,F=a.height,G=4*E,N=0;F>N;){for(f=0;G>f;){for(s=G*N+f,x=s,y=-1,z=0,A=0;64>A;A++)z=A>>3,y=4*(7&A),x=s+z*G+y,N+z>=F&&(x-=G*(N+1+z-F)),f+y>=G&&(x-=f+y-G+4),h=D[x++],j=D[x++],q=D[x++],K[A]=(O[h]+O[j+256>>0]+O[q+512>>0]>>16)-128,L[A]=(O[h+768>>0]+O[j+1024>>0]+O[q+1280>>0]>>16)-128,M[A]=(O[h+1280>>0]+O[j+1536>>0]+O[q+1792>>0]>>16)-128;c=p(K,B,c,t,v),d=p(L,C,d,u,w),e=p(M,C,e,u,w),f+=32}N+=8}if(J>=0){var P=[];P[1]=J+1,P[0]=(1<<J+1)-1,g(P)}i(65497);var Q="data:image/jpeg;base64,"+btoa(H.join(""));return H=[],Q},s()}return a.encode=function(b,c){var d=new a(c);return d.encode(b)},a}),b("runtime/html5/androidpatch",["runtime/html5/util","runtime/html5/jpegencoder","base"],function(a,b,c){var d,e=a.canvasToDataUrl;a.canvasToDataUrl=function(a,f,g){var h,i,j,k,l;return c.os.android?("image/jpeg"===f&&"undefined"==typeof d&&(k=e.apply(null,arguments),l=k.split(","),k=~l[0].indexOf("base64")?atob(l[1]):decodeURIComponent(l[1]),k=k.substring(0,2),d=255===k.charCodeAt(0)&&216===k.charCodeAt(1)),"image/jpeg"!==f||d?e.apply(null,arguments):(i=a.width,j=a.height,h=a.getContext("2d"),b.encode(h.getImageData(0,0,i,j),g))):e.apply(null,arguments)}}),b("runtime/html5/image",["base","runtime/html5/runtime","runtime/html5/util"],function(a,b,c){var d="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs%3D";return b.register("Image",{modified:!1,init:function(){var a=this,b=new Image;b.onload=function(){a._info={type:a.type,width:this.width,height:this.height},a._metas||"image/jpeg"!==a.type?a.owner.trigger("load"):c.parseMeta(a._blob,function(b,c){a._metas=c,a.owner.trigger("load")})},b.onerror=function(){a.owner.trigger("error")},a._img=b},loadFromBlob:function(a){var b=this,d=b._img;b._blob=a,b.type=a.type,d.src=c.createObjectURL(a.getSource()),b.owner.once("load",function(){c.revokeObjectURL(d.src)})},resize:function(a,b){var c=this._canvas||(this._canvas=document.createElement("canvas"));this._resize(this._img,c,a,b),this._blob=null,this.modified=!0,this.owner.trigger("complete","resize")},crop:function(a,b,c,d,e){var f=this._canvas||(this._canvas=document.createElement("canvas")),g=this.options,h=this._img,i=h.naturalWidth,j=h.naturalHeight,k=this.getOrientation();e=e||1,f.width=c,f.height=d,g.preserveHeaders||this._rotate2Orientaion(f,k),this._renderImageToCanvas(f,h,-a,-b,i*e,j*e),this._blob=null,this.modified=!0,this.owner.trigger("complete","crop")},getAsBlob:function(a){var b,d=this._blob,e=this.options;if(a=a||this.type,this.modified||this.type!==a){if(b=this._canvas,"image/jpeg"===a){if(d=c.canvasToDataUrl(b,a,e.quality),e.preserveHeaders&&this._metas&&this._metas.imageHead)return d=c.dataURL2ArrayBuffer(d),d=c.updateImageHead(d,this._metas.imageHead),d=c.arrayBufferToBlob(d,a)}else d=c.canvasToDataUrl(b,a);d=c.dataURL2Blob(d)}return d},getAsDataUrl:function(a){var b=this.options;return a=a||this.type,"image/jpeg"===a?c.canvasToDataUrl(this._canvas,a,b.quality):this._canvas.toDataURL(a)},getOrientation:function(){return this._metas&&this._metas.exif&&this._metas.exif.get("Orientation")||1},info:function(a){return a?(this._info=a,this):this._info},meta:function(a){return a?(this._metas=a,this):this._metas},destroy:function(){var a=this._canvas;this._img.onload=null,a&&(a.getContext("2d").clearRect(0,0,a.width,a.height),a.width=a.height=0,this._canvas=null),this._img.src=d,this._img=this._blob=null},_resize:function(a,b,c,d){var e,f,g,h,i,j=this.options,k=a.width,l=a.height,m=this.getOrientation();~[5,6,7,8].indexOf(m)&&(c^=d,d^=c,c^=d),e=Math[j.crop?"max":"min"](c/k,d/l),j.allowMagnify||(e=Math.min(1,e)),f=k*e,g=l*e,j.crop?(b.width=c,b.height=d):(b.width=f,b.height=g),h=(b.width-f)/2,i=(b.height-g)/2,j.preserveHeaders||this._rotate2Orientaion(b,m),this._renderImageToCanvas(b,a,h,i,f,g)},_rotate2Orientaion:function(a,b){var c=a.width,d=a.height,e=a.getContext("2d");switch(b){case 5:case 6:case 7:case 8:a.width=d,a.height=c}switch(b){case 2:e.translate(c,0),e.scale(-1,1);break;case 3:e.translate(c,d),e.rotate(Math.PI);break;case 4:e.translate(0,d),e.scale(1,-1);break;case 5:e.rotate(.5*Math.PI),e.scale(1,-1);break;case 6:e.rotate(.5*Math.PI),e.translate(0,-d);break;case 7:e.rotate(.5*Math.PI),e.translate(c,-d),e.scale(-1,1);break;case 8:e.rotate(-.5*Math.PI),e.translate(-c,0)}},_renderImageToCanvas:function(){function b(a,b,c){var d,e,f,g=document.createElement("canvas"),h=g.getContext("2d"),i=0,j=c,k=c;for(g.width=1,g.height=c,h.drawImage(a,0,0),d=h.getImageData(0,0,1,c).data;k>i;)e=d[4*(k-1)+3],0===e?j=k:i=k,k=j+i>>1;return f=k/c,0===f?1:f}function c(a){var b,c,d=a.naturalWidth,e=a.naturalHeight;return d*e>1048576?(b=document.createElement("canvas"),b.width=b.height=1,c=b.getContext("2d"),c.drawImage(a,-d+1,0),0===c.getImageData(0,0,1,1).data[3]):!1}return a.os.ios?a.os.ios>=7?function(a,c,d,e,f,g){var h=c.naturalWidth,i=c.naturalHeight,j=b(c,h,i);return a.getContext("2d").drawImage(c,0,0,h*j,i*j,d,e,f,g)}:function(a,d,e,f,g,h){var i,j,k,l,m,n,o,p=d.naturalWidth,q=d.naturalHeight,r=a.getContext("2d"),s=c(d),t="image/jpeg"===this.type,u=1024,v=0,w=0;for(s&&(p/=2,q/=2),r.save(),i=document.createElement("canvas"),i.width=i.height=u,j=i.getContext("2d"),k=t?b(d,p,q):1,l=Math.ceil(u*g/p),m=Math.ceil(u*h/q/k);q>v;){for(n=0,o=0;p>n;)j.clearRect(0,0,u,u),j.drawImage(d,-n,-v),r.drawImage(i,0,0,u,u,e+o,f+w,l,m),n+=u,o+=l;v+=u,w+=m}r.restore(),i=j=null}:function(b){var c=a.slice(arguments,1),d=b.getContext("2d");d.drawImage.apply(d,c)}}()})}),b("runtime/html5/transport",["base","runtime/html5/runtime"],function(a,b){var c=a.noop,d=a.$;return b.register("Transport",{init:function(){this._status=0,this._response=null},send:function(){var b,c,e,f=this.owner,g=this.options,h=this._initAjax(),i=f._blob,j=g.server;g.sendAsBinary?(j+=(/\?/.test(j)?"&":"?")+d.param(f._formData),c=i.getSource()):(b=new FormData,d.each(f._formData,function(a,c){b.append(a,c)}),b.append(g.fileVal,i.getSource(),g.filename||f._formData.name||"")),g.withCredentials&&"withCredentials"in h?(h.open(g.method,j,!0),h.withCredentials=!0):h.open(g.method,j),this._setRequestHeader(h,g.headers),c?(h.overrideMimeType&&h.overrideMimeType("application/octet-stream"),a.os.android?(e=new FileReader,e.onload=function(){h.send(this.result),e=e.onload=null},e.readAsArrayBuffer(c)):h.send(c)):h.send(b)},getResponse:function(){return this._response},getResponseAsJson:function(){return this._parseJson(this._response)},getStatus:function(){return this._status},abort:function(){var a=this._xhr;a&&(a.upload.onprogress=c,a.onreadystatechange=c,a.abort(),this._xhr=a=null)},destroy:function(){this.abort()},_initAjax:function(){var a=this,b=new XMLHttpRequest,d=this.options;return!d.withCredentials||"withCredentials"in b||"undefined"==typeof XDomainRequest||(b=new XDomainRequest),b.upload.onprogress=function(b){var c=0;return b.lengthComputable&&(c=b.loaded/b.total),a.trigger("progress",c)},b.onreadystatechange=function(){return 4===b.readyState?(b.upload.onprogress=c,b.onreadystatechange=c,a._xhr=null,a._status=b.status,b.status>=200&&b.status<300?(a._response=b.responseText,a.trigger("load")):b.status>=500&&b.status<600?(a._response=b.responseText,a.trigger("error","server-"+status)):a.trigger("error",a._status?"http-"+status:"abort")):void 0},a._xhr=b,b},_setRequestHeader:function(a,b){d.each(b,function(b,c){a.setRequestHeader(b,c)})},_parseJson:function(a){var b;try{b=JSON.parse(a)}catch(c){b={}}return b}})}),b("runtime/html5/md5",["runtime/html5/runtime"],function(a){var b=function(a,b){return 4294967295&a+b},c=function(a,c,d,e,f,g){return c=b(b(c,a),b(e,g)),b(c<<f|c>>>32-f,d)},d=function(a,b,d,e,f,g,h){return c(b&d|~b&e,a,b,f,g,h)},e=function(a,b,d,e,f,g,h){return c(b&e|d&~e,a,b,f,g,h)},f=function(a,b,d,e,f,g,h){return c(b^d^e,a,b,f,g,h)},g=function(a,b,d,e,f,g,h){return c(d^(b|~e),a,b,f,g,h)},h=function(a,c){var h=a[0],i=a[1],j=a[2],k=a[3];h=d(h,i,j,k,c[0],7,-680876936),k=d(k,h,i,j,c[1],12,-389564586),j=d(j,k,h,i,c[2],17,606105819),i=d(i,j,k,h,c[3],22,-1044525330),h=d(h,i,j,k,c[4],7,-176418897),k=d(k,h,i,j,c[5],12,1200080426),j=d(j,k,h,i,c[6],17,-1473231341),i=d(i,j,k,h,c[7],22,-45705983),h=d(h,i,j,k,c[8],7,1770035416),k=d(k,h,i,j,c[9],12,-1958414417),j=d(j,k,h,i,c[10],17,-42063),i=d(i,j,k,h,c[11],22,-1990404162),h=d(h,i,j,k,c[12],7,1804603682),k=d(k,h,i,j,c[13],12,-40341101),j=d(j,k,h,i,c[14],17,-1502002290),i=d(i,j,k,h,c[15],22,1236535329),h=e(h,i,j,k,c[1],5,-165796510),k=e(k,h,i,j,c[6],9,-1069501632),j=e(j,k,h,i,c[11],14,643717713),i=e(i,j,k,h,c[0],20,-373897302),h=e(h,i,j,k,c[5],5,-701558691),k=e(k,h,i,j,c[10],9,38016083),j=e(j,k,h,i,c[15],14,-660478335),i=e(i,j,k,h,c[4],20,-405537848),h=e(h,i,j,k,c[9],5,568446438),k=e(k,h,i,j,c[14],9,-1019803690),j=e(j,k,h,i,c[3],14,-187363961),i=e(i,j,k,h,c[8],20,1163531501),h=e(h,i,j,k,c[13],5,-1444681467),k=e(k,h,i,j,c[2],9,-51403784),j=e(j,k,h,i,c[7],14,1735328473),i=e(i,j,k,h,c[12],20,-1926607734),h=f(h,i,j,k,c[5],4,-378558),k=f(k,h,i,j,c[8],11,-2022574463),j=f(j,k,h,i,c[11],16,1839030562),i=f(i,j,k,h,c[14],23,-35309556),h=f(h,i,j,k,c[1],4,-1530992060),k=f(k,h,i,j,c[4],11,1272893353),j=f(j,k,h,i,c[7],16,-155497632),i=f(i,j,k,h,c[10],23,-1094730640),h=f(h,i,j,k,c[13],4,681279174),k=f(k,h,i,j,c[0],11,-358537222),j=f(j,k,h,i,c[3],16,-722521979),i=f(i,j,k,h,c[6],23,76029189),h=f(h,i,j,k,c[9],4,-640364487),k=f(k,h,i,j,c[12],11,-421815835),j=f(j,k,h,i,c[15],16,530742520),i=f(i,j,k,h,c[2],23,-995338651),h=g(h,i,j,k,c[0],6,-198630844),k=g(k,h,i,j,c[7],10,1126891415),j=g(j,k,h,i,c[14],15,-1416354905),i=g(i,j,k,h,c[5],21,-57434055),h=g(h,i,j,k,c[12],6,1700485571),k=g(k,h,i,j,c[3],10,-1894986606),j=g(j,k,h,i,c[10],15,-1051523),i=g(i,j,k,h,c[1],21,-2054922799),h=g(h,i,j,k,c[8],6,1873313359),k=g(k,h,i,j,c[15],10,-30611744),j=g(j,k,h,i,c[6],15,-1560198380),i=g(i,j,k,h,c[13],21,1309151649),h=g(h,i,j,k,c[4],6,-145523070),k=g(k,h,i,j,c[11],10,-1120210379),j=g(j,k,h,i,c[2],15,718787259),i=g(i,j,k,h,c[9],21,-343485551),a[0]=b(h,a[0]),a[1]=b(i,a[1]),a[2]=b(j,a[2]),a[3]=b(k,a[3])},i=function(a){var b,c=[];for(b=0;64>b;b+=4)c[b>>2]=a.charCodeAt(b)+(a.charCodeAt(b+1)<<8)+(a.charCodeAt(b+2)<<16)+(a.charCodeAt(b+3)<<24);return c},j=function(a){var b,c=[];for(b=0;64>b;b+=4)c[b>>2]=a[b]+(a[b+1]<<8)+(a[b+2]<<16)+(a[b+3]<<24);return c},k=function(a){var b,c,d,e,f,g,j=a.length,k=[1732584193,-271733879,-1732584194,271733878];for(b=64;j>=b;b+=64)h(k,i(a.substring(b-64,b)));for(a=a.substring(b-64),c=a.length,d=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],b=0;c>b;b+=1)d[b>>2]|=a.charCodeAt(b)<<(b%4<<3);if(d[b>>2]|=128<<(b%4<<3),b>55)for(h(k,d),b=0;16>b;b+=1)d[b]=0;return e=8*j,e=e.toString(16).match(/(.*?)(.{0,8})$/),f=parseInt(e[2],16),g=parseInt(e[1],16)||0,d[14]=f,d[15]=g,h(k,d),k},l=function(a){var b,c,d,e,f,g,i=a.length,k=[1732584193,-271733879,-1732584194,271733878];for(b=64;i>=b;b+=64)h(k,j(a.subarray(b-64,b)));for(a=i>b-64?a.subarray(b-64):new Uint8Array(0),c=a.length,d=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],b=0;c>b;b+=1)d[b>>2]|=a[b]<<(b%4<<3);if(d[b>>2]|=128<<(b%4<<3),b>55)for(h(k,d),b=0;16>b;b+=1)d[b]=0;return e=8*i,e=e.toString(16).match(/(.*?)(.{0,8})$/),f=parseInt(e[2],16),g=parseInt(e[1],16)||0,d[14]=f,d[15]=g,h(k,d),k},m=["0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"],n=function(a){var b,c="";for(b=0;4>b;b+=1)c+=m[15&a>>8*b+4]+m[15&a>>8*b];return c},o=function(a){var b;for(b=0;b<a.length;b+=1)a[b]=n(a[b]);return a.join("")},p=function(a){return o(k(a))},q=function(){this.reset()};return"5d41402abc4b2a76b9719d911017c592"!==p("hello")&&(b=function(a,b){var c=(65535&a)+(65535&b),d=(a>>16)+(b>>16)+(c>>16);return d<<16|65535&c}),q.prototype.append=function(a){return/[\u0080-\uFFFF]/.test(a)&&(a=unescape(encodeURIComponent(a))),this.appendBinary(a),this},q.prototype.appendBinary=function(a){this._buff+=a,this._length+=a.length;var b,c=this._buff.length;for(b=64;c>=b;b+=64)h(this._state,i(this._buff.substring(b-64,b)));return this._buff=this._buff.substr(b-64),this},q.prototype.end=function(a){var b,c,d=this._buff,e=d.length,f=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];for(b=0;e>b;b+=1)f[b>>2]|=d.charCodeAt(b)<<(b%4<<3);return this._finish(f,e),c=a?this._state:o(this._state),this.reset(),c},q.prototype._finish=function(a,b){var c,d,e,f=b;if(a[f>>2]|=128<<(f%4<<3),f>55)for(h(this._state,a),f=0;16>f;f+=1)a[f]=0;c=8*this._length,c=c.toString(16).match(/(.*?)(.{0,8})$/),d=parseInt(c[2],16),e=parseInt(c[1],16)||0,a[14]=d,a[15]=e,h(this._state,a)},q.prototype.reset=function(){return this._buff="",this._length=0,this._state=[1732584193,-271733879,-1732584194,271733878],this},q.prototype.destroy=function(){delete this._state,delete this._buff,delete this._length},q.hash=function(a,b){/[\u0080-\uFFFF]/.test(a)&&(a=unescape(encodeURIComponent(a)));
var c=k(a);return b?c:o(c)},q.hashBinary=function(a,b){var c=k(a);return b?c:o(c)},q.ArrayBuffer=function(){this.reset()},q.ArrayBuffer.prototype.append=function(a){var b,c=this._concatArrayBuffer(this._buff,a),d=c.length;for(this._length+=a.byteLength,b=64;d>=b;b+=64)h(this._state,j(c.subarray(b-64,b)));return this._buff=d>b-64?c.subarray(b-64):new Uint8Array(0),this},q.ArrayBuffer.prototype.end=function(a){var b,c,d=this._buff,e=d.length,f=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];for(b=0;e>b;b+=1)f[b>>2]|=d[b]<<(b%4<<3);return this._finish(f,e),c=a?this._state:o(this._state),this.reset(),c},q.ArrayBuffer.prototype._finish=q.prototype._finish,q.ArrayBuffer.prototype.reset=function(){return this._buff=new Uint8Array(0),this._length=0,this._state=[1732584193,-271733879,-1732584194,271733878],this},q.ArrayBuffer.prototype.destroy=q.prototype.destroy,q.ArrayBuffer.prototype._concatArrayBuffer=function(a,b){var c=a.length,d=new Uint8Array(c+b.byteLength);return d.set(a),d.set(new Uint8Array(b),c),d},q.ArrayBuffer.hash=function(a,b){var c=l(new Uint8Array(a));return b?c:o(c)},a.register("Md5",{init:function(){},loadFromBlob:function(a){var b,c,d=a.getSource(),e=2097152,f=Math.ceil(d.size/e),g=0,h=this.owner,i=new q.ArrayBuffer,j=this,k=d.mozSlice||d.webkitSlice||d.slice;c=new FileReader,b=function(){var l,m;l=g*e,m=Math.min(l+e,d.size),c.onload=function(b){i.append(b.target.result),h.trigger("progress",{total:a.size,loaded:m})},c.onloadend=function(){c.onloadend=c.onload=null,++g<f?setTimeout(b,1):setTimeout(function(){h.trigger("load"),j.result=i.end(),b=a=d=i=null,h.trigger("complete")},50)},c.readAsArrayBuffer(k.call(d,l,m))},b()},getResult:function(){return this.result}})}),b("runtime/flash/runtime",["base","runtime/runtime","runtime/compbase"],function(b,c,d){function e(){var a;try{a=navigator.plugins["Shockwave Flash"],a=a.description}catch(b){try{a=new ActiveXObject("ShockwaveFlash.ShockwaveFlash").GetVariable("$version")}catch(c){a="0.0"}}return a=a.match(/\d+/g),parseFloat(a[0]+"."+a[1],10)}function f(){function d(a,b){var c,d,e=a.type||a;c=e.split("::"),d=c[0],e=c[1],"Ready"===e&&d===j.uid?j.trigger("ready"):f[d]&&f[d].trigger(e.toLowerCase(),a,b)}var e={},f={},g=this.destroy,j=this,k=b.guid("webuploader_");c.apply(j,arguments),j.type=h,j.exec=function(a,c){var d,g=this,h=g.uid,k=b.slice(arguments,2);return f[h]=g,i[a]&&(e[h]||(e[h]=new i[a](g,j)),d=e[h],d[c])?d[c].apply(d,k):j.flashExec.apply(g,arguments)},a[k]=function(){var a=arguments;setTimeout(function(){d.apply(null,a)},1)},this.jsreciver=k,this.destroy=function(){return g&&g.apply(this,arguments)},this.flashExec=function(a,c){var d=j.getFlash(),e=b.slice(arguments,2);return d.exec(this.uid,a,c,e)}}var g=b.$,h="flash",i={};return b.inherits(c,{constructor:f,init:function(){var a,c=this.getContainer(),d=this.options;c.css({position:"absolute",top:"-8px",left:"-8px",width:"9px",height:"9px",overflow:"hidden"}),a='<object id="'+this.uid+'" type="application/'+'x-shockwave-flash" data="'+d.swf+'" ',b.browser.ie&&(a+='classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" '),a+='width="100%" height="100%" style="outline:0"><param name="movie" value="'+d.swf+'" />'+'<param name="flashvars" value="uid='+this.uid+"&jsreciver="+this.jsreciver+'" />'+'<param name="wmode" value="transparent" />'+'<param name="allowscriptaccess" value="always" />'+"</object>",c.html(a)},getFlash:function(){return this._flash?this._flash:(this._flash=g("#"+this.uid).get(0),this._flash)}}),f.register=function(a,c){return c=i[a]=b.inherits(d,g.extend({flashExec:function(){var a=this.owner,b=this.getRuntime();return b.flashExec.apply(a,arguments)}},c))},e()>=11.4&&c.addRuntime(h,f),f}),b("runtime/flash/filepicker",["base","runtime/flash/runtime"],function(a,b){var c=a.$;return b.register("FilePicker",{init:function(a){var b,d,e=c.extend({},a);for(b=e.accept&&e.accept.length,d=0;b>d;d++)e.accept[d].title||(e.accept[d].title="Files");delete e.button,delete e.id,delete e.container,this.flashExec("FilePicker","init",e)},destroy:function(){this.flashExec("FilePicker","destroy")}})}),b("runtime/flash/image",["runtime/flash/runtime"],function(a){return a.register("Image",{loadFromBlob:function(a){var b=this.owner;b.info()&&this.flashExec("Image","info",b.info()),b.meta()&&this.flashExec("Image","meta",b.meta()),this.flashExec("Image","loadFromBlob",a.uid)}})}),b("runtime/flash/transport",["base","runtime/flash/runtime","runtime/client"],function(b,c,d){var e=b.$;return c.register("Transport",{init:function(){this._status=0,this._response=null,this._responseJson=null},send:function(){var a,b=this.owner,c=this.options,d=this._initAjax(),f=b._blob,g=c.server;d.connectRuntime(f.ruid),c.sendAsBinary?(g+=(/\?/.test(g)?"&":"?")+e.param(b._formData),a=f.uid):(e.each(b._formData,function(a,b){d.exec("append",a,b)}),d.exec("appendBlob",c.fileVal,f.uid,c.filename||b._formData.name||"")),this._setRequestHeader(d,c.headers),d.exec("send",{method:c.method,url:g,forceURLStream:c.forceURLStream,mimeType:"application/octet-stream"},a)},getStatus:function(){return this._status},getResponse:function(){return this._response||""},getResponseAsJson:function(){return this._responseJson},abort:function(){var a=this._xhr;a&&(a.exec("abort"),a.destroy(),this._xhr=a=null)},destroy:function(){this.abort()},_initAjax:function(){var b=this,c=new d("XMLHttpRequest");return c.on("uploadprogress progress",function(a){var c=a.loaded/a.total;return c=Math.min(1,Math.max(0,c)),b.trigger("progress",c)}),c.on("load",function(){var d,e=c.exec("getStatus"),f=!1,g="";return c.off(),b._xhr=null,e>=200&&300>e?f=!0:e>=500&&600>e?(f=!0,g="server-"+e):g="http-"+e,f&&(b._response=c.exec("getResponse"),b._response=decodeURIComponent(b._response),d=function(b){try{return a.JSON&&a.JSON.parse?JSON.parse(b):new Function("return "+b).call()}catch(c){return{}}},b._responseJson=b._response?d(b._response):{}),c.destroy(),c=null,g?b.trigger("error",g):b.trigger("load")}),c.on("error",function(){var a=c.exec("getStatus"),d=a?"http-"+a:"http";c.off(),b._xhr=null,b.trigger("error",d)}),b._xhr=c,c},_setRequestHeader:function(a,b){e.each(b,function(b,c){a.exec("setRequestHeader",b,c)})}})}),b("runtime/flash/blob",["runtime/flash/runtime","lib/blob"],function(a,b){return a.register("Blob",{slice:function(a,c){var d=this.flashExec("Blob","slice",a,c);return new b(this.getRuid(),d)}})}),b("runtime/flash/md5",["runtime/flash/runtime"],function(a){return a.register("Md5",{init:function(){},loadFromBlob:function(a){return this.flashExec("Md5","loadFromBlob",a.uid)}})}),b("preset/all",["base","widgets/filednd","widgets/filepaste","widgets/filepicker","widgets/image","widgets/queue","widgets/runtime","widgets/upload","widgets/validator","widgets/md5","runtime/html5/blob","runtime/html5/dnd","runtime/html5/filepaste","runtime/html5/filepicker","runtime/html5/imagemeta/exif","runtime/html5/androidpatch","runtime/html5/image","runtime/html5/transport","runtime/html5/md5","runtime/flash/filepicker","runtime/flash/image","runtime/flash/transport","runtime/flash/blob","runtime/flash/md5"],function(a){return a}),b("widgets/log",["base","uploader","widgets/widget"],function(a,b){function c(a){var b=e.extend({},d,a),c=f.replace(/^(.*)\?/,"$1"+e.param(b)),g=new Image;g.src=c}var d,e=a.$,f=" http://static.tieba.baidu.com/tb/pms/img/st.gif??",g=(location.hostname||location.host||"protected").toLowerCase(),h=g&&/baidu/i.exec(g);if(h)return d={dv:3,master:"webuploader",online:/test/.exec(g)?0:1,module:"",product:g,type:0},b.register({name:"log",init:function(){var a=this.owner,b=0,d=0;a.on("error",function(a){c({type:2,c_error_code:a})}).on("uploadError",function(a,b){c({type:2,c_error_code:"UPLOAD_ERROR",c_reason:""+b})}).on("uploadComplete",function(a){b++,d+=a.size}).on("uploadFinished",function(){c({c_count:b,c_size:d}),b=d=0}),c({c_usage:1})}})}),b("webuploader",["preset/all","widgets/log"],function(a){return a}),c("webuploader")});
/**
 * 文件上传、图片上传控件，基于WebUploader扩展
 * @author bob 2016-09-12
 */
(function($){
	var DEFAULT_CHUNK_SIZE = 5 * 1024 * 1024;
	//生成uuid
	var genUuid = function(len, radix) {
		  var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
		  var uuid = [], i;
		  radix = radix || chars.length;
		 
		  if (len) {
		   // Compact form
		   for (i = 0; i < len; i++) uuid[i] = chars[0 | Math.random()*radix];
		  } else {
		   // rfc4122, version 4 form
		   var r;
		 
		   // rfc4122 requires these characters
		   uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
		   uuid[14] = '4';
		 
		   // Fill in random data. At i==19 set the high bits of clock sequence as
		   // per rfc4122, sec. 4.1.5
		   for (i = 0; i < 36; i++) {
		    if (!uuid[i]) {
		     r = 0 | Math.random()*16;
		     uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
		    }
		   }
		  }
		 
		  return uuid.join('');
	};
	
	var LANG = {
		"zh_CN":{
			pickerTitle:"选择文件",
			uploadTitle:"开始上传",
			attentionTitle:"[注：添加附件时，可用Ctrl或Shift+鼠标左键来选择多个文件]",
			waitForUploadTitle:"等待上传",
			uploadedTitle:"已上传成功",
			uploadErrorTitle:"上传失败："
		},
		"zh_TW":{
			pickerTitle:"選擇檔案",
			uploadTitle:"開始上傳",
			attentionTitle:"[注：添加附件時，可用Ctrl或Shift+滑鼠左鍵來選擇多個檔案]",
			waitForUploadTitle:"等待上傳",
			uploadedTitle:"已上傳成功",
			uploadErrorTitle:"上傳失敗："
		},
		"en":{
			pickerTitle:"Select file",
			uploadTitle:"Start upload",
			attentionTitle:"[Note: add attachments, use Ctrl or Shift+ the left mouse button to select multiple files]",
			waitForUploadTitle: "Waiting for upload",
			uploadTitle: "Uploaded successfully",
			uploadErrorTitle: "Upload failed:"
		}
	};
	
	var getUploadTpl = function(uuid, opt) {
		var lang = opt.language;
		//文件上传模板
		var uploadTpl = [
			'<div class="file_upload_div" style="display:', opt.readonly?'none':'block',';">','<div class="es_file_upload_button">',
			'<div id="file_picker_', uuid, '" >', LANG[lang].pickerTitle, '</div>',
			'<button class="webuploader-pick es_file_upload_bt_1" id="file_upload_', uuid, '"',
			'  type="button">',  LANG[lang].uploadTitle, '</button>',
			'<span class="es_file_upload_bt_2">',LANG[lang].attentionTitle ,'</span>',
			'</div>',
			'<div id="file_queue_', uuid, '" class="es_file_upload_queue"></div>','</div>'
		];
		return uploadTpl.join("");
	};
	
	
	//初始化控件
	var init = function(jq, opt) {
		var opts = getCommonOpt(jq, opt);
		var uuid = genUuid(8, 16);
		$(jq).html(getUploadTpl(uuid, opts));
		var uploader = WebUploader.create({
			// swf文件路径
			swf:opts.swfUrl,
			// 文件接收服务端。
			server:opts.uploadUrl,
			// 选择文件的按钮。可选。
			// 内部根据当前运行是创建，可能是input元素，也可能是flash.
			pick : {"id":'#file_picker_' + uuid,"multiple":opts.multiple},
			chunked: opts.chunked,  //分片处理
            chunkSize: DEFAULT_CHUNK_SIZE, //每片5M  
            threads:3,//上传并发数。允许同时最大上传进程数。
			// 不压缩image, 默认如果是jpeg，文件上传前会压缩一把再上传！
			resize : false,
			sendAsBinary:true,
			fileNumLimit:opts.maxFileNum,
			fileSizeLimit:opts.maxFileSize,
			fileSingleSizeLimit:opts.maxSingleFileSize
		});

		var lang = LANG[opts.language];
		var uuidMap = {};
		// 当有文件被添加进队列的时候
		uploader.on('fileQueued', function(file) {
			var val = $.fn.filebox.methods["getValue"](jq);
		    if(!val) {
		    	val = getAttachId(opts);
		    	$.fn.filebox.methods["setValue"](jq, val);
		    	if(opts["onFirstSelect"]) opts["onFirstSelect"].call(jq, val);
		    }
			$("#file_queue_" + uuid).append(
					['<div id="file_item_' , file.id , '" class="es_file_item">'
					        , '<div style="width:80%;display:inline-block;">', '<div>'
							, '<span class="info">' , file.name , '&nbsp;&nbsp;</span>'
							, '<span class="state">' , lang["waitForUploadTitle"] , '</span>'
							, '</div>'
							, '<div class="es-progressbar" style="width:92%;"></div>'
							, '</div>', '<div style="display:inline-block;"><a href="javascript:;" class="es_file_item_remove">x</a></div>', 
							'</div>'
							].join(""));
			var fiId = "#file_item_" + file.id;
			$(fiId).find("div.es-progressbar").progressbar({value:0, height:12});
			$(fiId).find(".es_file_item_remove").click(function(event) {
				uploader.removeFile(file, true);
				$(fiId).remove();
			});
		});

		uploader.on('uploadProgress', function(file, percentage) {
			$('#file_item_' + file.id).find('div.es-progressbar').progressbar('setValue', Math.floor(percentage*100));
		});
		
		uploader.on('uploadStart', function(file) {
			/*var formData = uploader.option("formData");
			if(!formData["uuidMap"]) {
				formData["uuidMap"] = {};
			}*/
			var uuid = genUuid(8, 16);
			uuidMap[file.id] = uuid;
			//formData["uuidMap"][file.id] = uuid;
			//formData["uuid"] = uuid;
		});
		
		uploader.on('uploadBeforeSend', function(object, data, headers) {
			data.id = "";
			var file = object.file;
			data.uuid = uuidMap[file.id];//uploader.option("formData")["uuidMap"][file.id];//修复并发上传时，文件被覆盖bug
			//console.log(data);
			//console.log(uuidMap);
		});
		
		uploader.on('uploadSuccess', function(file, response) {
			$('#file_item_' + file.id).find('span.state').text(lang["uploadedTitle"]);
			mergeFile(jq, file, uuidMap, function(data) {
				$('#file_item_' + file.id).fadeOut();
			});
		});

		uploader.on('uploadError', function(file, reason) {
			$('#file_item_' + file.id).find('span.state').text(lang["uploadErrorTitle"] + reason);
			uploader.cancelFile(file);
	        uploader.removeFile(file,true);
	        uploader.reset();
		});

		uploader.on('uploadComplete', function(file) {
			$('#file_item_' + file.id).find('div.es-progressbar').fadeOut();
		});
		
		uploader.on('uploadFinished', function() {
			var val = $.fn.filebox.methods["getValue"](jq);
			var oldVal = opts.value;
			uuidMap = {};
			if(opts["onAfterUploadFinished"]) opts["onAfterUploadFinished"].call(jq, val, oldVal, uploader.getFiles());
		});

		$("#file_upload_" + uuid).on('click', function() {
			if ($(this).hasClass('disabled')) {
				return false;
			}
			uploader.upload();
		});
		//初始化值
		if(opts.value) {
			$.fn.filebox.methods["setValue"](jq, opts.value);
		}
		$(jq).data("uploader", uploader);
	};
	
	var getCommonOpt = function(jq, opt) {
		opt.uploadUrl = opt.baseUrl + opt.uploadUrl;
		opt.queryUrl = opt.baseUrl + opt.queryUrl;
		opt.deleteUrl = opt.baseUrl + opt.deleteUrl;
		opt.downloadUrl = opt.baseUrl + opt.downloadUrl;
		opt.attachUrl = opt.baseUrl + opt.attachUrl;
		opt.swfUrl = opt.baseUrl + opt.swfUrl;
		opt.mergeUrl = opt.baseUrl + opt.mergeUrl;
	    return opt;
	};
	
	var getAttachId = function(opt) {
		var val = "";
		var options = {
				url : opt.attachUrl,
				async: false,
				success : function(data) {
					val = data.msg;
				}
		};
		fnFormAjaxWithJson(options);
		return val;
	};
	
	var mergeFile = function(jq, file, uuidMap, onAfterMerge) {
		var opt = $.fn.filebox.methods["options"](jq);
		var uploader = $(jq).data("uploader");
		var options = {
				url : opt.mergeUrl,
				data : {
					"uuid": uuidMap[file.id],//uploader.option("formData")["uuidMap"][file.id],
					"attachId": $.fn.filebox.methods["getValue"](jq),
					chunks: Math.ceil(file.size/DEFAULT_CHUNK_SIZE),
					filedataFileName: file.name,
				},
				success : function(data) {
					if(onAfterMerge) {
						onAfterMerge.call(jq, data);
					}
				}
		};
		fnFormAjaxWithJson(options);
	};
	
	$.fn.filebox = function(options, param) {
		if (typeof options == 'string') {
			var method = $.fn.filebox.methods[options];
			if (method) {
				return method(this, param);
			} else {
				return $(this).fileinput(options, param);//调用原生方法
			}
		}
		options = options || {};
		return this.each(function() {
			var state = $(this).data('filebox');
			if (state) {
				$.extend(state.options, options);
			} else {
				var opts = $.extend({}, $.fn.filebox.defaults, options);
				$(this).data('filebox', {
					options : opts,
					data : null
				});
				init(this, opts);
			}
		});
	};

	$.fn.filebox.methods = {
		options : function(jq) {
			return $(jq).data("filebox").options;
		},
		getValue : function(jq) {
			return $(jq).data("filebox").data;
		},
		setValue : function(jq, val) {
			var stateData = $(jq).data("filebox");
			if(stateData && stateData.data != val) {//只有值变更时才需要更新数据
				stateData.data = val;
			}
		},
		enable : function(jq){
			return jq.each(function() {
				$(this).find("div.file_upload_div").show();
			});
		},
		disable : function(jq){
			return jq.each(function() {
				$(this).find("div.file_upload_div").hide();
			});
		},
		destroy: function(jq){
			return jq.each(function() {
				$(this).find("div.file_upload_div").remove();
			});
		}
	};

	$.fn.filebox.defaults = $.extend({}, {
		language:"zh_CN",
		readonly:false,//文件只读，不可上传
		multiple:true, //上传多个文件
		baseUrl:"",//上下文路径
		isImage:false,//是否图片控件
		chunked: true, //默认分片
		isSimpleMode:false,//是否简单文件上传模式，不作为附件上传
		canDelete:true, //是否可以删除,readonly=true,不能删除；readonly=false，判断能否删除；
		canDownload:true, //是否可以下载
		swfUrl:"/js/webuploader/Uploader.swf",
		uploadUrl:"/sys/attach/upload-file!saveFile.action",
		mergeUrl:"/sys/attach/upload-file!mergeFile.action",
		queryUrl:"/sys/attach/attach!queryList.action",
		deleteUrl:"/sys/attach/attach!delete.action",
		downloadUrl:"/sys/attach/upload-file!downloadFile.action",
		attachUrl:"/sys/attach/attach!save.action",//获取附件ID url
		formData:{},//表单请求参数
		maxFileNum:10,//文件数量限制
		maxFileSize:500 * 1024 * 1024,//文件总大小限制，默认500M
		maxSingleFileSize: 50 * 1024 * 1024, //单文件大小限制，默认50M
		onFirstSelect:null,//第一次上传时调用
		onAfterUploadFinished:null //所有文件上传结束后调用方法
	});
})(jQuery);
!function(e){e(["jquery"],function(e){return function(){function t(e,t,n){return g({type:O.error,iconClass:m().iconClasses.error,message:e,optionsOverride:n,title:t})}function n(t,n){return t||(t=m()),v=e("#"+t.containerId),v.length?v:(n&&(v=u(t)),v)}function i(e,t,n){return g({type:O.info,iconClass:m().iconClasses.info,message:e,optionsOverride:n,title:t})}function o(e){w=e}function s(e,t,n){return g({type:O.success,iconClass:m().iconClasses.success,message:e,optionsOverride:n,title:t})}function a(e,t,n){return g({type:O.warning,iconClass:m().iconClasses.warning,message:e,optionsOverride:n,title:t})}function r(e,t){var i=m();v||n(i),l(e,i,t)||d(i)}function c(t){var i=m();return v||n(i),t&&0===e(":focus",t).length?void h(t):void(v.children().length&&v.remove())}function d(t){for(var n=v.children(),i=n.length-1;i>=0;i--)l(e(n[i]),t)}function l(t,n,i){var o=i&&i.force?i.force:!1;return t&&(o||0===e(":focus",t).length)?(t[n.hideMethod]({duration:n.hideDuration,easing:n.hideEasing,complete:function(){h(t)}}),!0):!1}function u(t){return v=e("<div/>").attr("id",t.containerId).addClass(t.positionClass).attr("aria-live","polite").attr("role","alert"),v.appendTo(e(t.target)),v}function p(){return{tapToDismiss:!0,toastClass:"toast",containerId:"toast-container",debug:!1,showMethod:"fadeIn",showDuration:300,showEasing:"swing",onShown:void 0,hideMethod:"fadeOut",hideDuration:1e3,hideEasing:"swing",onHidden:void 0,closeMethod:!1,closeDuration:!1,closeEasing:!1,extendedTimeOut:1e3,iconClasses:{error:"toast-error",info:"toast-info",success:"toast-success",warning:"toast-warning"},iconClass:"toast-info",positionClass:"toast-top-right",timeOut:5e3,titleClass:"toast-title",messageClass:"toast-message",escapeHtml:!1,target:"body",closeHtml:'<button type="button">&times;</button>',newestOnTop:!0,preventDuplicates:!1,progressBar:!1}}function f(e){w&&w(e)}function g(t){function i(e){return null==e&&(e=""),new String(e).replace(/&/g,"&amp;").replace(/"/g,"&quot;").replace(/'/g,"&#39;").replace(/</g,"&lt;").replace(/>/g,"&gt;")}function o(){r(),d(),l(),u(),p(),c()}function s(){y.hover(b,O),!x.onclick&&x.tapToDismiss&&y.click(w),x.closeButton&&k&&k.click(function(e){e.stopPropagation?e.stopPropagation():void 0!==e.cancelBubble&&e.cancelBubble!==!0&&(e.cancelBubble=!0),w(!0)}),x.onclick&&y.click(function(e){x.onclick(e),w()})}function a(){y.hide(),y[x.showMethod]({duration:x.showDuration,easing:x.showEasing,complete:x.onShown}),x.timeOut>0&&(H=setTimeout(w,x.timeOut),q.maxHideTime=parseFloat(x.timeOut),q.hideEta=(new Date).getTime()+q.maxHideTime,x.progressBar&&(q.intervalId=setInterval(D,10)))}function r(){t.iconClass&&y.addClass(x.toastClass).addClass(E)}function c(){x.newestOnTop?v.prepend(y):v.append(y)}function d(){t.title&&(I.append(x.escapeHtml?i(t.title):t.title).addClass(x.titleClass),y.append(I))}function l(){t.message&&(M.append(x.escapeHtml?i(t.message):t.message).addClass(x.messageClass),y.append(M))}function u(){x.closeButton&&(k.addClass("toast-close-button").attr("role","button"),y.prepend(k))}function p(){x.progressBar&&(B.addClass("toast-progress"),y.prepend(B))}function g(e,t){if(e.preventDuplicates){if(t.message===C)return!0;C=t.message}return!1}function w(t){var n=t&&x.closeMethod!==!1?x.closeMethod:x.hideMethod,i=t&&x.closeDuration!==!1?x.closeDuration:x.hideDuration,o=t&&x.closeEasing!==!1?x.closeEasing:x.hideEasing;return!e(":focus",y).length||t?(clearTimeout(q.intervalId),y[n]({duration:i,easing:o,complete:function(){h(y),x.onHidden&&"hidden"!==j.state&&x.onHidden(),j.state="hidden",j.endTime=new Date,f(j)}})):void 0}function O(){(x.timeOut>0||x.extendedTimeOut>0)&&(H=setTimeout(w,x.extendedTimeOut),q.maxHideTime=parseFloat(x.extendedTimeOut),q.hideEta=(new Date).getTime()+q.maxHideTime)}function b(){clearTimeout(H),q.hideEta=0,y.stop(!0,!0)[x.showMethod]({duration:x.showDuration,easing:x.showEasing})}function D(){var e=(q.hideEta-(new Date).getTime())/q.maxHideTime*100;B.width(e+"%")}var x=m(),E=t.iconClass||x.iconClass;if("undefined"!=typeof t.optionsOverride&&(x=e.extend(x,t.optionsOverride),E=t.optionsOverride.iconClass||E),!g(x,t)){T++,v=n(x,!0);var H=null,y=e("<div/>"),I=e("<div/>"),M=e("<div/>"),B=e("<div/>"),k=e(x.closeHtml),q={intervalId:null,hideEta:null,maxHideTime:null},j={toastId:T,state:"visible",startTime:new Date,options:x,map:t};return o(),a(),s(),f(j),x.debug&&console&&console.log(j),y}}function m(){return e.extend({},p(),b.options)}function h(e){v||(v=n()),e.is(":visible")||(e.remove(),e=null,0===v.children().length&&(v.remove(),C=void 0))}var v,w,C,T=0,O={error:"error",info:"info",success:"success",warning:"warning"},b={clear:r,remove:c,error:t,getContainer:n,info:i,options:{},subscribe:o,success:s,version:"2.1.2",warning:a};return b}()})}("function"==typeof define&&define.amd?define:function(e,t){"undefined"!=typeof module&&module.exports?module.exports=t(require("jquery")):window.toastr=t(window.jQuery)});
//# sourceMappingURL=toastr.js.map
/* 
Native FullScreen JavaScript API
CopyRight: Johndyer, http://johndyer.name/native-fullscreen-javascript-api-plus-jquery-plugin/
-------------
Assumes Mozilla naming conventions instead of W3C for now
*/
(function() {
    var
        fullScreenApi = {
            supportsFullScreen: false,
            isFullScreen: function() { return false; },
            requestFullScreen: function() {},
            cancelFullScreen: function() {},
            fullScreenEventName: '',
            prefix: ''
        },
        browserPrefixes = 'webkit moz o ms khtml'.split(' ');
 
    // check for native support
    if (typeof document.cancelFullScreen != 'undefined') {
        fullScreenApi.supportsFullScreen = true;
    } else {
        // check for fullscreen support by vendor prefix
        for (var i = 0, il = browserPrefixes.length; i < il; i++ ) {
            fullScreenApi.prefix = browserPrefixes[i];
 
            if (typeof document[fullScreenApi.prefix + 'CancelFullScreen' ] != 'undefined' ) {
                fullScreenApi.supportsFullScreen = true;
 
                break;
            }
        }
    }
 
    // update methods to do something useful
    if (fullScreenApi.supportsFullScreen) {
        fullScreenApi.fullScreenEventName = fullScreenApi.prefix + 'fullscreenchange';
 
        fullScreenApi.isFullScreen = function() {
            switch (this.prefix) {
                case '':
                    return document.fullScreen;
                case 'webkit':
                    return document.webkitIsFullScreen;
                default:
                    return document[this.prefix + 'FullScreen'];
            }
        }
        fullScreenApi.requestFullScreen = function(el) {
            return (this.prefix === '') ? el.requestFullScreen() : el[this.prefix + 'RequestFullScreen']();
        }
        fullScreenApi.cancelFullScreen = function(el) {
            return (this.prefix === '') ? document.cancelFullScreen() : document[this.prefix + 'CancelFullScreen']();
        }
    }
 
    // jQuery plugin
    if (typeof jQuery != 'undefined') {
        jQuery.fn.requestFullScreen = function() {
 
            return this.each(function() {
                if (fullScreenApi.supportsFullScreen) {
                    fullScreenApi.requestFullScreen(this);
                }
            });
        };
    }
 
    // export api
    window.fullScreenApi = fullScreenApi;
})();
/* NProgress, (c) 2013, 2014 Rico Sta. Cruz - http://ricostacruz.com/nprogress
 * @license MIT */

;(function(root, factory) {

  if (typeof define === 'function' && define.amd) {
    define(factory);
  } else if (typeof exports === 'object') {
    module.exports = factory();
  } else {
    root.NProgress = factory();
  }

})(this, function() {
  var NProgress = {};

  NProgress.version = '0.2.0';

  var Settings = NProgress.settings = {
    minimum: 0.08,
    easing: 'ease',
    positionUsing: '',
    speed: 200,
    trickle: true,
    trickleRate: 0.02,
    trickleSpeed: 800,
    showSpinner: true,
    barSelector: '[role="bar"]',
    spinnerSelector: '[role="spinner"]',
    parent: 'body',
    template: '<div class="bar" role="bar"><div class="peg"></div></div><div class="spinner" role="spinner"><div class="spinner-icon"></div></div>'
  };

  /**
   * Updates configuration.
   *
   *     NProgress.configure({
   *       minimum: 0.1
   *     });
   */
  NProgress.configure = function(options) {
    var key, value;
    for (key in options) {
      value = options[key];
      if (value !== undefined && options.hasOwnProperty(key)) Settings[key] = value;
    }

    return this;
  };

  /**
   * Last number.
   */

  NProgress.status = null;

  /**
   * Sets the progress bar status, where `n` is a number from `0.0` to `1.0`.
   *
   *     NProgress.set(0.4);
   *     NProgress.set(1.0);
   */

  NProgress.set = function(n) {
    var started = NProgress.isStarted();

    n = clamp(n, Settings.minimum, 1);
    NProgress.status = (n === 1 ? null : n);

    var progress = NProgress.render(!started),
        bar      = progress.querySelector(Settings.barSelector),
        speed    = Settings.speed,
        ease     = Settings.easing;

    progress.offsetWidth; /* Repaint */

    queue(function(next) {
      // Set positionUsing if it hasn't already been set
      if (Settings.positionUsing === '') Settings.positionUsing = NProgress.getPositioningCSS();

      // Add transition
      css(bar, barPositionCSS(n, speed, ease));

      if (n === 1) {
        // Fade out
        css(progress, { 
          transition: 'none', 
          opacity: 1 
        });
        progress.offsetWidth; /* Repaint */

        setTimeout(function() {
          css(progress, { 
            transition: 'all ' + speed + 'ms linear', 
            opacity: 0 
          });
          setTimeout(function() {
            NProgress.remove();
            next();
          }, speed);
        }, speed);
      } else {
        setTimeout(next, speed);
      }
    });

    return this;
  };

  NProgress.isStarted = function() {
    return typeof NProgress.status === 'number';
  };

  /**
   * Shows the progress bar.
   * This is the same as setting the status to 0%, except that it doesn't go backwards.
   *
   *     NProgress.start();
   *
   */
  NProgress.start = function() {
    if (!NProgress.status) NProgress.set(0);

    var work = function() {
      setTimeout(function() {
        if (!NProgress.status) return;
        NProgress.trickle();
        work();
      }, Settings.trickleSpeed);
    };

    if (Settings.trickle) work();

    return this;
  };

  /**
   * Hides the progress bar.
   * This is the *sort of* the same as setting the status to 100%, with the
   * difference being `done()` makes some placebo effect of some realistic motion.
   *
   *     NProgress.done();
   *
   * If `true` is passed, it will show the progress bar even if its hidden.
   *
   *     NProgress.done(true);
   */

  NProgress.done = function(force) {
    if (!force && !NProgress.status) return this;

    return NProgress.inc(0.3 + 0.5 * Math.random()).set(1);
  };

  /**
   * Increments by a random amount.
   */

  NProgress.inc = function(amount) {
    var n = NProgress.status;

    if (!n) {
      return NProgress.start();
    } else {
      if (typeof amount !== 'number') {
        amount = (1 - n) * clamp(Math.random() * n, 0.1, 0.95);
      }

      n = clamp(n + amount, 0, 0.994);
      return NProgress.set(n);
    }
  };

  NProgress.trickle = function() {
    return NProgress.inc(Math.random() * Settings.trickleRate);
  };

  /**
   * Waits for all supplied jQuery promises and
   * increases the progress as the promises resolve.
   *
   * @param $promise jQUery Promise
   */
  (function() {
    var initial = 0, current = 0;

    NProgress.promise = function($promise) {
      if (!$promise || $promise.state() === "resolved") {
        return this;
      }

      if (current === 0) {
        NProgress.start();
      }

      initial++;
      current++;

      $promise.always(function() {
        current--;
        if (current === 0) {
            initial = 0;
            NProgress.done();
        } else {
            NProgress.set((initial - current) / initial);
        }
      });

      return this;
    };

  })();

  /**
   * (Internal) renders the progress bar markup based on the `template`
   * setting.
   */

  NProgress.render = function(fromStart) {
    if (NProgress.isRendered()) return document.getElementById('nprogress');

    addClass(document.documentElement, 'nprogress-busy');
    
    var progress = document.createElement('div');
    progress.id = 'nprogress';
    progress.innerHTML = Settings.template;

    var bar      = progress.querySelector(Settings.barSelector),
        perc     = fromStart ? '-100' : toBarPerc(NProgress.status || 0),
        parent   = document.querySelector(Settings.parent),
        spinner;
    
    css(bar, {
      transition: 'all 0 linear',
      transform: 'translate3d(' + perc + '%,0,0)'
    });

    if (!Settings.showSpinner) {
      spinner = progress.querySelector(Settings.spinnerSelector);
      spinner && removeElement(spinner);
    }

    if (parent != document.body) {
      addClass(parent, 'nprogress-custom-parent');
    }

    parent.appendChild(progress);
    return progress;
  };

  /**
   * Removes the element. Opposite of render().
   */

  NProgress.remove = function() {
    removeClass(document.documentElement, 'nprogress-busy');
    removeClass(document.querySelector(Settings.parent), 'nprogress-custom-parent');
    var progress = document.getElementById('nprogress');
    progress && removeElement(progress);
  };

  /**
   * Checks if the progress bar is rendered.
   */

  NProgress.isRendered = function() {
    return !!document.getElementById('nprogress');
  };

  /**
   * Determine which positioning CSS rule to use.
   */

  NProgress.getPositioningCSS = function() {
    // Sniff on document.body.style
    var bodyStyle = document.body.style;

    // Sniff prefixes
    var vendorPrefix = ('WebkitTransform' in bodyStyle) ? 'Webkit' :
                       ('MozTransform' in bodyStyle) ? 'Moz' :
                       ('msTransform' in bodyStyle) ? 'ms' :
                       ('OTransform' in bodyStyle) ? 'O' : '';

    if (vendorPrefix + 'Perspective' in bodyStyle) {
      // Modern browsers with 3D support, e.g. Webkit, IE10
      return 'translate3d';
    } else if (vendorPrefix + 'Transform' in bodyStyle) {
      // Browsers without 3D support, e.g. IE9
      return 'translate';
    } else {
      // Browsers without translate() support, e.g. IE7-8
      return 'margin';
    }
  };

  /**
   * Helpers
   */

  function clamp(n, min, max) {
    if (n < min) return min;
    if (n > max) return max;
    return n;
  }

  /**
   * (Internal) converts a percentage (`0..1`) to a bar translateX
   * percentage (`-100%..0%`).
   */

  function toBarPerc(n) {
    return (-1 + n) * 100;
  }


  /**
   * (Internal) returns the correct CSS for changing the bar's
   * position given an n percentage, and speed and ease from Settings
   */

  function barPositionCSS(n, speed, ease) {
    var barCSS;

    if (Settings.positionUsing === 'translate3d') {
      barCSS = { transform: 'translate3d('+toBarPerc(n)+'%,0,0)' };
    } else if (Settings.positionUsing === 'translate') {
      barCSS = { transform: 'translate('+toBarPerc(n)+'%,0)' };
    } else {
      barCSS = { 'margin-left': toBarPerc(n)+'%' };
    }

    barCSS.transition = 'all '+speed+'ms '+ease;

    return barCSS;
  }

  /**
   * (Internal) Queues a function to be executed.
   */

  var queue = (function() {
    var pending = [];
    
    function next() {
      var fn = pending.shift();
      if (fn) {
        fn(next);
      }
    }

    return function(fn) {
      pending.push(fn);
      if (pending.length == 1) next();
    };
  })();

  /**
   * (Internal) Applies css properties to an element, similar to the jQuery 
   * css method.
   *
   * While this helper does assist with vendor prefixed property names, it 
   * does not perform any manipulation of values prior to setting styles.
   */

  var css = (function() {
    var cssPrefixes = [ 'Webkit', 'O', 'Moz', 'ms' ],
        cssProps    = {};

    function camelCase(string) {
      return string.replace(/^-ms-/, 'ms-').replace(/-([\da-z])/gi, function(match, letter) {
        return letter.toUpperCase();
      });
    }

    function getVendorProp(name) {
      var style = document.body.style;
      if (name in style) return name;

      var i = cssPrefixes.length,
          capName = name.charAt(0).toUpperCase() + name.slice(1),
          vendorName;
      while (i--) {
        vendorName = cssPrefixes[i] + capName;
        if (vendorName in style) return vendorName;
      }

      return name;
    }

    function getStyleProp(name) {
      name = camelCase(name);
      return cssProps[name] || (cssProps[name] = getVendorProp(name));
    }

    function applyCss(element, prop, value) {
      prop = getStyleProp(prop);
      element.style[prop] = value;
    }

    return function(element, properties) {
      var args = arguments,
          prop, 
          value;

      if (args.length == 2) {
        for (prop in properties) {
          value = properties[prop];
          if (value !== undefined && properties.hasOwnProperty(prop)) applyCss(element, prop, value);
        }
      } else {
        applyCss(element, args[1], args[2]);
      }
    }
  })();

  /**
   * (Internal) Determines if an element or space separated list of class names contains a class name.
   */

  function hasClass(element, name) {
    var list = typeof element == 'string' ? element : classList(element);
    return list.indexOf(' ' + name + ' ') >= 0;
  }

  /**
   * (Internal) Adds a class to an element.
   */

  function addClass(element, name) {
    var oldList = classList(element),
        newList = oldList + name;

    if (hasClass(oldList, name)) return; 

    // Trim the opening space.
    element.className = newList.substring(1);
  }

  /**
   * (Internal) Removes a class from an element.
   */

  function removeClass(element, name) {
    var oldList = classList(element),
        newList;

    if (!hasClass(element, name)) return;

    // Replace the class name.
    newList = oldList.replace(' ' + name + ' ', ' ');

    // Trim the opening and closing spaces.
    element.className = newList.substring(1, newList.length - 1);
  }

  /**
   * (Internal) Gets a space separated list of the class names on the element. 
   * The list is wrapped with a single space on each end to facilitate finding 
   * matches within the list.
   */

  function classList(element) {
    return (' ' + (element.className || '') + ' ').replace(/\s+/gi, ' ');
  }

  /**
   * (Internal) Removes an element from the DOM.
   */

  function removeElement(element) {
    element && element.parentNode && element.parentNode.removeChild(element);
  }

  return NProgress;
});


/*! sprintf-js v1.1.1 | Copyright (c) 2007-present, Alexandru Mărășteanu <hello@alexei.ro> | BSD-3-Clause */
!function(){"use strict";function e(e){return r(n(e),arguments)}function t(t,r){return e.apply(null,[t].concat(r||[]))}function r(t,r){var n,i,a,o,p,c,u,f,l,d=1,g=t.length,b="";for(i=0;i<g;i++)if("string"==typeof t[i])b+=t[i];else if(Array.isArray(t[i])){if((o=t[i])[2])for(n=r[d],a=0;a<o[2].length;a++){if(!n.hasOwnProperty(o[2][a]))throw new Error(e('[sprintf] property "%s" does not exist',o[2][a]));n=n[o[2][a]]}else n=o[1]?r[o[1]]:r[d++];if(s.not_type.test(o[8])&&s.not_primitive.test(o[8])&&n instanceof Function&&(n=n()),s.numeric_arg.test(o[8])&&"number"!=typeof n&&isNaN(n))throw new TypeError(e("[sprintf] expecting number but found %T",n));switch(s.number.test(o[8])&&(f=n>=0),o[8]){case"b":n=parseInt(n,10).toString(2);break;case"c":n=String.fromCharCode(parseInt(n,10));break;case"d":case"i":n=parseInt(n,10);break;case"j":n=JSON.stringify(n,null,o[6]?parseInt(o[6]):0);break;case"e":n=o[7]?parseFloat(n).toExponential(o[7]):parseFloat(n).toExponential();break;case"f":n=o[7]?parseFloat(n).toFixed(o[7]):parseFloat(n);break;case"g":n=o[7]?String(Number(n.toPrecision(o[7]))):parseFloat(n);break;case"o":n=(parseInt(n,10)>>>0).toString(8);break;case"s":n=String(n),n=o[7]?n.substring(0,o[7]):n;break;case"t":n=String(!!n),n=o[7]?n.substring(0,o[7]):n;break;case"T":n=Object.prototype.toString.call(n).slice(8,-1).toLowerCase(),n=o[7]?n.substring(0,o[7]):n;break;case"u":n=parseInt(n,10)>>>0;break;case"v":n=n.valueOf(),n=o[7]?n.substring(0,o[7]):n;break;case"x":n=(parseInt(n,10)>>>0).toString(16);break;case"X":n=(parseInt(n,10)>>>0).toString(16).toUpperCase()}s.json.test(o[8])?b+=n:(!s.number.test(o[8])||f&&!o[3]?l="":(l=f?"+":"-",n=n.toString().replace(s.sign,"")),c=o[4]?"0"===o[4]?"0":o[4].charAt(1):" ",u=o[6]-(l+n).length,p=o[6]&&u>0?c.repeat(u):"",b+=o[5]?l+n+p:"0"===c?l+p+n:p+l+n)}return b}function n(e){if(i[e])return i[e];for(var t,r=e,n=[],a=0;r;){if(null!==(t=s.text.exec(r)))n.push(t[0]);else if(null!==(t=s.modulo.exec(r)))n.push("%");else{if(null===(t=s.placeholder.exec(r)))throw new SyntaxError("[sprintf] unexpected placeholder");if(t[2]){a|=1;var o=[],p=t[2],c=[];if(null===(c=s.key.exec(p)))throw new SyntaxError("[sprintf] failed to parse named argument key");for(o.push(c[1]);""!==(p=p.substring(c[0].length));)if(null!==(c=s.key_access.exec(p)))o.push(c[1]);else{if(null===(c=s.index_access.exec(p)))throw new SyntaxError("[sprintf] failed to parse named argument key");o.push(c[1])}t[2]=o}else a|=2;if(3===a)throw new Error("[sprintf] mixing positional and named placeholders is not (yet) supported");n.push(t)}r=r.substring(t[0].length)}return i[e]=n}var s={not_string:/[^s]/,not_bool:/[^t]/,not_type:/[^T]/,not_primitive:/[^v]/,number:/[diefg]/,numeric_arg:/[bcdiefguxX]/,json:/[j]/,not_json:/[^j]/,text:/^[^\x25]+/,modulo:/^\x25{2}/,placeholder:/^\x25(?:([1-9]\d*)\$|\(([^\)]+)\))?(\+)?(0|'[^$])?(-)?(\d+)?(?:\.(\d+))?([b-gijostTuvxX])/,key:/^([a-z_][a-z_\d]*)/i,key_access:/^\.([a-z_][a-z_\d]*)/i,index_access:/^\[(\d+)\]/,sign:/^[\+\-]/},i=Object.create(null);"undefined"!=typeof exports&&(exports.sprintf=e,exports.vsprintf=t),"undefined"!=typeof window&&(window.sprintf=e,window.vsprintf=t,"function"==typeof define&&define.amd&&define(function(){return{sprintf:e,vsprintf:t}}))}();
//# sourceMappingURL=sprintf.min.js.map

/*! pace 1.0.2 */
(function(){var a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X=[].slice,Y={}.hasOwnProperty,Z=function(a,b){function c(){this.constructor=a}for(var d in b)Y.call(b,d)&&(a[d]=b[d]);return c.prototype=b.prototype,a.prototype=new c,a.__super__=b.prototype,a},$=[].indexOf||function(a){for(var b=0,c=this.length;c>b;b++)if(b in this&&this[b]===a)return b;return-1};for(u={catchupTime:100,initialRate:.03,minTime:250,ghostTime:100,maxProgressPerFrame:20,easeFactor:1.25,startOnPageLoad:!0,restartOnPushState:!0,restartOnRequestAfter:500,target:"body",elements:{checkInterval:100,selectors:["body"]},eventLag:{minSamples:10,sampleCount:3,lagThreshold:3},ajax:{trackMethods:["GET"],trackWebSockets:!0,ignoreURLs:[]}},C=function(){var a;return null!=(a="undefined"!=typeof performance&&null!==performance&&"function"==typeof performance.now?performance.now():void 0)?a:+new Date},E=window.requestAnimationFrame||window.mozRequestAnimationFrame||window.webkitRequestAnimationFrame||window.msRequestAnimationFrame,t=window.cancelAnimationFrame||window.mozCancelAnimationFrame,null==E&&(E=function(a){return setTimeout(a,50)},t=function(a){return clearTimeout(a)}),G=function(a){var b,c;return b=C(),(c=function(){var d;return d=C()-b,d>=33?(b=C(),a(d,function(){return E(c)})):setTimeout(c,33-d)})()},F=function(){var a,b,c;return c=arguments[0],b=arguments[1],a=3<=arguments.length?X.call(arguments,2):[],"function"==typeof c[b]?c[b].apply(c,a):c[b]},v=function(){var a,b,c,d,e,f,g;for(b=arguments[0],d=2<=arguments.length?X.call(arguments,1):[],f=0,g=d.length;g>f;f++)if(c=d[f])for(a in c)Y.call(c,a)&&(e=c[a],null!=b[a]&&"object"==typeof b[a]&&null!=e&&"object"==typeof e?v(b[a],e):b[a]=e);return b},q=function(a){var b,c,d,e,f;for(c=b=0,e=0,f=a.length;f>e;e++)d=a[e],c+=Math.abs(d),b++;return c/b},x=function(a,b){var c,d,e;if(null==a&&(a="options"),null==b&&(b=!0),e=document.querySelector("[data-pace-"+a+"]")){if(c=e.getAttribute("data-pace-"+a),!b)return c;try{return JSON.parse(c)}catch(f){return d=f,"undefined"!=typeof console&&null!==console?console.error("Error parsing inline pace options",d):void 0}}},g=function(){function a(){}return a.prototype.on=function(a,b,c,d){var e;return null==d&&(d=!1),null==this.bindings&&(this.bindings={}),null==(e=this.bindings)[a]&&(e[a]=[]),this.bindings[a].push({handler:b,ctx:c,once:d})},a.prototype.once=function(a,b,c){return this.on(a,b,c,!0)},a.prototype.off=function(a,b){var c,d,e;if(null!=(null!=(d=this.bindings)?d[a]:void 0)){if(null==b)return delete this.bindings[a];for(c=0,e=[];c<this.bindings[a].length;)e.push(this.bindings[a][c].handler===b?this.bindings[a].splice(c,1):c++);return e}},a.prototype.trigger=function(){var a,b,c,d,e,f,g,h,i;if(c=arguments[0],a=2<=arguments.length?X.call(arguments,1):[],null!=(g=this.bindings)?g[c]:void 0){for(e=0,i=[];e<this.bindings[c].length;)h=this.bindings[c][e],d=h.handler,b=h.ctx,f=h.once,d.apply(null!=b?b:this,a),i.push(f?this.bindings[c].splice(e,1):e++);return i}},a}(),j=window.Pace||{},window.Pace=j,v(j,g.prototype),D=j.options=v({},u,window.paceOptions,x()),U=["ajax","document","eventLag","elements"],Q=0,S=U.length;S>Q;Q++)K=U[Q],D[K]===!0&&(D[K]=u[K]);i=function(a){function b(){return V=b.__super__.constructor.apply(this,arguments)}return Z(b,a),b}(Error),b=function(){function a(){this.progress=0}return a.prototype.getElement=function(){var a;if(null==this.el){if(a=document.querySelector(D.target),!a)throw new i;this.el=document.createElement("div"),this.el.className="pace pace-active",document.body.className=document.body.className.replace(/pace-done/g,""),document.body.className+=" pace-running",this.el.innerHTML='<div class="pace-progress">\n  <div class="pace-progress-inner"></div>\n</div>\n<div class="pace-activity"></div>',null!=a.firstChild?a.insertBefore(this.el,a.firstChild):a.appendChild(this.el)}return this.el},a.prototype.finish=function(){var a;return a=this.getElement(),a.className=a.className.replace("pace-active",""),a.className+=" pace-inactive",document.body.className=document.body.className.replace("pace-running",""),document.body.className+=" pace-done"},a.prototype.update=function(a){return this.progress=a,this.render()},a.prototype.destroy=function(){try{this.getElement().parentNode.removeChild(this.getElement())}catch(a){i=a}return this.el=void 0},a.prototype.render=function(){var a,b,c,d,e,f,g;if(null==document.querySelector(D.target))return!1;for(a=this.getElement(),d="translate3d("+this.progress+"%, 0, 0)",g=["webkitTransform","msTransform","transform"],e=0,f=g.length;f>e;e++)b=g[e],a.children[0].style[b]=d;return(!this.lastRenderedProgress||this.lastRenderedProgress|0!==this.progress|0)&&(a.children[0].setAttribute("data-progress-text",""+(0|this.progress)+"%"),this.progress>=100?c="99":(c=this.progress<10?"0":"",c+=0|this.progress),a.children[0].setAttribute("data-progress",""+c)),this.lastRenderedProgress=this.progress},a.prototype.done=function(){return this.progress>=100},a}(),h=function(){function a(){this.bindings={}}return a.prototype.trigger=function(a,b){var c,d,e,f,g;if(null!=this.bindings[a]){for(f=this.bindings[a],g=[],d=0,e=f.length;e>d;d++)c=f[d],g.push(c.call(this,b));return g}},a.prototype.on=function(a,b){var c;return null==(c=this.bindings)[a]&&(c[a]=[]),this.bindings[a].push(b)},a}(),P=window.XMLHttpRequest,O=window.XDomainRequest,N=window.WebSocket,w=function(a,b){var c,d,e;e=[];for(d in b.prototype)try{e.push(null==a[d]&&"function"!=typeof b[d]?"function"==typeof Object.defineProperty?Object.defineProperty(a,d,{get:function(){return b.prototype[d]},configurable:!0,enumerable:!0}):a[d]=b.prototype[d]:void 0)}catch(f){c=f}return e},A=[],j.ignore=function(){var a,b,c;return b=arguments[0],a=2<=arguments.length?X.call(arguments,1):[],A.unshift("ignore"),c=b.apply(null,a),A.shift(),c},j.track=function(){var a,b,c;return b=arguments[0],a=2<=arguments.length?X.call(arguments,1):[],A.unshift("track"),c=b.apply(null,a),A.shift(),c},J=function(a){var b;if(null==a&&(a="GET"),"track"===A[0])return"force";if(!A.length&&D.ajax){if("socket"===a&&D.ajax.trackWebSockets)return!0;if(b=a.toUpperCase(),$.call(D.ajax.trackMethods,b)>=0)return!0}return!1},k=function(a){function b(){var a,c=this;b.__super__.constructor.apply(this,arguments),a=function(a){var b;return b=a.open,a.open=function(d,e){return J(d)&&c.trigger("request",{type:d,url:e,request:a}),b.apply(a,arguments)}},window.XMLHttpRequest=function(b){var c;return c=new P(b),a(c),c};try{w(window.XMLHttpRequest,P)}catch(d){}if(null!=O){window.XDomainRequest=function(){var b;return b=new O,a(b),b};try{w(window.XDomainRequest,O)}catch(d){}}if(null!=N&&D.ajax.trackWebSockets){window.WebSocket=function(a,b){var d;return d=null!=b?new N(a,b):new N(a),J("socket")&&c.trigger("request",{type:"socket",url:a,protocols:b,request:d}),d};try{w(window.WebSocket,N)}catch(d){}}}return Z(b,a),b}(h),R=null,y=function(){return null==R&&(R=new k),R},I=function(a){var b,c,d,e;for(e=D.ajax.ignoreURLs,c=0,d=e.length;d>c;c++)if(b=e[c],"string"==typeof b){if(-1!==a.indexOf(b))return!0}else if(b.test(a))return!0;return!1},y().on("request",function(b){var c,d,e,f,g;return f=b.type,e=b.request,g=b.url,I(g)?void 0:j.running||D.restartOnRequestAfter===!1&&"force"!==J(f)?void 0:(d=arguments,c=D.restartOnRequestAfter||0,"boolean"==typeof c&&(c=0),setTimeout(function(){var b,c,g,h,i,k;if(b="socket"===f?e.readyState<2:0<(h=e.readyState)&&4>h){for(j.restart(),i=j.sources,k=[],c=0,g=i.length;g>c;c++){if(K=i[c],K instanceof a){K.watch.apply(K,d);break}k.push(void 0)}return k}},c))}),a=function(){function a(){var a=this;this.elements=[],y().on("request",function(){return a.watch.apply(a,arguments)})}return a.prototype.watch=function(a){var b,c,d,e;return d=a.type,b=a.request,e=a.url,I(e)?void 0:(c="socket"===d?new n(b):new o(b),this.elements.push(c))},a}(),o=function(){function a(a){var b,c,d,e,f,g,h=this;if(this.progress=0,null!=window.ProgressEvent)for(c=null,a.addEventListener("progress",function(a){return h.progress=a.lengthComputable?100*a.loaded/a.total:h.progress+(100-h.progress)/2},!1),g=["load","abort","timeout","error"],d=0,e=g.length;e>d;d++)b=g[d],a.addEventListener(b,function(){return h.progress=100},!1);else f=a.onreadystatechange,a.onreadystatechange=function(){var b;return 0===(b=a.readyState)||4===b?h.progress=100:3===a.readyState&&(h.progress=50),"function"==typeof f?f.apply(null,arguments):void 0}}return a}(),n=function(){function a(a){var b,c,d,e,f=this;for(this.progress=0,e=["error","open"],c=0,d=e.length;d>c;c++)b=e[c],a.addEventListener(b,function(){return f.progress=100},!1)}return a}(),d=function(){function a(a){var b,c,d,f;for(null==a&&(a={}),this.elements=[],null==a.selectors&&(a.selectors=[]),f=a.selectors,c=0,d=f.length;d>c;c++)b=f[c],this.elements.push(new e(b))}return a}(),e=function(){function a(a){this.selector=a,this.progress=0,this.check()}return a.prototype.check=function(){var a=this;return document.querySelector(this.selector)?this.done():setTimeout(function(){return a.check()},D.elements.checkInterval)},a.prototype.done=function(){return this.progress=100},a}(),c=function(){function a(){var a,b,c=this;this.progress=null!=(b=this.states[document.readyState])?b:100,a=document.onreadystatechange,document.onreadystatechange=function(){return null!=c.states[document.readyState]&&(c.progress=c.states[document.readyState]),"function"==typeof a?a.apply(null,arguments):void 0}}return a.prototype.states={loading:0,interactive:50,complete:100},a}(),f=function(){function a(){var a,b,c,d,e,f=this;this.progress=0,a=0,e=[],d=0,c=C(),b=setInterval(function(){var g;return g=C()-c-50,c=C(),e.push(g),e.length>D.eventLag.sampleCount&&e.shift(),a=q(e),++d>=D.eventLag.minSamples&&a<D.eventLag.lagThreshold?(f.progress=100,clearInterval(b)):f.progress=100*(3/(a+3))},50)}return a}(),m=function(){function a(a){this.source=a,this.last=this.sinceLastUpdate=0,this.rate=D.initialRate,this.catchup=0,this.progress=this.lastProgress=0,null!=this.source&&(this.progress=F(this.source,"progress"))}return a.prototype.tick=function(a,b){var c;return null==b&&(b=F(this.source,"progress")),b>=100&&(this.done=!0),b===this.last?this.sinceLastUpdate+=a:(this.sinceLastUpdate&&(this.rate=(b-this.last)/this.sinceLastUpdate),this.catchup=(b-this.progress)/D.catchupTime,this.sinceLastUpdate=0,this.last=b),b>this.progress&&(this.progress+=this.catchup*a),c=1-Math.pow(this.progress/100,D.easeFactor),this.progress+=c*this.rate*a,this.progress=Math.min(this.lastProgress+D.maxProgressPerFrame,this.progress),this.progress=Math.max(0,this.progress),this.progress=Math.min(100,this.progress),this.lastProgress=this.progress,this.progress},a}(),L=null,H=null,r=null,M=null,p=null,s=null,j.running=!1,z=function(){return D.restartOnPushState?j.restart():void 0},null!=window.history.pushState&&(T=window.history.pushState,window.history.pushState=function(){return z(),T.apply(window.history,arguments)}),null!=window.history.replaceState&&(W=window.history.replaceState,window.history.replaceState=function(){return z(),W.apply(window.history,arguments)}),l={ajax:a,elements:d,document:c,eventLag:f},(B=function(){var a,c,d,e,f,g,h,i;for(j.sources=L=[],g=["ajax","elements","document","eventLag"],c=0,e=g.length;e>c;c++)a=g[c],D[a]!==!1&&L.push(new l[a](D[a]));for(i=null!=(h=D.extraSources)?h:[],d=0,f=i.length;f>d;d++)K=i[d],L.push(new K(D));return j.bar=r=new b,H=[],M=new m})(),j.stop=function(){return j.trigger("stop"),j.running=!1,r.destroy(),s=!0,null!=p&&("function"==typeof t&&t(p),p=null),B()},j.restart=function(){return j.trigger("restart"),j.stop(),j.start()},j.go=function(){var a;return j.running=!0,r.render(),a=C(),s=!1,p=G(function(b,c){var d,e,f,g,h,i,k,l,n,o,p,q,t,u,v,w;for(l=100-r.progress,e=p=0,f=!0,i=q=0,u=L.length;u>q;i=++q)for(K=L[i],o=null!=H[i]?H[i]:H[i]=[],h=null!=(w=K.elements)?w:[K],k=t=0,v=h.length;v>t;k=++t)g=h[k],n=null!=o[k]?o[k]:o[k]=new m(g),f&=n.done,n.done||(e++,p+=n.tick(b));return d=p/e,r.update(M.tick(b,d)),r.done()||f||s?(r.update(100),j.trigger("done"),setTimeout(function(){return r.finish(),j.running=!1,j.trigger("hide")},Math.max(D.ghostTime,Math.max(D.minTime-(C()-a),0)))):c()})},j.start=function(a){v(D,a),j.running=!0;try{r.render()}catch(b){i=b}return document.querySelector(".pace")?(j.trigger("start"),j.go()):setTimeout(j.start,50)},"function"==typeof define&&define.amd?define(["pace"],function(){return j}):"object"==typeof exports?module.exports=j:D.startOnPageLoad&&j.start()}).call(this);