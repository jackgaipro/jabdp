<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<%
String id = request.getParameter("id");
String idParam = "&id=" + id;
String suffix = ".cpt";
String entityName = request.getParameter("entityName");
String rptName = request.getParameter("rptName");
if(rptName==null || "".equals(rptName.trim())){
	if(null!=entityName && !"".equals(entityName.trim())) {
	String[] en = entityName.split("\\.");
	rptName = en[0];
	}
}
String type = request.getParameter("type");
String rpPath = "/rs?reportlet=";
String ctx = request.getContextPath();
String path = ctx + rpPath + rptName + suffix + idParam;
response.sendRedirect(path);
%>
