<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
<title><s:text name="system.index.title"/></title>
<%@ include file="/common/meta-gs.jsp" %>
<script type="text/javascript">
		function operFormatter(value, rowData, rowIndex) {
			return '<a href="javascript:void(0);" onclick="doView(' + rowIndex + ')"><s:text name="system.button.view.title"/><s:text name="system.sysmng.exceptionLog.message.title"/></a>';
		}
	
		function getOption() {
			return {
				title:'<s:text name="system.sysmng.exceptionLog.list.title"/>',
				iconCls:"icon-search",
				width:600,
				height:350,
				nowrap: false,
				striped: true,
				fit: true,
				url:'${ctx}/sys/log/exp-log!queryList.action',
				sortName: 'id',
				sortOrder: 'desc',
				idField:'id',
				frozenColumns:[[
	                {title:'<s:text name="system.search.id.title"/>',field:'id',width:50,sortable:true,align:'right'}
				]],
				columns:[[
							{field:'threadName',title:'<s:text name="system.sysmng.exceptionLog.threadName.title"/>',width:100, sortable:true},
							{field:'loggerName',title:'<s:text name="system.sysmng.exceptionLog.loggerName.title"/>',width:400, sortable:true},
							{field:'level',title:'<s:text name="system.sysmng.exceptionLog.level.title"/>',width:80, sortable:true},
							{field:'logTime',title:'<s:text name="system.sysmng.exceptionLog.logTime.title"/>',width:140, sortable:true, align:'center'},
							{field:'oper',title:'<s:text name="system.button.oper.title"/>',width:120,align:'center',formatter:operFormatter}
						]],
				pagination:true,
				rownumbers:true
			};
		}
		
		function doQuery() {
			var param = $("#queryForm").serializeArrayToParam();
			$("#queryList").datagrid("load", param);
		}
		
		function doView(index) {
			/*var options = {
				url:'${ctx}/sys/log/exp-log!view.action',
				data:{"id":id},
				success:function(data) {
					if(data.msg) {
						$("#win_message").window("open");
						$("#ta_message").text(data.msg.message);
					}
				}
			};
			fnFormAjaxWithJson(options);*/
			var rows = $("#queryList").datagrid("getRows");
			var data = rows[index];
			if(data) {
				$("#win_message").window("open");
				$("#ta_message").text(data.message);
			}
		}
		


		$(document).ready(function() {
			focusEditor("loggerName");
			doQuseryAction("queryForm");
			
			$("#queryList").datagrid(getOption());
			$("#logTimeStart").focus(function() {
				WdatePicker({maxDate:'#F{$dp.$D(\'logTimeEnd\')}'});
			});
			$("#logTimeEnd").focus(function() {
				WdatePicker({minDate:'#F{$dp.$D(\'logTimeStart\')}'});
			});
			$("#bt_query").click(doQuery);
			$("#bt_reset").click(function() {
				$("#queryForm")[0].reset();
				doQuery();
			});

			$("#win_message").window({
				width:500,
				height:400,
				onOpen:function() {
					$(this).window("move", {
						top:($(window).height()-400)*0.5,
						left:($(window).width()-500)*0.5
					});
				}
			});
		});
</script>
</head>
<body class="easyui-layout" fit="true">
		<div region="west" border="false" title="<s:text name="system.search.title"/>" split="true" style="width:260px;padding:0px;" iconCls="icon-search">
			<form action="" name="queryForm" id="queryForm">
				<table border="0" cellpadding="0" cellspacing="1" class="table_form">
					<tbody>
						<tr><th><label for="loggerName"><s:text name="system.sysmng.exceptionLog.loggerName.title"/>:</label></th>
							<td><input type="text" name="filter_LIKES_loggerName" id="loggerName" class="Itext"></input></td>
						</tr>
						<tr>	
							<th><label for="logTimeStart"><s:text name="system.sysmng.exceptionLog.logTime.title"/>:</label></th>
							<td><input type="text" name="filter_GED_logTime" id="logTimeStart" readonly="readonly" class="Idate Wdate" style="width:160px;"></input>
								<br/>--<br/><input type="text" name="filter_LED_logTime" id="logTimeEnd" readonly="readonly" class="Idate Wdate" style="width:160px;"></input></td>
						</tr>
						<tr>		
							<td colspan="2" align="center">
								<button type="button" id="bt_query"><s:text name="system.search.button.title"/></button>&nbsp;&nbsp;
								<button type="button" id="bt_reset"><s:text name="system.search.reset.title"/></button>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</div>
		<div region="center" title="" border="false">
			<table id="queryList" border="false"></table>
		</div>
		<div id="win_message" class="easyui-window" closed="true" modal="true" title="<s:text name="system.sysmng.exceptionLog.message.title"/>" style="width:500px;height:400px;padding:5px;background:#fafafa;">
			<div class="easyui-layout" fit="true">
				<div region="center" border="false" style="padding:10px;background:#fff;border:1px solid #ccc;">
					<div style="width:100%;height:100%;"><pre id="ta_message"></pre></div>
				</div>
			</div>
		</div>
</body>
</html>
