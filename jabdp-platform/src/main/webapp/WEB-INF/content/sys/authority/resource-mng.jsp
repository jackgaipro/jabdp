<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title><s:text name="system.sysmng.resource.manager.title"/></title>
<%@ include file="/common/meta-gs.jsp"%>
<script type="text/javascript">
	var setting = {
		data : {
			simpleData : {
				enable : true
			}
		},
		view : {
			dblClickExpand : false,
			fontCss: setFontCss
		},
		callback : {
			onRightClick : OnRightClick,
			onClick : onClick
		}
	};
	function setFontCss(treeId, treeNode) {
		var status = treeNode.resourceStatus;
		return status == 2 ? {color:"red"} : {};
	}
	//初始化窗口左边的所有资源树
	function initMenu() {
		var options = {
			url : '${ctx}/sys/account/authority!queryAllList.action',
			success : function(data) {
				if (data.msg) {
					var newNode = {
						id : null,
						name : "<s:text name='system.sysmng.resource.sysmng.title'/>",
						iconSkin:"iconMenu"
					};
					data.msg.push(newNode);
					$.fn.zTree.init($("#authorityTree"), setting, data.msg);
					zTree = $.fn.zTree.getZTreeObj("authorityTree");
					var rootNodes = zTree.getNodes();
					zTree.expandNode(rootNodes[0], true, false, true);
					//zTree.expandAll(true);
				}
			}
		};
		fnFormAjaxWithJson(options, true);
	}

	function OnRightClick(event, treeId, treeNode) {
		event.preventDefault();
		zTree.selectNode(treeNode);
		var status = treeNode.resourceStatus;
		var id = treeNode.id;
		if(status == 1){	
			$("#shiftDel").hide();
			$("#recover").hide();
		$('#mm').menu('show', {
			left : event.pageX,
			top : event.pageY
		});			
		}else if(status == 2){
			$("#shiftDel").show();
			$("#recover").show();
			$('#mm').menu('show', {
				left : event.pageX,
				top : event.pageY
			});	
		}else if(!id){//表示添加根节点
			$('#mm2').menu('show',{
				left : event.pageX,
				top : event.pageY
			});
		}
	}

	function onClick(event, treeId, treeNode, clickFlag) {
		var childNodes = zTree.transformToArray(treeNode);
		var nodes = new Array();
		for (var i = 0; i < childNodes.length; i++) {
			nodes[i] = childNodes[i].id;
		}
		var ids = nodes.join(",");
		$('#resourceList').datagrid('clearSelections');
		$("#resourceList").datagrid("load", {
			"aids" : ids
		});
	}
	//初始化窗口右边的资源datagrid
	function getOption() {
		return {
			width : 'auto',
			height : 'auto',
			fitColumns : true,
			nowrap : false,
			striped : true,
			url : '${ctx}/sys/account/authority!resourceList.action',
			sortName : 'id',
			sortOrder : 'desc',
			remoteSort : true,
			idField : 'id',
			pageSize : 20,
			frozenColumns : [ [ {
				field : 'ck',
				checkbox : true
			} ] ],
			columns : [ [ {
				field : 'resourceName',
				title : '<s:text name="system.sysmng.resource.resourceName.title"/>',
				width : 60,
				sortable : true
			}, {
				field : 'name',
				title : '<s:text name="system.sysmng.resource.resourceSign.title"/>',
				width : 60,
				sortable : true
			}, {
				field : 'resourceNo',
				title : '<s:text name="system.sysmng.resource.resourceNo.title"/>',
				width : 30,
				sortable : true
			}, {
				field : 'resourceIcon',
				title : '资源图标名',
				width : 60,
				sortable : true
			},
			{
				field : 'resourceUrl',
				title : '<s:text name="system.sysmng.resource.resourceUrl.title"/>',
				width : 150,
				sortable : true
			},{
				field : 'openType',
				title : '<s:text name="system.sysmng.resource.resourceOpenType.title"/>',
				width : 50,
				align : 'center',
				formatter : function(value, rowData, rowIndex) {
					if (value == 1) {
						return "标签方式";
					} else if (value == 0) {
						return "窗口方式";
					} else {
						return "";
					}
				}
			},{
				field : 'resourceStatus',
				title : '<s:text name="system.sysmng.resource.resourceStatus.title"/>',
				width : 50,
				align : 'center',
				formatter : function(value, rowData, rowIndex) {
					if (value == 1) {
						return "有效";
					} else if (value == 0) {
						return "无效";
					} else if (value==2){
						return "已删除"
					}else {
						return "";
					}
				},
				styler:function(value,row,index){//设置样式
                    if (value ==2){
                        return 'color:#FF0000;';
                    }
            }
			}, {
				field : 'resourceType',
				title : '<s:text name="system.sysmng.resource.resourceType.title"/>',
				width : 50,
				align : 'center',
				formatter : function(value, rowData, rowIndex) {
					if (value == 1) {
						return "按钮";
					} else if (value == 0) {
						return "菜单";
					} else if (value == 2) {
						return "字段";
					} else if (value == 3){
						return "数据";
					} else {
						return "";
					}
				}
			}, /*{
				field : 'resourceLevel',
				title : '资源等级',
				width : 30,
				sortable : true
			}, */
			/*{
				field : 'createUser',
				title : '创建用户',
				width : 30,
				sortable : true
			},*/
			{
				field : 'createTime',
				title : '<s:text name="system.sysmng.desktop.createTime.title"/>',
				width : 50,
				sortable : true
			},{
				field : 'logRule',
				title : '<s:text name="system.sysmng.resource.resourceLogRule.title"/>',
				width : 50,
				sortable : true
			} ] ],
			pagination : true,
			rownumbers : true,
			toolbar : [ {
				id : 'bt_add',
				text : '<s:text name="system.button.add.title"/>',
				iconCls : 'icon-add',
				handler : function() {
					doAdd();
				}
			}, '-', {
				id : 'bt_mod',
				text : '<s:text name="system.button.modify.title"/>',
				iconCls : 'icon-edit',
				handler : function() {
					doModify();
				}
			}, '-', {
				id : 'bt_del',
				text : '<s:text name='system.button.delete.title'/>',
				iconCls : 'icon-remove',
				handler : function() {
					doDeleteIds();
				}
			}, '-', {
				id : 'bt_rec',
				text : '<s:text name='system.button.recover.title' />',
				iconCls : 'icon-redo',
				handler : function() {
					doRecoverResourceIds();
				}
			}, '-' ,{
				id : 'bt_sDel',
				text : '<s:text name='system.button.shiftDel.title' />',
				iconCls : 'icon-remove',
				handler : function() {
					doShiftDeleteIds();
				}
			}, '-'
			]
		};
	}

	function doAdd() {
		var res = $('#resourceList').datagrid('getSelections');
		if (res != null && res.length == 1) {
			var pId = res[0].id;
			initImageCombox();
			clearLocaleInput();
			$("#aId").val("");
			$("#pId").val(pId);
			$("#win_res_message").window("open");
		} else if (1 < res.length) {
			$.messager.alert('<s:text name="system.javascript.alertinfo.titleInfo"/>', '<s:text name="system.javascript.alertinfo.onlyOne"/>', 'info');
		} else if (res[0] == null) {
			$.messager.alert('<s:text name="system.javascript.alertinfo.titleInfo"/>', '<s:text name="system.javascript.alertinfo.recordInfo"/>', 'info');
		}

	}
	//树形添加资源节点
	function doAddTree() {
		var nodes = zTree.getSelectedNodes();
		var parentId = nodes[0].id;
		initImageCombox();
		clearLocaleInput();
		$("#aId").val("");
		$("#pId").val(parentId);
		$("#win_res_message").window("open");
	}
	//修改资源节点
	function modify(rid, isCopy) {
		var options = {
			url : '${ctx}/sys/account/authority!modifyInfo.action',
			data : {
				"id" : rid
			},
			success : function(data) {
				if (data.msg) {
					var json2fromdata = data.msg;
					if(isCopy) json2fromdata["id"] = "";
					$("#saveForm").form('load', json2fromdata);
					initImageCombox(json2fromdata.resourceIcon);
					//先清空数据
					clearLocaleInput();
					if(!isCopy) {
						$.each(json2fromdata["localeMap"], function(k, v) {
							var key =$("#locale_" + k);
							key.val(v["resourceName"]);
							key.attr("name","locale_"+k+"-"+v["id"]);
						});
					}
					$("#win_res_message").window("open");
				}
			}
		};
		fnFormAjaxWithJson(options);
	}
	//先清空国际化资源数据
	function clearLocaleInput() {
		$("input.localeInput").each(function(k,v) {
			$(this).attr("name", $(this).attr("id"));
		});
	}

	function modifyTree(isCopy) {
		var nodes = zTree.getSelectedNodes();
		var r_id = nodes[0].id;
		var parentId = nodes[0].pId;
		$("#pId").val(parentId);
		modify(r_id, isCopy);
	}
	function doModify() {
		var res = $('#resourceList').datagrid('getSelections');
		if (res != null && res.length == 1) {
			var r_id = res[0].id;
			var node = zTree.getNodeByParam("id", r_id, null);
			var parentId = node.pId;
			$("#pId").val(parentId);
			modify(r_id);
			$("#win_res_message").window("open");
		} else if (1 < res.length) {
			$.messager.alert('<s:text name="system.javascript.alertinfo.titleInfo"/>', '<s:text name="system.javascript.alertinfo.onlyOne"/>', 'info');
		} else if (res[0] == null) {
			$.messager.alert('<s:text name="system.javascript.alertinfo.titleInfo"/>', '<s:text name="system.javascript.alertinfo.recordInfo"/>', 'info');
		}
	}
	//保存资源
	function saveRes() {
		var options = {
			url : '${ctx}/sys/account/authority!save.action',
			success : function(data) {
				if (data.msg) {
				/* 	$.messager
							.alert(
									'<s:text name="system.javascript.alertinfo.title"/>',
									'<s:text name="system.javascript.alertinfo.succees"/>',
									'info'); */
					$('#win_res_message').window('close');
					$('#resourceList').datagrid('clearSelections');
					initMenu();
					$("#resourceList").datagrid(getOption());
				}
			}
		};
		fnAjaxSubmitWithJson('saveForm', options);
	}
	//根据选取的节点ID，删除资源
	function doDeleteIds(){
		var ids = [];
		var rows = $('#resourceList').datagrid('getSelections');
		for ( var i = 0; i < rows.length; i++) {
			ids.push(rows[i].id);
		}
			if (ids != null && ids.length > 0) {
				deleteIds(ids);
			} else {
					$.messager.alert(
							'<s:text name="system.javascript.alertinfo.title"/>',
							'<s:text name="system.javascript.alertinfo.question"/>',
							'info');
				}
	}
	//彻底删除节点资源
	function doShiftDeleteIds() {
		var ids = [];
		var rows = $('#resourceList').datagrid('getSelections');
		for ( var i = 0; i < rows.length; i++) {
			if(rows[i].resourceStatus == 2){
				ids.push(rows[i].id);
			}else{
				return	$.messager.alert(
						'<s:text name="system.javascript.alertinfo.title"/>',
						'请选择资源状态为<已删除>的资源才能被彻底删除!',
						'info');
			}
		}
			if (ids != null && ids.length > 0) {			
				shiftDeleteIds(ids);
			} else {
					$.messager.alert(
							'<s:text name="system.javascript.alertinfo.title"/>',
							'<s:text name="system.javascript.alertinfo.question"/>',
							'info');
				}
	}
	//恢复节点资源
	function doRecoverResourceIds(){
		var ids = [];
		var rows = $('#resourceList').datagrid('getSelections');
		for ( var i = 0; i < rows.length; i++) {
			ids.push(rows[i].id);
		}
			if (ids != null && ids.length > 0) {
				recoverResource(ids);	
			} else {
					$.messager.alert(
							'<s:text name="system.javascript.alertinfo.title"/>',
							'请选择要恢复的记录\!',
							'info');
				}
	}

	function shiftDeleteIds(ids) {
			$.messager
					.confirm(
							'<s:text name="system.javascript.alertinfo.title"/>',
							'<s:text name="system.javascript.alertinfo.info"/>',
							function(r) {
								if (r) {
									var options = {
										url : '${ctx}/sys/account/authority!shiftDelete.action',
										data : {
											"rids" : ids
										},
										success : function(data) {
											if (data.msg){
												$('#resourceList').datagrid('clearSelections');										
												initMenu();
												$("#resourceList").datagrid(getOption());
											}
										},
										traditional : true
									};
									fnFormAjaxWithJson(options,true);
								}
							});		
	}
	function deleteIds(ids) {
		$.messager
				.confirm(
						'<s:text name="system.javascript.alertinfo.title"/>',
						'<s:text name="system.javascript.alertinfo.info"/>',
						function(r) {
							if (r) {
								var options = {
									url : '${ctx}/sys/account/authority!delete.action',
									data : {
										"rids" : ids

									},
									success : function(data) {
										if (data.msg) {
											$('#resourceList').datagrid(
													'clearSelections');										
										/* 	$.messager
													.alert(
															'<s:text name="system.javascript.alertinfo.title"/>',
															'<s:text name="system.javascript.alertinfo.succees"/>',
															'info'); */
											initMenu();
											$("#resourceList").datagrid(
													getOption());
										}
									},
									traditional : true
								};
								fnFormAjaxWithJson(options,true);
							}
						});		
	}
	
	function recoverResource(ids){
		$.messager
				.confirm(
						'<s:text name="system.javascript.alertinfo.title"/>',
						'确定要恢复该资源的状态吗?',
						function(r) {
							if (r) {
								var options = {
									url : '${ctx}/sys/account/authority!recover.action',
									data : {
										"rids" : ids

									},
									success : function(data) {
										if (data.msg) {
											$('#resourceList').datagrid(
													'clearSelections');										
										/* 	$.messager
													.alert(
															'<s:text name="system.javascript.alertinfo.title"/>',
															'<s:text name="system.javascript.alertinfo.succees"/>',
															'info'); */
											initMenu();
											$("#resourceList").datagrid(
													getOption());
										}
									},
									traditional : true
								};
								fnFormAjaxWithJson(options,true);
							}
						});
	}
	function delTree() {
		var nodes = zTree.getSelectedNodes();
		var r_id = nodes[0].id;
		if(r_id!=null){
			deleteIds(r_id);	
		}
	}
	function ShiftDelTree() {
		var nodes = zTree.getSelectedNodes();
		var r_id = nodes[0].id;
		if(r_id!=null){
			shiftDeleteIds(r_id);	
		}
	}
	
	function recoverTree(){
		var nodes = zTree.getSelectedNodes();
		var r_id = nodes[0].id;
		if(r_id!=null){
			recoverResource(r_id);	
		}
	}
	function cancelAdd() {
		$('#win_res_message').window('close');
	}

	$(function() {
		$("#resourceList").datagrid(getOption());
		initMenu();
		
		$("#_dictTree_qd_").bind('keydown', 'return',function (evt){evt.preventDefault(); _doDictTreeQuery_(); return false; });

		$("#win_res_message").window({
			width:600,
			height:500,
			onOpen: function() {
				$(this).window("move", {
					top:($(window).height()-500)*0.5,
					left:($(window).width()-600)*0.5
				});
			},
			onClose : function() {
				$('#saveForm').form('clear');
				//$('#saveForm')[0].reset();
				$("#aId").val("");
				$("#pId").val("");
			}
		});
	});
	//初始化资源图标
	function initImageCombox(selVal){
		$("#resourceIcon").combobox({
			url: "${ctx}/js/easyui/icons/iconSkin-data.json",
		    valueField:'id',   
		    textField:'text',
		    formatter:function(row){  
		    	return "<span style='width: 16px;height:16px;display:inline-block;' class='"+row.id+"'></span><span class='item-text'>"+row.text+"</span>";
		     } 
		});
		if(selVal){
			$("#resourceIcon").combobox("setValue",selVal);
		}
	}
	
	//选中或取消选中
	function doCheckNodesOnTree(treeId, param) {
		var treeObj = $.fn.zTree.getZTreeObj(treeId);
		treeObj.cancelSelectedNode();
		if(!param) {
			return;
		}
		var nodes = treeObj.getNodesByParamFuzzy("name", param, null);
		//for (var i=0, l=nodes.length; i < l; i++) {
		for (var i=nodes.length-1; i >= 0; i--) {	
			treeObj.selectNode(nodes[i],true);
		}
	}
	
	function _doDictTreeQuery_() {
		var val = $("#_dictTree_qd_").val();
		doCheckNodesOnTree("authorityTree", val);
	}
</script>
</head>
<body class="easyui-layout">
	<div region="west" split="true" title="<s:text name="system.sysmng.resource.authority.title"/>"
		style="width: 250px; padding: 10px;" border="false">
		<div>
			<input type="hidden" name="authorityId" id="authorityId" />
			<div><input type="text" id="_dictTree_qd_" style="width:130px" class="Itext"/>&nbsp;&nbsp;<button type="button" class="button_small" onclick="_doDictTreeQuery_()">树查询</button></div>
			<ul id="authorityTree" class="ztree"></ul>
		</div>
		<div id="mm" class="easyui-menu" style="width: 120px;">
			<div onclick="doAddTree()" iconCls="icon-add">
				<s:text name="system.sysmng.user.add.title" />
			</div>
			<div onclick="modifyTree()" iconCls="icon-edit">
				<s:text name="system.sysmng.user.modify.title" />
			</div>
			<div onclick="modifyTree(true)" iconCls="icon-copy">
				<s:text name="system.button.copy.title" />
			</div>
			<div onclick="delTree()" iconCls="icon-remove">
				<s:text name="system.sysmng.user.delete.title" />
			</div>
			<div id="recover" onclick=" recoverTree()" iconCls="icon-redo">
				<s:text name='system.button.recover.title' />
			</div>
			<div  id="shiftDel" onclick="ShiftDelTree()" iconCls="icon-remove">
			<s:text name='system.button.shiftDel.title' />
			</div>
		</div>
		<div id="mm2" class="easyui-menu" style="width: 120px;">
			<div onclick="doAddTree()" iconCls="icon-add">
				<s:text name="system.sysmng.user.add.title" />
			</div>
		</div>
	</div>
	<div region="center" title="<s:text name="system.sysmng.resource.list.title"/>" border="false">
		<table id="resourceList" fit="true" border="false">
		</table>
	</div>

	<div>
		<!-- Form表单 -->
		<div id="win_res_message" closed="true"
			modal="true" title="<s:text name="system.sysmng.authority.authorityRecord.title"/>" iconCls="icon-save"
			style="padding: 0px; background: #fafafa;">
			<div class="easyui-layout" fit="true">
				<div region="center" border="false"
					style="padding: 2px; background: #fff; overflow: hidden; border: 1px solid #ccc;">
					<form action="" name="saveForm" id="saveForm" method="post">
						<input type="hidden" name="id" id="aId" /> 
						<input type="hidden" name="parentId" id="pId" />
						<table align="center" border="0" cellpadding="0" cellspacing="1"
							class="table_form">
							<tbody>
								<tr>
									<th><label for="resourceName"><s:text name="system.sysmng.resource.resourceName.title"/>:</label></th>
									<td><input class="easyui-validatebox Itext" required="true"
										validType="length[1,100]" type="text" name="resourceName"
										id="resourceName"></input></td>
									<th><label for="name"><s:text name="system.sysmng.resource.resourceSign.title"/>:</label></th>
									<td><input class="easyui-validatebox Itext" 
										validType="length[1,100]" type="text" name="name"
										id="resourceSign"></input>
									</td>
								</tr>
								<tr>
									<th><label for="resourceIcon"><s:text name="system.sysmng.resource.resourceIcon.title"/></label></th>
									<td><input class="easyui-combobox" type="text" name="resourceIcon" id="resourceIcon" style="width:160px;"></input></td>
									<th><label for="resourceNo"><s:text name="system.sysmng.resource.resourceNo.title"/>:</label></th>
									<td ><input class="easyui-numberbox" required="true"
										validType="length[1,10]" type="text"
										name="resourceNo" id="resourceNo"></input></td>
								</tr>
								
									<tr>
									<th><label for="resourceLevel"><s:text name="system.sysmng.resource.resourceLevel.title"/>:</label></th>
									<td><input class="easyui-numberbox" min="1"
										required="true" validType="length[1,2]" type="text"
										name="resourceLevel" id="resourceLevel"></input></td>
									<th><label for="logRule"><s:text name="system.sysmng.resource.resourceLogRule.title"/>:</label></th>
									<td><input class="easyui-validatebox Itext" 
										validType="length[1,1000]" type="text" name="logRule"
										id="logRule"></input></td>
								</tr>
								<tr>
									<th><label for="openType"><s:text name="system.sysmng.resource.resourceOpenType.title"/>:</label></th>
									<td colspan="3"><select class="easyui-validatebox Itext"
										required="true" name="openType" id="openType"
										style="width: 150px;">
											<option value="0">窗口方式</option>
											<option value="1">标签方式</option>		
									</select></td>
								</tr>
								<tr>
									<th><label for="resourceType"><s:text name="system.sysmng.resource.resourceType.title"/>:</label></th>
									<td colspan="1"><select class="easyui-validatebox Itext"
										required="true" name="resourceType" id="resourceType"
										style="width: 150px;">
											<option value="0">菜单</option>
											<option value="1">按钮</option>
											<option value="2">字段</option>
											<option value="3">数据</option>
									</select></td>

									<th><label for="resourceStatus"><s:text name="system.sysmng.resource.resourceStatus.title"/>:</label>
									</th>
									<td colspan="1"><select class="easyui-validatebox Itext"
										required="true" name="resourceStatus" id="resourceStatus"
										style="width: 150px;">
											<option value="1"><s:text name="system.button.enabled.title"/></option>
											<option value="0"><s:text name="system.button.disable.title"/></option>
									</select>
									</td>
								</tr>

								<tr>
								<th><label for="resourceUrl"><s:text name="system.sysmng.resource.resourceUrl.title"/>:</label></th>
									<td colspan="3"><input class="easyui-validatebox Itext" 
								style="width: 400px;" validType="length[1,500]" type="text" name="resourceUrl"
										id="resourceUrl"></input>
									</td>
								</tr>
								<tr>
									<th><label for="resourceLocale"><s:text name="system.sysmng.resource.resourceLanguage.title"/>:</label>
									</th>
									<td colspan="3">
								<s:iterator
									value="%{@com.qyxx.platform.sysmng.dictmng.web.DefinitionCache@getSet('SYS_LOCALE_DICT','zh_CN')}">					
										<div>
											<input
											class="easyui-validatebox localeInput Itext" id="locale_<s:property value='value'/>"
											validType="length[1,100]" type="text" name="locale_<s:property value='value'/>"
											></input>
											<label>
											<s:property value='displayName'/></label>
										</div>
								</s:iterator>
									</td>
								<tr>
									<th><label for="remark"><s:text name="system.sysmng.resource.resourceRemark.title"/>:</label></th>
								<td colspan="3"><textarea name="remark" id="remark"
											type="text" class="easyui-validatebox" 
											style="width: 200px; height: 100px;"
											validType="length[1,500]"></textarea></td>
								</tr>
								
							</tbody>
							
						</table>

					</form>
				</div>
				<div region="south" border="false"
					style="text-align: right; height: 30px; line-height: 30px;">
					<a class="easyui-linkbutton" iconCls="icon-ok"
						href="javascript:void(0)" onclick="saveRes()"><s:text
							name="system.button.save.title" /> </a> <a class="easyui-linkbutton"
						iconCls="icon-cancel" href="javascript:void(0)"
						onclick="cancelAdd();"><s:text
							name="system.button.cancel.title" /> </a>
				</div>
			</div>
		</div>

	</div>
</body>
</html>