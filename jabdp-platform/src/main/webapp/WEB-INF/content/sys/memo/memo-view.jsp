<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
 <title><s:text name="system.index.title" /></title>
 <%@ include file="/common/meta-gs.jsp"%>
 <!-- 备忘录css -->
 <style type="text/css">
  .mycheckbox {
   margin-right:20px;
   position:relative;
   top:2px;
  }
 </style>
</head>
<body class="easyui-layout" fit="true" title="maintable-view">
 <div region="north" style="overflow: hidden;" border="false" id="toolbar_layout">
  <div class="datagrid-toolbar">
   <a id="tclose" href="javascript:doCloseCurrentTab();" class="easyui-linkbutton" plain="true" iconCls="icon-cancel">
	<s:text name="system.button.close.title"/>
   </a>
   <a id="tcancel" href="javascript:void(0);" onclick="doCancelEdit()" class="easyui-linkbutton" plain="true" iconCls="icon-undo">
    <s:text name="system.button.cancel.title"/>
   </a>		
   <a id="tsave" href="javascript:saveObj('0');" class="easyui-linkbutton" plain="true" iconCls="icon-save">
    <s:text name="system.button.save.title"/><s:text name="system.sysmng.news.draft.title"/>
   </a>
   <a id="tadd" href="javaScript:doAdd();" class="easyui-linkbutton" plain="true" iconCls="icon-add">
    <s:text name="system.button.add.title"/>
   </a>
   <a id="tedit" href="#" onclick="setFormToEdit();" class="easyui-linkbutton" plain="true" iconCls="icon-edit">
    <s:text name="system.button.modify.title"/>
   </a>
   <a id="tcopy" href="javaScript:doCopyMainTab();" class="easyui-linkbutton" plain="true" iconCls="icon-copy">
    <s:text name="system.button.copy.title"/>
   </a>
   <a id="release" href="javaScript:doRepeal('1');" class="easyui-linkbutton" plain="true" iconCls="icon-ok">
    <s:text name="正式发布"/>
   </a>
   <a id="disrelease" href="javaScript:doRepeal('0');" class="easyui-linkbutton" plain="true" iconCls="icon-undo">
    <s:text name="取消发布"/>
   </a>
   <%-- <a id="tattach" href="javaScript:doAccessory();" class="easyui-linkbutton" plain="true" iconCls="icon-attach">
    <s:text name='system.button.accessory.title'/>
   </a>	--%>
  </div>
 </div>
 <div region="center" style="position: relative; overflow: auto;" border="false">
  <form action="" method="post" name="viewForm" id="viewForm" style="margin:0px;">
   <input type="hidden" id="status" name="status" value="0"/>
   <input type="hidden" id="id" name="id" value/>
   <table border="0" cellpadding="0" cellspacing="1" class="table_form">
	<tr>
	 <th><label><s:text name="主题"/>：</label></th>
	 <td colspan="5">
	  <input name="title" id="title" class="easyui-validatebox Itext" required="true" style="width:900px"/>
	 </td>
	</tr>
	<tr>
	 <th><label><s:text name="内容"/>：</label></th>
	 <td colspan="5">
	  <textarea id="content" name="content" class="jquery_ckeditor" style="height:600px;"></textarea>
	 </td>
	</tr>
	<tr>
	 <th><label><s:text name="定时提醒"/>：</label></th>
	 <td>
	  <select name="remindType" id="remindType">
	   <option value="指定时刻">指定时刻</option>
	   <option value="每周提醒">每周提醒</option>
	   <option value="每天同一时刻">每天同一时刻</option>
	  </select>
	 </td>
	 <th><label><s:text name="时间"/>：</label></th>
	 <td>
	  <input name="remindTime" id="remindTime"/>
	 </td>
	 <th><label><s:text name="日期"/>：</label></th>
	 <td>
	  <input name="remindDate" id="remindDate" class="Wdate Itext" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})"/>
	 </td>
	</tr>
	<tr>
     <th><label><s:text name="每周提醒"/>：</label></th>
     <td colspan="5">
      <input type="hidden" id="remindWeek" name="remindWeek"/>
	  <span><s:text name="周一"/>：</span><input remindWeek="1" type="checkbox" class="mycheckbox"/>
	  <span><s:text name="周二"/>：</span><input remindWeek="2" type="checkbox" class="mycheckbox"/>
	  <span><s:text name="周三"/>：</span><input remindWeek="3" type="checkbox" class="mycheckbox"/>
	  <span><s:text name="周四"/>：</span><input remindWeek="4" type="checkbox" class="mycheckbox"/>
	  <span><s:text name="周五"/>：</span><input remindWeek="5" type="checkbox" class="mycheckbox"/>
	  <span><s:text name="周六"/>：</span><input remindWeek="6" type="checkbox" class="mycheckbox"/>
	  <span><s:text name="周日"/>：</span><input remindWeek="7" type="checkbox" class="mycheckbox"/>
	 </td>
	</tr>
	<tr>
     <th><label><s:text name="分享人选"/>：</label></th>
     <td colspan="5">
	  <input type="text" name="memoToUserIds" id="memoToUserIds"  style="width:900px;"/>
	  <!-- <ul id="userTree" class="ztree"></ul> -->
	 </td>
	</tr>
   </table>
  </form>
 </div>
<script type="text/javascript">
	//定义一个全局变量保存操作状态
	var operMethod = "${param.operMethod}" || "view";
	function doInitForm(data, callFunc) {
		if(data.msg) {
			var jsonData = data.msg;
			if(operMethod == "edit") {
				addState();
				jsonData["status"] = "0";
				delete jsonData.id;
			} else if(operMethod == "view") {
				viewState();
				if (jsonData["status"] == "0") { //待发布
					publishState();
				} else if (jsonData["status"] == "1") { //已发布
					revokeState();
				}
			}
			//加载ztree被选中节点
			//initTreeChecked(jsonData.memoToUserIds);
			//对数据库直接取出来的时间进行格式化
			if("指定时刻" == jsonData["remindType"]) {
				if(jsonData["remindDate"] != null) {
					jsonData["remindDate"] = jsonData["remindDate"].replace("00:00:00", "");
				}
			} else {
				delete jsonData.remindDate;
			}
			//easyui方法加载表单数据
			$('#viewForm').form('load', jsonData);
			$("#memoToUserIds").dialoguewindow("setValue", jsonData.memoToUserIds);
		}
		if(callFunc) {
			callFunc();
		}
	}

	//设置焦点
	function focusFirstElement() {
		focusEditor("title");
	}

	//将控件设置成可编辑状态
	function setFormToEdit() {
		addState();
		$("#tcancel").show();
		operMethod = "view";
		doEnable();
		parent.doModifyTabTitle('<s:text name="编辑备忘录"/>-' + $("#id").val());
	}

	//点击复制按钮触发
	function doCopyMainTab() {
		var id = $("#id").val();
		if(id == null) {
			$.messager.alert(
				'<s:text name="system.javascript.alertinfo.title"/>',
				'<s:text name="system.javascript.alertinfo.copy"/>',
				'info');
			return;
		} else {
			parent.addTab(
				'<s:text name="复制备忘录"/>-' + id,
				'${ctx}/sys/memo/memo-view.action?operMethod=edit&id=' + id);
		}
	}

	//页面数据初始化
	function doModify(id, callFunc) {
		var options = {
			url : '${ctx}/sys/memo/memo!view.action',
			data : {
				"memoId" : id
			},
			success : function(data) {
				doInitForm(data, callFunc);
			}
		};
		fnFormAjaxWithJson(options, true);
	}
	
	function initTreeChecked(userIdStr) {
		if (userIdStr && userIdStr.length > 0) {
			var userIds = userIdStr.split(",");
			var u_zTree = $.fn.zTree.getZTreeObj("userTree");
			if (u_zTree) {
				$.each(userIds, function(k, v) {
					var node = u_zTree.getNodeByParam("id", "user_" + v);
					if (node) {
						u_zTree.checkNode(node, true, true);
					}
				});
			}
		}
	}

	//将ztree中信息，获取至input元素
	function getIds() {
		/*var u_zTree = $.fn.zTree.getZTreeObj("userTree");
		var u_nodes = u_zTree.getCheckedNodes(true);
		var u_nodeIds = [];
		$.each(u_nodes, function(k, v) {
			if(v.userId) {
				u_nodeIds.push(v.userId);	
			}
		});
		$("#memoToUserIds").val(u_nodeIds.join(","));*/
	}

	//将复选框的值组合成字符串放到input元素里
	function getRemind() {
		var val = [];
		$("input:checked").each(function() {
			val.push($(this).attr("remindWeek"));
		});
		$("#remindWeek").val(val.join(","));
	}
	
	function saveObj(st) {
		if (st == "1") {
			$("#status").val("");
			$("#status").val("1");
		} else if (st == "0") {
			$("#status").val("");
			$("#status").val("0");
		}
		getIds();//将ztree中信息，获取至input元素
		getRemind();//将复选框的值组合成字符串放到input元素里
		var options = {
			url : '${ctx}/sys/memo/memo!save.action',
			success : function(data) {
				operMethod = "view";
				parent.doModifyTabTitle('<s:text name="查看备忘录"/>-' + data.msg);
				doModify(data.msg, doAfterSave);
			}
		};
		fnAjaxSubmitWithJson("viewForm", options);
	}

	//修改备忘录状态
	function doRepeal(st) {
		if (st == "1") {
			$("#status").val("1");
		} else if (st == "0") {
			$("#status").val("0");
		}
		var options = {
			url : '${ctx}/sys/memo/memo!updateStatus.action',
			data : {
				memoStatus : st,
				memoId : $("#id").val()
			},
			success : function(data) {
				doModify($("#id").val(), doAfterSave);
			}
		};
		fnFormAjaxWithJson(options, true);
	}

	function doAfterSave() {
		if (window.top.refreshTabData) {
			window.top.refreshTabData("<s:text name='新增备忘录'/>");
		}
		operMethod = "view";
		doDisabled();
	}

	//将页面上的控件置为可编辑状态
	function doEnable() {
		$("input[type!=hidden],textarea,select").removeAttr("disabled");
		$("#remindType").combobox('enable');
		$("#remindTime").timespinner("enable");
		$(".jquery_ckeditor").each(function() {
			try {
				$(this).ckeditorGet().setReadOnly(false);
			} catch (e) {
				$(this).ckeditorGet().setReadOnly(false);
			}
		});
		/*var zTree = $.fn.zTree.getZTreeObj("userTree");
		zTree.setting.callback.beforeCheck = function(treeId, treeNode) {
		    return true;
		};*/
		initRemindControl();
		focusFirstElement();
	}

	//将页面上的控件置为不可编辑状态
	function doDisabled() {
		$("input[type!=hidden],textarea,select").attr("disabled", "disabled");
		$("#remindType").combobox("disable");
		$("#remindTime").timespinner("disable");
		$(".jquery_ckeditor").each(function() {
			try {
				$(this).ckeditorGet().setReadOnly(true);
			} catch (e) {
				$(this).ckeditorGet().setReadOnly(true);
			}
		});
		/*var zTree = $.fn.zTree.getZTreeObj("userTree");
		zTree.setting.callback.beforeCheck = function(treeId, treeNode) {
		    return false;
		};*/
	}

	//查看里的新增
	function doAdd() {
		parent.addTab('新增备忘录', '${ctx}/sys/memo/memo-view.action?operMethod=add');
	}

	//更新附件ID flag: mainQuery-主实体查询，subEdit-子实体编辑，mainEdit-主实体编辑
	/* function doUpdateAttach(opts) {
		var options = {
			url : '${ctx}/sys/msg/mail!updateAttach.action',
			data : {
				"id" : opts.id,
				"atmId" : opts.attachId
			},
			async : false,
			success : function(data) {
				if (data.msg) {
					 $("#atmId").val(opts.attachId);
					 if (parent.refreshTabData) {
							parent.refreshTabData("<s:text name='邮件管理'/>");
					 }
				}
			}
		};
		fnFormAjaxWithJson(options, true);
	} */
	
	function doCloseCurrentTab() {
		if(window.top.closeCurrentTab) {
			window.top.closeCurrentTab();
		}
    }
	
	function addState() {//新增状态  只显示“关闭”，“保存”
		hideTool();
		showSave();
	}
	
	function viewState() {//(草稿)的查看状态  
		hideTool();
		hideSave();
	}
	
	function publishState() {//待发布状态，显示编辑/发布，不显示取消发布
		$("#tedit").show();
		$("#release").show();
		$("#disrelease").hide();
	}
	
	function revokeState() {//撤销发布状态，不显示编辑/发布，显示取消发布
		$("#tedit").hide();
		$("#disrelease").show();
		$("#release").hide();
	}

	function showSave(){
	    $("#tclose").show();
  	    $("#tsave").show();
    }
    
    function hideSave(){
	    $("#tclose").show();
	    $("#tadd").show();
	  	$('#tedit').show();
	  	$('#tcopy').show();
	  	$("#release").show();
    }
    
    //隐藏工具条
    function hideTool() {
  	    $("div.datagrid-toolbar a", "#toolbar_layout").hide();
    }
	
	//取消修改
	function doCancelEdit() {
		operMethod="view";
		doModify($("#id").val(), doDisabled);
	}
	
	//初始化分享人选
	function initUserZtree() {
		/*var options = {
			url : '${ctx}/sys/account/authority!userList.action',
			data : {
				orgId : "${session.USER.organizationId}"
			},
			async : false,
			success : function(data) {
				if (data.msg) {
					var settingZtree = {
						check : {
							enable : true
						},
						data : {
							simpleData : {
								enable : true
							}
						}
					};
					$.fn.zTree.init($("#userTree"), settingZtree, data.msg);
				}
			}
		};
		fnFormAjaxWithJson(options, true);*/
		$("#memoToUserIds").dialoguewindow({
 			title:"用户",
 			url:"/sys/dataFilter/user-select.action",
 			queryParams:{"orgId" : "${session.USER.organizationId}"},
 			filterUrl:"${ctx}/sys/account/user!queryUser.action",
 			filterParam:"filter_INL_id",
 			keyMap:{"idKey":"id","textKey":"nickName"}
 		});
		
	}
	
	//初始化表单元素逻辑
	/* function initFromObj1() {
		var remindTime = $("#remindTime");
		var remindDate = $("#remindDate");
		var inputcheck = $("input[type='checkbox']");
		var now = new Date();
		var remindTimeStr = now.getHours() + ":" + now.getMinutes();
		remindTime.timespinner();
		remindTime.timespinner("setValue",remindTimeStr);
		remindDate.val("");
		remindDate.attr("disabled",true);
		inputcheck.attr("checked",false);
		inputcheck.attr("disabled",false);
		$("#remindType").combobox({
			onSelect : function(record) {
				kongzhishifoukexuan(record.value);
			}
		}).combobox("setValue", "每周提醒");
		initUserZtree();//初始化分享人选
		$('.jquery_ckeditor').ckeditor();//初始化ckeditor
	} */
	
	//提醒时间相关控件关联规则
	function initRemindControl(value) {
		var remindDate = $("#remindDate");
		var inputcheck = $("input[type='checkbox']");
		if(!value) {
			value = $("#remindType").combobox("getValue");
		}
		if(value == "每周提醒") {
			remindDate.val("");
			remindDate.attr("disabled", true);
			inputcheck.attr("checked", true);
			inputcheck.attr("disabled", false);
		} else if(value == "每天同一时刻") {
			remindDate.val("");
			remindDate.attr("disabled", true);
			inputcheck.attr("checked", true);
			inputcheck.attr("disabled", true);
		} else if(value == "指定时刻") {
			var remindDateStr = remindDate.val();
			if(!remindDateStr || remindDateStr != "") {
				var month = "";
				var day = "";
				var now = new Date();
				now.getMonth()+1 < 10 ? month = "0" + (now.getMonth()+1) : month = now.getMonth()+1;
				now.getDate() < 10 ? day = "0" + now.getDate() : day = now.getDate();
				remindDateStr = now.getFullYear() + "-" + month + "-" + day;
			}
			remindDate.val(remindDateStr);
			remindDate.removeAttr("disabled");
			inputcheck.attr("checked", false);
			inputcheck.attr("disabled", true);
		}
	}
	
	//初始化表单控件
	function initFromObj() {
		$("#remindTime").timespinner({
			showSeconds : false
		});//初始化
		$("#remindType").combobox({
			width : 160,
			onSelect : function(record) {
				initRemindControl(record.value);
			}
		});//初始化
		$('.jquery_ckeditor').ckeditor();//初始化ckeditor
		initUserZtree();//初始化分享人选
	}
	
	$(function() {
		initFromObj();//初始化表单控件
		if(operMethod == "add") {
			//新增
			addState();//显示工具条的关闭与保存
			focusFirstElement();//设置焦点
			$("#remindType").combobox("setValue", "指定时刻");//新增页面默认
			var now = new Date();
			$("#remindTime").timespinner("setValue",now.getHours() + ":" + now.getMinutes());
			initRemindControl();//初始化提醒时间相关控件关联规则
		} else if(operMethod == "view") {
			//查看
			doModify("${param.id}", doDisabled);
		} else if(operMethod == "edit") {
			//查询页点击复制触发
			doModify("${param.id}", doEnable);
		}
	});
	
</script>
</body>
</html>
