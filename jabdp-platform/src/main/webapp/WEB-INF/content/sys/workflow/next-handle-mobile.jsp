<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<%@ include file="/common/meta.jsp"%>
<style type="text/css">
.button {
	display: inline-block;
	outline: none;
	cursor: pointer;
	text-align: center;
	text-decoration: none;
	font: 16px/100% 'Microsoft yahei',Arial, Helvetica, sans-serif;
	padding: .5em 2em .55em;
	text-shadow: 0 1px 1px rgba(0,0,0,.3);
	-webkit-border-radius: .5em; 
	-moz-border-radius: .5em;
	border-radius: .5em;
	-webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
	-moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
	box-shadow: 0 1px 2px rgba(0,0,0,.2);
}
.button:hover {
	text-decoration: none;
}
.button:active {
	position: relative;
	top: 1px;
}
/* white */
.white {
	color: #606060;
	border: solid 1px #b7b7b7;
	background: #fff;
	background: -webkit-gradient(linear, left top, left bottom, from(#fff), to(#ededed));
	background: -moz-linear-gradient(top,  #fff,  #ededed);
	filter:  progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#ededed');
}
.white:hover {
	background: #ededed;
	background: -webkit-gradient(linear, left top, left bottom, from(#fff), to(#dcdcdc));
	background: -moz-linear-gradient(top,  #fff,  #dcdcdc);
	filter:  progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#dcdcdc');
}
.white:active {
	color: #999;
	background: -webkit-gradient(linear, left top, left bottom, from(#ededed), to(#fff));
	background: -moz-linear-gradient(top,  #ededed,  #fff);
	filter:  progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#ffffff');
}
</style>
 </head>  
 <body>		
	<!-- 设置自定义选择审批人页面 -->
	<c:if test="${param.method == 'doApprove'}">
	 <div id="selApproveUserDiv" style="width:100%;">
		<div 
			style="width:100%;">
			<form action="" name="handleForm" id="handleForm" method="post">
				<input type="hidden" id="taskId" name="taskId" value="${param.id}" />
				<input type="hidden" id="taskName" name="taskName"
					value="${param.name}" /> <input type="hidden" id="executionId"
					name="executionId" value="${param.execId}" /> <input
					type="hidden" id="processInstanceId" name="processInstanceId"
					value="${param.proInsId}" /> <input type="hidden"
					id="approve" name="activiti_approve"/> <input type="hidden"
					id="resend" name="activiti_resend" />
				<table align="center" style="width:100%;">
					<tbody>
						<tr id="userSel">
							<th><label><s:text
										name="system.sysmng.process.customUser.title" />:</label></th>
							<td><input type="hidden" name="userIds" id="userIds" />
								<ul style="text-align: center; width: 90%;"
									id="roleIds" class="ztree"></ul></td>
						</tr>
						<tr id="reason">
							<th><label><s:text
										name="意见（原因）" />：</label></th>
							<td><textarea name="reason"
									style="text-align: left; width:90%;height:150px;" placeholder="请填写内容"></textarea>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</div>
		<div style="text-align: center;padding:5px;">
				<a href="javascript:void(0);" onclick="doComplete()" class="button white"><s:text
						name="system.button.submit.title" /></a> 
				<a href="javascript:void(0);"
					onclick="doCloseDialog()" class="button white"><s:text name="system.button.cancel.title" />
				</a>
		</div>
	</div>
	</c:if>
	<c:if test = "${param.method == 'doDesc'}">
	<!-- 设置任务代办描述 -->
	<div  id="despDiv"
		style="padding: 0px;width:100%;">
			<form action="" name="descriptionForm" id="descriptionForm" method="post">
					<table align="center" style="width:100%;">
						<tbody>	
						<tr id="descriptionContent">
							<th><label ><s:text name="描述内容"/>:</label>
								</th>
								<td>
							<textarea style="text-align: left; width: 90%;height:150px;" name="description" id ="description" placeholder="请填写内容"></textarea>	
							</td>			
						</tr>			
					</tbody>	
				</table>
			</form>	
			<div  style="text-align: center;padding:5px;">
				<a class="button white" href="javascript:void(0);" onclick="doDescription(${param.id})"><s:text name="system.button.submit.title"/></a> 
				<a class="button white" href="javascript:void(0);" onclick="doCloseDialog()"><s:text name="system.button.cancel.title"/></a>
			</div>	
	</div>
	</c:if>
<script type="text/javascript">
	$(document).ready(function() {
		<c:if test="${param.method == 'doApprove'}">
			approveMenu("${param.op}","${param.bn}");
		</c:if>
	});

    var setting = {
		check: {
			enable: true
		},
		data: {
			simpleData: {
				enable: true
			}
		}
	};

	//选人窗口
 function approveMenu(name,bn)  {
		$("#" + name).val(bn);
	    var pId = $("#processInstanceId").val();
		var options = {
				url : '${ctx}/gs/process!getRolesList.action',
				data : {
					"processInstanceId":pId,
					"taskId": "${param.id}",
					"variableName" : name,
					"value" :bn
				},
				async:false,
				success : function(data) {
					$("#userSel").hide();
					var chkFlag = false;//检查是否自动完成
					if (data.msg){
						if($.isArray(data.msg)){
							if(data.msg.length>1){//多个用户
								$("#userSel").show();
								$.fn.zTree.init($("#roleIds"), setting, data.msg);
								zTree = $.fn.zTree.getZTreeObj("roleIds");
								zTree.expandAll(true);
								chkFlag = true;
							}
						} else {//一个用户
							$("#userIds").val(data.msg);	
						}
					}
					/*if(!chkFlag && name == "resend") { //重新发送
						doComplete();
					}*/
				}
			};
			fnFormAjaxWithJson(options,true);
	} 

//获取用户树中选择的ID
 function getIds(){
		var zTree = $.fn.zTree.getZTreeObj("roleIds");
		if(zTree!=null){
			var nodes = zTree.getCheckedNodes(true);
			if(nodes.length){
				var nodeIds = [];
				$.each(nodes, function(k,v) {
					nodeIds.push(v.id);
				});
				$("#userIds").val(nodeIds.join(","));				
			}else{
				$.messager.alert('提示信息','至少选择一个办理人员','info');
				return false;
			}
		}
		return true;
	}

 //完成任务
 function doComplete(){
	 var flag = true;	
	 flag = getIds();
	 if(flag){
			var options = {
					url : '${ctx}/gs/process!complete.action',
					success : function(data) {
						if (data.msg) {
							 doClose();
						}
					}
				};
			fnAjaxSubmitWithJson('handleForm', options);
	}	 
 }

	function doClose() {
			var partenW = window.parent;
            var parentDiv = partenW.document.getElementById('contextForOper');     
            var parentModel = partenW.justep.Bind.contextFor(parentDiv).$model;  
			parentModel.closePage();
	}
	
	function doCloseDialog() {
		var partenW = window.parent;
            var parentDiv = partenW.document.getElementById('contextForOper');     
            var parentModel = partenW.justep.Bind.contextFor(parentDiv).$model;  
			parentModel.hideMapBtnClick();
	}
 //打开任务描述窗口
 function doDescription(taskId){
	 var pId = $("#processInstanceId").val();
		var options = {
				url : '${ctx}/gs/process!setTaskDescription.action',
				data : {
					"taskId" : taskId,
					"processInstanceId" : pId
				},
				success : function(data) {
					if (data.msg) {
						doCloseDialog();
					}
				}
			};
		fnAjaxSubmitWithJson('descriptionForm', options);
 }
 </script>	
</body>
</html>