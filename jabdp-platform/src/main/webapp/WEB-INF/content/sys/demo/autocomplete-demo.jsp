<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Autocomplete Demo</title>
	<%@ include file="/common/meta.jsp" %>
	<link type="text/css" href="${ctx}/js/jquery-ui/blue/jquery-ui-1.8.23.custom.css" rel="stylesheet"/>
	<script type="text/javascript" src="${ctx}/js/jquery-ui/scripts/jquery-1.8.0.min.js"></script>
	<script type="text/javascript" src="${ctx}/js/jquery-ui/scripts/jquery-ui-1.8.23.custom.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			var _userList=findAllUser();
			var _userSelList = [];
			$.each(_userList, function(k, v) {
				var obj = {};
				obj["label"]= v.realName + "(" + v.loginName + ")[" + v.orgName + "]";
				obj["value"]= v.loginName;
				_userSelList.push(obj);
			});
			
			$("#userSel").autocomplete({
				source: _userSelList,
				minLength: 1,
				select: function( event, ui ) {
					$("#userSel").val( ui.item.label);
					$("#userId").val( ui.item.value );
					return false;
				}
			});
		});
	</script>
</head>
<body class="easyui-layout">
		<div region="center" title="Autocomplete Demo" style="overflow:hidden;">
			<input id="userSel"/><input type="hidden" name="userId" id="userId"/> <button type="button" onclick="alert($('#userId').val());"><s:text name="system.search.button.title"/></button>
		</div>
</body>
</html>

