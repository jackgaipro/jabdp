<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<script type="text/javascript">
	//附件关联标识
	var attachId = "${param.attachId}";
	
	//加载附件列表
	function attach_getOptionAttach() {
		$.ajax({
			url:'${ctx}/sys/attach/attach!queryList.action',
			data:{"attachId":attachId},
			success:function(data) {
				var attachs = data.rows;
				if(attachs && attachs.length) {
					$("#_attachmentCount").text(attachs.length);
					for(var i in attachs) {
						var attach = attachs[i];
						var attachStr = '<a class="param-selected icon-tag J_Ajax" name="filter_INS_status" href="#" title="'
							+ attach.fileName
							+ '" filePath="'
							+ attach.filePath
							+ '">'
							+ attach.fileName
							+ '</a>';
						$(".attach-cont").append(attachStr);
					}
				}
				//添加单击选择事件
				attach_selectAttach();
			}
		});
	}
	
	//添加附件鼠标控制事件
	function attach_selectAttach() {
		var attach_cont = $(".attach-cont a");
		attach_cont.each(function(){
			var _this = $(this);
			_this.dblclick(function(){
				window.open("${ctx}/upload/gs-attach/" + $(this).attr("filePath"));
			});
			_this.click(function(){
				var __this = $(this);
				if(__this.attr("isselect")!=null){
					__this.removeAttr("isselect");
					__this.css({border:"1px solid #e8e8e8"});
				} else {
					__this.attr("isselect",true);
					__this.css({border:"1px solid #000000"});
				}
			});
		});
	}
	
	//下载
	function attach_downloadFile(url) {   
		try{ 
			var elemIF = document.createElement("iframe");   
			elemIF.src = url;   
			elemIF.style.display = "none";   
			document.body.appendChild(elemIF);   
		} catch(e){ 
	
		} 
	}  
	
	//组装文件路径
	function attach_getDownloadFileUrl(fileName, filePath) {
		var downUrl = ["${ctx}/sys/attach/upload-file!downloadFile.action?filedataFileName="];
		downUrl.push(encodeURIComponent(fileName));
		downUrl.push("&filePath=");
		downUrl.push(encodeURIComponent(filePath));
		return downUrl.join("");
	}
	
	//批量下载文件
	function attach_doDownloadAttach() {
		var attach_cont = $(".attach-cont a[isselect=true]");
		if (attach_cont && attach_cont.length) {
			attach_cont.each(function(){
				var url = attach_getDownloadFileUrl($(this).attr("title"), $(this).attr("filePath"));
				attach_downloadFile(url);
			});
		} else {
			$.messager.alert(
				'<s:text name="system.javascript.alertinfo.title"/>',
				'<s:text name="system.javascript.alertinfo.operRecord"/>',
				'info');
		}
	}
	
	//初始化
	$(function(){
		//加载附件列表
		attach_getOptionAttach();
	});
</script>

<style type="text/css">
.attach-cont {
	padding-left: 5px;
	line-height: 20px;
}
.attach-cont .param-selected {
	display: inline-block;
	border: 1px solid #e8e8e8;
	color: #666;
	margin: 5px 5px 5px 0;
	padding: 10px 19px 0 4px;
	height: 30px;
	line-height: 18px;
	max-width: 144px;
	vertical-align: top;
	position: relative;
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
	text-decoration: none;
	background-color: #f8f8f8;
}
.attach-cont .close-icon {
	position: absolute;
	right: 6px;
	top: 6px;
}
</style>

<div style="margin-top: 10px;padding:0 8px 6px 12px;background:#fff;_height:60px;line-height:140%;">
 <div class="graytext clear" style="padding-top:12px; padding-bottom:5px">
  <b style="font-size:14px;"><img src="" align="absmiddle" class="ico_att showattch" border="0" style="margin:-3px 2px 0 0;" />附件</b>(
  <span id="_attachmentCount">0</span> 个)&nbsp;&nbsp;&nbsp;&nbsp;
  <a href="javascript:;" onclick="attach_doDownloadAttach();" style="+zoom:1;font-size:12px;font-weight:normal;" title="单击选中附件后，进行批量下载" unset="true">
  <img src="${ctx}/js/easyui/icons/images/Png16/Download.png" style="position:relative;top:-2px;"/>批量下载</a> &nbsp;&nbsp;&nbsp;&nbsp;
  <span style="color:red">*双击可预览附件</span>
 </div>
</div>
<div class="attach-cont"></div>