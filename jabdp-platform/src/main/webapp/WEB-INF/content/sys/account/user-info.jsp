<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title><s:text name="system.sysmng.user.manager.title" /></title>
<%@ include file="/common/meta-gs.jsp"%>
<script type="text/javascript">
	var _isSupportBigData_ = false;//支持大数据
	var setting = {
		data : {
			simpleData : {
				enable : true
			}
		},
		view : {
			dblClickExpand : false
		},
		callback : {
			onClick : viewUsers
		}
	};
	function viewUsers(event, treeId, treeNode) {
		var orgId = treeNode.idNum;
		$("#queryList").datagrid(getOption(orgId));
	}
	function initMenu() {
		var options = {
			url : '${ctx}/sys/account/role!getOrgList.action',
			success : function(data) {
				if (data.msg) {
					zTree = $.fn.zTree.init($("#orgList"), setting, data.msg);
					zTree.expandAll(false);
					var rootNodes = zTree.getNodes();
					if (rootNodes && rootNodes.length) {
						zTree.selectNode(rootNodes[0]);
						viewUsers(null, null, rootNodes[0]);
						zTree.expandNode(rootNodes[0], true, false, true);
					}
				}
			}
		};
		fnFormAjaxWithJson(options, true);
	}
	
	function initOrgTree() {
		$("#organizationIds").selectTree({
			url:"${ctx}/sys/account/organization!getOrgList.action",<%--"${ctx}/sys/account/organization!ztreeList.action",--%>
			queryParams:{},
			filterUrl:"${ctx}/sys/account/organization!findOrgList.action",
			filterParam:"filter_INL_id",
			pIdKey:"pId"
		});
	}
	
	function getOption(orgId) {
		return {
			title : '<s:text name="system.sysmng.user.list.title"/>',
			iconCls : "icon-search",
			width : 700,
			height : 450,
			nowrap : false,
			striped : true,
			fit : true,
			url : '${ctx}/sys/account/user!queryList.action?orgId=' + orgId,
			sortName : 'id',
			sortOrder : 'desc',
			idField : 'id',
			queryParams:{},
			frozenColumns : [ [ {
				field : 'ck',
				checkbox : true
			}, {
				title : '<s:text name="system.search.id.title"/>',
				field : 'id',
				width : 50,
				sortable : true,
				align : 'right'
			} ] ],
			columns : [ [
					{
						field : 'loginName',
						title : '<s:text name="system.sysmng.user.loginName.title"/>',
						width : 80,
						sortable : true
					},
					{
						field : 'realName',
						title : '<s:text name="system.sysmng.user.name.title"/>',
						width : 80,
						sortable : true
					},
					{
						field : 'nickname',
						title : '<s:text name="system.sysmng.user.nickname.title"/>',
						width : 80,
						sortable : true
					},
					/*{
						field : 'managerName',
						title : '<s:text name="system.sysmng.user.upmanager.title"/>',
						width : 100,
						sortable : true
					},*/
					{
						field : 'organizationName',
						title : '<s:text name="system.sysmng.organization.name.title"/>',
						width : 100,
						sortable : false
					},
					{
						field : 'employeeId',
						title : '<s:text name="system.sysmng.user.employee.title"/>',
						width : 80,
						sortable : true,
						align : 'center'
					},
					{
						field : 'status',
						title : '<s:text name="system.sysmng.user.status.title"/>',
						width : 80,
						align : 'center',
						sortable : true,
						formatter : function(value, rowData, rowIndex) {
							if (value == 1) {
								return "<s:text name="system.button.enabled.title"/>";
							} else if (value == 0) {
								return "<s:text name="system.button.disable.title"/>";
							} else {
								return "";
							}
						}
					},
					{
						field : 'roleNames',
						title : '<s:text name="拥有角色"/>',
						width : 120,
						sortable : true
					},
					{
						field : 'isSuperAdmin',
						title : '<s:text name="system.sysmng.user.superuser.title"/>',
						width : 120,
						align : 'center',
						sortable : true,
						formatter : function(value, rowData, rowIndex) {
							if (value == 1) {
								return "是";
							} else {
								return "否";
							}
						}
					}, {
						field : 'oper',
						title : '<s:text name="system.button.oper.title"/>',
						width : 200,
						align : 'center',
						formatter : operFormatter
					}

			] ],
			pagination : true,
			rownumbers : true,
			toolbar : [ {
				id : 'bt_add',
				text : '<s:text name="system.button.add.title"/>',
				iconCls : 'icon-add',
				handler : function() {
					doAdd();
				}
			}, '-', {
				id : 'bt_del',
				text : '<s:text name="system.button.delete.title"/>',
				iconCls : 'icon-remove',
				handler : function() {
					doDeleteIds();
				}
			}, '-' ],
			onDblClickRow : function(rowIndex, rowData) {
				doView(rowData.id);
			}
		};
	}
	
	function doQuery() {
		var param = $("#queryForm").serializeArrayToParam();
		$("#queryList").datagrid("load", param);
	} 

	//焦点聚在第一行-
	function focusEditor(id) {
		setTimeout(function() {
			$("#" + id).focus();
		}, 0);
	}
	function doDisabled() {
		//$("#organizationIds").combotree("disable");
		$("input,select").attr("disabled", "disabled");
		$("#_bt_save_").hide();
	}

	function doEnabled() {
		//$("#organizationIds").combotree("enable");
		$("input,select").removeAttr("disabled");
		$("#_bt_save_").show();
	}

	//刷新列表
	function doRefreshDataGrid() {
		$("#queryList").datagrid("load");
	}
	function cancelAdd() {
		$('#win_add_message').window('close');
		doEnabled();
	}
	
	function isEnable(status) {
		if (status == "1") {
			$("#buttons").linkbutton({text : '<s:text name="system.button.disable.title"/>'});
			$("#buttons").linkbutton({iconCls : 'icon-no'});
			$('#userstatus').text('启用状态');
			$('#buttons').linkbutton('enable');
		} else {
			$("#buttons").linkbutton({text : '<s:text name="system.button.enabled.title"/>'});
			$("#buttons").linkbutton({iconCls : 'icon-ok'});
			$('#userstatus').text('禁用状态');
			$('#buttons').linkbutton('enable');
		}	
	}

	function doAdd() {
		var rootNodes = zTree.getNodes();
		var orgId = 0;
		if (rootNodes && rootNodes.length) {
			orgId = rootNodes[0].idNum;
			;
		}
		var options = {
			url : '${ctx}/sys/account/user!addInfo.action',
			data : {
				"orgId" : orgId
			},
			success : function(data) {
				if (data.msg) {
					doEnabled();
					/*$("#organizationIds").combotree('setValue',
							data.msg.parentId);*/
					$("#organizationIds").selectTree('setValue',
									data.msg.parentId);		
					$("#buttons").linkbutton({
						text : '<s:text name="system.button.disable.title"/>'
					});
					$("#buttons").linkbutton({
						iconCls : 'icon-no'
					});
					$('#userstatus').text('启用状态');
					$('#buttons').linkbutton('enable');
					$('#status').val("1");
					$('#sex').combobox('setValue', '1');
					$('#remoteLogin').combobox('setValue', '0');

					$("#win_add_message").window("open");
					focusEditor("loginName");
				}
			},
			traditional : true
		};
		fnFormAjaxWithJson(options);
	}
	function doView(userId) {
		var options = {
			url : '${ctx}/sys/account/user!view.action',
			data : {
				"id" : userId
			},
			success : function(data) {
				if (data.msg) {
					var json2fromdata = data.msg;
					$('#saveForm').form('load', json2fromdata);
					isEnable(json2fromdata.status);
					$("#organizationIds").selectTree('setValue',
								json2fromdata.organizationId);
					$("#win_add_message").window("setTitle",
							"<s:text name='system.sysmng.user.view.title'/>");
					$("#win_add_message").window("open");
					doDisabled();
				}
			}
		};
		fnFormAjaxWithJson(options);
	}

	function doModify(userId) {
		var options = {
			url : '${ctx}/sys/account/user!modifyInfo.action',
			data : {
				"id" : userId
			},
			success : function(data) {
				if (data.msg) {				
					var json2fromdata = data.msg;
					$('#saveForm').form('load', json2fromdata);
					isEnable(json2fromdata.status);	
					/*$("#organizationIds").combotree('setValue',
							json2fromdata.organizationId);*/
					$("#organizationIds").selectTree('setValue',
										json2fromdata.organizationId);		
					$("#win_add_message").window("setTitle",
							"<s:text name='system.sysmng.user.modify.title'/>");
					$("#mm").show();
					$("#win_add_message").window("open");
					doEnabled();
					//不禁用登录名修改
					/* $("#saveForm input[name='loginName']").attr("disabled",
							"disabled"); */
				}
			}
		};
		fnFormAjaxWithJson(options);
	}
	function doDelete(id) {
		$.messager.confirm(
				'<s:text name="system.javascript.alertinfo.title"/>',
				'<s:text name="system.javascript.alertinfo.info"/>',
				function(r) {
					if (r) {
						var options = {
							url : '${ctx}/sys/account/user!delete.action',
							data : {
								"id" : id
							},
							success : function(data) {
								if (data.msg) {
									var nodes = zTree.getSelectedNodes();
									var orgId = nodes[0].idNum;
									$("#queryList").datagrid(getOption(orgId));
									
								}
							}
						};
						fnFormAjaxWithJson(options);
					}
				});
	}
	function doDeleteIds() {
		var ids = [];
		var rows = $('#queryList').datagrid('getSelections');
		for ( var i = 0; i < rows.length; i++) {
			if (rows[i].isSuperAdmin == "1") {
				return $.messager
						.alert(
								'<s:text name="system.javascript.alertinfo.errorInfo"/>',
								'不能选择超级管理者进行删除', 'info');
			} else {
				ids.push(rows[i].id);
			}
		}
		if (ids != null && ids.length > 0) {
			$.messager
					.confirm(
							'<s:text name="system.javascript.alertinfo.title"/>',
							'<s:text name="system.javascript.alertinfo.info"/>',
							function(r) {
								if (r) {
									var options = {
										url : '${ctx}/sys/account/user!deleteUsers.action',
										data : {
											"ids" : ids

										},
										success : function(data) {
											if (data.msg) {
												$('#queryList').datagrid('clearSelections');
												/* var nodes = zTree.getSelectedNodes();
												var orgId = nodes[0].idNum;
												$("#queryList").datagrid(getOption(orgId)); */
												doQuery();
											}
										},
										traditional : true
									};
									fnFormAjaxWithJson(options, true);
								}
							})

		} else {
			$.messager.alert(
					'<s:text name="system.javascript.alertinfo.title"/>',
					'<s:text name="system.javascript.alertinfo.question"/>',
					'info');
		}
	}

	
	function saveUser(method) {
		var options = {
			url : '${ctx}/sys/account/user!saveUserInfo.action',
			success : function(data) {
				if (data.msg) {
					var nodes = zTree.getSelectedNodes();
					var orgId = nodes[0].idNum;
					$("#queryList").datagrid(getOption(orgId));
					$('#win_add_message').window('close');
				}
			}
		};
		fnAjaxSubmitWithJson('saveForm', options);
	}

	$(document).ready(function() {
		$("#buttons").toggle(function() {
			if (!$('#buttons').linkbutton('options').disabled) {
				$("#buttons").linkbutton({
					text : '<s:text name="system.button.enabled.title"/>'
				});
				$("#buttons").linkbutton({
					iconCls : 'icon-ok'
				});
				$('#userstatus').text('禁用状态');
				$('#status').val("0");
			}
		}, function() {
			if (!$('#buttons').linkbutton('options').disabled) {
				$("#buttons").linkbutton({
					text : '<s:text name="system.button.disable.title"/>'
				});
				$("#buttons").linkbutton({
					iconCls : 'icon-no'
				});
				$('#userstatus').text('启用状态');
				$('#status').val("1");
			}
		});

		 $('#queryForm input').bind('keydown', 'return', function(evt) {
			doQuery();
			return false;
		});

		$("#bt_query").click(doQuery);
		$("#bt_reset").click(function() {
			$("#queryForm")[0].reset();
			doQuery();
		}); 

		$("#win_add_message").window({
			width : 630,
			height : 320,
			onOpen : function() {
				$(this).window("move", {
					top : ($(window).height() - 470) * 0.5,
					left : ($(window).width() - 700) * 0.5
				});
			},
			onClose : function() {
				$("#saveForm")[0].reset();
				$("#id").val("");
			}
		});
		initMenu();
		initOrgTree();
	});

	function operFormatter(value, rowData, rowIndex) {
		var strArr = [];
		strArr.push('<a href="javascript:void(0);" onclick="doView(');
		strArr.push(rowData.id);
		strArr
				.push(')"><s:text name="system.button.view.title"/></a>&nbsp;<a href="javascript:void(0);" onclick="doModify(');
		strArr.push(rowData.id);
		strArr
				.push(')"><s:text name="system.button.modify.title"/></a>&nbsp;<a href="javascript:void(0);" onclick="doPassWord(');
		strArr.push(rowData.id);
		strArr.push(')"><s:text name="system.button.retPassword.title"/></a>');
		if (rowData.isSuperAdmin != "1") {
			strArr
					.push('&nbsp;<a href="javascript:void(0);" onclick="doDelete(');
			strArr.push(rowData.id);
			strArr.push(')"><s:text name="system.button.delete.title"/></a>');
		}
		return strArr.join("");
	}

	function doPassWord(id) {
		$.messager
				.confirm(
						'<s:text name="system.button.retPassword.title"/>',
						'<s:text name="system.javascript.alertinfo.passwordInfo"/>',
						function(r) {
							if (r) {
								var options = {
									url : '${ctx}/sys/account/user!resetPassword.action',
									data : {
										"id" : id
									},
									success : function(data) {
										if (data.msg) {
											$.messager.alert('<s:text name="system.javascript.alertinfo.succees"/>', data.msg,
											'info');
										}
									}
								};
								fnFormAjaxWithJson(options, true);
							}
						});

	}
</script>
</head>
<body class="easyui-layout">
	
	<div region="west" title="<s:text name="system.sysmng.user.organization.title"/>" split="true" border="false" style="width: 230px; padding1: 1px; overflow: hidden;">
		<ul id="orgList" class="ztree"></ul>
	</div>
	
	<div region="center" title="" border="false" style="overflow: text; width: 580px; padding: 1px;">
	<div class="easyui-layout" fit="true">
		<div region="north" title="<s:text name="system.search.title"/>" border="false"
		split="true" style="height: 100px; padding: 0px; overflow: hidden;"
		iconCls="icon-search">
		<form action="" name="queryForm" id="queryForm">
			<table border="0" cellpadding="0" cellspacing="1" class="table_form">
				<tbody>
					<tr>
						<th><label for="loginName1"><s:text
									name="system.sysmng.user.loginName.title" />:</label></th>
						<td><input type="text" name="filter_LIKES_loginName"
							id="loginName1" class="Itext"></input></td>
						<th><label for="realName1"><s:text
									name="system.sysmng.user.name.title" />:</label></th>
						<td><input type="text" name="filter_LIKES_realName" class="Itext"
							id="realName1"></input></td>
						<td></td>
					</tr>
					<tr>
						<th><label for="employeeId1"><s:text
									name="system.sysmng.user.employee.title" />:</label></th>
						<td><input type="text" name="filter_LIKES_employeeId" class="Itext"
							id="employeeId1"></input></td>
						<th><label for="nickname1"><s:text
									name="system.sysmng.user.nickname.title" />:</label></th>
						<td><input type="text" name="filter_LIKES_nickname" class="Itext"
							id="nickname1"></input></td> 
						<td><button type="button" id="bt_query">
								<s:text name="system.search.button.title" />
							</button>&nbsp;&nbsp;
							<button type="button" id="bt_reset">
								<s:text name="system.search.reset.title" />
							</button></td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>
	<div region="center" title="" border="false" style="padding: 0px; overflow: hidden;">
		<table id="queryList"></table>
	</div>
	</div>
	</div>

	<!--win弹出框  -->
	<div id="win_add_message" class="easyui-window" closed="true"
		modal="true" title="<s:text name="system.sysmng.user.add.title"/>" 
		iconCls="icon-save"
		style=" padding: 0px; background: #fafafa;">
		<div class="easyui-layout" fit="true">
			<div region="center" border="false"
				style="padding: 2px; background: #fff; overflow: hidden; border: 1px solid #ccc;">
				<form action="${ctx}/sys/account/organization!save.action"
					name="saveForm" id="saveForm" method="post">
					<input type="hidden" name="id" id="id" />
					<input type="hidden" name="isSuperAdmin" id="isSuperAdmin" value="0" />
					<table align="center" border="0" cellpadding="0" cellspacing="1"
						class="table_form">
						<tbody>
							<tr>
								<th><label for="loginName"><s:text
											name="system.sysmng.user.loginName.title" />:</label>
								</th>
								<td> <input
									type="text" name="loginName" id="loginName" class="Itext"></input>
								</td>
								<th><label for="realName"><s:text
											name="system.sysmng.user.name.title" />:</label>
								</th>
								<td><input class="easyui-validatebox Itext"  required="true" type="text" name="realName" id="realName"></input>
								</td>
							</tr>
							<tr>
								<th><label for="employeeId"><s:text
											name="system.sysmng.user.employee.title" />:</label>
								</th>
								<td><input class="easyui-validatebox Itext"  type="text" name="employeeId" id="employeeId" placeholder="手动填写或自动生成"></input>
								</td>
								<th><label for="nickname"><s:text
											name="system.sysmng.user.nickname.title" />:</label>
								</th>
								<td><input  type="text" name="nickname" id="nickname" class="Itext"></input>
								</td>
							</tr>
							<tr>
								<th><label for="organizationName"><s:text
											name="system.sysmng.role.organization.title" />:</label>
								</th>
								<td><input id="organizationIds" 
									<%-- url="${ctx}/sys/account/organization!userOrgList.action" class="easyui-combotree" --%>
									name="organizationId" style="width: 180px"></input></td>
								<th><label for="managerName"><s:text
											name="system.sysmng.user.upmanager.title" />:</label>
								</th>
								<td><input type="text" name="managerName" id="managerName" class="Itext"
									readonly="readonly"></input> <input type="hidden"
									name="managerId" id="managerId"></input></td>
							</tr>
							<tr>
								<th><label for="sex"><s:text
											name="system.sysmng.user.sex.title" />:</label>
								</th>
								<td><select  class="easyui-combobox" name="sex" id="sex" style="width: 152px;">
										<!-- <option value="">--请选择--</option> -->
										<option value="1">男</option>
										<option value="0">女</option>
								</select></td>
								<th><label for="mobilePhone"><s:text
											name="system.sysmng.user.mobilephone.title" />:</label>
								</th>
								<td><input class="easyui-validatebox Itext" type="text" name="mobilePhone" id="mobilePhone" validType="mobile" invalidMessage="请输入正确的11位手机号码.格式:13120002221"></input>
								</td>
							</tr>
							<tr>
								<th><label for="fax"><s:text
											name="system.sysmng.user.fax.title" />:</label>
								</th>
								<td><input class="easyui-validatebox Itext" type="text" name="fax" id="fax"  validType="faxno"></input>
								</td>
								<th><label for="email"><s:text
											name="system.sysmng.user.email.title" />:</label>
								</th>
								<td><input class="easyui-validatebox Itext" type="text" name="email" id="email"  validType="email"></input>
								</td>
							</tr>
							<tr>
								<th><label for="status"><s:text
											name="system.sysmng.user.status.title" />:</label>
								</th>
								<td><input type="hidden" name="status" id="status" /> <span
											id="userstatus" style="color: red"></span> <a href="#"
											class="easyui-linkbutton" id="buttons" ></a></td>
								<th><label for="remoteLogin"><s:text
											name="system.sysmng.user.remotelogin.title" />:</label>
								</th>
								<td><select class="easyui-combobox" name="remoteLogin" id="remoteLogin" style="width: 152px;">
										<!-- <option value="">--请选择--</option> -->
										<option value="0">不允许</option>
										<option value="1">允许</option>
								</select></td>
							</tr>
							<tr>

								<th><label for="address"><s:text
											name="system.sysmng.user.address.title" />:</label>
								</th>
								<td colspan="3"><input type="text" name="address" class="Itext"
									id="address" style="width: 400px;"></input></td>
							</tr>

						</tbody>
					</table>
				</form>
			</div>
			<div region="south" border="false"
				style="text-align: right; height: 30px; line-height: 30px;">
				 <a id="_bt_save_" class="easyui-linkbutton" iconCls="icon-ok"
					href="javascript:void(0)" onclick="saveUser('add')"><s:text
						name="system.button.save.title" /> </a> <a class="easyui-linkbutton"
					iconCls="icon-cancel" href="javascript:void(0)"
					onclick="cancelAdd();"><s:text
						name="system.button.cancel.title" /> </a>
              
			</div>
		</div>
	</div>

</body>
</html>

