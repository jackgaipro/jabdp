<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<%@ page import="org.springframework.security.web.WebAttributes" %>
<%@ page import="org.springframework.security.web.authentication.session.SessionAuthenticationException" %>
<%@ page import="org.springframework.security.core.AuthenticationException" %>
<%@ page import="com.qyxx.platform.sysmng.utils.Constants" %>
<%@ page import="com.qyxx.platform.common.utils.spring.SpringContextHolder" %>
<%@ page import="com.qyxx.platform.gsc.utils.SystemParam" %>
<%
	SystemParam systemParam = SpringContextHolder.getBean("systemParam");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><%=systemParam.getSystemTitle()%></title>
	<%@ include file="/common/meta-min.jsp" %>
	<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/blue/easyui.css"/>
	<script type="text/javascript" src="${ctx}/js/easyui/scripts/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="${ctx}/js/easyui/scripts/locale/easyui-lang-zh_CN.js"></script>
	<link href="${ctx}/js/loadmask/jquery.loadmask.css" rel="stylesheet" type="text/css"/>
	<script type="text/javascript" src="${ctx}/js/loadmask/jquery.loadmask.min.js"></script>
	<script type="text/javascript" src="${ctx}/js/common/scripts/common.js"></script>
	<script type="text/javascript" src="${ctx}/js/common/scripts/focus_tips.js"></script>
	<link href="${ctx}/${themeStyle}/blue2/css/login.css" rel="stylesheet" type="text/css"/>
	<script type="text/javascript">
		<s:if test="#parameters.timeout!=null">
			if(window.top!=window) {
				window.top.document.location.replace("${ctx}/login1.action?timeout=true");
			}
		</s:if>
		function doCheckUserIsLogin(userName, userPass) {
			var flag = false;
			var options = {
					async:false,
					url : '${ctx}/sys/account/online-user!checkUserIsLogin.action',
					data : {
						"userName" : userName,
						"userPass" : userPass
					},
					success : function(data) {
						var fm = data.msg;
						if(fm) {
							switch(fm.flag) {
								case "0":
									var cMsg = "<div>" + fm.msg + "。是否确认将其强制下线？</div>";
									$.messager.confirm('该用户已经登录', cMsg, function(r){
										if (r){
											doRemoveOnlineUser(userName);
										} else {
											$("#j_username").val("");
											$("#j_password").val("");
											$("#j_username").focus();
										}
									});
									break;
								case "2":
									$(".user_login_msg").text(fm.msg);
									break;
								default:
									flag = true;
							}
						}
					}
			};
			fnFormAjaxWithJson(options);
			return flag;
		}

		function doRemoveOnlineUser(userName) {
			var options = {
					async:false,
					url : '${ctx}/sys/account/online-user!removeLoginUser.action',
					data : {
						"userName" : userName
					},
					success : function(data) {
						$("#loginForm").unbind("submit");
						$("#loginForm").submit();
					}
			};
			fnFormAjaxWithJson(options);
		}
		
		$(document).ready(function() {
			<%
				AuthenticationException authException = (AuthenticationException)session.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
				if(authException!=null && authException instanceof SessionAuthenticationException) {
					String errorMsg = authException.getMessage();
			%>
					$(".user_login_msg").text("<%=errorMsg%>");
			<%
				}
			%>
			
			$("#loginForm").submit(function() {
				var userName = $("#j_username").val();
				if(!userName) {
					$(".user_login_msg").text("请输入用户名");
					$("#j_username").focus();
					return false;
				}
				var pass = $("#j_password").val();
				if(!pass) {
					$(".user_login_msg").text("请输入密码");
					$("#j_password").focus();
					return false;
				}
				return doCheckUserIsLogin(userName, pass);
			});
			
		});
	</script>
</head>
<%
	String username = "";
	Cookie[] cs = request.getCookies();
	if(null!=cs) {
		for(Cookie ck : cs) {
			if(Constants.USER_NAME_COOKIE.equals(ck.getName())) {
				username = java.net.URLDecoder.decode(ck.getValue(), Constants.DEFAULT_ENCODE);
				break;
			}
		}
	}
%>
<body class="login_page_one">
<form id="loginForm" action="${ctx}/j_spring_security_check" method="post">
	<div class="login_box">
    	<div class="b_l">
        <div class="logo">	
        </div>
        <div class="tools_box" ></div>
        </div>
        <div class="login_content">
        	<div class="login_title">
        		<%-- <span class="login_title_inner_logo"></span>
        		<span class="login_title_inner_logo_title"></span>--%>
        		<img src="${imgPath}/<%=systemParam.getSystemLogo()%>" style="height:0px;width:0px;vertical-align:bottom;"/>
				<span class="user_top_title" style="height:50px;position:relative;left:10px;top:-10px;color:#00106d"></span>
        	</div>
            <div class="login_form">
            	<ul>
                	<li class="username"><label for="j_username">用户名:</label><input type="text" name="j_username" class="input_style" id="j_username"
                			value="" /></li>
                    <li class="pwd"><label for="j_password">密<i class="w10"></i>码:</label><input type="password" name="j_password" value="" class="input_style"  id="j_password"/></li>
                    <li class="code"><label for="request_locale"><s:text name="system.login.language.title"/></label><input type="hidden" name="request_locale" value="zh_CN" id="request_locale" /></li>
                </ul>
            </div>
            <div class="login_submit">
            	<input type="submit" name="bt_login" value="登录" class="submit_btn" />
                <input type="reset" name="bt_reset" value="取消" class="submit_btn" />
            </div>
            <div class="user_login_msg">
					<s:if test="#parameters.error!=null">
						<span><s:text name="system.login.error.userpass.title"/></span>&nbsp;&nbsp;
					</s:if>
					<s:if test="#parameters.timeout!=null">
						<span><s:text name="system.login.error.timeout.title"/></span>&nbsp;&nbsp;
					</s:if>
			</div>
        </div>
    </div>
    <div class="copy">
    	<%=systemParam.getSystemPowerBy()%>
    </div>
</form>
</body>
<script type="text/javascript">
	$(function(){
		$("#j_username").span_tip({text:'请输入用户名'});
		$("#j_password").span_tip({text:'请输入密码'});
		<s:if test="#parameters.error!=null">
		$("#j_username").val('<%=session.getAttribute(WebAttributes.LAST_USERNAME)%>');
		</s:if>
		<s:else>
		$("#j_username").val("${param.username}");
		$("#j_password").val("${param.password}");
		</s:else>
		$("#j_username").focus();
		$("#loginForm").submit();
	})
</script>
</html>

