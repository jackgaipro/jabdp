<%@ page import="com.qyxx.platform.common.utils.spring.SpringContextHolder" %>
<%@ page import="com.qyxx.platform.gsc.utils.SystemParam" %>
<%@ page import="com.qyxx.platform.sysmng.accountmng.entity.User" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%
	SystemParam systemParam = SpringContextHolder.getBean("systemParam");
	String ctxPath = request.getContextPath();
	String indexUrl = "index.action";
	String systemIndexUrl = systemParam.getSystemIndexUrl();
	if(StringUtils.isNotBlank(systemIndexUrl)) {
		indexUrl = systemIndexUrl; 
	}
	User user = (User)session.getAttribute("USER");
	if(null!=user) {
		String roleIndexUrl = user.getIndexUrl();
		if(StringUtils.isNotBlank(roleIndexUrl)) {
			indexUrl = roleIndexUrl;
		}
	}
	response.sendRedirect(ctxPath + "/" + indexUrl);
%>