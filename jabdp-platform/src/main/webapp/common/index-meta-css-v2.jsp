<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta name="renderer" content="webkit"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="stylesheet" href="${ctx}/js/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="${ctx}/js/toastr/toastr.min.css">
<link rel="stylesheet" href="${ctx}/js/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="${ctx}/js/index/animate.min.css">
<link rel="stylesheet" href="${ctx}/js/index/pace-theme-minimal.css">
<link rel="stylesheet" href="${ctx}/js/metismenu/metisMenu.min.css">
<link rel="stylesheet" href="${ctx}/js/index/index.min.css">
<link rel="stylesheet" href="${ctx}/js/dist/easyui-bootstrap.min.css" colorTitle="bootstrap"/>
<link rel="stylesheet" href="${ctx}/js/easyui-1.4/themes/icon.css"/>
<link rel="stylesheet" href="${ctx}/js/easyui/icons/icon-add.css"/>
<style>
.Itext,.Idate, textarea, .datagrid-view .Itext{border: #95B8E7 1px solid;margin: 0;padding: 1px;
	  vertical-align: top;outline-style: none;resize: both;-moz-border-radius: 5px 5px 5px 5px;
	  -webkit-border-radius: 5px 5px 5px 5px;border-radius: 5px 5px 5px 5px;font-size:12px !important;}
.datagrid-row-selected {background: #ffe48d;color:#000;}	
.simple-query-form .form-group {margin-bottom:2px;}
	/*::-webkit-input-placeholder{ color:#F8F8FF;font-size:12px;}
	:-moz-placeholder{color:#F8F8FF;font-size:12px;}
	::moz-placeholder{color:#F8F8FF;font-size:12px;}
	:-ms-input-placeholder{color:#F8F8FF;font-size:12px;}*/
	.form-control{color:#000;}
	.select2-container--bootstrap .select2-selection--single .select2-selection__rendered{color:#000;}
	.select2-container--bootstrap .select2-selection--multiple .select2-selection__choice{color:#000;}
</style>

