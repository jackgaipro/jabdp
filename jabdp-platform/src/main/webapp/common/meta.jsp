<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<meta http-equiv="Cache-Control" content="no-store"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<!-- <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" /> -->
<link rel="icon" type="image/x-icon" href="${ctx}/favicon.ico"></link> 
<link rel="shortcut icon" type="image/x-icon" href="${ctx}/favicon.ico"></link>
<!-- YUI3 common css 
<link rel="stylesheet" type="text/css" href="${ctx}/css/yui/reset-min.css"/>
<link rel="stylesheet" type="text/css" href="${ctx}/css/yui/base-min.css"/>
<link rel="stylesheet" type="text/css" href="${ctx}/css/yui/fonts-min.css"/>
<link rel="stylesheet" type="text/css" href="${ctx}/css/yui/grids-min.css"/>
-->
<%@ include file="/common/meta-html5-v2.jsp"%>
<!-- jquery js -->
<script src="${ctx}/js/jquery/jquery-1.7.2.min.js" type="text/javascript"></script>

<!-- easyui -->
<c:choose>
   <c:when test="${sessionScope.THEME_VERSION == 'bootstrap'}">  
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/gray/easyui.css" colorTitle="gray"/>
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/gray/easyui-add.css" colorTitle="gray"/>
   </c:when>
   <c:otherwise> 
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/gray/easyui.css" colorTitle="gray"/>
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/blue/easyui.css" colorTitle="blue"/>
<%-- <link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/green/easyui.css" colorTitle="green">
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/orange/easyui.css" colorTitle="orange">
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/pink/easyui.css" colorTitle="pink"> --%>
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/gray/easyui-add.css" colorTitle="gray"/>
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/blue/easyui-add.css" colorTitle="blue"/>
<%-- <link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/green/easyui-add.css" colorTitle="green">
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/orange/easyui-add.css" colorTitle="orange">
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/pink/easyui-add.css" colorTitle="pink"> --%>
   </c:otherwise>
</c:choose>
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/icons/icon.css"/>
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/icons/icon-add.css"/>

<%-- 
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/icons/icon-add.css"/>
<script type="text/javascript" src="${ctx}/js/easyui/scripts/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${ctx}/js/easyui/scripts/locale/easyui-lang-${locale}.js"></script>

<script type="text/javascript" src="${ctx}/js/easyui/scripts/easyui.extend.js"></script>
<script type="text/javascript" src="${ctx}/js/easyui/scripts/datagrid-detailview.js"></script>
<script type="text/javascript" src="${ctx}/js/easyui/scripts/jquery.edatagrid.js"></script>

<!--common i18n -->
<script type="text/javascript" src="${ctx}/js/common/scripts/locale/data-${locale}.js"></script>
<!-- jquery form -->
<script type="text/javascript" src="${ctx}/js/form/scripts/jquery.form.js"></script>
<script type="text/javascript" src="${ctx}/js/form/scripts/json2form.js"></script>
<!-- jquery loadmask -->
<link href="${ctx}/js/loadmask/jquery.loadmask.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="${ctx}/js/loadmask/jquery.loadmask.min.js"></script>
<!-- common.js -->
<script type="text/javascript" src="${ctx}/js/common/scripts/common.js?v20140808"></script>
<script type="text/javascript" src="${ctx}/js/common/scripts/json2.js?v20140808"></script>
<script type="text/javascript" src="${ctx}/js/common/scripts/jwpf.js?v20140720"></script>
<script type="text/javascript" src="${ctx}/js/common/scripts/focus_tips.js"></script>
<link href="${ctx}/js/common/scripts/focus_tips.css" rel="stylesheet" type="text/css"/>

<!-- ztree -->
<link href="${ctx}/js/ztree/blue/zTreeStyle.css" rel="stylesheet" type="text/css"/>
<link href="${ctx}/js/ztree/blue/icon.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/icons/icon-ztree.css"/>
<script src="${ctx}/js/ztree/scripts/jquery.ztree.all-3.2.min.js" type="text/javascript"></script>

<!-- datepicker -->
<script type="text/javascript" src="${ctx}/js/datepicker/${locale}_WdatePicker.js"></script>

<link href="${cssPath}/main.css" rel="stylesheet" type="text/css"/>


<!-- theme.js -->
<script type="text/javascript" src="${ctx}/js/common/scripts/theme.js"></script>
<!-- theme.js -->
<script type="text/javascript" src="${ctx}/js/common/scripts/gscommon.js"></script>
<script type="text/javascript" src="${ctx}/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript"
        src="${ctx}/js/ckeditor/adapters/jquery.js"></script>
        
<!-- hotkey -->
<script type="text/javascript" src="${ctx}/js/hotkey/jquery.hotkeys.js"></script>    
<!-- imgpreview -->
<link href="${ctx}/js/imgpreview/blue/jquery.gzoom.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="${ctx}/js/imgpreview/scripts/jquery.gzoom.js"></script>
<script type="text/javascript" src="${ctx}/js/imgpreview/scripts/ui.core.min.js"></script>
<script type="text/javascript" src="${ctx}/js/imgpreview/scripts/ui.slider.min.js"></script>
<!-- uploadify -->
<link href="${ctx}/js/uploadify/uploadify-3.1.css" rel="stylesheet"
	type="text/css" />
<script type="text/javascript"
	src="${ctx}/js/uploadify/jquery.uploadify-3.1.min.js"></script>
<!-- XDate -->
<script type="text/javascript" src="${ctx}/js/common/scripts/xdate.js"></script>
<script type="text/javascript" src="${ctx}/js/common/scripts/accounting.js"></script>
<!-- qtip2 -->
<link type="text/css" rel="stylesheet" href="${ctx}/js/qtip2/jquery.qtip.min.css" />
<script type="text/javascript" src="${ctx}/js/qtip2/jquery.qtip.min.js"></script>
<script type="text/javascript" src="${ctx}/js/qtip2/imagesloaded.pkgd.min.js"></script>
<!-- ZeroClipboard -->
<script src="${ctx}/js/zeroclipboard/ZeroClipboard.min.js"></script>
<style type="text/css">
    #global-zeroclipboard-html-bridge {
      z-index: 99999999 !important;
      /*background-color: red;*/
    }
</style>
<!--webuploader start-->
<link rel="stylesheet" type="text/css" href="${ctx}/js/webuploader/webuploader.css">
<script type="text/javascript" src="${ctx}/js/webuploader/webuploader.min.js"></script>
<script type="text/javascript" src="${ctx}/js/webuploader/es.filebox.js"></script>
<!--webuploader end-->
<!-- toastr -->
<link rel="stylesheet" href="${ctx}/js/toastr/toastr.min.css">
<script src="${ctx}/js/toastr/toastr.min.js"></script>--%>
<link rel="stylesheet" type="text/css" href="${ctx}/js/dist/${jsVersion}/app.min.css"/>
<style type="text/css">
    #global-zeroclipboard-html-bridge {
      z-index: 99999999 !important;
      /*background-color: red;*/
    }
</style>
<script type="text/javascript" src="${ctx}/js/datepicker/${locale}_WdatePicker.js"></script>
<script type="text/javascript" src="${ctx}/js/dist/${jsVersion}/app-v1.min.js"></script>
<script type="text/javascript" src="${ctx}/js/easyui/scripts/locale/easyui-lang-${locale}.js"></script>
<!--common i18n -->
<script type="text/javascript" src="${ctx}/js/common/scripts/locale/data-${locale}.js"></script>
<script type="text/javascript" src="${ctx}/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="${ctx}/js/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript" src="${ctx}/js/dist/${jsVersion}/app.min.js"></script>
<c:if test="${sessionScope.THEME_VERSION != 'bootstrap'}">  
<script>
$(document).ready(function() {
	var c = readCookie('style');
	if(c) switchStylestyle(c);
});
</script>
</c:if>

