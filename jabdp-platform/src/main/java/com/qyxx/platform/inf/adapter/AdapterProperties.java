package com.qyxx.platform.inf.adapter;

import java.util.HashMap;
import java.util.Map;

/**
 *  适配器属性
 *  
 *  @author gxj
 *  @version 1.0 2014-4-16 下午02:21:35
 *  @since jdk1.6
 */
public class AdapterProperties {
	
	/**
	 * 适配器类ID
	 */
	private Long adapterId;
	
	/**
	 * 适配器名称
	 */
	private String adapterName;
	
	/**
	 * 适配器类
	 */
	private String adapterClass;
	
	/**
	 * 适配器描述
	 */
	private String adapterRemark;

	/**
	 * 适配器编码
	 */
	private String adapterCode;

	/**
	 * 访问用户
	 */
	private String userName;

	/**
	 * 访问密码
	 */
	private String password;

	/**
	 * IP地址
	 */
	private String ipAddress;

	/**
	 * 访问端口
	 */
	private String accessPort;

	/**
	 * 网络类型
	 */
	private String networkType;

	/**
	 * 协议类型
	 */
	private String protocolType;

	/**
	 * 连接类型，短连接-SHORT，长连接-LONG
	 */
	private String connectType;
	
	/**
	 * 适配器工作状态，P-挂起状态，S-停止状态，R-运行中
	 */
	private String adapterStatus;

	/**
	 * 适配器最大并发数
	 */
	private Integer concurrentNum;
	
	/**
	 * 初始时的并发数，对应交互器池的核心池数量
	 */
	private Integer initConcurNum = new Integer(1);
	
	/**
	 * 交互超时时间（包括连接超时、登录超时、指令发送超时）
	 */
	private Long overTime;
	
	/**
	 * 连接超时等待重连时间
	 */
	private Long reConnTime;
	
	/**
	 * 登录、指令发送超时重发次数
	 */
	private Long reSendTimes;
	
	/**
	 * 连续失败指令次数
	 */
	private Long failCmdTimes;
	
	/**
	 * 空闲交互器最大等待时间，即空闲后多少时间需要回收，默认5小时
	 */
	private Long keepAliveTime = new Long(18000000);
	
	/**
	 * 参数Map,key为参数编码，value为参数值
	 */
	private Map<String, String> paramMap = new HashMap<String, String>();

	/**
	 * @return the adapterId
	 */
	public Long getAdapterId() {
		return adapterId;
	}

	/**
	 * @param adapterId the adapterId to set
	 */
	public void setAdapterId(Long adapterId) {
		this.adapterId = adapterId;
	}

	/**
	 * @return the adapterName
	 */
	public String getAdapterName() {
		return adapterName;
	}

	/**
	 * @param adapterName the adapterName to set
	 */
	public void setAdapterName(String adapterName) {
		this.adapterName = adapterName;
	}

	/**
	 * @return the adapterClass
	 */
	public String getAdapterClass() {
		return adapterClass;
	}

	/**
	 * @param adapterClass the adapterClass to set
	 */
	public void setAdapterClass(String adapterClass) {
		this.adapterClass = adapterClass;
	}

	/**
	 * @return the adapterRemark
	 */
	public String getAdapterRemark() {
		return adapterRemark;
	}

	/**
	 * @param adapterRemark the adapterRemark to set
	 */
	public void setAdapterRemark(String adapterRemark) {
		this.adapterRemark = adapterRemark;
	}

	/**
	 * @return the adapterCode
	 */
	public String getAdapterCode() {
		return adapterCode;
	}

	/**
	 * @param adapterCode the adapterCode to set
	 */
	public void setAdapterCode(String adapterCode) {
		this.adapterCode = adapterCode;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the ipAddress
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @param ipAddress the ipAddress to set
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * @return the accessPort
	 */
	public String getAccessPort() {
		return accessPort;
	}

	/**
	 * @param accessPort the accessPort to set
	 */
	public void setAccessPort(String accessPort) {
		this.accessPort = accessPort;
	}

	/**
	 * @return the networkType
	 */
	public String getNetworkType() {
		return networkType;
	}

	/**
	 * @param networkType the networkType to set
	 */
	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}

	/**
	 * @return the protocolType
	 */
	public String getProtocolType() {
		return protocolType;
	}

	/**
	 * @param protocolType the protocolType to set
	 */
	public void setProtocolType(String protocolType) {
		this.protocolType = protocolType;
	}

	/**
	 * @return the connectType
	 */
	public String getConnectType() {
		return connectType;
	}

	/**
	 * @param connectType the connectType to set
	 */
	public void setConnectType(String connectType) {
		this.connectType = connectType;
	}

	/**
	 * @return the adapterStatus
	 */
	public String getAdapterStatus() {
		return adapterStatus;
	}

	/**
	 * @param adapterStatus the adapterStatus to set
	 */
	public void setAdapterStatus(String adapterStatus) {
		this.adapterStatus = adapterStatus;
	}

	/**
	 * @return the concurrentNum
	 */
	public Integer getConcurrentNum() {
		return concurrentNum;
	}

	/**
	 * @param concurrentNum the concurrentNum to set
	 */
	public void setConcurrentNum(Integer concurrentNum) {
		this.concurrentNum = concurrentNum;
	}

	/**
	 * @return the overTime
	 */
	public Long getOverTime() {
		return overTime;
	}

	/**
	 * @param overTime the overTime to set
	 */
	public void setOverTime(Long overTime) {
		this.overTime = overTime;
	}

	/**
	 * @return the reConnTime
	 */
	public Long getReConnTime() {
		return reConnTime;
	}

	/**
	 * @param reConnTime the reConnTime to set
	 */
	public void setReConnTime(Long reConnTime) {
		this.reConnTime = reConnTime;
	}

	/**
	 * @return the reSendTimes
	 */
	public Long getReSendTimes() {
		return reSendTimes;
	}

	/**
	 * @param reSendTimes the reSendTimes to set
	 */
	public void setReSendTimes(Long reSendTimes) {
		this.reSendTimes = reSendTimes;
	}

	/**
	 * @return the failCmdTimes
	 */
	public Long getFailCmdTimes() {
		return failCmdTimes;
	}

	/**
	 * @param failCmdTimes the failCmdTimes to set
	 */
	public void setFailCmdTimes(Long failCmdTimes) {
		this.failCmdTimes = failCmdTimes;
	}

	/**
	 * @return the paramMap
	 */
	public Map<String, String> getParamMap() {
		return paramMap;
	}

	/**
	 * @param paramMap the paramMap to set
	 */
	public void setParamMap(Map<String, String> paramMap) {
		this.paramMap = paramMap;
	}
	
	/**
	 * 根据参数名称获取参数值
	 * @param paramName
	 * @return
	 */
	public String getParamValue(String paramName) {
		return this.paramMap.get(paramName);
	}
	
	/**
	 * 设置参数值
	 * 
	 * @param paramName
	 * @param paramValue
	 */
	public void setParamValue(String paramName, String paramValue) {
		this.paramMap.put(paramName, paramValue);
	}

	/**
	 * @return the keepAliveTime
	 */
	public Long getKeepAliveTime() {
		return keepAliveTime;
	}

	/**
	 * @param keepAliveTime the keepAliveTime to set
	 */
	public void setKeepAliveTime(Long keepAliveTime) {
		this.keepAliveTime = keepAliveTime;
	}

	/**
	 * @return the initConcurNum
	 */
	public Integer getInitConcurNum() {
		return initConcurNum;
	}

	/**
	 * @param initConcurNum the initConcurNum to set
	 */
	public void setInitConcurNum(Integer initConcurNum) {
		this.initConcurNum = initConcurNum;
	}
}
