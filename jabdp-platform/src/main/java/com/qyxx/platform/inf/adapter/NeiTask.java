package com.qyxx.platform.inf.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *  系统接口任务
 *  
 *  @author gxj
 *  @version 1.0 2014-4-16 下午02:22:19
 *  @since jdk1.6
 */
public class NeiTask {
	
	/**
	 * 目标编码
	 */
	private String targetCode = "";
	
	/**
	 * 指令内容
	 */
	private String cmd = "";
	
	/**
	 * 返回报告
	 */
	private String report = "";
	
	/**
	 * 消息编码
	 */
	private String msgCode = "";
	
	/**
	 * 是否告警
	 */
	private boolean isAlarm = false;
	
	/**
	 * 取结果标记，true-取结果，false-取发送指令
	 */
	private boolean isGetResult = false;
	
	/**
	 * 用户自定义序列，多个值用分号隔开
	 */
	private String userSeq = "";
	
	/**
	 * 返回结果，Map的key值为:userSeq,handleFlag,errorCode
	 */
	private List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
	
	/**
	 * 是否返回成功标志
	 */
	private Boolean isReturnSuccess = false;


	/**
	 * @return targetCode
	 */
	public String getTargetCode() {
		return targetCode;
	}

	
	/**
	 * @param targetCode
	 */
	public void setTargetCode(String targetCode) {
		this.targetCode = targetCode;
	}

	public String getCmd() {
		return cmd;
	}

	public void setCmd(String cmd) {
		this.cmd = cmd;
	}

	public String getReport() {
		return report;
	}

	public void setReport(String report) {
		this.report = report;
	}

	/**
	 * @return the isAlarm
	 */
	public boolean isAlarm() {
		return isAlarm;
	}

	/**
	 * @param isAlarm the isAlarm to set
	 */
	public void setAlarm(boolean isAlarm) {
		this.isAlarm = isAlarm;
	}

	/**
	 * @return the msgCode
	 */
	public String getMsgCode() {
		return msgCode;
	}

	/**
	 * @param msgCode the msgCode to set
	 */
	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	/**
	 * @return the isGetResult
	 */
	public boolean isGetResult() {
		return isGetResult;
	}

	/**
	 * @param isGetResult the isGetResult to set
	 */
	public void setGetResult(boolean isGetResult) {
		this.isGetResult = isGetResult;
	}

	/**
	 * @return the userSeq
	 */
	public String getUserSeq() {
		return userSeq;
	}

	/**
	 * @param userSeq the userSeq to set
	 */
	public void setUserSeq(String userSeq) {
		this.userSeq = userSeq;
	}

	/**
	 * @return the resultList
	 */
	public List<Map<String, String>> getResultList() {
		return resultList;
	}

	/**
	 * @param resultList the resultList to set
	 */
	public void setResultList(List<Map<String, String>> resultList) {
		this.resultList = resultList;
	}


	
	/**
	 * @return isReturnSuccess
	 */
	public Boolean getIsReturnSuccess() {
		return isReturnSuccess;
	}


	
	/**
	 * @param isReturnSuccess
	 */
	public void setIsReturnSuccess(Boolean isReturnSuccess) {
		this.isReturnSuccess = isReturnSuccess;
	}

	
}
