/*
 * @(#)BsTable.java
 * 2016年10月12日 下午4:11:17
 * 
 * Copyright (c) 2016 QiYunInfoTech - All Rights Reserved.
 * ====================================================================
 * The QiYunInfoTech License, Version 1.0
 *
 * This software is the confidential and proprietary information of QiYunInfoTech.
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with QiYunInfoTech.
 */
package com.qyxx.platform.sysmng.userconfig.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.qyxx.platform.common.module.entity.BaseEntity;
import com.qyxx.platform.sysmng.utils.Constants;

/**
 *  业务表
 *  @author bobgao
 *  @version 1.0 2016年10月12日 下午4:11:17
 *  @since jdk1.7 
 */
@Entity
//表名与类名不相同时重新定义表名.
@Table(name = "SYS_BS_TABLE")
//默认的缓存策略.
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class BsTable extends BaseEntity {

	private Long id;//主键
	
	private String systemCode;//系统编码，默认5位数
	
	private String tableKey;//编码
	
	private String caption;//表名称
	
	private String entityName;//实体名
	
	private String remark;//备注描述
	
	private String allowUserDefField = Constants.ENABLED;//是否允许用户自定义字段
	
	private String status = Constants.STATUS_PASS;//状态--默认审批通过
	
	private String moduleKey; //模块key
	
	private String masterEntityName; //关联主实体名，如果为空，则为主实体
	
	private List<BsField> bsFieldList; //字段列表
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SEQ_SYS_BS_TABLE_ID")
	@SequenceGenerator(name="SEQ_SYS_BS_TABLE_ID", sequenceName="SEQ_SYS_BS_TABLE_ID")
    public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	
	/**
	 * @return systemCode
	 */
	@Column(name="SYSTEM_CODE", length=100)
	public String getSystemCode() {
		return systemCode;
	}

	
	/**
	 * @param systemCode
	 */
	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}

	
	/**
	 * @return tableKey
	 */
	@Column(name="TABLE_KEY", length=200)
	public String getTableKey() {
		return tableKey;
	}

	
	/**
	 * @param tableKey
	 */
	public void setTableKey(String tableKey) {
		this.tableKey = tableKey;
	}

	
	/**
	 * @return entityName
	 */
	@Column(name="ENTITY_NAME", length=400)
	public String getEntityName() {
		return entityName;
	}

	
	/**
	 * @param entityName
	 */
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	
	/**
	 * @return caption
	 */
	@Column(name="CAPTION", length=200)
	public String getCaption() {
		return caption;
	}

	
	/**
	 * @param caption
	 */
	public void setCaption(String caption) {
		this.caption = caption;
	}

	
	/**
	 * @return remark
	 */
	@Column(name="REMARK", length=1000)
	public String getRemark() {
		return remark;
	}

	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	
	/**
	 * @return allowUserDefField
	 */
	@Column(name="ALLOW_USER_DEF_FIELD", length=100)
	public String getAllowUserDefField() {
		return allowUserDefField;
	}

	
	/**
	 * @param allowUserDefField
	 */
	public void setAllowUserDefField(String allowUserDefField) {
		this.allowUserDefField = allowUserDefField;
	}

	
	/**
	 * @return status
	 */
	@Column(name="STATUS", length=100)
	public String getStatus() {
		return status;
	}

	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	
	/**
	 * @return moduleKey
	 */
	@Column(name="MODULE_KEY", length=200)
	public String getModuleKey() {
		return moduleKey;
	}

	
	/**
	 * @param moduleKey
	 */
	public void setModuleKey(String moduleKey) {
		this.moduleKey = moduleKey;
	}

	
	/**
	 * @return masterEntityName
	 */
	@Column(name="MASTER_ENTITY_NAME", length=400)
	public String getMasterEntityName() {
		return masterEntityName;
	}

	
	/**
	 * @param masterEntityName
	 */
	public void setMasterEntityName(String masterEntityName) {
		this.masterEntityName = masterEntityName;
	}

	
	/**
	 * @return bsFieldList
	 */
	@Transient
	public List<BsField> getBsFieldList() {
		return bsFieldList;
	}

	
	/**
	 * @param bsFieldList
	 */
	public void setBsFieldList(List<BsField> bsFieldList) {
		this.bsFieldList = bsFieldList;
	}

}
