/*
 * @(#)UserConfig.java
 * 2014-5-29 下午00:00:27
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.userconfig.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.qyxx.platform.common.module.entity.BaseEntity;


/**
 *  用户信息配置
 *  @author tsd
 *  @version 1.0 2014-5-29 下午00:00:27
 *  @since jdk1.6 
 */
@Entity
//表名与类名不相同时重新定义表名.
@Table(name = "SYS_USER_CONFIG")
//默认的缓存策略.
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UserConfig extends BaseEntity {

	private Long id;//主键
	private Long userId;//用户编号
	private String shortcut;//快捷方式
	private String deskUrl;//桌面背景

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SEQ_SYS_USER_CONFIG_ID")
	@SequenceGenerator(name="SEQ_SYS_USER_CONFIG_ID", sequenceName="SEQ_SYS_USER_CONFIG_ID")
    public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	
	/**
	 * @return userId
	 */
	@Column(name="USER_ID")
	public Long getUserId() {
		return userId;
	}

	
	/**
	 * @param userId
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	
	/**
	 * @return shortcut
	 */
	@Lob
	@Column(name="SHORTCUT", columnDefinition = "ntext")
	public String getShortcut() {
		return shortcut;
	}

	
	/**
	 * @param shortcut
	 */
	public void setShortcut(String shortcut) {
		this.shortcut = shortcut;
	}

	
	/**
	 * @return deskUrl
	 */
	@Column(name="DESK_URL", length=500)
	public String getDeskUrl() {
		return deskUrl;
	}

	
	/**
	 * @param deskUrl
	 */
	public void setDeskUrl(String deskUrl) {
		this.deskUrl = deskUrl;
	}

}
