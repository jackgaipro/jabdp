/*
 * @(#)DeskTopToUserDao.java
 * 2012-9-5 下午06:16:47
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.desktop.dao;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.qyxx.platform.common.orm.Page;
import com.qyxx.platform.common.orm.hibernate.HibernateDao;
import com.qyxx.platform.sysmng.desktop.entity.DesktopToUser;



/**
 *  个人用户桌面Dao类
 *  @author ly
 *  @version 1.0 2012-9-5 下午06:16:47
 *  @since jdk1.6 
 */
@Component
public class DesktopToUserDao extends HibernateDao<DesktopToUser, Long>{
	
	/**
	 * 根据sql语句及参数查询指定数量的记录
	 * 
	 * @param sql
	 * @param pageSize
	 * @param param
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> findDataList(String sql, int pageSize, Map<String, Object> param) {
		SQLQuery sq = getSession().createSQLQuery(sql);
		if(null!=param) {
			sq.setProperties(param);
		}
		sq.setFirstResult(0);
		sq.setMaxResults(pageSize);
		sq.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		return sq.list();
	}

	/*
	@SuppressWarnings("unchecked")
	public Page<Map<String, Object>> findDataPage(Page<Map<String, Object>> page, String sql,Map<String, Object> param) {
		SQLQuery sq = getSession().createSQLQuery(sql);
		if(null!=param) {
			sq.setProperties(param);
		}
		if (page.isAutoCount()) {
			page.setTotalCount(findDataListCount(sql, param));
		}
		sq.setFirstResult(page.getFirst() - 1);
		sq.setMaxResults(page.getPageSize());
		sq.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		List<Map<String, Object>> result = sq.list();
		page.setResult(result);
		return page;
	}*/
	
	/**
	 * 根据sql语句查询数据集记录
	 * 
	 * @param sql
	 * @param param
	 * @return
	 */
	/*
	public Long findDataListCount(String sql, Map<String, Object> param) {
		String fromHql = sql;
		//select子句与order by子句会影响count查询,进行简单的排除.
		fromHql = "from " + StringUtils.substringAfter(fromHql, "from");
		fromHql = StringUtils.substringBefore(fromHql, "order by");
		String countSql = "select count(*) " + fromHql;
		long totalCount =0L;
		try {
			SQLQuery cq = getSession().createSQLQuery(countSql);
			if(null!=param) {
				cq.setProperties(param);
			}
			totalCount = ((Number) cq.uniqueResult()).longValue();
		} catch (Exception e) {
			throw new RuntimeException("sql can't be auto count, sql is:" + countSql, e);
		}
		return totalCount;
	}*/
}
