/*
 * @(#)OnlineUserSessionListener.java
 * 2012-7-29 下午06:12:02
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.accountmng.web;

import javax.servlet.http.HttpSessionEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.session.HttpSessionEventPublisher;

import com.qyxx.platform.common.utils.spring.SpringContextHolder;
import com.qyxx.platform.sysmng.accountmng.service.OnlineUserManager;


/**
 *  在线用户session监听器
 *  @author gxj
 *  @version 1.0 2012-7-29 下午06:12:02
 *  @since jdk1.6 
 */

public class OnlineUserSessionListener extends HttpSessionEventPublisher{

	private static Logger logger = LoggerFactory.getLogger(OnlineUserSessionListener.class);
	
	@Override
	public void sessionDestroyed(HttpSessionEvent event) {
		String sessionId = event.getSession().getId();
		try {
			OnlineUserManager onlineUserManager = SpringContextHolder.getBean("onlineUserManager");
			onlineUserManager.removeOnlineUser(sessionId);
			logger.info("从在线用户表中移除sessionId为" + sessionId + "的用户记录操作成功！");
		} catch(Exception e) {
			logger.error("从在线用户表中移除sessionId为" + sessionId + "的用户记录出现异常", e);
		} finally {
			super.sessionDestroyed(event);
		}
	}
	
}
