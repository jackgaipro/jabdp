package com.qyxx.platform.sysmng.tie.web;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Namespace;

import com.qyxx.platform.common.json.JsonOutput;
import com.qyxx.platform.common.module.web.CrudActionSupport;
import com.qyxx.platform.common.utils.web.struts2.Struts2Utils;
import com.qyxx.platform.sysmng.accountmng.entity.User;
import com.qyxx.platform.sysmng.attach.entity.Attach;
import com.qyxx.platform.sysmng.attach.entity.AttachList;
import com.qyxx.platform.sysmng.attach.service.AttachManager;
import com.qyxx.platform.sysmng.tie.entity.Tie;
import com.qyxx.platform.sysmng.tie.service.TieManager;
import com.qyxx.platform.sysmng.utils.Constants;

@Namespace("/sys/tie")
public class TieAction extends CrudActionSupport<Tie> {
	
	private TieManager tieManager;
	private int pageindex;
	private int pageSize;
	private Long relevanceId;
	private String moduleName;
	private Long tieId;
	
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	
	public void setTieId(Long tieId) {
		this.tieId = tieId;
	}

	public void setRelevanceId(Long relevanceId) {
		this.relevanceId = relevanceId;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public void setPageindex(int pageindex) {
		this.pageindex = pageindex;
	}

	public void setTieManager(TieManager tieManager) {
		this.tieManager = tieManager;
	}

	@JsonOutput
	@Override
	public String list() throws Exception {
		// TODO Auto-generated method stub
		List<Map<String, Object>> map = tieManager.selectTies(relevanceId, moduleName, pageindex, pageSize);
		Struts2Utils.renderJson(map);
		return null;
	}

	@Override
	public String input() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@JsonOutput
	@Override
	public String save() throws Exception {
		// TODO Auto-generated method stub
		Map<String,Object> map = new HashMap<String,Object>();
		User user = (User)Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		Date time = new Date();
		Long id = tieManager.saveTie(entity,user,time);
		map.put("id", id);
		map.put("nickname", user.getNickname());
		map.put("loginName", user.getLoginName());
		map.put("time", time);
		Struts2Utils.renderJson(map);
		return null;
	}

	@JsonOutput
	@Override
	public String view() throws Exception {
		// TODO Auto-generated method stub
		List<Map<String, Object>> map = tieManager.selectParentTies(entity.getParentId());
		Struts2Utils.renderJson(map);
		return null;
	}

	@Override
	public String delete() throws Exception {
		// TODO Auto-generated method stub
		User user = (User)Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		boolean b = tieManager.deleteTie(tieId, user.getId());
		Struts2Utils.renderJson(b);
		return null;
	}

	@Override
	protected void prepareModel() throws Exception {
		// TODO Auto-generated method stub
		entity = new Tie();
	}
	
}
