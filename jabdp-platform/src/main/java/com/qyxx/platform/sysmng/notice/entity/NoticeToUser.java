/*
 * @(#)NoticeToUser.java
 * 2012-9-5 下午04:09:37
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.notice.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 *  
 *  @author ly
 *  @version 1.0 2012-9-5 下午04:09:37
 *  @since jdk1.6 
 */
/**
 * 用户通知关联表
 */
@Entity
@Table(name = "SYS_NOTICE_TO_USER")
// 默认的缓存策略.
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class NoticeToUser {
	public static final String STATUS_0="0";//0.未读  1.阅读
	public static final String STATUS_1="1";
	private Long id;
	private Notice notice;
	private Long userId;
	private Date readTime;
	private String status;//0.未读  1.阅读
	
	private String userName;

	/**
	 * @return id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_SYS_NOTICE_TO_USER_ID")
	@SequenceGenerator(name = "SEQ_SYS_NOTICE_TO_USER_ID", sequenceName = "SEQ_SYS_NOTICE_TO_USER_ID")
	public Long getId() {
		return id;
	}

	

	/**
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	

	/**
	 * @return notice
	 */
	@ManyToOne
	@JoinColumn(name = "NOTICE_ID")
	public Notice getNotice() {
		return notice;
	}

	

	/**
	 * @param notice
	 */
	public void setNotice(Notice notice) {
		this.notice = notice;
	}

	

	/**
	 * @return userId
	 */
	@Column(name = "USER_ID")
	public Long getUserId() {
		return userId;
	}

	

	/**
	 * @param userId
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	

	/**
	 * @return readTime
	 */
	@Column(name = "READ_TIME")
	public Date getReadTime() {
		return readTime;
	}

	

	/**
	 * @param readTime
	 */
	public void setReadTime(Date readTime) {
		this.readTime = readTime;
	}

	

	/**
	 * @return status
	 */
	@Column(name = "STATUS", length = 1)
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	
	/**
	 * @return userName
	 */
	@Transient
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	

}
