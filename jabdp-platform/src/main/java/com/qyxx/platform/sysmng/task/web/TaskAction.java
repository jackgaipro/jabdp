/*
 * @(#)TaskAction.java
 * 2013-2-25 下午03:00:39
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.task.web;

import java.util.List;

import org.apache.struts2.convention.annotation.Namespace;
import org.springframework.beans.factory.annotation.Autowired;

import com.qyxx.platform.common.json.JsonOutput;
import com.qyxx.platform.common.module.web.CrudActionSupport;
import com.qyxx.platform.common.orm.PropertyFilter;
import com.qyxx.platform.common.utils.web.struts2.Struts2Utils;
import com.qyxx.platform.sysmng.accountmng.entity.User;
import com.qyxx.platform.sysmng.task.entity.Task;
import com.qyxx.platform.sysmng.task.service.TaskManager;
import com.qyxx.platform.sysmng.utils.Constants;


/**
 *  任务Action
 *  @author gxj
 *  @version 1.0 2013-2-25 下午03:00:39
 *  @since jdk1.6 
 */
@Namespace("/sys/task")
public class TaskAction extends CrudActionSupport<Task> {

	private static final long serialVersionUID = 6579274374515156621L;
	
	private TaskManager taskManager;

	/**
	 * @return taskManager
	 */
	public TaskManager getTaskManager() {
		return taskManager;
	}

	
	/**
	 * @param taskManager
	 */
	@Autowired
	public void setTaskManager(TaskManager taskManager) {
		this.taskManager = taskManager;
	}

	@Override
	public String list() throws Exception {
		return SUCCESS;
	}
	
	/**
	 * 分页查询
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String queryList() throws Exception {
		List<PropertyFilter> filters = PropertyFilter.buildFromHttpRequest(Struts2Utils.getRequest());
		pager = taskManager.searchTask(pager, filters);
		Struts2Utils.renderJson(pager);
		return null;
	}
	
	/**
	 * 查询任务
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String findList() throws Exception {
		List<PropertyFilter> filters = PropertyFilter.buildFromHttpRequest(Struts2Utils.getRequest());
		List<Task> list = taskManager.findTaskList(filters);
		formatMessage.setMsg(list);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	@Override
	public String input() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@JsonOutput
	public String save() throws Exception {
		User user = (User)Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		taskManager.saveTask(entity, user.getId());
		formatMessage.setMsg(entity.getId());
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	@Override
	@JsonOutput
	public String view() throws Exception {
		formatMessage.setMsg(entity);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	@Override
	@JsonOutput
	public String delete() throws Exception {
		taskManager.deleteTask(ids);
		formatMessage.setMsg(ids);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	@Override
	protected void prepareModel() throws Exception {
		try {
			if (id != null) {
				entity = taskManager.getTask(id);
			} else {
				entity = new Task();
			}	
		} catch(Exception e) {
			logger.error("", e);
		}
	}
	
	

}
