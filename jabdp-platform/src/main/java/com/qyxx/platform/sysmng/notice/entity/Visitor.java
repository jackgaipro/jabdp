package com.qyxx.platform.sysmng.notice.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *  
 *  @author tsd
 *  @version 1.0 2015-04-08 下午04:09:37
 *  @since jdk1.6 
 */

/**
 * 新闻访问者关联表
 */
@Entity
@Table(name = "SYS_VISITOR")
// 默认的缓存策略.
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Visitor {
	
	private Long id;
	private Long relevanceId;
	private Long userId;
	private Date visitTime;
	
	private String userName;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_SYS_VISITOR_ID")
	@SequenceGenerator(name = "SEQ_SYS_VISITOR_ID", sequenceName = "SEQ_SYS_VISITOR_ID")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "RELEVANCE_ID")
	public Long getRelevanceId() {
		return relevanceId;
	}
	
	public void setRelevanceId(Long relevanceId) {
		this.relevanceId = relevanceId;
	}
	
	@Column(name = "USER_ID")
	public Long getUserId() {
		return userId;
	}
	
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	@Column(name = "VISIT_TIME")
	public Date getVisitTime() {
		return visitTime;
	}
	
	public void setVisitTime(Date visitTime) {
		this.visitTime = visitTime;
	}
	
	/**
	 * @return userName
	 */
	@Transient
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
}
