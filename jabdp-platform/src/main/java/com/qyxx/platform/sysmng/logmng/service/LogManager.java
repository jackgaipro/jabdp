/*
 * @(#)LogManager.java
 * 2011-5-17 上午10:49:53
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.logmng.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.qyxx.platform.common.orm.Page;
import com.qyxx.platform.common.orm.PropertyFilter;
import com.qyxx.platform.sysmng.logmng.dao.LogDao;
import com.qyxx.platform.sysmng.logmng.entity.Log;



/**
 *  日志管理类
 *  @author YB
 *  @version 1.0
 *  @since 1.6 2011-5-17 上午10:49:53
 */
@Component
@Transactional
public class LogManager {
	private static Logger logger = LoggerFactory.getLogger(LogManager.class);
	private LogDao logDao;
	@Autowired
	public void setLogDao(LogDao logDao) {
		this.logDao = logDao;
	}
	/**
	 * 使用属性过滤条件查询日志
	 */
	@Transactional(readOnly = true)
	public Page<Log> searchLog(final Page<Log> page, final List<PropertyFilter> filters) {
		return logDao.findPage(page, filters);
	}

	//-- Log Manager --//
	@Transactional(readOnly = true)
	public Log getLog(Long id) {
		return logDao.get(id);
	}
	
	/**
	 * 保存日志
	 * 
	 * @param entity
	 */
	public void saveLog(Log entity) {
		logDao.save(entity);
	}

}
