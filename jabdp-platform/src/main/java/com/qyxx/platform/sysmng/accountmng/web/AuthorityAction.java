package com.qyxx.platform.sysmng.accountmng.web;

import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;

import com.qyxx.platform.common.json.JsonOutput;
import com.qyxx.platform.common.module.service.ServiceException;
import com.qyxx.platform.common.module.web.CrudActionSupport;
import com.qyxx.platform.common.orm.PropertyFilter;
import com.qyxx.platform.common.utils.web.ServletUtils;
import com.qyxx.platform.common.utils.web.struts2.Struts2Utils;
import com.qyxx.platform.sysmng.accountmng.entity.Authority;
import com.qyxx.platform.sysmng.accountmng.entity.AuthorityJson;
import com.qyxx.platform.sysmng.accountmng.entity.User;
import com.qyxx.platform.sysmng.accountmng.entity.UserJson;
import com.qyxx.platform.sysmng.accountmng.service.AuthorityManager;
import com.qyxx.platform.sysmng.utils.Constants;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

/**
 * 权限处理类
 */
@Namespace("/sys/account")
@Results({
		@Result(name = CrudActionSupport.RELOAD, location = "authority.action", type = "redirect"),
		@Result(name = "authoritylist", location = "/WEB-INF/content/sys/authority/authority.jsp"),
		@Result(name = "resourcelist", location = "/WEB-INF/content/sys/authority/resource-mng.jsp")})
public class AuthorityAction extends CrudActionSupport<Authority>{


	private static final long serialVersionUID = 1L;

	
	private Authority entity;
	private AuthorityManager authorityManager;
	private Long id;
	private Long orgId;
	private Long rid;
	private Long[] rids;
	private Long parentId;
	private String aids;
	//private 	resName
	
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	// -- ModelDriven 与 Preparable函数 --//
	public Authority getModel() {
		return entity;
	}

	
	/**
	 * @return aids
	 */
	public String getAids() {
		return aids;
	}

	
	/**
	 * @param aids
	 */
	public void setAids(String aids) {
		this.aids = aids;
	}

	public Long[] getRids() {
		return rids;
	}
	
	public void setRids(Long[] rids) {
		this.rids = rids;
	}

	public Long getRid() {
		return rid;
	}

	
	public void setRid(Long rid) {
		this.rid = rid;
	}

	public Long getOrgId() {
		return orgId;
	}
	
	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public AuthorityManager getAuthorityManager() {
		return authorityManager;
	}
	@Autowired
	public void setAuthorityManager(AuthorityManager authorityManager) {
		this.authorityManager = authorityManager;
	}
	@Override
	@JsonOutput
	/**
	 * 获取当前用户所拥有的权限
	 * 
	 * @return
	 * @throws Exception
	 */
	public String list() throws Exception {
		User user = (User)Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		Long userId = user.getId();
		List<AuthorityJson> list = authorityManager.getUserAuthorityList(userId,id);
		formatMessage.setMsg(list);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	@Override
	public String input() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@JsonOutput
	/**
	 * 保存权限设置
	 * 
	 * @return
	 * @throws Exception
	 */
	public String save() throws Exception {
		//从request中获取含属性前缀名的参数,构造去除前缀名后的参数Map.
		Map<String, Object> authorityLocaleMap = ServletUtils.getParametersStartingWith(Struts2Utils.getRequest(), "locale_");
		authorityManager.saveResource(entity,parentId,authorityLocaleMap);
		formatMessage.setMsg(true);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	@JsonOutput
	/**
	 * 获取权限信息
	 * 
	 * @return
	 * @throws Exception
	 */
	public String modifyInfo() throws Exception {		
		entity = authorityManager.getAuthority(id);
		formatMessage.setMsg(entity);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	@Override
	public String view() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@JsonOutput
	/**
	 * 删除权限（只是改变权限状态）
	 * 
	 * @return
	 * @throws Exception
	 */
	public String delete() throws Exception {
	
			if (rids == null) {
				throw new ServiceException("请选择你要删除的资源", new Exception(
						"请选择你要删除的资源"));
			}
			for (Long id : rids) {
				 authorityManager.deleteAuthority(Long.valueOf(id));
			}
			formatMessage.setMsg(true);
			Struts2Utils.renderJson(formatMessage);

			return null;
		
	}
	@JsonOutput
	public String recover() throws Exception {
	
			if (rids == null) {
				throw new ServiceException("请选择你要恢复的资源", new Exception(
						"请选择你要恢复的资源"));
			}
			for (Long id : rids) {
				 authorityManager.recoverAuthority(Long.valueOf(id));
			}
			formatMessage.setMsg(true);
			Struts2Utils.renderJson(formatMessage);
			return null;	
	}
	@JsonOutput
	/**
	 * 彻底删除权限
	 * 
	 * @return
	 * @throws Exception
	 */
	public String shiftDelete() throws Exception {
			if (rids == null) {
				throw new ServiceException("请选择你要删除的资源", new Exception(
						"请选择你要删除的资源"));
			}
			for (Long id : rids) {
				authorityManager.shiftDeleteAuthority(Long.valueOf(id));
		}			
			formatMessage.setMsg(true);
			Struts2Utils.renderJson(formatMessage);
			return null;	
	}

	@Override
	protected void prepareModel() throws Exception {
		if (id != null) {
			entity = authorityManager.getAuthority(id);
		} else {
			entity = new Authority();
		}
		
	}
	@JsonOutput
	/**
	 * 获取权限集合
	 * 
	 * @return
	 * @throws Exception
	 */
	public String queryList() throws Exception {
		List<AuthorityJson> list = authorityManager.getAuthorityList(id);
		formatMessage.setMsg(list);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	/**
	 * 用户列表
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String userList() throws Exception {
		List<UserJson> list = authorityManager.getUserList(rid,orgId);
		formatMessage.setMsg(list);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	/**
	 * 组织用户树
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String userTree() throws Exception {
		List<UserJson> list = authorityManager.getUserTree(orgId);
		formatMessage.setMsg(list);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	/**
	 * 获取角色可见用户树列表
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String seeUserList() throws Exception {
		List<UserJson> list = authorityManager.getSeeUserList(rid,orgId);
		formatMessage.setMsg(list);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	/**
	 * 分页查询所有资源列表
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String resourceList() throws Exception {
		List<PropertyFilter> filters = PropertyFilter.buildFromHttpRequest(Struts2Utils.getRequest());
		/*String status = Constants.NO_USING+","+Constants.IS_USING;
		PropertyFilter statusFilter = new PropertyFilter(
				"INS_resourceStatus", status);
		filters.add(statusFilter);*/
		
		if(aids != null) {
			PropertyFilter filter = new PropertyFilter("INL_id",aids);
			filters.add(filter);
		}
		pager = authorityManager.findResourceList(pager, filters);
		Struts2Utils.renderJson(pager);
		return null;
	}
	
	/**
	 * 查询所有资源，展现为树形
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String queryAllList() throws Exception {
		List<AuthorityJson> list = authorityManager.getAuthorityList();
		formatMessage.setMsg(list);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	/**
	 * 获取角色权限ID列表
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String getRoleAuthIds() throws Exception {
		List<Long> list = authorityManager.getRoleAuthorityId(rid);
		formatMessage.setMsg(list);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	public String init() throws Exception {
		return "authoritylist";
	}
	public String initResource() throws Exception {
		return "resourcelist";
	}
}
