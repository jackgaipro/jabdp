/*
 * @(#)OnlineUser.java
 * 2011-5-16 下午11:38:04
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.accountmng.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 *  在线用户
 *  
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-5-16 下午11:38:04
 */
@Entity
@Table(name = "SYS_ONLINE_USER")
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class OnlineUser implements Serializable{

	private static final long serialVersionUID = -291065382988745118L;
	
	private Long id;
	
	private String loginUsername;
	
	private String loginIp;
	
	private String loginMachine;
	
	private Date loginTime;
	
	private String sessionId;
	
	private User user;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SEQ_SYS_ONLINE_USER_ID")
	@SequenceGenerator(name="SEQ_SYS_ONLINE_USER_ID", sequenceName="SEQ_SYS_ONLINE_USER_ID")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	
	/**
	 * @return loginUsername
	 */
	public String getLoginUsername() {
		return loginUsername;
	}

	
	/**
	 * @param loginUsername
	 */
	public void setLoginUsername(String loginUsername) {
		this.loginUsername = loginUsername;
	}

	
	/**
	 * @return loginIp
	 */
	public String getLoginIp() {
		return loginIp;
	}

	
	/**
	 * @param loginIp
	 */
	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}

	
	/**
	 * @return loginMachine
	 */
	public String getLoginMachine() {
		return loginMachine;
	}

	
	/**
	 * @param loginMachine
	 */
	public void setLoginMachine(String loginMachine) {
		this.loginMachine = loginMachine;
	}

	
	/**
	 * @return loginTime
	 */
	public Date getLoginTime() {
		return loginTime;
	}

	
	/**
	 * @param loginTime
	 */
	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	
	/**
	 * @return sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}

	
	/**
	 * @param sessionId
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * @return user
	 */
	@JsonIgnore
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="LOGIN_USERID", updatable = false)
	public User getUser() {
		return user;
	}

	
	/**
	 * @param user
	 */
	public void setUser(User user) {
		this.user = user;
	}

}
