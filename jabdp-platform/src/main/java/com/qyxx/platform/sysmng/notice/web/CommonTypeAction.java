/*
 * @(#)CommonTypeAction.java
 * 2013-3-4 下午03:55:19
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.notice.web;

import java.util.List;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;

import com.qyxx.platform.common.json.JsonOutput;
import com.qyxx.platform.common.module.web.CrudActionSupport;
import com.qyxx.platform.common.utils.web.struts2.Struts2Utils;
import com.qyxx.platform.sysmng.accountmng.entity.RoleJson;
import com.qyxx.platform.sysmng.accountmng.entity.User;
import com.qyxx.platform.sysmng.notice.entity.CommonType;
import com.qyxx.platform.sysmng.notice.service.CommonTypeManager;
import com.qyxx.platform.sysmng.utils.Constants;


/**
 *  通用分类Action
 *  @author gxj
 *  @version 1.0 2013-3-4 下午03:55:19
 *  @since jdk1.6 
 */
@Namespace("/sys/common")
@Results({
	@Result(name = "init", location = "/WEB-INF/content/sys/systemType/type-mng.jsp")
})
public class CommonTypeAction extends CrudActionSupport<CommonType> {

	/**
	 * long
	 */
	private static final long serialVersionUID = 489825188624065650L;
	
	private CommonTypeManager commonTypeManager;
	private String roleIds;
	private String type;
	private String typeVal;
	private String isSys;
	
	public String getTypeVal() {
		return typeVal;
	}



	
	public void setTypeVal(String typeVal) {
		this.typeVal = typeVal;
	}



	public String getRoleIds() {
		return roleIds;
	}


	
	public void setRoleIds(String roleIds) {
		this.roleIds = roleIds;
	}
	
	/**
	 * @return isSys
	 */
	public String getIsSys() {
		return isSys;
	}

	
	/**
	 * @param isSys
	 */
	public void setIsSys(String isSys) {
		this.isSys = isSys;
	}

	/**
	 * @return commonTypeManager
	 */
	public CommonTypeManager getCommonTypeManager() {
		return commonTypeManager;
	}

	
	/**
	 * @param commonTypeManager
	 */
	@Autowired
	public void setCommonTypeManager(CommonTypeManager commonTypeManager) {
		this.commonTypeManager = commonTypeManager;
	}

	/**
	 * @return type
	 */
	public String getType() {
		return type;
	}


	
	/**
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}


	@Override
	public String list() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String input() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@JsonOutput
	public String save() throws Exception {
		User user = (User) Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		commonTypeManager.saveCommonType(entity, user.getId());
		formatMessage.setMsg(entity);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	@Override
	public String view() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@JsonOutput
	public String delete() throws Exception {
		commonTypeManager.deleteCommonType(ids);
		formatMessage.setMsg(ids);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	@Override
	protected void prepareModel() throws Exception {
		if(null!=id) {
			entity = commonTypeManager.get(id);
		} else {
			entity = new CommonType();
		}
	}
	
	/**
	 * 根据类型获取所有分类
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String queryList() throws Exception {
		if(CommonType.TYPE_SYSTEM.equals(isSys)) {
			User user = (User) Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
			List<CommonType> list = commonTypeManager.findSysCommonType(type, user.getId());
			formatMessage.setMsg(list);
		} else {
			List<CommonType> list = commonTypeManager.findCommonType(type);
			formatMessage.setMsg(list);
		}
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	/**
	 * 根据类型获取所有分类
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String queryListByKey() throws Exception {
		String q = Struts2Utils.getParameter("q");
		List<CommonType> list = commonTypeManager.getTypeListByIds(q);
		formatMessage.setMsg(list);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	/**
	 *获取所有系统分类
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String getAllSystemType() throws Exception {
		List<CommonType> list = commonTypeManager.getAllSystemType();
		formatMessage.setMsg(list);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	/**
	 * 根据id获取指定的系统类型
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getSystemType() throws Exception {
		entity = commonTypeManager.get(id);
		formatMessage.setMsg(entity);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	/**
	 * 根据类型id获取关联角色
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String getRoleList() throws Exception {
		List<RoleJson> list = commonTypeManager.getRoleList(id);
		formatMessage.setMsg(list);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	public void prepareSaveSystemType() throws Exception {
		if(null!=id) {
			entity = commonTypeManager.get(id);//修改的话：将页面 上的数据和数据库里的进行合并
		} else {
			entity = new CommonType();//新增
		}
	} 
	/**
	 * 保存系统类型
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String saveSystemType() throws Exception {
		User user = (User) Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		commonTypeManager.saveSystemType(entity,roleIds,user.getId());
		formatMessage.setMsg(true);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	/**
	 * 删除系统类型
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String deleteSystemType() throws Exception {
		commonTypeManager.deleteSystemType(id);
		formatMessage.setMsg(true);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	@JsonOutput
	public String validateTypeVal() throws Exception {
		Boolean flag = commonTypeManager.validateVal(typeVal,id);
		formatMessage.setMsg(flag);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	@JsonOutput
	public String getTypeList() throws Exception {
		List<CommonType> list = commonTypeManager.getTypeList(id);
		formatMessage.setMsg(list);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	/**
	 * 进入系统分类管理界面
	 * 
	 * @return
	 * @throws Exception
	 */
	public String initSystemType() throws Exception {
		return "init";
	}
	
}
