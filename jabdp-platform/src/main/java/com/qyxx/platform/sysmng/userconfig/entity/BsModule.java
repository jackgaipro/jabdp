/*
 * @(#)BsModule.java
 * 2017年04月14日 下午11:07:17
 * 
 * Copyright (c) 2016 QiYunInfoTech - All Rights Reserved.
 * ====================================================================
 * The QiYunInfoTech License, Version 1.0
 *
 * This software is the confidential and proprietary information of QiYunInfoTech.
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with QiYunInfoTech.
 */
package com.qyxx.platform.sysmng.userconfig.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.qyxx.platform.common.module.entity.BaseEntity;
import com.qyxx.platform.sysmng.utils.Constants;

/**
 *  模块表
 *  @author bobgao
 *  @version 1.0 2017年04月14日 下午11:07:17
 *  @since jdk1.7 
 */
@Entity
//表名与类名不相同时重新定义表名.
@Table(name = "SYS_BS_MODULE")
//默认的缓存策略.
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class BsModule extends BaseEntity {

	private Long id;//主键
	
	private String caption;//模块名称
	
	private String remark;//备注描述
	
	private String status = Constants.STATUS_PASS;//状态--默认审批通过
	
	private String moduleKey; //模块key
	
	private String moduleType; //模块类型
	
	private String refModuleKey; //引用关联公用模块key
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SEQ_SYS_BS_MODULE_ID")
	@SequenceGenerator(name="SEQ_SYS_BS_MODULE_ID", sequenceName="SEQ_SYS_BS_MODULE_ID")
    public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}


	/**
	 * @return caption
	 */
	@Column(name="CAPTION", length=200)
	public String getCaption() {
		return caption;
	}

	
	/**
	 * @param caption
	 */
	public void setCaption(String caption) {
		this.caption = caption;
	}

	
	/**
	 * @return remark
	 */
	@Column(name="REMARK", length=1000)
	public String getRemark() {
		return remark;
	}

	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	
	/**
	 * @return status
	 */
	@Column(name="STATUS", length=100)
	public String getStatus() {
		return status;
	}

	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	
	/**
	 * @return moduleKey
	 */
	@Column(name="MODULE_KEY", length=200)
	public String getModuleKey() {
		return moduleKey;
	}

	
	/**
	 * @param moduleKey
	 */
	public void setModuleKey(String moduleKey) {
		this.moduleKey = moduleKey;
	}

	
	/**
	 * @return moduleType
	 */
	@Column(name="MODULE_TYPE", length=100)
	public String getModuleType() {
		return moduleType;
	}

	
	/**
	 * @param moduleType
	 */
	public void setModuleType(String moduleType) {
		this.moduleType = moduleType;
	}

	
	/**
	 * @return refModuleKey
	 */
	@Column(name="REF_MODULE_KEY", length=200)
	public String getRefModuleKey() {
		return refModuleKey;
	}

	
	/**
	 * @param refModuleKey
	 */
	public void setRefModuleKey(String refModuleKey) {
		this.refModuleKey = refModuleKey;
	}

	
	

}
