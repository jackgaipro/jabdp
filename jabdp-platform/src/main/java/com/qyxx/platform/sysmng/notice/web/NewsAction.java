/*
 * @(#)NewsAction.java
 * 2012-9-4 下午06:02:10
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.notice.web;


import java.util.List;


import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;

import com.qyxx.platform.common.json.JsonOutput;
import com.qyxx.platform.common.module.web.CrudActionSupport;
import com.qyxx.platform.common.module.web.Messages;
import com.qyxx.platform.common.orm.PropertyFilter;
import com.qyxx.platform.common.utils.web.struts2.Struts2Utils;
import com.qyxx.platform.sysmng.accountmng.entity.User;
import com.qyxx.platform.sysmng.notice.entity.CommonType;
import com.qyxx.platform.sysmng.notice.entity.News;
import com.qyxx.platform.sysmng.notice.service.CommonTypeManager;
import com.qyxx.platform.sysmng.notice.service.NewsManager;
import com.qyxx.platform.sysmng.utils.Constants;

/**
 * 
 * @author ly
 * @version 1.0 2012-9-4 下午06:02:10
 * @since jdk1.6
 */
@Namespace("/sys/notice")
@Results({})
public class NewsAction extends CrudActionSupport<News> {

	private NewsManager newsManager;
	private CommonTypeManager commonTypeManager;
	private Long atmId ;
	
	// 操作方法
	private String operMethod = CrudActionSupport.VIEW;
	private String name;
	private String organizationIds;		
	private String status;
		
	public String getStatus() {
		return status;
	}



	
	public void setStatus(String status) {
		this.status = status;
	}



	public String getOrganizationIds() {
		return organizationIds;
	}


	
	public void setOrganizationIds(String organizationIds) {
		this.organizationIds = organizationIds;
	}


	public Long getAtmId() {
		return atmId;
	}

	
	public void setAtmId(Long atmId) {
		this.atmId = atmId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOperMethod() {
		return operMethod;
	}

	public void setOperMethod(String operMethod) {
		this.operMethod = operMethod;
	}

	public NewsManager getNewsManager() {
		return newsManager;
	}

	public CommonTypeManager getCommonTypeManager() {
		return commonTypeManager;
	}
	
	@Autowired
	public void setCommonTypeManager(CommonTypeManager commonTypeManager) {
		this.commonTypeManager = commonTypeManager;
	}

	@Autowired
	public void setNewsManager(NewsManager newsManager) {
		this.newsManager = newsManager;
	}

	@Override
	public String list() throws Exception {
		// TODO Auto-generated method stub
		return SUCCESS;
	}

	/**
	 * 
	 * 查询一条新闻
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String queryNews() throws Exception {
		entity = newsManager.findNewsById(id);
		formatMessage.setMsg(entity);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	
	/**
	 * 
	 * 查询一条新闻
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String viewNewsInit() throws Exception {
		entity = newsManager.addNewsClickCount(id);
		formatMessage.setMsg(entity);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	

	/**
	 * 查询新闻列表
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String queryList() throws Exception {
		List<PropertyFilter> filters = PropertyFilter
				.buildFromHttpRequest(Struts2Utils.getRequest());
		User user = (User) Struts2Utils
				.getSessionAttribute(Constants.USER_SESSION_ID);
		if(!Constants.IS_SUPER_USERR.equals(user.getIsSuperAdmin())) {
			PropertyFilter filter = new PropertyFilter("EQL_createUser",
					String.valueOf(user.getId()));
			filters.add(filter);
		}
		pager = newsManager.findNewsList(pager, filters);
		Struts2Utils.renderJson(pager);
		return null;

	}

	@Override
	public final String input() throws Exception {

		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 跳转到发布新闻页面
	 * 
	 * @return
	 * @throws Exception
	 */
	public String add() throws Exception {
		operMethod = CrudActionSupport.ADD;
		return VIEW;
	}

	/**
	 * 保存一条新闻
	 * @see com.qyxx.platform.common.module.web.CrudActionSupport#save()
	 */
	@Override
	@JsonOutput
	public String save() throws Exception {
		User user = (User) Struts2Utils
				.getSessionAttribute(Constants.USER_SESSION_ID);
		newsManager.saveNews(entity, user.getId(),organizationIds);
		formatMessage.setMsg(entity.getId());
		Struts2Utils.renderJson(formatMessage);
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 修改新闻的发布状态
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String repeal() throws Exception {
		User user = (User) Struts2Utils
				.getSessionAttribute(Constants.USER_SESSION_ID);
		newsManager.repealNews(id, user.getId(),status);
		formatMessage.setMsg(id);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	/**
	 * 跳转到修改新闻页面
	 * @see com.qyxx.platform.common.module.web.CrudActionSupport#view()
	 */
	@Override
	public String view() throws Exception {
		operMethod = CrudActionSupport.VIEW;
		return VIEW;
	}

	/**
	 * 跳转到复制新闻页面
	 * 
	 * @return
	 * @throws Exception
	 */
	public String edit() throws Exception {
		operMethod = CrudActionSupport.EDIT;
		return VIEW;
	}

	/**
	 * 删除新闻
	 * @see com.qyxx.platform.common.module.web.CrudActionSupport#delete()
	 */
	@Override
	@JsonOutput
	public String delete() throws Exception {
		newsManager.deleteNews(ids);
		formatMessage.setMsg(true);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	

	@Override
	protected void prepareModel() throws Exception {
		if (id != null) {
			entity = newsManager.findNewsById(id);
		} else {
			entity = new News();
		}

	}

	

/**
 * 更新一条新闻
 * 
 * @return
 * @throws Exception
 */
	@JsonOutput
	public String updateNews() throws Exception {
		newsManager.updateNews(id,atmId);
		formatMessage.setMsg(true);
		Struts2Utils.renderJson(formatMessage);
		return null;

	}
	
	

	
	/**
	 * 跳转到新闻查看页面
	 * 
	 * @return
	 * @throws Exception
	 */
	public String viewNews() throws Exception {
		String ntuId=Struts2Utils.getParameter("ntuId");
		Struts2Utils.getSession().setAttribute("ntuId", ntuId);
		User user = (User) Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		return "query-view";
	}

	
	public String newsToUserList() throws Exception {
		return "query";
	}
	
	/**
	 * 查找所有已发布的新闻
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String getAllNews() throws Exception {
		
		List<PropertyFilter> filters = PropertyFilter
				.buildFromHttpRequest(Struts2Utils.getRequest());
		User user = (User) Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		pager = newsManager
				.findNewsToUserList(pager,user,filters);
		Struts2Utils.renderJson(pager);
         return null;
    }
	

	/**
	 * 查询分类类型
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String queryNewsType() throws Exception{
		List<CommonType> commonTypes=commonTypeManager.findNewsType();
		formatMessage.setMsg(commonTypes);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	/**
	 * 增加新闻类型
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
    public String addNewsType() throws Exception{
    	User user = (User) Struts2Utils
				.getSessionAttribute(Constants.USER_SESSION_ID);
    	//判断数据库中是否存在该类型,不存在才能添加，存在就提示不能添加重复类型
    	List<CommonType> list=commonTypeManager.findNewsTypeByName(name);
    	if(list==null || list.isEmpty()){
    		CommonType ct=commonTypeManager.addNewsType(name, user.getId(),id);
        	formatMessage.setMsg(ct.getId());
    		Struts2Utils.renderJson(formatMessage);
    	}else{
    		formatMessage.setFlag(Messages.FAIL);
    		formatMessage.setMsg("该类型已存在!");
    		Struts2Utils.renderJson(formatMessage);
    	}
    	
		return null;
    }

	
}
