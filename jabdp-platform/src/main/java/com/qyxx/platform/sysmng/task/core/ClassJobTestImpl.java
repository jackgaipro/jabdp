/*
 * @(#)ClassJobTestImpl.java
 * 2013-3-1 下午12:17:14
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.task.core;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *  class类测试任务
 *  @author gxj
 *  @version 1.0 2013-3-1 下午12:17:14
 *  @since jdk1.6 
 */
@DisallowConcurrentExecution
public class ClassJobTestImpl implements Job{
	
	private static Logger logger = LoggerFactory.getLogger(ClassJobTestImpl.class);

	@Override
	public void execute(JobExecutionContext context)
													throws JobExecutionException {
		Long time = System.currentTimeMillis();
		logger.info(time + "test1...");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info(time + "Hello World, Class Job Test Sucessfully.");
	}

}
