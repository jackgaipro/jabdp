package com.qyxx.platform.sysmng.checkins.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "SYS_CHECKINS")
public class Checkins {
	
	public static final String TYPE_1 = "1";//手动签到
	public static final String TYPE_2 = "2";//自动GPS跟踪
	
	private Long id;
	private double lng; // 经度
	private double lat; // 维度
	private Long userId;
	private Date checkinsTime; // 签到时间
	private String title; // 主题
	private String content; // 内容
	private String palce; // 地址
	private String attachContent;//附件内容
	private String type;//类型，1-手动签到；2-自动GPS跟踪
	
	private String userName;
	
	@Column(name = "PALCE", length = 50)
	public String getPalce() {
		return palce;
	}
	public void setPalce(String palce) {
		this.palce = palce;
	}
	
	@Column(name = "LNG", scale = 14)
	public double getLng() {
		return lng;
	}
	public void setLng(double lng) {
		this.lng = lng;
	}
	
	@Column(name = "LAT", scale = 14)
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	
	@Column(name = "TITLE", length = 50)
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	@Column(name = "CONTENT", length = 1000)
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SEQ_SYS_ATTACH_ID")
	@SequenceGenerator(name="SEQ_SYS_ATTACH_ID", sequenceName="SEQ_SYS_ATTACH_ID")
	@Column(name = "ID", nullable = false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "USER_ID")
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CHECHINS_TIME")
	public Date getCheckinsTime() {
		return checkinsTime;
	}
	public void setCheckinsTime(Date checkinsTime) {
		this.checkinsTime = checkinsTime;
	}

	
	/**
	 * @return attachContent
	 */
	@Lob
	@Column(name = "ATTACH_CONTENT", columnDefinition = "ntext")
	public String getAttachContent() {
		return attachContent;
	}
	
	/**
	 * @param attachContent
	 */
	public void setAttachContent(String attachContent) {
		this.attachContent = attachContent;
	}
	
	/**
	 * @return type
	 */
	@Column(name = "TYPE", length = 2)
	public String getType() {
		return type;
	}
	
	/**
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * @return userName
	 */
	@Transient
	public String getUserName() {
		return userName;
	}
	
	/**
	 * @param userName
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
	
}
