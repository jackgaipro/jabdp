/*
 * @(#)Log.java
 * 2011-5-16 下午07:53:18
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.logmng.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import com.qyxx.platform.common.module.entity.BaseEntity;


/**
 *  变更日志记录
 *  
 *  @author gxj
 *  @version 1.0 2013年12月21日 下午6:47:30
 *  @since jdk1.6
 */
@Entity
//表名与类名不相同时重新定义表名.
@Table(name = "SYS_CHANGE_LOG")
//默认的缓存策略.
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ChangeLog implements Serializable{

	private static final long serialVersionUID = -6985954353434473756L;
	private Long id;
	
	private String moduleNameEn; //模块英文名
	private String moduleNameCn; //模块中文名
	
	private String tableNameEn; //表英文名
	private String tableNameCn; //表中文名
	
	private String fieldNameEn; //字段英文名
	private String fieldNameCn; //字段中文名
	
	private String entityName; //实体名
	private String entityId; //实体主键值
	private String subEntityId; //子实体主键值
	
	private String beforeVal; //变更前值
	private String afterVal; //变更后值
	private String changeDesc; //变更详情
	
	private Long userId; //操作用户ID
	private String loginName;//登录账户
	private String realName;//账户名称
	private String employeeName;//员工姓名
	private String orgName;//组织名称
	
	private Date operTime;//变更时间
	private String operIp;//变更IP
	

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SEQ_SYS_CHANGE_LOG_ID")
	@SequenceGenerator(name="SEQ_SYS_CHANGE_LOG_ID", sequenceName="SEQ_SYS_CHANGE_LOG_ID")
    public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	
	/**
	 * @return moduleNameEn
	 */
	@Column(name="MODULE_NAME_EN",length=100)
	public String getModuleNameEn() {
		return moduleNameEn;
	}

	
	/**
	 * @param moduleNameEn
	 */
	public void setModuleNameEn(String moduleNameEn) {
		this.moduleNameEn = moduleNameEn;
	}

	
	/**
	 * @return moduleNameCn
	 */
	@Column(name="MODULE_NAME_CN",length=100)
	public String getModuleNameCn() {
		return moduleNameCn;
	}

	
	/**
	 * @param moduleNameCn
	 */
	public void setModuleNameCn(String moduleNameCn) {
		this.moduleNameCn = moduleNameCn;
	}

	
	/**
	 * @return tableNameEn
	 */
	@Column(name="TABLE_NAME_EN",length=100)
	public String getTableNameEn() {
		return tableNameEn;
	}

	
	/**
	 * @param tableNameEn
	 */
	public void setTableNameEn(String tableNameEn) {
		this.tableNameEn = tableNameEn;
	}

	
	/**
	 * @return tableNameCn
	 */
	@Column(name="TABLE_NAME_CN",length=100)
	public String getTableNameCn() {
		return tableNameCn;
	}

	
	/**
	 * @param tableNameCn
	 */
	public void setTableNameCn(String tableNameCn) {
		this.tableNameCn = tableNameCn;
	}

	
	/**
	 * @return fieldNameEn
	 */
	@Column(name="FIELD_NAME_EN",length=100)
	public String getFieldNameEn() {
		return fieldNameEn;
	}

	
	/**
	 * @param fieldNameEn
	 */
	public void setFieldNameEn(String fieldNameEn) {
		this.fieldNameEn = fieldNameEn;
	}

	
	/**
	 * @return fieldNameCn
	 */
	@Column(name="FIELD_NAME_CN",length=100)
	public String getFieldNameCn() {
		return fieldNameCn;
	}

	
	/**
	 * @param fieldNameCn
	 */
	public void setFieldNameCn(String fieldNameCn) {
		this.fieldNameCn = fieldNameCn;
	}

	
	/**
	 * @return entityName
	 */
	@Column(name="ENTITY_NAME",length=200)
	public String getEntityName() {
		return entityName;
	}

	
	/**
	 * @param entityName
	 */
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	
	/**
	 * @return entityId
	 */
	@Column(name="ENTITY_ID",length=100)
	public String getEntityId() {
		return entityId;
	}

	
	/**
	 * @param entityId
	 */
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	
	/**
	 * @return subEntityId
	 */
	@Column(name="SUB_ENTITY_ID",length=100)
	public String getSubEntityId() {
		return subEntityId;
	}

	
	/**
	 * @param subEntityId
	 */
	public void setSubEntityId(String subEntityId) {
		this.subEntityId = subEntityId;
	}

	/**
	 * @return beforeVal
	 */
	@Column(name="BEFORE_VAL",length=1000)
	public String getBeforeVal() {
		return beforeVal;
	}

	
	/**
	 * @param beforeVal
	 */
	public void setBeforeVal(String beforeVal) {
		this.beforeVal = beforeVal;
	}

	
	/**
	 * @return afterVal
	 */
	@Column(name="AFTER_VAL",length=1000)
	public String getAfterVal() {
		return afterVal;
	}

	
	/**
	 * @param afterVal
	 */
	public void setAfterVal(String afterVal) {
		this.afterVal = afterVal;
	}

	
	/**
	 * @return changeDesc
	 */
	@Lob
	@Column(name="CHANGE_DESC", columnDefinition = "ntext")
	public String getChangeDesc() {
		return changeDesc;
	}

	
	/**
	 * @param changeDesc
	 */
	public void setChangeDesc(String changeDesc) {
		this.changeDesc = changeDesc;
	}

	
	/**
	 * @return userId
	 */
	@Column(name="USER_ID")
	public Long getUserId() {
		return userId;
	}

	
	/**
	 * @param userId
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	
	/**
	 * @return loginName
	 */
	@Column(name="LOGIN_NAME",length=100)
	public String getLoginName() {
		return loginName;
	}

	
	/**
	 * @param loginName
	 */
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	
	/**
	 * @return realName
	 */
	@Column(name="REAL_NAME",length=100)
	public String getRealName() {
		return realName;
	}

	
	/**
	 * @param realName
	 */
	public void setRealName(String realName) {
		this.realName = realName;
	}

	
	/**
	 * @return employeeName
	 */
	@Column(name="EMPLOYEE_NAME",length=100)
	public String getEmployeeName() {
		return employeeName;
	}

	
	/**
	 * @param employeeName
	 */
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	
	/**
	 * @return orgName
	 */
	@Column(name="ORG_NAME",length=100)
	public String getOrgName() {
		return orgName;
	}

	
	/**
	 * @param orgName
	 */
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	
	/**
	 * @return operTime
	 */
	@Column(name="OPER_TIME")
	@Temporal(value = TemporalType.TIMESTAMP)
	public Date getOperTime() {
		return operTime;
	}

	
	/**
	 * @param operTime
	 */
	public void setOperTime(Date operTime) {
		this.operTime = operTime;
	}

	
	/**
	 * @return operIp
	 */
	@Column(name="OPER_IP",length=100)
	public String getOperIp() {
		return operIp;
	}

	
	/**
	 * @param operIp
	 */
	public void setOperIp(String operIp) {
		this.operIp = operIp;
	}

	

}
