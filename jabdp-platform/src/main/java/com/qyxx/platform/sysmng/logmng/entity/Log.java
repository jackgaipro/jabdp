/*
 * @(#)Log.java
 * 2011-5-16 下午07:53:18
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.logmng.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import com.qyxx.platform.common.module.entity.BaseEntity;


/**
 *  日志表
 *  @author YB
 *  @version 1.0
 *  @since 1.6 2011-5-16 下午07:53:18
 */
@Entity
//表名与类名不相同时重新定义表名.
@Table(name = "SYS_LOG")
//默认的缓存策略.
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Log implements Serializable{
	/**
	 * long
	 */
	private static final long serialVersionUID = -1049386625972108604L;
	private Long id;
	private String operationUrl;//操作URL
	private String operationMethod;//操作方法
	private String operationName;//操作
	private String operationDesc;//操作描述
	private Date operationTime;//操作时间
	private String ip;
	private String machineName;
	private String remark;
	private String loginName;//登录名
	private String realName;//真实姓名
	private String orgName;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SEQ_SYS_LOG_ID")
	@SequenceGenerator(name="SEQ_SYS_LOG_ID", sequenceName="SEQ_SYS_LOG_ID")
    public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	@Column(name="OPERATION_URL",length=500)
	public String getOperationUrl() {
		return operationUrl;
	}
	
	public void setOperationUrl(String operationUrl) {
		this.operationUrl = operationUrl;
	}
	@Column(name="OPERATION_METHOD",length=100)
	public String getOperationMethod() {
		return operationMethod;
	}
	
	public void setOperationMethod(String operationMethod) {
		this.operationMethod = operationMethod;
	}
	@Column(name="OPERATION_NAME",length=100)
	public String getOperationName() {
		return operationName;
	}
	
	public void setOperationName(String operationName) {
		this.operationName = operationName;
	}
	@Column(name="OPERATION_DESC",length=1000)
	public String getOperationDesc() {
		return operationDesc;
	}
	
	public void setOperationDesc(String operationDesc) {
		this.operationDesc = operationDesc;
	}
	@Column(name="OPERATION_TIME",nullable=false)
	@Temporal(value = TemporalType.TIMESTAMP)
	public Date getOperationTime() {
		return operationTime;
	}
	
	public void setOperationTime(Date operationTime) {
		this.operationTime = operationTime;
	}
	@Column(name="OPERATION_IP",length=100)
	public String getIp() {
		return ip;
	}
	
	public void setIp(String ip) {
		this.ip = ip;
	}
	@Column(name="OPERATION_MACHINE_NAME",length=100)
	public String getMachineName() {
		return machineName;
	}
	
	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}
	@Column(name="REAMRK",length=500)
	public String getRemark() {
		return remark;
	}
	
	public void setRemark(String remark) {
		this.remark = remark;
	}
	@Column(name="LOGIN_NAME",length=50)
	public String getLoginName() {
		return loginName;
	}
	
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	@Column(name="REAL_NAME",length=50)
	public String getRealName() {
		return realName;
	}
	
	public void setRealName(String realName) {
		this.realName = realName;
	}
	@Column(name="ORG_NAME",length=100)
	public String getOrgName() {
		return orgName;
	}
	
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	
	

}
