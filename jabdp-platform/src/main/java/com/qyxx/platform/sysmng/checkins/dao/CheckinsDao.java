package com.qyxx.platform.sysmng.checkins.dao;

import org.springframework.stereotype.Component;

import com.qyxx.platform.common.orm.hibernate.HibernateDao;
import com.qyxx.platform.sysmng.checkins.entity.Checkins;

@Component
public class CheckinsDao extends HibernateDao<Checkins, Long> {

}
