/*
 * @(#)TaskScheduler.java
 * 2013-2-25 下午02:09:41
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.task.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.qyxx.platform.common.module.entity.BaseEntity;


/**
 *  任务调度实体类
 *  @author gxj
 *  @version 1.0 2013-2-25 下午02:09:41
 *  @since jdk1.6 
 */
@Entity
@Table(name = "SYS_TASK_SCHEDULER")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class TaskScheduler extends BaseEntity {
	/**
	 *  Everyday--每天；
	 	Someday--每月；
		Date--指定日期；
		Week--每周；
		Hour--每隔*小时；
		Minute--每隔*分钟；
		Second--每隔*秒；
	 */
	public static final String DATE_TYPE_EVERYDAY = "Everyday";
	public static final String DATE_TYPE_SOMEDAY = "Someday";
	public static final String DATE_TYPE_DATE = "Date";
	public static final String DATE_TYPE_WEEK = "Week";
	public static final String DATE_TYPE_HOUR = "Hour";
	public static final String DATE_TYPE_MINUTE = "Minute";
	public static final String DATE_TYPE_SECOND = "Second";
	
	/**
	 * 任务调度运行状态
	 * 0--停止
	 * 1--运行
	 */
	public static final String STATUS_STOP = "0";
	public static final String STATUS_RUN = "1";
	
	private Long id;
	
	private String name;
	
	private String remark;
	
	private String dateType;
	
	private String runDay;
	
	private String runHour;
	
	private String runMinute;
	
	private String runSecond;
	
	private String runMonth;
	
	private String runWeek;
	
	private String status;
	
	private Long taskId;
	
	/**
	 * @return id
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SEQ_SYS_TASK_SCHEDULER_ID")
	@SequenceGenerator(name="SEQ_SYS_TASK_SCHEDULER_ID", sequenceName="SEQ_SYS_TASK_SCHEDULER_ID")
	public Long getId() {
		return id;
	}

	
	/**
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	
	/**
	 * @return name
	 */
	@Column(name="NAME",length=200)
	public String getName() {
		return name;
	}

	
	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	
	/**
	 * @return remark
	 */
	@Column(name="REMARK",length=500)
	public String getRemark() {
		return remark;
	}

	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}


	
	/**
	 * @return dateType
	 */
	@Column(name="DATE_TYPE",length=50)
	public String getDateType() {
		return dateType;
	}


	
	/**
	 * @param dateType
	 */
	public void setDateType(String dateType) {
		this.dateType = dateType;
	}


	
	/**
	 * @return runDay
	 */
	@Column(name="RUN_DAY",length=50)
	public String getRunDay() {
		return runDay;
	}


	
	/**
	 * @param runDay
	 */
	public void setRunDay(String runDay) {
		this.runDay = runDay;
	}


	
	/**
	 * @return runHour
	 */
	@Column(name="RUN_HOUR",length=50)
	public String getRunHour() {
		return runHour;
	}


	
	/**
	 * @param runHour
	 */
	public void setRunHour(String runHour) {
		this.runHour = runHour;
	}


	
	/**
	 * @return runMinute
	 */
	@Column(name="RUN_MINUTE",length=50)
	public String getRunMinute() {
		return runMinute;
	}


	
	/**
	 * @param runMinute
	 */
	public void setRunMinute(String runMinute) {
		this.runMinute = runMinute;
	}


	
	/**
	 * @return runSecond
	 */
	@Column(name="RUN_SECOND",length=50)
	public String getRunSecond() {
		return runSecond;
	}


	
	/**
	 * @param runSecond
	 */
	public void setRunSecond(String runSecond) {
		this.runSecond = runSecond;
	}


	
	/**
	 * @return runMonth
	 */
	@Column(name="RUN_MONTH",length=50)
	public String getRunMonth() {
		return runMonth;
	}


	
	/**
	 * @param runMonth
	 */
	public void setRunMonth(String runMonth) {
		this.runMonth = runMonth;
	}


	
	/**
	 * @return runWeek
	 */
	@Column(name="RUN_WEEK",length=50)
	public String getRunWeek() {
		return runWeek;
	}


	
	/**
	 * @param runWeek
	 */
	public void setRunWeek(String runWeek) {
		this.runWeek = runWeek;
	}


	
	/**
	 * @return status
	 */
	@Column(name="STATUS",length=50)
	public String getStatus() {
		return status;
	}


	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}


	
	/**
	 * @return taskId
	 */
	@Column(name="TASK_ID")
	public Long getTaskId() {
		return taskId;
	}


	
	/**
	 * @param taskId
	 */
	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}
	
}
