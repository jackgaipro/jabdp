package com.qyxx.platform.gsmng.common.service;

import java.util.Date;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qyxx.platform.gsmng.common.dao.ProcessInfoDao;
import com.qyxx.platform.gsmng.common.entity.ProcessInfo;
/**
 *结束事件处理类
 */
@Service
@Transactional
public class EndEvent implements JavaDelegate {

	private ProcessInfoDao processInfoDao;
	
	private GsMngManager gsMngManager;
	
	private RuntimeService runtimeService;
	
	protected ProcessEngineConfigurationImpl processEngineConfigurationImpl;
	
	protected HistoryService historyService;
	
	@Autowired
	public void setRuntimeService(RuntimeService runtimeService) {
		this.runtimeService = runtimeService;
	}
	
	@Autowired
	public void setGsMngManager(GsMngManager gsMngManager) {
		this.gsMngManager = gsMngManager;
	}
	
	@Autowired
	public void setProcessEngineConfigurationImpl(
			ProcessEngineConfigurationImpl processEngineConfigurationImpl) {
		this.processEngineConfigurationImpl = processEngineConfigurationImpl;
	}

	@Autowired
	public void setProcessInfoDao(ProcessInfoDao processInfoDao) {
		this.processInfoDao = processInfoDao;
	}
	@Autowired
	public void setHistoryService(HistoryService historyService) {
		this.historyService = historyService;
	}

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		ProcessInfo entity = new ProcessInfo();
		String processInstanceId = execution.getProcessInstanceId();
		entity.setProcessInstanceId(processInstanceId);
		Object times= execution.getVariable("countVao");
		entity.setCountVal((Long)times);
		entity.setDealTime(new Date());
		entity.setUserId(1L);
		entity.setDealName("系统自动处理");
		entity.setTaskName("结束申请");
		entity.setActivitiId(execution.getCurrentActivityId());	
		HistoricProcessInstance historicProcessInstance = historyService.createHistoricProcessInstanceQuery().processInstanceId(processInstanceId).singleResult();
		String bKey = historicProcessInstance.getBusinessKey();
		if(StringUtils.isBlank(bKey)){
			bKey = (String) execution.getVariable("varInBusinessKey");
		}
		String bbbbbbb = execution.getProcessBusinessKey();
		String a[] = bKey.split("\\|");
		String bname = a[0];// 模块名称
		Long bid = Long.valueOf(a[1]);// 单据ID
		entity.setBusinessId(bid);
		entity.setBusinessName(bname);
		processInfoDao.save(entity);
		
		gsMngManager.setEntityName(entity.getBusinessName());
		String isApprove=(String)execution.getVariable("approve");
		if("1".equals(isApprove)){
			gsMngManager.updateFlowColumn(entity.getBusinessId(),gsMngManager.STATUS_PASS,null);
		}else if("0".equals(isApprove)){
			gsMngManager.updateFlowColumn(entity.getBusinessId(),gsMngManager.STATUS_NO_PASS,null);
		}else{
			gsMngManager.updateFlowColumn(entity.getBusinessId(),gsMngManager.STATUS_FINISH,null);
		}
		
	}

}
