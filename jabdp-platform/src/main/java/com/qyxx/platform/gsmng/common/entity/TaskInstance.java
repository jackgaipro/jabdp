/*
 * @(#)ProcessInfo.java
 * 2012-9-13 上午11:01:38
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.gsmng.common.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 *  流程任务历史表
 *  
 *  @author xk
 *  @version 1.0 2012-12-27 上午09:35:18
 *  @since jdk1.6
 */
@Entity
@Table(name = "ACT_HI_TASKINST")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class TaskInstance implements Serializable {

	private String id;
	private String processDefinitionId;
	private String processInstanceId;
	private String taskDefinitionKey;          
	private String executionId;
	private String name;
	private String parentTaskId;
	private String description;
	private String owner;
	private String assignee;
	private Date startTime;
	private Date endTime;
	private Long duration ;
	private String deleteReason ;
	private int priority;
	private Date dueDate;
	private String moduleName;
	@Id
	@Column(name="ID_",length=64,nullable=false)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	@Column(name="PROC_DEF_ID_", length=64)
	public String getProcessDefinitionId() {
		return processDefinitionId;
	}
	
	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}
	@Column(name="PROC_INST_ID_",nullable=true)
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	@Column(name="TASK_DEF_KEY_",nullable=true)
	public String getTaskDefinitionKey() {
		return taskDefinitionKey;
	}
	
	public void setTaskDefinitionKey(String taskDefinitionKey) {
		this.taskDefinitionKey = taskDefinitionKey;
	}
	@Column(name="EXECUTION_ID_",nullable=true)
	public String getExecutionId() {
		return executionId;
	}
	
	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}
	@Column(name="NAME_")
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	@Column(name="PARENT_TASK_ID_" ,nullable=true,insertable=false,updatable=false)
	public String getParentTaskId() {
		return parentTaskId;
	}
	
	public void setParentTaskId(String parentTaskId) {
		this.parentTaskId = parentTaskId;
	}
	@Column(name="DESCRIPTION_",nullable=true)
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	@Column(name="OWNER_",nullable=true)
	public String getOwner() {
		return owner;
	}
	
	public void setOwner(String owner) {
		this.owner = owner;
	}
	@Column(name="ASSIGNEE_",nullable=true)
	public String getAssignee() {
		return assignee;
	}
	
	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}
	@Column(name="START_TIME_",nullable=true)
	@Temporal(value = TemporalType.TIMESTAMP)
	public Date getStartTime() {
		return startTime;
	}
	
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	@Column(name="END_TIME_",nullable=true)
	@Temporal(value = TemporalType.TIMESTAMP)
	public Date getEndTime() {
		return endTime;
	}
	
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	@Column(name="DURATION_",nullable=true)
	public Long getDuration() {
		return duration;
	}
	
	public void setDuration(Long duration) {
		this.duration = duration;
	}
	@Column(name="PARENT_TASK_ID_",nullable=true)
	public String getDeleteReason() {
		return deleteReason;
	}
	
	public void setDeleteReason(String deleteReason) {
		this.deleteReason = deleteReason;
	}
	@Column(name="PRIORITY_",nullable=true)
	public int getPriority() {
		return priority;
	}
	
	public void setPriority(int priority) {
		this.priority = priority;
	}
	@Column(name="DUE_DATE_",nullable=true)
	@Temporal(value = TemporalType.TIMESTAMP)
	public Date getDueDate() {
		return dueDate;
	}
	
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	 @Transient
	public String getModuleName() {
		return moduleName;
	}

	
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}


}
