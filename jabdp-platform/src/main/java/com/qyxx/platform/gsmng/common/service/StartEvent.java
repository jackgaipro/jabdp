package com.qyxx.platform.gsmng.common.service;

import java.util.Date;
import java.util.List;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.context.Context;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.interceptor.CommandExecutor;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qyxx.platform.common.orm.hibernate.HibernateDao;
import com.qyxx.platform.gsmng.common.dao.ProcessInfoDao;
import com.qyxx.platform.gsmng.common.entity.ProcessInfo;
/**
 *启动事件处理类
 */
@Service
@Transactional
public class StartEvent extends HibernateDao<Object, Long> implements
		JavaDelegate {

	private ProcessInfoDao processInfoDao;

	private RuntimeService runtimeService;
	
	private GsMngManager gsMngManager;

	protected ProcessEngineConfigurationImpl processEngineConfigurationImpl;
	
	@Autowired
	public void setGsMngManager(GsMngManager gsMngManager) {
		this.gsMngManager = gsMngManager;
	}

	@Autowired
	public void setRuntimeService(RuntimeService runtimeService) {
		this.runtimeService = runtimeService;
	}

	@Autowired
	public void setProcessEngineConfigurationImpl(
			ProcessEngineConfigurationImpl processEngineConfigurationImpl) {
		this.processEngineConfigurationImpl = processEngineConfigurationImpl;
	}

	@Autowired
	public void setProcessInfoDao(ProcessInfoDao processInfoDao) {
		this.processInfoDao = processInfoDao;
	}

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		String varOutOrder = (String) execution
				.getVariable("varInBusinessKey");
		if (StringUtils.isNotBlank(varOutOrder)) {
			ProcessInfo entity = new ProcessInfo();
			String processInstanceId = execution
					.getProcessInstanceId();
			entity.setProcessInstanceId(processInstanceId);
			entity.setDealTime(new Date());
			entity.setUserId(1L);
			entity.setDealName("admin");
			entity.setTaskName("发起申请");
			execution.setVariable("countVao", 1L);
			entity.setCountVal(1L);

			String a[] = varOutOrder.split("\\|");
			String bname = a[0];// 模块名称
			Long bid = Long.valueOf(a[1]);// 单据ID

			entity.setBusinessId(bid);
			entity.setBusinessName(bname);
			String b[] = bname.split("\\.");
			final String processDefinitionKey = b[0];
			CommandExecutor commandExecutor = processEngineConfigurationImpl
					.getCommandExecutor();
			ProcessDefinitionEntity processDefinition = commandExecutor
					.execute(new Command<ProcessDefinitionEntity>() {

						public ProcessDefinitionEntity execute(
								CommandContext commandContext) {
							return Context
									.getProcessEngineConfiguration()
									.getDeploymentManager()
									.findDeployedLatestProcessDefinitionByKey(
											processDefinitionKey);
						}
					});
			List<ActivityImpl> activitiList = processDefinition
					.getActivities();
			for (ActivityImpl activityImpl : activitiList) {

				if ("startEvent".equals(activityImpl
						.getProperty("type"))) {
					entity.setActivitiId(activityImpl.getId());

				}
			}
			processInfoDao.save(entity);
			gsMngManager.setEntityName(entity.getBusinessName());
			gsMngManager.updateFlowColumn(entity.getBusinessId(),gsMngManager.STATUS_APPROVAL,processInstanceId );
		}

	}

}
