/*
 * @(#)TemplateConfig.java
 * 2011-8-23 下午07:21:15
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.common.utils.template;

/**
 * 模板配置类
 * 
 * @author YB
 * @version 1.0
 * @since 1.6 2011-8-23 下午07:21:15
 */

public class TemplateConfig {
	public static final String LOADPATH = "E:/javasoft/jiawasoft/jiawasoft-gsweb/src/main/resources/template/gsc/";// 模板路径
	public static final String DEFAULTENCODING = "UTF-8";// 默认编码
	public static final String INPUTINCODING = "";// 读入编码 为空表示自动识别
	public static final String OUTPUTINCODING = "UTF-8";// 输出编码

}
