/*
 * @(#)IdEntity.java
 * 2011-5-9 下午08:20:34
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.common.module.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.persistence.Transient;

/**
 *  统一定义id的entity基类.
 * 
 *  基类统一定义id的属性名称、数据类型、列名映射及生成策略.
 *  子类可重载getId()函数重定义id的列名映射和生成策略.
 *  
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-5-9 下午08:20:34
 */
//JPA 基类的标识
@MappedSuperclass
public abstract class BaseEntity {

	protected Long createUser;
	
	protected Date createTime;
	
	protected Long lastUpdateUser;
	
	protected Date lastUpdateTime;	
	
	protected Long version;
	
	protected String createUserCaption;

	@Column(updatable = false)
	public Long getCreateUser() {
		return createUser;
	}

	
	public void setCreateUser(Long createUser) {
		this.createUser = createUser;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	public Date getCreateTime() {
		return createTime;
	}

	
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	
	public Long getLastUpdateUser() {
		return lastUpdateUser;
	}

	
	public void setLastUpdateUser(Long lastUpdateUser) {
		this.lastUpdateUser = lastUpdateUser;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}

	
	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	/**
	 * 乐观锁
	 * 
	 * @return
	 */
	@Version
	public Long getVersion() {
		return version;
	}

	
	public void setVersion(Long version) {
		this.version = version;
	}


	
	/**
	 * @return createUserCaption
	 */
	@Transient
	public String getCreateUserCaption() {
		return createUserCaption;
	}


	
	/**
	 * @param createUserCaption
	 */
	public void setCreateUserCaption(String createUserCaption) {
		this.createUserCaption = createUserCaption;
	}
	
	
	
}
