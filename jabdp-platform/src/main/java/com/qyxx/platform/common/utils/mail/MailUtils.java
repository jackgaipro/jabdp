/*
 * @(#)MailUtils.java
 * 2014-1-7 下午11:07:02
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.common.utils.mail;

import java.util.Properties;

import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * 邮件操作工具类
 * @author gxj
 * @version 1.0 2014-1-7 下午11:07:02
 * @since jdk1.6
 */

public class MailUtils {

	/**
	 * 发送邮件的props文件
	 */
	private final transient Properties props = new Properties();
	/**
	 * 邮件服务器登录验证
	 */
	private transient MailAuthenticator authenticator;

	/**
	 * 邮箱session
	 */
	private transient Session session;

	/**
	 * 初始化邮件发送器
	 * 
	 * @param smtpHostName
	 *            SMTP邮件服务器地址
	 * @param username
	 *            发送邮件的用户名(地址)
	 * @param password
	 *            发送邮件的密码
	 */
	public MailUtils(final String smtpHostName,
			final String username, final String password) {
		init(username, password, smtpHostName);
	}

	/**
	 * 初始化邮件发送器
	 * 
	 * @param username
	 *            发送邮件的用户名(地址)，并以此解析SMTP服务器地址
	 * @param password
	 *            发送邮件的密码
	 */
	public MailUtils(final String username, final String password) {
		// 通过邮箱地址解析出smtp服务器，对大多数邮箱都管用
		final String smtpHostName = "smtp." + username.split("@")[1];
		init(username, password, smtpHostName);
	}

	/**
	 * 初始化
	 * 
	 * @param username
	 *            发送邮件的用户名(地址)
	 * @param password
	 *            密码
	 * @param smtpHostName
	 *            SMTP主机地址
	 */
	private void init(String username, String password,
			String smtpHostName) {
		// 初始化props
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.host", smtpHostName);
		//props.put("mail.smtp.port", 587);
		// 验证
		authenticator = new MailAuthenticator(username, password, null);
		// 创建session
		session = Session.getInstance(props, authenticator);
	}

	/**
	 * 群发邮件
	 * 
	 * @param recipients
	 *            收件人邮箱集合
	 * @param subject
	 *            主题
	 * @param content
	 *            内容
	 * @param cc 抄送邮箱
	 * @param bcc 密送邮箱
	 * @throws AddressException
	 * @throws MessagingException
	 */
	public void send(String[] recipients, String subject,
			Object content, String[] cc, String[] bcc) throws AddressException,
							MessagingException {
		// 创建mime类型邮件
		final MimeMessage message = new MimeMessage(session);
		// 设置发信人
		message.setFrom(new InternetAddress(authenticator
				.getUsername()));
		// 设置收件人
		/*final int num = recipients.length;
		InternetAddress[] addresses = new InternetAddress[num];
		for (int i = 0; i < num; i++) {
			addresses[i] = new InternetAddress(recipients[i]);
		}
		message.setRecipients(RecipientType.TO, addresses);*/
		
		//设置收件人
		if(recipients!=null && recipients.length > 0) {
			for(String recp : recipients) {
				message.addRecipients(RecipientType.TO, InternetAddress.parse(recp));
			}
		} else {
			throw new RuntimeException("收件人邮箱未设置");
		}
		
		//设置抄送人
		if(cc!=null && cc.length > 0) {
			for(String c : cc) {
				message.addRecipients(RecipientType.CC, InternetAddress.parse(c));
			}
		}
		
		//设置密送人
		if(bcc!=null && bcc.length > 0) {
			for(String b : bcc) {
				message.addRecipients(RecipientType.BCC, InternetAddress.parse(b));
			}
		}
		
		// 设置主题
		message.setSubject(subject);
		// 设置邮件内容
		message.setContent(content.toString(),
				"text/html;charset=utf-8");
		// 发送
		Transport.send(message);
	}
	
	public static void main(String[] args) {
		/*MailUtils mu = new MailUtils("smtp.exmail.qq.com","gxj@jiawasoft.com","");
		String msg = "<html><head></head><body><a href='http://www.163.com'>Hello, 网易</a></body></html>";
		try {
			mu.send(new String[]{"2355278131@qq.com"}, "自动测试邮件111", msg, null, null);
		} catch (AddressException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block  2355278131@qq.com bzp11141845! smtp.qq.com
			e.printStackTrace();
		}*/
		MailUtils mu = new MailUtils("192.168.1.253","ship.confirm@cobetterfilter.com","123456");
		//MailUtils mu = new MailUtils("smtp.cobetter.com.cn","survey@cobetter.com.cn","Cobetter150209");
		String msg = "您订的货物已发货，请注意查收。";
		try {
			mu.send(new String[]{"22454930@qq.com"}, "发货通知", msg, null, null);
		} catch (AddressException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
