/*
 * @(#)NumberArraySequence.java
 * 2012-7-4 下午06:38:40
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.common.freemarker;

import freemarker.template.SimpleNumber;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateNumberModel;
import freemarker.template.TemplateSequenceModel;

/**
 *  自定义数字数组序列
 *  
 *  @author gxj
 *  @version 1.0 2012-7-4 下午06:38:40
 *  @since jdk1.6
 */
public class NumberArraySequence implements TemplateSequenceModel {
	
    private TemplateNumberModel[] array;

    public NumberArraySequence(int size) {
    	this.array = new TemplateNumberModel[size];
    	for(int i=0;i<size;i++) {
    		this.array[i] = new SimpleNumber(0);
    	}
    }

    public TemplateModel get(int index) throws TemplateModelException {
        TemplateNumberModel result = array[index];
        return result;
    }

    public int size() throws TemplateModelException {
        return this.array.length;
    }
    
    public void setValue(int index, int value) {
    	array[index] = new SimpleNumber(value);
    }
}
