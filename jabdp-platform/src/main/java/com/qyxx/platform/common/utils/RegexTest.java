/*
 * @(#)RegexTest.java
 * 2014-9-1 下午03:46:05
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.common.utils;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;


/**
 *  
 *  @author gxj
 *  @version 1.0 2014-9-1 下午03:46:05
 *  @since jdk1.6 
 */

public class RegexTest {
	
	public static double div(double v1, double v2, int scale) {// 除法
		BigDecimal b1 = new BigDecimal(Double.toString(v1));
		BigDecimal b2 = new BigDecimal(Double.toString(v2));
		System.out.println(b1.divide(b2, scale, BigDecimal.ROUND_HALF_UP));
		return b1.divide(b2, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
	}

	/**
	 * 
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		/*String hql = FileUtils.readFileToString(new File("E:/project/yk/ss.txt"));
		Pattern p = Pattern.compile("order\\s*by\\s*[\\w\\s,.]*[\\s]*", Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(hql);
		StringBuffer sb = new StringBuffer();
		String orderStr = "";
		int start = 0;
		int len = hql.length();
		while (m.find()) {
			//m.appendReplacement(sb, "");
			orderStr = m.group();
			System.out.println(orderStr);
			if(m.end()==len) {
				start = m.start();
			}
		}
		//m.appendTail(sb);
		//return sb.toString();
		System.out.println(start);
		if(start > 0) {
			hql = hql.substring(0, start);
		}
		System.out.println(hql);*/
		System.out.println(div(200, 1.17, 15));
	}

}
