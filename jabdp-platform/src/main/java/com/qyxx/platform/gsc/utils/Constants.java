/*
 * @(#)Constants.java
 * 2011-5-15 下午01:04:17
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.gsc.utils;

import java.util.Map;

import com.google.common.collect.Maps;


/**
 *  GS解析常量类
 *  
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-5-15 下午01:04:17
 */

public class Constants {
	
	/**
	 * 实体名称分隔符
	 */
	public static final String NAME_SPLIT_SYMBOL = ".";
	
	/**
	 * s后缀
	 */
	public static final String S_SUFFIX = "s";
	
	/**
	 * 路径分隔符
	 */
	public static final String PATH_SEPARATOR = "/";
	
	/**
	 * 压缩包后缀名
	 */
	public static final String CONFIG_FILE_SUFFIX = ".jwp";
	
	/**
	 * 配置文件名称
	 */
	public static final String CONFIG_FILE_NAME = "config" + CONFIG_FILE_SUFFIX;
	
	/**
	 * config配置路径
	 */
	public static final String CONFIG_PATH = "config/";
	
	/**
	 * 模块路径
	 */
	public static final String MODULE_PATH = "module/";
	
	/**
	 * 存放GS包文件夹路径
	 */
	public static final String GS_PACKAGE_PATH = "gs-package/";
	
	/**
	 * 存放GS包jsp路径
	 */
	public static final String GS_PACKAGE_JSP_PATH = "jsp/";
	
	/**
	 * 存放GS包移动端路径
	 */
	public static final String  GS_PACKAGE_APP_PATH = "app/";
	
	
	/**
	 * 存放GS包vue路径
	 */
	public static final String  GS_PACKAGE_VUE_PATH = "pages/";
	
	/**
	 * GS名称
	 */
	public static final String GS_PATH = "gs/";
	
	/**
	 * 存放模块jsp页面路径
	 */
	public static final String GS_MODULES_JSP_PATH = GS_PACKAGE_JSP_PATH + GS_PATH;
	
	/**
	 * 存放模块vue页面路径
	 */
	
	public static final String  GS_MODULES_VUE_PATH = GS_PACKAGE_VUE_PATH +GS_PATH;
	
	/**
	 * 存放业务字典页面路径
	 */
	public static final String GS_DICTS_JSP_PATH = GS_PACKAGE_JSP_PATH + "sys/dicts/";
	/**
	 * 存放HBM XML文件夹路径
	 */
	public static final String HBM_PATH = "hbm/";
	
	/**
	 * 存放实体缓存XML文件夹路径
	 */
	public static final String ENTITY_PATH = "entity/";
	/**
	 * 存放上传文件夹路径
	 */
	public static final String UPLOADFILE_PATH= "gs-attach/";
	/**
	 * 存放附件文件夹路径
	 */
	public static final String UPLOADFILE_ATTACH= "attach/";
	/**
	 * 存放图片文件夹路径
	 */
	public static final String UPLOADFILE_IMAGE= "image/";
	/**
	 * 存放临时文件夹路径
	 */
	public static final String UPLOADFILE_TEMP= "temp/";
	/**
	 * 存放GS模块JSP文件路径
	 */
	public static final String GS_JSP_PATH = "/WEB-INF/content/";
	
	/**
	 * 模板文件夹路径
	 */
	public static final String TEMPLATE_PATH = "template/gsc/";
	
	/**
	 * HBM模板路径
	 */
	public static final String HBM_FTL_PATH = "hbm.xml.ftl";
	
	/**
	 * 列表页面模板路径
	 */
	public static final String JSP_LIST_FTL_PATH = "object-list.ftl";
	
	/**
	 * 列表页面JS模板路径
	 */
	public static final String JSP_LIST_JS_FTL_PATH = "object-list-js.ftl";
	
	/**
	 * 业务字典列表页面模板路径
	 */
	public static final String JSP_DICT_LIST_FTL_PATH = "dict-list.ftl";
	
	/**
	 * 移动端业务字典列表页面模板路径
	 */
	public static final String VUE_DICT_LIST_FTL_PATH = "app-dict-list.ftl";

	/**
	 * 移动端业务字典查看页面模板路径
	 */
	public static final String VUE_DICT_VIEW_FTL_PATH = "app-dict-view.ftl";

	/**
	 * 移动端业务字典编辑页面模板路径
	 */
	public static final String VUE_DICT_EDIT_FTL_PATH = "app-dict-edit.ftl";

	/**
	 * 移动端业务字典筛选页面模板路径
	 */
	public static final String VUE_DICT_FILTER_FTL_PATH = "app-dict-filter.ftl";

	/**
	 * 业务字典集合页面模板路径
	 */
	public static final String JSP_DICTS_JSP_FTL_PATH = "dicts-jsp.ftl";
	
	/**
	 * 自定义表单页面路径
	 */
	public static final String JSP_FORM_JSP_FTL_PATH = "form-jsp.ftl";
	
	/**
	 * 新增页面模板路径
	 */
	public static final String JSP_ADD_FTL_PATH = "object-add.ftl";
	
	/**
	 * 修改页面模板路径
	 */
	public static final String JSP_EDIT_FTL_PATH = "object-edit.ftl";
	
	/**
	 * 查看页面模板路径
	 */
	public static final String JSP_VIEW_FTL_PATH = "object-view.ftl";
	/**
	 * 子表明细页面模板路径
	 */
	public static final String JSP_SUB_FTL_PATH = "object-sub.ftl";
	
	/**
	 * app列表页面模板路径
	 */
	public static final String VUE_APP_LIST_FTL_PATH = "app-list.ftl";
	
	/**
	 * 查看页面JS模板路径
	 */
	public static final String JSP_VIEW_JS_FTL_PATH = "object-view-js.ftl";
	
	
	/**
	 * app查看页面模板路径
	 */
	public static final String VUE_APP_VIEW_FTL_PATH = "app-view.ftl";
	
	
	/**
	 * app修改页面模板路径
	 */
	public static final String VUE_APP_EDIT_FTL_PATH = "app-edit.ftl";
	
	/**
	 *app筛选页面模板路径 
	 */
	public static final String  VUE_APP_FILTER_PATH = "app-filter.ftl";
	
	/**
	 * 表单查询sql key前缀
	 */
	public static final String QUERY_SUFFIX = "query";
	
	/**
	 * 规则前缀
	 */
	public static final String RULE_SUFFIX = "rule";
	
	/**
	 * 模块菜单文件名称
	 */
	public static final String MENU_CONFIG_FILE_NAME = "menu.xml";
	
	/**
	 * xml配置文件名后缀
	 */
	public static final String XML_CONFIG_FILE_SUFFIX = ".xml";
	
	/**
	 * 业务模块
	 */
	public static final String MENU_MODULESS = "moduless";
	
	/**
	 * PC业务模块
	 */
	public static final String MENU_MODULE = "module";
	
	/**
	 * APP业务模块
	 */
	public static final String MENU_APP = "app";
	
	/**
	 * 业务模块集合类型
	 */
	public static final String MENU_MODULES_TYPE_MODULES = "modules";
	
	/**
	 * 业务字典集合类型
	 */
	public static final String MENU_MODULES_TYPE_DICTS = "dicts";
	
	/**
	 * 自定义表单集合类型
	 */
	public static final String MENU_MODULES_TYPE_FORMS = "forms";
	/**
	 * 系统模块
	 */
	public static final String MENU_SYSTEM = "system";
	
	/**
	 * 系统文件路径
	 */
	public static final String SYSTEM_PATH = "system/";
	
	/**
	 * 工作流文件路径
	 */
	public static final String WORKFLOW_PATH = SYSTEM_PATH + "workflow/";
	
	/**
	 * 工作流文件扩展名
	 */
	public static final String FLOW_FILE_SUFFIX = "bpmn20.xml";
	
	/**
	 * 等于号 =
	 */
	public static final String EQUAL_SYMBOL = "=";
	
	/**
	 * i18n资源字符串后缀
	 */
	public static final String TITLE = NAME_SPLIT_SYMBOL + "title";
	
	/**
	 * 回车换行符
	 */
	public static final String ENTER_SYMBOL = "\r\n";
	
	/**
	 * 国际化语言
	 */
	public static final String[] LOCALE = {"zh_CN","zh_TW","en"};
	
	/**
	 * 表单
	 */
	public static final String FORM = "form";
	
	/**
	 * 字段
	 */
	public static final String FIELD = "field";
	
	/**
	 * 标签页
	 */
	public static final String TAB = "tab";
	
	/**
	 * 查询界面按钮
	 */
	public static final String QUERY_BUTTON = "extension.queryPage.button";
	
	/**
	 * 编辑界面按钮
	 */
	public static final String EDIT_BUTTON = "extension.editPage.button";
	
	/**
	 * 国际化资源目录
	 */
	public static final String I18N_PATH = "i18n/";
	
	/**
	 * 下划线
	 */
	public static final String UNDERLINE = "_";
	
	/**
	 * 资源文件后缀名
	 */
	public static final String I18N_SUFFIX = ".properties";
	
	/**
	 * app国际化资源js文件类容前缀
	 */
	public static final String JSON_STRING_PREFIX = "export default ";
	
	/**
	 * app资源文件后缀名
	 */
	public static final String I18N_APP_SUFFIX = ".js";
	
	/**
	 * 应用资源文件路径
	 */
	public static final String GS_I18N_PATH = "/WEB-INF/classes/" + I18N_PATH;
	
	/**
	 * 模块资源路径前缀
	 */
	public static final String MODULE_I18N_PREFIX = "i18n.module.";
	
	/**
	 * 系统资源路径
	 */
	public static final String SYSTEM_I18N = "i18n.system";
	
	/**
	 * JSP文件后缀名
	 */
	public static final String JSP_SUFFIX = ".jsp";
	
	/**
	 * 默认配置文件名
	 */
	public static final String DEFAULT_CONFIG_FILE_NAME = "default";
	
	/**
	 * 默认配置文件夹
	 */
	public static final String DEFAULT_CONFIG_FILE = DEFAULT_CONFIG_FILE_NAME + "/";
	
	/**
	 * 版本文件名
	 */
	public static final String VERSION_FILE_NAME = "version";
	
	/**
	 * 版本文件路径
	 */
	public static final String VERSION_FILE_PATH = Constants.CONFIG_PATH + Constants.VERSION_FILE_NAME;
	
	/**
	 * 默认版本文件路径
	 */
	public static final String DEFAULT_VERSION_FILE_PATH = Constants.DEFAULT_CONFIG_FILE + Constants.CONFIG_PATH + Constants.VERSION_FILE_NAME;
	
	/**
	 * 列表查询sql语句
	 */
	public static final String QUERY_LIST_SQL_KEY = "queryListSql";
	
	/**
	 * 公用表查询sql语句
	 */
	public static final String COMMON_QUERY_SQL_KEY = "commonQuerySql";
	
	/**
	 * Excel模板文件夹路径
	 */
	public static final String EXCEL_TEMPLATE_PATH = "template/excel/";
	
	/**
	 * Excel导入映射文件模板
	 */
	public static final String EXCEL_MAPPING_FTL_NAME = "importMapping.xml.ftl";
	
	/**
	 * 存放Excel导入导出相关文件路径
	 */
	public static final String EXCEL_PATH = "excel/";
	
	/**
	 * Excel导入映射文件后缀名
	 */
	public static final String EXCEL_MAPPING_FILE_SUFFIX = ".mapping.xml";
	
	/**
	 * Excel导入模板文件名称
	 */
	public static final String EXCEL_TEMPLATE_FILE_NAME = "importTemplate.xls";
	
	/**
	 * Excel导入模板文件后缀名
	 */
	public static final String EXCEL_TEMPLATE_FILE_SUFFIX = ".importTemplate.xls";
	
	/**
	 * Excel导出模板文件名称
	 */
	public static final String EXCEL_EXPORT_TEMPLATE_FILE_NAME = "exportTemplate.xls";
	
	/**
	 * Excel导出模板文件后缀名
	 */
	public static final String EXCEL_EXPORT_TEMPLATE_FILE_SUFFIX = ".exportTemplate.xls";
	
	/**
	 * Excel导入异常模板文件名称
	 */
	public static final String EXCEL_EXP_TEMPLATE_FILE_NAME = "importExpTemplate.xls";
	
	/**
	 * Excel导入异常模板文件后缀名
	 */
	public static final String EXCEL_EXP_TEMPLATE_FILE_SUFFIX = ".importExpTemplate.xls";
	
	/**
	 * 导入时使用，系统编码，由用户填写，确定主子表数据关联关系
	 */
	public static final String SYSTEM_CODE = "systemCode";
	
	/**
	 * 导入时使用，关联系统编码，由用户填写，确定主子表数据关联关系
	 */
	public static final String REF_SYSTEM_CODE = "refSystemCode";
	
	/**
	 * 固定值类型
	 */
	public static final String FIX_ITEM = "fixItem";
	
	/**
	 *weex的路由配置文件
	 */
	public static final String WEEX_ROUTER_FILE = "router.js";
	
	/**
	 * weex的路由文件的模板
	 */
	public static final String WEEX_ROUTER_FTL_PATH = "router.ftl";

	/**
	 *移动端weexplus.jsom文件
	 */
	public static final String WEEX_WEEXPLUS_FILE = "weexplus.json";

	/**
	 * 移动端weexplus文件的模板
	 */
	public static final String  WEEX_WEEXPLUS_FTL_PATH = "weexplus.ftl";

	/**
	 *移动端weexplus.json路径
	 */
	public static final String WEEX_WEEXPLUS_PATH = "configs/";

	/*
	* weex工程的业务代码目录
	* */
	public  static  final  String  WEEX_BOOKS_PATH = "src/native/books";



	/**
	 * 系统分类树
	 */
	public static final String SYS_TREE = "SysTree";
	
	public static final String CREATE_USER = "createUser";
	public static final String LAST_UPDATE_USER = "lastUpdateUser";
	public static final String STATUS = "status";
	public static final String NICK_NAME = "NICKNAME";//姓名
	public static final String REAL_NAME = "REAL_NAME";//登录账户
	public static final String ID = "ID";
	
	/**
	 * 系统状态列表
	 */
	public static final Map<String, String> SYSTEM_STATUS_MAP = Maps.newHashMap();
	
	static {
		SYSTEM_STATUS_MAP.put("00","初始化");
		SYSTEM_STATUS_MAP.put("10","草稿");
		SYSTEM_STATUS_MAP.put("20","审批中");
		SYSTEM_STATUS_MAP.put("30","审批结束");
		SYSTEM_STATUS_MAP.put("31","审批通过");
		SYSTEM_STATUS_MAP.put("32","审批不通过");
		SYSTEM_STATUS_MAP.put("35","审核通过");
		SYSTEM_STATUS_MAP.put("40","作废");
	}
	
}
