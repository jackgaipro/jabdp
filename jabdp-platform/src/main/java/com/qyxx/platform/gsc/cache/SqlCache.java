/*
 * @(#)SqlCache.java
 * 2011-9-28 下午11:35:59
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.gsc.cache;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.commons.lang.StringUtils;

import com.qyxx.jwp.bean.Item;
import com.qyxx.jwp.bean.ModuleProperties;
import com.qyxx.jwp.bean.Sql;
import com.qyxx.jwp.datasource.QuerySql;
import com.qyxx.platform.gsc.utils.Constants;


/**
 *  SQL语句缓存
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-9-28 下午11:35:59
 */

public final class SqlCache {
	
	private static SqlCache tec;
	
	private ConcurrentMap<String, QuerySql> teMap;//sql语句缓存
	
	private ConcurrentMap<String, Map<String, String>> referTableFieldMap;//关联其他表字段缓存
	
	private SqlCache() {
		teMap = new ConcurrentHashMap<String, QuerySql>();
		referTableFieldMap = new ConcurrentHashMap<String, Map<String, String>>();
	}
	
	public static synchronized SqlCache getInstance() {
		if(null==tec) {
			tec = new SqlCache();
		}
		return tec;
	}
	
	/**
	 * 加入sql缓存
	 * 
	 * @param entityName
	 * @param te
	 * @param moduleType
	 */
	public void put(String entityName, QuerySql te, String moduleType) {
		this.teMap.put(entityName, te);
		if(!ModuleProperties.TYPE_FORM.equals(moduleType)) {//自定义表单需要排除掉
			this.updateReferTableField(te);
		}
	}
	
	/**
	 * 更新关联表字段缓存，用于删除记录时，检查其他表是否有引用本实体数据，如果存在，则不能进行删除
	 * 
	 * @param qs
	 */
	public void updateReferTableField(QuerySql qs) {
		String key = qs.getKey();
		String dataType = qs.getDataType();
		Sql sql = qs.getSql();
		if(!key.startsWith(Constants.RULE_SUFFIX) //字段查询sql
				&& !key.startsWith(Constants.QUERY_SUFFIX)
				&& StringUtils.countMatches(key, Constants.NAME_SPLIT_SYMBOL) == 3 //出现三个小数点
				&& null!=sql) {
			String entityName = sql.getEntityName();
			if(StringUtils.isNotBlank(entityName)) {
				//来源实体名非空
				//如：chanpinxuanxing.ChanPinXuanXingZhuBiao.xinghaobianma.sql12，将最后一个.后的字符串去除，保留实体名+字段
				String refKey = StringUtils.substring(key, 0, StringUtils.lastIndexOf(key, Constants.NAME_SPLIT_SYMBOL));
				String idKey = null;
				List<Item> svList = sql.getSetValueItemList();
				if(null!=svList) {
					for(Item item : svList) {
						if("key".equals(item.getKey()) 
								|| "id".equals(item.getKey())) {
							idKey = StringUtils.lowerCase(item.getColumn());
							break;
						}
					}
				}
				if(null!=idKey) { //放入chanpinxuanxing.ChanPinXuanXingZhuBiao.xinghaobianma和对应字段
					idKey = idKey + Constants.NAME_SPLIT_SYMBOL + dataType;
					Map<String, String> map = this.referTableFieldMap.get(entityName);
					if(null!=map) {
						map.put(refKey, idKey);
					} else {
						map = new HashMap<String, String>();
						map.put(refKey, idKey);
						this.referTableFieldMap.put(entityName, map);
					}
				}
			}
		}
	}
	
	/**
	 * 根据实体名查询关联实体字段
	 * 
	 * @param entityName
	 * @return
	 */
	public Map<String, String> getReferTableField(String entityName) {
		return this.referTableFieldMap.get(entityName);
	}
	
	public QuerySql get(String entityName) {
		return this.teMap.get(entityName);
	}
	
	public Sql getSql(String entityName) {
		QuerySql qs = this.get(entityName);
		Sql sql = null;
		if(null!=qs) {
			sql = qs.getSql();
		}
		return sql;
	}
	
	public String getSqlContent(String entityName) {
		Sql sql = this.getSql(entityName);
		String sqlStr = null;
		if(null!=sql) {
			sqlStr = sql.getContent();
		}
		return sqlStr;
	}

	public void destroy() {
		this.teMap.clear();
		this.referTableFieldMap.clear();
	}

}
