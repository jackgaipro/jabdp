/*
 * @(#)Table.java
 * 2012-11-15 下午08:46:39
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.bean;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;


/**
 *  sql向导信息
 *  @author xk
 *  @version 1.0 2013-07-19 下午15:07:39
 *  @since jdk1.6 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SelectInfo {
	
	@XmlElementWrapper(name = "table")
	@XmlElement(name="item")
	private List<Item> tableItemList = new ArrayList<Item>();
	
	@XmlElementWrapper(name = "where")
	@XmlElement(name="item")
	private List<Item> whereItemList = new ArrayList<Item>();

	
	public List<Item> getTableItemList() {
		return tableItemList;
	}

	
	public void setTableItemList(List<Item> tableItemList) {
		this.tableItemList = tableItemList;
	}

	
	public List<Item> getWhereItemList() {
		return whereItemList;
	}

	
	public void setWhereItemList(List<Item> whereItemList) {
		this.whereItemList = whereItemList;
	}
	
	
}
