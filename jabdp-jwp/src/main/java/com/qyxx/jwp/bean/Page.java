/*
 * @(#)Page.java
 * 2014-3-17 上午09:04:12
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;


/**
 *  页面设置属性
 *  @author gxj
 *  @version 1.0 2014-3-17 上午09:04:12
 *  @since jdk1.6 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Page {
	
	@XmlAttribute
	private String paddingLeft; //左边距
	
	@XmlAttribute
	private String paddingRight; //右边距
	
	@XmlAttribute
	private String paddingTop; //上边距
	
	@XmlAttribute
	private String paddingBottom; //下边距
	
	@XmlElement
	private String html; //页面内容

	
	/**
	 * @return paddingLeft
	 */
	public String getPaddingLeft() {
		return paddingLeft;
	}

	
	/**
	 * @param paddingLeft
	 */
	public void setPaddingLeft(String paddingLeft) {
		this.paddingLeft = paddingLeft;
	}

	
	/**
	 * @return paddingRight
	 */
	public String getPaddingRight() {
		return paddingRight;
	}

	
	/**
	 * @param paddingRight
	 */
	public void setPaddingRight(String paddingRight) {
		this.paddingRight = paddingRight;
	}

	
	/**
	 * @return paddingTop
	 */
	public String getPaddingTop() {
		return paddingTop;
	}

	
	/**
	 * @param paddingTop
	 */
	public void setPaddingTop(String paddingTop) {
		this.paddingTop = paddingTop;
	}

	
	/**
	 * @return paddingBottom
	 */
	public String getPaddingBottom() {
		return paddingBottom;
	}

	
	/**
	 * @param paddingBottom
	 */
	public void setPaddingBottom(String paddingBottom) {
		this.paddingBottom = paddingBottom;
	}

	
	/**
	 * @return html
	 */
	public String getHtml() {
		return html;
	}

	
	/**
	 * @param html
	 */
	public void setHtml(String html) {
		this.html = html;
	}
	
	

}
