/*
 * @(#)DynamicProp.java
 * 2013-6-7 上午09:51:11
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.bean;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;


/**
 *  动态映射属性
 *  @author gxj
 *  @version 1.0 2013-6-7 上午09:51:11
 *  @since jdk1.6 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DynamicProp {
	
	/**
	 * 动态显示子表属性
	 */
	@XmlAttribute
	private Boolean dynamicTable = false;
	
	/**
	 * 动态显示子表属性Item对象的key表示引用的sqlkey
	 */
	@XmlElement
	private Item dynamicTableItem;
	
	/**
	 * 动态显示子表字段属性
	 */
	@XmlAttribute
	private Boolean dynamicColumn = false;
	
	/**
	 * 动态显示子表字段属性Item对象的key表示引用的sqlkey，value表示子表属性key
	 */
	@XmlElementWrapper(name = "dynamicColumnItems")
	@XmlElement(name="dynamicColumnItem")
	private List<Item> dynamicColumnItems = new ArrayList<Item>();

	
	/**
	 * @return dynamicTable
	 */
	public Boolean getDynamicTable() {
		return dynamicTable;
	}

	
	/**
	 * @param dynamicTable
	 */
	public void setDynamicTable(Boolean dynamicTable) {
		this.dynamicTable = dynamicTable;
	}

	
	/**
	 * @return dynamicTableItem
	 */
	public Item getDynamicTableItem() {
		return dynamicTableItem;
	}

	
	/**
	 * @param dynamicTableItem
	 */
	public void setDynamicTableItem(Item dynamicTableItem) {
		this.dynamicTableItem = dynamicTableItem;
	}

	
	/**
	 * @return dynamicColumn
	 */
	public Boolean getDynamicColumn() {
		return dynamicColumn;
	}

	
	/**
	 * @param dynamicColumn
	 */
	public void setDynamicColumn(Boolean dynamicColumn) {
		this.dynamicColumn = dynamicColumn;
	}

	
	/**
	 * @return dynamicColumnItems
	 */
	public List<Item> getDynamicColumnItems() {
		return dynamicColumnItems;
	}

	
	/**
	 * @param dynamicColumnItems
	 */
	public void setDynamicColumnItems(List<Item> dynamicColumnItems) {
		this.dynamicColumnItems = dynamicColumnItems;
	}

}
