/*
 * @(#)DataSource.java
 * 2012-5-8 下午04:35:50
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.bean;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

/**
 *  模块数据源
 *  
 *  @author gxj
 *  @version 1.0 2012-5-8 下午04:35:50
 *  @since jdk1.6
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DataSource {

	@XmlElementWrapper(name = "forms")
	@XmlElement(name="form")
	private List<Form> formList = new ArrayList<Form>();
	
	@XmlElementWrapper(name = "tabLayout")
	@XmlElement(name="tabs")
	private List<Tabs> tabsList = new ArrayList<Tabs>();

	/**
	 * @return formList
	 */
	public List<Form> getFormList() {
		return formList;
	}

	/**
	 * @param formList
	 */
	public void setFormList(List<Form> formList) {
		this.formList = formList;
	}

	/**
	 * @return the tabsList
	 */
	public List<Tabs> getTabsList() {
		return tabsList;
	}

	/**
	 * @param tabsList the tabsList to set
	 */
	public void setTabsList(List<Tabs> tabsList) {
		this.tabsList = tabsList;
	}

}
