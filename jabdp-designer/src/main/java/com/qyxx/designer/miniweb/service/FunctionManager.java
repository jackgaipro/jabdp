package com.qyxx.designer.miniweb.service;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.qyxx.designer.miniweb.entity.functions.Function;
import com.qyxx.designer.modules.mapper.JaxbMapper;
import com.qyxx.designer.modules.utils.EncryptAndDecryptUtils;




@Component
public class FunctionManager {
private static Logger logger = LoggerFactory.getLogger(FunctionManager.class);
	
	private JaxbMapper jaxb = new JaxbMapper(Function.class);
	
	public final static String ENCODING = "UTF-8";
	
	//public final static String MODULE_PATH = "E:/space/eclipseSpace/idesigner/src/main/resources/";
	/**获得平台函数实现类
	 * @param path 保存平台函数文件的路径
	 * @return function*/
	public Function getFunction(String path) {
		try {
			File file = new File(path+ "functions.xml");
			String xml = FileUtils.readFileToString(file, ENCODING);
			xml = EncryptAndDecryptUtils.aesDecrypt(xml, "");
			Function function = jaxb.fromXml(xml);
			return function;
		} catch (IOException e) {
			throw new ServiceException("读文件时出现异常", e);
		} catch (Exception e) {
			throw new ServiceException("", e);
		}
	}
}
