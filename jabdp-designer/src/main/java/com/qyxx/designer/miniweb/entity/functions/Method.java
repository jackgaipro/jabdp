package com.qyxx.designer.miniweb.entity.functions;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 *  树形二级子节点
 *  
 *  @author thy
 *  @version 1.0 
 *  @since jdk1.6
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Method {
	@XmlAttribute
	private String key;
	
	@XmlAttribute
	private String caption;
	
	
	@XmlElement
	private String description;
	
	@JsonProperty("children")
	@XmlElement
	private List<Param> param = new ArrayList<Param>();
	
	@XmlElement(name="return")
	private Returns returns = new Returns();

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Param> getParam() {
		return param;
	}

	public void setParam(List<Param> param) {
		this.param = param;
	}

	public Returns getReturns() {
		return returns;
	}

	public void setReturns(Returns returns) {
		this.returns = returns;
	}
	
	
}
