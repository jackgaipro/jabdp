<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@ include file="/common/meta.jsp"%>
<script type="text/javascript" src="${ctx}/js/ruler/jquery.zxxPageRuler.1.1.js"></script>
<link rel="stylesheet" href="${ctx}/js/ruler/css/ruler.css" type="text/css" />
<style type="text/css">
.demo_box{left:0; top:0;position:relative; z-index:1001;  }
</style>
<script type="text/javascript">
		$(document).ready(function() {
			$.pageRuler({
			});	
			initDiv();
		});	
		/**
		 * 根据字段List初始化div块
		 */
	function initDiv() {		
		var editors = "${param.editors}";//生成的tab或者datagrid的id
		var tRows = "${param.tRows}";
		var tCols = "${param.tCols}";
		var tabObj = parent.parent.$.fn.module.getTabList(tRows,tCols);
		//console.debug(tabObj);
		var tTableCols = tabObj.tableCols;
		var form = parent.parent.$.fn.module.getFormByKey(tabObj.form);
		var rows = form.fieldList.length;//生成的表格的行数
		var data = form.fieldList;
		var tabId = form.index;
		if (form.isMaster || form.listType == "Form"){//主表显示或Form表单显示
			var mark = 0;//换行标记变量
			for (var nn = 0, m = 0; m < data.length; data[m].isCaption ? nn : nn++, m++) {			
			if((!form.isMaster || (data[m].tabRows==tRows && data[m].tabCols==tCols)) && data[m].editProperties.visible && !data[m].isCaption){
				var div ="";
				var t = data[m].editProperties.top;
				var l = data[m].editProperties.left;
				var remainder = null;
				if(nn!=0){
					remainder = nn%tTableCols;
				}
				if(remainder===0){
					if(!l || l==""){		
						data[m].editProperties.left=25;
					}
					if(!t || t==""){
						data[m].editProperties.top=50+data[mark].editProperties.top;
					}
					//parent.parent.$.fn.module.setFieldProp(tabId, data[m].index,data[m]);
					div = parent.parent.$.fn.module.createDiv(data[m],tabId);
				}else{
					if(!t || t==""){					
						if(nn==0){
							data[m].editProperties.top=25;
						}else{
							data[m].editProperties.top=data[mark].editProperties.top;
						}
					}
					if(!l || l==""){
						if(nn==0){
							data[m].editProperties.left=25;
						}else{
							data[m].editProperties.left=105+parseInt(data[mark].editProperties.width)+parseInt(data[mark].editProperties.left);
						}
					}
					//parent.parent.$.fn.module.setFieldProp(tabId, data[m].index,data[m]);
				 	div = parent.parent.$.fn.module.createDiv(data[m],tabId);//创建DIV块
				}
				mark = m;
				$("#xk").append(div);
				$('#' + data[m].index).draggable({
					edge : 6,
					onBeforeDrag : function(e) {

					},
					onStartDrag : function(e) {

					},
					onDrag : function(e) {//添加游标尺样式
						var t = parseInt($(this).css("top"));
						var l = parseInt($(this).css("left"));				
						var h = parseInt($(this).css("height"));
						$.createVLine({
							"v" : [ l + 9 ]
						});
						$.createHLine({
							"h" : [ t + 9]
						});
					},
					onStopDrag : function(e) {	//去除游标尺样式					
						var l = parseInt($("#vtltie1").text());
						var t = parseInt($("#vtltie2").text());
						if(t && l){
							$.hideVHLine();
							//var t = parseInt($(this).css("top"));
							//var l = parseInt($(this).css("left"));						
							if(t<=0){
								t=0;
								$(this).css("top", 0);
							}
							if(l<=0){
								l=0;
								$(this).css("left", 0);
							}
							var fieldIndex = parseInt($(this).attr("id"));
							var tableIndex = parseInt($(this).attr("tabIndex"));
							var ep = parent.parent.$.fn.module.getFieldProp(tableIndex,fieldIndex).editProperties;
							ep.top = t;
							ep.left = l;	
						}
					}

				});
				$('#' + data[m].index).resizable({
					edge:3,
					onStopResize:function(e){//设置div块绝对定位的高和宽
						var fieldIndex = parseInt($(this).attr("id"));
						var tableIndex = parseInt($(this).attr("tabIndex"));
						var w = parseInt($(this).css("width"))-80;
						var h = parseInt($(this).css("height"));
						var img = $(this).find("img");
						$(img).css("height",h);
						$(img).css("width",w);
						var ep = parent.parent.$.fn.module.getFieldProp(tableIndex,fieldIndex).editProperties;
						ep.width = w;
						ep.height = h;
					}
				});
			}
				}

			}
		}

</script>
</head>
<body  >
  <div id = "xk" class="demo_box">
  </div>
		<!-- <div region="center"  style="overflow:hidden;padding:20px 50px;">
		"content":"<div style ='height:400px;'><iframe  id='"+editors+"' src='${ctx}/jsp/absolute.jsp?tab="+tab.form+"&editors="+editors+"&tableCols="+tableCols+"' frameborder='0' style='width:100%;height:100%'></iframe></div>"
		</div>	-->	
</body>
</html>