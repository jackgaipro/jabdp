(function($){
	$.fn.getzTreeJson = function(b){
	var d ={
				"TextBox":{"id":1,"name":"TextBox",iconSkin:"icon01",children:[
	                                  {"id":10,"name":"onStyler","params":"value,rowData,rowIndex","description":"此事件仅在列表显示中使用，用于单元格样式显示。返回值如：return 'background:red'。value：为该单元格的值；rowData：为该行的数据，以rowData.id或者rowData['id']调用；rowIndex：列表行索引，从0开始；",iconSkin:"icon03"},                                         
							          {"id":11,"name":"onChange","params":"newValue,oldValue","description":"当内容改变时触发（该事件仅在数据类型为数字型时可用）。newValue--新值；oldValue--旧值；",iconSkin:"icon03"},
							          {"id":12,"name":"onclick","params":"event","description":"在用户用鼠标左键单击对象时触发，event为事件对象。",iconSkin:"icon03"},
							          {"id":18,"name":"ondblclick","params":"event","description":"在用户用鼠标双击对象时触发，event为事件对象。",iconSkin:"icon03"},
							          {"id":13,"name":"onkeydown","params":"event","description":"当用户按下键盘按键时触发，event为事件对象。",iconSkin:"icon03"},
							          {"id":14,"name":"onkeyup","params":"event","description":"当用户释放键盘按键时触发，event为事件对象。",iconSkin:"icon03"},
							          {"id":15,"name":"onfocus","params":"event","description":"当对象获得焦点时触发，event为事件对象。",iconSkin:"icon03"},
							          {"id":16,"name":"onblur","params":"event","description":"在对象失去输入焦点时触发，event为事件对象。",iconSkin:"icon03"},
							          {"id":20,"name":"onListEnter","params":"fieldVal, fieldKey, tableKey, rowIndex",
								        	 "description":"当用户在列表上编辑控件内容后按回车键触发（该事件仅在数据类型为字符串型时可用）。fieldVal：该单元格的值；fieldKey：该单元格的属性key；" +
								        	 		"tableKey：列表属性Key；rowIndex：列表行索引，从0开始。",iconSkin:"icon03"},
							          {"id":21,"name":"onListChange","params":"fieldVal, fieldKey, tableKey, rowIndex",
								        	 "description":"当用户在列表上编辑控件的内容改变时触发（该事件仅在数据类型为数字型时可用）。fieldVal：该单元格的值；fieldKey：该单元格的属性key；" +
								        	 		"tableKey：列表属性Key；rowIndex：列表行索引，从0开始。",iconSkin:"icon03"},
								      {"id":17,"name":"onFormatter","params":"value,row,index","description":"此事件仅在列表显示中使用，用于控制值的显示格式。返回值如：return 'test'。value：为该单元格的值；row：为该行的数据，以row.id或者row['id']调用；index：列表行索引，从0开始；",iconSkin:"icon03"},
								      {"id":33,"name":"onQuickIconClick","params":"target,tableIndex, rowIndex, fieldIndex","description":"target:触发jquery对象，用于设值等操作。此方法用于控制快速图标点击事件，用于快速选择数据等操作",iconSkin:"icon03"},
								      {"id":31,"name":"onBeforeRevise","params":"newVal,oldVal,rowIndex,tableKey,rowData","description":"此事件在修订操作结束后（即保存修订日志后）触发，用于操作关联数据的保存。newVal:修订后值;oldVal:修订前值;newCap:修订后显示值，只针对于combo类控件有效;oldCap:修订前显示值，只针对于combo类控件有效;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"},
								      {"id":30,"name":"onAfterRevise","params":"newVal,oldVal,newCap,oldCap,rowIndex,tableKey,rowData","description":"此事件在修订操作结束后（即保存修订日志后）触发，用于操作关联数据的保存。newVal:修订后值;oldVal:修订前值;newCap:修订后显示值，只针对于combo类控件有效;oldCap:修订前显示值，只针对于combo类控件有效;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"}
				]},
				"PasswordBox":{"id":1,"name":"PasswordBox",iconSkin:"icon01",children:[
				                    {"id":10,"name":"onStyler","params":"value,rowData,rowIndex","description":"此事件仅在列表显示中使用，用于单元格样式显示。返回值如：return 'background:red'。value：为该单元格的值；rowData：为该行的数据，以rowData.id或者rowData['id']调用；rowIndex：列表行索引，从0开始；",iconSkin:"icon03"},                                                   
									{"id":12,"name":"onclick","params":"event","description":"在用户用鼠标左键单击对象时触发，event为事件对象。",iconSkin:"icon03"},
									{"id":13,"name":"onkeydown","params":"event","description":"当用户按下键盘按键时触发，event为事件对象。",iconSkin:"icon03"},
									{"id":14,"name":"onkeyup","params":"event","description":"当用户释放键盘按键时触发，event为事件对象。",iconSkin:"icon03"},
									{"id":15,"name":"onfocus","params":"event","description":"当对象获得焦点时触发，event为事件对象。",iconSkin:"icon03"},
									{"id":16,"name":"onblur","params":"event","description":"在对象失去输入焦点时触发，event为事件对象。",iconSkin:"icon03"},
									{"id":31,"name":"onBeforeRevise","params":"newVal,oldVal,rowIndex,tableKey,rowData","description":"此事件在修订操作结束后（即保存修订日志后）触发，用于操作关联数据的保存。newVal:修订后值;oldVal:修订前值;newCap:修订后显示值，只针对于combo类控件有效;oldCap:修订前显示值，只针对于combo类控件有效;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"},
								    {"id":30,"name":"onAfterRevise","params":"newVal,oldVal,newCap,oldCap,rowIndex,tableKey,rowData","description":"此事件在修订操作结束后（即保存修订日志后）触发，用于操作关联数据的保存。newVal:修订后值;oldVal:修订前值;newCap:修订后显示值，只针对于combo类控件有效;oldCap:修订前显示值，只针对于combo类控件有效;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"}
				]},
				"DateBox":{"id":1,"name":"DateBox",iconSkin:"icon01",children:[
				                    {"id":10,"name":"onStyler","params":"value,rowData,rowIndex","description":"此事件仅在列表显示中使用，用于单元格样式显示。返回值如：return 'background:red'。value：为该单元格的值；rowData：为该行的数据，以rowData.id或者rowData['id']调用；rowIndex：列表行索引，从0开始；",iconSkin:"icon03"},
									{"id":12,"name":"onclick","params":"event","description":"在用户用鼠标左键单击对象时触发，event为事件对象。",iconSkin:"icon03"},
									{"id":13,"name":"onkeydown","params":"event","description":"当用户按下键盘按键时触发，event为事件对象。",iconSkin:"icon03"},
									{"id":14,"name":"onkeyup","params":"event","description":"当用户释放键盘按键时触发，event为事件对象。",iconSkin:"icon03"},
									{"id":15,"name":"onfocus","params":"event","description":"当对象获得焦点时触发，event为事件对象。",iconSkin:"icon03"},
									{"id":16,"name":"onblur","params":"event","description":"在对象失去输入焦点时触发，event为事件对象。",iconSkin:"icon03"},
									{"id":17,"name":"onpicked","params":"dp","description":"选中日期后触发。可以使用this.value获取当前值，this: 指向文本框；dp: 指向$dp；dp.cal: 指向日期控件对象，其他方法请咨询技术",iconSkin:"icon03"},
									{"id":20,"name":"onListEnter","params":"fieldVal, fieldKey, tableKey, rowIndex",
							        	 "description":"当用户在列表上编辑控件内容后按回车键触发（该事件仅在数据类型为字符串型时可用）。fieldVal：该单元格的值；fieldKey：该单元格的属性key；" +
							        	 		"tableKey：列表属性Key；rowIndex：列表行索引，从0开始。",iconSkin:"icon03"},
							        {"id":31,"name":"onBeforeRevise","params":"newVal,oldVal,rowIndex,tableKey,rowData","description":"此事件在修订操作结束后（即保存修订日志后）触发，用于操作关联数据的保存。newVal:修订后值;oldVal:修订前值;newCap:修订后显示值，只针对于combo类控件有效;oldCap:修订前显示值，只针对于combo类控件有效;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"},
								    {"id":30,"name":"onAfterRevise","params":"newVal,oldVal,newCap,oldCap,rowIndex,tableKey,rowData","description":"此事件在修订操作结束后（即保存修订日志后）触发，用于操作关联数据的保存。newVal:修订后值;oldVal:修订前值;newCap:修订后显示值，只针对于combo类控件有效;oldCap:修订前显示值，只针对于combo类控件有效;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"}
				]},
				"ComboBox":{"id":1,"name":"ComboBox",iconSkin:"icon01",children:[
				                    {"id":10,"name":"onStyler","params":"value,rowData,rowIndex","description":"此事件仅在列表显示中使用，用于单元格样式显示。返回值如：return 'background:red'。value：为该单元格的值；rowData：为该行的数据，以rowData.id或者rowData['id']调用；rowIndex：列表行索引，从0开始；",iconSkin:"icon03"},  
				                    {"id":32,"name":"onEditFormatter","params":"rowData","description":"此事件用于定义编辑显示行格式。rowData：为该行的数据，以rowData.id或者rowData['id']调用；",iconSkin:"icon03"},  
				                    {"id":11,"name":"onChange","params":"newValue,oldValue","description":"当内容改变时触发（该事件仅在数据类型为数字型时可用）。newValue--新值；oldValue--旧值；",iconSkin:"icon03"},
									{"id":12,"name":"onclick","params":"event","description":"在用户用鼠标左键单击对象时触发，event为事件对象。",iconSkin:"icon03"},
									{"id":13,"name":"onkeydown","params":"event","description":"当用户按下键盘按键时触发，event为事件对象。",iconSkin:"icon03"},
									{"id":14,"name":"onkeyup","params":"event","description":"当用户释放键盘按键时触发，event为事件对象。",iconSkin:"icon03"},
									{"id":15,"name":"onfocus","params":"event","description":"当对象获得焦点时触发，event为事件对象。",iconSkin:"icon03"},
									{"id":16,"name":"onblur","params":"event","description":"在对象失去输入焦点时触发，event为事件对象。",iconSkin:"icon03"},
									{"id":17,"name":"onSelect","params":"record","description":"当用户选中记录时触发，record为选中的记录对象",iconSkin:"icon03"},
									{"id":33,"name":"onSetQueryParam","params":"params, rowData, rowIndex","description":"此方法用于设置查询参数，一般用在级联查询，如省市区级联查询等；params参数为{term:\"\",page:1},term表示查询关键字，page表示查询第几页，用于参数参考；返回值为对象，如：return {\"filter_EQL_id\":\"1\"}，返回需要设置的查询参数对象；rowData--子表行数据；rowIndex--子表行索引；",iconSkin:"icon03"},
									{"id":21,"name":"onListChange","params":"fieldVal, fieldKey, tableKey, rowIndex",
							        	 "description":"当用户在列表上编辑控件的内容改变时触发。fieldVal：该单元格的值；fieldKey：该单元格的属性key；" +
							        	 		"tableKey：列表属性Key；rowIndex：列表行索引，从0开始。",iconSkin:"icon03"},
							        {"id":22,"name":"onListSelect","params":"record, fieldKey, tableKey, rowIndex",
										 "description":"当用户在列表上Combo类编辑控件的内容选中时触发。record：选中值对象；fieldKey：该单元格的属性key；" +
												"tableKey：列表属性Key；rowIndex：列表行索引，从0开始。",iconSkin:"icon03"},
									{"id":31,"name":"onBeforeRevise","params":"newVal,oldVal,rowIndex,tableKey,rowData","description":"此事件在修订操作结束后（即保存修订日志后）触发，用于操作关联数据的保存。newVal:修订后值;oldVal:修订前值;newCap:修订后显示值，只针对于combo类控件有效;oldCap:修订前显示值，只针对于combo类控件有效;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"},
									{"id":30,"name":"onAfterRevise","params":"newVal,oldVal,newCap,oldCap,rowIndex,tableKey,rowData","description":"此事件在修订操作结束后（即保存修订日志后）触发，用于操作关联数据的保存。newVal:修订后值;oldVal:修订前值;newCap:修订后显示值，只针对于combo类控件有效;oldCap:修订前显示值，只针对于combo类控件有效;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"}
				]},
				"ComboBoxSearch":{"id":1,"name":"ComboBoxSearch",iconSkin:"icon01",children:[
                    {"id":10,"name":"onStyler","params":"value,rowData,rowIndex","description":"此事件仅在列表显示中使用，用于单元格样式显示。返回值如：return 'background:red'。value：为该单元格的值；rowData：为该行的数据，以rowData.id或者rowData['id']调用；rowIndex：列表行索引，从0开始；",iconSkin:"icon03"},  
                    {"id":32,"name":"onEditFormatter","params":"rowData","description":"此事件用于定义显示行格式。rowData：为该行的数据，以rowData.id或者rowData['id']调用；",iconSkin:"icon03"},  
                    {"id":11,"name":"onChange","params":"newValue,oldValue","description":"当内容改变时触发（该事件仅在数据类型为数字型时可用）。newValue--新值；oldValue--旧值；",iconSkin:"icon03"},
					{"id":12,"name":"onclick","params":"event","description":"在用户用鼠标左键单击对象时触发，event为事件对象。",iconSkin:"icon03"},
					{"id":13,"name":"onkeydown","params":"event","description":"当用户按下键盘按键时触发，event为事件对象。",iconSkin:"icon03"},
					{"id":14,"name":"onkeyup","params":"event","description":"当用户释放键盘按键时触发，event为事件对象。",iconSkin:"icon03"},
					{"id":15,"name":"onfocus","params":"event","description":"当对象获得焦点时触发，event为事件对象。",iconSkin:"icon03"},
					{"id":16,"name":"onblur","params":"event","description":"在对象失去输入焦点时触发，event为事件对象。",iconSkin:"icon03"},
					{"id":17,"name":"onSelect","params":"record","description":"当用户选中记录时触发，record为选中的记录对象",iconSkin:"icon03"},
					{"id":33,"name":"onSetQueryParam","params":"params,rowData,rowIndex","description":"此方法用于设置查询参数，一般用在级联查询，如省市区级联查询等；params参数为{term:\"\",page:1},term表示查询关键字，page表示查询第几页，用于参数参考；返回值为对象，如：return {\"filter_EQL_id\":\"1\"}，返回需要设置的查询参数对象；；rowData--子表行数据；rowIndex--子表行索引；",iconSkin:"icon03"},
					{"id":21,"name":"onListChange","params":"fieldVal, fieldKey, tableKey, rowIndex",
			        	 "description":"当用户在列表上编辑控件的内容改变时触发。fieldVal：该单元格的值；fieldKey：该单元格的属性key；" +
			        	 		"tableKey：列表属性Key；rowIndex：列表行索引，从0开始。",iconSkin:"icon03"},
			        {"id":22,"name":"onListSelect","params":"record, fieldKey, tableKey, rowIndex",
						 "description":"当用户在列表上Combo类编辑控件的内容选中时触发。record：选中值对象；fieldKey：该单元格的属性key；" +
								"tableKey：列表属性Key；rowIndex：列表行索引，从0开始。",iconSkin:"icon03"},
					{"id":31,"name":"onBeforeRevise","params":"newVal,oldVal,rowIndex,tableKey,rowData","description":"此事件在修订操作结束后（即保存修订日志后）触发，用于操作关联数据的保存。newVal:修订后值;oldVal:修订前值;newCap:修订后显示值，只针对于combo类控件有效;oldCap:修订前显示值，只针对于combo类控件有效;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"},
					{"id":30,"name":"onAfterRevise","params":"newVal,oldVal,newCap,oldCap,rowIndex,tableKey,rowData","description":"此事件在修订操作结束后（即保存修订日志后）触发，用于操作关联数据的保存。newVal:修订后值;oldVal:修订前值;newCap:修订后显示值，只针对于combo类控件有效;oldCap:修订前显示值，只针对于combo类控件有效;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"}	 		
	            ]},
	            "ComboCheckBox":{"id":1,"name":"ComboCheckBox",iconSkin:"icon01",children:[
	                                         				                    {"id":10,"name":"onStyler","params":"value,rowData,rowIndex","description":"此事件仅在列表显示中使用，用于单元格样式显示。返回值如：return 'background:red'。value：为该单元格的值；rowData：为该行的数据，以rowData.id或者rowData['id']调用；rowIndex：列表行索引，从0开始；",iconSkin:"icon03"},  
	                                         									{"id":11,"name":"onChange","params":"newValue,oldValue","description":"当内容改变时触发（该事件仅在数据类型为数字型时可用）。newValue--新值；oldValue--旧值；",iconSkin:"icon03"},
	                                         									{"id":12,"name":"onclick","params":"event","description":"在用户用鼠标左键单击对象时触发，event为事件对象。",iconSkin:"icon03"},
	                                         									{"id":13,"name":"onkeydown","params":"event","description":"当用户按下键盘按键时触发，event为事件对象。",iconSkin:"icon03"},
	                                         									{"id":14,"name":"onkeyup","params":"event","description":"当用户释放键盘按键时触发，event为事件对象。",iconSkin:"icon03"},
	                                         									{"id":15,"name":"onfocus","params":"event","description":"当对象获得焦点时触发，event为事件对象。",iconSkin:"icon03"},
	                                         									{"id":16,"name":"onblur","params":"event","description":"在对象失去输入焦点时触发，event为事件对象。",iconSkin:"icon03"},
	                                         									{"id":17,"name":"onSelect","params":"record","description":"当用户选中记录时触发，record为选中的记录对象",iconSkin:"icon03"},
	                                         									{"id":21,"name":"onListChange","params":"fieldVal, fieldKey, tableKey, rowIndex",
	                                         							        	 "description":"当用户在列表上编辑控件的内容改变时触发。fieldVal：该单元格的值；fieldKey：该单元格的属性key；" +
	                                         							        	 		"tableKey：列表属性Key；rowIndex：列表行索引，从0开始。",iconSkin:"icon03"},
	                                         							        {"id":22,"name":"onListSelect","params":"record, fieldKey, tableKey, rowIndex",
	                                         										 "description":"当用户在列表上Combo类编辑控件的内容选中时触发。record：选中值对象；fieldKey：该单元格的属性key；" +
	                                         												"tableKey：列表属性Key；rowIndex：列表行索引，从0开始。",iconSkin:"icon03"},
	                                         									{"id":31,"name":"onBeforeRevise","params":"newVal,oldVal,rowIndex,tableKey,rowData","description":"此事件在修订操作结束后（即保存修订日志后）触发，用于操作关联数据的保存。newVal:修订后值;oldVal:修订前值;newCap:修订后显示值，只针对于combo类控件有效;oldCap:修订前显示值，只针对于combo类控件有效;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"},
	                                         									{"id":30,"name":"onAfterRevise","params":"newVal,oldVal,newCap,oldCap,rowIndex,tableKey,rowData","description":"此事件在修订操作结束后（即保存修订日志后）触发，用于操作关联数据的保存。newVal:修订后值;oldVal:修订前值;newCap:修订后显示值，只针对于combo类控件有效;oldCap:修订前显示值，只针对于combo类控件有效;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"}
	             ]},
	             "ComboRadioBox":{"id":1,"name":"ComboRadioBox",iconSkin:"icon01",children:[
	                                          				                    {"id":10,"name":"onStyler","params":"value,rowData,rowIndex","description":"此事件仅在列表显示中使用，用于单元格样式显示。返回值如：return 'background:red'。value：为该单元格的值；rowData：为该行的数据，以rowData.id或者rowData['id']调用；rowIndex：列表行索引，从0开始；",iconSkin:"icon03"},  
	                                          									{"id":11,"name":"onChange","params":"newValue,oldValue","description":"当内容改变时触发（该事件仅在数据类型为数字型时可用）。newValue--新值；oldValue--旧值；",iconSkin:"icon03"},
	                                          									{"id":12,"name":"onclick","params":"event","description":"在用户用鼠标左键单击对象时触发，event为事件对象。",iconSkin:"icon03"},
	                                          									{"id":13,"name":"onkeydown","params":"event","description":"当用户按下键盘按键时触发，event为事件对象。",iconSkin:"icon03"},
	                                          									{"id":14,"name":"onkeyup","params":"event","description":"当用户释放键盘按键时触发，event为事件对象。",iconSkin:"icon03"},
	                                          									{"id":15,"name":"onfocus","params":"event","description":"当对象获得焦点时触发，event为事件对象。",iconSkin:"icon03"},
	                                          									{"id":16,"name":"onblur","params":"event","description":"在对象失去输入焦点时触发，event为事件对象。",iconSkin:"icon03"},
	                                          									{"id":17,"name":"onSelect","params":"record","description":"当用户选中记录时触发，record为选中的记录对象",iconSkin:"icon03"},
	                                          									{"id":21,"name":"onListChange","params":"fieldVal, fieldKey, tableKey, rowIndex",
	                                          							        	 "description":"当用户在列表上编辑控件的内容改变时触发。fieldVal：该单元格的值；fieldKey：该单元格的属性key；" +
	                                          							        	 		"tableKey：列表属性Key；rowIndex：列表行索引，从0开始。",iconSkin:"icon03"},
	                                          							        {"id":22,"name":"onListSelect","params":"record, fieldKey, tableKey, rowIndex",
	                                          										 "description":"当用户在列表上Combo类编辑控件的内容选中时触发。record：选中值对象；fieldKey：该单元格的属性key；" +
	                                          												"tableKey：列表属性Key；rowIndex：列表行索引，从0开始。",iconSkin:"icon03"},
	                                          									{"id":31,"name":"onBeforeRevise","params":"newVal,oldVal,rowIndex,tableKey,rowData","description":"此事件在修订操作结束后（即保存修订日志后）触发，用于操作关联数据的保存。newVal:修订后值;oldVal:修订前值;newCap:修订后显示值，只针对于combo类控件有效;oldCap:修订前显示值，只针对于combo类控件有效;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"},
	        	                                         						{"id":30,"name":"onAfterRevise","params":"newVal,oldVal,newCap,oldCap,rowIndex,tableKey,rowData","description":"此事件在修订操作结束后（即保存修订日志后）触发，用于操作关联数据的保存。newVal:修订后值;oldVal:修订前值;newCap:修订后显示值，只针对于combo类控件有效;oldCap:修订前显示值，只针对于combo类控件有效;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"}
	             ]},
				"ImageBox":{"id":1,"name":"ImageBox",iconSkin:"icon01",children:[
				                     {"id":10,"name":"onStyler","params":"value,rowData,rowIndex","description":"此事件仅在列表显示中使用，用于单元格样式显示。返回值如：return 'background:red'。value：为该单元格的值；rowData：为该行的数据，以rowData.id或者rowData['id']调用；rowIndex：列表行索引，从0开始；",iconSkin:"icon03"},                                            
					                 {"id":11,"name":"onclick","params":"event","description":"在用户用鼠标左键单击对象时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":12,"name":"ondblclick","params":"event","description":"在用户用鼠标左键双击对象时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":13,"name":"onkeydown","params":"event","description":"当用户按下键盘按键时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":14,"name":"onkeyup","params":"event","description":"当用户释放键盘按键时触发。，event为事件对象。",iconSkin:"icon03"},
							         {"id":15,"name":"oncontextmenu","params":"event","description":"在用户用鼠标右键单击对象时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":16,"name":"onfocus","params":"event","description":"在该控件获得焦点时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":17,"name":"onblur","params":"event","description":"在该控件失去焦点时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":18,"name":"onmouseover","params":"event","description":"当用户将鼠标指针移动到对象内时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":19,"name":"onmouseout","params":"event","description":"当用户在鼠标位于对象之上时释放鼠标按钮时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":31,"name":"onBeforeRevise","params":"newVal,oldVal,rowIndex,tableKey,rowData","description":"此事件在修订操作结束后（即保存修订日志后）触发，用于操作关联数据的保存。newVal:修订后值;oldVal:修订前值;newCap:修订后显示值，只针对于combo类控件有效;oldCap:修订前显示值，只针对于combo类控件有效;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"},
  									 {"id":30,"name":"onAfterRevise","params":"newVal,oldVal,newCap,oldCap,rowIndex,tableKey,rowData","description":"此事件在修订操作结束后（即保存修订日志后）触发，用于操作关联数据的保存。newVal:修订后值;oldVal:修订前值;newCap:修订后显示值，只针对于combo类控件有效;oldCap:修订前显示值，只针对于combo类控件有效;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"},
  									 {"id":35,"name":"onAfterSetImage","params":"value,fieldKey,tableKey,rowIndex,rowData","description":"此事件在设置图片/清空图片后触发，用于操作关联数据的保存。value:图片路径值;fieldKey:字段key;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"}
				]},
				"RichTextBox":{"id":1,"name":"RichTextBox",iconSkin:"icon01",children:[
				       //ckeditor控件构成             暂时不支持事件
				       {"id":10,"name":"onStyler","params":"value,rowData,rowIndex","description":"此事件仅在列表显示中使用，用于单元格样式显示。返回值如：return 'background:red'。value：为该单元格的值；rowData：为该行的数据，以rowData.id或者rowData['id']调用；rowIndex：列表行索引，从0开始；",iconSkin:"icon03"},
				       {"id":17,"name":"onFormatter","params":"value,row,index","description":"此事件仅在列表显示中使用，用于控制值的显示格式。返回值如：return 'test'。value：为该单元格的值；row：为该行的数据，以row.id或者row['id']调用；index：列表行索引，从0开始；",iconSkin:"icon03"},
				       {"id":33,"name":"onQuickIconClick","params":"target,tableIndex, rowIndex, fieldIndex","description":"target:触发jquery对象，用于设值等操作。此方法用于控制快速图标点击事件，用于快速选择数据等操作",iconSkin:"icon03"},
				       {"id":31,"name":"onBeforeRevise","params":"newVal,oldVal,rowIndex,tableKey,rowData","description":"此事件在修订操作结束后（即保存修订日志后）触发，用于操作关联数据的保存。newVal:修订后值;oldVal:修订前值;newCap:修订后显示值，只针对于combo类控件有效;oldCap:修订前显示值，只针对于combo类控件有效;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"},
					   {"id":30,"name":"onAfterRevise","params":"newVal,oldVal,newCap,oldCap,rowIndex,tableKey,rowData","description":"此事件在修订操作结束后（即保存修订日志后）触发，用于操作关联数据的保存。newVal:修订后值;oldVal:修订前值;newCap:修订后显示值，只针对于combo类控件有效;oldCap:修订前显示值，只针对于combo类控件有效;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"}
				]},
				"ComboTree":{"id":1,"name":"ComboTree",iconSkin:"icon01",children:[
				                     {"id":10,"name":"onStyler","params":"value,rowData,rowIndex","description":"此事件仅在列表显示中使用，用于单元格样式显示。返回值如：return 'background:red'。value：为该单元格的值；rowData：为该行的数据，以rowData.id或者rowData['id']调用；rowIndex：列表行索引，从0开始；",iconSkin:"icon03"},
							         {"id":11,"name":"onChange","params":"newValue,oldValue","description":"当内容改变时触发（该事件仅在数据类型为数字型时可用）。newValue--新值；oldValue--旧值；",iconSkin:"icon03"},
							         {"id":12,"name":"onclick","params":"node","description":"在用户用鼠标左键单击对象时触发，node为节点对象。",iconSkin:"icon03"},
							         {"id":13,"name":"onDblClick","params":"node","description":"在用户用鼠标左键双击对象时触发，node为节点对象。",iconSkin:"icon03"},
							         {"id":14,"name":"onBeforeLoad","params":"node,param","description":"在该控件激活加载对象时触发，当返回false时取消加载，node为节点对象，param为参数对象传递到远程服务上。",iconSkin:"icon03"},
							         {"id":15,"name":"onLoadSuccess","params":"node,data","description":"在该控件加载数据成功时触发，node为节点对象，data为加载的源数据。",iconSkin:"icon03"},
							         {"id":16,"name":"onLoadError","params":"arguments","description":"在该控件加载数据失败时触发，arguments和jquery.ajax里面的error回调一样",iconSkin:"icon03"},
							         {"id":17,"name":"onBeforeExpand","params":"node","description":"在该控件节点展开时触发，node为节点对象",iconSkin:"icon03"},
							         {"id":18,"name":"onExpand","params":"node","description":"在该控件节点展开时触发，node为节点对象",iconSkin:"icon03"},
							         {"id":19,"name":"onBeforeCollapse","params":"node","description":"在该控件节点收起之前触发，当返回false时取消收起节点操作，node为节点对象",iconSkin:"icon03"},
							         {"id":110,"name":"onCollapse","params":"node","description":"在该控件节点收起时触发，node为节点对象",iconSkin:"icon03"},
							         {"id":111,"name":"onBeforeCheck","params":"node,checked","description":"在当用户点击节点中多选框时触发，当返回false时取消选中，node为节点对象，checked为多选框对象",iconSkin:"icon03"},
							         {"id":112,"name":"onCheck","params":"node,checked","description":"在用户点击节点上多选框时触发，node为节点对象，checked为多选框对象",iconSkin:"icon03"},
							         {"id":113,"name":"onBeforeSelect","params":"node","description":"在用户点击选中节点之前触发，当返回false时取消选中操作，node为节点对象",iconSkin:"icon03"},
							         {"id":114,"name":"onSelect","params":"node","description":"在用户点击节选中节点时触发，node为节点对象",iconSkin:"icon03"},
							         {"id":33,"name":"onSetQueryParam","params":"params,rowData,rowIndex","description":"此方法用于设置查询参数，一般用在级联查询，如省市区级联查询等；params参数为{term:\"\",page:1},term表示查询关键字，page表示查询第几页，用于参数参考；返回值为对象，如：return {\"filter_EQL_id\":\"1\"}，返回需要设置的查询参数对象；；rowData--子表行数据；rowIndex--子表行索引；",iconSkin:"icon03"},
							         {"id":115,"name":"onContextMenu","params":"e, node","description":"在用户右键单击节点时触发，node为节点对象，e为单击事件对象",iconSkin:"icon03"},
							         {"id":116,"name":"onDrop","params":"target, source, point","description":"在用户移动节点时候触发，node为树的DOM对象，该对象是将要移动的节点  source为源节点，point为表示拖放操作可能的值包括：append（追加），top（顶部），bottom（底部）",iconSkin:"icon03"},
							         {"id":117,"name":"onBeforeEdit","params":"node","description":"在修改节点节点之前时触发，node为节点对象",iconSkin:"icon03"},
							         {"id":118,"name":"onAfterEdit","params":"node","description":"在修改节点节点之后时触发，node为节点对象",iconSkin:"icon03"},
							         {"id":119,"name":"onCancelEdit","params":"node","description":"在取消修改节点节点时触发，node为节点对象",iconSkin:"icon03"},
							         {"id":21,"name":"onListChange","params":"fieldVal, fieldKey, tableKey, rowIndex",
							        	 "description":"当用户在列表上编辑控件的内容改变时触发。fieldVal：该单元格的值；fieldKey：该单元格的属性key；" +
							        	 		"tableKey：列表属性Key；rowIndex：列表行索引，从0开始。",iconSkin:"icon03"},
							         {"id":22,"name":"onListSelect","params":"record, fieldKey, tableKey, rowIndex",
										  "description":"当用户在列表上Combo类编辑控件的内容选中时触发。record：选中值对象；fieldKey：该单元格的属性key；" +
										        "tableKey：列表属性Key；rowIndex：列表行索引，从0开始。",iconSkin:"icon03"},
									{"id":31,"name":"onBeforeRevise","params":"newVal,oldVal,rowIndex,tableKey,rowData","description":"此事件在修订操作结束后（即保存修订日志后）触发，用于操作关联数据的保存。newVal:修订后值;oldVal:修订前值;newCap:修订后显示值，只针对于combo类控件有效;oldCap:修订前显示值，只针对于combo类控件有效;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"},
             						{"id":30,"name":"onAfterRevise","params":"newVal,oldVal,newCap,oldCap,rowIndex,tableKey,rowData","description":"此事件在修订操作结束后（即保存修订日志后）触发，用于操作关联数据的保存。newVal:修订后值;oldVal:修订前值;newCap:修订后显示值，只针对于combo类控件有效;oldCap:修订前显示值，只针对于combo类控件有效;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"} 		
				]},
				"RadioBox":{"id":1,"name":"RadioBox",iconSkin:"icon01",children:[
                                     {"id":10,"name":"onStyler","params":"value,rowData,rowIndex","description":"此事件仅在列表显示中使用，用于单元格样式显示。返回值如：return 'background:red'。value：为该单元格的值；rowData：为该行的数据，以rowData.id或者rowData['id']调用；rowIndex：列表行索引，从0开始；",iconSkin:"icon03"},
				                     {"id":11,"name":"onChange","params":"newValue,oldValue","description":"当内容改变时触发（该事件仅在数据类型为数字型时可用）。newValue--新值；oldValue--旧值；",iconSkin:"icon03"},
							         {"id":12,"name":"onclick","params":"event","description":"在用户用鼠标左键单击对象时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":13,"name":"ondblclick","params":"event","description":"在用户用鼠标左键双击对象时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":14,"name":"onkeydown","params":"event","description":"当用户按下键盘按键时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":15,"name":"onkeyup","params":"event","description":"当用户释放键盘按键时触发。，event为事件对象。",iconSkin:"icon03"},
							         {"id":16,"name":"oncontextmenu","params":"event","description":"在用户用鼠标右键单击对象时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":17,"name":"onfocus","params":"event","description":"在该控件获得焦点时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":18,"name":"onblur","params":"event","description":"在该控件失去焦点时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":19,"name":"onmouseover","params":"event","description":"当用户将鼠标指针移动到对象内时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":110,"name":"onmouseout","params":"event","description":"当用户在鼠标位于对象之上时释放鼠标按钮时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":21,"name":"onListChange","params":"fieldVal, fieldKey, tableKey, rowIndex",
							        	 "description":"当用户在列表上编辑控件的内容改变时触发。fieldVal：该单元格的值；fieldKey：该单元格的属性key；" +
							        	 		"tableKey：列表属性Key；rowIndex：列表行索引，从0开始。",iconSkin:"icon03"},
							         {"id":31,"name":"onBeforeRevise","params":"newVal,oldVal,rowIndex,tableKey,rowData","description":"此事件在修订操作结束后（即保存修订日志后）触发，用于操作关联数据的保存。newVal:修订后值;oldVal:修订前值;newCap:修订后显示值，只针对于combo类控件有效;oldCap:修订前显示值，只针对于combo类控件有效;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"},
             						 {"id":30,"name":"onAfterRevise","params":"newVal,oldVal,newCap,oldCap,rowIndex,tableKey,rowData","description":"此事件在修订操作结束后（即保存修订日志后）触发，用于操作关联数据的保存。newVal:修订后值;oldVal:修订前值;newCap:修订后显示值，只针对于combo类控件有效;oldCap:修订前显示值，只针对于combo类控件有效;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"}
				]},
				"CheckBox":{"id":1,"name":"CheckBox",iconSkin:"icon01",children:[
                                     {"id":10,"name":"onStyler","params":"value,rowData,rowIndex","description":"此事件仅在列表显示中使用，用于单元格样式显示。返回值如：return 'background:red'。value：为该单元格的值；rowData：为该行的数据，以rowData.id或者rowData['id']调用；rowIndex：列表行索引，从0开始；",iconSkin:"icon03"},
				                     {"id":11,"name":"onclick","params":"event","description":"在用户用鼠标左键单击对象时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":12,"name":"ondblclick","params":"event","description":"在用户用鼠标左键双击对象时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":13,"name":"onkeydown","params":"event","description":"当用户按下键盘按键时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":14,"name":"onkeyup","params":"event","description":"当用户释放键盘按键时触发。，event为事件对象。",iconSkin:"icon03"},
							         {"id":15,"name":"oncontextmenu","params":"event","description":"在用户用鼠标右键单击对象时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":16,"name":"onfocus","params":"event","description":"在该控件获得焦点时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":17,"name":"onblur","params":"event","description":"在该控件失去焦点时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":18,"name":"onmouseover","params":"event","description":"当用户将鼠标指针移动到对象内时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":19,"name":"onmouseout","params":"event","description":"当用户在鼠标位于对象之上时释放鼠标按钮时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":21,"name":"onListChange","params":"fieldVal, fieldKey, tableKey, rowIndex",
							        	 "description":"当用户在列表上编辑控件的内容改变时触发。fieldVal：该单元格的值；fieldKey：该单元格的属性key；" +
							        	 		"tableKey：列表属性Key；rowIndex：列表行索引，从0开始。",iconSkin:"icon03"},
							        {"id":31,"name":"onBeforeRevise","params":"newVal,oldVal,rowIndex,tableKey,rowData","description":"此事件在修订操作结束后（即保存修订日志后）触发，用于操作关联数据的保存。newVal:修订后值;oldVal:修订前值;newCap:修订后显示值，只针对于combo类控件有效;oldCap:修订前显示值，只针对于combo类控件有效;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"},
             						{"id":30,"name":"onAfterRevise","params":"newVal,oldVal,newCap,oldCap,rowIndex,tableKey,rowData","description":"此事件在修订操作结束后（即保存修订日志后）触发，用于操作关联数据的保存。newVal:修订后值;oldVal:修订前值;newCap:修订后显示值，只针对于combo类控件有效;oldCap:修订前显示值，只针对于combo类控件有效;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"}
				]},
				"Button":{"id":1,"name":"Button",iconSkin:"icon01",children:[
                                     {"id":10,"name":"onStyler","params":"value,rowData,rowIndex","description":"此事件仅在列表显示中使用，用于单元格样式显示。返回值如：return 'background:red'。value：为该单元格的值；rowData：为该行的数据，以rowData.id或者rowData['id']调用；rowIndex：列表行索引，从0开始；",iconSkin:"icon03"}, 
							         {"id":11,"name":"onclick","params":"event","description":"在用户用鼠标左键单击对象时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":12,"name":"ondblclick","params":"event","description":"在用户用鼠标左键双击对象时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":13,"name":"onkeydown","params":"event","description":"当用户按下键盘按键时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":14,"name":"onkeyup","params":"event","description":"当用户释放键盘按键时触发。，event为事件对象。",iconSkin:"icon03"},
							         {"id":15,"name":"oncontextmenu","params":"event","description":"在用户用鼠标右键单击对象时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":16,"name":"onfocus","params":"event","description":"在该控件获得焦点时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":17,"name":"onblur","params":"event","description":"在该控件失去焦点时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":18,"name":"onmouseover","params":"event","description":"当用户将鼠标指针移动到对象内时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":19,"name":"onmouseout","params":"event","description":"当用户在鼠标位于对象之上时释放鼠标按钮时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":20,"name":"onListClick","params":"fieldVal, fieldKey, tableKey, rowIndex",
							        	 "description":"当用户单击显示在列表中某列按钮的时候触发。fieldVal：该单元格的值；fieldKey：该单元格的属性key；" +
							        	 		"tableKey：列表属性Key；rowIndex：列表行索引，从0开始。",iconSkin:"icon03"},
							         {"id":35,"name":"onAfterUploadFile","params":"value,rowIndex,tableKey,rowData","description":"此事件在上传文件结束后触发，用于操作关联数据的保存。value:上传的附件ID值;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"}	 		
				]},
				"HyperLink":{"id":1,"name":"HyperLink",iconSkin:"icon01",children:[
                                     {"id":10,"name":"onStyler","params":"value,rowData,rowIndex","description":"此事件仅在列表显示中使用，用于单元格样式显示。返回值如：return 'background:red'。value：为该单元格的值；rowData：为该行的数据，以rowData.id或者rowData['id']调用；rowIndex：列表行索引，从0开始；",iconSkin:"icon03"},
			                         {"id":11,"name":"onclick","params":"event","description":"在用户用鼠标左键单击对象时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":12,"name":"ondblclick","params":"event","description":"在用户用鼠标左键双击对象时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":13,"name":"onkeydown","params":"event","description":"当用户按下键盘按键时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":14,"name":"onkeyup","params":"event","description":"当用户释放键盘按键时触发。，event为事件对象。",iconSkin:"icon03"},
							         {"id":15,"name":"oncontextmenu","params":"event","description":"在用户用鼠标右键单击对象时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":16,"name":"onfocus","params":"event","description":"在该控件获得焦点时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":17,"name":"onblur","params":"event","description":"在该控件失去焦点时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":18,"name":"onmouseover","params":"event","description":"当用户将鼠标指针移动到对象内时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":19,"name":"onmouseout","params":"event","description":"当用户在鼠标位于对象之上时释放鼠标按钮时触发，event为事件对象。",iconSkin:"icon03"},
							         {"id":20,"name":"onListClick","params":"fieldVal, fieldKey, tableKey, rowIndex",
							        	 "description":"当用户单击显示在列表中某列超链接的时候触发。fieldVal：该单元格的值；fieldKey：该单元格的属性key；" +
							        	 		"tableKey：列表属性Key；rowIndex：列表行索引，从0开始。",iconSkin:"icon03"},
								     {"id":35,"name":"onAfterUploadFile","params":"value,rowIndex,tableKey,rowData","description":"此事件在上传文件结束后触发，用于操作关联数据的保存。value:上传的附件ID值;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"}
				]},
				"BrowserBox":{"id":1,"name":"BrowserBox",iconSkin:"icon01",children:[
				                          //该控件由iframe组成暂时不支持事件                                    
				                     {"id":10,"name":"onStyler","params":"value,rowData,rowIndex","description":"此事件仅在列表显示中使用，用于单元格样式显示。返回值如：return 'background:red'。value：为该单元格的值；rowData：为该行的数据，以rowData.id或者rowData['id']调用；rowIndex：列表行索引，从0开始；",iconSkin:"icon03"}
				]},
				"DataGrid":{"id":1,"name":"DataGrid",iconSkin:"icon01",children:[
                                     {"id":10,"name":"onStyler","params":"value,rowData,rowIndex","description":"此事件仅在列表显示中使用，用于单元格样式显示。返回值如：return 'background:red'。value：为该单元格的值；rowData：为该行的数据，以rowData.id或者rowData['id']调用；rowIndex：列表行索引，从0开始；",iconSkin:"icon03"},
							         {"id":11,"name":"onLoadSuccess","params":"data","description":"在控件加载数据时触发，data为加载的数据。",iconSkin:"icon03"},
							         {"id":12,"name":"onLoadError","params":"","description":"在控件加载数据是失败时触发。",iconSkin:"icon03"},
							         {"id":13,"name":"onBeforeLoad","params":"param","description":"在控件加载数据之前触发，如果返回false则取消加载，param为加载传递到远程服务器上数据。",iconSkin:"icon03"},
							         {"id":14,"name":"onClickRow","params":"rowIndex, rowData","description":"在鼠标左键单击候表格一行时触发，rowIndex为表格行号，rowData为该行的数据。",iconSkin:"icon03"},
							         {"id":15,"name":"onDblClickRow","params":"rowIndex, rowData","description":"在鼠标左键双击候表格一行时触发，rowIndex为表格行号，rowData为该行的数据。",iconSkin:"icon03"},
							         {"id":16,"name":"onClickCell","params":"rowIndex, field, value","description":"在鼠标左键单击候表格单元格时触发，rowIndex为表格行号，field为单元格一列的属性，value为该但与格的值。",iconSkin:"icon03"},
							         {"id":17,"name":"onDblClickCell","params":"rowIndex, field, value","description":"在鼠标左键单双候表格单元格时触发，rowIndex为表格行号，field为单元格一列的属性，value为该但与格的值。",iconSkin:"icon03"},
							         {"id":18,"name":"onSortColumn","params":"sort, order","description":"在鼠标左键点击击候表格一列排序时触发，sort为表格排序列的字段名，oder为排序列的顺序。",iconSkin:"icon03"},
							         {"id":19,"name":"onResizeColumn","params":"field, width","description":"在拖动改变表格列宽时触发，sort为表格列属性，width为列宽度。",iconSkin:"icon03"},
							         {"id":110,"name":"onSelect","params":"rowIndex, rowData","description":"在用户选中一行时触发，rowIndex为表格行号，行号从0开始，rowData为该行数据。",iconSkin:"icon03"},
							         {"id":111,"name":"onUnselect","params":"rowIndex, rowData","description":"在用户取消选中一行时触发，rowIndex为表格行号，行号从0开始，rowData为该行数据。",iconSkin:"icon03"},
							         {"id":112,"name":"onSelectAll","params":"rows","description":"在用户选中所有行时触发，rows为所有行的数据。",iconSkin:"icon03"},
							         {"id":113,"name":"onUnselectAll","params":"rows","description":"在用户取消选中所有行时触发，rows为所有行的数据。",iconSkin:"icon03"},
							         {"id":114,"name":"onCheck","params":"rowIndex,rowData","description":"在用户选中一行前多选按钮时触发，rowIndex为表格行号，行号从0开始，rowData为该行数据。",iconSkin:"icon03"},
							         {"id":115,"name":"onUncheck","params":"rowIndex,rowData","description":"在用户取消选中一行前多选按钮时触发，rowIndex为表格行号，行号从0开始，rowData为该行数据。",iconSkin:"icon03"},
							         {"id":116,"name":"onCheckAll","params":"rows","description":"在用户选中所有行前多选按钮时触发，rows为所有行的数据。",iconSkin:"icon03"},
							         {"id":117,"name":"onUncheckAll","params":"rows","description":"在用户取消选中所有行前多选按钮时触发，rows为所有行的数据。",iconSkin:"icon03"},
							         {"id":118,"name":"onBeforeEdit","params":"rowIndex,rowData","description":"在用户开始编辑一行之前触发，rowIndex为表格行号，行号从0开始，rowData为该行数据。",iconSkin:"icon03"},
							         {"id":119,"name":"onAfterEdit","params":"rowIndex,rowData,changes","description":"在用户结束编辑一行之后触发，rowIndex为表格行号，行号从0开始，rowData为该行数据，changes为更改的字段/值对。",iconSkin:"icon03"},
							         {"id":120,"name":"onCancelEdit","params":"rowIndex,rowData","description":"在用户取消编辑一行时触发，rowIndex为表格行号，行号从0开始，rowData为该行数据。",iconSkin:"icon03"},
							         {"id":121,"name":"onHeaderContextMenu","params":"e, field","description":"在用户鼠标右键单击表头弹出菜单时触发，e为点击事件对象，field为一列属性。",iconSkin:"icon03"},
							         {"id":122,"name":"onRowContextMenu","params":"e, rowIndex, rowData","description":"在用户鼠标右键单击一行弹出菜单时触发，e为点击事件对象，rowIndex为该行行号，field为一列属性。",iconSkin:"icon03"}
				]},
				"TreeGrid":{"id":1,"name":"TreeGrid",iconSkin:"icon01",children:[
                                    {"id":10,"name":"onStyler","params":"value,rowData,rowIndex","description":"此事件仅在列表显示中使用，用于单元格样式显示。返回值如：return 'background:red'。value：为该单元格的值；rowData：为该行的数据，以rowData.id或者rowData['id']调用；rowIndex：列表行索引，从0开始；",iconSkin:"icon03"},
						            {"id":11,"name":"onClickRow","params":"row","description":"在鼠标左键单击候树一个节点时触发，row为加载出行的数据",iconSkin:"icon03"},
									{"id":12,"name":"onDblClickRow","params":"row","description":"在鼠标左键双击候表格一行时触发，row为加载出行的数据。",iconSkin:"icon03"},
									{"id":13,"name":"onClickCell","params":"field,row","description":"在鼠标左键单击表格单元格元素时触发，field为表格列的属性，row为单元格行的数据。",iconSkin:"icon03"},
									{"id":14,"name":"onDblClickCell","params":"field,row","description":"在鼠标左键双击候表格单元格元素时触发，field为表格列的属性，row为单元格行的数据。",iconSkin:"icon03"},
									{"id":15,"name":"onLoadSuccess","params":"row, data","description":"在控件加载数据时触发，row为行的数据，data为加载的数据。",iconSkin:"icon03"},
									{"id":16,"name":"onLoadError","params":"arguments","description":"在控件加载数据是失败时触发。arguments和jquery.ajax里面的error回调一样",iconSkin:"icon03"},
									{"id":17,"name":"onBeforeLoad","params":"row, param","description":"在控件加载数据之前触发，如果返回false则取消加载，row为行的数据，param为加载传递到远程服务器上数据。",iconSkin:"icon03"},
									{"id":18,"name":"onBeforeExpand","params":"row","description":"在该控件节点展开时触发，row为行的数据。",iconSkin:"icon03"},
									{"id":19,"name":"onExpand","params":"row","description":"在该控件节点展开时触发，row为行的数据。",iconSkin:"icon03"},
									{"id":110,"name":"onBeforeCollapse","params":"row","description":"在该控件节点收起之前触发，当返回false时取消收起节点操作，row为行的数据。",iconSkin:"icon03"},
									{"id":111,"name":"onCollapse","params":"row","description":"在该控件节点收起时触发，row为行的数据。",iconSkin:"icon03"},
									{"id":112,"name":"onContextMenu","params":"e,row","description":"在用户鼠标右键单击一行弹出菜单时触发，e为点击事件对象，row为行的数据。",iconSkin:"icon03"},
									{"id":113,"name":"onBeforeEdit","params":"row","description":"在用户开始编辑一行之前触发，row为行的数据。",iconSkin:"icon03"},
									{"id":120,"name":"onAfterEdit","params":"row,changes","description":"在用户编辑一行之后时触发，row为行的数据，changes为更改的字段/值对。",iconSkin:"icon03"},
									{"id":121,"name":"onCancelEdit","params":"row","description":"在用户取消编辑一行时触发，row为行的数据。",iconSkin:"icon03"}
				         
				]},
				"Tree":{"id":1,"name":"Tree",iconSkin:"icon01",children:[
                                  {"id":10,"name":"onStyler","params":"value,rowData,rowIndex","description":"此事件仅在列表显示中使用，用于单元格样式显示。返回值如：return 'background:red'。value：为该单元格的值；rowData：为该行的数据，以rowData.id或者rowData['id']调用；rowIndex：列表行索引，从0开始；",iconSkin:"icon03"},
				                  {"id":11,"name":"onclick","params":"node","description":"在用户用鼠标左键单击对象时触发，node为节点对象。",iconSkin:"icon03"},
				                  {"id":12,"name":"onDblClick","params":"node","description":"在用户用鼠标左键双击对象时触发，node为节点对象。",iconSkin:"icon03"},
				                  {"id":13,"name":"onBeforeLoad","params":"node,param","description":"在该控件激活加载对象时触发，当返回false时取消加载，node为节点对象，param为参数对象传递到远程服务上。",iconSkin:"icon03"},
				                  {"id":14,"name":"onLoadSuccess","params":"node,data","description":"在该控件加载数据成功时触发，node为节点对象，data为加载的源数据。",iconSkin:"icon03"},
				                  {"id":15,"name":"onLoadError","params":"arguments","description":"在该控件加载数据失败时触发，arguments和jquery.ajax里面的error回调一样",iconSkin:"icon03"},
				                  {"id":16,"name":"onBeforeExpand","params":"node","description":"在该控件节点展开时触发，node为节点对象",iconSkin:"icon03"},
				                  {"id":17,"name":"onExpand","params":"node","description":"在该控件节点展开时触发，node为节点对象",iconSkin:"icon03"},
				                  {"id":18,"name":"onBeforeCollapse","params":"node","description":"在该控件节点收起之前触发，当返回false时取消收起节点操作，node为节点对象",iconSkin:"icon03"},
				                  {"id":19,"name":"onCollapse","params":"node","description":"在该控件节点收起时触发，node为节点对象",iconSkin:"icon03"},
				                  {"id":110,"name":"onBeforeCheck","params":"node,checked","description":"在当用户点击节点中多选框时触发，当返回false时取消选中，node为节点对象，checked为多选框对象",iconSkin:"icon03"},
				                  {"id":111,"name":"onCheck","params":"node,checked","description":"在用户点击节点上多选框时触发，node为节点对象，checked为多选框对象",iconSkin:"icon03"},
				                  {"id":112,"name":"onBeforeSelect","params":"node","description":"在用户点击选中节点之前触发，当返回false时取消选中操作，node为节点对象",iconSkin:"icon03"},
				                  {"id":113,"name":"onSelect","params":"node","description":"在用户点击节选中节点时触发，node为节点对象",iconSkin:"icon03"},
				                  {"id":114,"name":"onContextMenu","params":"e, node","description":"在用户右键单击节点时触发，node为节点对象，e为单击事件对象",iconSkin:"icon03"},
				                  {"id":115,"name":"onDrop","params":"target, source, point","description":"在用户移动节点时候触发，node为树的DOM对象，该对象是将要移动的节点  source为源节点，point为表示拖放操作可能的值包括：append（追加），top（顶部），bottom（底部）",iconSkin:"icon03"},
				                  {"id":116,"name":"onBeforeEdit","params":"node","description":"在修改节点节点之前时触发，node为节点对象",iconSkin:"icon03"},
				                  {"id":117,"name":"onAfterEdit","params":"node","description":"在修改节点节点之后时触发，node为节点对象",iconSkin:"icon03"},
				                  {"id":118,"name":"onCancelEdit","params":"node","description":"在取消修改节点节点时触发，node为节点对象",iconSkin:"icon03"}
				]},
				"Label":{"id":1,"name":"Label",iconSkin:"icon01",children:[
                                 {"id":10,"name":"onStyler","params":"value,rowData,rowIndex","description":"此事件仅在列表显示中使用，用于单元格样式显示。返回值如：return 'background:red'。value：为该单元格的值；rowData：为该行的数据，以rowData.id或者rowData['id']调用；rowIndex：列表行索引，从0开始；",iconSkin:"icon03"},
		                         {"id":11,"name":"onclick","params":"event","description":"在用户用鼠标左键单击对象时触发，event为事件对象。",iconSkin:"icon03"},
						         {"id":12,"name":"ondblclick","params":"event","description":"在用户用鼠标左键双击对象时触发，event为事件对象。",iconSkin:"icon03"},
						         {"id":13,"name":"onkeydown","params":"event","description":"当用户按下键盘按键时触发，event为事件对象。",iconSkin:"icon03"},
						         {"id":14,"name":"onkeyup","params":"event","description":"当用户释放键盘按键时触发。，event为事件对象。",iconSkin:"icon03"},
						         {"id":15,"name":"oncontextmenu","params":"event","description":"在用户用鼠标右键单击对象时触发，event为事件对象。",iconSkin:"icon03"},
						         {"id":16,"name":"onfocus","params":"event","description":"在该控件获得焦点时触发，event为事件对象。",iconSkin:"icon03"},
						         {"id":17,"name":"onblur","params":"event","description":"在该控件失去焦点时触发，event为事件对象。",iconSkin:"icon03"},
						         {"id":18,"name":"onmouseover","params":"event","description":"当用户将鼠标指针移动到对象内时触发，event为事件对象。",iconSkin:"icon03"},
						         {"id":19,"name":"onmouseout","params":"event","description":"当用户在鼠标位于对象之上时释放鼠标按钮时触发，event为事件对象。",iconSkin:"icon03"}
				]},
				"ComboGrid":{"id":1,"name":"ComboGrid",iconSkin:"icon01",children:[
                         {"id":10,"name":"onStyler","params":"value,rowData,rowIndex","description":"此事件仅在列表显示中使用，用于单元格样式显示。返回值如：return 'background:red'。value：为该单元格的值；rowData：为该行的数据，以rowData.id或者rowData['id']调用；rowIndex：列表行索引，从0开始；",iconSkin:"icon03"},
	                     {"id":11,"name":"onChange","params":"newValue,oldValue","description":"当内容改变时触发（该事件仅在数据类型为数字型时可用）。newValue--新值；oldValue--旧值；",iconSkin:"icon03"},                                                        
				         //{"id":11,"name":"onLoadSucces","params":"data","description":"当参数data加成成功时触发","caption":"成功加载事件",iconSkin:"icon03"},{"id":12,"name":"onLoadError","params":"","description":"当控件加成失败时触发",iconSkin:"icon03"},{"id":13,"name":"onBeforeLoad","params":"param","description":"当控件加载前请求加载数据。如果返回false，加载操作将被取消。",iconSkin:"icon03"},{"id":14,"name":"onClickRow",iconSkin:"icon03"},{"id":15,"name":"onDblClickRow",iconSkin:"icon03"},{"id":16,"name":"onClickCell",iconSkin:"icon03"},{"id":17,"name":"onDblClickCell",iconSkin:"icon03"},{"id":18,"name":"onSortColumn",iconSkin:"icon03"},{"id":19,"name":"onResizeColumn",iconSkin:"icon03"},{"id":110,"name":"onSelect",iconSkin:"icon03"},{"id":111,"name":"onUnselect",iconSkin:"icon03"},{"id":112,"name":"onSelectAll",iconSkin:"icon03"},{"id":113,"name":"onUnselectAll",iconSkin:"icon03"},{"id":114,"name":"onCheck",iconSkin:"icon03"},{"id":115,"name":"onUncheck",iconSkin:"icon03"},{"id":116,"name":"onCheckAll",iconSkin:"icon03"},{"id":117,"name":"onUncheckAll",iconSkin:"icon03"},{"id":118,"name":"onBeforeEdit",iconSkin:"icon03"},{"id":119,"name":"onCancelEdit",iconSkin:"icon03"},{"id":120,"name":"onHeaderContextMenu",iconSkin:"icon03"},{"id":121,"name":"onRowContextMenu",iconSkin:"icon03"},{"id":122,"name":"onChange",iconSkin:"icon03"}
	                     {"id":12,"name":"onLoadSuccess","params":"data","description":"在控件加载数据时触发，data为加载的数据。",iconSkin:"icon03"},
	                     {"id":13,"name":"onLoadError","params":"","description":"在控件加载数据是失败时触发。",iconSkin:"icon03"},
				         {"id":14,"name":"onBeforeLoad","params":"param","description":"在控件加载数据之前触发，如果返回false则取消加载，param为加载传递到远程服务器上数据。",iconSkin:"icon03"},
				         {"id":15,"name":"onClickRow","params":"rowIndex, rowData","description":"在鼠标左键单击候表格一行时触发，rowIndex为表格行号，rowData为该行的数据。",iconSkin:"icon03"},
				         {"id":16,"name":"onDblClickRow","params":"rowIndex, rowData","description":"在鼠标左键双击候表格一行时触发，rowIndex为表格行号，rowData为该行的数据。",iconSkin:"icon03"},
				         {"id":17,"name":"onClickCell","params":"rowIndex, field, value","description":"在鼠标左键单击候表格单元格时触发，rowIndex为表格行号，field为单元格一列的属性，value为该但与格的值。",iconSkin:"icon03"},
				         {"id":18,"name":"onDblClickCell","params":"rowIndex, field, value","description":"在鼠标左键单双候表格单元格时触发，rowIndex为表格行号，field为单元格一列的属性，value为该但与格的值。",iconSkin:"icon03"},
				         {"id":19,"name":"onSortColumn","params":"sort, order","description":"在鼠标左键点击击候表格一列排序时触发，sort为表格排序列的字段名，oder为排序列的顺序。",iconSkin:"icon03"},
				         {"id":110,"name":"onResizeColumn","params":"field, width","description":"在拖动改变表格列宽时触发，sort为表格列属性，width为列宽度。",iconSkin:"icon03"},
				         {"id":111,"name":"onSelect","params":"rowIndex, rowData","description":"在用户选中一行时触发，rowIndex为表格行号，行号从0开始，rowData为该行数据。",iconSkin:"icon03"},
				         {"id":112,"name":"onUnselect","params":"rowIndex, rowData","description":"在用户取消选中一行时触发，rowIndex为表格行号，行号从0开始，rowData为该行数据。",iconSkin:"icon03"},
				         {"id":113,"name":"onSelectAll","params":"rows","description":"在用户选中所有行时触发，rows为所有行的数据。",iconSkin:"icon03"},
				         {"id":114,"name":"onUnselectAll","params":"rows","description":"在用户取消选中所有行时触发，rows为所有行的数据。",iconSkin:"icon03"},
				         {"id":115,"name":"onCheck","params":"rowIndex,rowData","description":"在用户选中一行前多选按钮时触发，rowIndex为表格行号，行号从0开始，rowData为该行数据。",iconSkin:"icon03"},
				         {"id":116,"name":"onUncheck","params":"rowIndex,rowData","description":"在用户取消选中一行前多选按钮时触发，rowIndex为表格行号，行号从0开始，rowData为该行数据。",iconSkin:"icon03"},
				         {"id":117,"name":"onCheckAll","params":"rows","description":"在用户选中所有行前多选按钮时触发，rows为所有行的数据。",iconSkin:"icon03"},
				         {"id":118,"name":"onUncheckAll","params":"rows","description":"在用户取消选中所有行前多选按钮时触发，rows为所有行的数据。",iconSkin:"icon03"},
				         {"id":119,"name":"onBeforeEdit","params":"rowIndex,rowData","description":"在用户开始编辑一行之前触发，rowIndex为表格行号，行号从0开始，rowData为该行数据。",iconSkin:"icon03"},
				         {"id":120,"name":"onAfterEdit","params":"rowIndex,rowData,changes","description":"在用户结束编辑一行之后触发，rowIndex为表格行号，行号从0开始，rowData为该行数据，changes为更改的字段/值对。",iconSkin:"icon03"},
				         {"id":121,"name":"onCancelEdit","params":"rowIndex,rowData","description":"在用户取消编辑一行时触发，rowIndex为表格行号，行号从0开始，rowData为该行数据。",iconSkin:"icon03"},
				         {"id":122,"name":"onHeaderContextMenu","params":"e, field","description":"在用户鼠标右键单击表头弹出菜单时触发，e为点击事件对象，field为一列属性。",iconSkin:"icon03"},
				         {"id":123,"name":"onRowContextMenu","params":"e, rowIndex, rowData","description":"在用户鼠标右键单击一行弹出菜单时触发，e为点击事件对象，rowIndex为该行行号，field为一列属性。",iconSkin:"icon03"},
				         {"id":33,"name":"onSetQueryParam","params":"params","description":"此方法用于设置查询参数，一般用在级联查询，如省市区级联查询等；params参数为{term:\"\",page:1},term表示查询关键字，page表示查询第几页，用于参数参考；返回值为对象，如：return {\"filter_EQL_id\":\"1\"}，返回需要设置的查询参数对象",iconSkin:"icon03"},
				         {"id":21,"name":"onListChange","params":"fieldVal, fieldKey, tableKey, rowIndex",
				        	 "description":"当用户在列表上编辑控件的内容改变时触发。fieldVal：该单元格的值；fieldKey：该单元格的属性key；" +
				        	 		"tableKey：列表属性Key；rowIndex：列表行索引，从0开始。",iconSkin:"icon03"},
				        {"id":31,"name":"onBeforeRevise","params":"newVal,oldVal,rowIndex,tableKey,rowData","description":"此事件在修订操作结束后（即保存修订日志后）触发，用于操作关联数据的保存。newVal:修订后值;oldVal:修订前值;newCap:修订后显示值，只针对于combo类控件有效;oldCap:修订前显示值，只针对于combo类控件有效;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"},
				        {"id":30,"name":"onAfterRevise","params":"newVal,oldVal,newCap,oldCap,rowIndex,tableKey,rowData","description":"此事件在修订操作结束后（即保存修订日志后）触发，用于操作关联数据的保存。newVal:修订后值;oldVal:修订前值;newCap:修订后显示值，只针对于combo类控件有效;oldCap:修订前显示值，只针对于combo类控件有效;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"}
	               ]},
	           		"zTree":{"id":1,"name":"zTree",iconSkin:"icon01",children:[	           
	           	    {"id":10,"name":"onClick","params":"event, treeId, treeNode","description":"当点击一个树节点时触发事件。event---点击事件, treeId---树的ID, treeNode---当前被点击的树节点对象；",iconSkin:"icon03"}                                                      
	           	    ]},
	               "ProgressBar":{"id":1,"name":"ProgressBar",iconSkin:"icon01",children:[
	                     //该控件暂时不支持事件                        
	                     {"id":10,"name":"onStyler","params":"value,rowData,rowIndex","description":"此事件仅在列表显示中使用，用于单元格样式显示。返回值如：return 'background:red'。value：为该单元格的值；rowData：为该行的数据，以rowData.id或者rowData['id']调用；rowIndex：列表行索引，从0开始；",iconSkin:"icon03"},
	                     {"id":31,"name":"onBeforeRevise","params":"newVal,oldVal,rowIndex,tableKey,rowData","description":"此事件在修订操作结束后（即保存修订日志后）触发，用于操作关联数据的保存。newVal:修订后值;oldVal:修订前值;newCap:修订后显示值，只针对于combo类控件有效;oldCap:修订前显示值，只针对于combo类控件有效;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"},
						 {"id":30,"name":"onAfterRevise","params":"newVal,oldVal,newCap,oldCap,rowIndex,tableKey,rowData","description":"此事件在修订操作结束后（即保存修订日志后）触发，用于操作关联数据的保存。newVal:修订后值;oldVal:修订前值;newCap:修订后显示值，只针对于combo类控件有效;oldCap:修订前显示值，只针对于combo类控件有效;rowIndex:子表行索引，只针对于子表字段有效；tableKey:子表Key，只针对于子表字段有效;rowData:字表行数据，只针对字表字段有效。",iconSkin:"icon03"}
	               ]},
	               "EditPage":{"id":1,"name":"EditPage",iconSkin:"icon01",children:[
	                     {"id":11,"name":"onBeforeSave","params":"data","description":"data数据为{'businessMessage':[]}; businessMassage是数组，里面存放信息对象" +
	                     		"{" +
	                     		"'userId' : 接收用户的ID,"+
		 				   		"'title' :'通知的标题',"+
		 						"'content' : '通知内容',"+
		 						"'fields': [{'key':'字段KEY','caption':'字段名称CAPYION','dataType':'字段类型TYPE'}]"+
		 						"}",iconSkin:"icon03"},
	                     {"id":12,"name":"onAfterSave","params":"data","description":"当页面表单数据保存完毕后调用，data为数据库获取的数据对象",iconSkin:"icon03"},
		 				 {"id":13,"name":"onAfterLoadData","params":"data","description":"当页面表单数据加载完毕后调用，data为数据库获取的数据对象",iconSkin:"icon03"},
		 				 {"id":14,"name":"onAfterInvalidData","params":"","description":"当用户点击作废按钮后调用，用于执行数据作废后的操作",iconSkin:"icon03"},
		 				 {"id":25,"name":"onBeforeInvalidData","params":"","description":"当用户点击作废按钮后立即调用，用于执行数据作废前的操作，返回false，表示不继续操作；其余情况均为继续操作",iconSkin:"icon03"},
		 				 {"id":15,"name":"addJavaScript","params":"","description":"在script标签中添加js代码块",iconSkin:"icon03"},
		 				 {"id":16,"name":"addCssStyle","params":"","description":"在初始化页面之前添加指定样式",iconSkin:"icon03"},
		 	 			 {"id":21,"name":"referJsOrCss","params":"","description":"引用外部JS或CSS文件",iconSkin:"icon03"},
		 				 {"id":17,"name":"onAfterAddInit","params":"","description":"新增页面或新增操作初始化后调用",iconSkin:"icon03"},
		 				 {"id":18,"name":"onBeforeStartProcess","params":"","description":"启动流程（或者审核通过）操作前触发，用于做些流程启动（或者审核通过）前的设置操作，return false表示后续不启动流程操作（或者审核通过）",iconSkin:"icon03"},
		 				 {"id":19,"name":"onAfterStartProcess","params":"","description":"启动流程（或者审核通过）操作成功后触发，用于做些流程启动（或者审核通过）后的设置操作",iconSkin:"icon03"},
		 				 {"id":22,"name":"onBeforeCancelProcess","params":"","description":"取消审批（或取消审核/撤销审批）操作前触发，用于做些取消审批（或取消审核/撤销审批）前的设置操作，return false表示后续不取消审批（或取消审核/撤销审批）",iconSkin:"icon03"},
		 				 {"id":23,"name":"onAfterCancelProcess","params":"","description":"取消审批（或取消审核/撤销审批）操作成功后触发，用于做些取消审批（或取消审核/撤销审批）后的设置操作",iconSkin:"icon03"},
						 {"id":20,"name":"onAfterCopyData","params":"data,isChange","description":"当页面表单数据被复制后初始化成功后调用，data为数据库获取的数据对象，isChange表示是否变更，true--变更，其他为复制",iconSkin:"icon03"},
						 {"id":40,"name":"onBeforeDelete","params":"","description":"当删除页面表单数据前触发，函数返回false，表示不会继续删除操作",iconSkin:"icon03"},
						 {"id":21,"name":"onAfterDisplayButton","params":"","description":"当查看单据时，所有操作按钮加载完成后触发。一般用于控制按钮的显示和隐藏操作，如$('#tcopy').hide(); 表示隐藏复制按钮。tadd--新增按钮key；tedit--修改按钮key；tdel--删除按钮key；tcopy--复制按钮key",iconSkin:"icon03"},
						 {"id":24,"name":"onAfterModifyInit","params":"","description":"当查看单据时点击修改按钮后触发，即启用修改后触发。一般可用于设置按钮只读等操作",iconSkin:"icon03"}
		 				 ]},
	               "QueryPage":{"id":1,"name":"QueryPage",iconSkin:"icon01",children:[
	            	   {"id":11,"name":"onRowStyler","params":"rowIndex,rowData","description":"此事件仅在列表显示中使用，用于行样式显示。返回值如：return 'background:red'。rowIndex：列表行索引，从0开始;rowData：为该行的数据，以rowData.id或者rowData['id']调用",iconSkin:"icon03"},
	            	   {"id":12,"name":"onAfterFieldEdit","params":"field,rowIndex,rowData,changes","description":"在用户结束编辑一单元格时触发，field为列key，rowIndex为表格行号，行号从0开始，rowData为该行数据，changes为更改的字段/值对。",iconSkin:"icon03"},
	            	   {"id":17,"name":"onBeforeFieldEdit","params":"field,rowIndex,rowData","description":"在用户开始编辑一单元格时触发，field为列key，rowIndex为表格行号，行号从0开始，rowData为该行数据。",iconSkin:"icon03"},
	            	   {"id":13,"name":"onAfterDeleteRow","params":"rowIndex,rowData","description":"在用户删除一行后触发，rowIndex为表格行号，行号从0开始，rowData为该删除行数据。",iconSkin:"icon03"},
	            	   {"id":27,"name":"onAfterInitEdatagrid","params":"target","description":"当子表数据加载完成后触发，一般可用于动态初始化子表单元格样式等操作。target：子表datagrid的jQuery对象",iconSkin:"icon03"},         	   
	            	   {"id":14,"name":"onLoadSuccess","params":"data","description":"当列表数据加载完毕后触发。data为列表数据，格式为：{\"pageSize\":10,\"pageNumber\":1,\"rows\":[]}，其中data.rows表示实际数据",iconSkin:"icon03"},
	            	   {"id":15,"name":"onDblClickRow","params":"rowIndex,rowData","description":"在用户双击列表一行后触发，rowIndex为表格行号，行号从0开始，rowData为该行数据。",iconSkin:"icon03"},
	            	   {"id":18,"name":"onBeforeStartProcess","params":"","description":"审核通过操作前触发，用于做些审核通过前的设置操作，return false表示后续不启动审核通过。供列表页面审核通过按钮使用",iconSkin:"icon03"},
		 			   {"id":19,"name":"onAfterStartProcess","params":"","description":"审核通过操作成功后触发，用于做些审核通过后的设置操作。供列表页面审核通过按钮使用",iconSkin:"icon03"},
		 			   {"id":22,"name":"onBeforeCancelProcess","params":"","description":"取消审批（或取消审核/撤销审批）操作成功前触发，用于做些取消审批（或取消审核/撤销审批）前的设置操作，return false表示后续不取消审批（或取消审核/撤销审批）。供列表页面取消审核按钮使用",iconSkin:"icon03"},
		 			   {"id":23,"name":"onAfterCancelProcess","params":"","description":"取消审核操作成功后触发，用于做些取消审核后的设置操作。供列表页面取消审核按钮使用",iconSkin:"icon03"},
		 			   {"id":25,"name":"setQueryParam","params":"","description":"用于设置虚拟表列表查询参数，如：{\"fix_EQL_id\":jwpf.getId(),\"fix_EQS_name\":jwpf.getFormVal('name')},注意尾部不需要加分号",iconSkin:"icon03"},
		 			   {"id":26,"name":"showDetailView","params":"index,row,container","description":"用于设置列表每条记录展开后的详情视图，index为行号，row为行记录，container为详情视图的容器对象",iconSkin:"icon03"},
		 			   {"id":30,"name":"addJavaScript","params":"","description":"在script标签中添加js代码块",iconSkin:"icon03"},
		 			   {"id":31,"name":"addCssStyle","params":"","description":"在初始化页面之前添加指定样式",iconSkin:"icon03"},
		 	 		   {"id":32,"name":"referJsOrCss","params":"","description":"引用外部JS或CSS文件",iconSkin:"icon03"},
		 	 		   {"id":40,"name":"onBeforeDelete","params":"","description":"当删除页面表单数据前触发，函数返回false，表示不会继续删除操作",iconSkin:"icon03"},
		 	 		   {"id":41,"name":"onBeforeQuery","params":"paramObj","description":"当页面查询开始前触发，一般用于设置默认查询参数。paramObj参数值如{\"fix_EQL_id\":1}对象",iconSkin:"icon03"}
		 	 		   ]},
		 			"DialogueWindow":{"id":1,"name":"DialogueWindow",iconSkin:"icon01",children:[
 	                     {"id":11,"name":"onChange","params":"newValue,oldValue","description":"当内容改变时触发（该事件仅在数据类型为数字型时可用）。newValue--新值{key:关联id,caption:该单元格的值}；oldValue--旧值{key:关联id,caption:该单元格的值}；",iconSkin:"icon03"},                                                        
 				         {"id":12,"name":"onListChange","params":"fieldVal, fieldKey, tableKey, rowIndex",
 				        	 "description":"当用户在列表上编辑控件的内容改变时触发。fieldVal：{key:关联id,caption:该单元格的值}；fieldKey：该单元格的属性key；" +
 				        	 		"tableKey：列表属性Key；rowIndex：列表行索引，从0开始。",iconSkin:"icon03"},
 				         {"id":33,"name":"onSetQueryParam","params":"params,rowData,rowIndex","description":"此方法用于设置查询参数，一般用在级联查询，如省市区级联查询等；params参数为{term:\"\",page:1},term表示查询关键字，page表示查询第几页，用于参数参考；返回值为对象，如：return {\"filter_EQL_id\":\"1\"}，返回需要设置的查询参数对象；rowData--子表行数据；rowIndex--子表行索引；",iconSkin:"icon03"}
 	               ]}
				};
	if(b){
		if(d[b]){
			return d[b];
		}
	}
};
})(jQuery);