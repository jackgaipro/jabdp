/**
 * edatagrid - jQuery EasyUI
 * 
 * Licensed under the GPL:
 *   http://www.gnu.org/licenses/gpl.txt
 *
 * Copyright 2011 stworthy [ stworthy@gmail.com ] 
 * 
 * Dependencies:
 *   datagrid
 *   messager
 * 
 */
(function($){
	function buildGrid(target){
		var opts = $.data(target, 'edatagrid').options;
		$(target).datagrid($.extend({}, opts, {
			/*onDblClickCell:function(index,field){
				if (opts.editing){
					$(this).edatagrid('editRow', index);
					focusEditor(field);
				}
			},*/
			onClickCell:function(index,field){
				//var x = $(target).datagrid('getPanel').panel("panel");
				//console.log(x.find("input",$("#table_grid_${param.index}").parent()[0]));
				var shiftisdowning = $(target).data("shiftisdowning");
				var ctrlisdowning = $(target).data("ctrlisdowning");
				//console.log(ctrlisdowning);
				if(shiftisdowning) {
					var x = $(target).edatagrid('getSelections');
					//console.log(x);alert();
					if(x.length>0){
						var xx = $(target).edatagrid('getRowIndex',x[x.length-1]);
						$(target).edatagrid('unselectAll');
						if(xx>parseInt(index)) {
							for(var i=parseInt(index); i <= xx; i++) {
								$(target).edatagrid('selectRow', i);
							}
						}else if(xx<parseInt(index)) {
							for(var i=parseInt(index); i >= xx; i--) {
								$(target).edatagrid('selectRow', i);
							}
						}
					}else{
						$(target).edatagrid('selectRow', index);
					}
					$(target).edatagrid('selectRow', index);
				}else if(ctrlisdowning){
					//console.log(field);
					//console.log($(target).edatagrid('getEditor',{index:index}));
					var x = $(target).edatagrid('getSelections');
					var m = true;
					for(var i in x) {
						var xx = $(target).edatagrid('getRowIndex',x[i]);
						//console.log(parseInt(xx));
						//console.log(index);
						if(xx===parseInt(index)) {
							$(target).edatagrid('unselectRow', index);
							m = false;
							break;
						}
					}
					if(m){
						$(target).edatagrid('selectRow', index);
					}
					//console.log($(target).edatagrid('getSelections'));
				}else{
					if(opts.editing && opts.editIndex >= 0) {
						$(this).edatagrid('editRow', index);
						focusEditor(field);
					}
				}
			},
			onAfterEdit: function(index, row){
			//	opts.editIndex = undefined;
				var url = row.isNewRecord ? opts.saveUrl : opts.updateUrl;
				if (url){
					$.post(url, row, function(data){
						data.isNewRecord = null;
						$(target).datagrid('updateRow', {
							index: index,
							row: data
						});
						if (opts.tree){
							var t = $(opts.tree);
							var node = t.tree('find', row.id);
							if (node){
								node.text = row[opts.treeTextField];
								t.tree('update', node);
							} else {
								var pnode = t.tree('find', row[opts.treeParentField]);
								t.tree('append', {
									parent: (pnode ? pnode.target : null),
									data: [{id:row.id,text:row[opts.treeTextField]}]
								});
							}
						}
						opts.onSave.call(target, index, row);
					},'json');
				}
				if (opts.onAfterEdit) opts.onAfterEdit.call(target, index, row);
			},
			onCancelEdit: function(index, row){
				opts.editIndex = undefined;
				// by GaoXJ 2012-06-03
				/*if (row.isNewRecord) {
					$(this).datagrid('deleteRow', index);
				}*/
				if (opts.onCancelEdit) opts.onCancelEdit.call(target, index, row);
			},
			onBeforeLoad: function(param){
				$(this).datagrid('rejectChanges');
				if (opts.tree){
					var node = $(opts.tree).tree('getSelected');
					param[opts.treeParentField] = node ? node.id : undefined;
				}
				if (opts.onBeforeLoad) opts.onBeforeLoad.call(target, param);
			}
		}));
		
		function focusEditor(field){
			var editor = $(target).datagrid('getEditor', {index:opts.editIndex,field:field});
			if (editor){
				editor.target.focus();
			} else {
				var editors = $(target).datagrid('getEditors', opts.editIndex);
				if (editors.length){
					editors[0].target.focus();
				}
			}
		}
		
		if (opts.tree){
			$(opts.tree).tree({
				url: opts.treeUrl,
				onClick: function(node){
					$(target).datagrid('load');
				},
				onDrop: function(dest,source,point){
					var targetId = $(this).tree('getNode', dest).id;
					$.ajax({
						url: opts.treeDndUrl,
						type:'post',
						data:{
							id:source.id,
							targetId:targetId,
							point:point
						},
						dataType:'json',
						success:function(){
							$(target).datagrid('load');
						}
					});
				}
			});
		}
	}
	
	$.fn.edatagrid = function(options, param){
		if (typeof options == 'string'){
			var method = $.fn.edatagrid.methods[options];
			if (method){
				return method(this, param);
			} else {
				return this.datagrid(options, param);
			}
		}
		
		options = options || {};
		return this.each(function(){
			var state = $.data(this, 'edatagrid');
			if (state){
				$.extend(state.options, options);
			} else {
				$.data(this, 'edatagrid', {
					options: $.extend({}, $.fn.edatagrid.defaults, $.fn.edatagrid.parseOptions(this), options)
				});
			}
			buildGrid(this);
		});
	};
	
	$.fn.edatagrid.parseOptions = function(target){
		return $.extend({}, $.fn.datagrid.parseOptions(target), {
		});
	};
	
	//设置鼠标焦点
	function setFocus(target,opts,field){
		var editors = target.datagrid('getEditors', opts.editIndex);
		if (editors.length){
			editors[0].target.focus();
			//alert(editors[0].actions.getValue());
			//$(editors[0].target).bind('keyup',function(){alert("keyup")});
		}
	}
	$.fn.edatagrid.methods = {			
		options: function(jq){
			var opts = $.data(jq[0], 'edatagrid').options;
			return opts;
		},
		enableEditing: function(jq){
			return jq.each(function(){
				var opts = $.data(this, 'edatagrid').options;
				opts.editing = true;
			});
		},
		disableEditing: function(jq){
			return jq.each(function(){
				var opts = $.data(this, 'edatagrid').options;
				opts.editing = false;
			});
		},
		editRow: function(jq, index){
			return jq.each(function(){
				var dg = $(this);
				var opts = $.data(this, 'edatagrid').options;
				var editIndex = opts.editIndex;
				if (editIndex != index){
					if (dg.datagrid('validateRow', editIndex)){
						dg.datagrid('endEdit', editIndex);
						dg.datagrid('beginEdit', index);
						opts.editIndex = index;
					} else {
						setTimeout(function(){
							dg.datagrid('selectRow', editIndex);
						}, 0);
					}
				}
			});
		},
		loadTableData:function(jq,data){
			return jq.each(function(){
				var dg = $(this);
				if(data){
					if(data.rows){
						$.each(data.rows,function(k,v){
							dg.datagrid('appendRow',v);
							setTimeout(function(){dg.datagrid('beginEdit', k);},0);
								
						});
					}
				}
			});
		},
		addRow: function(jq, row,index){
			return jq.each(function(){
				var dg = $(this);
				var opts = $.data(this, 'edatagrid').options;
			//	alert("Index:"+opts.editIndex);
				if(index){
					opts.editIndex = index;
				}
				var editIndex = opts.editIndex;
				if (opts.editIndex >= 0){
					if (!dg.datagrid('validateRow', opts.editIndex)){
						dg.datagrid('selectRow', opts.editIndex);
						return;
					}
					dg.datagrid('endEdit', editIndex);
					//alert(editIndex);
					//dg.datagrid('beginEdit', editIndex);
						
				}
				if(row) {
					$.extend(row, {isNewRecord:true});
					dg.datagrid('appendRow', row);
				} else {
					dg.datagrid('appendRow', {isNewRecord:true});
				}
				var rows = dg.datagrid('getRows');
				opts.editIndex = rows.length - 1;
				//alert( opts.editIndex);
				dg.datagrid('beginEdit', opts.editIndex);
				//dg.datagrid('selectRow', opts.editIndex);
				if (opts.tree){
					var node = $(opts.tree).tree('getSelected');
					rows[opts.editIndex][opts.treeParentField] = (node ? node.id : 0);
				}
				//延迟加载鼠标焦点事件
				setTimeout(function(){setFocus(dg,opts);}, 0);
				opts.onAdd.call(this, opts.editIndex, rows[opts.editIndex]);
				
				});
		},
		insertNewRow: function(jq, index){
			return jq.each(function(){
				var dg = $(this);
				var opts = $.data(this, 'edatagrid').options;
				if (opts.editIndex >= 0){
					if (!dg.datagrid('validateRow', opts.editIndex)){
						dg.datagrid('selectRow', opts.editIndex);
						return;
					}
					dg.datagrid('endEdit', opts.editIndex);
				}
				var rows = dg.datagrid('getRows');
				if(index >= 0 && index <= rows.length) {
					dg.datagrid('insertRow', {index:index, row:{isNewRecord:true}});
					rows = dg.datagrid('getRows');
					opts.editIndex = index;
				} else {
					dg.datagrid('appendRow', {isNewRecord:true});
					rows = dg.datagrid('getRows');
					opts.editIndex = rows.length - 1;
				}
				dg.datagrid('beginEdit', opts.editIndex);
				//dg.datagrid('selectRow', opts.editIndex);
				if (opts.tree){
					var node = $(opts.tree).tree('getSelected');
					rows[opts.editIndex][opts.treeParentField] = (node ? node.id : 0);
				}
				
				opts.onAdd.call(this, opts.editIndex, rows[opts.editIndex]);
			});
		},
		saveRow: function(jq){
			return jq.each(function(){
				var index = $(this).edatagrid('options').editIndex;
				$(this).datagrid('endEdit', index);
			});
		},
		cancelRow: function(jq){
			return jq.each(function(){
				var index = $(this).edatagrid('options').editIndex;
				$(this).datagrid('cancelEdit', index);
			});
		},
		destroyRow: function(jq){
			return jq.each(function(){
				var dg = $(this);
				var opts = $.data(this, 'edatagrid').options;
				var row = dg.datagrid('getSelected');
				if (!row){
					$.messager.show({
						title: opts.destroyMsg.norecord.title,
						msg: opts.destroyMsg.norecord.msg
					});
					return;
				}
				$.messager.confirm(opts.destroyMsg.confirm.title,opts.destroyMsg.confirm.msg,function(r){
					if (r){
						var index = dg.datagrid('getRowIndex', row);
						if (row.isNewRecord){
							dg.datagrid('cancelEdit', index);
							//add by GaoXJ 2012-06-03
							dg.datagrid('deleteRow', index);
							opts.editIndex = opts.editIndex-1;
						} else {
							if (opts.destroyUrl){
								$.post(opts.destroyUrl, {id:row.id}, function(){
									if (opts.tree){
										dg.datagrid('reload');
										var t = $(opts.tree);
										var node = t.tree('find', row.id);
										if (node){
											t.tree('remove', node.target);
										}
									} else {
										dg.datagrid('cancelEdit', index);
										dg.datagrid('deleteRow', index);
										opts.editIndex = opts.editIndex-1;
									}
									opts.onDestroy.call(dg[0], index, row);
								});
							} else {
								dg.datagrid('cancelEdit', index);
								dg.datagrid('deleteRow', index);
								opts.editIndex = opts.editIndex-1;
							}
						}
					}
				});
			});
		}
	};
	
	$.fn.edatagrid.defaults = $.extend({}, $.fn.datagrid.defaults, {
		editing: true,
		editIndex: -1,
		destroyMsg:{
			norecord:{
				title:'Warning',
				msg:'No record is selected.'
			},
			confirm:{
				title:'Confirm',
				msg:'Are you sure you want to delete?'
			}
		},
		destroyConfirmTitle: 'Confirm',			
		destroyConfirmMsg: 'Are you sure you want to delete?',
		
		url: null,	// return the datagrid data
		saveUrl: null,	// return the added row
		updateUrl: null,	// return the updated row
		destroyUrl: null,	// return {success:true}
		
		tree: null,		// the tree selector
		treeUrl: null,	// return tree data
		treeDndUrl: null,	// to process the drag and drop operation, return {success:true}
		treeTextField: 'name',
		treeParentField: 'parentId',
		
		onAdd: function(index, row){},
		onSave: function(index, row){},
		onDestroy: function(index, row){}
	});
})(jQuery);